
# Given a n-ary tree, find its maximum depth.
#
# The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
#
# Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).
#
#
#
# Example 1:
#
#
#
# Input: root = [1,null,3,2,4,null,5,6]
# Output: 3
# Example 2:
#
#
#
# Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: 5
#
#
# Constraints:
#
# The depth of the n-ary tree is less than or equal to 1000.
# The total number of nodes is between [0, 10^4].

from typing import List
from python3.practice.n_ary_tree import Node


class Solution:
    def maxDepth(self, root: 'Node') -> int:
        if not root:
            return 0

        result = []

        def bsf(nodes):
            if not nodes:
                return
            result.append([node.val for node in nodes])
            bsf([child for node in nodes for child in node.children])

        result.append([root.val])
        bsf(root.children)
        return len(result)

    def maxDepth(self, root: 'Node') -> int:
        if not root:
            return 0
        depth = 0
        stack = [root]
        while stack:
            size = len(stack)
            for i in range(size):
                node = stack.pop()
                stack.extend(node.children)
            depth += 1
        return depth


root = Node(1, children=[
    Node(2, children=[
        Node(5),
        Node(6),
        ]),
    Node(3),
    Node(4),
    ]
)

sol = Solution().maxDepth(root)
print(sol)
