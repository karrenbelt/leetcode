
# Given an n-ary tree, return the postorder traversal of its nodes' values.
#
# Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).
#
#
#
# Follow up:
#
# Recursive solution is trivial, could you do it iteratively?
#
#
#
# Example 1:
#
#
#
# Input: root = [1,null,3,2,4,null,5,6]
# Output: [5,6,3,2,4,1]
# Example 2:
#
#
#
# Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: [2,6,14,11,7,3,12,8,4,13,9,10,5,1]
#
#
# Constraints:
#
# The height of the n-ary tree is less than or equal to 1000
# The total number of nodes is between [0, 10^4]

from typing import List
from python3.practice.n_ary_tree import Node


class Solution:
    def postorder(self, root: 'Node', result=None) -> List[int]:  # recursive
        if result is None:
            result = []

        if root:
            for child in root.children:
                self.postorder(child, result)
            result.append(root.val)

        return result

    def preorder(self, root: 'Node') -> List[int]:  # iterative
        if not root:
            return []

        stack = [root]
        result = []
        while stack:
            node = stack.pop()
            stack.extend(reversed(node.children))
            result.append(node.val)

        return result


root = Node(1, children=[
    Node(2, children=[
        Node(5),
        Node(6),
        ]),
    Node(3),
    Node(4),
    ]
)

sol = Solution().postorder(root)


