
# Given an n-ary tree, return the level order traversal of its nodes' values.
#
# Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).
#
#
#
# Example 1:
#
#
#
# Input: root = [1,null,3,2,4,null,5,6]
# Output: [[1],[3,2,4],[5,6]]
# Example 2:
#
#
#
# Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
#
#
# Constraints:
#
# The height of the n-ary tree is less than or equal to 1000
# The total number of nodes is between [0, 10^4]

from typing import List
from python3.practice.n_ary_tree import Node


class Solution:

    def levelOrder(self, root: 'Node', result=None) -> List[List[int]]:  # recursive
        if not root:
            return

        result = []

        def bsf(nodes):
            if not nodes:
                return
            result.append([node.val for node in nodes])
            bsf([child for node in nodes for child in node.children])

        result.append([root.val])
        bsf(root.children)
        return result

    def levelOrder(self, root: 'Node') -> List[List[int]]:  # iterative
        q, ret = [root], []
        while any(q):
            ret.append([node.val for node in q])
            q = [child for node in q for child in node.children if child]
        return ret

    def levelOrder(self, root: 'Node') -> List[List[int]]:
        levels = [[root] if root else []]
        while levels[-1]:
            levels.append([child for node in levels[-1] for child in node.children])
        return [[node.val for node in level] for level in levels[:-1]]


root = Node(1, children=[
    Node(2, children=[
        Node(5),
        Node(6),
        ]),
    Node(3),
    Node(4),
    ]
)

sol = Solution().levelOrder(root)
print(sol)
