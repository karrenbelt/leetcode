
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children if children else []

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"
