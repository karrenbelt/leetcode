
# Given an n-ary tree, return the preorder traversal of its nodes' values.
#
# Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).
#
#
#
# Follow up:
#
# Recursive solution is trivial, could you do it iteratively?
#
#
#
# Example 1:
#
#
#
# Input: root = [1,null,3,2,4,null,5,6]
# Output: [1,3,5,6,2,4]
# Example 2:
#
#
#
# Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: [1,2,3,6,7,11,14,4,8,12,5,9,13,10]
#
#
# Constraints:
#
# The height of the n-ary tree is less than or equal to 1000
# The total number of nodes is between [0, 10^4]

from typing import List
from python3.practice.n_ary_tree import Node


class Solution:
    def preorder(self, root: 'Node', result=None) -> List[int]:  # recursive
        # Top Down:
        # 1. return specific value for null node
        # 2. update the answer if needed                              // answer <-- params
        # 3. for each child node root.children[k]:
        # 4.      ans[k] = top_down(root.children[k], new_params[k])  // new_params <-- root.val, params
        # 5. return the answer if needed

        if result is None:
            result = []

        if root:
            result.append(root.val)
            for child in root.children:
                self.preorder(child, result)

        return result

    def preorder(self, root: 'Node') -> List[int]:  # iterative
        if not root:
            return []

        stack = [root]
        result = []
        while stack:
            node = stack.pop()
            result.append(node.val)
            stack.extend(reversed(node.children))

        return result


root = Node(1, children=[
    Node(2, children=[
        Node(5),
        Node(6),
        ]),
    Node(3),
    Node(4),
    ]
)

sol = Solution().preorder(root)

