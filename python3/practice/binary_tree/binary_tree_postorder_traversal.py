
# Given the root of a binary tree, return the postorder traversal of its nodes' values.
#
#
#
# Example 1:
#
#
# Input: root = [1,null,2,3]
# Output: [3,2,1]
# Example 2:
#
# Input: root = []
# Output: []
# Example 3:
#
# Input: root = [1]
# Output: [1]
# Example 4:
#
#
# Input: root = [1,2]
# Output: [2,1]
# Example 5:
#
#
# Input: root = [1,null,2]
# Output: [2,1]
#
#
# Constraints:
#
# The number of the nodes in the tree is in the range [0, 100].
# -100 <= Node.val <= 100
#
#
# Follow up:
#
# Recursive solution is trivial, could you do it iteratively?

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def postorderTraversal(self, root: TreeNode, sol=None) -> List[int]:  # recursive
        if sol is None:
            sol = []
        if root:
            self.postorderTraversal(root.left, sol)
            self.postorderTraversal(root.right, sol)
            sol.append(root.val)
        return sol

    def postorderTraversal(self, root: TreeNode, sol=None) -> List[int]:  # using a stack
        if not root:
            return []

        stack = [root]
        res = []
        while stack:
            cur = stack.pop()
            res.append(cur.val)
            if cur.left:
                stack.append(cur.left)
            if cur.right:
                stack.append(cur.right)
        return res[::-1]
