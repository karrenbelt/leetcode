
# Implement pow(x, n), which calculates x raised to the power n (i.e. xn).
#
#
#
# Example 1:
#
# Input: x = 2.00000, n = 10
# Output: 1024.00000
# Example 2:
#
# Input: x = 2.10000, n = 3
# Output: 9.26100
# Example 3:
#
# Input: x = 2.00000, n = -2
# Output: 0.25000
# Explanation: 2-2 = 1/22 = 1/4 = 0.25
#
#
# Constraints:
#
# -100.0 < x < 100.0
# -231 <= n <= 231-1
# -104 <= xn <= 104


class Solution:

    def myPow(self, x: float, n: int) -> float:  # recursive
        if n <= 1:
            return 1 if n == 0 else x if n == 1 else 1 / self.myPow(x, -n)
        half = self.myPow(x, n // 2)
        return half * half * x if n % 2 else half * half

    def myPow(self, x: float, n: int) -> float:

        def fast(x, n):
            if n == 0:
                return 1
            half = fast(x, n // 2)

            if not n % 2:
                return half * half
            else:
                return x * half * half

        if n < 0:
            x = 1 / x
            n = -n

        return fast(x, n)

    def myPow(self, x: float, n: int) -> float:
        if n == 0:
            return 1
        elif n < 0:
            return 1 / self.myPow(x, abs(n))
        elif n % 2 != 0:
            return x * self.myPow(x, n - 1)
        else:
            return self.myPow(x * x, n // 2)


x = -5
n = 2
sol = Solution().myPow(x, n)
print(sol)
