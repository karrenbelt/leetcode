
# Given inorder and postorder traversal of a tree, construct the binary tree.
#
# Note:
# You may assume that duplicates do not exist in the tree.
#
# For example, given
#
# inorder = [9,3,15,20,7]
# postorder = [9,15,7,20,3]
# Return the following binary tree:
#
#     3
#    / \
#   9  20
#     /  \
#    15   7

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        # the tree root is the last element in postorder sequence
        # everything to the left of it in the inorder sequence forms the left subtree, on the right the right
        n = len(postorder) - 1
        postorder_index = n

        def build(start, end):
            nonlocal postorder_index

            if start > end:
                return

            node = TreeNode(postorder[postorder_index])
            postorder_index -= 1

            if start == end:
                return node

            inorder_index = inorder.index(node.val)
            node.right = build(inorder_index + 1, end)
            node.left = build(start, inorder_index - 1)
            return node

        return build(0, n)

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        # simpler and faster implementation
        def helper(in_left, in_right):
            if in_left > in_right:
                return None

            val = postorder.pop()
            node = TreeNode(val)
            index = idx_map[val]
            node.right = helper(index + 1, in_right)
            node.left = helper(in_left, index - 1)
            return node

        idx_map = dict(map(reversed, enumerate(inorder)))
        return helper(0, len(inorder) - 1)

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        if not inorder:
            return None
        root_index = inorder.index(postorder[-1])
        root = TreeNode(postorder[-1])
        root.left = self.buildTree(inorder[:root_index], postorder[:root_index])
        root.right = self.buildTree(inorder[root_index + 1:], postorder[root_index:-1])
        return root

# def inorderTraversal(root: TreeNode, li=None) -> List[int]:
#     if li is None: li = []
#     if root:
#         inorderTraversal(root.left, li)
#         li.append(root.val)
#         inorderTraversal(root.right, li)
#     return li
#
#
# def postorderTraversal(root: TreeNode, li=None) -> List[int]:
#     if li is None: li = []
#     if root:
#         postorderTraversal(root.left, li)
#         postorderTraversal(root.right, li)
#         li.append(root.val)
#     return li


inorder = [9, 3, 15, 20, 7]
postorder = [9, 15, 7, 20, 3]
root = Solution().buildTree(inorder=inorder, postorder=postorder)

# inorderTraversal(root) == inorder
# postorderTraversal(root) == postorder

