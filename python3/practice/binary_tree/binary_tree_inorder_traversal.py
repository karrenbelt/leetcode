
# Given the root of a binary tree, return the inorder traversal of its nodes' values.
#
#
#
# Example 1:
#
#
# Input: root = [1,null,2,3]
# Output: [1,3,2]
# Example 2:
#
# Input: root = []
# Output: []
# Example 3:
#
# Input: root = [1]
# Output: [1]
# Example 4:
#
#
# Input: root = [1,2]
# Output: [2,1]
# Example 5:
#
#
# Input: root = [1,null,2]
# Output: [1,2]
#
#
# Constraints:
#
# The number of nodes in the tree is in the range [0, 100].
# -100 <= Node.val <= 100
#
#
# Follow up:
#
# Recursive solution is trivial, could you do it iteratively?

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def inorderTraversal(self, root: TreeNode, sol=None) -> List[int]:  # recursive
        if sol is None:
            sol = []

        if root:  # depth-first search
            self.inorderTraversal(root.left, sol)
            sol.append(root.val)
            self.inorderTraversal(root.right, sol)

        return sol

    def inorderTraversal(self, root: TreeNode) -> List[int]:  # using a stack
        node = root
        sol = []
        stack = []
        while node or stack:
            if node:
                stack.append(node)
                node = node.left
            else:
                node = stack.pop()
                sol.append(node.val)
                node = node.right
        return sol




