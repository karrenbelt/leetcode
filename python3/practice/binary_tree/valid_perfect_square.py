
# Given a positive integer num, write a function which returns True if num is a perfect square else False.
#
# Follow up: Do not use any built-in library function such as sqrt.
#
#
#
# Example 1:
#
# Input: num = 16
# Output: true
# Example 2:
#
# Input: num = 14
# Output: false
#
#
# Constraints:
#
# 1 <= num <= 2^31 - 1

class Solution:
    def isPerfectSquare(self, num: int) -> bool:  # no built-in sqrt!
        return (num**0.5).is_integer()

    def isPerfectSquare(self, num: int) -> bool:  # binary search
        if num == 1:
            return True
        left, right = 1, num // 2
        while left <= right:
            mid = left + (right-left) // 2
            square = mid * mid
            if square == num:
                return True
            elif square < num:
                left = mid + 1
            elif square > num:
                right = mid - 1
        return False

    def isPerfectSquare(self, num: int) -> bool:  # newton's method
        r = num
        while r*r > num:
            r = (r + num/r) // 2
        return r*r == num

    def isPerfectSquare(self, num: int) -> bool:  # newton's method
        # 1 + 3 + ... + (2 * x - 1) = (2 * x - 1 + 1) * x / 2 = x * x
        i = 1
        while num:
            num -= i
            i += 2
        return num == 0


num = 9
sol = Solution().isPerfectSquare(num)