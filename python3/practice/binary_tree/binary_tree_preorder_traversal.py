
# Given the root of a binary tree, return the preorder traversal of its nodes' values.
#
# Input: root = [1,null,2,3]
# Output: [1,2,3]
# Example 2:
#
# Input: root = []
# Output: []
# Example 3:
#
# Input: root = [1]
# Output: [1]
#
# Input: root = [1,2]
# Output: [1,2]
#
# Input: root = [1,null,2]
# Output: [1,2]
#
# Constraints:
#
# The number of nodes in the tree is in the range [0, 100].
# -100 <= Node.val <= 100
#
#
# Follow up:
#
# Recursive solution is trivial, could you do it iteratively?

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def preorderTraversal(self, root: TreeNode, sol=None) -> List[int]:  # recursive
        if sol is None:
            sol = []

        if root:
            sol.append(root.val)
            self.preorderTraversal(root.left, sol)
            self.preorderTraversal(root.right, sol)
        return sol

    def preorderTraversal(self, root: TreeNode) -> List[int]:  # using a stack
        stack = [root]
        sol = []
        while stack:
            item = stack.pop()
            sol.append(item.val)
            if item.right:
                stack.append(item.right)
            if item.left:
                stack.append(item.left)
        return sol
