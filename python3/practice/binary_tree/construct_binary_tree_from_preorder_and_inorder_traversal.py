
# Given preorder and inorder traversal of a tree, construct the binary tree.
#
# Note:
# You may assume that duplicates do not exist in the tree.
#
# For example, given
#
# preorder = [3,9,20,15,7]
# inorder = [9,3,15,20,7]
# Return the following binary tree:
#
#     3
#    / \
#   9  20
#     /  \
#    15   7

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:  # using a non-local pointer
        preorder_index = 0

        def helper(start, end):
            nonlocal preorder_index
            if start == end:
                return None

            root = TreeNode(preorder[preorder_index])
            split = idx_map[root.val]
            preorder_index += 1
            root.left = helper(start, split)
            root.right = helper(split + 1, end)
            return root

        idx_map = dict(map(reversed, enumerate(inorder)))
        return helper(0, len(preorder))

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:  # without non-local

        def helper(in_left: int, in_right: int) -> TreeNode:
            if in_left >= in_right:
                return None
            root_val = preorder.pop(0)
            root = TreeNode(root_val)
            index = idx_map[root_val]
            root.left = helper(in_left, index)
            root.right = helper(index + 1, in_right)
            return root

        idx_map = dict(map(reversed, enumerate(inorder)))
        return helper(0, len(inorder))

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:  # most concise
        if not inorder:
            return None
        root_idx = inorder.index(preorder.pop(0))
        root = TreeNode(inorder[root_idx])
        root.left = self.buildTree(preorder, inorder[:root_idx])
        root.right = self.buildTree(preorder, inorder[root_idx + 1:])
        return root


preorder = [3, 9, 20, 15, 7]
inorder = [9, 3, 15, 20, 7]
sol = Solution().buildTree(preorder, inorder)
