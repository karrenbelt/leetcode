
# Given a binary tree, find its maximum depth.
#
# The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
#
# Note: A leaf is a node with no children.
#
# Example:
#
# Given binary tree [3,9,20,null,null,15,7],
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
# return its depth = 3.

# from collections import deque


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: TreeNode, depth=0) -> int:
        if root is None:
            return depth
        return max(self.maxDepth(root.left, depth + 1),
                   self.maxDepth(root.right, depth + 1))


# # values = [3, 9, 20, None, None, 15, 7]
# root = TreeNode()
# sol = Solution().maxDepth()
