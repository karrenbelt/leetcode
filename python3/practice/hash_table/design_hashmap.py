
# Design a HashMap without using any built-in hash table libraries.
#
# To be specific, your design should include these functions:
#
# put(key, value) : Insert a (key, value) pair into the HashMap. If the value already exists in the HashMap, update the value.
# get(key): Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key.
# remove(key) : Remove the mapping for the value key if this map contains the mapping for the key.
#
# Example:
#
# MyHashMap hashMap = new MyHashMap();
# hashMap.put(1, 1);
# hashMap.put(2, 2);
# hashMap.get(1);            // returns 1
# hashMap.get(3);            // returns -1 (not found)
# hashMap.put(2, 1);          // update the existing value
# hashMap.get(2);            // returns 1
# hashMap.remove(2);          // remove the mapping for 2
# hashMap.get(2);            // returns -1 (not found)
#
# Note:
#
# All keys and values will be in the range of [0, 1000000].
# The number of operations will be in the range of [1, 10000].
# Please do not use the built-in HashMap library.


class MyHashMap:
    # implemented as a List[List[Optional[Tuple[int, int]]]]
    def __init__(self):
        self.array = [[] for _ in range(100)]

    @staticmethod
    def hash(key):
        return key % 100

    def put(self, key: int, value: int) -> None:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(list(zip(*self.array[hash_key]))[0])
            if key in keys:
                self.array[hash_key][keys.index(key)] = (key, value)
            else:
                self.array[hash_key].append((key, value))
        else:
            self.array[hash_key].append((key, value))

    def get(self, key: int) -> int:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(zip(*self.array[hash_key]))[0]
            if key in keys:
                return self.array[hash_key][keys.index(key)][1]
        return -1

    def remove(self, key: int) -> None:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(zip(*self.array[hash_key]))[0]
            if key in keys:
                del self.array[hash_key] [keys.index(key)]


class ListNode(object):
    def __init__(self, key):
        self.key = key
        self.val = None
        self.next = None


class MyHashMap:
    # implemented as a linked-list
    size = 100

    def __init__(self):
        self.hashing = [ListNode(-1) for _ in range(self.size)]

    def put(self, key: int, value: int) -> None:
        head = self.hashing[key % self.size]
        current = head.next
        while current:
            if current.key == key: break
            current = current.next
        else:
            current = ListNode(key)
            current.next = head.next
            head.next = current
        current.val = value

    def get(self, key: int) -> int:
        current = self.hashing[key % self.size].next
        while current:
            if current.key == key:
                break
            current = current.next
        else:
            return -1
        return current.val

    def remove(self, key: int) -> None:
        current = self.hashing[key % self.size]
        while current and current.next:
            if current.next.key == key:
                break
            current = current.next
        else:
            return None
        current.next = current.next.next


# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)


hash_table = MyHashMap()
hash_table.put(1, 1)
print(hash_table.get(1))
hash_table.put(1, 2)
print(hash_table.get(1))
hash_table.get(1)
hash_table.remove(1)
self = hash_table
