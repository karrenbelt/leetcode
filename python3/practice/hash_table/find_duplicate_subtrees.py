
# Given the root of a binary tree, return all duplicate subtrees.
#
# For each kind of duplicate subtrees, you only need to return the root node of any one of them.
#
# Two trees are duplicate if they have the same structure with the same node values.
#
#
#
# Example 1:
#
#
# Input: root = [1,2,3,4,null,2,4,null,null,4]
# Output: [[2,4],[4]]
# Example 2:
#
#
# Input: root = [2,1,1]
# Output: [[1]]
# Example 3:
#
#
# Input: root = [2,2,2,3,null,3,null]
# Output: [[2,3],[3]]
#
#
# Constraints:
#
# The number of the nodes in the tree will be in the range [1, 10^4]
# -200 <= Node.val <= 200

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __repr__(self):
        return f"{self.__class__.__name__} {self.val}"


def serialize(root):
    return f'{root.val},{serialize(root.left)}{serialize(root.right)}' if root else ','


class Solution:

    def findDuplicateSubtrees(self, root: TreeNode) -> List[TreeNode]:  # works, but slow
        result = {}
        seen = set()

        def preorder(node):
            if not node:
                return
            s = serialize(node)
            if s in seen:
                result[s] = node  # overwrites 'duplicates'
            seen.add(s)
            preorder(node.left)
            preorder(node.right)

        preorder(root)
        return list(result.values())

    def findDuplicateSubtrees(self, root: TreeNode) -> List[TreeNode]:  # much faster
        count = {}
        ans = []

        def dfs(root):
            if not root:
                return ""
            serialized = f"{root.val},{dfs(root.left)},{dfs(root.right)},"
            if serialized in count:
                count[serialized] += 1
            else:
                count[serialized] = 1
            if count[serialized] == 2:
                ans.append(root)
            return serialized

        dfs(root)
        return ans


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.right.left = TreeNode(2)
root.right.left.left = TreeNode(4)
root.right.right = TreeNode(4)

sol = Solution().findDuplicateSubtrees(root)
print(sol)
