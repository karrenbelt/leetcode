
# Given a string s, find the length of the longest substring without repeating characters.
#
#
#
# Example 1:
#
# Input: s = "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.
# Example 2:
#
# Input: s = "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# Example 3:
#
# Input: s = "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
# Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
# Example 4:
#
# Input: s = ""
# Output: 0
#
#
# Constraints:
#
# 0 <= s.length <= 5 * 104
# s consists of English letters, digits, symbols and spaces.

class Solution:

    def lengthOfLongestSubstring(self, s: str) -> int:  # hash map to keep track of last index
        sub = {}
        longest, start, size = [0 for _ in range(3)]
        for i, c in enumerate(s):
            if c in sub and sub[c] >= start:
                start = sub[c] + 1
                size = i - sub[c]
            else:
                size += 1
                longest = max(size, longest)
            sub[c] = i
        return longest

    def lengthOfLongestSubstring(self, s: str) -> int:  # two-pointer, no hash map
        left = 0
        right = 1
        if len(s) <= 1:
            return len(s)
        l = right - left
        s += '$'
        while right < len(s):
            l = max(l, len(s[left:right]))
            if s[right] in s[left:right]:
                left += 1
            else:
                right += 1
        return l

    def lengthOfLongestSubstring(self, s: str) -> int:  # more concise, linear time, no hash map
        last_index = [-1] * 256
        res = 0
        i = 0
        for j in range(len(s)):
            i = max(i, last_index[ord(s[j])] + 1)
            res = max(res, j - i + 1)
            last_index[ord(s[j])] = j
        return res


s = 'pwwkew'  # 3
s = 'abcabcbb'  # 3
# s = ' '
# s = 'aabaab!bb'
sol = Solution().lengthOfLongestSubstring(s)
print(sol)
