
# Given two arrays, write a function to compute their intersection.
#
# Example 1:
#
# Input: nums1 = [1,2,2,1], nums2 = [2,2]
# Output: [2,2]
# Example 2:
#
# Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
# Output: [4,9]
# Note:
#
# Each element in the result should appear as many times as it shows in both arrays.
# The result can be in any order.
# Follow up:
#
# What if the given array is already sorted? How would you optimize your algorithm?
# What if nums1's size is small compared to nums2's size? Which algorithm is better?
# What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?

from typing import List


class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:  # simple but slow
        res = []
        for c in set(nums1).intersection(nums2):
            res.extend([c] * min(nums1.count(c), nums2.count(c)))
        return res

    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:  # fast

        def count(nums):
            d = {}
            for n in nums:
                if n in d:
                    d[n] += 1
                else:
                    d[n] = 1
            return d

        def min_value_matching_keys(c1, c2):
            l = []
            for k in set(c1).intersection(c2):
                l.extend([k] * min(c1[k], c2[k]))
            return l

        return min_value_matching_keys(count(nums1), count(nums2))

    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:  # shorter and faster

        def count(nums):
            d = {k: 0 for k in set(nums)}
            for n in nums:
                d[n] += 1
            return d

        def min_value_matching_keys(c1, c2):
            return [a for b in [[k] * min(c1[k], c2[k]) for k in set(c1).intersection(c2)] for a in b]

        return min_value_matching_keys(count(nums1), count(nums2))


nums1 = [4, 9, 5]
nums2 = [9, 4, 9, 8, 4]
nums1 = [1, 2, 2, 1]
nums2 = [2, 2]
sol = Solution().intersect(nums1, nums2)
print(sol)
