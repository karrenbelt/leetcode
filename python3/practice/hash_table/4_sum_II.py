
# Given four lists A, B, C, D of integer values, compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.
#
# To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500. All integers are in the range of -228 to 228 - 1 and the result is guaranteed to be at most 231 - 1.
#
# Example:
#
# Input:
# A = [ 1, 2]
# B = [-2,-1]
# C = [-1, 2]
# D = [ 0, 2]
#
# Output:
# 2
#
# Explanation:
# The two tuples are:
# 1. (0, 0, 0, 1) -> A[0] + B[0] + C[0] + D[1] = 1 + (-2) + (-1) + 2 = 0
# 2. (1, 1, 0, 0) -> A[1] + B[1] + C[0] + D[0] = 2 + (-1) + (-1) + 0 = 0

from typing import List


# def permutations_with_repetition(s):
#     base = len(s)
#     for n in range(base ** base):
#         yield [s[n // base ** (base - d - 1) % base] for d in range(base)]


def product(*args, repeat=1):
    # product('ABCD', 'xy') --> Ax Ay Bx By Cx Cy Dx Dy
    # product(range(2), repeat=3) --> 000 001 010 011 100 101 110 111
    pools = [tuple(pool) for pool in args] * repeat
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


class Solution:
    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:  # TLE
        ctr = 0
        zero_sets = set()
        for p in product(set(A), set(B), set(C), set(D)):
            if sum(p) == 0:
                zero_sets.add(p)
        if not zero_sets:
            return 0
        for p in product(A, B, C, D):
            if p in zero_sets:
                ctr += 1
        return ctr

    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:  # O(N^2)
        ctr = 0
        d = {}
        for i in range(len(A)):
            for j in range(len(B)):
                s = A[i] + B[j]
                d[s] = d[s] + 1 if s in d else 1
        for i in range(len(C)):
            for j in range(len(D)):
                s = C[i] + D[j]
                if -s in d:
                    ctr += d[-s]
        return ctr

    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:  # O(N^2), more concise
        ctr = 0
        hashmap = {}
        for a in A:
            for b in B:
                s = a + b
                hashmap[s] = hashmap[s] + 1 if s in hashmap else 1
        for c in C:
            for d in D:
                s = c + d
                ctr += hashmap[-s] if -s in hashmap else 0
        return ctr


A = [1, 2]
B = [-2, -1]
C = [-1, 2]
D = [0, 2]
sol = Solution().fourSumCount(A, B, C, D)
print(sol)
