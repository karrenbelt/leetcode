
# Given a string, find the first non-repeating character in it and return its index. If it doesn't exist, return -1.
#
# Examples:
#
# s = "leetcode"
# return 0.
#
# s = "loveleetcode"
# return 2.
#
#
# Note: You may assume the string contains only lowercase English letters.


class Solution:
    def firstUniqChar(self, s: str) -> int:
        # count = {k: 0 for k in list('abcdefghijklmnoprstquvwxyz')}
        d = {}
        for i, c in enumerate(s):
            if c not in d:
                d[c] = [1, i]
            else:
                d[c][0] += 1
        least = min(d.values(), key=lambda x: x[0]) if s else [-1, -1]
        return least[1] if least[0] == 1 else -1

    def firstUniqChar(self, s: str) -> int:
        seen = {}
        candidates = set(list(s))
        for i, c in enumerate(s):
            if c not in seen:
                seen[c] = i
            elif c in candidates:
                candidates.remove(c)
        return min([seen[c] for c in candidates]) if candidates else -1



s = 'elitecode'
s = 'ee'
sol = Solution().firstUniqChar(s)
print(sol)
