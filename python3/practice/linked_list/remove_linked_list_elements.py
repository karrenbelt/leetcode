
# Remove all elements from a linked list of integers that have value val.
#
# Example:
#
# Input:  1->2->6->3->4->5->6, val = 6
# Output: 1->2->3->4->5

from python3.practice.linked_list import Node


class Solution:
    def removeElements(self, head: Node, val: int) -> Node:

        while head and head.val == val:
            head = head.next

        node = prev = head
        while node:
            if node.val == val:
                prev.next = node.next
            else:
                prev = node
            node = node.next
        return head

    def removeElements(self, head: Node, val: int) -> Node:  # recursive solution:
        if head is None:
            return
        if head.val == val:
            return self.removeElements(head.next, val);
        head.next = self.removeElements(head.next, val)
        return head


head = Node(0, Node(1, Node(0, Node(2, Node(0)))))
val = 0
sol = Solution().removeElements(head, val)
print(sol)

