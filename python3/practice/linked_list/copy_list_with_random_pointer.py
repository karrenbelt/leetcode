
# A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.
#
# Return a deep copy of the list.
#
# The Linked List is represented in the input/output as a list of n nodes. Each node is represented as a pair of [val, random_index] where:
#
# val: an integer representing Node.val
# random_index: the index of the node (range from 0 to n-1) where random pointer points to, or null if it does not point to any node.
#
#
# Example 1:
#
#
# Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
# Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]
# Example 2:
#
#
# Input: head = [[1,1],[2,1]]
# Output: [[1,1],[2,1]]
# Example 3:
#
#
#
# Input: head = [[3,null],[3,0],[3,null]]
# Output: [[3,null],[3,0],[3,null]]
# Example 4:
#
# Input: head = []
# Output: []
# Explanation: Given linked list is empty (null pointer), so return null.
#
#
# Constraints:
#
# -10000 <= Node.val <= 10000
# Node.random is null or pointing to a node in the linked list.
# The number of nodes will not exceed 1000.


# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = x
        self.next = next
        self.random = random

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"


class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':  # O(n) time and O(n) space
        if not head:
            return head

        ordered, copy, head = [], [], head
        while node:
            ordered.append(node)
            copy.append(Node(node.val))
            node = node.next

        for i in range(len(copy)-1):
            copy[i].next = copy[i+1]
            if ordered[i].random:
                copy[i].random = copy[ordered.index(ordered[i].random)]
        if ordered[-1].random:
            copy[-1].random = copy[ordered.index(ordered[-1].random)]
        return copy[0]

    def copyRandomList(self, head):  # O(n) time and O(1) space
        dic, prev, node = {}, None, head
        while node:
            if node not in dic:
                dic[node] = Node(node.val, node.next, node.random)
            if prev:  # change pointer from copy to original
                prev.next = dic[node]
            else:  # store pointer to the head to return once done
                head = dic[node]
            if node.random:
                if node.random not in dic:  # if not yet seen, create copy
                    dic[node.random] = Node(node.random.val, node.random.next, node.random.random)
                dic[node].random = dic[node.random]  # set pointer from original to copy
            prev, node = dic[node], node.next
        return head


head = Node(0, Node(1))
sol = Solution().copyRandomList(head)
print(sol)

