
# You are given a doubly linked list which in addition to the next and previous pointers, it could have a child pointer,
# which may or may not point to a separate doubly linked list. These child lists may have one or more children of their
# own, and so on, to produce a multilevel data structure, as shown in the example below.
#
# Flatten the list so that all the nodes appear in a single-level, doubly linked list. You are given the head of the
# first level of the list.
#
#
#
# Example 1:
#
# Input: head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
# Output: [1,2,3,7,8,11,12,9,10,4,5,6]
# Explanation:
#
# The multilevel linked list in the input is as follows:
#
#
#
# After flattening the multilevel linked list it becomes:
#
#
# Example 2:
#
# Input: head = [1,2,null,3]
# Output: [1,3,2]
# Explanation:
#
# The input multilevel linked list is as follows:
#
#   1---2---NULL
#   |
#   3---NULL
# Example 3:
#
# Input: head = []
# Output: []
#
#
# How multilevel linked list is represented in test case:
#
# We use the multilevel linked list from Example 1 above:
#
#  1---2---3---4---5---6--NULL
#          |
#          7---8---9---10--NULL
#              |
#              11--12--NULL
# The serialization of each level is as follows:
#
# [1,2,3,4,5,6,null]
# [7,8,9,10,null]
# [11,12,null]
# To serialize all levels together we will add nulls in each level to signify no node connects to the upper node of the
# previous level. The serialization becomes:
#
# [1,2,3,4,5,6,null]
# [null,null,7,8,9,10,null]
# [null,11,12,null]
# Merging the serialization of each level and removing trailing nulls we obtain:
#
# [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
#
#
# Constraints:
#
# Number of Nodes will not exceed 1000.
# 1 <= Node.val <= 10^

from python3.practice.linked_list import Node


class Solution:

    def flatten(self, head: 'Node') -> 'Node':  # recursive solution
        if not head:
            return head

        def travel(node):
            while node:
                q = node.next
                if not q:
                    tail = node
                if node.child:
                    node.next = node.child
                    node.child.prev = node
                    tail = travel(node.child)
                    if q:
                        q.prev = tail
                    tail.next = q
                    node.child = None
                node = node.next
            return tail

        travel(head)
        return head

    def flatten(self, head: 'Node') -> 'Node':  # recursive solution

        def helper(head):
            prev, current_node = None, head
            while current_node:
                if current_node.child:
                    current_child, current_tail = current_node.child, helper(current_node.child)
                    current_node.child = None
                    original_next = current_node.next
                    if original_next:
                        original_next.prev = current_tail
                    current_tail.next = original_next
                    current_node.next, current_child.prev = current_child, current_node
                    prev, current_node = current_tail, original_next
                else:
                    prev, current_node = current_node, current_node.next
            return prev

        helper(head)
        return head

    def flatten(self, head: 'Node') -> 'Node':  # more consice recursive
        if not head:
            return head

        res = []

        def traverse(node):
            if node is not None:
                res.append(node)
                traverse(node.child)
                traverse(node.next)

        traverse(head)
        prev_node = res[0]
        for i in range(1, len(res)):
            res[i].prev, prev_node.next, prev_node = prev_node, res[i], res[i]
            res[i].child = None
        res[0].child = None
        return res[0]

    def flatten(self, head: 'Node') -> 'Node':  # using a stack and dummy node
        if not head:
            return

        dummy = Node(0, None, head, None)
        stack = [head]
        prev = dummy

        while stack:
            root = stack.pop()
            root.prev = prev
            prev.next = root
            if root.next:
                stack.append(root.next)
                root.next = None
            if root.child:
                stack.append(root.child)
                root.child = None
            prev = root
        dummy.next.prev = None
        return dummy.next

    def flatten(self, head: 'Node') -> 'Node':  # stack without dummy node. O(n) time and O(n) space
        if head:
            stack = []
            curr_node = head
            while curr_node:
                if curr_node.next:
                    stack.append(curr_node.next)
                if curr_node.child:
                    stack.append(curr_node.child)
                    curr_node.child = None
                if stack:
                    next_node = stack.pop()
                    curr_node.next = next_node
                    next_node.prev = curr_node
                curr_node = curr_node.next
        return head

    def flatten(self, head):  # more concise, O(n) time and O(n) space
        node, stack = head, []
        while node:
            if node.child:
                if node.next:
                    stack.append(node.next)
                node.next, node.next.prev, node.child = node.child, node, None
            if not node.next and stack:
                temp = stack.pop()
                temp.prev, node.next = node, temp
            node = node.next
        return head

    def flatten(self, head: 'Node') -> 'Node':  # two-pointers, O(n) time O(1) space
        if not head:
            return head
        node = head
        while node:
            if not node.child:
                node = node.next
                continue
            sub_node = node.child
            while sub_node.next:
                sub_node = sub_node.next
            sub_node.next = node.next
            if node.next:
                node.next.prev = sub_node
            node.next = node.child
            node.child.prev = node
            node.child = None
        return head


level3 = Node(11, Node(12))
level2 = Node(7, Node(8, Node(9, Node(10)), child=level3))
head = Node(1, Node(2, Node(3, Node(4, Node(5, Node(6))), child=level2)))

sol = Solution().flatten1(head)


nodes = []
while sol:
    nodes.append(sol)
    sol = sol.next

for node in nodes:
    print(f'{node.prev} <- {node} -> {node.next}   {node.child}')
