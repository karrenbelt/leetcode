# You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
#
# You may assume the two numbers do not contain any leading zero, except the number 0 itself.
#
#
# Example 1:
#
# Input: l1 = [2,4,3], l2 = [5,6,4]
# Output: [7,0,8]
# Explanation: 342 + 465 = 807.
# Example 2:
#
# Input: l1 = [0], l2 = [0]
# Output: [0]
# Example 3:
#
# Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
# Output: [8,9,9,9,0,0,0,1]
#
#
# Constraints:
#
# The number of nodes in each linked list is in the range [1, 100].
# 0 <= Node.val <= 9
# It is guaranteed that the list represents a number that does not have leading zeros.

from python3.practice.linked_list import ListNode


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:  # took quite a long time to get this one
        if not l1 or not l2:
            return l1 if l1 else l2
        carry, val = divmod(l1.val + l2.val, 10)
        head = ListNode(val)
        prev = head
        new = head
        l1, l2 = l1.next, l2.next
        while l1 or l2:
            carry, val = divmod((l1.val if l1 else 0) + (l2.val if l2 else 0) + carry, 10)
            new = ListNode(val)
            prev.next = new
            prev = new
            l1, l2 = l1.next if l1 else None, l2.next if l2 else None
        if carry > 0:
            new.next = ListNode(carry)
        return head

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:  # more concise
        carry = 0
        result = ListNode()
        node = result
        while l1 or l2:
            carry, val = divmod((l1.val if l1 else 0) + (l2.val if l2 else 0) + carry, 10)
            new_node = ListNode(val=val)
            node.next = new_node
            node = new_node
            l1, l2 = l1.next if l1 else None, l2.next if l2 else None
        if carry:
            node.next = ListNode(val=1)
        return result.next

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:  # most concise
        carry = 0
        res = n = ListNode(0)
        while l1 or l2 or carry:
            v1 = v2 = 0
            if l1:
                v1, l1 = l1.val, l1.next
            if l2:
                v2, l2 = l2.val, l2.next
            carry, val = divmod(v1 + v2 + carry, 10)
            n.next = ListNode(val)
            n = n.next
        return res.next


l1 = ListNode(1, ListNode(2, ListNode(5)))
l2 = ListNode(1, ListNode(2, ListNode(5)))
# l1 = ListNode(1, ListNode(2, ListNode(5, ListNode(4))))
#
l1 = ListNode(9)
l2 = ListNode(9, ListNode(9, ListNode(9)))

# l1 = ListNode(5)
# l2 = ListNode(5)

sol = Solution().addTwoNumbers(l1, l2)

values = []
while sol:
    values.append(sol.val)
    sol = sol.next
print(values)

# Solution found on leetcode that is the absolute fastest (cheat):
#
# f = open("user.out", "w")
# lines = __Utils__().read_lines()
# trash = {91: None, 93: None, 44: None, 10: None}
# while True:
#     try:
#         param_1 = int(next(lines).translate(trash)[::-1])
#         param_2 = int(next(lines).translate(trash)[::-1])
#         f.writelines(("[", ",".join(str(param_1 + param_2))[::-1], "]\n",))
#     except StopIteration:
#         break
# exit()
