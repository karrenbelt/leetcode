
# Design your implementation of the linked list. You can choose to use a singly or doubly linked list.
# A node in a singly linked list should have two attributes: val and next. val is the value of the current node,
# and next is a pointer/reference to the next node.
# If you want to use the doubly linked list, you will need one more attribute prev to indicate the previous node
# in the linked list. Assume all nodes in the linked list are 0-indexed.
#
# Implement the MyLinkedList class:
#
# MyLinkedList() Initializes the MyLinkedList object.
# int get(int index) Get the value of the indexth node in the linked list. If the index is invalid, return -1.
# void addAtHead(int val) Add a node of value val before the first element of the linked list. After the insertion,
# the new node will be the first node of the linked list.
# void addAtTail(int val) Append a node of value val as the last element of the linked list.
# void addAtIndex(int index, int val) Add a node of value val before the indexth node in the linked list.
# If index equals the length of the linked list, the node will be appended to the end of the linked list.
# If index is greater than the length, the node will not be inserted.
# void deleteAtIndex(int index) Delete the indexth node in the linked list, if the index is valid.
#
#
# Example 1:
#
# Input
# ["MyLinkedList", "addAtHead", "addAtTail", "addAtIndex", "get", "deleteAtIndex", "get"]
# [[], [1], [3], [1, 2], [1], [1], [1]]
# Output
# [null, null, null, null, 2, null, 3]
#
# Explanation
# MyLinkedList myLinkedList = new MyLinkedList();
# myLinkedList.addAtHead(1);
# myLinkedList.addAtTail(3);
# myLinkedList.addAtIndex(1, 2);    // linked list becomes 1->2->3
# myLinkedList.get(1);              // return 2
# myLinkedList.deleteAtIndex(1);    // now the linked list is 1->3
# myLinkedList.get(1);              // return 3
#
#
# Constraints:
#
# 0 <= index, val <= 1000
# Please do not use the built-in LinkedList library.
# At most 2000 calls will be made to get, addAtHead, addAtTail,  addAtIndex and deleteAtIndex.

from python3.practice.linked_list import Node


class MyLinkedList:
    """
    Singly Linked List
    """
    def __init__(self):
        self.head = None

    def get_node(self, index: int) -> int:
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        """
        if index < 0 or not self.head:
            return
        node, i = self.head, 0
        while node.next and i < index:
            node = node.next
            i += 1
        return node if i == index else None

    def get(self, index: int) -> int:
        node = self.get_node(index)
        return node.val if node else -1

    def addAtHead(self, val: int) -> None:
        self.head = Node(val, next=self.head)

    def addAtTail(self, val: int) -> None:
        node = self.head
        if not node:
            self.head = Node(val)
        else:
            while node.next:
                node = node.next
            node.next = Node(val)

    def addAtIndex(self, index: int, val: int) -> None:
        if index == 0:
            self.addAtHead(val)
        else:
            prev = self.get_node(index-1)
            if prev:
                prev.next = Node(val, next=prev.next)

    def deleteAtIndex(self, index: int) -> None:
        if index == 0 and self.head:
            self.head = self.head.next
        else:
            prev = self.get_node(index-1)
            if prev and prev.next:
                prev.next = prev.next.next

    def __repr__(self):
        nodes = []
        current_node = self.head
        while current_node is not None:
            nodes.append(current_node.val)
            current_node = current_node.next
        return f"{self.__class__.__name__}: {'->'.join(map(str, nodes))}"


class MyLinkedList:
    """
    Doubly Linked List
    """
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def get_node(self, index):
        if index < 0 or index >= self.size:
            return
        elif index < self.size // 2:  # speed up the search
            node, i = self.head, 0
            while i < index:
                node = node.next
                i += 1
        else:
            node, i = self.tail, self.size - 1
            while i > index:
                node = node.prev
                i -= 1
        return node

    def get(self, index):
        return -1 if index < 0 or index >= self.size else self.get_node(index).val

    def addAtHead(self, val):
        n = Node(val)
        if not self.head:
            self.head = self.tail = n
        else:
            self.head.prev, n.next, self.head = n, self.head, n
        self.size += 1

    def addAtTail(self, val):
        n = Node(val)
        if not self.head:
            self.head = self.tail = n
        else:
            n.prev, self.tail.next, self.tail = self.tail, n, n
        self.size += 1

    def addAtIndex(self, index, val):
        if index < 0 or index > self.size:
            return
        if index == 0:
            self.addAtHead(val)
        elif index == self.size:
            self.addAtTail(val)
        else:
            node = self.get_node(index)
            n = Node(val)
            n.prev, n.next, node.prev.next, node.prev = node.prev, node, n, n
            self.size += 1

    def deleteAtIndex(self, index):
        if index < 0 or index >= self.size:
            return
        if self.size == 1:  # delete last and only node
            self.head = self.tail = None
        elif index == 0:  # delete head
            self.head.next.prev, self.head = None, self.head.next
        elif index == self.size - 1:  # delete tail
            self.tail.prev.next, self.tail = None, self.tail.prev
        else:
            node = self.get_node(index)
            node.next.prev, node.prev.next = node.prev, node.next
        self.size -= 1

    def __repr__(self):
        nodes = []
        current_node = self.head
        while current_node is not None:
            nodes.append(current_node.val)
            current_node = current_node.next
        return f"{self.__class__.__name__}: {'->'.join(map(str, nodes))}"


# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)


l = MyLinkedList()
l.addAtHead(3)
l.addAtTail(4)
l.addAtHead(2)
l.addAtTail(5)
l.addAtHead(1)
l.get(3)
l.get(0)
l.addAtIndex(2, 100)
l.deleteAtIndex(0)


# another
# ["MyLinkedList","addAtHead","addAtHead","addAtHead","addAtIndex","deleteAtIndex","addAtHead","addAtTail","get","addAtHead","addAtIndex","addAtHead"]
# [[],[7],[2],[1],[3,0],[2],[6],[4],[4],[4],[5,0],[6]]
l = MyLinkedList()
l.addAtHead(7)
l.addAtHead(2)
l.addAtHead(1)

l.addAtIndex(3, 0)
l.deleteAtIndex(2)
l.addAtHead(6)
l.addAtTail(4)
l.get(4)
l.addAtHead(4)
l.addAtIndex(5, 0)
l.addAtHead(6)




