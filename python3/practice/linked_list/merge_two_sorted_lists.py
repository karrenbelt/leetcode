
# Merge two sorted linked lists and return it as a new sorted list. The new list should be made by splicing together the nodes of the first two lists.
#
#
#
# Example 1:
#
#
# Input: l1 = [1,2,4], l2 = [1,3,4]
# Output: [1,1,2,3,4,4]
# Example 2:
#
# Input: l1 = [], l2 = []
# Output: []
# Example 3:
#
# Input: l1 = [], l2 = [0]
# Output: [0]
#
#
# Constraints:
#
# The number of nodes in both lists is in the range [0, 50].
# -100 <= Node.val <= 100
# Both l1 and l2 are sorted in non-decreasing order.

from python3.practice.linked_list import ListNode


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # iterating twice, not O(1) space

        def add(n1, n2):
            if n1 and n2:
                if n1.val > n2.val:
                    stack.append(n2)
                    add(n1, n2.next)
                else:
                    stack.append(n1)
                    add(n1.next, n2)
            elif n1 or n2:
                stack.append(n1 if n1 else n2)

        stack = []
        add(l1, l2)
        for i in range(len(stack)-1):
            stack[i].next = stack[i+1]
        return stack[0] if stack else l1

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # pointer
        pointer = result = ListNode()
        while l1 and l2:
            if l1.val <= l2.val:
                tmp, l1 = l1, l1.next
                tmp.next, pointer.next = None, tmp
            else:
                tmp, l2 = l2, l2.next
                tmp.next, pointer.next = None, tmp
            pointer = pointer.next
        pointer.next = l1 or l2
        return result.next


