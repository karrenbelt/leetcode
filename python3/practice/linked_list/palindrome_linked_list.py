
# Given a singly linked list, determine if it is a palindrome.
#
# Example 1:
#
# Input: 1->2
# Output: false
# Example 2:
#
# Input: 1->2->2->1
# Output: true
# Follow up:
# Could you do it in O(n) time and O(1) space?


from python3.practice.linked_list import Node


class Solution:
    def isPalindrome(self, head: Node) -> bool:  # simplest, not O(1) space
        sequence = []
        node = head
        while node:
            sequence.append(node.val)
            node = node.next
        for i in range(len(sequence)//2):
            if sequence[i] != sequence[-i-1]:
                return False
        return True

    def isPalindrome(self, head: Node) -> bool:  # two-pointer, O(1) space: find middle, reverse half, compare
        slow = fast = head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
        prev = None
        while slow:
            slow_next, slow.next = slow.next, prev
            prev, slow = slow, slow_next
        while prev:
            if prev.val != head.val:
                return False
            head, prev = head.next, prev.next
        return True


head = Node(1, Node(2, Node(3, Node(2, Node(1)))))
sol = Solution().isPalindrome(head)
print(sol)

