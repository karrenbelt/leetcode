
# Given the head of a linked list, remove the nth node from the end of the list and return its head.
#
# Follow up: Could you do this in one pass?
#
#
#
# Example 1:
#
#
# Input: head = [1,2,3,4,5], n = 2
# Output: [1,2,3,5]
# Example 2:
#
# Input: head = [1], n = 1
# Output: []
# Example 3:
#
# Input: head = [1,2], n = 1
# Output: [1]
#
#
# Constraints:
#
# The number of nodes in the list is sz.
# 1 <= sz <= 30
# 0 <= Node.val <= 100
# 1 <= n <= sz

from python3.practice.linked_list import ListNode


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        l = []
        node = head
        while node:
            l.append(node)
            node = node.next
        index = len(l) - n
        if index == 0:
            return head.next
        else:
            prev = l[index-1]
            prev.next = l[index].next
        return head

    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:  # two-pointer, O(1) space
        fast = slow = head
        i = 0
        while fast and i < n:
            fast = fast.next
            i += 1
        if not fast:
            return head.next
        while fast.next:
            fast = fast.next
            slow = slow.next
        slow.next = slow.next.next
        return head


head = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5)))))
n = 6
sol = Solution().removeNthFromEnd(head, n)
print(sol)
