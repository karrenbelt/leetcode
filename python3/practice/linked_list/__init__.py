
class Node:
    def __init__(self, val=None, next=None, prev=None, child=None):
        self.val = val
        self.next = next
        self.prev = prev
        self.child = child

    def __int__(self):
        return int(self.val)

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"


ListNode = Node  # naming is inconsistent on leetcode

