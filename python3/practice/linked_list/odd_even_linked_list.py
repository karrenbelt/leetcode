
# Given a singly linked list, group all odd nodes together followed by the even nodes. Please note here we are talking about the node number and not the value in the nodes.
#
# You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.
#
# Example 1:
#
# Input: 1->2->3->4->5->NULL
# Output: 1->3->5->2->4->NULL
# Example 2:
#
# Input: 2->1->3->5->6->4->7->NULL
# Output: 2->3->6->7->1->5->4->NULL
#
#
# Constraints:
#
# The relative order inside both the even and odd groups should remain as it was in the input.
# The first node is considered odd, the second node even and so on ...
# The length of the linked list is between [0, 10^4].

from python3.practice.linked_list import Node


class Solution:
    def oddEvenList(self, head: Node) -> Node:  # not O(1) space
        if not head:
            return
        node = head
        tail = []
        while node and node.next and node.next.next:
            tail.append(node.next)
            node.next = node.next.next
            node = node.next
        tail.append(node.next)
        for even_node in tail:
            node.next = even_node
            node = even_node
        return head

    def oddEvenList(self, head: Node) -> Node:  # two-pointer, O(1) space
        if not head:
            return head
        odd, even, even_head = head, head.next, head.next
        while even and even.next:
            odd.next, odd = odd.next.next, odd.next.next
            even.next, even = even.next.next, even.next.next
        odd.next = even_head
        return head

    def oddEvenList(self, head: Node) -> Node:  # two-pointer, O(1) space, more concise
        if not head:
            return head
        odd, even, even_head = head, head.next, head.next
        while even and even.next:
            odd.next, odd = even.next, even.next
            even.next, even = odd.next, odd.next
        odd.next = even_head
        return head


head = Node(1, Node(2, Node(3, Node(4))))
# head = Node(1, Node(2, Node(3, Node(4, Node(5)))))
sol = Solution().oddEvenList(head)


# 1 2 3 4 5 6 7 - 2 0
# 1 3 2 4 5 6 7 - 4 2
# 1 3 5 2 4 6 7 - 6 3
# 1 3 5 7 2 4 6


