
# Given the head of a linked list, rotate the list to the right by k places.
#
#
#
# Example 1:
#
#
# Input: head = [1,2,3,4,5], k = 2
# Output: [4,5,1,2,3]
# Example 2:
#
#
# Input: head = [0,1,2], k = 4
# Output: [2,0,1]
#
#
# Constraints:
#
# The number of nodes in the list is in the range [0, 500].
# -100 <= Node.val <= 100
# 0 <= k <= 2 * 109

from python3.practice.linked_list import ListNode


class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:
        if not head or not head.next or not k:
            return head
        ordered = []
        node = head
        while node:
            ordered.append(node)
            node = node.next
        k %= len(ordered)
        ordered[-1].next = head  # make circular
        ordered[-k - 1].next = None  # break circle
        return ordered[-k]

    # def rotateRight(self, head: ListNode, k: int) -> ListNode:
    #     c, h = 0, head
    #     while h is not None:
    #         c += 1
    #         h = h.next
    #     if c == 0 or k % c == 0:
    #         return head
    #     k = k % c
    #     s, f = head, head
    #     for _ in range(k):
    #         f = f.next
    #     while f.next is not None:
    #         f = f.next
    #         s = s.next
    #     nh = s.next
    #     s.next = None
    #     f.next = head
    #     return nh


head = ListNode(1, ListNode(2, ListNode(3, ListNode(4))))
head = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5)))))
k = 2

head = ListNode(1, ListNode(2))
k = 1
sol = Solution().rotateRight(head, k)

nodes = []
node = sol
while node:
    nodes.append(node)
    node = node.next
print(nodes)
