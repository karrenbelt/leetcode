
# Reverse a singly linked list.
#
# Example:
#
# Input: 1->2->3->4->5->NULL
# Output: 5->4->3->2->1->NULL
# Follow up:
#
# A linked list can be reversed either iteratively or recursively. Could you implement both?

from python3.practice.linked_list import Node


class Solution:
    def reverseList(self, head: Node) -> Node:
        return self.solve(head)

    def solve(self, head, back=None):  # recursive solution
        if not head:
            return
        temp = head.next
        head.next = back
        back = head
        if not temp:
            return head
        head = temp
        return self.solve(head, back)

    def solve(self, head):  # iterative solution
        prev = None
        while head:
            temp = head
            head = head.next
            temp.next = prev
            prev = temp
        return prev


head = Node(1, Node(2, Node(3, Node(4, Node(5)))))
sol = Solution().reverseList(head)
print(sol)
