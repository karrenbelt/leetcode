
# The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each other.
#
# Given an integer n, return the number of distinct solutions to the n-queens puzzle.
#
#
#
# Example 1:
#
#
# Input: n = 4
# Output: 2
# Explanation: There are two distinct solutions to the 4-queens puzzle as shown.
# Example 2:
#
# Input: n = 1
# Output: 1
#
#
# Constraints:
#
# 1 <= n <= 9

class Solution:

    def totalNQueens(self, n: int) -> int:
        col = [False] * n
        diag = [False] * (2 * n - 1)
        r_diag = [False] * (2 * n - 1)

        def dfs(r):
            nonlocal ans
            if r == n:
                ans += 1
                return
            for c in range(n):
                if not col[c] and not diag[r - c] and not r_diag[r + c]:
                    col[c] = diag[r - c] = r_diag[r + c] = True  # place Queen
                    dfs(r + 1)  # go next row
                    col[c] = diag[r - c] = r_diag[r + c] = False  # remove Queen

        ans = 0
        dfs(0)
        return ans

    def totalNQueens(self, n: int) -> int:

        def is_valid(nums, n):
            for i in range(n):
                if nums[i] == nums[n] or abs(nums[n] - nums[i]) == n - i:
                    return False
            return True

        def dfs(nums, index):
            nonlocal ans
            if index == n:
                ans += 1
                return  # backtracking
            for i in range(len(nums)):
                nums[index] = i
                if is_valid(nums, index):
                    dfs(nums, index + 1)

        ans = 0
        dfs([-1] * n, 0)
        return ans


n = 5
sol = Solution().totalNQueens(n)
print(sol)
