# Given an array of integers nums, sort the array in ascending order.
#
#
#
# Example 1:
#
# Input: nums = [5,2,3,1]
# Output: [1,2,3,5]
# Example 2:
#
# Input: nums = [5,1,1,2,0,0]
# Output: [0,0,1,1,2,5]
#
#
# Constraints:
#
# 1 <= nums.length <= 50000
# -50000 <= nums[i] <= 50000

from typing import List


class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:  # this is not the exercise, obviously
        nums.sort()
        return nums

    def sortArray(self, nums: List[int]) -> List[int]:  # top-down

        def merge_sort(nums):
            if len(nums) <= 1:  # forgot this one first time
                return nums
            pivot = len(nums) // 2
            left_list = merge_sort(nums[:pivot])
            right_list = merge_sort(nums[pivot:])
            return merge(left_list, right_list)

        def merge(left_list, right_list):
            left = right = 0
            ans = []
            while left < len(left_list) and right < len(right_list):
                if left_list[left] < right_list[right]:
                    ans.append(left_list[left])
                    left += 1
                else:
                    ans.append(right_list[right])
                    right += 1
            ans.extend(left_list[left:])
            ans.extend(right_list[right:])
            return ans

        return merge_sort(nums)

    def sortArray(self, nums: List[int]) -> List[int]:  # top-down, NOT in place

        def merge_sort(nums):
            if len(nums) <= 1:  # forgot this one first time
                return nums
            pivot = len(nums) // 2
            left_list = merge_sort(nums[:pivot])
            right_list = merge_sort(nums[pivot:])
            return merge(left_list, right_list)

        def merge(left_list, right_list):
            left = right = 0
            ans = []
            while left < len(left_list) and right < len(right_list):
                if left_list[left] < right_list[right]:
                    ans.append(left_list[left])
                    left += 1
                else:
                    ans.append(right_list[right])
                    right += 1
            ans.extend(left_list[left:])
            ans.extend(right_list[right:])
            return ans

        return merge_sort(nums)

    def sortArray(self, nums: List[int]) -> List[int]:  # O(n log n) time  (average case, worst case is O(n^2))
        # e.g. pick pivot as median of three: pick first, last and middle element, sort and pick median to balance

        def qsort(lst, lo, hi):
            if lo < hi:
                p = partition(lst, lo, hi)
                qsort(lst, lo, p - 1)
                qsort(lst, p + 1, hi)

        def partition(lst, lo, hi):
            pivot = lst[hi]
            i = lo
            for j in range(lo, hi):
                if lst[j] < pivot:
                    lst[i], lst[j] = lst[j], lst[i]
                    i += 1
            lst[i], lst[hi] = lst[hi], lst[i]
            return i  # index of pivot value in the sorted array

        qsort(nums, 0, len(nums) - 1)
        return nums  # sorted in-place


nums = [5, 1, 1, 2, 0, 0]
sol = Solution().sortArray(nums)
print(sol)
