# Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, find the area of largest rectangle in the histogram.
#
#
#
#
# Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].
#
#
#
#
# The largest rectangle is shown in the shaded area, which has area = 10 unit.
#
#
#
# Example:
#
# Input: [2,1,5,6,2,3]
# Output: 10

from typing import List


class Solution:

    def largestRectangleArea(self, heights: List[int]) -> int:  # O(n)

        # left[i], right[i] represent how many bars are >= than the current bar
        left = [1] * len(heights)
        right = [1] * len(heights)
        max_rect = 0

        for i in range(0, len(heights)):
            j = i - 1
            while j >= 0:
                if heights[j] >= heights[i]:
                    left[i] += left[j]
                    j -= left[j]
                else:
                    break

        for i in range(len(heights) - 1, -1, -1):
            j = i + 1
            while j < len(heights):
                if heights[j] >= heights[i]:
                    right[i] += right[j]
                    j += right[j]
                else:
                    break

        for i in range(len(heights)):
            max_rect = max(max_rect, heights[i] * (left[i] + right[i] - 1))

        return max_rect


heights = [2, 1, 5, 6, 2, 3]
sol = Solution().largestRectangleArea(heights)
print(sol)
