
# Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
#
#
#
# Example 1:
#
# Input: n = 3
# Output: ["((()))","(()())","(())()","()(())","()()()"]
# Example 2:
#
# Input: n = 1
# Output: ["()"]
#
#
# Constraints:
#
# 1 <= n <= 8

from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:  # dfs

        def dfs(p, left, right):
            if left:
                dfs(p + '(', left - 1, right)
            if right > left:
                dfs(p + ')', left, right - 1)
            if not right:  # all brackets closed, append the solution
                ans.append(p)

        ans = []
        dfs('', n, n)
        return ans

    def generateParenthesis(self, n: int) -> List[str]:  # iterative
        ans = ['()']
        for i in range(2, n + 1):
            new = set()
            for each in ans:
                for j in range(len(each) + 1):
                    new.add(each[:j] + '()' + each[j:])
            ans = list(new)
        return ans


n = 4
sol = Solution().generateParenthesis(n)
print(sol)
