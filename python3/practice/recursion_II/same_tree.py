
# Given the roots of two binary trees p and q, write a function to check if they are the same or not.
#
# Two binary trees are considered the same if they are structurally identical, and the nodes have the same value.
#
#
#
# Example 1:
#
#
# Input: p = [1,2,3], q = [1,2,3]
# Output: true
# Example 2:
#
#
# Input: p = [1,2], q = [1,null,2]
# Output: false
# Example 3:
#
#
# Input: p = [1,2,1], q = [1,1,2]
# Output: false
#
#
# Constraints:
#
# The number of nodes in both trees is in the range [0, 100].
# -104 <= Node.val <= 104

from python3.practice.recursion_II import TreeNode


class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        from collections import deque

        def are_equal(p, q):
            if not p and not q:
                return True
            if not p or not q:
                return False
            return p.val == q.val

        queue = deque([(p, q), ])
        while queue:
            p, q = queue.popleft()
            if not are_equal(p, q):
                return False
            elif p and q:
                queue.append((p.left, q.left))
                queue.append((p.right, q.right))
        return True

    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:

        def are_equal(p, q):
            if not p and not q:
                return True
            if not p or not q:
                return False
            return p.val == q.val

        stack = [(p, q), ]
        while stack:
            p, q = stack.pop()
            if not are_equal(p, q):
                return False
            elif p and q:
                stack.append((p.left, q.left))
                stack.append((p.right, q.right))
        return True

