
# Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.
#
# According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
#
#
#
# Example 1:
#
#
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
# Output: 6
# Explanation: The LCA of nodes 2 and 8 is 6.
# Example 2:
#
#
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
# Output: 2
# Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant of itself according to the LCA definition.
# Example 3:
#
# Input: root = [2,1], p = 2, q = 1
# Output: 2
#
#
# Constraints:
#
# The number of nodes in the tree is in the range [2, 105].
# -10^9 <= Node.val <= 10^9
# All Node.val are unique.
# p != q
# p and q will exist in the BST.


from python3.practice.binary_search_tree import TreeNode


class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        # store the path if the first find, see where the other deviates
        node = root
        while node:
            next_p = node if node.val == p.val else node.left if p.val < node.val else node.right
            next_q = node if node.val == q.val else node.left if q.val < node.val else node.right
            if next_p is not next_q:
                return node
            node = next_p

    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        if p.val < root.val and q.val < root.val:
            return self.lowestCommonAncestor(root.left, p, q)
        elif p.val > root.val and q.val > root.val:
            return self.lowestCommonAncestor(root.right, p, q)
        return root


root = TreeNode(6,
                TreeNode(2, TreeNode(0), TreeNode(4, TreeNode(3), TreeNode(5))),
                TreeNode(8, TreeNode(7), TreeNode(9)),
                )

# p = root.left
# q = root.right
p = root.left
q = root.left.right
sol = Solution().lowestCommonAncestor(root, p, q)
print(sol)
