# Given an array of integers, find out whether there are two distinct indices i and j in the array such that the absolute difference between nums[i] and nums[j] is at most t and the absolute difference between i and j is at most k.
#
#
#
# Example 1:
#
# Input: nums = [1,2,3,1], k = 3, t = 0
# Output: true
# Example 2:
#
# Input: nums = [1,0,1,1], k = 1, t = 2
# Output: true
# Example 3:
#
# Input: nums = [1,5,9,1,5,9], k = 2, t = 3
# Output: false
#
#
# Constraints:
#
# 0 <= nums.length <= 2 * 104
# -231 <= nums[i] <= 231 - 1
# 0 <= k <= 104
# 0 <= t <= 231 - 1

from typing import List


# exercise is to do it with a BST. We can use a self-balancing BST, of which I found there exist many flavors
# https://en.wikipedia.org/wiki/Self-balancing_binary_search_tree
# Since I haven't learned these yet, the first implementation will be a regular BST build using sorted nums

from python3.practice.binary_search_tree import BinarySearchTree


class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:  # O(n^2) -> TLE
        for i in range(len(nums)):
            for j in range(i + 1, min(i + k + 1, len(nums))):
                if abs(nums[i] - nums[j]) <= t:
                    return True
        return False

    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:  # O(n log k)
        tree = BinarySearchTree(nums)

        for i, num in enumerate(nums):
            predecessor = tree.predecessor(num)
            if predecessor and abs(predecessor - num) <= t:
                return True
            successor = tree.successor(num)
            if successor and abs(successor - num) <= t:
                return True
            tree.insert(num)

            if tree.size > k:
                tree.delete(nums[i - k])

        return False

    # def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:  # Bucket sort: O(n)
    #     if t < 0:
    #         return False
    #
    #     d = {}
    #     for i, num in enumerate(nums):
    #         b = num // (t + 1)  # bucket for the number
    #         # if it exists in d, or either of the neighboring buckets, we're done
    #         if b in d or (b - 1 in d and num - d[b - 1] <= t) or (b + 1 in d and d[b + 1] - num <= t):
    #             return True
    #         d[b] = num
    #         if i >= k:  # we passed the sliding window, remove the leftmost value from it's bucket
    #             del d[nums[i - k] // (t + 1)]
    #     return False


nums = [1, 2, 3, 1]
k = 3
t = 0
sol = Solution().containsNearbyAlmostDuplicate(nums, k, t)
print(sol)
