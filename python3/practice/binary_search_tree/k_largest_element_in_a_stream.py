# Design a class to find the kth largest element in a stream. Note that it is the kth largest element in the sorted order, not the kth distinct element.
#
# Implement KthLargest class:
#
# KthLargest(int k, int[] nums) Initializes the object with the integer k and the stream of integers nums.
# int add(int val) Returns the element representing the kth largest element in the stream.
#
#
# Example 1:
#
# Input
# ["KthLargest", "add", "add", "add", "add", "add"]
# [[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
# Output
# [null, 4, 5, 5, 8, 8]
#
# Explanation
# KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
# kthLargest.add(3);   // return 4
# kthLargest.add(5);   // return 5
# kthLargest.add(10);  // return 5
# kthLargest.add(9);   // return 8
# kthLargest.add(4);   // return 8
#
#
# Constraints:
#
# 1 <= k <= 104
# 0 <= nums.length <= 104
# -104 <= nums[i] <= 104
# -104 <= val <= 104
# At most 104 calls will be made to add.
# It is guaranteed that there will be at least k elements in the array when you search for the kth element.

from typing import List
from python3.practice.binary_search_tree import TreeNode


class KthLargest:

    class __Node(TreeNode):

        def __init__(self, val):
            super().__init__(val)
            self.height = 1
            self.cnt = 1

    def __init__(self, k, nums):
        self.k = k
        self.root = None
        for num in nums:
            self.root = self.insert(self.root, num)

    def insert(self, root, val):
        # Perform common BST insertion
        if not root:
            return self.__Node(val)
        root.cnt += 1
        if val < root.val:
            root.left = self.insert(root.left, val)
        else:
            root.right = self.insert(root.right, val)

        # Update Height
        root.height = 1 + max(self.get_height(root.left), self.get_height(root.right))

        # Get the balance factor and re-balance the tree
        balance = self.get_balance(root)

        # Left-left case
        if balance > 1 and self.get_balance(root.left) >= 0:
            return self.rightRotate(root)

        # Right-right case
        if balance < -1 and self.get_balance(root.right) <= 0:
            return self.leftRotate(root)

        # Left-right case
        if balance > 1 and self.get_balance(root.right) <= 0:
            root.left = self.leftRotate(root.left)
            return self.rightRotate(root)

        # Right-left case
        if balance < -1 and self.get_balance(root.left) >= 0:
            root.right = self.rightRotate(root.right)
            return self.leftRotate(root)

        return root

    def leftRotate(self, z):
        """ Implement left rotate operation on subtree z"""
        y = z.right
        T2 = y.left

        # Perform rotation
        y.left = z
        z.right = T2


        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))
        z.cnt = self.get_count(z)
        y.cnt = self.get_count(y)
        return y

    def rightRotate(self, z):
        """ Implement right rotate operation on subtree z """
        y = z.left
        T3 = y.right
        # Perform rotation
        y.right = z
        z.left = T3

        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))
        z.cnt = self.get_count(z)
        y.cnt = self.get_count(y)

        return y

    def get_height(self, root):
        """ Returns height of the (sub)tree represented by root """
        if not root:
            return 0
        return root.height

    def get_balance(self, root):
        """ Get the balance factor of the (sub)tree represented by root """
        if not root:
            return 0
        return self.get_height(root.left) - self.get_height(root.right)

    def get_count(self, root):
        if not root:
            return 0
        left = self.get_count(root.left)
        right = self.get_count(root.right)
        return left + right + 1

    def find_kth(self, root):
        k = self.k
        cur = self.root
        while k > 0:
            larger = 1 + cur.right.cnt if cur.right else 1
            if k == larger:
                break
            elif k > larger:
                cur = cur.left
                k -= larger
            elif k < larger:
                cur = cur.right
        return cur.val

    def add(self, val: int) -> int:
        self.root = self.insert(self.root, val)
        return self.find_kth(self.root)


# Your KthLargest object will be instantiated and called as such:
# obj = KthLargest(k, nums)
# param_1 = obj.add(val)


kth_largest = KthLargest(3, [4, 5, 8, 2])
assert kth_largest.add(3) == 4
assert kth_largest.add(5) == 5
assert kth_largest.add(10) == 5
assert kth_largest.add(9) == 8
assert kth_largest.add(4) == 8


kth_largest = KthLargest(2, [0])
assert kth_largest.add(-1) == -1
assert kth_largest.add(1) == 0
assert kth_largest.add(-2) == 0
assert kth_largest.add(-4) == 0
assert kth_largest.add(3) == 1


# import bisect
#
#
# class KthLargest:
#     """
#     Binary Search + Insertion
#     Time: O(n*log(n))
#     Space: O(N)
#     """
#     def __init__(self, k: int, nums: List[int]):
#         self.k = k
#         self.nums = sorted(nums)
#
#     def add(self, val: int) -> int:
#         bisect.insort_left(self.nums, val)
#         return self.nums[len(self.nums) - self.k]


# import heapq
#
#
# class KthLargest:
#     """
#     Time: O(n - k*log(k))
#     Space: O(N)
#     """
#     def __init__(self, k: int, nums: List[int]):
#         self.k = k
#         self.nums = nums
#         heapq.heapify(self.nums)
#
#     def add(self, val: int) -> int:
#         heapq.heappush(self.nums, val)
#         while len(self.nums) > self.k:
#             heapq.heappop(self.nums)
#         return self.nums[0]
