
from typing import List


class TreeNode:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"


class BinarySearchTree:

    def __init__(self, values: List[int] = None, sort: bool = True):

        if not values:
            values = []

        def build(left, right):
            if left > right:
                return
            mid = left + (right - left) // 2
            node = TreeNode(values[mid])
            node.left = build(left, mid - 1)
            node.right = build(mid + 1, right)
            return node

        if sort:
            values.sort()
        self.root = build(0, len(values) - 1)
        self.size = 0

    def search(self, value: int) -> TreeNode:

        def find(node) -> TreeNode:
            while node and value != node.val:
                node = node.left if value < node.val else node.right
            return node

        return find(self.root)

    def insert(self, value: int) -> None:

        self.size += 1

        def add(node) -> TreeNode:
            if not node:
                return TreeNode(value)
            if node.val < value:
                node.right = add(node.right)
            else:
                node.left = add(node.left)
            return node

        self.root = add(self.root)

    def delete(self, value: int):  # iterative approach that moves the node, not the attribute

        def delete_node(node: TreeNode):
            if not node:
                return None
            if not node.left:
                return node.right
            if not node.right:
                return node.left
            n = node.right
            while n.left:
                n = n.left
            n.left = node.left
            return node.right

        node, parent = self.root, None
        while node and node.val != value:
            parent, node = node, node.left if node.val > value else node.right

        if node:
            if not parent:  # node is self.root
                self.root = delete_node(node)
            elif parent.left == node:
                parent.left = delete_node(node)
            else:
                parent.right = delete_node(node)
        return node

    # def predecessor(self, value: int):
    #
    #     def traverse(node):
    #         if not node:
    #             return
    #         if node.val == value:
    #             return node.val
    #         elif node.val > value:
    #             return traverse(node.left)
    #         else:
    #             right = traverse(node.right)
    #             return right if right else node.val
    #
    #     return traverse(self.root)
    #
    # def successor(self, value: int):
    #
    #     def traverse(node):
    #         if not node:
    #             return
    #         if node.val == value:
    #             return node.val
    #         elif node.val < value:
    #             return traverse(node.right)
    #         else:
    #             left = traverse(node.left)
    #             return left if left else node.val
    #
    #     return traverse(self.root)

    def balance(self):  # rebuild in-place

        def traverse(node: TreeNode):
            if node:
                traverse(node.left)
                array.append(node)
                traverse(node.right)

        def build(left, right):
            if left > right:
                return
            mid = left + (right - left) // 2
            node = array[mid]
            node.left = build(left, mid - 1)
            node.right = build(mid + 1, right)
            return node

        array = []
        traverse(self.root)
        self.root = build(0, len(array) - 1)

    @property
    def is_valid(self):

        def traverse(root, low, high):
            if not root:
                return True
            elif not low < root.val < high:
                return False
            return traverse(root.left, low, root.val) and traverse(root.right, root.val, high)

        return traverse(self.root, float('-inf'), float('inf'))

    def __iter__(self):

        def traverse(node: TreeNode):
            if node:
                yield from traverse(node.left)
                yield node.val
                yield from traverse(node.right)

        return traverse(self.root)


def test():
    values = list(range(5))
    bst = BinarySearchTree(values)
    assert list(bst) == values

    for value in range(5, 10):
        bst.insert(value)

    assert bst.search(2) is bst.root
    bst.balance()
    root = bst.root
    assert bst.search(4) is bst.root
    assert bst.delete(4) is root

    assert bst.search(7) is bst.root
    assert bst.search(7) is not root

    bst.delete(3)
    bst.delete(9)
    # assert
    print(list(bst))
    # assert list(bst) == [0, 1, 4, 6, 7, 8, 9]
