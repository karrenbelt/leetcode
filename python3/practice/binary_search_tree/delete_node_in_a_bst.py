
# Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.
#
# Basically, the deletion can be divided into two stages:
#
# Search for a node to remove.
# If the node is found, delete the node.
# Follow up: Can you solve it with time complexity O(height of tree)?
#
#
#
# Example 1:
#
#
# Input: root = [5,3,6,2,4,null,7], key = 3
# Output: [5,4,6,2,null,null,7]
# Explanation: Given key to delete is 3. So we find the node with value 3 and delete it.
# One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
# Please notice that another valid answer is [5,2,6,null,4,null,7] and it's also accepted.
#
# Example 2:
#
# Input: root = [5,3,6,2,4,null,7], key = 0
# Output: [5,3,6,2,4,null,7]
# Explanation: The tree does not contain a node with value = 0.
# Example 3:
#
# Input: root = [], key = 0
# Output: []
#
#
# Constraints:
#
# The number of nodes in the tree is in the range [0, 104].
# -105 <= Node.val <= 105
# Each node has a unique value.
# root is a valid binary search tree.
# -105 <= key <= 105

from python3.practice.binary_search_tree import TreeNode


class Solution:
    def deleteNode(self, root: TreeNode, key: int) -> TreeNode:

        def del_node(node: TreeNode):
            if not node:
                return None
            if not node.left:
                return node.right
            if not node.right:
                return node.left

            n = node.right
            while n.left:
                n = n.left
            n.left = node.left
            return node.right

        # find the node
        node, prev = root, None
        while node:
            if node.val == key:
                break
            prev = node
            node = node.left if node.val > key else node.right

        # delete the node
        if node:
            if not prev:
                return del_node(node)
            if prev.left == node:
                prev.left = del_node(node)
            else:
                prev.right = del_node(node)
        return root

    def deleteNode(self, root: TreeNode, key: int) -> TreeNode:
        if not root:
            return root
        elif root.val > key:
            root.left = self.deleteNode(root.right, key)
        elif root.val < key:
            root.right = self.deleteNode(root.left, key)
        else:
            if not root.left:
                return root.right
            elif not root.right:
                return root.left
            node = root.right
            while node.left:
                node = node.left
            root.val = node.val
            root.right = self.deleteNode(root.right, node.val)
        return root


root = TreeNode(5,
                TreeNode(3, TreeNode(1, TreeNode(0), TreeNode(2)), TreeNode(4)),
                TreeNode(8, TreeNode(6), TreeNode(9))
                )

sol = Solution().deleteNode(root, 1)


