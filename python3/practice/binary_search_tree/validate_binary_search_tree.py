
# Given the root of a binary tree, determine if it is a valid binary search tree (BST).
#
# A valid BST is defined as follows:
#
# The left subtree of a node contains only nodes with keys less than the node's key.
# The right subtree of a node contains only nodes with keys greater than the node's key.
# Both the left and right subtrees must also be binary search trees.
#
#
# Example 1:
#
#
# Input: root = [2,1,3]
# Output: true
# Example 2:
#
#
# Input: root = [5,1,4,null,null,3,6]
# Output: false
# Explanation: The root node's value is 5 but its right child's value is 4.
#
#
# Constraints:
#
# The number of nodes in the tree is in the range [1, 104].
# -231 <= Node.val <= 231 - 1

from python3.practice.binary_search_tree import TreeNode


class Solution:

    def isValidBST(self, root: TreeNode, left=float('-inf'), right=float('inf')) -> bool:
        if not root:
            return True
        if root.val <= left or root.val >= right:
            return False
        return self.isValidBST(root.left, left, root.val) and self.isValidBST(root.right, root.val, right)

    def isValidBST(self, root: TreeNode, left=float('-inf'), right=float('inf')) -> bool:
        return not root or left < root.val < right and \
               self.isValidBST(root.left, left, root.val) and self.isValidBST(root.right, root.val, right)

    def isValidBST(self, root: TreeNode) -> bool:  # iterative in-order traverszl

        node = root
        value = float('-inf')
        stack = []
        while node or stack:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            if node.val <= value:
                return False
            value = node.val
            node = node.right
        return True


# root = TreeNode(5,
#                 TreeNode(4),
#                 TreeNode(6, TreeNode(3), TreeNode(7)))
# sol = Solution().isValidBST(root)
# print(sol)
