
# DONE (non-premium part only):
- [array_and_string](array_and_string)
- [arrays_101](arrays_101)
- [binary_search](binary_search)
- [binary_tree](binary_tree)
- [hash_table](hash_table)
- [linked_list](linked_list)
- [n_ary_tree](n_ary_tree)
- [queue_and_stack](queue_and_stack)
- [recursion_I](recursion_I)
- [recursion_II](recursion_II)

# TODO:
- [binary_search_tree](binary_search_tree) (3 left)
- decision_tree (not started)
- machine_learning (not started)
- [trie](trie) (1 left)


I came to realize that these practice (explore) exercises are part of the general problems. 
I keep solutions in both folders, which can be different from each other.

Additionally, I found this:
https://github.com/LeetCode-OpenSource/vscode-leetcode
(uses https://github.com/skygragon/leetcode-cli underneath)

