
# Design a data structure that supports adding new words and finding if a string matches any previously added string.
#
# Implement the WordDictionary class:
#
# WordDictionary() Initializes the object.
# void addWord(word) Adds word to the data structure, it can be matched later.
# bool search(word) Returns true if there is any string in the data structure that matches word or false otherwise. word may contain dots '.' where dots can be matched with any letter.
#
#
# Example:
#
# Input
# ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
# [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
# Output
# [null,null,null,null,false,true,true,true]
#
# Explanation
# WordDictionary wordDictionary = new WordDictionary();
# wordDictionary.addWord("bad");
# wordDictionary.addWord("dad");
# wordDictionary.addWord("mad");
# wordDictionary.search("pad"); // return False
# wordDictionary.search("bad"); // return True
# wordDictionary.search(".ad"); // return True
# wordDictionary.search("b.."); // return True
#
#
# Constraints:
#
# 1 <= word.length <= 500
# word in addWord consists lower-case English letters.
# word in search consist of  '.' or lower-case English letters.
# At most 50000 calls will be made to addWord and search.


class WordDictionary:

    def __init__(self):
        self.trie = {}

    def addWord(self, word: str) -> None:
        node = self.trie
        for c in word:
            if c not in node:
                node[c] = dict()
            node = node[c]
        node['.'] = None  # not super clear since this is wild card in search now, but w/e

    def search(self, word: str) -> bool:

        def dfs(node, word):
            nonlocal is_a_word
            if node is None:
                return
            if not word:
                if '.' in node:
                    is_a_word = True
            elif word[0] == ".":
                any((dfs(n, word[1:]) for n in node.values()))
            else:
                dfs(node.get(word[0]), word[1:])

        is_a_word = False
        dfs(self.trie, word)
        return is_a_word


# bits
# bottle
# b.t

# Your WordDictionary object will be instantiated and called as such:
# obj = WordDictionary()
# obj.addWord(word)
# param_2 = obj.search(word)

obj = WordDictionary()
obj.addWord('bits')
obj.addWord('bottle')

assert obj.search('b.t') is False
assert obj.search('b.ts') is True
assert obj.search('b.ttle') is True
assert obj.search('bottles') is False

assert obj.search('..t') is False
assert obj.search('..ts') is True
assert obj.search('....le') is True

assert obj.search('.....e') is True
assert obj.search('...t..') is True


assert obj.search('..') is False
assert obj.search('......') is True
assert obj.search('..........') is False

obj.addWord('aa')
assert obj.search('a.') is True



def test():
    # methods = ["WordDictionary", "addWord", "addWord", "addWord", "addWord", "addWord", "addWord", "addWord", "addWord", "search",
    #  "search", "search", "search", "search", "search", "search", "search", "search", "search"]
    # args = [[], ["ran"], ["rune"], ["runner"], ["runs"], ["add"], ["adds"], ["adder"], ["addee"], ["r.n"], ["ru.n.e"], ["add"],
    #  ["add."], ["adde."], [".an."], ["...s"], ["....e."], ["......."], ["..n.r"]]
    # expected = [null,null,null,null,null,null,null,null,null,true,false,true,true,true,false,true,true,false,false]

    methods = ["WordDictionary","addWord","addWord","search","search","search","search","search","search"]
    args = [[],["a"],["a"],["."],["a"],["aa"],["a"],[".a"],["a."]]
    # expected = [null,null,null,true,true,false,true,false,false]

    methods = ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
    args = [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
    # expected [null,null,null,null,null,null,null,null,null,true,false,true,true,true,false,true,true,false,false]

    out = []
    self = WordDictionary()
    for method, arg in zip(methods[1:], args[1:]):
        # print(method, arg)
        res = getattr(self, method)(arg[0])
        out.append(res)

    self.search('.ad')

