
# Given an integer array nums, return the maximum result of nums[i] XOR nums[j], where 0 ≤ i ≤ j < n.
#
# Follow up: Could you do this in O(n) runtime?
#
#
#
# Example 1:
#
# Input: nums = [3,10,5,25,2,8]
# Output: 28
# Explanation: The maximum result is 5 XOR 25 = 28.
# Example 2:
#
# Input: nums = [0]
# Output: 0
# Example 3:
#
# Input: nums = [2,4]
# Output: 6
# Example 4:
#
# Input: nums = [8,10,2]
# Output: 10
# Example 5:
#
# Input: nums = [14,70,53,83,49,91,36,80,92,51,66,70]
# Output: 127
#
#
# Constraints:
#
# 1 <= nums.length <= 2 * 104
# 0 <= nums[i] <= 231 - 1

from typing import List


class TrieNode:
    def __init__(self):
        self.children = {}
        self.val = 0


class Trie:
    def __init__(self, bit_length):
        self.root = TrieNode()
        self.bit_length = bit_length

    def add_num(self, num):  # building the trie
        node = self.root
        for shift in range(self.bit_length, -1, -1):
            val = 1 if num & (1 << shift) else 0
            if val not in node.children:
                node.children[val] = TrieNode()
            node = node.children[val]
        node.val = num


class Solution:  # TODO: I did not solve any of these myself apart from the linear solution

    def findMaximumXOR(self, nums: List[int]) -> int:  # TLE: O(n^2)
        value = 0
        for i in range(len(nums)):
            for j in range(i+1, len(nums)):
                xor = nums[i] ^ nums[j]
                if xor > value:
                    value = xor
        return value

    def findMaximumXOR(self, nums: List[int]) -> int:   # bit-by-bit: O(n)
        ans = 0
        for i in reversed(range(32)):
            prefixes = {x >> i for x in nums}
            ans <<= 1
            candidate = ans + 1  # equivalent to ans^1 in OP
            if any(candidate ^ p in prefixes for p in prefixes):
                ans = candidate
        return ans

    def findMaximumXOR(self, nums: List[int]) -> int:
        max_len = len(bin(max(nums))) - 2
        trie = Trie(max_len)
        for num in nums:
            trie.add_num(num)

        ans = 0
        for num in nums:
            node = trie.root
            for shift in range(max_len, -1, -1):
                val = 1 if num & (1 << shift) else 0
                node = node.children[1 ^ val] if 1 ^ val in node.children else node.children[val]
            ans = max(ans, num ^ node.val)
        return ans


nums = [3, 10, 5, 25, 2, 8]
sol = Solution().findMaximumXOR(nums)
print(sol)

