# Given an m x n board of characters and a list of strings words, return all words on the board.
#
# Each word must be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once in a word.
#
#
#
# Example 1:
#
#
# Input: board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]], words = ["oath","pea","eat","rain"]
# Output: ["eat","oath"]
# Example 2:
#
#
# Input: board = [["a","b"],["c","d"]], words = ["abcb"]
# Output: []
#
#
# Constraints:
#
# m == board.length
# n == board[i].length
# 1 <= m, n <= 12
# board[i][j] is a lowercase English letter.
# 1 <= words.length <= 3 * 104
# 1 <= words[i].length <= 10
# words[i] consists of lowercase English letters.
# All the strings of words are unique.

from typing import List


class Solution:
    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:
        # any on list comprehension -> TLE (whereas nested for loop slow but ok)
        trie, n, m, found = {}, len(board), len(board[0]), []

        for word in words:  # build trie
            node = trie
            for c in word:
                if c not in node:
                    node[c] = {}
                node = node[c]
            node['.'] = None

        def is_valid(i, j) -> bool:
            return 0 <= i < n and 0 <= j < m

        def dfs(i, j, node, prefix=''):  # backtracking
            if '.' in node and prefix not in found:
                found.append(prefix)
            if not is_valid(i, j):
                return
            c = board[i][j]
            if not used[i][j] and c in node:  # if 'c' not in node, we prune the tree
                used[i][j] = True  # initial mistake: [(1, 1), (1, -1),  (-1, 1) ,  (-1, -1)]
                any([dfs(i + x, j + y, node[c], prefix+c) for x, y in [(0, 1), (0, -1), (1, 0), (-1, 0)]])
                used[i][j] = False

        used = [[False] * m for _ in range(n)]
        any([dfs(i, j, trie) for i in range(n) for j in range(m)])
        return found

    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:
        # using sets, as well as if, elif, elif for early return in dfs, still slow
        trie, n, m, found = {}, len(board), len(board[0]), set()

        for word in words:
            node = trie
            for c in word:
                if c not in node:
                    node[c] = {}
                node = node[c]
            node['.'] = None

        def is_valid(i, j) -> bool:
            return 0 <= i < n and 0 <= j < m

        def dfs(i, j, node, prefix=''):  # backtracking
            if '.' in node:
                found.add(prefix)
            elif (i, j) in seen or not is_valid(i, j):
                return
            elif board[i][j] in node:  # if 'c' not in node, we prune the tree
                c = board[i][j]
                seen.add((i, j))
                any([dfs(i + x, j + y, node[c], prefix+c) for x, y in [(0, 1), (0, -1), (1, 0), (-1, 0)]])
                seen.pop()

        seen = set()
        any([dfs(i, j, trie) for i in range(n) for j in range(m)])
        return list(found)


board = [
    ["o", "a", "a", "n"],
    ["e", "t", "a", "e"],
    ["i", "h", "k", "r"],
    ["i", "f", "l", "v"],
]
words = ["oath", "pea", "eat", "rain"]  # expected = ['oath', 'eat']

# board = [
#     ["a", "b"],
#     ["c", "d"],
# ]
# words = ["abcb"]  # expected = []

sol = Solution().findWords(board, words)
print(sol)

