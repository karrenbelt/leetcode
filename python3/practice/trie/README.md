
# Binary Search

Identification and template introduction

1. pre-processing - sort if unsorted 
2. binary search - using a loop or recursion to divide the search space in half after each comparison
3. post-processing - determine viable candidates for the next iteration

## Binary search 101

### Fundamentals
1. boundary <br>
   left & right (or low & high); two pointers storing the indices of boundary elements within which we might find our target
    ```python: 
    left, right = 0, len(array) - 1
    ```
   
2. middle <br>
   the middle element within the boundary, separating the space into two halves. When an array has an even number of elements, 
   we can pick either the lower middle or higher middle index - by default I pick the lower middle.
    ```python: 
    mid = left + (right - left) // 2
    ```
   
3. comparing target the middle <br>
   we compare if our target is smaller than the middle element, it means we want to look in the lower half and hence shrink the right boundary, and vice versa.
   ```python
   if array[mid] == target:
       return target
   elif array[mid] < target:
       left = mid + 1
   else:
       right = mid + 1 
   ```
   
4. keep the loop going
   the following while loop only exits when left == right
   ```python
   while left < right:
       pass
   ```
    
### The pattern

1. The boundary: choosing ```left``` and ```right```
   

## Binary search template

### Introduction

Binary Search is quite easy to understand conceptually. Basically, it splits the search space into two halves and only keep the half that probably has the search target and throw away the other half that would not possibly have the answer. 
In this manner, we reduce the search space to half the size at every step, until we find the target. Binary Search helps us reduce the search time from linear O(n) to logarithmic O(log n). 
But when it comes to implementation, it's rather difficult to write a bug-free code in just a few minutes. Some of the most common problems include:

- When to exit the loop? Should we use left < right or left <= right as the while loop condition?
- How to initialize the boundary variable left and right?
- How to update the boundary? How to choose the appropriate combination from left = mid, left = mid + 1 and right = mid, right = mid - 1?

A rather common misunderstanding of binary search is that people often think this technique could only be used in simple scenario like "Given a sorted array, find a specific value in it". As a matter of fact, it can be applied to much more complicated situations.

## Most Generalized Binary Search
Suppose we have a search space. It could be an array, a range, etc. Usually it's sorted in ascend order. For most tasks, we can transform the requirement into the following generalized form:

<b>Minimize k , s.t. condition(k) is True</b>

The following code is the most generalized binary search template:

```python:
def binary_search(array) -> int:
    
    def condition(value) -> bool:
        pass

    left, right = 0, len(array)
    while left < right:
        mid = left + (right - left) // 2
        if condition(mid):
            right = mid
        else:
            left = mid + 1
    return left
```

What's really nice of this template is that, for most of the binary search problems, we only need to modify three parts after copy-pasting this template, and never need to worry about corner cases and bugs in code any more:

- Correctly initialize the boundary variables left and right. Only one rule: set up the boundary to include all possible elements;
- Decide return value. Is it return left or return left - 1? Remember this: after exiting the while loop, left is the minimal k​ satisfying the condition function;
- Design the condition function. This is the most difficult and most beautiful part. Needs lots of practice.

## Most common problems

- Infinity loop
- Can't decide where to shrink
- Do I use lo or hi
- When to exit the loop