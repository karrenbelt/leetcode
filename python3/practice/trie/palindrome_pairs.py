# Given a list of unique words, return all the pairs of the distinct indices (i, j) in the given list, so that the concatenation of the two words words[i] + words[j] is a palindrome.
#
#
#
# Example 1:
#
# Input: words = ["abcd","dcba","lls","s","sssll"]
# Output: [[0,1],[1,0],[3,2],[2,4]]
# Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
# Example 2:
#
# Input: words = ["bat","tab","cat"]
# Output: [[0,1],[1,0]]
# Explanation: The palindromes are ["battab","tabbat"]
# Example 3:
#
# Input: words = ["a",""]
# Output: [[0,1],[1,0]]
#
#
# Constraints:
#
# 1 <= words.length <= 5000
# 0 <= words[i].length <= 300
# words[i] consists of lower-case English letters.

from typing import List


class Solution:
    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # TLE
        res = []
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                for x, y in [(i, j), (j, i)]:
                    new = words[x] + words[y]
                    if len(new) == 1:
                        res.append([x, y])
                    elif new[:len(new) // 2] == new[-(len(new) // 2):][::-1]:
                        res.append([x, y])
        return res

    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # TLE as well
        res = []
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                for x, y in [(i, j), (j, i)]:
                    new = words[x] + words[y]
                    for k in range(len(new)):
                        if new[k] != new[-k-1]:
                            break
                    else:
                        res.append([x, y])
        return res

    def palindromePairs(self, words: List[str]) -> List[List[int]]:

        trie = {}  # build the trie
        for i, word in enumerate(words):
            node = trie
            for c in word:
                if c not in node:
                    node[c] = {}
                node = node[c]
            node['.'] = i

        for i, word in enumerate(words):
            node = trie
            for c in word:
                if c not in node:
                    break  # next node
                # node =

        return


words = ["abcd", "dcba", "lls", "s", "sssll"]
# expected = [[0, 1], [1, 0], [3, 2], [2, 4]]

words = ["bat", "tab", "cat"]
expected = [[0, 1], [1, 0]]

# words = ["a", ""]
# expected = [[0, 1], [1, 0]]

# words = ["a", "abc", "aba", ""]
# expected = [[0, 3], [3, 0], [2, 3], [3, 2]]

sol = Solution().palindromePairs(words)
print(sol)
