
# Implement the MapSum class:
#
# MapSum() Initializes the MapSum object.
# void insert(String key, int val) Inserts the key-val pair into the map. If the key already existed, the original key-value pair will be overridden to the new one.
# int sum(string prefix) Returns the sum of all the pairs' value whose key starts with the prefix.
#
#
# Example 1:
#
# Input
# ["MapSum", "insert", "sum", "insert", "sum"]
# [[], ["apple", 3], ["ap"], ["app", 2], ["ap"]]
# Output
# [null, null, 3, null, 5]
#
# Explanation
# MapSum mapSum = new MapSum();
# mapSum.insert("apple", 3);
# mapSum.sum("ap");           // return 3 (apple = 3)
# mapSum.insert("app", 2);
# mapSum.sum("ap");           // return 5 (apple + app = 3 + 2 = 5)
#
#
# Constraints:
#
# 1 <= key.length, prefix.length <= 50
# key and prefix consist of only lowercase English letters.
# 1 <= val <= 1000
# At most 50 calls will be made to insert and sum.


class MapSum:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = {}

    def insert(self, key: str, val: int) -> None:
        node = self.head
        for c in key:
            if c not in node:
                node[c] = dict()
            node = node[c]
        node['.'] = val

    def sum(self, prefix: str) -> int:  # stack

        node = self.head
        for c in prefix:
            if c not in node:
                return 0
            node = node[c]

        total = 0
        stack = [node]
        while stack:
            node = stack.pop()
            if isinstance(node, int):
                total += node
            else:
                stack.extend(node.values())
        return total

    def sum(self, prefix: str) -> int:  # dfs
        node = self.head
        for c in prefix:
            if c not in node:
                return 0
            node = node[c]

        total = 0

        def dfs(node):
            nonlocal total
            if isinstance(node, int):
                total += node
                return
            for c in node.values():
                dfs(c)

        dfs(node)
        return total


# Your MapSum object will be instantiated and called as such:
# obj = MapSum()
# obj.insert(key,val)
# param_2 = obj.sum(prefix)


obj = MapSum()
obj.insert("apple", 3)
print(obj.sum('ap'))
obj.insert("app", 2)
print(obj.sum('ap'))
print(obj.sum('appp'))
