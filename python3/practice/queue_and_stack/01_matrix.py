
# Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
#
# The distance between two adjacent cells is 1.
#
#
#
# Example 1:
#
# Input:
# [[0,0,0],
#  [0,1,0],
#  [0,0,0]]
#
# Output:
# [[0,0,0],
#  [0,1,0],
#  [0,0,0]]
# Example 2:
#
# Input:
# [[0,0,0],
#  [0,1,0],
#  [1,1,1]]
#
# Output:
# [[0,0,0],
#  [0,1,0],
#  [1,2,1]]
#
#
# Note:
#
# The number of elements of the given matrix will not exceed 10,000.
# There are at least one 0 in the given matrix.
# The cells are adjacent in only four directions: up, down, left and right.

from typing import List


class Solution:
    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:

        if not matrix or not matrix[0]:
            return matrix

        M, N = len(matrix), len(matrix[0])
        directions = [[-1, 0], [1, 0], [0, 1], [0, -1]]

        def is_valid_index(i, j):
            return 0 <= i < M and 0 <= j < N

        def dp():
            for i in range(M):
                for j in range(N):
                    if matrix[i][j] != 0:
                        left = matrix[i][j - 1] if j > 0 else float('inf')
                        up = matrix[i - 1][j] if i > 0 else float('inf')
                        matrix[i][j] = 1 + min(left, up)

            for i in reversed(range(M)):
                for j in reversed(range(N)):
                    if matrix[i][j] != 0:
                        right = matrix[i][j + 1] if j < N - 1 else float('inf')
                        down = matrix[i + 1][j] if i < M - 1 else float('inf')
                        matrix[i][j] = min(matrix[i][j], 1 + right, 1 + down)

        def bfs():
            queue = []
            for i in range(len(matrix)):
                for j in range(len(matrix[0])):
                    if matrix[i][j] == 1:
                        matrix[i][j] = float('inf')
                    elif matrix[i][j] == 0:
                        queue.append((i, j))

            while queue:
                r, c = queue.pop(0)
                for x, y in directions:
                    row = r + x
                    col = c + y
                    if is_valid_index(row, col) and matrix[r][c] + 1 < matrix[row][col]:
                        matrix[row][col] = matrix[r][c] + 1
                        queue.append((row, col))

        def dfs():

            def solve(i, j, seen):
                if is_valid_index(i, j) and (i, j) not in seen:
                    seen.add((i, j))
                    if matrix[i][j] == 0:
                        tbr = 0
                    else:
                        tbr = min([solve(i+x, j+y, seen)+1 for x, y in directions])
                    seen.remove((i, j))
                    return tbr
                return float("inf")

            for i in range(M):
                for j in range(N):
                    matrix[i][j] = solve(i, j, set())

        dp()
        # bfs()
        # dfs()

        return matrix


matrix = [[0, 1, 0],
          [0, 1, 0],
          [0, 1, 0],
          [0, 1, 0],
          [0, 1, 0]]
matrix = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]
sol = Solution().updateMatrix(matrix)
print(sol)
