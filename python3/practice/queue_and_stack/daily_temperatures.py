
# Given a list of daily temperatures T, return a list such that, for each day in the input, tells you how many days you would have to wait until a warmer temperature. If there is no future day for which this is possible, put 0 instead.
#
# For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73], your output should be [1, 1, 4, 2, 1, 1, 0, 0].
#
# Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the range [30, 100].

from typing import List


class Solution:
    def dailyTemperatures(self, T: List[int]) -> List[int]:
        # works but too slow: time-limit exceeded
        # result = [0] * len(T)
        # for i in range(len(T)):
        #     for j in range(i+1, len(T)):
        #         if T[j] > T[i]:
        #             result[i] = j - i
        #             break
        # return result

        # using a stack: stack will hold (temperature, index) pairs
        # for each iteration, check if T[i] > stack[-1][0], if so store the index difference
        # time complexity is O(n)
        stack = []
        result = [0] * len(T)
        for i, t in enumerate(T):
            while stack and stack[-1][0] < t:  # next highest found
                v, j = stack.pop()
                result[j] = i - j
            stack.append((t, i))
        return result


T = [73, 74, 75, 71, 69, 72, 76, 73]
#   [1, 1, 4, 2, 1, 1, 0, 0]
sol = Solution().dailyTemperatures(T)
print(sol)
