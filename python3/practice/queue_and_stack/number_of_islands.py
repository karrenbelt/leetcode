
### Source : https://leetcode.com/problems/number-of-islands/
### Author : karrenbelt
### Date   : 2019-04-22

#####################################################################################################
#
# Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is
# surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may
# assume all four edges of the grid are all surrounded by water.
#
# Example 1:
#
# Input:
# 11110
# 11010
# 11000
# 00000
#
# Output: 1
#
# Example 2:
#
# Input:
# 11000
# 11000
# 00100
# 00011
#
# Output: 3
#####################################################################################################
from typing import List
from queue import Queue
import collections


class Solution:

    def numIslands(self, grid: List[List[str]]) -> int:

        if not grid or not grid[0]:
            return 0

        M, N = len(grid), len(grid[0])
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        def isValidIndex(i, j):
            return 0 <= i < M and 0 <= j < N

        def dfs(i, j):
            grid[i][j] = '0'
            for dr, dc in directions:
                nr = i + dr
                nc = j + dc
                if isValidIndex(nr, nc) and grid[nr][nc] == "1":
                    dfs(nr, nc)

        def bfs(i, j):
            grid[i][j] = '0'
            q = collections.deque([(i, j), ])
            while q:
                r, c = q.popleft()
                for dr, dc in directions:
                    nr = r + dr
                    nc = c + dc
                    if isValidIndex(nr, nc) and grid[nr][nc] == '1':
                        grid[nr][nc] = '0'
                        q.append((nr, nc))

        count = 0
        for i in range(M):
            for j in range(N):
                if grid[i][j] == "1":
                    count += 1
                    # choose the method
                    # dfs(i, j)
                    bfs(i, j)

        return count
