
# Given a positive integer n, find the least number of perfect square numbers (for example, 1, 4, 9, 16, ...) which sum to n.
#
# Example 1:
#
# Input: n = 12
# Output: 3
# Explanation: 12 = 4 + 4 + 4.
# Example 2:
#
# Input: n = 13
# Output: 2
# Explanation: 13 = 4 + 9.

import math
import collections


def is_square(n: int):
    sq = int(math.sqrt(n))
    return sq * sq == n


class Solution:
    def numSquares(self, n: int) -> int:
        # implementation using
        if n < 2:
            return n
        l = []
        i = 1
        # find all squares that can potentially be used first
        while i * i <= n:
            l.append(i * i)
            i += 1
        #
        ctr = 0
        root = {n}
        while root:
            ctr += 1
            tmp = set()
            for x in root:
                for y in l:
                    if x == y:
                        return ctr
                    if x < y:
                        break
                    tmp.add(x-y)
            root = tmp
        return ctr

    # def numSquares(self, n: int) -> int:
    #     # implementation using math
    #     num = [0]
    #     while len(num) <= n:
    #         num += min(num[-i*i] for i in range(1, int(len(num)**0.5+1))) + 1,
    #     return num[n]

    # def numSquares(self, n: int) -> int:
    #     # implementation using Lagrange's four-square and Legendre's three-square theorems
    #     while (n & 3) == 0:
    #         n >>= 2  # reducing the 4^k factor from number
    #     if (n & 7) == 7:  # mod 8
    #         return 4
    #
    #     if is_square(n):
    #         return 1
    #     # check if the number can be decomposed into sum of two squares
    #     for i in range(1, int(n ** 0.5) + 1):
    #         if is_square(n - i * i):
    #             return 2
    #     # bottom case from the three-square theorem
    #     return 3

    def numSquares(self, n: int) -> int:
        # implementation using a queue
        from collections import deque
        squares = [i * i for i in range(1, int(n**0.5)+1)]
        queue = deque([n])
        visited = set()
        steps = 0
        while queue:
            for _ in range(len(queue)):
                val = queue.popleft()
                if val == 0:
                    return steps
                for sq in squares:
                    diff = val - sq
                    if diff == 0:
                        return steps + 1
                    if diff < 0:
                        break
                    if diff in visited:
                        continue
                    visited.add(diff)
                    queue.append(diff)
            steps += 1
        return -1


n = 12
sol = Solution().numSquares(n)
print(sol)
