

# Given a reference of a node in a connected undirected graph.
#
# Return a deep copy (clone) of the graph.
#
# Each node in the graph contains a val (int) and a list (List[Node]) of its neighbors.
#
# class Node {
#     public int val;
#     public List<Node> neighbors;
# }
#
#
# Test case format:
#
# For simplicity sake, each node's value is the same as the node's index (1-indexed). For example, the first node with val = 1, the second node with val = 2, and so on. The graph is represented in the test case using an adjacency list.
#
# Adjacency list is a collection of unordered lists used to represent a finite graph. Each list describes the set of neighbors of a node in the graph.
#
# The given node will always be the first node with val = 1. You must return the copy of the given node as a reference to the cloned graph.
#
# Example 1:
# Input: adjList = [[2,4],[1,3],[2,4],[1,3]]
# Output: [[2,4],[1,3],[2,4],[1,3]]
# Explanation: There are 4 nodes in the graph.
# 1st node (val = 1)'s neighbors are 2nd node (val = 2) and 4th node (val = 4).
# 2nd node (val = 2)'s neighbors are 1st node (val = 1) and 3rd node (val = 3).
# 3rd node (val = 3)'s neighbors are 2nd node (val = 2) and 4th node (val = 4).
# 4th node (val = 4)'s neighbors are 1st node (val = 1) and 3rd node (val = 3).
#
# Example 2:
# Input: adjList = [[]]
# Output: [[]]
# Explanation: Note that the input contains one empty list. The graph consists of only one node with val = 1 and it does not have any neighbors.
#
# Example 3:
# Input: adjList = []
# Output: []
# Explanation: This an empty graph, it does not have any nodes.
#
# Example 4:
# Input: adjList = [[2],[1]]
# Output: [[2],[1]]
#
# Constraints:
#
# 1 <= Node.val <= 100
# Node.val is unique for each node.
# Number of Nodes will not exceed 100.
# There is no repeated edges and no self-loops in the graph.
# The Graph is connected and all nodes can be visited starting from the given node.

# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, neighbors=None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

    # def __eq__(self, other):  # circular: recursion depth error
    #     if not isinstance(other, type(self)):
    #         return NotImplemented
    #     return self.val == other.val and all([a == b for a, b in zip(self.neighbors, other.neighbors)])


class Solution:

    # # this solution works for the test case, but yields a Runtime Error on leetcode
    # def cloneGraph(self, node: 'Node') -> 'Node':
    #     return self.clone(node, dict())
    #
    # def clone(self, node, seen) -> 'Node':
    #     if node in seen:
    #         return seen[node]
    #     root = Node(node.val)
    #     seen[node] = root
    #     for neighbor in node.neighbors:
    #         root.neighbors.append(self.clone(neighbor, seen))
    #     return root

    def cloneGraph(self, node: 'Node') -> 'Node':
        memo = {}

        def clone(node):
            if not node:
                return node
            if node in memo:
                return memo[node]
            memo[node] = Node(node.val)
            for neighbor in node.neighbors:
                memo[node].neighbors.append(clone(neighbor))
            return memo[node]

        return clone(node)

    def cloneGraph(self, node: 'Node') -> 'Node':
        seen = dict()
        if not node:
            return node
        return self.dfs(node, seen)

    def dfs(self, node, seen):
        if node in seen:
            return seen[node]
        new = Node(node.val)
        seen[node] = new
        for neighbor in node.neighbors:
            new.neighbors.append(self.dfs(neighbor, seen))
        return seen[node]


adjList = [[2, 4], [1, 3], [2, 4], [1, 3]]

nodes = [Node(i+1) for i in range(len(adjList))]
for i, neighbors in enumerate(adjList):
    nodes[i].neighbors = [nodes[j-1] for j in neighbors]


node = nodes[0]
sol = Solution().cloneGraph(node)
print(sol)


