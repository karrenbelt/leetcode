
# You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.
#
# Find out how many ways to assign symbols to make sum of integers equal to target S.
#
# Example 1:
#
# Input: nums is [1, 1, 1, 1, 1], S is 3.
# Output: 5
# Explanation:
#
# -1+1+1+1+1 = 3
# +1-1+1+1+1 = 3
# +1+1-1+1+1 = 3
# +1+1+1-1+1 = 3
# +1+1+1+1-1 = 3
#
# There are 5 ways to assign symbols to make the sum of nums be target 3.
#
#
# Constraints:
#
# The length of the given array is positive and will not exceed 20.
# The sum of elements in the given array will not exceed 1000.
# Your output answer is guaranteed to be fitted in a 32-bit integer.

from typing import List


class Solution:

    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        # need to use all numbers: dfs is suitable
        total = sum(S)
        if -S < total or S > total:
            return 0

        ctr = 0
        memo = {}

        def dfs(nums, i, cum_sum, S, ctr):  # this brute force recursive solution is too slow; time complexity O(2**N)
            if i == len(nums) and cum_sum == S:
                ctr += 1
            else:
                if (cum_sum, i) in memo:
                    return memo[(cum_sum, i)]
                dfs(nums, i + 1, cum_sum + nums[i], S)
                dfs(nums, i + 1, cum_sum - nums[i], S)

            return ctr

        return dfs(nums, 0, 0, S, ctr)

    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        memo = {}  # backtracking using memoization

        def backtracking(idx, s):
            if idx == len(nums) and s == S:
                return 1
            elif idx == len(nums):
                return 0
            key = (idx, s)
            if key not in memo:
                memo[key] = backtracking(idx+1, s+nums[idx]) + backtracking(idx+1, s-nums[idx])
            return memo[key]

        return backtracking(0, 0)

    def findTargetSumWays(self, nums, S):  # knapsack
        s = sum(nums) + S
        if s % 2 != 0 or abs(sum(nums)) < S:
            return 0
        else:
            target = s // 2
            dp = [0] * (target + 1)
            dp[0] = 1
            for num in nums:
                for j in range(target, num-1, -1):
                    dp[j] += dp[j-num]
        return dp[-1]

nums = [1, 1, 1, 1, 1]
S = 3

sol = Solution().findTargetSumWays(nums, S)
print(sol)

