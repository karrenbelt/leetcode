

# You have a lock in front of you with 4 circular wheels. Each wheel has 10 slots: '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'. The wheels can rotate freely and wrap around: for example we can turn '9' to be '0', or '0' to be '9'. Each move consists of turning one wheel one slot.
#
# The lock initially starts at '0000', a string representing the state of the 4 wheels.
#
# You are given a list of deadends dead ends, meaning if the lock displays any of these codes, the wheels of the lock will stop turning and you will be unable to open it.
#
# Given a target representing the value of the wheels that will unlock the lock, return the minimum total number of turns required to open the lock, or -1 if it is impossible.
#
#
#
# Example 1:
#
# Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
# Output: 6
# Explanation:
# A sequence of valid moves would be "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202".
# Note that a sequence like "0000" -> "0001" -> "0002" -> "0102" -> "0202" would be invalid,
# because the wheels of the lock become stuck after the display becomes the dead end "0102".
# Example 2:
#
# Input: deadends = ["8888"], target = "0009"
# Output: 1
# Explanation:
# We can turn the last wheel in reverse to move from "0000" -> "0009".
# Example 3:
#
# Input: deadends = ["8887","8889","8878","8898","8788","8988","7888","9888"], target = "8888"
# Output: -1
# Explanation:
# We can't reach the target without getting stuck.
# Example 4:
#
# Input: deadends = ["0000"], target = "8888"
# Output: -1
#
#
# Constraints:
#
# 1 <= deadends.length <= 500
# deadends[i].length == 4
# target.length == 4
# target will not be in the list deadends.
# target and deadends[i] consist of digits only.

from typing import List
import collections


class Solution:
    def openLock(self, deadends: List[str], target: str) -> int:
        root = "0000"
        queue = collections.deque()
        visited = set()

        step = -1
        dead_ends = set(deadends)
        queue.append(root)
        visited.add(root)

        if root in dead_ends:
            return -1

        while queue:
            step += 1

            for i in range(len(queue)):
                node = queue.popleft()
                if node == target:
                    return step

                # start rotating
                for j in range(len(node)):  # all positions
                    for k in (-1, 1):  # rotate one up / down
                        new_digit = str((int(node[j]) + k) % 10)
                        next_node = node[:j] + new_digit + node[j + 1:]
                        if next_node not in dead_ends and next_node not in visited:
                            queue.append(next_node)
                            visited.add(next_node)

        return -1


        return


deadends = ["0201", "0101", "0102", "1212", "2002"]
target = "0202"
sol = Solution().openLock(deadends, target)
print(sol)


class Solution:

    def numIslands(self, grid: List[List[str]]) -> int:

        if not grid or not grid[0]:
            return 0

        M, N = len(grid), len(grid[0])
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        def isValidIndex(i, j):
            return 0 <= i < M and 0 <= j < N

        def dfs(i, j):
            grid[i][j] = '0'
            for dr, dc in directions:
                nr = i + dr
                nc = j + dc
                if isValidIndex(nr, nc) and grid[nr][nc] == "1":
                    dfs(nr, nc)

        def bfs(i, j):
            grid[i][j] = '0'
            q = collections.deque([(i, j), ])
            while q:
                r, c = q.popleft()
                for dr, dc in directions:
                    nr = r + dr
                    nc = c + dc
                    if isValidIndex(nr, nc) and grid[nr][nc] == '1':
                        grid[nr][nc] = '0'
                        q.append((nr, nc))

        count = 0
        for i in range(M):
            for j in range(N):
                if grid[i][j] == "1":
                    count += 1
                    # choose the method
                    # dfs(i, j)
                    bfs(i, j)

        return count