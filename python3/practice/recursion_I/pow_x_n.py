
# Implement pow(x, n), which calculates x raised to the power n (i.e. xn).
#
#
#
# Example 1:
#
# Input: x = 2.00000, n = 10
# Output: 1024.00000
# Example 2:
#
# Input: x = 2.10000, n = 3
# Output: 9.26100
# Example 3:
#
# Input: x = 2.00000, n = -2
# Output: 0.25000
# Explanation: 2-2 = 1/22 = 1/4 = 0.25
#
#
# Constraints:
#
# -100.0 < x < 100.0
# -231 <= n <= 231-1
# -104 <= xn <= 104


class Solution:
    def myPow(self, x: float, n: int) -> float:  # this is not the exercise
        return x ** n

    def myPow(self, x: float, n: int) -> float:  # recursive
        if n <= 1:
            return 1 if n == 0 else x if n == 1 else 1 / self.myPow(x, -n)
        half = self.myPow(x, n // 2)
        return half * half * x if n % 2 else half * half


x = 2.0
n = 10
sol = Solution().myPow(x, n)
print(sol)
