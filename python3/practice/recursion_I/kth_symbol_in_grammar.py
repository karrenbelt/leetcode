
# On the first row, we write a 0. Now in every subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each occurrence of 1 with 10.
#
# Given row N and index K, return the K-th indexed symbol in row N. (The values of K are 1-indexed.) (1 indexed).
#
# Examples:
# Input: N = 1, K = 1
# Output: 0
#
# Input: N = 2, K = 1
# Output: 0
#
# Input: N = 2, K = 2
# Output: 1
#
# Input: N = 4, K = 5
# Output: 1
#
# Explanation:
# row 1: 0
# row 2: 01
# row 3: 0110
# row 4: 01101001
# Note:
#
# N will be an integer in the range [1, 30].
# K will be an integer in the range [1, 2^(N-1)].


class Solution:
    def kthGrammar(self, N: int, K: int) -> int:  # TLE
        n = '0'
        for i in range(1, N):
            zero_idx = [i for i, s in enumerate(n) if s == '0']
            n = ''.join(['01' if i in zero_idx else '10' for i in range(len(n))])
        return n[K-1]

    def kthGrammar(self, N: int, K: int) -> int:  # tail-recursion
        def helper(k):
            if k == 1:
                return 0
            s = helper((k + 1) // 2)
            return s if k % 2 == 1 else s ^ 1
        return helper(K)

    def kthGrammar(self, N: int, K: int) -> int:  # iterative
        ans = 0
        while K > 1:
            if K > 2**(N-2):
                ans ^= 1
                K -= 2**(N-2)
            else:
                N -= 1
        return ans

    def kthGrammar(self, N: int, K: int) -> int:  # one-liner
        # return 0 if K == 1 else self.kthGrammar(N-1, K) if K <= 2**(N-2) else 1 ^ self.kthGrammar(N, K-2**(N-2))
        return 0 if N == 1 else (1 - K % 2) ^ self.kthGrammar(N-1, (K+1) // 2)


N = 30
K = 4
sol = Solution().kthGrammar(N, K)
print(sol)
