
# Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1 ... n.
#
# Example:
#
# Input: 3
# Output:
# [
#   [1,null,3,2],
#   [3,2,null,1],
#   [3,1,null,null,2],
#   [2,1,3],
#   [1,null,2,null,3]
# ]
# Explanation:
# The above output corresponds to the 5 unique BST's shown below:
#
#    1         3     3      2      1
#     \       /     /      / \      \
#      3     2     1      1   3      2
#     /     /       \                 \
#    2     1         2                 3
#
#
# Constraints:
#
# 0 <= n <= 8

from typing import List


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def generateTrees(self, n: int) -> List[TreeNode]:

        def build_trees(i, j):
            if i > j:
                return [None]
            if i == j:
                return [TreeNode(i)]
            trees = []
            for k in range(i, j + 1):
                left = build_trees(i, k - 1)
                right = build_trees(k + 1, j)
                for l in left:
                    for r in right:
                        trees.append(TreeNode(k, l, r))
            return trees

        return build_trees(1, n) if n else []


n = 3
sol = Solution().generateTrees(n)

