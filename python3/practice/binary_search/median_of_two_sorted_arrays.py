# Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
#
# Follow up: The overall run time complexity should be O(log (m+n)).
#
#
#
# Example 1:
#
# Input: nums1 = [1,3], nums2 = [2]
# Output: 2.00000
# Explanation: merged array = [1,2,3] and median is 2.
# Example 2:
#
# Input: nums1 = [1,2], nums2 = [3,4]
# Output: 2.50000
# Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
# Example 3:
#
# Input: nums1 = [0,0], nums2 = [0,0]
# Output: 0.00000
# Example 4:
#
# Input: nums1 = [], nums2 = [1]
# Output: 1.00000
# Example 5:
#
# Input: nums1 = [2], nums2 = []
# Output: 2.00000
#
#
# Constraints:
#
# nums1.length == m
# nums2.length == n
# 0 <= m <= 1000
# 0 <= n <= 1000
# 1 <= m + n <= 2000
# -106 <= nums1[i], nums2[i] <= 106

from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(n log n)
        nums1[len(nums1):] = nums2
        nums1.sort()
        return float(nums1[len(nums1) // 2]) if len(nums1) % 2 else \
            sum(nums1[len(nums1) // 2 - 1:len(nums1) // 2 + 1]) / 2

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(n log n)
        # I believe this is faster in python because python sort is a mixture of merge and quick sort
        #
        nums1.extend(nums2)
        nums1.sort()
        return (nums1[len(nums1) // 2] + nums1[~len(nums1) // 2]) / 2

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(log m+n)

        def kth(a, b, k):
            if not a:
                return b[k]
            if not b:
                return a[k]
            ia, ib = len(a) // 2, len(b) // 2
            ma, mb = a[ia], b[ib]
            if ia + ib < k:  # when k is bigger than the sum of a and b's median indices
                if ma > mb:  # if a's median is bigger than b's, b's first half doesn't include k
                    return kth(a, b[ib + 1:], k - ib - 1)
                return kth(a[ia + 1:], b, k - ia - 1)
            else:  # when k is smaller than the sum of a and b's indices
                if ma > mb:  # if a's median is bigger than b's, a's second half doesn't include k
                    return kth(a[:ia], b, k)
                return kth(a, b[:ib], k)

        length = len(nums1) + len(nums2)
        if length % 2 == 1:
            return kth(nums1, nums2, length // 2)
        return (kth(nums1, nums2, length // 2) + kth(nums1, nums2, length // 2 - 1)) / 2


nums1 = [1, 2]
nums2 = [3, 4]
sol = Solution().findMedianSortedArrays(nums1, nums2)
print(sol)
