
# A peak element is an element that is greater than its neighbors.
#
# Given an input array nums, where nums[i] ≠ nums[i+1], find a peak element and return its index.
#
# The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.
#
# You may imagine that nums[-1] = nums[n] = -∞.
#
# Example 1:
#
# Input: nums = [1,2,3,1]
# Output: 2
# Explanation: 3 is a peak element and your function should return the index number 2.
# Example 2:
#
# Input: nums = [1,2,1,3,5,6,4]
# Output: 1 or 5
# Explanation: Your function can return either index number 1 where the peak element is 2,
#              or index number 5 where the peak element is 6.
# Follow up: Your solution should be in logarithmic complexity.

from typing import List


class Solution:

    def findPeakElement(self, nums: List[int]) -> int:  # brute force: O(n) time and O(1) space
        # simply keep moving until you find a peak
        idx, n = 0, len(nums) - 1
        while idx < n and nums[idx] < nums[idx + 1]:
            idx += 1
        return idx

    def findPeakElement(self, nums: List[int]) -> int:  # O(log n) time, O(1) space
        # 1. no two consecutive numbers are the same
        # 2. the start and end are -inf
        # hence, there must exist a peak, and binary search can be used
        if len(nums) <= 1:
            return 0

        mid, left, right = 0, 0, len(nums) - 1
        while left < right:
            mid = (left + right) // 2
            if nums[mid] < nums[mid + 1]:
                left = mid + 1
            else:
                right = mid

        return left

    def findPeakElement(self, nums: List[int]) -> int:  # recursive binary search
        # I don't prefer this solution, but it can done and performs well (more if's to check)
        if len(nums) == 1:
            return 0
        if nums[0] > nums[1]:
            return 0
        if nums[-1] > nums[-2]:
            return len(nums) - 1

        mid = len(nums) // 2
        if nums[mid] > nums[mid - 1] and nums[mid] > nums[mid + 1]:
            return mid
        elif nums[mid] < nums[mid + 1]:
            return self.findPeakElement(nums[mid:]) + mid  # right
        else:
            return self.findPeakElement(nums[:mid + 1])  # left


nums = [1, 2, 1, 3, 5, 6, 4]   # 1 or 5
sol = Solution().findPeakElement(nums)
print(sol)

