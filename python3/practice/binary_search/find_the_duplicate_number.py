# Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.
#
# There is only one repeated number in nums, return this repeated number.
#
#
#
# Example 1:
#
# Input: nums = [1,3,4,2,2]
# Output: 2
# Example 2:
#
# Input: nums = [3,1,3,4,2]
# Output: 3
# Example 3:
#
# Input: nums = [1,1]
# Output: 1
# Example 4:
#
# Input: nums = [1,1,2]
# Output: 1
#
#
# Constraints:
#
# 2 <= n <= 3 * 104
# nums.length == n + 1
# 1 <= nums[i] <= n
# All the integers in nums appear only once except for precisely one integer which appears two or more times.
#
#
# Follow up:
#
# How can we prove that at least one duplicate number must exist in nums?
# Can you solve the problem without modifying the array nums?
# Can you solve the problem using only constant, O(1) extra space?
# Can you solve the problem with runtime complexity less than O(n2)?

from typing import List


class Solution:

    def findDuplicate(self, nums: List[int]) -> int:  # O(n log n) time O(1) space
        nums.sort()
        while len(nums) != nums[len(nums) - 1]:
            nums.pop()
        return nums[-1]

    def findDuplicate(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        h = {}
        for num in nums:
            if num in h:
                return num
            else:
                h[num] = 1

    def findDuplicate(self, nums: List[int]) -> int:  # O(n) time O(1) space
        # virtual linked list: only works if all numbers between 1 and n (as given)
        slow, fast = nums[0], nums[0]
        while True:
            slow, fast = nums[slow], nums[nums[fast]]
            if slow == fast:
                break

        slow = nums[0]
        while slow != fast:
            slow, fast = nums[slow], nums[fast]
        return slow

    def findDuplicate(self, nums):  # O(n log n) time O(1) space
        # binary search - only works if all numbers between 1 and n (as given)
        beg, end = 1, len(nums) - 1

        while beg + 1 <= end:
            mid, count = (beg + end) // 2, 0
            for num in nums:
                if num <= mid:
                    count += 1
            if count <= mid:
                beg = mid + 1
            else:
                end = mid
        return end


nums = [3, 1, 3, 4, 2]
nums = [1, 2, 3, 4, 5, 6, 7, 7]
sol = Solution().findDuplicate(nums)
print(sol)
