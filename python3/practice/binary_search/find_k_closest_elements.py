
# Given a sorted array arr, two integers k and x, find the k closest elements to x in the array.
# The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.
#
#
#
# Example 1:
#
# Input: arr = [1,2,3,4,5], k = 4, x = 3
# Output: [1,2,3,4]
# Example 2:
#
# Input: arr = [1,2,3,4,5], k = 4, x = -1
# Output: [1,2,3,4]
#
#
# Constraints:
#
# 1 <= k <= arr.length
# 1 <= arr.length <= 10^4
# Absolute value of elements in the array and x will not exceed 104

from typing import List


class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(n log(n) + k log(k)), O(k + n)
        return sorted(sorted(arr, key=lambda n: abs(x - n))[:k])

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # sliding window
        k_closest = arr[:k]
        differences = [abs(number - x) for number in arr]  # O(n)
        target_sum = sum(differences[:k])  # O(k)

        i = 0
        while i + k < len(arr):  # O(n - k)
            prev_sum = target_sum
            target_sum -= differences[i]
            target_sum += differences[i + k]
            if target_sum < prev_sum:
                k_closest = arr[i + 1: i + k + 1]
            i += 1

        return k_closest

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(log(n) + k), O(k)

        left, right = 0, len(arr) - 1
        while left <= right:
            mid = (left + right) // 2
            if arr[mid] < x:
                left = mid + 1
            else:
                right = mid - 1

        right = left
        while right - left < k:
            if left == 0:
                return arr[:k]
            if right == len(arr):
                return arr[-k:]
            if x - arr[left - 1] <= arr[right] - x:
                left -= 1
            else:
                right += 1

        return arr[left:right]

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(log(n) + k), O(k)
        low = 0
        high = len(arr) - k

        while low < high:
            mid = low + (high - low) // 2
            # There are k+1 elements between mid and mid+k, inclusive. if x<=arr[mid], then mid+k element won't be part
            # of our output since the array is sorted and x will be further away from mid+k element. So, in order to
            # change the start of the boundary, our boundary can't start from mid+1 anymore (because we won't be able
            # to include k elements.) Thus, we realise that the boundary for k elements needs to either start at mid or
            # before it. Thus, high moves to mid(not mid-1) because mid is a valid value for the k element starting
            # window.
            if x <= arr[mid]:
                high = mid
            # Quite similar to the case above,if x is not less than or equal to mid, that is x is greater than mid,
            # then the boundary for start of our answer lies after mid(excluding mid), thus low moves to mid+1.
            elif arr[mid + k] <= x:
                low = mid + 1
            # If both of the earlier conditions are not satisfied, then we consider the distance of x from both the
            # mid and mid+k . If x is closer to mid,similar to the 1st case, move it away from mid+k and move high
            # to mid. else move low to mid+1.
            elif abs(x - arr[mid]) <= abs(x - arr[mid + k]):
                high = mid
            else:
                low = mid + 1

        return arr[low:low + k]

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        left, right = 0, len(arr) - k
        while left < right:
            mid = (left + right) // 2
            if x - arr[mid] > arr[mid + k] - x:
                left = mid + 1
            else:
                right = mid
        return arr[left:left + k]


arr = [1, 2, 3, 4, 5]
arr = [1, 2, 3, 4, 5, 100, 101, 102, 103, 104, 105]
# arr = [1, 2, 4, 5]
k = 3
x = 70
sol = Solution().findClosestElements(arr, k, x)
print(sol)
