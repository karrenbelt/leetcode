# Given an array nums which consists of non-negative integers and an integer m, you can split the array into m non-empty continuous subarrays.
#
# Write an algorithm to minimize the largest sum among these m subarrays.
#
#
#
# Example 1:
#
# Input: nums = [7,2,5,10,8], m = 2
# Output: 18
# Explanation:
# There are four ways to split nums into two subarrays.
# The best way is to split it into [7,2,5] and [10,8],
# where the largest sum among the two subarrays is only 18.
# Example 2:
#
# Input: nums = [1,2,3,4,5], m = 2
# Output: 9
# Example 3:
#
# Input: nums = [1,4,4], m = 3
# Output: 4
#
#
# Constraints:
#
# 1 <= nums.length <= 1000
# 0 <= nums[i] <= 106
# 1 <= m <= min(50, nums.length)

from typing import List


class Solution:
    def splitArray(self, nums: List[int], m: int) -> int:
        # a variation of binary search (in the explore card - gives it away)
        # the search space: all possible results
        # we need left, right - or low, high boundaries: the largest element in the array, and the sum of the array
        # and our target: the smallest value of the largest sum among the subarrays

        def can_split(mid, nums, m):
            sub_sum, n_subarrays = 0, 1
            for num in nums:
                sub_sum += num
                if sub_sum > mid:  # new array needed
                    n_subarrays += 1
                    sub_sum = num
                    if n_subarrays > m:
                        return False
            return True

        low, high = max(nums), sum(nums)
        while low < high:
            mid = low + (high - low) // 2
            if can_split(mid, nums, m):
                high = mid
            else:
                low = mid + 1

        return low


nums = [7, 2, 5, 10, 8]
m = 2
nums = [1, 2, 3, 4, 5]
m = 2
nums = [1, 4, 4]
m = 3

sol = Solution().splitArray(nums, m)
print(sol)
