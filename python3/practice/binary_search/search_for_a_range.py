
# Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
#
# If target is not found in the array, return [-1, -1].
#
# Follow up: Could you write an algorithm with O(log n) runtime complexity?
#
#
#
# Example 1:
#
# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]
# Example 2:
#
# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]
# Example 3:
#
# Input: nums = [], target = 0
# Output: [-1,-1]
#
#
# Constraints:
#
# 0 <= nums.length <= 105
# -109 <= nums[i] <= 109
# nums is a non-decreasing array.
# -109 <= target <= 109

from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:  # brute force
        if target not in nums:
            return [-1, -1]
        idx = nums.index(target)
        start = idx
        while idx + 1 < len(nums) and nums[idx + 1] == target:
            idx += 1
        return [start, idx]

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # simplest, not the exercise
        return [nums.index(target), (len(nums)-1) - list(reversed(nums)).index(target)] if target in nums else [-1, -1]

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # combination of binary search and brute force
        # find one, if found then move left and right
        if not nums:
            return [-1, -1]

        left, right = 0, len(nums) - 1  # template I binary search
        while left <= right:
            mid = (left + right) // 2
            if nums[mid] == target:
                break
            elif nums[mid] < target:
                left = mid + 1
            else:
                right = mid - 1

        if nums[mid] != target:
            return [-1, -1]

        result = [mid, mid]

        # brute force exploration of neighbors - could use binary search here as well
        i = 1
        while mid-i >= 0 and nums[mid - i] == target:
            result[0] -= 1
            i += 1
        i = 1
        while mid + 1 < len(nums) and nums[mid + i] == target:
            result[1] += 1
            i += 1

        return result

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # double binary search: find start and end
        if not nums:
            return [-1, -1]

        def search(nums, target, first=True):
            left, right = 0, len(nums) - 1
            while left < right - 1:
                mid = left + (right - left) // 2
                if nums[mid] == target:
                    if first:
                        right = mid
                    else:
                        left = mid
                elif nums[mid] < target:
                    left = mid + 1
                else:
                    right = mid - 1

            if first:
                return left if nums[left] == target else right if nums[right] == target else -1
            else:
                return right if nums[right] == target else left if nums[left] == target else -1

        start = search(nums, target, first=True)
        end = search(nums, target, first=False)
        return [start, end]

    def searchRange(self, nums: List[int], target: int) -> List[int]:
        result = [-1, -1]
        if not nums:
            return result

        l, r = 0, len(nums) - 1
        while l < r:  # search left index
            m = (l + r) / 2
            if nums[m] < target:
                l = m + 1
            else:
                r = m
        if nums[l] != target:
            return result
        result[0] = l
        r = len(nums) - 1  # starting from previous left index
        while l < r:  # search right index
            m = (l + r) / 2 + 1
            if nums[m] == target:
                l = m
            else:
                r = m - 1
        result[1] = l
        return result

    def searchRange(self, nums, target):  # two binary searches, the smart way
        # Here, my helper function is a simple binary search, telling me the first index where I could insert a
        # number n into nums to keep it sorted. Thus, if nums contains target, I can find the first occurrence with
        # search(target). I do that, and if target isn't actually there, then I return [-1, -1]. Otherwise, I ask
        # search(target+1), which tells me the first index where I could insert target+1, which of course is one index
        # behind the last index containing target, so all I have left to do is subtract 1.
        def search(n):
            left, right = 0, len(nums)
            while left < right:
                mid = (left + right) / 2
                if nums[mid] >= n:
                    right = mid
                else:
                    left = mid + 1
            return left
        lo = search(target)
        return [lo, search(target + 1) - 1] if target in nums[lo:lo + 1] else [-1, -1]

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # recursion, O(log n)
        # The search helper function returns an index range just like the requested searchRange function, but only
        # searches in nums[lo..hi]. It first compares the end points and immediately returns [lo, hi] if that whole
        # part of nums is full of target, and immediately returns [-1, -1] if target is outside the range. The
        # interesting case is when target can be in the range but doesn't fill it completely.
        #
        # In that case, we split the range in left and right half, solve them recursively, and combine their results
        # appropriately. Why doesn't this explode exponentially? Well, let's call the numbers in the left half A, ..., B
        # and the numbers in the right half C, ..., D. Now if one of them immediately return their [lo, hi] or [-1, -1],
        # then this doesn't explode. And if neither immediately returns, that means we have
        # A <= target <= B and C <= target <= D. And since nums is sorted, we actually have target <= B <= C <= target,
        # so B = C = target. The left half thus ends with target and the right half starts with it. I highlight that
        # because it's important. Now consider what happens further. The left half gets halved again. Call the middle
        # elements a and b, so the left half is A, ..., a, b, ..., B. Then a <= target and:
        #
        # If a < target, then the call analyzing A, ..., a immediately returns [-1, -1] and we only look further into
        # b, ..., B which is again a part that ends with target.
        # If a == target, then a = b = ... = B = target and thus the call analyzing b, ..., B immediately returns its
        # [lo, hi] and we only look further into A, ..., a which is again a part that ends with target.
        # Same for the right half C, ..., D. So in the beginning of the search, as long as target is only in at most
        # one of the two halves (so the other immediately stops), we have a single path. And if we ever come across the
        # case where target is in both halves, then we split into two paths, but then each of those remains a single
        # path. And both paths are only O(log n) long, so we have overall runtime O(log n).
        def search(lo, hi):
            if nums[lo] == target == nums[hi]:
                return [lo, hi]
            if nums[lo] <= target <= nums[hi]:
                mid = (lo + hi) / 2
                l, r = search(lo, mid), search(mid+1, hi)
                return max(l, r) if -1 in l+r else [l[0], r[1]]
            return [-1, -1]
        return search(0, len(nums)-1)


nums = [5, 7, 7, 8, 8, 8, 10]
target = 8

nums = [1]
target = 1

nums = [2, 2]
target = 2

sol = Solution().searchRange(nums, target)
print(sol)
