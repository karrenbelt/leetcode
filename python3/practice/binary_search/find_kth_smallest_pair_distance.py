
# Given an integer array, return the k-th smallest distance among all the pairs. The distance of a pair (A, B) is defined as the absolute difference between A and B.
#
# Example 1:
# Input:
# nums = [1,3,1]
# k = 1
# Output: 0
# Explanation:
# Here are all the pairs:
# (1,3) -> 2
# (1,1) -> 0
# (3,1) -> 2
# Then the 1st smallest distance pair is (1,1), and its distance is 0.
# Note:
# 2 <= len(nums) <= 10000.
# 0 <= nums[i] < 1000000.
# 1 <= k <= len(nums) * (len(nums) - 1) / 2.

from typing import List


class Solution:
    def smallestDistancePair(self, nums: List[int], k: int) -> int:  # O(n^2) - TLE
        distances = []
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                distances.append(abs(nums[i] - nums[j]))
        distances.sort()
        return distances[k-1]

    def smallestDistancePair(self, nums: List[int], k: int) -> int:  # O(n log n + m log n) - m is max distance
        nums.sort()
        low, high = 0, nums[-1] - nums[0]

        def possible(guess_dist) -> bool:
            i = ctr = 0
            j = 1
            while i < len(nums):
                # if distance j - i is less than guess, increment j
                while j < len(nums) and (nums[j] - nums[i]) <= guess_dist:
                    j += 1
                ctr += j - i - 1  # all that satisfy
                i += 1
            return ctr >= k

        while low < high:
            mid = (high + low) // 2
            if possible(mid):
                high = mid  # not mid - 1, since >= k, thus if mid == k is also correct
            else:
                low = mid + 1

        return low

    def smallestDistancePair(self, nums: List[int], k: int) -> int:
        nums.sort()
        left, right = 0, nums[-1] - nums[0]  # max distance
        while left < right:
            mid = (left + right) // 2  # guess
            j = ctr = 0
            for i in range(len(nums)):
                while j < len(nums) and nums[j] <= nums[i] + mid:   # if distance j - i is less than guess, increment j
                    j += 1
                ctr += j - i - 1  # all that satisfy the distance cut off criterion
            if ctr >= k:
                right = mid   # not mid - 1, since >= k, thus if mid == k is also correct
            else:
                left = mid + 1

        return left  # because bs ends when 2nd to last did not produce enough results


nums = [1, 3, 1]
k = 1
sol = Solution().smallestDistancePair(nums, k)
print(sol)
