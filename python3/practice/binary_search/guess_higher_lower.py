
# We are playing the Guess Game. The game is as follows:
#
# I pick a number from 1 to n. You have to guess which number I picked.
#
# Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.
#
# You call a pre-defined API int guess(int num), which returns 3 possible results:
#
# -1: The number I picked is lower than your guess (i.e. pick < num).
# 1: The number I picked is higher than your guess (i.e. pick > num).
# 0: The number I picked is equal to your guess (i.e. pick == num).
# Return the number that I picked.
#
#
#
# Example 1:
#
# Input: n = 10, pick = 6
# Output: 6
# Example 2:
#
# Input: n = 1, pick = 1
# Output: 1
# Example 3:
#
# Input: n = 2, pick = 1
# Output: 1
# Example 4:
#
# Input: n = 2, pick = 2
# Output: 2
#
#
# Constraints:
#
# 1 <= n <= 231 - 1
# 1 <= pick <= n


# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num):

class Solution(object):

    def guessNumber(self, n, method='binary_search'):
        """
        :type n: int
        :rtype: int
        """
        if n == 1: return 1
        low, high = 1, n

        if method == 'brute_force':
            for i in range(n):
                if guess(i) == 0:
                    return i

        elif method == 'binary_search':
            while low <= high:
                mid = (low + high) // 2
                ans = guess(mid)
                if ans == 0:
                    return mid
                elif ans == 1:
                    low = mid + 1
                elif ans == -1:
                    high = mid - 1

        elif method == 'ternary_search':
            while low <= high:
                mid1 = low + (high - low) / 3
                mid2 = high - (high - low) / 3
                res1 = guess(mid1)
                res2 = guess(mid2)
                if res1 == 0:
                    return mid1
                if res2 == 0:
                    return mid2
                elif res1 < 0:
                    high = mid1 - 1
                elif res2 > 0:
                    low = mid2 + 1
                else:
                    low = mid1 + 1
                    high = mid2 - 1;

        return -1

