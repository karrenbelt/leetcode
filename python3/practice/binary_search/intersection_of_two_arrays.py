
# Given two arrays, write a function to compute their intersection.
#
# Example 1:
#
# Input: nums1 = [1,2,2,1], nums2 = [2,2]
# Output: [2]
# Example 2:
#
# Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
# Output: [9,4]
# Note:
#
# Each element in the result must be unique.
# The result can be in any order.

from typing import List


class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        return list(set(nums1) & set(nums2))

    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:

        def binary_search(arr, target):
            left, right = 0, len(arr) - 1
            while left <= right:
                mid = left + (right - left) // 2
                if arr[mid] == target:
                    return True
                elif arr[mid] > target:
                    right = mid - 1
                else:
                    left = mid + 1
            return False

        if len(nums2) > len(nums1):
            nums1, nums2 = nums2, nums1

        nums1.sort()
        result = []
        for num in nums2:
            if num not in result and binary_search(nums1, num):
                result.append(num)

        return result


nums1 = [4, 9, 5]
nums2 = [9, 4, 9, 8, 4]
sol = Solution().intersection(nums1, nums2)
print(sol)
