
# Given a list of sorted characters letters containing only lowercase letters, and given a target letter target, find the smallest element in the list that is larger than the given target.
#
# Letters also wrap around. For example, if the target is target = 'z' and letters = ['a', 'b'], the answer is 'a'.
#
# Examples:
# Input:
# letters = ["c", "f", "j"]
# target = "a"
# Output: "c"
#
# Input:
# letters = ["c", "f", "j"]
# target = "c"
# Output: "f"
#
# Input:
# letters = ["c", "f", "j"]
# target = "d"
# Output: "f"
#
# Input:
# letters = ["c", "f", "j"]
# target = "g"
# Output: "j"
#
# Input:
# letters = ["c", "f", "j"]
# target = "j"
# Output: "c"
#
# Input:
# letters = ["c", "f", "j"]
# target = "k"
# Output: "c"
# Note:
# letters has a length in range [2, 10000].
# letters consists of lowercase letters, and contains at least 2 unique letters.
# target is a lowercase letter.

from typing import List


class Solution:

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # linear
        for letter in letters:
            if letter > target:
                return letter
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # hashset
        for letter in sorted(set(letters)):
            if letter > target:
                return letter
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # binary search (template III)

        left, mid, right = 0, 0, len(letters) - 1
        while left + 1 < right:
            mid = left + (right - left) // 2
            if letters[mid] <= target:
                left = mid
            elif letters[mid] > target:
                right = mid

        if letters[left] > target:
            return letters[left]
        if letters[mid] > target:
            return letters[mid]
        if letters[right] > target:
            return letters[right]
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # binary (template II)

        left, right = 0, len(letters)
        while left < right:
            mid = left + (left + right) // 2
            if letters[mid] <= target:
                left = mid + 1
            else:
                right = mid

        if left != len(letters) and letters[left] > target:
            return letters[left]
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # pre-process instead of post

        if letters[-1] <= target:
            return letters[0]

        left, right = 0, len(letters)
        while left < right:
            mid = left + (right - left) // 2
            if letters[mid] <= target:
                left = mid + 1
            else:
                right = mid

        return letters[left]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # binary (template I)
        left, right = 0, len(letters) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if letters[mid] > target:
                right = mid - 1
            else:
                left = mid + 1
        return letters[left] if left < len(letters) else letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str, left=None, right=None) -> str:  # recursive
        if left is right is None:
            left, right = 0, len(letters) - 1

        if left > right:
            return letters[left] if left < len(letters) else letters[0]

        mid = left + (right - left) // 2
        if letters[mid] > target:
            right = mid - 1
        else:
            left = mid + 1
        return self.nextGreatestLetter(letters, target, left, right)

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # bisect
        import bisect  # bisect.bisect is bisect.bisect_right
        return letters[bisect.bisect(letters, target) % len(letters)]


letters = ["c", "f", "j"]
targets = ['a', 'c', 'd', 'g', 'j', 'k']
solutions = ['c', 'f', 'f', 'j', 'c', 'c']

for i, target in enumerate(targets):
    sol = Solution().nextGreatestLetter(letters, target)
    assert sol == solutions[i]
