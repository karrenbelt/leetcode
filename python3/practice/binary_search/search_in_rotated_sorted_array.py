
# You are given an integer array nums sorted in ascending order, and an integer target.
#
# Suppose that nums is rotated at some pivot unknown to you beforehand (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
#
# If target is found in the array return its index, otherwise, return -1.
#
#
#
# Example 1:
#
# Input: nums = [4,5,6,7,0,1,2], target = 0
# Output: 4
# Example 2:
#
# Input: nums = [4,5,6,7,0,1,2], target = 3
# Output: -1
# Example 3:
#
# Input: nums = [1], target = 0
# Output: -1
#
#
# Constraints:
#
# 1 <= nums.length <= 5000
# -10^4 <= nums[i] <= 10^4
# All values of nums are unique.
# nums is guranteed to be rotated at some pivot.
# -10^4 <= target <= 10^4

from typing import List


class Solution:

    def search(self, nums: List[int], target: int) -> int:
        # 1. find the pivot point
        # 2. divide the array in two subarrays
        # 3. call binary search
        arr, n, key = nums, len(nums), target
        pivot = find_pivot(arr, 0, n - 1)
        # If we didn't find a pivot, then the array is not rotated at all
        if pivot == -1:
            return binary_search(arr, 0, n - 1, key)
        # If we found a pivot:
        # 1. compare with pivot
        # 2. search in two subarrays around pivot
        if arr[pivot] == key:
            return pivot
        if arr[0] <= key:
            return binary_search(arr, 0, pivot - 1, key)
        return binary_search(arr, pivot + 1, n - 1, key)


def find_pivot(arr, low, high):
    if high < low:
        return -1
    if high == low:
        return low
    mid = (low + high) // 2
    if mid < high and arr[mid] > arr[mid + 1]:
        return mid
    if mid > low and arr[mid] < arr[mid - 1]:
        return mid - 1
    if arr[low] >= arr[mid]:
        return find_pivot(arr, low, mid - 1)
    return find_pivot(arr, mid + 1, high)


def binary_search(arr, low, high, key):
    if high < low:
        return -1
    mid = (low + high) // 2
    if key == arr[mid]:
        return mid
    if key > arr[mid]:
        return binary_search(arr, (mid + 1), high, key)
    return binary_search(arr, low, (mid - 1), key)
