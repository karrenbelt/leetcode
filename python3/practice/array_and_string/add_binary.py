
# Given two binary strings, return their sum (also a binary string).
#
# The input strings are both non-empty and contains only characters 1 or 0.
#
# Example 1:
#
# Input: a = "11", b = "1"
# Output: "100"
# Example 2:
#
# Input: a = "1010", b = "1011"
# Output: "10101"
#
#
# Constraints:
#
# Each string consists only of '0' or '1' characters.
# 1 <= a.length, b.length <= 10^4
# Each string is either "0" or doesn't contain any leading zero.


class Solution:
    def addBinary(self, a: str, b: str) -> str:
        # return bin(int(a, 2) + int(b, 2))[2:]

        # ensure b is always shorter; swap variables otherwise
        max_len = max(len(a), len(b))
        a = a.zfill(max_len)
        b = b.zfill(max_len)
        result = ''
        carry_over = 0

        for i in range(max_len - 1, -1, -1):
            r = carry_over + int(a[i]) + int(b[i])
            result = ('1' if r % 2 == 1 else '0') + result
            carry_over = 0 if r < 2 else 1

        return '1' + result if carry_over else result



a = "1010"
b = "1011"

a = '11'
b = '1'
sol = Solution().addBinary(a, b)
print(sol)
