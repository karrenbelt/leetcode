
# Given an integer rowIndex, return the rowIndexth row of the Pascal's triangle.
#
# Notice that the row index starts from 0.
#
# Follow up:

# Could you optimize your algorithm to use only O(k) extra space?

from typing import List


class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        # iterative solution (no recursion, no memoization)
        row = [1]
        for i in range(rowIndex):
            row = [1] + [sum(row[j-1:j+1]) for j in range(1, len(row))] + [1]
        return row


for rowIndex in range(5):
    sol = Solution().getRow(rowIndex)
    print(sol)
