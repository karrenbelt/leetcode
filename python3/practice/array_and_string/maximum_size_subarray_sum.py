
# Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.
#
# Example:
#
# Input: s = 7, nums = [2,3,1,2,4,3]
# Output: 2
# Explanation: the subarray [4,3] has the minimal length under the problem constraint.
# Follow up:
# If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log n).

from typing import List


class Solution:
    def minSubArrayLen(self, s: int, nums: List[int]) -> int:
        # move right pointer until we hit the threshold, then move left threshold until we don't anymore, repeat
        min_len = float('inf')
        left, right, cum_sum = 0, 0, 0

        while right < len(nums):
            cum_sum += nums[right]
            if cum_sum >= s:
                while left <= right and cum_sum >= s:
                    min_len = min(min_len, right - left + 1)
                    cum_sum -= nums[left]
                    left += 1
            right += 1

        return min_len if min_len != float('inf') else 0


s = 7
nums = [2, 3, 1, 2, 4, 3]

s = 4
nums = [0, 4, 4]

s = 11
nums = [1, 2, 3, 4, 5]

s = 7
nums = [2, 3, 1, 2, 4, 3]

sol = Solution().minSubArrayLen(s, nums)
print(sol)
