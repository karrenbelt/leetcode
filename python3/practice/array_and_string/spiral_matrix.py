
# Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

# Example 1:
#
# Input:
# [
#  [ 1, 2, 3 ],
#  [ 4, 5, 6 ],
#  [ 7, 8, 9 ]
# ]
# Output: [1,2,3,6,9,8,7,4,5]
# Example 2:
#
# Input:
# [
#   [1, 2, 3, 4],
#   [5, 6, 7, 8],
#   [9,10,11,12]
# ]
# Output: [1,2,3,4,8,12,11,10,9,5,6,7]

from typing import List


matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    ]


class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:

        res = []

        while matrix:

            if matrix[0]:  # top left to right
                res.extend(matrix.pop(0))  # slow
                # matrix = matrix[1:]
            if matrix and matrix[0]:  # top right to bottom
                for row in matrix:
                    res.append(row.pop())
            if matrix:  # bottom right to left
                res.extend(matrix.pop()[::-1])
            if matrix and matrix[0]:  # bottom left to top
                for row in matrix[::-1]:
                    res.append(row.pop(0))

        return res


sol = Solution().spiralOrder(matrix)
print(sol)
