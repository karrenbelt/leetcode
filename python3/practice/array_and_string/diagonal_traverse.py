
# Given a matrix of M x N elements (M rows, N columns),
# return all elements of the matrix in diagonal order as shown in the below image.

# Input:
# [
#  [ 1, 2, 3 ],
#  [ 4, 5, 6 ],
#  [ 7, 8, 9 ]
# ]
#
# Output:  [1,2,4,7,5,3,6,8,9]

from typing import List


class Solution:
    def findDiagonalOrder(self, matrix: List[List[int]]) -> List[int]:
        if not matrix or not matrix[0]:
            return []

        m = len(matrix)
        n = len(matrix[0])
        forward = [[] for _ in range(m + n - 1)]
        # backward = [[] for _ in range(m + n - 1)]
        for r in range(m):
            for c in range(n):
                forward[r + c].append(matrix[r][c])
                # backward[c - r + m - 1].append(matrix[r][c])

        # we need to reverse the order of every other diagonal and flatten the structure
        return [y for i, x in enumerate(forward) for y in x[::[-1, 1][i % 2]]]




matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    ]

# matrix = [[1,2,3],[4,5,6],[7,8,9],[10,11,12]]

sol = Solution().findDiagonalOrder(matrix)
print(sol)

