
# Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.

# Input: 5
# Output:
# [
#      [1],
#     [1,1],
#    [1,2,1],
#   [1,3,3,1],
#  [1,4,6,4,1]
# ]

from typing import List


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:

        triangle = [[] for _ in range(numRows)]
        if numRows == 0:
            return triangle
        triangle[0] = [1]
        if numRows == 1:
            return triangle
        triangle[1] = [1, 1]
        if numRows == 2:
            return triangle

        for i in range(2, numRows):
            prev = triangle[i-1]
            row = [1] + [sum(prev[j-1:j+1]) for j in range(1, len(prev))] + [1]
            triangle[i].extend(row)

        return triangle


sol = Solution().generate(5)
print(sol)
