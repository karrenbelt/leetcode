
# stack problems:
1130. Minimum Cost Tree From Leaf Values
907. Sum of Subarray Minimums
901. Online Stock Span
856. Score of Parentheses
503. Next Greater Element II
496. Next Greater Element I
84. Largest Rectangle in Histogram
42. Trapping Rain Water

# cyclic swapping:
41. First Missing Positive
268. Missing Number
765. Couples Holding Hands

# bit manipulation
- https://leetcode.com/discuss/general-discussion/1049269/Understanding-how-numbers-are-stored-in-the-computer-using-only-0-and-1.
- https://leetcode.com/discuss/general-discussion/1073221/all-about-bitwise-operations-beginner-intermediate

# binary search
- https://leetcode.com/problems/binary-search/discuss/423162/Binary-Search-101-The-Ultimate-Binary-Search-Handbook
- https://leetcode.com/problems/find-k-th-smallest-pair-distance/discuss/769705/Python-Clear-explanation-Powerful-Ultimate-Binary-Search-Template.-Solved-many-problems.
- https://leetcode.com/problems/magnetic-force-between-two-balls/discuss/819165/6-questions-in-one-template-of-binary-search-for-beginners!-python

problems:
410 Split Array Largest Sum
774 Minimize Max Distance to Gas Station
875 Koko Eating Bananas
1011 Capacity To Ship Packages In N Days
1231 Divide Chocolate
1283 Find The Smallest Divisor Given A Threshold

# backtracking
- https://leetcode.com/problems/permutations/discuss/301437/A-more-general-approach-to-backtracking-questions-in-Python

# bfs
https://leetcode.com/problems/count-good-nodes-in-binary-tree/discuss/635351/BFS-With-MaxValue-or-Template-of-Similar-Problems-Python

# dynamic programming
https://leetcode.com/problems/target-sum/discuss/455024/DP-IS-EASY!-5-Steps-to-Think-Through-DP-Questions.
https://leetcode.com/discuss/general-discussion/1050391/must-do-dynamic-programming-problems-category-wise

# backtracking
https://medium.com/algorithms-and-leetcode/backtracking-e001561b9f28

# sliding window
https://leetcode.com/problems/find-all-anagrams-in-a-string/discuss/92007/Sliding-Window-algorithm-template-to-solve-all-the-Leetcode-substring-search-problem.

# subarray
https://leetcode.com/problems/make-sum-divisible-by-p/discuss/854441/Python-O(n).-Explanation-and-generalization-to-other-subarray-problems

# interesting website
https://cheatsheet.dennyzhang.com/

# stock problems
https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/discuss/75924/Most-consistent-ways-of-dealing-with-the-series-of-stock-problems


# data structures I:
1. array: 217, 53
2. array: 1, 88
3. array: 350, 121
4. array: 566, 118
5. array: 36, 74
6. string: 387, 383, 242
7. linked list: 141, 21, 203
8. linked list: 206, 83
9. stack / queue: 20, 232
10. tree: 144, 94, 145
11. tree: 102, 104, 101
12. tree: 226, 112
13. tree: 700, 701
14. tree: 98, 653, 235

# data structures II:

# algorithms I:
1. binary search: 704, 278, 35
2. two pointers: 977, 189
3. two pointers: 283, 167
4. two pointers: 344, 557
5. two pointers: 876, 19
6. sliding window: 3, 567
7. bfs / dfs: 733, 695
8. bfs / dfs: 617, 116
9. bfs / dfs: 542, 994
10. recursion / backtracking: 21, 206
11. recursion / backtracking: 77, 46, 784
12. dynamic programming: 70, 198, 120
13. bit manipulation: 231, 191
14. bit manipulation: 190, 136

# algorithms II:
