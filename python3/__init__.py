

# basic data structures
import re
import time
from copy import deepcopy
from typing import List, Iterable, Union
from collections import deque


# Linked List

class LinkedListError(Exception):
    pass


class SinglyLinkedListNode:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next

    @property
    def val(self):
        return self.value

    @val.setter
    def val(self, value):
        self.value = value

    def __int__(self):
        return int(self.val)

    def __iter__(self):
        node = self
        while node:
            yield node
            node = node.next

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"

    # def __hash__(self):
    #     return hash(self.__dict__)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.__dict__ == other.__dict__


class DoublyLinkedListNode(SinglyLinkedListNode):

    def __init__(self, value, next=None, prev=None, child=None, children: List = None, parents: List = None):
        super().__init__(value, next)
        self.prev = prev
        self.child = child
        # self.children = children if children else []
        self.parents = parents if parents else []


# class LinkedListBase:
#
#     def __init__(self, data: Iterable = None):
#         self._head = None
#         self._tail = None
#         self._size = 0
#         if data:
#             self.extend(data)


class SinglyLinkedList:

    Node = SinglyLinkedListNode

    def __init__(self, data: Iterable = None):
        self._head = None
        self._tail = None
        self._size = 0
        if data:
            self.extend(data)

    @property
    def head(self):
        return self._head

    @property
    def tail(self):
        return self._tail

    def _index_exists(self, index: int) -> bool:
        return 0 <= index < self._size

    def get_node(self, index: int) -> SinglyLinkedListNode:
        if not self._index_exists(index):
            raise IndexError(f'Linked List index out of range')
        node, i = self.head, 0
        while node.next and i < index:
            node = node.next
            i += 1
        return node

    def get_value(self, index: int) -> int:
        node = self.get_node(index)
        return node.value if node else None

    def appendleft(self, val: int) -> None:
        self._head = self.Node(val, next=self.head)
        self._size += 1

    def append(self, val: int) -> None:
        if not self.tail:
            self._head = self._tail = self.Node(val)
        else:
            self.tail.next = self.Node(val)
            self._tail = self.tail.next
        self._size += 1

    def extend(self, values: Iterable):
        for value in values:
            self.append(value)

    def extendleft(self, values: Iterable):
        for value in values:
            self.appendleft(value)

    def pop(self, index: int = None):
        node = self.delete_index(index if index is not None else self._size - 1)
        return node

    def popleft(self):
        return self.delete_index(0)

    def insert(self, index: int, value: int) -> None:
        if not self._index_exists(index):
            IndexError(f'cannot insert {self.Node} at index {index}')
        if index == 0:
            self.appendleft(value)
        else:
            prev = self.get_node(index - 1)
            prev.next = self.Node(value, next=prev.next)
        self._size += 1

    def delete_index(self, index: int):
        if index == 0 and self.head:
            node = self.head
            self._head = self.head.next
        else:
            prev = self.get_node(index - 1)
            node = prev.next
            if prev and node:
                prev.next = prev.next.next
        self._size -= 1
        return node

    def index(self, value) -> int:
        node = self.head
        i = 0
        while node and node.value != value:
            node = node.next
            i += 1
        return i

    def reverse(self):
        prev = None
        node = self.head
        while node:
            temp = node
            node = node.next
            temp.next = prev
            prev = temp

    def rotate(self, n: int):  # TODO: negative values?
        if n < 0:
            raise LinkedListError(f'n must be positive')
        if self.is_cyclic:
            raise LinkedListError('cannot rotate a cyclic Linked List')
        if not self.head or self.head.next or not n:
            return

        nodes = list(self)
        pivot = n % len(nodes)
        nodes[-1].next = nodes[0]
        nodes[- pivot - 1].next = None
        self._head = nodes[-pivot]
        self._tail = nodes[(-pivot-1)]

    def copy(self):
        return deepcopy(self)

    @property
    def is_cyclic(self):
        slow = fast = self.head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
            if slow == fast:
                return True
        return False

    def detectCycle(self):
        slow = fast = self.head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
            if slow == fast:
                break
        else:
            return None
        node = self.head
        while node != slow:
            slow = slow.next
            node = node.next
        return node

    def __len__(self):
        return self._size

    def __getitem__(self, item: int):
        return self.get_node(item)

    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.next

    def __eq__(self, other):  # TODO: circular reference
        if not isinstance(other, type(self)):
            return NotImplemented
        return list(self) == list(other)

    def __str__(self):
        if self.is_cyclic:
            raise LinkedListError('cannot rotate a cyclic Linked List')
        return f"{self.__class__.__name__}: {'->'.join(map(lambda node: str(node.val), self))}"

    def __repr__(self):
        return f"{self.__class__.__name__}"


class DoublyLinkedList:

    Node = DoublyLinkedListNode

    def __init__(self, data: Iterable = None):
        self._head = None
        self._tail = None
        self._size = 0
        if data:
            self.extend(data)

    @property
    def head(self):
        return self._head

    @property
    def tail(self):
        return self._tail

    def appendleft(self, data):
        node = self.Node(data)
        if self.head is None:
            self._head = self._tail = node
        else:
            node.next = self.head
            self.head.prev = node
            self._head = node
        self._size += 1

    def extendleft(self, data):
        for value in data:
            self.appendleft(value)

    def append(self, data):
        node = self.Node(data)
        if self.head is None:
            self._head = self._tail = node
        else:
            node.prev = self.tail
            self.tail.next = node
            self._tail = node
        self._size += 1

    def extend(self, data):
        for value in data:
            self.append(value)

    def popleft(self):
        node = self.head
        if self.head:
            if not self.head.next:
                self._head = self._tail = None
            else:
                self._head = self.head.next
                self.head.prev = None
            self._size -= 1
        return node

    def pop(self):
        node = self.tail
        if self.tail:
            if not self.tail.prev:
                self._head = self._tail = None
            else:
                self._tail = self.tail.prev
                self.tail.next = None
            self._size -= 1
        return node

    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.next

    def _index_exists(self, index: int) -> bool:
        return 0 <= abs(index + (index < 0)) < self._size

    def get_node(self, index: int) -> SinglyLinkedListNode:
        if not self._index_exists(index):
            raise IndexError(f'Doubly Linked List index out of range')
        if index >= 0:
            node, i = self.head, 0
            while node.next and i < index:
                node = node.next
                i += 1
        else:
            node, i = self.tail, 0
            while node.next and i < index:
                node = node.next
                i += 1
        return node

    def __getitem__(self, item: int):
        return self.get_node(item)


def singly_linked_list_from_array(nums: Iterable) -> SinglyLinkedList:  # TODO: refactor away
    llist = SinglyLinkedList()
    llist.extend(nums)
    return llist


def test_linked_list():
    n = 10
    ll = SinglyLinkedList(range(n))
    assert len(ll) == n
    ll.get_node(4)

    # doubly
    n = 10
    ll = DoublyLinkedList(range(n))
    assert len(ll) == n
    ll.get_node(4)


# class MLLLCodec:
#
#     empty = 'null'
#
#     @staticmethod
#     def deserialize(data: str):
#         data = deque(re.findall(rf'-?\d+|{Codec.empty}', data))
#         if not data:
#             return
#         head = DoublyLinkedListNode(int(data.popleft()))
#
#         return head


# Trees

class BinaryTreeNode:
    def __init__(self, value: int, left: 'BinaryTreeNode' = None, right: 'BinaryTreeNode' = None):
        self.value = value
        self.left = left
        self.right = right

    @property
    def val(self):  # often abbreviated as such in leetcode exercises
        return self.value

    @val.setter
    def val(self, value: int):
        self.value = value

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    @left.setter
    def left(self, node: 'BinaryTreeNode'):
        self._left = node

    @right.setter
    def right(self, node: 'BinaryTreeNode'):
        self._right = node

    def copy(self):
        return deepcopy(self)

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.value}"

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.__dict__ == other.__dict__

    def __hash__(self) -> int:
        return hash(Codec.serialize(self))

    def __str__(self):
        # binary tree pretty print
        def _build_tree_string(root, curr_index, index=False, delimiter='-'):
            """Recursively walk down the binary tree and build a pretty-print string.
            In each recursive call, a "box" of characters visually representing the
            current (sub)tree is constructed line by line. Each line is padded with
            whitespaces to ensure all lines in the box have the same length. Then the
            box, its width, and start-end positions of its root node value repr string
            (required for drawing branches) are sent up to the parent call. The parent
            call then combines its left and right sub-boxes to build a larger box etc.
            """
            if root is None:
                return [], 0, 0, 0

            line1 = []
            line2 = []
            if index:
                node_repr = '{}{}{}'.format(curr_index, delimiter, root.value)
            else:
                node_repr = str(root.value)

            new_root_width = gap_size = len(node_repr)

            # Get the left and right sub-boxes, their widths, and root repr positions
            l_box, l_box_width, l_root_start, l_root_end = \
                _build_tree_string(root.left, 2 * curr_index + 1, index, delimiter)
            r_box, r_box_width, r_root_start, r_root_end = \
                _build_tree_string(root.right, 2 * curr_index + 2, index, delimiter)

            # Draw the branch connecting the current root node to the left sub-box
            # Pad the line with whitespaces where necessary
            if l_box_width > 0:
                l_root = (l_root_start + l_root_end) // 2 + 1
                line1.append(' ' * (l_root + 1))
                line1.append('_' * (l_box_width - l_root))
                line2.append(' ' * l_root + '/')
                line2.append(' ' * (l_box_width - l_root))
                new_root_start = l_box_width + 1
                gap_size += 1
            else:
                new_root_start = 0

            # Draw the representation of the current root node
            line1.append(node_repr)
            line2.append(' ' * new_root_width)

            # Draw the branch connecting the current root node to the right sub-box
            # Pad the line with whitespaces where necessary
            if r_box_width > 0:
                r_root = (r_root_start + r_root_end) // 2
                line1.append('_' * r_root)
                line1.append(' ' * (r_box_width - r_root + 1))
                line2.append(' ' * r_root + '\\')
                line2.append(' ' * (r_box_width - r_root))
                gap_size += 1
            new_root_end = new_root_start + new_root_width - 1

            # Combine the left and right sub-boxes with the branches drawn above
            gap = ' ' * gap_size
            new_box = [''.join(line1), ''.join(line2)]
            for i in range(max(len(l_box), len(r_box))):
                l_line = l_box[i] if i < len(l_box) else ' ' * l_box_width
                r_line = r_box[i] if i < len(r_box) else ' ' * r_box_width
                new_box.append(l_line + gap + r_line)

            # Return the new box, its width and its root repr positions
            return new_box, len(new_box[0]), new_root_start, new_root_end

        lines = _build_tree_string(self, 0, False, '-')[0]
        return '\n' + '\n'.join((line.rstrip() for line in lines))

    def __iter__(self):

        def traverse(node):
            if node:
                yield from traverse(node.left)
                yield node
                yield from traverse(node.right)

        return traverse(self)


class BinaryTreeNodeWithParent(BinaryTreeNode):
    # automatically sets parent node link
    def __init__(self, value: int, left: 'BinaryTreeNodeWithParent' = None, right: 'BinaryTreeNodeWithParent' = None):
        super().__init__(value, left, right)
        self._parent = None

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    @left.setter
    def left(self, node: 'BinaryTreeNodeWithParent'):
        if node is not None:
            node._parent = self
        self._left = node

    @right.setter
    def right(self, node: 'BinaryTreeNodeWithParent'):
        if node is not None:
            node._parent = self
        self._right = node

    @property
    def parent(self):
        return self._parent


class NAryTreeNode:

    def __init__(self, value, children: Iterable['NAryTreeNode'] = None):
        self.value = value
        self.children = list(children) if children is not None else []

    @property
    def val(self):  # often abbreviated as such in leetcode exercises
        return self.value

    @val.setter
    def val(self, value: int):
        self.value = value

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.value}"


class TrieNode:  # TODO: revise

    def __init__(self, letter: str, is_word: bool = False):
        self.letter = letter
        self.is_word = is_word


class Codec:  # TODO: rename?
    empty = 'null'  # None

    @staticmethod
    def serialize(root: BinaryTreeNode) -> str:
        string = []
        queue = deque([root])
        while queue:
            node = queue.popleft()
            if not node:
                string.append(Codec.empty)
            else:
                string.append(node.val)
                queue.extend([node.left, node.right])
        return f"[{','.join(map(str, string)).rstrip(f'{Codec.empty},')}]"

    @staticmethod
    def deserialize(data: str) -> BinaryTreeNode:
        data = deque(re.findall(rf'-?\d+|{Codec.empty}', data))
        if not data:
            return
        root = BinaryTreeNode(int(data.popleft()))
        queue = deque([root])
        while queue:
            node = queue.popleft()
            left = data.popleft() if data else str(Codec.empty)
            right = data.popleft() if data else str(Codec.empty)
            node.left = BinaryTreeNode(int(left)) if left != str(Codec.empty) else None
            node.right = BinaryTreeNode(int(right)) if right != str(Codec.empty) else None
            queue.extend(filter(None, [node.left, node.right]))
        return root

    @staticmethod
    def test(data: str = None):
        data = f'[5,4,0,3,{Codec.empty},2,{Codec.empty},-1,{Codec.empty},9]' if data is None else data
        root = Codec.deserialize(data)
        recovered = Codec.serialize(root)
        assert data == recovered, 'Codec deserialization / serialization test failed'


class NAryTreeCodec:
    empty = 'null'  # None

    @staticmethod
    def serialize(root: NAryTreeNode) -> str:
        string = []
        queue = deque([root, Codec.empty])
        while queue:
            node = queue.popleft()
            if node is NAryTreeCodec.empty:
                string.append(NAryTreeCodec.empty)
            else:
                string.append(node.val)
                queue.extend(node.children)
                queue.append(Codec.empty)
        return f"[{','.join(map(str, string)).rstrip(f'{NAryTreeCodec.empty},')}]"

    @staticmethod
    def deserialize(data: str) -> NAryTreeNode:
        data = deque(re.findall(rf'-?\d+|{NAryTreeCodec.empty}', data))
        if not data:
            return
        root = NAryTreeNode(int(data.popleft()))
        if data:
            data.popleft()
        queue = deque([root])
        while queue:
            node = queue.popleft()
            while data and data[0] != NAryTreeCodec.empty:
                child = NAryTreeNode(int(data.popleft()))
                node.children.append(child)
                queue.append(child)
            if data:
                data.popleft()
        return root

    @staticmethod
    def test(data: str = None):
        data = f"[1,{Codec.empty},2,3,4,5,{Codec.empty},{Codec.empty},6,7,{Codec.empty},8]" if data is None else data
        root = NAryTreeCodec.deserialize(data)
        recovered = NAryTreeCodec.serialize(root)
        assert data == recovered, 'NAryTreeCodec deserialization / serialization test failed'


def inorder_morris(root):  # O(n) time and O(1) space traversal
    node = root
    while node:
        if node.left:
            temp = node.left
            while temp.right and temp.right != node:
                temp = temp.right
            if not temp.right:
                temp.right, node = node, node.left
                continue
            temp.right = None
        node = node.right


# Graph
class GraphNode:
    def __init__(self, val):
        self.val = val
        self.neighbors = []  # set()  #set(neighbors) if neighbors is not None else set()

    def copy(self):
        return deepcopy(self)

    def _traverse(self):
        traversed = [self]
        stack = [self]
        seen = {self}
        while stack:
            node = stack.pop()
            for n in node.neighbors:
                if n not in seen:
                    traversed.append(n)
                    seen.add(n)
                    stack.append(n)
        return traversed

    # def __hash__(self):
    #     return hash(frozenset(self._traverse()))

    # def __eq__(self, other):
    #     if not isinstance(other, type(self)):
    #         return NotImplemented
    #     return hash(frozenset(self._traverse())) == hash(frozenset(other._traverse()))

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.val}"


def test():
    graph = UndirectedGraph()
    graph.adjacency_list = dict(enumerate(map(list, [[2, 4], [1, 3], [2, 4], [1, 3]]), start=1))
    graph2 = graph.copy()
    assert graph is not graph2
    assert graph == graph2
    node = graph.get_node(2)
    node2 = node.copy()
    graph.remove_node(2)


from typing import Hashable, Dict, Set
class UndirectedGraph:
    # store graph as a adjacency last to storage in memory; ensures that it is hashable
    # https://www.python.org/doc/essays/graphs/
    Node = GraphNode

    def __init__(self):  # nodes: List[GraphNode] = None
        self._adjacency_list = {}
        self._nodes = {}

    @property
    def nodes(self):
        # yield from self._nodes.values()
        return list(self._nodes.values())

    @property
    def adjacency_list(self):
        return {self._nodes[k]: {self._nodes[n] for n in v} for k, v in self._adjacency_list.items()}

    @adjacency_list.setter
    def adjacency_list(self, adjacency_list: Dict[Hashable, Iterable]):
        for source, neighbors in adjacency_list.items():
            for sink in neighbors:
                self.add_edge(source, sink)

    def get_node(self, node) -> GraphNode:
        return self._nodes.get(node)

    def add_node(self, node):
        if node not in self._nodes:
            self._nodes[node] = self.Node(node)
            self._adjacency_list[node] = set()

    def add_nodes(self, *nodes):
        for node in nodes:
            self.add_node(node)

    def remove_node(self, node):
        node = self._nodes.pop(node)
        neighbors = self._adjacency_list.pop(node)
        for neighbor in neighbors:
            self._adjacency_list[neighbor].remove(node)
        # return node

    def add_edge(self, source, sink):
        self.add_nodes(source, sink)
        if self._nodes[sink] not in self._nodes[source].neighbors:
            self._nodes[source].neighbors.append(self._nodes[sink])
        if self._nodes[source] not in self._nodes[sink].neighbors:
            self._nodes[sink].neighbors.append(self._nodes[source])
        self._adjacency_list[source].add(sink)
        self._adjacency_list[sink].add(source)

    def copy(self):
        return deepcopy(self)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self._adjacency_list == other._adjacency_list and self._nodes.keys() == other._nodes.keys()

    def __repr__(self):
        return f"{self.__class__.__name__}: {self._adjacency_list}"


# heap
import heapq


# class MaxHeap:
#     def __init__(self, values=None):
#         self.heap = []
#         (values := values if values else []) or any(self.push(value) for value in values)
#
#     def _invert(self, value):
#         return -value if isinstance(value, int) else tuple(-v for v in value)
#
#     def push(self, value) -> None:
#         heapq.heappush(self.heap, self._invert(value))
#
#     def pop(self):
#         return self._invert(heapq.heappop(self.heap))
#
#     def __len__(self) -> int:
#         return len(self.heap)
#
#     def __getitem__(self, i: int):
#         return self.heap[i]
#
#
# # timer
# def timeit(method: callable):
#     def timed(*args, **kwargs):
#         start = time.time()
#         result = method(*args, **kwargs)
#         end = time.time()
#         print(method.__name__, end - start)
#         return result
#     return timed


# Other
def auto_win():
    """
    This script was used to read and write the output from leetcode problem: 2. Add Two Numbers
    """
    f = open("user.out", "w")
    lines = __Utils__().read_lines()
    trash = {91: None, 93: None, 44: None, 10: None}
    while True:
        try:
            param_1 = int(next(lines).translate(trash)[::-1])
            param_2 = int(next(lines).translate(trash)[::-1])
            f.writelines(("[", ",".join(str(param_1 + param_2))[::-1], "]\n",))
        except StopIteration:
            break
    exit()
