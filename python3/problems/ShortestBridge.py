### Source : https://leetcode.com/problems/shortest-bridge/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-08

##################################################################################################### 
#
# In a given 2D binary array grid, there are two islands.  (An island is a 4-directionally connected 
# group of 1s not connected to any other 1s.)
# 
# Now, we may change 0s to 1s so as to connect the two islands together to form 1 island.
# 
# Return the smallest number of 0s that must be flipped.  (It is guaranteed that the answer is at 
# least 1.)
# 
# Example 1:
# 
# Input: grid = [[0,1],[1,0]]
# Output: 1
# 
# Example 2:
# 
# Input: grid = [[0,1,0],[0,0,0],[0,0,1]]
# Output: 2
# 
# Example 3:
# 
# Input: grid = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
# Output: 1
# 
# Constraints:
# 
# 	2 <= grid.length == grid[0].length <= 100
# 	grid[i][j] == 0 or grid[i][j] == 1
#####################################################################################################

from typing import List


class Solution:
    def shortestBridge(self, grid: List[List[int]]) -> int:  # O(n^2) time -> TLE: 88 / 96 test cases passed.

        def dfs(i: int, j: int, new: bool):
            if new:
                islands.append(set())
            if 0 <= i < len(grid) and 0 <= j < len(grid[0]) and grid[i][j]:
                islands[-1].add((i, j))
                grid[i][j] = 0
                for x, y in [(i, j + 1), (i + 1, j), (i, j - 1), (i - 1, j)]:
                    dfs(x, y, False)

        def distance(i1: int, j1: int, i2: int, j2: int) -> int:
            return abs(i1 - i2) + abs(j1 - j2) - 1

        islands = []
        any(dfs(i, j, True) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j])
        return min(distance(*coord1, *coord2) for coord1 in islands[0] for coord2 in islands[1])

    def shortestBridge(self, grid: List[List[int]]) -> int:
        # find one of the islands using dfs, then expand it till you reach the next using bfs
        def dfs(i: int, j: int):
            if 0 <= i < len(grid) and 0 <= j < len(grid[0]) and grid[i][j] and (i, j) not in seen:
                seen.add((i, j))
                for next_ij in [(i, j + 1), (i + 1, j), (i, j - 1), (i - 1, j)]:
                    dfs(*next_ij)

        seen = set()
        dfs(*next((i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j]))
        queue, shortest = list(seen), 0
        while queue:
            new_queue = []
            for (i, j) in queue:
                for x, y in [(i, j + 1), (i + 1, j), (i, j - 1), (i - 1, j)]:
                    if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and (x, y) not in seen:
                        if grid[x][y]:
                            return shortest
                        seen.add((x, y))
                        new_queue.append((x, y))
            queue = new_queue
            shortest += 1


def test():
    arguments = [
        [[0, 1], [1, 0]],
        [[0, 1, 0], [0, 0, 0], [0, 0, 1]],
        [[1, 1, 1, 1, 1], [1, 0, 0, 0, 1], [1, 0, 1, 0, 1], [1, 0, 0, 0, 1], [1, 1, 1, 1, 1]],
    ]
    expectations = [1, 2, 1]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().shortestBridge(grid)
        assert solution == expected, (solution, expected)
