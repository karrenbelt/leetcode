### Source : https://leetcode.com/problems/restore-ip-addresses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given a string s containing only digits, return all possible valid IP addresses that can be 
# obtained from s. You can return them in any order.
# 
# A valid IP address consists of exactly four integers, each integer is between 0 and 255, separated 
# by single dots and cannot have leading zeros. For example, "0.1.2.201" and "192.168.1.1" are valid 
# IP addresses and "0.011.255.245", "192.168.1.312" and "192.168@1.1" are invalid IP addresses. 
# 
# Example 1:
# Input: s = "25525511135"
# Output: ["255.255.11.135","255.255.111.35"]
# Example 2:
# Input: s = "0000"
# Output: ["0.0.0.0"]
# Example 3:
# Input: s = "1111"
# Output: ["1.1.1.1"]
# Example 4:
# Input: s = "010010"
# Output: ["0.10.0.10","0.100.1.0"]
# Example 5:
# Input: s = "101023"
# Output: ["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
# 
# Constraints:
# 
# 	0 <= s.length <= 3000
# 	s consists of digits only.
#####################################################################################################

from typing import List


class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:

        def backtracking(i, path: List[str]):
            if len(path) == 4 and i == len(s):
                paths.append(".".join(path))
            elif len(path) > 4:
                return
            else:
                for j in range(i, min(i + 3, len(s))):
                    if s[i] == '0' and j > i or int(s[i:j + 1]) > 255:
                        continue
                    backtracking(j + 1, path + [s[i:j + 1]])

        paths = []
        backtracking(0, [])
        return paths

    def restoreIpAddresses(self, s: str) -> List[str]:
        paths = []
        for a in range(1, 4):
            for b in range(1, 4):
                for c in range(1, 4):
                    for d in range(1, 4):
                        if a + b + c + d == len(s):
                            parts = (s[0: a], s[a: a + b], s[a + b: a + b + c], s[a + b + c: a + b + c + d])
                            if all(map(lambda x: int(x) <= 255 and x[0] != '0' if len(x) > 1 else True, parts)):
                                paths.append('.'.join(parts))
        return paths


def test():
    arguments = [
        "25525511135",
        "0000",
        "1111",
        "010010",
        "101023",
        ]
    expectations = [
        ["255.255.11.135", "255.255.111.35"],
        ["0.0.0.0"],
        ["1.1.1.1"],
        ["0.10.0.10", "0.100.1.0"],
        ["1.0.10.23", "1.0.102.3", "10.1.0.23", "10.10.2.3", "101.0.2.3"],
        ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().restoreIpAddresses(s)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
