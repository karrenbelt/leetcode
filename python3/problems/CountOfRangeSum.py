### Source : https://leetcode.com/problems/count-of-range-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-02

##################################################################################################### 
#
# Given an integer array nums and two integers lower and upper, return the number of range sums that 
# lie in [lower, upper] inclusive.
# 
# Range sum S(i, j) is defined as the sum of the elements in nums between indices i and j inclusive, 
# where i <= j.
# 
# Example 1:
# 
# Input: nums = [-2,5,-1], lower = -2, upper = 2
# Output: 3
# Explanation: The three ranges are: [0,0], [2,2], and [0,2] and their respective sums are: -2, -1, 2.
# 
# Example 2:
# 
# Input: nums = [0], lower = 0, upper = 0
# Output: 1
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-231 <= nums[i] <= 231 - 1
# 	-105 <= lower <= upper <= 105
# 	The answer is guaranteed to fit in a 32-bit integer.
#####################################################################################################

from typing import List


class Solution:
    # there exist other solutions to explore:
    # Fenwick tree (binary indexed tree), segmentation tree, red-black tree (binary search tree), merge sort
    def countRangeSum(self, nums: List[int], lower: int, upper: int) -> int:  # O(n^2) time and O(n^2) space
        return sum(lower <= sum(nums[i: j]) <= upper for i in range(len(nums)) for j in range(i + 1, len(nums) + 1))

    def countRangeSum(self, nums: List[int], lower: int, upper: int) -> int:  # O(n * (upper - lower)) time
        # prefix sum and hashmap
        dp = [0]
        for n in nums:  # O(n) time
            dp.append(dp[-1] + n)

        seen, ctr = {}, 0
        for prefix_sum in dp:  # O(n) time
            for target in range(lower, upper + 1):  # O(upper - lower + 1) time
                ctr += seen.get(prefix_sum - target, 0)
            seen[prefix_sum] = seen.get(prefix_sum, 0) + 1
        return ctr

    def countRangeSum(self, nums: List[int], lower: int, upper: int) -> int:  # O(n log n) time
        import bisect
        ctr = prefix = 0
        seen = [0]
        for x in nums:
            prefix += x
            left = bisect.bisect_left(seen, prefix - upper)
            right = bisect.bisect_left(seen, prefix - lower + 1)
            ctr += right - left
            bisect.insort(seen, prefix)
        return ctr


def test():
    arguments = [
        ([-2, 5, -1], -2, 2),
        ([0], 0, 0),
    ]
    expectations = [3, 1]
    for (nums, lower, upper), expected in zip(arguments, expectations):
        solution = Solution().countRangeSum(nums, lower, upper)
        assert solution == expected
