### Source : https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
# 
# According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between two 
# nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node 
# to be a descendant of itself).&rdquo;
# 
# Example 1:
# 
# Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
# Output: 3
# Explanation: The LCA of nodes 5 and 1 is 3.
# 
# Example 2:
# 
# Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
# Output: 5
# Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a descendant of itself according to 
# the LCA definition.
# 
# Example 3:
# 
# Input: root = [1,2], p = 1, q = 2
# Output: 1
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 105].
# 	-109 <= Node.val <= 109
# 	All Node.val are unique.
# 	p != q
# 	p and q will exist in the tree.
#####################################################################################################


from python3 import BinaryTreeNode as TreeNode


class Solution:
    class Solution:
        def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
            # find p and q using bfs or dfs in the tree
            # store path from root to p and q in the process
            # see where these paths intersect
            def dfs(root, p, q):
                if root is None or root == q or root == p:
                    return root
                left = dfs(root.left, p, q)
                right = dfs(root.right, p, q)
                if right and left:
                    return root
                if right:
                    return right
                if left:
                    return left
                return None

            return dfs(root, p, q)

    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if root is None or root == q or root == p:
            return root
        left = self.lowestCommonAncestor(root.left, p, q)
        right = self.lowestCommonAncestor(root.right, p, q)
        return root if left and right else (left or right)

    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':  # O(n) time

        def dfs(node: TreeNode) -> TreeNode:
            if not node or node == q or node == p:
                return node
            left = dfs(node.left)
            right = dfs(node.right)
            return node if left and right else (left or right)

        return dfs(root)

    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':  # O(n) time

        stack = [root]
        parents = {root: None}
        while p not in parents or q not in parents:
            node = stack.pop()
            if node.left:
                parents[node.left] = node
                stack.append(node.left)
            if node.right:
                parents[node.right] = node
                stack.append(node.right)

        ancestors = set()
        while p:
            ancestors.add(p)
            p = parents[p]
        while q not in ancestors:
            q = parents[q]
        return q


def test():
    from python3 import Codec
    serialized_trees = [
        ("[3,5,1,6,2,0,8,null,null,7,4]", 5, 1),
        ("[3,5,1,6,2,0,8,null,null,7,4]", 5, 4),
        ("[1,2]", 1, 2),
        ]
    expectations = [3, 5, 1]
    for (serialized_tree, p_val, q_val), expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        p, q = (node for node in root if node.val == p_val or node.val == q_val)
        solution = Solution().lowestCommonAncestor(root, p, q)
        assert solution.val == expected
