### Source : https://leetcode.com/problems/delete-operation-for-two-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-07

##################################################################################################### 
#
# Given two strings word1 and word2, return the minimum number of steps required to make word1 and 
# word2 the same.
# 
# In one step, you can delete exactly one character in either string.
# 
# Example 1:
# 
# Input: word1 = "sea", word2 = "eat"
# Output: 2
# Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
# 
# Example 2:
# 
# Input: word1 = "leetcode", word2 = "etco"
# Output: 4
# 
# Constraints:
# 
# 	1 <= word1.length, word2.length <= 500
# 	word1 and word2 consist of only lowercase English letters.
#####################################################################################################


class Solution:
    def minDistance(self, word1: str, word2: str) -> int:  # top-down dp: O(m * n) time
        import functools

        @functools.lru_cache(None)
        def dfs(i: int, j: int):
            if i == len(word1) or j == len(word2):
                return max(len(word1) - i, len(word2) - j)
            if word1[i] == word2[j]:
                return dfs(i + 1, j + 1)
            return 1 + min(dfs(i + 1, j), dfs(i, j + 1))

        return dfs(0, 0)

    def minDistance(self, word1: str, word2: str) -> int:  # bottom-up dp: O(m * n) time and O(m * n) space
        dp = [[0] * (len(word2) + 1) for _ in range(len(word1) + 1)]
        for i in range(1, len(word1) + 1):
            dp[i][0] = i
        for j in range(1, len(word2) + 1):
            dp[0][j] = j
        for i in range(1, len(word1) + 1):
            for j in range(1, len(word2) + 1):
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = 1 + min(dp[i - 1][j], dp[i][j - 1])
        return dp[-1][-1]

    def minDistance(self, word1: str, word2: str) -> int:  # bottom-up dp: O(m * n) time and O(m * n) space
        dp = [[0] * (len(word2) + 1) for _ in range(len(word1) + 1)]
        for i, c1 in enumerate(word1, 1):
            for j, c2 in enumerate(word2, 1):
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1], dp[i - 1][j - 1] + (c1 == c2))
        return len(word1) + len(word2) - 2 * dp[-1][-1]


def test():
    arguments = [
        ("sea", "eat"),
        ("leetcode", "etco"),
        ("plasma", "altruism"),
    ]
    expectations = [2, 4, 8]
    for (word1, word2), expected in zip(arguments, expectations):
        solution = Solution().minDistance(word1, word2)
        assert solution == expected
