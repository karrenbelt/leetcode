### Source : https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given inorder and postorder traversal of a tree, construct the binary tree.
# 
# Note:
# You may assume that duplicates do not exist in the tree.
# 
# For example, given
# 
# inorder = [9,3,15,20,7]
# postorder = [9,15,7,20,3]
# 
# Return the following binary tree:
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        if not inorder:
            return None
        root_index = inorder.index(postorder.pop())
        root = TreeNode(inorder[root_index])
        root.left = self.buildTree(inorder[:root_index], postorder[:root_index])
        root.right = self.buildTree(inorder[root_index + 1:], postorder[root_index:-1])
        return root

    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:

        def traverse(left: int, right: int):
            if left > right:
                return None
            val = postorder.pop()
            node = TreeNode(val)
            index = idx_map[val]
            node.right = traverse(index + 1, right)
            node.left = traverse(left, index - 1)
            return node

        idx_map = {n: i for i, n in enumerate(inorder)}
        return traverse(0, len(inorder) - 1)


def test():
    from python3 import Codec
    arrays_of_numbers = [
        ([9, 3, 15, 20, 7], [9, 15, 7, 20, 3]),
        ([-1], [-1]),
        ]
    expectations = [
        "[3,9,20,null,null,15,7]",
        "[-1]",
        ]
    for (inorder, postorder), expected in zip(arrays_of_numbers, expectations):
        solution = Solution().buildTree(inorder, postorder)
        assert solution == Codec.deserialize(expected)
