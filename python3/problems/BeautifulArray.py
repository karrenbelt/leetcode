### Source : https://leetcode.com/problems/beautiful-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-28

##################################################################################################### 
#
# For some fixed n, an array nums is beautiful if it is a permutation of the integers 1, 2, ..., n, 
# such that:
# 
# For every i < j, there is no k with i < k < j such that nums[k] * 2 = nums[i] + nums[j].
# 
# Given n, return any beautiful array nums.  (It is guaranteed that one exists.)
# 
# Example 1:
# 
# Input: n = 4
# Output: [2,1,4,3]
# 
# Example 2:
# 
# Input: n = 5
# Output: [3,1,2,5,4]
# 
# Note:
# 
# 	1 <= n <= 1000
# 
#####################################################################################################

from typing import List


class Solution:
    def beautifulArray(self, n: int) -> List[int]:  # O(n) time and O(n) space

        def dfs(nums) -> List[int]:
            if len(nums) < 3:
                return nums
            return dfs(nums[::2]) + dfs(nums[1::2])

        return dfs(list(range(1, n + 1)))

    def beautifulArray(self, n: int) -> List[int]:  # O(n) time
        nums = [1]
        while len(nums) < n:
            odd, even = [], []
            for num in nums:
                if 2 * num - 1 <= n:
                    odd.append(2 * num - 1)
                if 2 * num <= n:
                    even.append(2 * num)
            nums = odd + even
        return nums

    def beautifulArray(self, n: int) -> List[int]:  # O(n) time
        from collections import deque
        nums = deque([1])
        while len(nums) < n:
            next_nums = deque()
            for num in nums:
                if 2 * num - 1 <= n:
                    next_nums.append(2 * num - 1)
                if 2 * num <= n:
                    next_nums.appendleft(2 * num)
            nums = next_nums
        return list(nums)

    def beautifulArray(self, n: int):  # O(n log n) time
        return sorted(range(1, n + 1), key=lambda x: bin(x)[:1:-1])


def test():
    arguments = [4, 5, 9]
    expectations = [
        [2, 1, 4, 3],
        [3, 1, 2, 5, 4],
        [1, 9, 5, 3, 7, 2, 6, 4, 8],
    ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().beautifulArray(n)
        assert solution

