### Source : https://leetcode.com/problems/univalued-binary-tree/
### Author : Karrenbelt
### Date   : 2020-04-14

#####################################################################################################
#
# A binary tree is univalued if every node in the tree has the same value.
#
# Return true if and only if the given tree is univalued.
#
# Example 1:
#
# Input: [1,1,1,1,1,null,1]
# Output: true
#
# Example 2:
#
# Input: [2,2,2,5,2]
# Output: false
#
# Note:
#
# 	The number of nodes in the given tree will be in the range [1, 100].
# 	Each node's value will be an integer in the range [0, 99].
#
#####################################################################################################

from python3 import  BinaryTreeNode as TreeNode


class Solution:
    def isUnivalTree(self, root: TreeNode) -> bool:  # O(n) time and O(log n) space

        def dfs(node):
            return not node or node.val == root.val and dfs(node.left) and dfs(node.right)

        return dfs(root)

    def isUnivalTree(self, root: TreeNode) -> bool:  # O(n) time and O(1) space

        def dfs(node: TreeNode) -> TreeNode:  # generator for early stopping
            if node:
                yield node
                yield from dfs(node.left)
                yield from dfs(node.right)

        return all(node.val == root.val for node in dfs(root))

    def isUnivalTree(self, root: TreeNode) -> bool:  # O(n) time and O(log n) space
        stack = [root]  # bfs
        while stack:
            node = stack.pop()
            if node.val != root.val:
                return False
            if node.left:
                stack.append(node.left)
            if node.right:
                stack.append(node.right)
        return True


def test():
    from python3 import Codec
    arguments = [
        "[1,1,1,1,1,null,1]",
        "[2,2,2,5,2]",
    ]
    expectations = [True, False]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().isUnivalTree(root)
        assert solution == expected
