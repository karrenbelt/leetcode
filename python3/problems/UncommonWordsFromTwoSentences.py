### Source : https://leetcode.com/problems/uncommon-words-from-two-sentences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-07

##################################################################################################### 
#
# A sentence is a string of single-space separated words where each word consists only of lowercase 
# letters.
# 
# A word is uncommon if it appears exactly once in one of the sentences, and does not appear in the 
# other sentence.
# 
# Given two sentences s1 and s2, return a list of all the uncommon words. You may return the answer 
# in any order.
# 
# Example 1:
# Input: s1 = "this apple is sweet", s2 = "this apple is sour"
# Output: ["sweet","sour"]
# Example 2:
# Input: s1 = "apple apple", s2 = "banana"
# Output: ["banana"]
# 
# Constraints:
# 
# 	1 <= s1.length, s2.length <= 200
# 	s1 and s2 consist of lowercase English letters and spaces.
# 	s1 and s2 do not have leading or trailing spaces.
# 	All the words in s1 and s2 are separated by a single space.
#####################################################################################################

from typing import List


class Solution:
    def uncommonFromSentences(self, s1: str, s2: str) -> List[str]:  # O(n) time
        counts = {}
        for word in (word for words in (s1, s2) for word in words.split()):
            counts[word] = counts.get(word, 0) + 1
        return [word for word, count in counts.items() if count == 1]

    def uncommonFromSentences(self, s1: str, s2: str) -> List[str]:
        seen, single = set(), set()
        for word in (word for words in (s1, s2) for word in words.split()):
            if word in single:
                single.remove(word)
            if word not in seen:
                single.add(word)
            seen.add(word)
        return list(single)


def test():
    arguments = [
        ("this apple is sweet", "this apple is sour"),
        ("apple apple", "banana"),
    ]
    expectations = [
        ["sweet", "sour"],
        ["banana"]
    ]
    for (s1, s2), expected in zip(arguments, expectations):
        solution = Solution().uncommonFromSentences(s1, s2)
        assert solution == expected
