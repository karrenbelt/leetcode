### Source : https://leetcode.com/problems/magic-squares-in-grid/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-11

##################################################################################################### 
#
# A 3 x 3 magic square is a 3 x 3 grid filled with distinct numbers from 1 to 9 such that each row, 
# column, and both diagonals all have the same sum.
# 
# Given a row x col grid of integers, how many 3 x 3 "magic square" subgrids are there?  (Each 
# subgrid is contiguous).
# 
# Example 1:
# 
# Input: grid = [[4,3,8,4],[9,5,1,9],[2,7,6,2]]
# Output: 1
# Explanation: 
# The following subgrid is a 3 x 3 magic square:
# 
# while this one is not:
# 
# In total, there is only one magic square inside the given grid.
# 
# Example 2:
# 
# Input: grid = [[8]]
# Output: 0
# 
# Example 3:
# 
# Input: grid = [[4,4],[3,3]]
# Output: 0
# 
# Example 4:
# 
# Input: grid = [[4,7,8],[9,5,1],[2,3,6]]
# Output: 0
# 
# Constraints:
# 
# 	row == grid.length
# 	col == grid[i].length
# 	1 <= row, col <= 10
# 	0 <= grid[i][j] <= 15
#####################################################################################################

from typing import List


class Solution:
    def numMagicSquaresInside(self, grid: List[List[int]]) -> int:
        # there are only two configurations of the 3 x 3 square that meet this conditions.
        # we also need to consider all 90 degree rotations of these two squares
        magic_squares = [
            [[4, 3, 8],
             [9, 5, 1],
             [2, 7, 6]],

            [[4, 9, 2],
             [3, 5, 7],
             [8, 1, 6]],
        ]

        all_rotations = []
        for square in magic_squares:
            all_rotations.append(square)
            for _ in range(3):
                all_rotations.append([list(row[::-1]) for row in zip(*all_rotations[-1])])

        ctr = 0
        for i in range(len(grid) - 2):
            for j in range(len(grid[0]) - 2):
                square = [[grid[x][y] for y in range(j, j + 3)] for x in range(i, i + 3)]
                ctr += square in all_rotations
        return ctr


def test():
    arguments = [
        [[4, 3, 8, 4], [9, 5, 1, 9], [2, 7, 6, 2]],
        [[8]],
        [[4, 4], [3, 3]],
        [[4, 7, 8], [9, 5, 1], [2, 3, 6]],
        [[8, 1, 6], [3, 5, 7], [4, 9, 2]],
    ]
    expectations = [1, 0, 0, 0, 1]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().numMagicSquaresInside(grid)
        assert solution == expected
