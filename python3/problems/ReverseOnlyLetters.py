### Source : https://leetcode.com/problems/reverse-only-letters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-14

##################################################################################################### 
#
# Given a string s, reverse the string according to the following rules:
# 
# 	All the characters that are not English letters remain in the same position.
# 	All the English letters (lowercase or uppercase) should be reversed.
# 
# Return s after reversing it.
# 
# Example 1:
# Input: s = "ab-cd"
# Output: "dc-ba"
# Example 2:
# Input: s = "a-bC-dEf-ghIj"
# Output: "j-Ih-gfE-dCba"
# Example 3:
# Input: s = "Test1ng-Leet=code-Q!"
# Output: "Qedo1ct-eeLg=ntse-T!"
# 
# Constraints:
# 
# 	1 <= s.length <= 100
# 	s consists of characters with ASCII values in the range [33, 122].
# 	s does not contain '\"' or '\\'.
#####################################################################################################


class Solution:
    def reverseOnlyLetters(self, s: str) -> str:
        letters = iter(c for c in reversed(s) if c.isalpha())
        return ''.join(c if not c.isalpha() else next(letters) for c in s)

    def reverseOnlyLetters(self, s: str) -> str:
        letters = [c for c in s if c.isalpha()]
        return ''.join(c if not c.isalpha() else letters.pop() for c in s)


def test():
    arguments = [
        "ab-cd",
        "a-bC-dEf-ghIj",
        "Test1ng-Leet=code-Q!",
    ]
    expectations = [
        "dc-ba",
        "j-Ih-gfE-dCba",
        "Qedo1ct-eeLg=ntse-T!",
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().reverseOnlyLetters(s)
        assert solution == expected
