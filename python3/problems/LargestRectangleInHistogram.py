### Source : https://leetcode.com/problems/largest-rectangle-in-histogram/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-13

##################################################################################################### 
#
# Given n non-negative integers representing the histogram's bar height where the width of each bar 
# is 1, find the area of largest rectangle in the histogram.
# 
# Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].
# 
# The largest rectangle is shown in the shaded area, which has area = 10 unit.
# 
# Example:
# 
# Input: [2,1,5,6,2,3]
# Output: 10
# 
#####################################################################################################

from typing import List


class Solution:

    def largestRectangleArea(self, heights: List[int]) -> int:  # TLE: O(n^2)
        max_area = 0
        for h in set(heights):
            w = 0
            max_width = 1
            for i in range(len(heights)):
                if heights[i] >= h:
                    w += 1
                else:
                    max_width = max(w, max_width)
                    w = 0
            max_width = max(w, max_width)
            max_area = max(max_area, h * max_width)
        return max_area

    def largestRectangleArea(self, heights: List[int]) -> int:  # O(n)
        # left[i], right[i] represent how many bars are >= than the current bar
        left = [1] * len(heights)
        right = [1] * len(heights)
        max_rect = 0

        for i in range(0, len(heights)):
            j = i - 1
            while j >= 0:
                if heights[j] >= heights[i]:
                    left[i] += left[j]
                    j -= left[j]
                else:
                    break

        for i in range(len(heights) - 1, -1, -1):
            j = i + 1
            while j < len(heights):
                if heights[j] >= heights[i]:
                    right[i] += right[j]
                    j += right[j]
                else:
                    break

        for i in range(len(heights)):
            max_rect = max(max_rect, heights[i] * (left[i] + right[i] - 1))

        return max_rect

    def largestRectangleArea(self, heights: List[int]) -> int:  # segmentation tree
        raise NotImplementedError()

    def largestRectangleArea(self, heights: List[int]) -> int:  # O(n)
        max_area = 0
        stack = []
        p = 0
        while p < len(heights):
            if not stack or heights[p] >= heights[stack[-1]]:
                stack.append(p)
                p += 1
            else:
                h = heights[stack.pop()]
                left = stack[-1] if stack else -1
                area = (p - left - 1) * h
                max_area = max(max_area, area)

        while stack:
            h = heights[stack.pop()]
            left = stack[-1] if stack else -1
            area = (len(heights) - left - 1) * h
            max_area = max(max_area, area)

        return max_area

    def largestRectangleArea(self, heights: List[int]) -> int:  # O(n) time and O(n) space
        # 1. Add to stack if curr value is equal or bigger than the top of stack
        # 2. Keep removing from stack till a number which is smaller or equal than current is found
        # 3. Calculate every time you remove:
        #    - if stack is empty: area = input[top] * i
        #    - else: area = input[top] * (i - stack_top - 1)
        heights.append(0)
        top, max_area, i = 0, 0, 0
        stack = []
        while i < len(heights):
            while stack and heights[stack[-1]] > heights[i]:
                top = stack.pop()
                if not stack:
                    max_area = max(heights[top] * i, max_area)
                else:
                    max_area = max(heights[top] * (i - stack[-1] - 1), max_area)
            stack.append(i)
            i += 1

        return max_area

    def largestRectangleArea(self, heights: List[int]) -> int:
        stack = [-1, 0]
        heights.append(0)
        max_area = 0
        for i in range(1, len(heights)):
            while heights[i] < heights[stack[-1]]:
                h = heights[stack.pop()]
                w = i - stack[-1] - 1
                max_area = max(max_area, h * w)
            stack.append(i)
        return max_area

    def largestRectangleArea(self, heights: List[int]) -> int:

        def get_mid_area(heights, left, mid, right):
            i = mid
            j = mid + 1
            min_height = min(heights[i], heights[j])
            area = min_height * 2
            while i >= left and j <= right:
                min_height = min(min_height, min(heights[i], heights[j]))
                area = max(area, min_height * (j - i + 1))
                if i == left:
                    j += 1
                elif j == right:
                    i -= 1
                elif heights[i - 1] >= heights[j + 1]:
                    i -= 1
                else:
                    j += 1
            return area

        def get_max_area(heights, left, right):
            if left == right:
                return heights[left]
            mid = left + (right - left) // 2
            area1 = get_max_area(heights, left, mid)
            area2 = get_max_area(heights, mid + 1, right)
            area3 = get_mid_area(heights, left, mid, right)
            return max(area1, area2, area3)

        return get_max_area(heights, 0, len(heights) - 1) if heights else 0

    def largestRectangleArea(self, heights: List[int]) -> int:
        heights.append(0)
        stack = [-1]
        ans = 0
        for i in range(len(heights)):
            while heights[i] < heights[stack[-1]]:
                h = heights[stack.pop()]
                w = i - stack[-1] - 1
                ans = max(ans, h * w)
            stack.append(i)
        heights.pop()
        return ans


def test():
    arrays_of_numbers = [
        [2, 1, 5, 6, 2, 3],
        [2, 4],
        ]
    expectations = [10, 4]
    for heights, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().largestRectangleArea(heights)
        assert solution == expected
