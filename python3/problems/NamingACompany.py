### Source : https://leetcode.com/problems/naming-a-company/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-09

##################################################################################################### 
#
# You are given an array of strings ideas that represents a list of names to be used in the process 
# of naming a company. The process of naming a company is as follows:
# 
# 	Choose 2 distinct names from ideas, call them ideaA and ideaB.
# 	Swap the first letters of ideaA and ideaB with each other.
# 	If both of the new names are not found in the original ideas, then the name ideaA ideaB 
# (the concatenation of ideaA and ideaB, separated by a space) is a valid company name.
# 	Otherwise, it is not a valid name.
# 
# Return the number of distinct valid names for the company.
# 
# Example 1:
# 
# Input: ideas = ["coffee","donuts","time","toffee"]
# Output: 6
# Explanation: The following selections are valid:
# - ("coffee", "donuts"): The company name created is "doffee conuts".
# - ("donuts", "coffee"): The company name created is "conuts doffee".
# - ("donuts", "time"): The company name created is "tonuts dime".
# - ("donuts", "toffee"): The company name created is "tonuts doffee".
# - ("time", "donuts"): The company name created is "dime tonuts".
# - ("toffee", "donuts"): The company name created is "doffee tonuts".
# Therefore, there are a total of 6 distinct company names.
# 
# The following are some examples of invalid selections:
# - ("coffee", "time"): The name "toffee" formed after swapping already exists in the original array.
# - ("time", "toffee"): Both names are still the same after swapping and exist in the original array.
# - ("coffee", "toffee"): Both names formed after swapping already exist in the original array.
# 
# Example 2:
# 
# Input: ideas = ["lack","back"]
# Output: 0
# Explanation: There are no valid selections. Therefore, 0 is returned.
# 
# Constraints:
# 
# 	2 <= ideas.length <= 5 * 104
# 	1 <= ideas[i].length <= 10
# 	ideas[i] consists of lowercase English letters.
# 	All the strings in ideas are unique.
#####################################################################################################

from typing import List


class Solution:
    def distinctNames(self, ideas: List[str]) -> int:  # 74 / 89 testcases passed
        from itertools import combinations, product

        def count(words1: set[str], words2: set[str]) -> int:
            n = 0
            for w1, w2 in product(words1, words2):
                new_w1, new_w2 = w2[0] + w1[1:], w1[0] + w2[1:]
                if new_w1 not in existing and new_w2 not in existing:
                    n += 2
            return n

        existing, by_first_letter = set(ideas), dict()
        for word in ideas:
            by_first_letter.setdefault(word[0], set()).add(word)

        ctr = 0
        for k1_words, k2_words in combinations(by_first_letter.values(), 2):
            ctr += count(k1_words, k2_words)

        return ctr

    def distinctNames(self, ideas: List[str]) -> int:  # O(n * 26^2) time and O(n) space
        from itertools import combinations

        ctr, by_first_letter = 0, dict()

        for word in ideas:
            by_first_letter.setdefault(word[0], set()).add(word[1:])

        for suffixes1, suffixes2 in combinations(by_first_letter.values(), 2):
            count = len(suffixes1 & suffixes2)
            ctr += 2 * (len(suffixes1) - count) * (len(suffixes2) - count)

        return ctr


def test():
    arguments = [
        ["coffee", "donuts", "time", "toffee"],
        ["lack", "back"],
    ]
    expectations = [6, 0]
    for ideas, expected in zip(arguments, expectations):
        solution = Solution().distinctNames(ideas)
        assert solution == expected
