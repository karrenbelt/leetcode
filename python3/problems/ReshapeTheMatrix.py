### Source : https://leetcode.com/problems/reshape-the-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-05

##################################################################################################### 
#
# In ATLAB, there is a handy function called reshape which can reshape an m x n matrix into a new 
# one with a different size r x c keeping its original data.
# 
# You are given an m x n matrix mat and two integers r and c representing the row number and column 
# number of the wanted reshaped matrix.
# 
# The reshaped matrix should be filled with all the elements of the original matrix in the same 
# row-traversing order as they were.
# 
# If the reshape operation with given parameters is possible and legal, output the new reshaped 
# matrix; Otherwise, output the original matrix.
# 
# Example 1:
# 
# Input: mat = [[1,2],[3,4]], r = 1, c = 4
# Output: [[1,2,3,4]]
# 
# Example 2:
# 
# Input: mat = [[1,2],[3,4]], r = 2, c = 4
# Output: [[1,2],[3,4]]
# 
# Constraints:
# 
# 	m == mat.length
# 	n == mat[i].length
# 	1 <= m, n <= 100
# 	-1000 <= mat[i][j] <= 1000
# 	1 <= r, c <= 300
#####################################################################################################

from typing import List


class Solution:
    def matrixReshape(self, mat: List[List[int]], r: int, c: int) -> List[List[int]]:  # 80 ms
        if len(mat) * len(mat[0]) == r * c:
            values = (v for row in mat for v in row)
            return [[next(values) for _ in range(c)] for _ in range(r)]
        return mat

    def matrixReshape(self, mat: List[List[int]], r: int, c: int) -> List[List[int]]:  # 68 ms
        return list(map(list, zip(*([(v for row in mat for v in row)]*c)))) if len(mat) * len(mat[0]) == r * c else mat


def test():
    arguments = [
        ([[1, 2], [3, 4]], 1, 4),
        ([[1, 2], [3, 4]], 2, 4),
    ]
    expectations = [
        [[1, 2, 3, 4]],
        [[1, 2], [3, 4]],
    ]
    for (mat, r, c), expected in zip(arguments, expectations):
        solution = Solution().matrixReshape(mat, r, c)
        assert solution == expected
test()