### Source : https://leetcode.com/problems/insert-interval/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-11

##################################################################################################### 
#
# Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if 
# necessary).
# 
# You may assume that the intervals were initially sorted according to their start times.
# 
# Example 1:
# 
# Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
# Output: [[1,5],[6,9]]
# 
# Example 2:
# 
# Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
# Output: [[1,2],[3,10],[12,16]]
# Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].
# 
# Example 3:
# 
# Input: intervals = [], newInterval = [5,7]
# Output: [[5,7]]
# 
# Example 4:
# 
# Input: intervals = [[1,5]], newInterval = [2,3]
# Output: [[1,5]]
# 
# Example 5:
# 
# Input: intervals = [[1,5]], newInterval = [2,7]
# Output: [[1,7]]
# 
# Constraints:
# 
# 	0 <= intervals.length <= 104
# 	intervals[i].length == 2
# 	0 <= intervals[i][0] <= intervals[i][1] <= 105
# 	intervals is sorted by intervals[i][0] in ascending order.
# 	newInterval.length == 2
# 	0 <= newInterval[0] <= newInterval[1] <= 105
#####################################################################################################

from typing import List


class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:  # O(n) time O(n) space
        # already sorted and non-overlapping; our new interval might overlap with zero to many
        start, end = newInterval
        left, right = [], []
        for interval in intervals:
            if interval[-1] < start:
                left += interval,
            elif interval[0] > end:
                right += interval,
            else:
                start = min(start, interval[0])
                end = max(end, interval[-1])
        return left + [[start, end]] + right

    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:  # O(n) time O(1) space
        # exactly the same as MergeIntervals.py (leetcode 56), except we append the new interval first
        intervals.append(newInterval)
        from python3.problems.MergeIntervals import Solution as MergeIntervals
        return MergeIntervals().merge(intervals)

    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:  # O(log n) time
        # since the intervals are sorted by default, we should consider binary search
        def binary_search(array: List, target: int):
            left, right = 0, len(array) - 1
            while left <= right:
                mid = left + (right - left) // 2
                if array[mid][1] < target:
                    left = mid + 1
                elif array[mid][0] > target:
                    right = mid - 1
                else:
                    return mid
            return left

        # we need to consider all these corner cases now
        if not intervals:
            return [newInterval]
        elif newInterval[0] > intervals[-1][-1]:
            return intervals + [newInterval]
        elif newInterval[-1] < intervals[0][0]:
            return [newInterval] + intervals

        left = binary_search(intervals, newInterval[0])
        right = binary_search(intervals, newInterval[1])
        if right == len(intervals) or intervals[right][0] > newInterval[1]:
            right -= 1
        middle = [min(intervals[left][0], newInterval[0]), max(intervals[right][1], newInterval[1])]
        return intervals[:left] + [middle] + intervals[right + 1:]


def test():
    arguments = [
        ([[1, 3], [6, 9]], [2, 5]),
        ([[1, 2], [3, 5], [6, 7], [8, 10], [12, 16]], [4, 8]),
        ([], [5, 7]),
        ([[1, 5]], [2, 3]),
        ([[1, 5]], [2, 7]),
        ]
    expectations = [
        [[1, 5], [6, 9]],
        [[1, 2], [3, 10], [12, 16]],
        [[5, 7]],
        [[1, 5]],
        [[1, 7]],
        ]
    for (intervals, newInterval), expected in zip(arguments, expectations):
        solution = Solution().insert(intervals, newInterval)
        assert solution == expected
