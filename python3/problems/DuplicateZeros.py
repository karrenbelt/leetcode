### Source : https://leetcode.com/problems/duplicate-zeros/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-26

##################################################################################################### 
#
# Given a fixed length array arr of integers, duplicate each occurrence of zero, shifting the 
# remaining elements to the right.
# 
# Note that elements beyond the length of the original array are not written.
# 
# Do the above modifications to the input array in place, do not return anything from your function.
# 
# Example 1:
# 
# Input: [1,0,2,3,0,4,5,0]
# Output: null
# Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]
# 
# Example 2:
# 
# Input: [1,2,3]
# Output: null
# Explanation: After calling your function, the input array is modified to: [1,2,3]
# 
# Note:
# 
# 	1 <= arr.length <= 10000
# 	0 <= arr[i] <= 9
#####################################################################################################
from typing import List


class Solution:
    def duplicateZeros(self, arr: List[int]) -> None:  # O(n^2) time and O(n) space
        """
        Do not return anything, modify arr in-place instead.
        """
        i = 0
        while i < len(arr):
            if arr[i] == 0:
                arr.insert(i, 0) or arr.pop()
                i += 1
            i += 1

    def duplicateZeros(self, arr: List[int]) -> None:  # O(n^2) time and O(n) space
        arr[:] = [n for seq in [[n] * 2 if not n else [n] for n in arr] for n in seq][:len(arr)]

    def duplicateZeros(self, arr: List[int]) -> None:  # O(n) time and O(1) space
        zeroes = arr.count(0)
        for i in range(len(arr) - 1, -1, -1):
            if i + zeroes < len(arr):
                arr[i + zeroes] = arr[i]
            if arr[i] == 0:
                zeroes -= 1
                if i + zeroes < len(arr):
                    arr[i + zeroes] = 0


def test():
    arguments = [
        [1, 0, 2, 3, 0, 4, 5, 0],
        [1, 2, 3],
    ]
    expectations = [
        [1, 0, 0, 2, 3, 0, 0, 4],
        [1, 2, 3]
    ]
    for arr, expected in zip(arguments, expectations):
        Solution().duplicateZeros(arr)
        assert arr == expected
