### Source : https://leetcode.com/problems/vowel-spellchecker/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-22

##################################################################################################### 
#
# Given a wordlist, we want to implement a spellchecker that converts a query word into a correct 
# word.
# 
# For a given query word, the spell checker handles two categories of spelling mistakes:
# 
# 	Capitalization: If the query matches a word in the wordlist (case-insensitive), then the 
# query word is returned with the same case as the case in the wordlist.
# 
# 		Example: wordlist = ["yellow"], query = "YellOw": correct = "yellow"
# 		Example: wordlist = ["Yellow"], query = "yellow": correct = "Yellow"
# 		Example: wordlist = ["yellow"], query = "yellow": correct = "yellow"
# 
# 	Vowel Errors: If after replacing the vowels ('a', 'e', 'i', 'o', 'u') of the query word 
# with any vowel individually, it matches a word in the wordlist (case-insensitive), then the query 
# word is returned with the same case as the match in the wordlist.
# 
# 		Example: wordlist = ["YellOw"], query = "yollow": correct = "YellOw"
# 		Example: wordlist = ["YellOw"], query = "yeellow": correct = "" (no match)
# 		Example: wordlist = ["YellOw"], query = "yllw": correct = "" (no match)
# 
# In addition, the spell checker operates under the following precedence rules:
# 
# 	When the query exactly matches a word in the wordlist (case-sensitive), you should return 
# the same word back.
# 	When the query matches a word up to capitlization, you should return the first such match 
# in the wordlist.
# 	When the query matches a word up to vowel errors, you should return the first such match in 
# the wordlist.
# 	If the query has no matches in the wordlist, you should return the empty string.
# 
# Given some queries, return a list of words answer, where answer[i] is the correct word for query = 
# queries[i].
# 
# Example 1:
# 
# Input: wordlist = ["KiTe","kite","hare","Hare"], queries = 
# ["kite","Kite","KiTe","Hare","HARE","Hear","hear","keti","keet","keto"]
# Output: ["kite","KiTe","KiTe","Hare","hare","","","KiTe","","KiTe"]
# 
# Note:
# 
# 	1 <= wordlist.length <= 5000
# 	1 <= queries.length <= 5000
# 	1 <= wordlist[i].length <= 7
# 	1 <= queries[i].length <= 7
# 	All strings in wordlist and queries consist only of english letters.
# 
#####################################################################################################

from typing import List


class Solution:
    def spellchecker(self, wordlist: List[str], queries: List[str]) -> List[str]:  # TODO: I guess I misunderstood
        # we construct a Trie to look up any queries
        # we query as follows, in order of precedence:
        # - return case sensitive, if possible
        # - else return case-insensitive match, if possible
        # - return vowel insensitive match, if possible

        def build_trie(words: List[str]) -> dict:
            trie = {}
            for i, word in enumerate(words):
                node = trie
                for c in word:
                    if c not in node:
                        node[c] = {}
                    node = node[c]
                node['.'] = i
            return trie

        # def backtracking(word: str, node: dict, i: int = 0, path: str = ''):
        #     if i == len(word):
        #         return path if '.' in node else ''
        #     letter = word[i]
        #     if letter in node:
        #         return backtracking(word, node[letter], i + 1, path + letter)
        #     elif letter.lower() in node:
        #         return backtracking(word, node[letter.lower()], i + 1, path + letter.lower())
        #     elif letter.upper() in node:
        #         return backtracking(word, node[letter.upper()], i + 1, path + letter.upper())
        #     elif letter in vowels:
        #         for vowel in vowels:
        #             if vowel in node:
        #                 return backtracking(word, node[vowel], i + 1, path + vowel)
        #     return ''

        def backtracking(word: str, node: dict, i: int = 0, path: str = ''):
            if i == len(word):
                if '.' in node:
                    matches.add((i, path))
                return
            letter = word[i]
            if letter in node:
                backtracking(word, node[letter], i + 1, path + letter)
            if letter.lower() in node:
                backtracking(word, node[letter.lower()], i + 1, path + letter.lower())
            if letter.upper() in node:
                backtracking(word, node[letter.upper()], i + 1, path + letter.upper())
            if letter in vowels:
                for vowel in vowels:
                    if vowel in node:
                        backtracking(word, node[vowel], i + 1, path + vowel)

        answer = []
        vowels = set('aeiouAEIOU')
        trie = build_trie(wordlist)
        for word in queries:
            matches = set()
            backtracking(word, trie)
            answer.append(sorted(matches, key=lambda x: x[0])[0][1] if matches else '')
        return answer  # [backtracking(word, trie) for word in queries]

    def spellchecker(self, wordlist: List[str], queries: List[str]) -> List[str]:

        def mask(w: str) -> str:
            return "".join('*' if c in 'aeiou' else c for c in w.lower())

        def solve(query: str) -> str:
            if query in d0:
                return query
            if query.lower() in d1:
                return d1[query.lower()]
            if mask(query) in d2:
                return d2[mask(query)]
            return ""

        d0 = set(wordlist)
        d1 = {w.lower(): w for w in wordlist[::-1]}
        d2 = {mask(w): w for w in wordlist[::-1]}

        return [solve(q) for q in queries]

    def spellchecker(self, wordlist: List[str], queries: List[str]) -> List[str]:

        def mask(w: str) -> str:
            return "".join('*' if c in 'aeiou' else c for c in w.lower())

        def solve(query: str) -> str:
            return query if query in d0 else d1.get(query.lower(), d2.get(mask(query), ""))

        d0, d1, d2 = set(wordlist), {w.lower(): w for w in wordlist[::-1]}, {mask(w): w for w in wordlist[::-1]}
        return list(map(solve, queries))


def test():
    arguments = [
        (["KiTe", "kite", "hare", "Hare"],
         ["kite", "Kite", "KiTe", "Hare", "HARE", "Hear", "hear", "keti", "keet", "keto"]),
        ]
    expectations = [
        ["kite", "KiTe", "KiTe", "Hare", "hare", "", "", "KiTe", "", "KiTe"],
        ]
    for (wordlist, queries), expected in zip(arguments, expectations):
        solution = Solution().spellchecker(wordlist, queries)
        assert solution == expected
