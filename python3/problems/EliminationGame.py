### Source : https://leetcode.com/problems/elimination-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-14

#####################################################################################################
#
# You have a list arr of all integers in the range [1, n] sorted in a strictly increasing order.
# Apply the following algorithm on arr:
#
# 	Starting from left to right, remove the first number and every other number afterward until
# you reach the end of the list.
# 	Repeat the previous step again, but this time from right to left, remove the rightmost
# number and every other number from the remaining numbers.
# 	Keep repeating the steps again, alternating left to right and right to left, until a single
# number remains.
#
# Given the integer n, return the last number that remains in arr.
#
# Example 1:
#
# Input: n = 9
# Output: 6
# Explanation:
# arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# arr = [2, 4, 6, 8]
# arr = [2, 6]
# arr = [6]
#
# Example 2:
#
# Input: n = 1
# Output: 1
#
# Constraints:
#
# 	1 <= n <= 109
#####################################################################################################


class Solution:
    def lastRemaining(self, n: int) -> int:  # brute for, should be TLE
        array, left_to_right = list(range(1, n + 1)), True
        while len(array) > 1:
            array = array[left_to_right::2]
            left_to_right ^= True
        return array[0]

    def lastRemaining(self, n: int) -> int:  # O(log n) time and O(1) space
        # can find the formula: f(n) = 2(1 + n / 2 - f(n / 2))
        return 1 if n == 1 else 2 * (1 + n // 2 - self.lastRemaining(n // 2))


def test():
    arguments = [9, 1]
    expectations = [6, 1]
    for n, expected in zip(arguments, expectations):
        solution = Solution().lastRemaining(n)
        assert solution == expected
