### Source : https://leetcode.com/problems/find-common-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# Given an array A of strings made only from lowercase letters, return a list of all characters that 
# show up in all strings within the list (including duplicates).  For example, if a character occurs 
# 3 times in all strings but not 4 times, you need to include that character three times in the final 
# answer.
# 
# You may return the answer in any order.
# 
# Example 1:
# 
# Input: ["bella","label","roller"]
# Output: ["e","l","l"]
# 
# Example 2:
# 
# Input: ["cool","lock","cook"]
# Output: ["c","o"]
# 
# Note:
# 
# 	1 <= A.length <= 100
# 	1 <= A[i].length <= 100
# 	A[i][j] is a lowercase letter
# 
#####################################################################################################

from typing import List


class Solution:
    def commonChars(self, A: List[str]) -> List[str]:  # this was my first solution 10 months ago
        ans = []
        for c in list('abcdefghijklmnopqrstuvwxyz'):
            ctr = 100
            for w in A:
                ctr = min(ctr, w.count(c))
            ans.extend([c] * ctr)
        return ans

    def commonChars(self, A: List[str]) -> List[str]:
        from functools import reduce
        from collections import Counter
        return [k for k, v in reduce(lambda x, y: x & y, map(Counter, A)).items() for _ in range(v)]

    def commonChars(self, A: List[str]) -> List[str]:  # should do this without str to list conversion
        return list(''.join(c * min(w.count(c) for w in A) for c in set(A[0])))

    def commonChars(self, A: List[str]) -> List[str]:
        return [c for c in set(A[0]) for _ in range(min(w.count(c) for w in A))]

    def commonChars(self, A: List[str]) -> List[str]:  # whatever
        return [c for c in chars for _ in range(min(w.count(c) for w in A))] if (chars := set(A[0])) else []


def test():
    from collections import Counter
    arguments = [
        ["bella", "label", "roller"],
        ["cool", "lock", "cook"],
    ]
    expectations = [
        ["e", "l", "l"],
        ["c", "o"],
    ]
    for A, expected in zip(arguments, expectations):
        solution = Solution().commonChars(A)
        assert Counter(solution) == Counter(expected), solution

