### Source : https://leetcode.com/problems/game-of-life/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-01

##################################################################################################### 
#
# According to Wikipedia's article: "The Game of Life, also known simply as Life, is a cellular 
# automaton devised by the British mathematician John Horton Conway in 1970."
# 
# The board is made up of an m x n grid of cells, where each cell has an initial state: live 
# (represented by a 1) or dead (represented by a 0). Each cell interacts with its eight neighbors 
# (horizontal, vertical, diagonal) using the following four rules (taken from the above Wikipedia 
# article):
# 
# 	Any live cell with fewer than two live neighbors dies as if caused by under-population.
# 	Any live cell with two or three live neighbors lives on to the next generation.
# 	Any live cell with more than three live neighbors dies, as if by over-population.
# 	Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
# 
# The next state is created by applying the above rules simultaneously to every cell in the current 
# state, where births and deaths occur simultaneously. Given the current state of the m x n grid 
# board, return the next state.
# 
# Example 1:
# 
# Input: board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
# Output: [[0,0,0],[1,0,1],[0,1,1],[0,1,0]]
# 
# Example 2:
# 
# Input: board = [[1,1],[1,0]]
# Output: [[1,1],[1,1]]
# 
# Constraints:
# 
# 	m == board.length
# 	n == board[i].length
# 	1 <= m, n <= 25
# 	board[i][j] is 0 or 1.
# 
# Follow up:
# 
# 	Could you solve it in-place? Remember that the board needs to be updated simultaneously: 
# You cannot update some cells first and then use their updated values to update other cells.
# 	In this question, we represent the board using a 2D array. In principle, the board is 
# infinite, which would cause problems when the active area encroaches upon the border of the array 
# (i.e., live cells reach the border). How would you address these problems?
#####################################################################################################

from typing import List


class Solution:
    def gameOfLife(self, board: List[List[int]]) -> None:  # O(m * n) time
        """
        Do not return anything, modify board in-place instead.
        """
        def count_neighbors(i: int, j: int):
            ctr = 0
            for x, y in directions:
                r, c = i + x, j + y
                if 0 <= r < len(board) and 0 <= c < len(board[0]) and board[r][c]:
                    ctr += 1
            return ctr

        directions = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, 1), (1, -1), (-1, -1)]
        to_be_flipped = []
        for i in range(len(board)):
            for j in range(len(board[0])):
                n = count_neighbors(i, j)
                if board[i][j] and (n < 2 or n > 3):
                    to_be_flipped.append((i, j))
                elif not board[i][j] and n == 3:
                    to_be_flipped.append((i, j))

        for i, j in to_be_flipped:
            board[i][j] = 1 - board[i][j]

    def gameOfLife(self, board: List[List[int]]) -> None:  # O(n * m) time and O(1) space

        def count_neighbors(i, j):
            return sum(1 for x in (i - 1, i, i + 1) for y in (j - 1, j, j + 1)
                       if 0 <= x < len(board) and 0 <= y < len(board[0]) and board[x][y] > 0)

        for i in range(len(board)):
            for j in range(len(board[0])):
                board[i][j] = count_neighbors(i, j) if board[i][j] else - count_neighbors(i, j)
        for i in range(len(board)):
            for j in range(len(board[0])):
                board[i][j] = 1 if board[i][j] in {3, 4, -3} else 0


def test():
    arguments = [
        [[0, 1, 0], [0, 0, 1], [1, 1, 1], [0, 0, 0]],
        [[1, 1], [1, 0]],
        ]
    expectations = [
        [[0, 0, 0], [1, 0, 1], [0, 1, 1], [0, 1, 0]],
        [[1, 1], [1, 1]],
        ]
    for board, expected in zip(arguments, expectations):
        # 1/0
        Solution().gameOfLife(board)
        assert board == expected
test()