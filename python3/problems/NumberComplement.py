### Source : https://leetcode.com/problems/number-complement/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# Given a positive integer num, output its complement number. The complement strategy is to flip the 
# bits of its binary representation.
# 
# Example 1:
# 
# Input: num = 5
# Output: 2
# Explanation: The binary representation of 5 is 101 (no leading zero bits), and its complement is 
# 010. So you need to output 2.
# 
# Example 2:
# 
# Input: num = 1
# Output: 0
# Explanation: The binary representation of 1 is 1 (no leading zero bits), and its complement is 0. 
# So you need to output 0.
# 
# Constraints:
# 
# 	The given integer num is guaranteed to fit within the range of a 32-bit signed integer.
# 	num >= 1
# 	You could assume no leading zero bit in the integer&rsquo;s binary representation.
# 	This question is the same as 1009: 
# https://leetcode.com/problems/complement-of-base-10-integer/
#####################################################################################################


class Solution:
    # This question is the same as 1009: https://leetcode.com/problems/complement-of-base-10-integer/
    def findComplement(self, num: int) -> int:  # 36 ms
        return num ^ int('1' * num.bit_length(), 2) if num else 1

    def findComplement(self, num: int) -> int:  # 48 ms
        return num ^ ((1 << num.bit_length()) - 1) if num else 1


def test():
    arguments = [5, 1, 0]
    expectations = [2, 0, 1]
    for num, expected in zip(arguments, expectations):
        solution = Solution().findComplement(num)
        assert solution == expected
