### Source : https://leetcode.com/problems/subarray-sum-equals-k/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-22

##################################################################################################### 
#
# Given an array of integers nums and an integer k, return the total number of continuous subarrays 
# whose sum equals to k.
# 
# Example 1:
# Input: nums = [1,1,1], k = 2
# Output: 2
# Example 2:
# Input: nums = [1,2,3], k = 3
# Output: 2
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	-1000 <= nums[i] <= 1000
# 	-107 <= k <= 107
#####################################################################################################
##################################################################################################### 
#
# Given an array of integers and an integer k, you need to find the total number of continuous 
# subarrays whose sum equals to k.
# 
# Example 1:
# 
# Input:nums = [1,1,1], k = 2
# Output: 2
# 
# Note:
# 
# The length of the array is in range [1, 20,000].
# The range of numbers in the array is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].
# 
#####################################################################################################
from typing import List


class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:  # O(n) time and O(n) space
        # hash table prevent having to brute force - we cache how often we encountered a specific sum
        cum_counts = dict()
        ctr = current = 0
        for i in range(len(nums)):
            current += nums[i]
            ctr += int(current == k) + cum_counts.get(current - k, 0)
            cum_counts[current] = cum_counts.get(current, 0) + 1
        return ctr


def test():
    arguments = [
        ([1, 1, 1], 2),
        ([1, 2, 3], 3),
        ([10, 2, -2, -20, 10], -10)
        ]
    expectations = [2, 2, 3]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().subarraySum(nums, k)
        assert solution == expected
