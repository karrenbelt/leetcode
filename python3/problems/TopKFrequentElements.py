### Source : https://leetcode.com/problems/top-k-frequent-elements/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given a non-empty array of integers, return the k most frequent elements.
# 
# Example 1:
# 
# Input: nums = [1,1,1,2,2,3], k = 2
# Output: [1,2]
# 
# Example 2:
# 
# Input: nums = [1], k = 1
# Output: [1]
# 
# Note: 
# 
# 	You may assume k is always valid, 1 &le; k &le; number of unique elements.
# 	Your algorithm's time complexity must be better than O(n log n), where n is the array's 
# size.
# 	It's guaranteed that the answer is unique, in other words the set of the top k frequent 
# elements is unique.
# 	You can return the answer in any order.
#####################################################################################################

from typing import List


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        d = {}
        for n in nums:
            d[n] = d[n] + 1 if n in d else 1
        return [k for k, v in sorted(d.items(), key=lambda x: x[1], reverse=True)[:k]]


def test():
    arrays_of_numbers = [
        [1, 1, 1, 2, 2, 3],
        [1],
        ]
    top_ks = [2, 1]
    expectations = [
        [1, 2],
        [1],
        ]
    for nums, k, expected in zip(arrays_of_numbers, top_ks, expectations):
        solution = Solution().topKFrequent(nums, k)
        assert solution == expected
