### Source : https://leetcode.com/problems/my-calendar-i/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-10

##################################################################################################### 
#
# Implement a yCalendar class to store your events. A new event can be added if adding the event 
# will not cause a double booking.
# 
# Your class will have the method, book(int start, int end). Formally, this represents a booking on 
# the half open interval [start, end), the range of real numbers x such that start <= x < end.
# 
# A double booking happens when two events have some non-empty intersection (ie., there is some time 
# that is common to both events.)
# 
# For each call to the method yCalendar.book, return true if the event can be added to the calendar 
# successfully without causing a double booking. Otherwise, return false and do not add the event to 
# the calendar.
# Your class will be called like this: yCalendar cal = new yCalendar(); yCalendar.book(start, end)
# 
# Example 1:
# 
# yCalendar();
# yCalendar.book(10, 20); // returns true
# yCalendar.book(15, 25); // returns false
# yCalendar.book(20, 30); // returns true
# Explanation: 
# The first event can be booked.  The second can't because time 15 is already booked by another event.
# The third event can be booked, as the first event takes every time less than 20, but not including 
# 20.
# 
# Note:
# 
# 	The number of calls to yCalendar.book per test case will be at most 1000.
# 	In calls to yCalendar.book(start, end), start and end are integers in the range [0, 109].
# 
#####################################################################################################


# class Event:
#
#     def __init__(self, start: int, end: int):
#         self.start = start
#         self.end = end


class MyCalendar:
    def __init__(self):
        self.events = []

    def book(self, start: int, end: int) -> bool:
        return False if any(start < e and end > s for s, e in self.events) else self.events.append((start, end)) or True


class MyCalendar:
    def __init__(self):
        self.events = [(-1 << 31, -1 << 31), ((1 << 31) - 1, (1 << 31) - 1)]

    def book(self, start: int, end: int) -> bool:
        import bisect
        left = bisect.bisect_left(self.events, (start, end))
        if self.events[left - 1][1] <= start and self.events[left][0] >= end:
            return bisect.insort_left(self.events, (start, end)) or True
        return False


class MyCalendar:
    def __init__(self):
        self.events = []

    def book(self, start: int, end: int) -> bool:
        import bisect
        i = bisect.bisect_right(self.events, start)
        if i % 2 or i != bisect.bisect_left(self.events, end):
            return False
        self.events[i:i] = [start, end]
        return True

# Your MyCalendar object will be instantiated and called as such:
# obj = MyCalendar()
# param_1 = obj.book(start,end)


def test():
    operations = ["MyCalendar", "book", "book", "book"]
    arguments = [[], (10, 20), (15, 25), (20, 30)]
    expectations = [None, True, False, True]
    obj = MyCalendar(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
