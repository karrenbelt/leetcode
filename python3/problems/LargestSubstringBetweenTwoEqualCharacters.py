### Source : https://leetcode.com/problems/largest-substring-between-two-equal-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-21

##################################################################################################### 
#
# Given a string s, return the length of the longest substring between two equal characters, 
# excluding the two characters. If there is no such substring return -1.
# 
# A substring is a contiguous sequence of characters within a string.
# 
# Example 1:
# 
# Input: s = "aa"
# Output: 0
# Explanation: The optimal substring here is an empty substring between the two 'a's.
# 
# Example 2:
# 
# Input: s = "abca"
# Output: 2
# Explanation: The optimal substring here is "bc".
# 
# Example 3:
# 
# Input: s = "cbzxy"
# Output: -1
# Explanation: There are no characters that appear twice in s.
# 
# Example 4:
# 
# Input: s = "cabbac"
# Output: 4
# Explanation: The optimal substring here is "abba". Other non-optimal substrings include "bb" and "".
# 
# Constraints:
# 
# 	1 <= s.length <= 300
# 	s contains only lowercase English letters.
#####################################################################################################


class Solution:
    def maxLengthBetweenEqualCharacters(self, s: str) -> int:  # O(n) time and O(n) space
        maximum, d = -1, {}
        for i, c in enumerate(s):
            if c in d:
                maximum = max(maximum, i - d[c] - 1)
            else:
                d[c] = i
        return maximum

    def maxLengthBetweenEqualCharacters(self, s: str) -> int:  # O(n^2) time and O(1) space
        return max(s.rfind(c) - s.find(c) - 1 for c in set(s))  # can use .rindex and .index as well

    def maxLengthBetweenEqualCharacters(self, s: str) -> int:  # O(n) time and O(n) space
        return max(a - b for a, b in zip(range(len(s)), map({}.setdefault, s, range(1, len(s) + 1))))


def test():
    arguments = ["aa", "abca", "cbzxy", "cabbac"]
    expectations = [0, 2, -1, 4]
    for s, expected in zip(arguments, expectations):
        solution = Solution().maxLengthBetweenEqualCharacters(s)
        assert solution == expected, (solution, expected)
