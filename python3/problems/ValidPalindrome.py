### Source : https://leetcode.com/problems/valid-palindrome/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-30

##################################################################################################### 
#
# Given a string, determine if it is a palindrome, considering only alphanumeric characters and 
# ignoring cases.
# 
# Note: For the purpose of this problem, we define empty string as valid palindrome.
# 
# Example 1:
# 
# Input: "A man, a plan, a canal: Panama"
# Output: true
# 
# Example 2:
# 
# Input: "race a car"
# Output: false
# 
# Constraints:
# 
# 	s consists only of printable ASCII characters.
#####################################################################################################


class Solution:
    def isPalindrome(self, s: str) -> bool:
        alnum = list(filter(str.isalnum, s.lower()))
        return all([alnum[i] == alnum[-i-1] for i in range(len(alnum)//2)])


def test():
    strings = [
        "A man, a plan, a canal: Panama",
        "race a car",
        ]
    expectations = [True, False]
    for s, expected in zip(strings, expectations):
        solution = Solution().isPalindrome(s)
        assert solution == expected
