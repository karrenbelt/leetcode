### Source : https://leetcode.com/problems/validate-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-26

##################################################################################################### 
#
# Given the root of a binary tree, determine if it is a valid binary search tree (BST).
# 
# A valid BST is defined as follows:
# 
# 	The left subtree of a node contains only nodes with keys less than the node's key.
# 	The right subtree of a node contains only nodes with keys greater than the node's key.
# 	Both the left and right subtrees must also be binary search trees.
# 
# Example 1:
# 
# Input: root = [2,1,3]
# Output: true
# 
# Example 2:
# 
# Input: root = [5,1,4,null,null,3,6]
# Output: false
# Explanation: The root node's value is 5 but its right child's value is 4.
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-231 <= Node.val <= 231 - 1
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isValidBST(self, root: TreeNode) -> bool:  # O(n) time and O(n) space

        def traverse(node):
            if node:
                traverse(node.left)
                order.append(node.val)
                traverse(node.right)

        order = []
        traverse(root)
        return not any(order[i - 1] >= order[i] for i in range(1, len(order)))

    def isValidBST(self, root: TreeNode) -> bool:  # O(n) time and O(h) space

        def dfs(node, low, high):
            if not node:
                return True
            if not low < node.val < high:
                return False
            return dfs(node.left, low, node.val) and dfs(node.right, node.val, high)

        return dfs(root, -float("inf"), float("inf"))

    def isValidBST(self, root: TreeNode, left=float('-inf'), right=float('inf')) -> bool:  # O(n) time and O(h) space
        if not root:
            return True
        if root.val <= left or root.val >= right:
            return False
        return self.isValidBST(root.left, left, root.val) and self.isValidBST(root.right, root.val, right)

    def isValidBST(self, root: TreeNode) -> bool:  # O(n) time and O(1) space

        def traverse(node: TreeNode):
            if node:
                yield from traverse(node.left)
                yield node.val
                yield from traverse(node.right)

        prev = - 1 << 32
        for next_node in traverse(root):
            if prev >= next_node:
                return False
            prev = next_node
        return True

    def isValidBST(self, root: TreeNode) -> bool:  # iterative in-order traversal
        order, stack = [], []
        node = root
        while stack or node:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            order.append(node.val)
            node = node.right

        return not any(order[i - 1] >= order[i] for i in range(1, len(order)))


def test():
    from python3 import Codec
    serialized_trees = [
        "[2,1,3]",
        "[5,1,4,null,null,3,6]",
        "[2,2,2]",
        "[-2147483648]",
    ]
    expectations = [True, False, False, True]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().isValidBST(root)
        assert solution == expected
