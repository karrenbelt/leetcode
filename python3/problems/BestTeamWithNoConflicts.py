### Source : https://leetcode.com/problems/best-team-with-no-conflicts/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-02

##################################################################################################### 
#
# You are the manager of a basketball team. For the upcoming tournament, you want to choose the team 
# with the highest overall score. The score of the team is the sum of scores of all the players in 
# the team.
# 
# However, the basketball team is not allowed to have conflicts. A conflict exists if a younger 
# player has a strictly higher score than an older player. A conflict does not occur between players 
# of the same age.
# 
# Given two lists, scores and ages, where each scores[i] and ages[i] represents the score and age of 
# the ith player, respectively, return the highest overall score of all possible basketball teams.
# 
# Example 1:
# 
# Input: scores = [1,3,5,10,15], ages = [1,2,3,4,5]
# Output: 34
# Explanation: You can choose all the players.
# 
# Example 2:
# 
# Input: scores = [4,5,6,5], ages = [2,1,2,1]
# Output: 16
# Explanation: It is best to choose the last 3 players. Notice that you are allowed to choose 
# multiple people of the same age.
# 
# Example 3:
# 
# Input: scores = [1,2,3,5], ages = [8,9,10,1]
# Output: 6
# Explanation: It is best to choose the first 3 players. 
# 
# Constraints:
# 
# 	1 <= scores.length, ages.length <= 1000
# 	scores.length == ages.length
# 	1 <= scores[i] <= 106
# 	1 <= ages[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def bestTeamScore(self, scores: List[int], ages: List[int]) -> int:  # O(n^2) time and O(n) space
        candidates = sorted(zip(ages, scores))
        dp = [0] * len(scores)
        for i in range(len(scores)):
            dp[i] = candidates[i][1]
            for j in range(i):
                if candidates[i][1] >= candidates[j][1]:
                    dp[i] = max(dp[i], candidates[i][1] + dp[j])
        return max(dp)

    def bestTeamScore(self, scores: List[int], ages: List[int]) -> int:
        dp = [0] * (max(ages) + 1)
        for score, age in sorted(zip(scores, ages)):
            dp[age] = score + max(dp[:age + 1])
        return max(dp)


def test():
    arguments = [
        ([1, 3, 5, 10, 15], [1, 2, 3, 4, 5]),
        ([4, 5, 6, 5], [2, 1, 2, 1]),
        ([1, 2, 3, 5], [8, 9, 10, 1]),
    ]
    expectations = [34, 16, 6]
    for (scores, ages), expected in zip(arguments, expectations):
        solution = Solution().bestTeamScore(scores, ages)
        assert solution == expected
