### Source : https://leetcode.com/problems/erect-the-fence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-07

##################################################################################################### 
#
# You are given an array trees where trees[i] = [xi, yi] represents the location of a tree in the 
# garden.
# 
# You are asked to fence the entire garden using the minimum length of rope as it is expensive. The 
# garden is well fenced only if all the trees are enclosed.
# 
# Return the coordinates of trees that are exactly located on the fence perimeter.
# 
# Example 1:
# 
# Input: points = [[1,1],[2,2],[2,0],[2,4],[3,3],[4,2]]
# Output: [[1,1],[2,0],[3,3],[2,4],[4,2]]
# 
# Example 2:
# 
# Input: points = [[1,2],[2,2],[4,2]]
# Output: [[4,2],[2,2],[1,2]]
# 
# Constraints:
# 
# 	1 <= points.length <= 3000
# 	points[i].length == 2
# 	0 <= xi, yi <= 100
# 	All the given points are unique.
#####################################################################################################

from typing import List


class Point:

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"{self.__class__.__name__}: ({self.x}, {self.y})"

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return (self.x, self.y) < (other.x, other.y)

    def __getitem__(self, item):
        return (self.x, self.y)[item]

    def __hash__(self):
        return hash((self.x, self.y))


class Polygon:

    def __init__(self, points: List[List[int]]):
        self.points = list(map(lambda p: Point(*p), points))

    @property
    def convex_hull(self):
        # https://www.oreilly.com/library/view/python-cookbook/0596001673/ch17s19.html

        def cross_product(p1, p2, p3):
            return ((p2.x - p1.x) * (p3.y - p1.y)) - ((p2.y - p1.y) * (p3.x - p1.x))

        def build(points):
            hull = []
            for p in points:  # if cross-product is negative we have a clockwise rotation
                while len(hull) >= 2 and cross_product(hull[-2], hull[-1], p) < 0:
                    hull.pop()
                hull.append(p)
            return hull

        points = sorted(self.points)
        return set(build(points) + build(reversed(points)))


class Solution:
    def outerTrees(self, trees: List[List[int]]) -> List[List[int]]:
        # we have some series of point and need to determine the convex hull.
        # in order to do so we have to determine whether the line formed by 3 points rotates counter-clockwise
        if len(trees) <= 3:
            return trees
        polygon = Polygon(trees)
        return [[p.x, p.y] for p in polygon.convex_hull]


def test():
    from collections import Counter
    arguments = [
        [[1, 1], [2, 2], [2, 0], [2, 4], [3, 3], [4, 2]],
        [[1, 2], [2, 2], [4, 2]],
    ]
    expectations = [
        [[1, 1], [2, 0], [3, 3], [2, 4], [4, 2]],
        [[4, 2], [2, 2], [1, 2]],
    ]
    for trees, expected in zip(arguments, expectations):
        solution = Solution().outerTrees(trees)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected)), (expected, solution)
