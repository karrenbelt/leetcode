### Source : https://leetcode.com/problems/positions-of-large-groups/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-04

##################################################################################################### 
#
# In a string s of lowercase letters, these letters form consecutive groups of the same character.
# 
# For example, a string like s = "abbxxxxzyy" has the groups "a", "bb", "xxxx", "z", and "yy".
# 
# A group is identified by an interval [start, end], where start and end denote the start and end 
# indices (inclusive) of the group. In the above example, "xxxx" has the interval [3,6].
# 
# A group is considered large if it has 3 or more characters.
# 
# Return the intervals of every large group sorted in increasing order by start index.
# 
# Example 1:
# 
# Input: s = "abbxxxxzzy"
# Output: [[3,6]]
# Explanation: "xxxx" is the only large group with start index 3 and end index 6.
# 
# Example 2:
# 
# Input: s = "abc"
# Output: []
# Explanation: We have groups "a", "b", and "c", none of which are large groups.
# 
# Example 3:
# 
# Input: s = "abcdddeeeeaabbbcd"
# Output: [[3,5],[6,9],[12,14]]
# Explanation: The large groups are "ddd", "eeee", and "bbb".
# 
# Example 4:
# 
# Input: s = "aba"
# Output: []
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s contains lower-case English letters only.
#####################################################################################################

from typing import List


class Solution:
    def largeGroupPositions(self, s: str) -> List[List[int]]:  # O(n) time
        from itertools import groupby
        i, indices = 0, []
        for c, group in groupby(s):
            size = len(list(group))
            if size >= 3:
                indices.append([i, i + size - 1])
            i += size
        return indices

    def largeGroupPositions(self, s: str) -> List[List[int]]:  # O(n) time
        i, indices = 0, []
        while i < len(s):
            j = i + 1
            while j < len(s) and s[j] == s[i]:
                j += 1
            if j - i >= 3:
                indices.append([i, j - 1])
            i = j
        return indices


def test():
    arguments = [
        "abbxxxxzzy",
        "abc",
        "abcdddeeeeaabbbcd",
    ]
    expectations = [
        [[3, 6]],
        [],
        [[3, 5], [6, 9], [12, 14]],
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().largeGroupPositions(s)
        assert solution == expected
