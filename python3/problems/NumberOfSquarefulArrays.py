### Source : https://leetcode.com/problems/number-of-squareful-arrays/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-22

##################################################################################################### 
#
# Given an array nums of non-negative integers, the array is squareful if for every pair of adjacent 
# elements, their sum is a perfect square.
# 
# Return the number of permutations of nums that are squareful.  Two permutations perm1 and perm2 
# differ if and only if there is some index i such that perm1[i] != perm2[i].
# 
# Example 1:
# 
# Input: nums = [1,17,8]
# Output: 2
# Explanation: 
# [1,8,17] and [17,8,1] are the valid permutations.
# 
# Example 2:
# 
# Input: nums = [2,2,2]
# Output: 1
# 
# Note:
# 
# 	1 <= nums.length <= 12
# 	0 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def numSquarefulPerms(self, nums: List[int]) -> int:  # O(n!) time -> TLE: 13 / 76 test cases passed.
        import itertools

        def unique_permutations(iterable):  # works but too slow when the iterable is of length > 10
            previous = tuple()
            for permutation in itertools.permutations(sorted(iterable)):
                if permutation > previous:
                    previous = permutation
                    yield permutation

        def is_squareful(array: List[int]) -> bool:
            return all(((array[i] + array[i + 1]) ** 0.5).is_integer() for i in range(len(array) - 1))

        return sum(map(is_squareful, unique_permutations(nums)))

    def numSquarefulPerms(self, nums: List[int]) -> int:  # O(n!) time
        import collections

        def backtracking(x: int, left: int = len(nums) - 1) -> int:
            counts[x] -= 1
            count = sum(backtracking(y, left - 1) for y in candidates[x] if counts[y]) if left else 1
            counts[x] += 1
            return count

        counts = collections.Counter(nums)
        candidates = {i: {j for j in counts if ((i + j) ** 0.5).is_integer()} for i in counts}
        return sum(map(backtracking, counts))


def test():
    arguments = [
        [1, 17, 8],
        [2, 2, 2],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    ]
    expectations = [2, 1, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().numSquarefulPerms(nums)
        assert solution == expected, solution
