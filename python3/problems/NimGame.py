### Source : https://leetcode.com/problems/nim-game/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-18

##################################################################################################### 
#
# You are playing the following Nim Game with your friend: There is a heap of stones on the table, 
# each time one of you take turns to remove 1 to 3 stones. The one who removes the last stone will be 
# the winner. You will take the first turn to remove the stones.
# 
# Both of you are very clever and have optimal strategies for the game. Write a function to determine 
# whether you can win the game given the number of stones in the heap.
# 
# Example:
# 
# Input: 4
# Output: false 
# Explanation: If there are 4 stones in the heap, then you will never win the game;
#              No matter 1, 2, or 3 stones you remove, the last stone will always be 
#              removed by your friend.
#####################################################################################################


class Solution:

    def canWinNim(self, n: int) -> bool:
        dp = [True] * n
        for i in range(3, n):
            dp[i] = not dp[i - 1] or not dp[i - 2] or not dp[i - 3]
        return dp[n - 1]

    def canWinNim(self, n: int) -> bool:
        return bool(n % 4)

    def canWinNim(self, n: int) -> bool:
        return bool(n & 3)


def test():
    numbers = [4, 1, 2, 1348820612]
    expectations = [False, True, True, False]
    for n, expected in zip(numbers, expectations):
        solution = Solution().canWinNim(n)
        assert solution == expected
