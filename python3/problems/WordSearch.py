### Source : https://leetcode.com/problems/word-search/
### Author : M.A.P. Karrenbelt
### Date   : 2021-01-31

##################################################################################################### 
#
# Given an m x n board and a word, find if the word exists in the grid.
# 
# The word can be constructed from letters of sequentially adjacent cells, where "adjacent" cells are 
# horizontally or vertically neighboring. The same letter cell may not be used more than once.
# 
# Example 1:
# 
# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
# Output: true
# 
# Example 2:
# 
# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
# Output: true
# 
# Example 3:
# 
# Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
# Output: false
# 
# Constraints:
# 
# 	m == board.length
# 	n = board[i].length
# 	1 <= m, n <= 200
# 	1 <= word.length <= 103
# 	board and word consists only of lowercase and uppercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        # NOTE: call any() directly, not using a list comprehension inside of it;
        # will instead use a generator and break out early

        def count(letters):
            d = {}
            for c in letters:
                d[c] = d[c] + 1 if c in d else 1
            return d

        def backtrack(i, j, suffix):
            if not suffix:
                return True
            if not -1 < i < len(board) or not -1 < j < len(board[0]) or board[i][j] != suffix[0] or used[i][j]:
                return False
            used[i][j] = True
            found = any(backtrack(i + x, j + y, suffix[1:]) for x, y in directions)
            used[i][j] = False
            return found

        # check if required letters are all present - otherwise break out early
        board_count = count(sum(board, []))
        for c, n in count(word).items():
            if board_count.get(c, 0) < n:
                return False

        directions = [(-1, 0), (0, -1), (1, 0), (0, 1)]
        used = [[False for _ in range(len(board[0]))] for _ in range(len(board))]
        for i in range(len(board)):
            for j in range(len(board[0])):
                if backtrack(i, j, word):
                    return True
        return False

    def exist(self, board: List[List[str]], word: str) -> bool:  # time: O(mn * min(3^k, mn))
        from collections import Counter

        def backtrack(i: int, j: int, suffix: str = word) -> bool:
            if suffix and 0 <= i < len(board) and 0 <= j < len(board[0]) and board[i][j] == suffix[0]:
                board[i][j] = ''
                exists = any(backtrack(x, y, suffix[1:]) for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)])
                board[i][j] = suffix[0]
                return exists
            return not suffix

        missing = Counter(word) - Counter(letter for row in board for letter in row)
        return not missing and any(backtrack(i, j) for i in range(len(board)) for j in range(len(board[0])))


def test():
    board = [
        ["A", "B", "C", "E"],
        ["S", "F", "C", "S"],
        ["A", "D", "E", "E"],
        ]
    words = ["ABCCED", "SEE", "ABCB"]
    expectations = [True, True, False]
    for word, expected in zip(words, expectations):
        solution = Solution().exist(board.copy(), word)
        assert solution == expected
