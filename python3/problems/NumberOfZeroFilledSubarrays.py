### Source : https://leetcode.com/problems/number-of-zero-filled-subarrays/
### Author : karrenbelt
### Date   : 2023-03-21

##################################################################################################### 
#
# Given an integer array nums, return the number of subarrays filled with 0.
# 
# A subarray is a contiguous non-empty sequence of elements within an array.
# 
# Example 1:
# 
# Input: nums = [1,3,0,0,2,0,0,4]
# Output: 6
# Explanation: 
# There are 4 occurrences of [0] as a subarray.
# There are 2 occurrences of [0,0] as a subarray.
# There is no occurrence of a subarray with a size more than 2 filled with 0. Therefore, we return 6.
# 
# Example 2:
# 
# Input: nums = [0,0,0,2,0,0]
# Output: 9
# Explanation:
# There are 5 occurrences of [0] as a subarray.
# There are 3 occurrences of [0,0] as a subarray.
# There is 1 occurrence of [0,0,0] as a subarray.
# There is no occurrence of a subarray with a size more than 3 filled with 0. Therefore, we return 9.
# 
# Example 3:
# 
# Input: nums = [2,10,2019]
# Output: 0
# Explanation: There is no subarray filled with 0. Therefore, we return 0.
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def zeroFilledSubarray(self, nums: List[int]) -> int:
        from itertools import groupby

        total = 0
        for n in (len(list(g)) for num, g in groupby(nums) if num == 0):
            total += (n * (n + 1)) // 2
        return total
        
    def zeroFilledSubarray(self, nums: List[int]) -> int:

        ctr = subarrays_ending_at_current_index = 0
        for n in nums:
            if n == 0:
                subarrays_ending_at_current_index += 1
                ctr += subarrays_ending_at_current_index
            else:
                subarrays_ending_at_current_index = 0

        return ctr
        
        
def test():
    arguments = [
        [1, 3, 0, 0, 2, 0, 0, 4],
        [0, 0, 0, 2, 0, 0],
        [2, 10, 2019],
    ]
    expectations = [6, 9, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().zeroFilledSubarray(nums)
        assert solution == expected, (solution, expected)
