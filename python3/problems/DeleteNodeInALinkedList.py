### Source : https://leetcode.com/problems/delete-node-in-a-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-01

##################################################################################################### 
#
# Write a function to delete a node in a singly-linked list. You will not be given access to the head 
# of the list, instead you will be given access to the node to be deleted directly.
# 
# It is guaranteed that the node to be deleted is not a tail node in the list.
# 
# Example 1:
# 
# Input: head = [4,5,1,9], node = 5
# Output: [4,1,9]
# Explanation: You are given the second node with value 5, the linked list should become 4 -> 1 -> 9 
# after calling your function.
# 
# Example 2:
# 
# Input: head = [4,5,1,9], node = 1
# Output: [4,5,9]
# Explanation: You are given the third node with value 1, the linked list should become 4 -> 5 -> 9 
# after calling your function.
# 
# Example 3:
# 
# Input: head = [1,2,3,4], node = 3
# Output: [1,2,4]
# 
# Example 4:
# 
# Input: head = [0,1], node = 0
# Output: [1]
# 
# Example 5:
# 
# Input: head = [-3,5,-99], node = -3
# Output: [5,-99]
# 
# Constraints:
# 
# 	The number of the nodes in the given list is in the range [2, 1000].
# 	-1000 <= Node.val <= 1000
# 	The value of each node in the list is unique.
# 	The node to be deleted is in the list and is not a tail node
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def deleteNode(self, node: ListNode) -> None:
        # super weird question. We move the values?
        while node.next and node.next.next:
            node.val = node.next.val
            node = node.next
        node.val = node.next.val
        node.next = None

    def deleteNode(self, node):
        node.val = node.next.val
        node.next = node.next.next


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [4, 5, 1, 9],
        [4, 5, 1, 9],
        [1, 2, 3, 4],
        [0, 1],
        [-3, 5, -99],
        ]
    targets = [5, 1, 3, 0, -3]
    expectations = [
        [4, 1, 9],
        [4, 5, 9],
        [1, 2, 4],
        [1],
        [5, -99],
        ]
    for nums, node_val, expected in zip(arrays_of_numbers, targets, expectations):
        ll = SinglyLinkedList(nums)
        node = next(node for node in ll if node.val == node_val)
        Solution().deleteNode(node)
        assert ll == SinglyLinkedList(expected)
