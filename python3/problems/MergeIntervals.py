### Source : https://leetcode.com/problems/merge-intervals/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-11

#####################################################################################################
#
# Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
# and return an array of the non-overlapping intervals that cover all the intervals in the input.
#
# Example 1:
#
# Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
# Output: [[1,6],[8,10],[15,18]]
# Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
#
# Example 2:
#
# Input: intervals = [[1,4],[4,5]]
# Output: [[1,5]]
# Explanation: Intervals [1,4] and [4,5] are considered overlapping.
#
# Constraints:
#
# 	1 <= intervals.length <= 104
# 	intervals[i].length == 2
# 	0 <= starti <= endi <= 104
#####################################################################################################

from typing import List


class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:  # O(n log n) time and O(1) space
        intervals.sort()
        i = 1
        while i < len(intervals):
            if intervals[i][0] <= intervals[i - 1][1]:
                intervals[i] = [intervals[i - 1][0], max(intervals[i][1], intervals[i - 1][1])]
                del intervals[i - 1]
            else:
                i += 1
        return intervals

    def merge(self, intervals: List[List[int]]) -> List[List[int]]:  # O(n log n) time and O(n) space
        import heapq
        merged, heap = [], intervals
        heapq.heapify(heap)
        while heap:
            interval = heapq.heappop(heap)
            if not merged or merged[-1][1] < interval[0]:
                merged.append(interval)
            else:  # overlap
                merged[-1][1] = max(interval[1], merged[-1][1])
        return merged


def test():
    arguments = [
        [[1, 3], [2, 6], [8, 10], [15, 18]],
        [[1, 4], [4, 5]],
        [[1, 4], [2, 3]],
        ]
    expectations = [
        [[1, 6], [8, 10], [15, 18]],
        [[1, 5]],
        [[1, 4]],
        ]
    for intervals, expected in zip(arguments, expectations):
        solution = Solution().merge(intervals)
        assert solution == expected, solution
