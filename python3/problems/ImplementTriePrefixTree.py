### Source : https://leetcode.com/problems/implement-trie-prefix-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-03

##################################################################################################### 
#
# Implement a trie with insert, search, and startsWith methods.
# 
# Example:
# 
# Trie trie = new Trie();
# 
# trie.insert("apple");
# trie.search("apple");   // returns true
# trie.search("app");     // returns false
# trie.startsWith("app"); // returns true
# trie.insert("app");   
# trie.search("app");     // returns true
# 
# Note:
# 
# 	You may assume that all inputs are consist of lowercase letters a-z.
# 	All inputs are guaranteed to be non-empty strings.
# 
#####################################################################################################


class Trie:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = {}

    def insert(self, word: str) -> None:
        """
        Inserts a word into the trie.
        """
        node = self.head
        for i, c in enumerate(word):
            if c not in node:
                node[c] = dict()
            node = node[c]
        node['.'] = None

    def search(self, word: str) -> bool:
        """
        Returns if the word is in the trie.
        """
        node = self.head
        for c in word:
            if c not in node:
                return False
            node = node[c]
        return '.' in node

    def startsWith(self, prefix: str) -> bool:
        """
        Returns if there is any word in the trie that starts with the given prefix.
        """
        node = self.head
        for c in prefix:
            if c not in node:
                return False
            node = node[c]
        return True


# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)


def test():
    trie = Trie()
    strings = ["apple", "apple", "app", "app", "app", "app"]
    operations = ["insert", "search", "search", "startsWith", "insert", "search"]
    expectations = [None, True, False, True, None, True]
    for word, operation, expected in zip(strings, operations, expectations):
        assert getattr(trie, operation)(word) == expected
