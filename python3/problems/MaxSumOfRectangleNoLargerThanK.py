### Source : https://leetcode.com/problems/max-sum-of-rectangle-no-larger-than-k/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-17

##################################################################################################### 
#
# Given an m x n matrix matrix and an integer k, return the max sum of a rectangle in the matrix such 
# that its sum is no larger than k.
# 
# It is guaranteed that there will be a rectangle with a sum no larger than k.
# 
# Example 1:
# 
# Input: matrix = [[1,0,1],[0,-2,3]], k = 2
# Output: 2
# Explanation: Because the sum of the blue rectangle [[0, 1], [-2, 3]] is 2, and 2 is the max number 
# no larger than k (k = 2).
# 
# Example 2:
# 
# Input: matrix = [[2,2,-1]], k = 3
# Output: 3
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 100
# 	-100 <= matrix[i][j] <= 100
# 	-105 <= k <= 105
# 
# Follow up: What if the number of rows is much larger than the number of columns?
#####################################################################################################

from typing import List


class Solution:
    # should probably do some 1D version of this or similar problems first:
    # 209. MinimumSizeSubarraySum.py
    # 862. ShortestSubarrayWithSumAtLeastK.py
    def maxSumSubmatrix(self, matrix: List[List[int]], k: int) -> int:  # O(n^2 * m^2)
        import bisect

        for i in range(len(matrix)):
            for j in range(len(matrix[0]) - 1):
                matrix[i][j + 1] += matrix[i][j]

        maximum = -1 << 31
        for i in range(len(matrix[0])):
            for j in range(i, len(matrix[0])):
                prefix_sums = [0]
                partial_sum = 0
                for c in range(len(matrix)):
                    partial_sum += matrix[c][j] - (matrix[c][i-1] if i > 0 else 0)
                    idx = bisect.bisect_left(prefix_sums, partial_sum - k)
                    if 0 <= idx < len(prefix_sums):
                        maximum = max(maximum, partial_sum - prefix_sums[idx])
                    bisect.insort(prefix_sums, partial_sum)  # O(n) time! Search is O(log n), but insert is O(n)
        return maximum


def test():
    arguments = [
        ([[1, 0, 1], [0, -2, 3]], 2),
        ([[2, 2, -1]], 3),
        ([[5, -4, -3, 4], [-3, -4, 4, 5], [5, 1, 5, -4]], 10),
        ([[5, -4, -3, 4], [-3, -4, 4, 5], [5, 1, 5, -4]], 8),
    ]
    expectations = [2, 3, 10, 8]
    for (matrix, k), expected in zip(arguments, expectations):
        solution = Solution().maxSumSubmatrix(matrix, k)
        assert solution == expected, (solution, expected)


test()

## TODO: this I stored in this file initially, must be misplaced but not sure which question it is
# class Solution:
#     def minSubarray(self, nums: List[int], p: int) -> int:  # dp: O(n) time and O(n) space
#         # record the prefix sum modulo p remainder in a hashmap linking to it's last index
#         need = sum(nums) % p
#         dp = {0: -1}
#         cur = 0
#         res = n = len(nums)
#         for i, a in enumerate(nums):
#             cur = (cur + a) % p
#             dp[cur] = i
#             if (cur - need) % p in dp:
#                 res = min(res, i - dp[(cur - need) % p])
#         return res if res < n else -1
#
# def test():
#     arguments = [
#         ([3, 1, 4, 2], 6),
#         ([6, 3, 5, 2], 9),
#         ([1, 2, 3], 3),
#         ([1, 2, 3], 7),
#         ([1000000000, 1000000000, 1000000000], 3),
#     ]
#     expectations = [1, 2, 0, -1, 0]
#     for (nums, p), expected in zip(arguments, expectations):
#         solution = Solution().minSubarray(nums, p)
#         assert solution == expected
# test()
