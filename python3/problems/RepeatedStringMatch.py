### Source : https://leetcode.com/problems/repeated-string-match/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given two strings a and b, return the minimum number of times you should repeat string a so that 
# string b is a substring of it. If it is impossible for b-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K to be a substring of a 
# after repeating it, return -1.
# 
# Notice: string "abc" repeated 0 times is "",  repeated 1 time is "abc" and repeated 2 times is 
# "abcabc".
# 
# Example 1:
# 
# Input: a = "abcd", b = "cdabcdab"
# Output: 3
# Explanation: We return 3 because by repeating a three times "abcdabcdabcd", b is a substring of it.
# 
# Example 2:
# 
# Input: a = "a", b = "aa"
# Output: 2
# 
# Example 3:
# 
# Input: a = "a", b = "a"
# Output: 1
# 
# Example 4:
# 
# Input: a = "abc", b = "wxyz"
# Output: -1
# 
# Constraints:
# 
# 	1 <= a.length <= 104
# 	1 <= b.length <= 104
# 	a and b consist of lower-case English letters.
#####################################################################################################


class Solution:
    def repeatedStringMatch(self, a: str, b: str) -> int:
        return next((i for i in range(1, len(b) // len(a) + 3) if b in a * i), -1) if set(b).issubset(a) else -1

    def repeatedStringMatch(self, a: str, b: str) -> int:
        return next((i for i in range(len(b)//len(a), len(b)//len(a)+3) if b in a * i), -1) if set(b) <= set(a) else -1

    def repeatedStringMatch(self, a: str, b: str) -> int:
        multiplier = -(-len(b) // len(a))  # equal to math.ceil(len(b) / len(a))
        return multiplier * (b in a * multiplier) or (multiplier + 1) * (b in a * (multiplier + 1)) or -1


def test():
    arguments = [
        ("abcd", "cdabcdab"),
        ("a", "aa"),
        ("a", "a"),
        ("abc", "wxyz"),
        ("aa", "a"),
        ("abc", "cabcabca"),
    ]
    expectations = [3, 2, 1, -1, 1, 4]
    for (a, b), expected in zip(arguments, expectations):
        solution = Solution().repeatedStringMatch(a, b)
        assert solution == expected, (a, b)
