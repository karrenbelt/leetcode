### Source : https://leetcode.com/problems/minesweeper/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-15

##################################################################################################### 
#
# Let's play the minesweeper game (Wikipedia, online game)!
# 
# You are given a 2D char matrix representing the game board. '' represents an unrevealed mine, 'E' 
# represents an unrevealed empty square, 'B' represents a revealed blank square that has no adjacent 
# (above, below, left, right, and all 4 diagonals) mines, digit ('1' to '8') represents how many 
# mines are adjacent to this revealed square, and finally 'X' represents a revealed mine.
# 
# Now given the next click position (row and column indices) among all the unrevealed squares ('' or 
# 'E'), return the board after revealing this position according to the following rules:
# 
# 	If a mine ('') is revealed, then the game is over - change it to 'X'.
# 	If an empty square ('E') with no adjacent mines is revealed, then change it to revealed 
# blank ('B') and all of its adjacent unrevealed squares should be revealed recursively.
# 	If an empty square ('E') with at least one adjacent mine is revealed, then change it to a 
# digit ('1' to '8') representing the number of adjacent mines.
# 	Return the board when no more squares will be revealed.
# 
# Example 1:
# 
# Input: 
# 
# [['E', 'E', 'E', 'E', 'E'],
#  ['E', 'E', '', 'E', 'E'],
#  ['E', 'E', 'E', 'E', 'E'],
#  ['E', 'E', 'E', 'E', 'E']]
# 
# Click : [3,0]
# 
# Output: 
# 
# [['B', '1', 'E', '1', 'B'],
#  ['B', '1', '', '1', 'B'],
#  ['B', '1', '1', '1', 'B'],
#  ['B', 'B', 'B', 'B', 'B']]
# 
# Explanation:
# 
# Example 2:
# 
# Input: 
# 
# [['B', '1', 'E', '1', 'B'],
#  ['B', '1', '', '1', 'B'],
#  ['B', '1', '1', '1', 'B'],
#  ['B', 'B', 'B', 'B', 'B']]
# 
# Click : [1,2]
# 
# Output: 
# 
# [['B', '1', 'E', '1', 'B'],
#  ['B', '1', 'X', '1', 'B'],
#  ['B', '1', '1', '1', 'B'],
#  ['B', 'B', 'B', 'B', 'B']]
# 
# Explanation:
# 
# Note:
# 
# 	The range of the input matrix's height and width is [1,50].
# 	The click position will only be an unrevealed square ('' or 'E'), which also means the 
# input board contains at least one clickable square.
# 	The input board won't be a stage when game is over (some mines have been revealed).
# 	For simplicity, not mentioned rules should be ignored in this problem. For example, you 
# don't need to reveal all the unrevealed mines when the game is over, consider any cases that you 
# will win the game or flag any squares.
# 
#####################################################################################################

from typing import List


class Solution:
    def updateBoard(self, board: List[List[str]], click: List[int]) -> List[List[str]]:

        def make_move(i, j, clear=False):
            if not 0 <= i < len(board) or not 0 <= j < len(board[0]):
                return
            if board[i][j] == 'M' and not clear:
                board[i][j] = 'X'
            elif board[i][j] == 'E':
                n = sum([board[i + x][j + y] == 'M'
                        for y, x in directions if 0 <= i + x < len(board) and 0 <= j + y < len(board[0])])
                if n:
                    board[i][j] = str(n)
                else:
                    board[i][j] = 'B'
                    for y, x in directions:
                        make_move(i + x, j + y, clear=True)

        directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, 1), (1, -1), (-1, -1)]
        make_move(*click)
        return board


def test():
    arguments = [
        ([['E', 'E', 'E', 'E', 'E'],
         ['E', 'E', 'M', 'E', 'E'],
         ['E', 'E', 'E', 'E', 'E'],
         ['E', 'E', 'E', 'E', 'E']],
         [3, 0]),

        ([['B', '1', 'E', '1', 'B'],
         ['B', '1', 'M', '1', 'B'],
         ['B', '1', '1', '1', 'B'],
         ['B', 'B', 'B', 'B', 'B']],
         [1, 2]),
        ]
    expectations = [
        [['B', '1', 'E', '1', 'B'],
         ['B', '1', 'M', '1', 'B'],
         ['B', '1', '1', '1', 'B'],
         ['B', 'B', 'B', 'B', 'B']],

        [['B', '1', 'E', '1', 'B'],
         ['B', '1', 'X', '1', 'B'],
         ['B', '1', '1', '1', 'B'],
         ['B', 'B', 'B', 'B', 'B']],
        ]
    for (board, click), expected in zip(arguments, expectations):
        solution = Solution().updateBoard(board, click)
        assert solution == expected
