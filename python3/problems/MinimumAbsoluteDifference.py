### Source : https://leetcode.com/problems/minimum-absolute-difference/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-07

##################################################################################################### 
#
# Given an array of distinct integers arr, find all pairs of elements with the minimum absolute 
# difference of any two elements. 
# 
# Return a list of pairs in ascending order(with respect to pairs), each pair [a, b] follows
# 
# 	a, b are from arr
# 	a < b
# 	b - a equals to the minimum absolute difference of any two elements in arr
# 
# Example 1:
# 
# Input: arr = [4,2,1,3]
# Output: [[1,2],[2,3],[3,4]]
# Explanation: The minimum absolute difference is 1. List all pairs with difference equal to 1 in 
# ascending order.
# 
# Example 2:
# 
# Input: arr = [1,3,6,10,15]
# Output: [[1,3]]
# 
# Example 3:
# 
# Input: arr = [3,8,-10,23,19,-4,-14,27]
# Output: [[-14,-10],[19,23],[23,27]]
# 
# Constraints:
# 
# 	2 <= arr.length <= 105
# 	-106 <= arr[i] <= 106
#####################################################################################################

from typing import List


class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:  # O(n log n) time and O(n) space
        arr.sort()
        minimum = min(b - a for a, b in zip(arr, arr[1:]))
        return [[a, b] for a, b in zip(arr, arr[1:]) if b - a == minimum]


def test():
    arguments = [
        [4, 2, 1, 3],
        [1, 3, 6, 10, 15],
        [3, 8, -10, 23, 19, -4, -14, 27],
    ]
    expectations = [
        [[1, 2], [2, 3], [3, 4]],
        [[1, 3]],
        [[-14, -10], [19, 23], [23, 27]],
    ]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().minimumAbsDifference(arr)
        assert solution == expected
