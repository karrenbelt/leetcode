### Source : https://leetcode.com/problems/largest-component-size-by-common-factor/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-23

##################################################################################################### 
#
# You are given an integer array of unique positive integers nums. Consider the following graph:
# 
# 	There are nums.length nodes, labeled nums[0] to nums[nums.length - 1],
# 	There is an undirected edge between nums[i] and nums[j] if nums[i] and nums[j] share a 
# common factor greater than 1.
# 
# Return the size of the largest connected component in the graph.
# 
# Example 1:
# 
# Input: nums = [4,6,15,35]
# Output: 4
# 
# Example 2:
# 
# Input: nums = [20,50,9,63]
# Output: 2
# 
# Example 3:
# 
# Input: nums = [2,3,6,7,4,12,21,39]
# Output: 8
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	1 <= nums[i] <= 105
# 	All the values of nums are unique.
#####################################################################################################

from typing import List


class Solution:
    def largestComponentSize(self, nums: List[int]) -> int:
        # 1. find all prime factors of numbers
        # 2. for each prime record the indices of numbers if factorizes
        # 3. union find on prime-factor-grouped indices
        # 4. count the parents and return the maximum group size
        from collections import Counter

        def factorize(n: int) -> set:
            for i in range(2, int(n ** 0.5) + 1):
                if n % i == 0:
                    return factorize(n // i) | {i}
            return {n}

        def find(n: int) -> int:
            if parents[n] != n:
                parents[n] = find(parents[n])
            return parents[n]

        def union(x: int, y: int) -> None:
            parents[find(x)] = find(y)

        primes = {}
        for i, n in enumerate(nums):
            for prime in factorize(n):
                primes.setdefault(prime, []).append(i)

        parents = list(range(len(nums)))
        for indexes in primes.values():
            for i in range(len(indexes) - 1):
                union(indexes[i], indexes[i + 1])

        return max(Counter(map(find, range(len(nums)))).values())


def test():
    arguments = [
        [4, 6, 15, 35],
        [20, 50, 9, 63],
        [2, 3, 6, 7, 4, 12, 21, 39],
    ]
    expectations = [4, 2, 8]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().largestComponentSize(nums)
        assert solution == expected
