### Source : https://leetcode.com/problems/house-robber/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# You are a professional robber planning to rob houses along a street. Each house has a certain 
# amount of money stashed, the only constraint stopping you from robbing each of them is that 
# adjacent houses have security systems connected and it will automatically contact the police if two 
# adjacent houses were broken into on the same night.
# 
# Given an integer array nums representing the amount of money of each house, return the maximum 
# amount of money you can rob tonight without alerting the police.
# 
# Example 1:
# 
# Input: nums = [1,2,3,1]
# Output: 4
# Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
# Total amount you can rob = 1 + 3 = 4.
# 
# Example 2:
# 
# Input: nums = [2,7,9,3,1]
# Output: 12
# Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
# Total amount you can rob = 2 + 9 + 1 = 12.
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 400
#####################################################################################################

from typing import List


class Solution:
    # Find recursive relation
    # Recursive (top-down)
    # Recursive + memo (top-down)
    # Iterative + memo (bottom-up)
    # Iterative + N variables (bottom-up)
    def rob(self, nums: List[int]) -> int:  # O(n^2) time and O(n^2) space: TLE

        def dfs(i: int):
            return 0 if i < 0 else max(dfs(i - 1), dfs(i - 2) + nums[i])

        return dfs(len(nums) - 1)

    def rob(self, nums: List[int]) -> int:  # O(n) time and O(n) space

        def dfs(i: int):
            if i not in memo:
                memo[i] = 0 if i < 0 else max(dfs(i - 1), dfs(i - 2) + nums[i])
            return memo[i]

        memo = {}
        return dfs(len(nums) - 1)

    def rob(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        memo = {-2: 0, -1: 0}
        for i, n in enumerate(nums):
            memo[i] = max(memo[i - 1], memo[i - 2] + n)
        return memo[len(nums) - 1]

    def rob(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        prev = current = 0
        for n in nums:
            prev, current = current, max(prev + n, current)
        return current


def test():
    arguments = [
        [1, 2, 3, 1],
        [2, 7, 9, 3, 1],
        [10, 0, 0, 100, 0],
        [10, 0, 0, 100, 0, 10],
    ]
    expectations = [4, 12, 110, 120]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().rob(nums)
        assert solution == expected
