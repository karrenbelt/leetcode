### Source : https://leetcode.com/problems/find-elements-in-a-contaminated-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# Given a binary tree with the following rules:
# 
# 	root.val == 0
# 	If treeNode.val == x and treeNode.left != null, then treeNode.left.val == 2 * x + 1
# 	If treeNode.val == x and treeNode.right != null, then treeNode.right.val == 2 * x + 2
# 
# Now the binary tree is contaminated, which means all treeNode.val have been changed to -1.
# 
# Implement the FindElements class:
# 
# 	FindElements(TreeNode* root) Initializes the object with a contaminated binary tree and 
# recovers it.
# 	bool find(int target) Returns true if the target value exists in the recovered binary tree.
# 
# Example 1:
# 
# Input
# ["FindElements","find","find"]
# [[[-1,null,-1]],[1],[2]]
# Output
# [null,false,true]
# Explanation
# FindElements findElements = new FindElements([-1,null,-1]); 
# findElements.find(1); // return False 
# findElements.find(2); // return True 
# 
# Example 2:
# 
# Input
# ["FindElements","find","find","find"]
# [[[-1,-1,-1,-1,-1]],[1],[3],[5]]
# Output
# [null,true,true,false]
# Explanation
# FindElements findElements = new FindElements([-1,-1,-1,-1,-1]);
# findElements.find(1); // return True
# findElements.find(3); // return True
# findElements.find(5); // return False
# 
# Example 3:
# 
# Input
# ["FindElements","find","find","find","find"]
# [[[-1,null,-1,-1,null,-1]],[2],[3],[4],[5]]
# Output
# [null,true,false,false,true]
# Explanation
# FindElements findElements = new FindElements([-1,null,-1,-1,null,-1]);
# findElements.find(2); // return True
# findElements.find(3); // return False
# findElements.find(4); // return False
# findElements.find(5); // return True
# 
# Constraints:
# 
# 	TreeNode.val == -1
# 	The height of the binary tree is less than or equal to 20
# 	The total number of nodes is between [1, 104]
# 	Total calls of find() is between [1, 104]
# 	0 <= target <= 106
#####################################################################################################


from python3 import BinaryTreeNode as TreeNode


class FindElements:

    def __init__(self, root: TreeNode):

        def traverse(node: TreeNode, x: int):  # O(n) time
            if not node:
                return
            self.seen.add(x)
            traverse(node.left, 2 * x + 1)
            traverse(node.right, 2 * x + 2)

        self.seen = {0}
        traverse(root, 0)

    def find(self, target: int) -> bool:  # O(1) time
        return target in self.seen

# Your FindElements object will be instantiated and called as such:
# obj = FindElements(root)
# param_1 = obj.find(target)


def test():
    from python3 import Codec
    operations = ["FindElements", "find", "find", "find"]
    arguments = [["[-1,-1,-1,-1,-1]"], [1], [3], [5]]
    expectations = [None, True, True, False]

    operations = ["FindElements", "find", "find", "find", "find"]
    arguments = [["[-1,null,-1,-1,null,-1]"], [2], [3], [4], [5]]
    expectations = [None, True, False, False, True]

    root = Codec.deserialize(*arguments[0])
    obj = FindElements(root)
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
