### Source : https://leetcode.com/problems/utf-8-validation/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-18

##################################################################################################### 
#
# Given an integer array data representing the data, return whether it is a valid UTF-8 encoding.
# 
# A character in UTF8 can be from 1 to 4 bytes long, subjected to the following rules:
# 
# 	For a 1-byte character, the first bit is a 0, followed by its Unicode code.
# 	For an n-bytes character, the first n bits are all one's, the n + 1 bit is 0, followed by n 
# - 1 bytes with the most significant 2 bits being 10.
# 
# This is how the UTF-8 encoding would work:
# 
#    Char. number range  |        UTF-8 octet sequence
#       (hexadecimal)    |              (binary)
#    --------------------+---------------------------------------------
#    0000 0000-0000 007F | 0xxxxxxx
#    0000 0080-0000 07FF | 110xxxxx 10xxxxxx
#    0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
#    0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
# 
# Note: The input is an array of integers. Only the least significant 8 bits of each integer is used 
# to store the data. This means each integer represents only 1 byte of data.
# 
# Example 1:
# 
# Input: data = [197,130,1]
# Output: true
# Explanation: data represents the octet sequence: 11000101 10000010 00000001.
# It is a valid utf-8 encoding for a 2-bytes character followed by a 1-byte character.
# 
# Example 2:
# 
# Input: data = [235,140,4]
# Output: false
# Explanation: data represented the octet sequence: 11101011 10001100 00000100.
# The first 3 bits are all one's and the 4th bit is 0 means it is a 3-bytes character.
# The next byte is a continuation byte which starts with 10 and that's correct.
# But the second continuation byte does not start with 10, so it is invalid.
# 
# Constraints:
# 
# 	1 <= data.length <= 2 * 104
# 	0 <= data[i] <= 255
#####################################################################################################

from typing import List


class Solution:
    def validUtf8(self, data: List[int]) -> bool:  # O(n) time and O(n) space
        sequence = [bin(n)[2:].zfill(8) for n in reversed(data)]
        while sequence and (byte := sequence.pop()):
            if byte.startswith('0'):
                continue
            i = byte.find('0')
            if not 2 <= i <= 4 or len(sequence) < i - 1:
                return False
            if any(sequence.pop()[:2] != '10' for _ in range(i - 1)):
                return False
        return True

    def validUtf8(self, data: List[int]) -> bool:  # O(n) time and O(n) space
        sequence = [bin(n)[2:].zfill(8) for n in reversed(data) if n > 127]
        while sequence and (i := sequence.pop().find('0')):
            if not 2 <= i <= 4 or len(sequence) < i - 1 or any(sequence.pop()[:2] != '10' for _ in range(i - 1)):
                return False
        return True


def test():
    arguments = [
        [197, 130, 1],
        [235, 140, 4],
        [240, 162, 138, 147, 17],
        [250, 145, 145, 145, 145],  # 5 bit -> False
        [237],
    ]
    expectations = [True, False, True, False, False]
    for data, expected in zip(arguments, expectations):
        solution = Solution().validUtf8(data)
        assert solution == expected, data


test()
