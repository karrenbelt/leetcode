### Source : https://leetcode.com/problems/binary-tree-pruning/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# We are given the head node root of a binary tree, where additionally every node's value is either a 
# 0 or a 1.
# 
# Return the same tree where every subtree (of the given tree) not containing a 1 has been removed.
# 
# (Recall that the subtree of a node X is X, plus every node that is a descendant of X.)
# 
# Example 1:
# Input: [1,null,0,0,1]
# Output: [1,null,0,null,1]
# 
# Explanation: 
# Only the red nodes satisfy the property "every subtree not containing a 1".
# The diagram on the right represents the answer.
# 
# Example 2:
# Input: [1,0,1,0,0,0,1]
# Output: [1,null,1,null,1]
# 
# Example 3:
# Input: [1,1,0,1,1,0,1,0]
# Output: [1,1,0,1,1,null,1]
# 
# Note: 
# 
# 	The binary tree will have at most 200 nodes.
# 	The value of each node will only be 0 or 1.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def pruneTree(self, root: TreeNode) -> TreeNode:  # O(n) time and O(1) space

        def traverse(node: TreeNode, parent):
            if not node:
                return
            traverse(node.left, node)
            traverse(node.right, node)
            if not node.right and not node.left and node.val == 0 and parent:
                if node is parent.left:
                    parent.left = None
                else:
                    parent.right = None

        traverse(root, None)
        return root if root.right or root.left else None

    def pruneTree(self, root: TreeNode) -> TreeNode:  # O(n) time and O(1) space

        def traverse(node: TreeNode):
            if not node:
                return
            node.left = traverse(node.left)
            node.right = traverse(node.right)
            return node if node.val or node.left or node.right else None

        return traverse(root)

    def pruneTree(self, root: TreeNode) -> TreeNode:
        if root:
            root.left, root.right = self.pruneTree(root.left), self.pruneTree(root.right)
        return root if root and (root.left or root.right or root.val) else None

    def pruneTree(self, root: TreeNode) -> TreeNode:
        stack = [(0, root)]
        while stack:
            seen, node = stack.pop()
            if node is None:
                continue
            if not seen:
                stack.extend([(1, node), (0, node.right), (0, node.left)])
            else:
                if node.left and not (node.left.val or node.left.left or node.left.right):
                    node.left = None
                if node.right and not (node.right.val or node.right.left or node.right.right):
                    node.right = None
        return root


def test():
    from python3 import Codec
    serialized_trees = [
        "[1,null,0,0,1]",
        "[1,0,1,0,0,0,1]",
        "[1,1,0,1,1,0,1,0]",
        ]
    expectations = [
        "[1,null,0,null,1]",
        "[1,null,1,null,1]",
        "[1,1,0,1,1,null,1]",
        ]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().pruneTree(root)
        assert solution == Codec.deserialize(expected)
