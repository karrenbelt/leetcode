### Source : https://leetcode.com/problems/longest-duplicate-substring/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-22

##################################################################################################### 
#
# Given a string s, consider all duplicated substrings: (contiguous) substrings of s that occur 2 or 
# more times. The occurrences may overlap.
# 
# Return any duplicated substring that has the longest possible length. If s does not have a 
# duplicated substring, the answer is "".
# 
# Example 1:
# Input: s = "banana"
# Output: "ana"
# Example 2:
# Input: s = "abcd"
# Output: ""
# 
# Constraints:
# 
# 	2 <= s.length <= 3 * 104
# 	s consists of lowercase English letters.
#####################################################################################################


class Solution:
    # Trie, Suffix tree, Suffix array
    def longestDupSubstring(self, s: str) -> str:  # O(n^2) time -> probably yields TLE
        # works ok for up to a size of 3*10**3, not 3*10**4
        trie = {}
        longest = ""
        for i in range(len(s)):
            node = trie
            for j in range(i, len(s)):
                if s[j] not in node:
                    node[s[j]] = {}
                else:
                    if j - i + 1 > len(longest):
                        longest = s[i:j + 1]
                node = node[s[j]]
        return longest


def test():
    arguments = [
        "banana",
        "abcd",
    ]
    expectations = ["ana", ""]
    for s, expected in zip(arguments, expectations):
        solution = Solution().longestDupSubstring(s)
        assert solution == expected
