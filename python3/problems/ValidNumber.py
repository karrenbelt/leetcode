### Source : https://leetcode.com/problems/valid-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Validate if a given string can be interpreted as a decimal number.
# 
# Some examples:
# "0" => true
# " 0.1 " => true
# "abc" => false
# "1 a" => false
# "2e10" => true
# " -90e3   " => true
# " 1e" => false
# "e3" => false
# " 6e-1" => true
# " 99e2.5 " => false
# "53.5e93" => true
# " --6 " => false
# "-+3" => false
# "95a54e53" => false
# 
# Note: It is intended for the problem statement to be ambiguous. You should gather all requirements 
# up front before implementing one. However, here is a list of characters that can be in a valid 
# decimal number:
# 
# 	Numbers 0-9
# 	Exponent - "e"
# 	Positive/negative sign - "+"/"-"
# 	Decimal point - "."
# 
# Of course, the context of these characters also matters in the input.
# 
# Update (2015-02-10):
# The signature of the C++ function had been updated. If you still see your function signature 
# accepts a const char * argument, please click the reload button to reset your code definition.
#####################################################################################################


class Solution:
    def isNumber(self, s: str) -> bool:  # used to work, not anymore
        try:
            float(s.strip())
        except ValueError:
            return False
        return True

    def isNumber(self, s: str) -> bool:
        s = s.strip()
        met_dot = met_e = met_digit = False
        for i, char in enumerate(s):
            if char in ['+', '-']:
                if i > 0 and s[i - 1] != 'e':
                    return False
            elif char == '.':
                if met_dot or met_e:
                    return False
                met_dot = True
            elif char == 'e':
                if met_e or not met_digit:
                    return False
                met_e, met_digit = True, False
            elif char.isdigit():
                met_digit = True
            else:
                return False
        return met_digit

    def isNumber(self, s: str):  # Deterministic finite automaton
        state = [{},
                 # State (1) - initial state (scan ahead thru blanks)
                 {'blank': 1, 'sign': 2, 'digit': 3, '.': 4},
                 # State (2) - found sign (expect digit/dot)
                 {'digit': 3, '.': 4},
                 # State (3) - digit consumer (loop until non-digit)
                 {'digit': 3, '.': 5, 'e': 6, 'blank': 9},
                 # State (4) - found dot (only a digit is valid)
                 {'digit': 5},
                 # State (5) - after dot (expect digits, e, or end of valid input)
                 {'digit': 5, 'e': 6, 'blank': 9},
                 # State (6) - found 'e' (only a sign or digit valid)
                 {'sign': 7, 'digit': 8},
                 # State (7) - sign after 'e' (only digit)
                 {'digit': 8},
                 # State (8) - digit after 'e' (expect digits or end of valid input)
                 {'digit': 8, 'blank': 9},
                 # State (9) - Terminal state (fail if non-blank found)
                 {'blank': 9}]
        current_state = 1
        for c in s:
            c = 'digit' if '0' <= c <= '9' else 'blank' if c == ' ' else 'sign' if c in ['+', '-'] else c.lower()
            if c not in state[current_state].keys():
                return False
            current_state = state[current_state][c]
        if current_state not in {3, 5, 8, 9}:
            return False
        return True


def test():
    strings = ["0", " 0.1 ", "abc", "1 a", "2e10", " -90e3   ", " 1e", "e3",
               " 6e-1", " 99e2.5 ", "53.5e93", " --6 ", "-+3", "95a54e53",
               "inf", "+inf", "-inf", "nan", "Infinity", '1E9']  # new cases, crash non-DFA approach
    expectations = [True, True, False, False, True, True, False, False,
                    True, False, True, False, False, False,
                    False, False, False, False, False, True]
    for s, expected in zip(strings, expectations):
        solution = Solution().isNumber(s)
        assert solution == expected
