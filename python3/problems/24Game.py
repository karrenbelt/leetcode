### Source : https://leetcode.com/problems/24-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-11

##################################################################################################### 
#
# You are given an integer array cards of length 4. You have four cards, each containing a number in 
# the range [1, 9]. You should arrange the numbers on these cards in a mathematical expression using 
# the operators ['+', '-', '*', '/'] and the parentheses '(' and ')' to get the value 24.
# 
# You are restricted with the following rules:
# 
# 	The division operator '/' represents real division, not integer division.
# 
# 		For example, 4 / (1 - 2 / 3) = 4 / (1 / 3) = 12.
# 
# 	Every operation done is between two numbers. In particular, we cannot use '-' as a unary 
# operator.
# 
# 		For example, if cards = [1, 1, 1, 1], the expression "-1 - 1 - 1 - 1" is not 
# allowed.
# 
# 	You cannot concatenate numbers together
# 
# 		For example, if cards = [1, 2, 1, 2], the expression "12 + 12" is not valid.
# 
# Return true if you can get such expression that evaluates to 24, and false otherwise.
# 
# Example 1:
# 
# Input: cards = [4,1,8,7]
# Output: true
# Explanation: (8-4) * (7-1) = 24
# 
# Example 2:
# 
# Input: cards = [1,2,1,2]
# Output: false
# 
# Constraints:
# 
# 	cards.length == 4
# 	1 <= cards[i] <= 9
#####################################################################################################

from typing import List


class Solution:
    def judgePoint24(self, cards: List[int]) -> bool:
        # I use fractions because floating point arithmetic will mess things up otherwise
        # size is 24 [nums] x 64 [operators] x 2 [precedence]
        from operator import add, sub, mul
        from fractions import Fraction
        from itertools import permutations, product

        def div(x, y):
            return x / y if y else 0

        operators = [add, sub, mul, div]
        for (a, b, c, d) in set(permutations(map(Fraction, cards))):  # O(24)
            for (f1, f2, f3) in product(operators, repeat=3):  # O(64)
                if f1(d, f2(c, f3(a, b))) == 24 or f1(f2(a, b), f3(c, d)) == 24:  # O(2)
                    return True
        return False

    def judgePoint24(self, cards: List[int]) -> bool:
        import math
        import itertools
        if len(cards) == 1:
            return math.isclose(cards[0], 24)
        return any(self.judgePoint24([x] + rest)
                   for a, b, *rest in itertools.permutations(cards)
                   for x in {a + b, a - b, a * b, b and a / b})


def test():
    arguments = [
        [4, 1, 8, 7],
        [1, 2, 1, 2],
        [1, 2, 3, 4],  # 4 / (1 - 2 / 3)
        [1, 3, 4, 6],
        [8, 1, 6, 6],
    ]
    expectations = [True, False, True, True, True]
    for cards, expected in zip(arguments, expectations):
        solution = Solution().judgePoint24(cards)
        assert solution == expected
