### Source : https://leetcode.com/problems/find-smallest-letter-greater-than-target/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# 
# Given a list of sorted characters letters containing only lowercase letters, and given a target 
# letter target, find the smallest element in the list that is larger than the given target.
# 
# Letters also wrap around.  For example, if the target is target = 'z' and letters = ['a', 'b'], the 
# answer is 'a'.
# 
# Examples:
# 
# Input:
# letters = ["c", "f", "j"]
# target = "a"
# Output: "c"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "c"
# Output: "f"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "d"
# Output: "f"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "g"
# Output: "j"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "j"
# Output: "c"
# 
# Input:
# letters = ["c", "f", "j"]
# target = "k"
# Output: "c"
# 
# Note:
# 
# letters has a length in range [2, 10000].
# letters consists of lowercase letters, and contains at least 2 unique letters.
# target is a lowercase letter.
# 
#####################################################################################################

from typing import List


class Solution:

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # O(n) time and O(1) space
        for letter in letters:
            if letter > target:
                return letter
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # O(n log n) time and O(1) space
        for letter in sorted(set(letters)):
            if letter > target:
                return letter
        return letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # O(log n) time - binary search template I
        left, right = 0, len(letters) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if letters[mid] > target:
                right = mid - 1
            else:
                left = mid + 1
        return letters[left] if left < len(letters) else letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # O(log n) time - binary search template II
        left, right = 0, len(letters)
        while left < right:
            mid = (left + right) // 2
            if letters[mid] <= target:
                left = mid + 1
            else:
                right = mid
        return letters[left] if left < len(letters) else letters[0]

    def nextGreatestLetter(self, letters: List[str], target: str) -> str:  # O(log n) time
        import bisect
        return letters[bisect.bisect_right(letters, target) % len(letters)]


def test():
    arguments = [
        (["c", "f", "j"], "a"),
        (["c", "f", "j"], "c"),
        (["c", "f", "j"], "d"),
        (["c", "f", "j"], "g"),
        (["c", "f", "j"], "j"),
        (["c", "f", "j"], "k"),
        ]
    expectations = ["c", "f", "f", "j", "c", "c"]
    for (letters, target), expected in zip(arguments, expectations):
        solution = Solution().nextGreatestLetter(letters, target)
        assert solution == expected
