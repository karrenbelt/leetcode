### Source : https://leetcode.com/problems/shortest-path-visiting-all-nodes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-16

##################################################################################################### 
#
# An undirected, connected graph of N nodes (labeled 0, 1, 2, ..., N-1) is given as graph.
# 
# graph.length = N, and j != i is in the list graph[i] exactly once, if and only if nodes i and j are 
# connected.
# 
# Return the length of the shortest path that visits every node. You may start and stop at any node, 
# you may revisit nodes multiple times, and you may reuse edges.
# 
# Example 1:
# 
# Input: [[1,2,3],[0],[0],[0]]
# Output: 4
# Explanation: One possible path is [1,0,2,0,3]
# 
# Example 2:
# 
# Input: [[1],[0,2,4],[1,3,4],[2],[1,2]]
# Output: 4
# Explanation: One possible path is [0,1,4,2,3]
# 
# Note:
# 
# 	1 <= graph.length <= 12
# 	0 <= graph[i].length < graph.length
# 
#####################################################################################################

from typing import List


class Solution:
    def shortestPathLength(self, graph: List[List[int]]) -> int:
        # 1. minimum path in a graph: BFS
        # 2. need to prevent infinite cycles: somehow store visited nodes,
        #    but we cannot use simple bfs: nodes and edges (undirected - both ways) can be used again.
        #    We should allow: 0 -> 1 -> 0 but not 0 -> 1 -> 0 -> 1
        #    Hence we somehow need to store information on the current node and the nodes visited: a bitmask
        # 3. we need to start the search from all starting points (nodes) simultaneously to be able to break early
        from collections import deque

        distance = float('inf')
        all_visited = (1 << len(graph)) - 1
        bit_masks = [1 << i for i in range(len(graph))]
        queue = deque([(i, bit_masks[i], 0) for i in range(len(graph))])
        visited_states = [{bit_masks[i]} for i in range(len(graph))]
        while queue:
            node, visited, distance = queue.popleft()
            if visited == all_visited:
                break
            for neighbor in graph[node]:
                new_visited = visited | bit_masks[neighbor]
                if new_visited not in visited_states[neighbor]:
                    visited_states[neighbor].add(new_visited)
                    queue.append((neighbor, new_visited, distance + 1))
        return distance


def test():
    arguments = [
        [[1, 2, 3], [0], [0], [0]],
        [[1], [0, 2, 4], [1, 3, 4], [2], [1, 2]],
        ]
    expectations = [4, 4]
    for graph, expected in zip(arguments, expectations):
        solution = Solution().shortestPathLength(graph)
        assert solution == expected
