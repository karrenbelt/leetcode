### Source : https://leetcode.com/problems/largest-odd-number-in-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-20

##################################################################################################### 
#
# You are given a string num, representing a large integer. Return the largest-valued odd integer (as 
# a string) that is a non-empty substring of num, or an empty string "" if no odd integer exists.
# 
# A substring is a contiguous sequence of characters within a string.
# 
# Example 1:
# 
# Input: num = "52"
# Output: "5"
# Explanation: The only non-empty substrings are "5", "2", and "52". "5" is the only odd number.
# 
# Example 2:
# 
# Input: num = "4206"
# Output: ""
# Explanation: There are no odd numbers in "4206".
# 
# Example 3:
# 
# Input: num = "35427"
# Output: "35427"
# Explanation: "35427" is already an odd number.
# 
# Constraints:
# 
# 	1 <= num.length <= 105
# 	num only consists of digits and does not contain any leading zeros.
#####################################################################################################


class Solution:
    def largestOddNumber(self, num: str) -> str:
        return next((num[:i + 1] for i in range(len(num) - 1, -1, -1) if int(num[i]) % 2), "")

    def largestOddNumber(self, num: str) -> str:
        return num.rstrip('02468')


def test():
    arguments = ["52", "4206", "35427"]
    expectations = ["5", "", "35427"]
    for num, expected in zip(arguments, expectations):
        solution = Solution().largestOddNumber(num)
        assert solution == expected, solution
