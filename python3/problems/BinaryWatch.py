### Source : https://leetcode.com/problems/binary-watch/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-15

##################################################################################################### 
#
# A binary watch has 4 LEDs on the top which represent the hours (0-11), and the 6 LEDs on the bottom 
# represent the minutes (0-59).
# Each LED represents a zero or one, with the least significant bit on the right.
# 
# For example, the above binary watch reads "3:25".
# 
# Given a non-negative integer n which represents the number of LEDs that are currently on, return 
# all possible times the watch could represent.
# 
# Example:
# Input: n = 1Return: ["1:00", "2:00", "4:00", "8:00", "0:01", "0:02", "0:04", "0:08", "0:16", "0:32"]
# 
# Note:
# 
# The order of output does not matter.
# The hour must not contain a leading zero, for example "01:00" is not valid, it should be "1:00".
# The minute must be consist of two digits and may contain a leading zero, for example "10:2" is not 
# valid, it should be "10:02".
# 
#####################################################################################################

from typing import List


class Solution:
    def readBinaryWatch(self, num: int) -> List[str]:
        # backtracking - all combinations that are no more than 12 hours and no more than 59 minutes
        def backtrack(n: int, ctr, limit, start):
            h = n & 15
            m = n >> 4
            if h >= 12 or m >= 60:
                return
            if ctr == 0:
                possible_times.append(f'{h}:{m:02d}')
            for i in range(start, limit):
                if (n & (1 << i)) == 0:
                    backtrack(n | (1 << i), ctr - 1, limit, i + 1)

        possible_times = []
        backtrack(0, num, 10, 0)
        return possible_times

    def readBinaryWatch(self, num):
        from itertools import combinations

        watch = [1, 2, 4, 8, 1, 2, 4, 8, 16, 32]
        times = []
        for leds in combinations(range(len(watch)), num):
            h = sum(watch[i] for i in leds if i < 4)
            m = sum(watch[i] for i in leds if i >= 4)
            if h > 11 or m > 59:
                continue
            times.append("{}:{:02d}".format(h, m))
        return times

    def readBinaryWatch(self, num: int) -> List[str]:
        return ['%d:%02d' % (h, m) for h in range(12) for m in range(60) if (bin(h) + bin(m)).count('1') == num]


def test():
    numbers = [1, 2]
    expectations = [
        ['0:01', '0:02', '0:04', '0:08', '0:16', '0:32', '1:00', '2:00', '4:00', '8:00'],
        ['0:03', '0:05', '0:06', '0:09', '0:10', '0:12', '0:17', '0:18', '0:20', '0:24', '0:33', '0:34', '0:36', '0:40',
         '0:48', '1:01', '1:02', '1:04', '1:08', '1:16', '1:32', '2:01', '2:02', '2:04', '2:08', '2:16', '2:32', '3:00',
         '4:01', '4:02', '4:04', '4:08', '4:16', '4:32', '5:00', '6:00', '8:01', '8:02', '8:04', '8:08', '8:16', '8:32',
         '9:00', '10:00'],
        ]
    for num, expected in zip(numbers, expectations):
        solution = Solution().readBinaryWatch(num)
        assert set(solution) == set(expected)
