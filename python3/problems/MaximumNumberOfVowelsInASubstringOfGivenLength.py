### Source : https://leetcode.com/problems/maximum-number-of-vowels-in-a-substring-of-given-length/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# Given a string s and an integer k.
# 
# Return the maximum number of vowel letters in any substring of s with length k.
# 
# Vowel letters in English are (a, e, i, o, u).
# 
# Example 1:
# 
# Input: s = "abciiidef", k = 3
# Output: 3
# Explanation: The substring "iii" contains 3 vowel letters.
# 
# Example 2:
# 
# Input: s = "aeiou", k = 2
# Output: 2
# Explanation: Any substring of length 2 contains 2 vowels.
# 
# Example 3:
# 
# Input: s = "leetcode", k = 3
# Output: 2
# Explanation: "lee", "eet" and "ode" contain 2 vowels.
# 
# Example 4:
# 
# Input: s = "rhythms", k = 4
# Output: 0
# Explanation: We can see that s doesn't have any vowel letters.
# 
# Example 5:
# 
# Input: s = "tryhard", k = 4
# Output: 1
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s consists of lowercase English letters.
# 	1 <= k <= s.length
#####################################################################################################


class Solution:
    def maxVowels(self, s: str, k: int) -> int:  # O(n) time and O(1) space
        vowels = {'a', 'e', 'i', 'o', 'u'}
        maximum = current = sum(1 for i in range(k) if s[i] in vowels)
        for i in range(len(s) - k):
            current += - (s[i] in vowels) + (s[i + k] in vowels)
            maximum = max(maximum, current)
        return maximum


def test():
    arguments = [
        ("abciiidef", 3),
        ("aeiou", 2),
        ("leetcode", 3),
        ("rhythms", 4),
        ("tryhard", 4),
        ]
    expectations = [3, 2, 2, 0, 1]
    for (s, k), expected in zip(arguments, expectations):
        solution = Solution().maxVowels(s, k)
        assert solution == expected
