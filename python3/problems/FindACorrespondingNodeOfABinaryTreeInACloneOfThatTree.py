### Source : https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-03

##################################################################################################### 
#
# Given two binary trees original and cloned and given a reference to a node target in the original 
# tree.
# 
# The cloned tree is a copy of the original tree.
# 
# Return a reference to the same node in the cloned tree.
# 
# Note that you are not allowed to change any of the two trees or the target node and the answer must 
# be a reference to a node in the cloned tree.
# 
# Follow up: Solve the problem if repeated values on the tree are allowed.
# 
# Example 1:
# 
# Input: tree = [7,4,3,null,null,6,19], target = 3
# Output: 3
# Explanation: In all examples the original and cloned trees are shown. The target node is a green 
# node from the original tree. The answer is the yellow node from the cloned tree.
# 
# Example 2:
# 
# Input: tree = [7], target =  7
# Output: 7
# 
# Example 3:
# 
# Input: tree = [8,null,6,null,5,null,4,null,3,null,2,null,1], target = 4
# Output: 4
# 
# Example 4:
# 
# Input: tree = [1,2,3,4,5,6,7,8,9,10], target = 5
# Output: 5
# 
# Example 5:
# 
# Input: tree = [1,2,null,3], target = 2
# Output: 2
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	The values of the nodes of the tree are unique.
# 	target node is a node from the original tree and is not null.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def getTargetCopy(self, original: TreeNode, cloned: TreeNode, target: TreeNode) -> TreeNode:  # O(h) time

        def traverse(node: TreeNode) -> TreeNode:
            if node:
                yield node
                yield from traverse(node.left)
                yield from traverse(node.right)

        return next(clone for node, clone in zip(traverse(original), traverse(cloned)) if node is target)


def test():
    from python3 import Codec
    arguments = [
        ("[7,4,3,null,null,6,19]", 3),
        ("[7]", 7),
        ("[8,null,6,null,5,null,4,null,3,null,2,null,1]", 4),
        ("[1,2,3,4,5,6,7,8,9,10]", 5),
        ("[1,2,null,3]", 2)
    ]
    expectations = [3, 7, 4, 5, 2]
    for (serialized_tree, value), expected in zip(arguments, expectations):
        original = Codec.deserialize(serialized_tree)
        cloned = original.copy()
        target = next(node for node in original if node.val == value)
        solution = Solution().getTargetCopy(original, cloned, target)
        assert solution is next(node for node in cloned if node.val == expected)
