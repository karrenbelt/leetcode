### Source : https://leetcode.com/problems/n-queens-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-30

##################################################################################################### 
#
# The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two 
# queens attack each other.
# 
# Given an integer n, return the number of distinct solutions to the n-queens puzzle.
# 
# Example 1:
# 
# Input: n = 4
# Output: 2
# Explanation: There are two distinct solutions to the 4-queens puzzle as shown.
# 
# Example 2:
# 
# Input: n = 1
# Output: 1
# 
# Constraints:
# 
# 	1 <= n <= 9
#####################################################################################################


class Solution:

    def totalNQueens(self, n: int) -> int:
        return [0, 1, 0, 0, 2, 10, 4, 40, 92, 352][n]

    def totalNQueens(self, n: int) -> int:
        col = [False] * n
        diag = [False] * (2 * n - 1)
        r_diag = [False] * (2 * n - 1)

        def dfs(r):
            nonlocal ans
            if r == n:
                ans += 1
                return
            for c in range(n):
                if not col[c] and not diag[r - c] and not r_diag[r + c]:
                    col[c] = diag[r - c] = r_diag[r + c] = True  # place
                    dfs(r + 1)  # go next row
                    col[c] = diag[r - c] = r_diag[r + c] = False  # remove

        ans = 0
        dfs(0)
        return ans

    def totalNQueens(self, n: int) -> int:

        def is_valid(nums, n):
            for i in range(n):
                if nums[i] == nums[n] or abs(nums[n] - nums[i]) == n - i:
                    return False
            return True

        def dfs(nums, index):
            nonlocal ans
            if index == len(nums):
                ans += 1
                return  # backtracking
            for i in range(len(nums)):
                nums[index] = i
                if is_valid(nums, index):
                    dfs(nums, index + 1)

        ans = 0
        dfs([-1] * n, 0)
        return ans

    def totalNQueens(self, n: int) -> int:

        def backtrack(i):
            if i == n:
                return 1
            ans = 0
            for j in range(n):
                if j not in cols and i - j not in diag and i + j not in off_diag:
                    cols.add(j)
                    diag.add(i - j)
                    off_diag.add(i + j)
                    ans += backtrack(i + 1)
                    off_diag.remove(i + j)
                    diag.remove(i - j)
                    cols.remove(j)
            return ans

        cols, diag, off_diag = (set() for _ in range(3))
        return backtrack(0)

    def totalNQueens(self, n: int) -> int:

        def backtrack(i: int) -> int:
            if i == n:
                return 1
            ans = 0
            for j in range(n):
                if j not in cols and i - j not in diag and i + j not in off_diag:
                    cols.add(j) or diag.add(i - j) or off_diag.add(i + j)
                    ans += backtrack(i + 1)
                    off_diag.remove(i + j) or diag.remove(i - j) or cols.remove(j)
            return ans

        cols, diag, off_diag = (set() for _ in range(3))
        return backtrack(0)

    def totalNQueens(self, n: int) -> int:

        def dfs(board, row):
            if row == n:
                return 1
            count = 0
            for x in set_n - set(board):
                if all(row - i != abs(x - y) for i, y in enumerate(board[:row])):
                    board[row] = x
                    count += dfs(board, row + 1)
                    board[row] = -1
            return count

        set_n = {i for i in range(n)}
        return dfs([-1] * n, 0)

    def totalNQueens(self, n: int) -> int:  # without backtracking
        total = set(range(n))

        def dfs(track, level: int):
            if level == n:
                return 1
            count = 0
            for i in total - set(track):
                if all(level - j != abs(i - l) for j, l in enumerate(track)):
                    count += dfs(track + [i], level + 1)
            return count

        return dfs([], 0)


def test():
    numbers = list(range(1, 10))
    expectations = [1, 0, 0, 2, 10, 4, 40, 92, 352]
    for n, expected in zip(numbers, expectations):
        solution = Solution().totalNQueens(n)
        assert solution == expected
