### Source : https://leetcode.com/problems/minimum-height-trees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-04

##################################################################################################### 
#
# A tree is an undirected graph in which any two vertices are connected by exactly one path. In other 
# words, any connected graph without simple cycles is a tree.
# 
# Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1 edges where edges[i] = [ai, 
# bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree, you can 
# choose any node of the tree as the root. When you select a node x as the root, the result tree has 
# height h. Among all possible rooted trees, those with minimum height (i.e. min(h))  are called 
# minimum height trees (HTs).
# 
# Return a list of all HTs' root labels. You can return the answer in any order.
# 
# The height of a rooted tree is the number of edges on the longest downward path between the root 
# and a leaf.
# 
# Example 1:
# 
# Input: n = 4, edges = [[1,0],[1,2],[1,3]]
# Output: [1]
# Explanation: As shown, the height of the tree is 1 when the root is the node with label 1 which is 
# the only HT.
# 
# Example 2:
# 
# Input: n = 6, edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
# Output: [3,4]
# 
# Example 3:
# 
# Input: n = 1, edges = []
# Output: [0]
# 
# Example 4:
# 
# Input: n = 2, edges = [[0,1]]
# Output: [0,1]
# 
# Constraints:
# 
# 	1 <= n <= 2 * 104
# 	edges.length == n - 1
# 	0 <= ai, bi < n
# 	ai != bi
# 	All the pairs (ai, bi) are distinct.
# 	The given input is guaranteed to be a tree and there will be no repeated edges.
#####################################################################################################

from typing import List


class Solution:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:  # O(n) and O(n)
        # we're not going to actually construct the tree, I don't think that's necessary
        # we can use a dict mapping int to a set of int, then for each key determine the height of the tree
        # important to note that:
        # - it is undirected and acyclic
        # - it can have either 1 or 2 nodes, depending on parity of the nodes
        # - the root(s) is / are the middle or middle two nodes of the longest path

        # Any connected graph who has n nodes with n-1 edges is a tree.
        # assert len(edges) == n - 1
        if not edges:
            return [0]

        graph = {}
        for node_a, node_b in edges:
            graph.setdefault(node_a, set()).add(node_b)
            graph.setdefault(node_b, set()).add(node_a)

        # pruning leaves at each iteration
        nodes = set(graph)
        while len(nodes) > 2:
            leaves = {node for node in nodes if len(graph[node]) == 1}
            for leaf in leaves:
                for neighbor in graph[leaf]:
                    graph[neighbor].remove(leaf)
            nodes -= leaves
        return list(nodes)

    def findMinHeightTrees(self, n, edges):  # O(n) and O(n)
        # question is finding the diameter of the tree (longest path between two nodes)
        # find longest path, then middle index / indices
        if not edges:
            return [0]

        graph = {}
        for node_a, node_b in edges:
            graph.setdefault(node_a, set()).add(node_b)
            graph.setdefault(node_b, set()).add(node_a)

        def dfs(v: int, seen: set) -> List[int]:
            seen.add(v)
            return max(map(lambda x: dfs(x, seen), graph[v] - seen), default=[], key=len) + [v]

        path = dfs(dfs(0, set())[0], set())
        return path[(len(path) - 1) // 2:len(path) // 2 + 1]


def test():
    numbers = [4, 6, 1, 2, 6]
    arrays_of_numbers = [
        [[1, 0], [1, 2], [1, 3]],
        [[3, 0], [3, 1], [3, 2], [3, 4], [5, 4]],
        [],
        [[0, 1]],
        [[0, 1], [0, 2], [0, 3], [3, 4], [4, 5]],
        ]
    expectations = [
        [1],
        [3, 4],
        [0],
        [0, 1],
        [3],
        ]
    for n, edges, expected in zip(numbers, arrays_of_numbers, expectations):
        solution = Solution().findMinHeightTrees(n, edges)
        assert solution == expected
