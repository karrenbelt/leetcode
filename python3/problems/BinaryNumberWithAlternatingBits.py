### Source : https://leetcode.com/problems/binary-number-with-alternating-bits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given a positive integer, check whether it has alternating bits: namely, if two adjacent bits will 
# always have different values.
# 
# Example 1:
# 
# Input: n = 5
# Output: true
# Explanation: The binary representation of 5 is: 101
# 
# Example 2:
# 
# Input: n = 7
# Output: false
# Explanation: The binary representation of 7 is: 111.
# 
# Example 3:
# 
# Input: n = 11
# Output: false
# Explanation: The binary representation of 11 is: 1011.
# 
# Example 4:
# 
# Input: n = 10
# Output: true
# Explanation: The binary representation of 10 is: 1010.
# 
# Example 5:
# 
# Input: n = 3
# Output: false
# 
# Constraints:
# 
# 	1 <= n <= 231 - 1
#####################################################################################################


class Solution:
    def hasAlternatingBits(self, n: int) -> bool:
        return (number := bin(n)) and '00' not in number and '11' not in number

    def hasAlternatingBits(self, n: int) -> bool:
        return (number := n ^ (n >> 1)) and not (number & (number + 1))


def test():
    arguments = [5, 7, 11, 10, 3]
    expectations = [True, False, False, True, False]
    for n, expected in zip(arguments, expectations):
        solution = Solution().hasAlternatingBits(n)
        assert solution == expected
