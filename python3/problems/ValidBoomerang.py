### Source : https://leetcode.com/problems/valid-boomerang/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-30

##################################################################################################### 
#
# A boomerang is a set of 3 points that are all distinct and not in a straight line.
# 
# Given a list of three points in the plane, return whether these points are a boomerang.
# 
# Example 1:
# 
# Input: [[1,1],[2,3],[3,2]]
# Output: true
# 
# Example 2:
# 
# Input: [[1,1],[2,2],[3,3]]
# Output: false
# 
# Note:
# 
# 	points.length == 3
# 	points[i].length == 2
# 	0 <= points[i][j] <= 100
# 
#####################################################################################################

from typing import List


class Solution:
    def isBoomerang(self, points: List[List[int]]) -> bool:  # cross product (prevents division by zero)
        delta_x1 = points[0][0] - points[1][0]
        delta_y1 = points[0][1] - points[1][1]
        delta_x2 = points[0][0] - points[2][0]
        delta_y2 = points[0][1] - points[2][1]
        return bool(delta_x1 * delta_y2 - delta_x2 * delta_y1)  # area of the parallelogram

    def isBoomerang(self, points: List[List[int]]) -> bool:  # area of triangle
        return (p := points) and bool(p[0][0]*(p[1][1]-p[2][1]) + p[1][0]*(p[2][1]-p[0][1]) + p[2][0]*(p[0][1]-p[1][1]))

    def isBoomerang(self, points: List[List[int]]) -> bool:  # area of triangle: readable
        (x1, y1), (x2, y2), (x3, y3) = points
        return x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)


def test():
    arguments = [
        [[1, 1], [2, 3], [3, 2]],
        [[1, 1], [2, 2], [3, 3]],
        [[1, 2], [2, 3], [3, 4]],
        [[0, 0], [1, 3], [2, 9]],
        [[0, 0], [1, 1], [1, 1]],
    ]
    expectations = [True, False, False, True, False]
    for points, expected in zip(arguments, expectations):
        solution = Solution().isBoomerang(points)
        assert solution == expected
