### Source : https://leetcode.com/problems/crawler-log-folder/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-02

##################################################################################################### 
#
# The Leetcode file system keeps a log each time some user performs a change folder operation.
# 
# The operations are described below:
# 
# 	"../" : ove to the parent folder of the current folder. (If you are already in the main 
# folder, remain in the same folder).
# 	"./" : Remain in the same folder.
# 	"x/" : ove to the child folder named x (This folder is guaranteed to always exist).
# 
# You are given a list of strings logs where logs[i] is the operation performed by the user at the 
# ith step.
# 
# The file system starts in the main folder, then the operations in logs are performed.
# 
# Return the minimum number of operations needed to go back to the main folder after the change 
# folder operations.
# 
# Example 1:
# 
# Input: logs = ["d1/","d2/","../","d21/","./"]
# Output: 2
# Explanation: Use this change folder operation "../" 2 times and go back to the main folder.
# 
# Example 2:
# 
# Input: logs = ["d1/","d2/","./","d3/","../","d31/"]
# Output: 3
# 
# Example 3:
# 
# Input: logs = ["d1/","../","../","../"]
# Output: 0
# 
# Constraints:
# 
# 	1 <= logs.length <= 103
# 	2 <= logs[i].length <= 10
# 	logs[i] contains lowercase English letters, digits, '.', and '/'.
# 	logs[i] follows the format described in the statement.
# 	Folder names consist of lowercase English letters and digits.
#####################################################################################################


from typing import List

class Solution:
    def minOperations(self, logs: List[str]) -> int:
        depth = 0
        for log in logs:
            if log == './':
                continue
            elif log == '../':
                depth = max(0, depth - 1)
            else:
                depth += 1
        return depth

    def minOperations(self, logs: List[str]) -> int:  # O(n) time and O(n) space
        return (d := 0) or list(d := max(0, d - 1) if l == '../' else d if l == './' else d + 1 for l in logs)[-1]

    def minOperations(self, logs: List[str]) -> int:  # O(n) time and O(1) space
        from collections import deque
        return (d := 0) or deque((d := {"../": max(d - 1, 0), "./": d}.get(l, d + 1) for l in logs), maxlen=1).pop()

    def minOperations(self, logs: List[str]) -> int:  # O(n) time and O(1) space
        from functools import reduce
        return reduce(lambda depth, log: max(0, depth - 1) if log == '../' else depth + (log != './'), logs, 0)


def test():
    arguments = [
        ["d1/", "d2/", "../", "d21/", "./"],
        ["d1/", "d2/", "./", "d3/", "../", "d31/"],
        ["d1/", "../", "../", "../"],
    ]
    expectations = [2, 3, 0]
    for logs, expected in zip(arguments, expectations):
        solution = Solution().minOperations(logs)
        assert solution == expected
test()