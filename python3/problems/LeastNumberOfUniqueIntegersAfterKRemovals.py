### Source : https://leetcode.com/problems/least-number-of-unique-integers-after-k-removals/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-12

##################################################################################################### 
#
# Given an array of integers arr and an integer k. Find the least number of unique integers after 
# removing exactly k elements.
# 
# Example 1:
# 
# Input: arr = [5,5,4], k = 1
# Output: 1
# Explanation: Remove the single 4, only 5 is left.
# 
# Example 2:
# 
# Input: arr = [4,3,1,1,3,3,2], k = 3
# Output: 2
# Explanation: Remove 4, 2 and either one of the two 1s or three 3s. 1 and 3 will be left.
# 
# Constraints:
# 
# 	1 <= arr.length <= 105
# 	1 <= arr[i] <= 109
# 	0 <= k <= arr.length
#####################################################################################################

from typing import List


class Solution:
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:  # O(n log n) time and O(n) space
        d = {}
        for n in arr:
            d[n] = d[n] + 1 if n in d else 1
        counts = sorted(d.items(), key=lambda x: x[1], reverse=True)
        while counts and counts[-1][-1] <= k:
            k -= counts[-1][-1]
            counts.pop()
        return len(counts)

    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        import collections
        c = collections.Counter(arr)
        s = sorted(arr, key=lambda x: (c[x], x))
        return len(set(s[k:]))

    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:  # O(n log n) time and O(n) space
        import heapq
        import collections
        hp = [(val, key) for key, val in collections.Counter(arr).items()]
        heapq.heapify(hp)
        while k > 0:
            k -= heapq.heappop(hp)[0]
        return len(hp) + (k < 0)

    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:  # O(n) time and O(n) space
        import collections
        c = collections.Counter(arr)
        cnt = collections.Counter(c.values())
        remaining = len(c)
        for key in range(1, len(arr) + 1):
            if k >= key * cnt[key]:
                k -= key * cnt[key]
                remaining -= cnt[key]
            else:
                return remaining - k // key
        return remaining


def test():
    arguments = [
        ([5, 5, 4], 1),
        ([4, 3, 1, 1, 3, 3, 2], 3),
        ]
    expectations = [1, 2]
    for (arr, k), expected in zip(arguments, expectations):
        solution = Solution().findLeastNumOfUniqueInts(arr, k)
        assert solution == expected
