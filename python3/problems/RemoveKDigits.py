### Source : https://leetcode.com/problems/remove-k-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

##################################################################################################### 
#
# Given string num representing a non-negative integer num, and an integer k, return the smallest 
# possible integer after removing k digits from num.
# 
# Example 1:
# 
# Input: num = "1432219", k = 3
# Output: "1219"
# Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.
# 
# Example 2:
# 
# Input: num = "10200", k = 1
# Output: "200"
# Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain 
# leading zeroes.
# 
# Example 3:
# 
# Input: num = "10", k = 2
# Output: "0"
# Explanation: Remove all the digits from the number and it is left with nothing which is 0.
# 
# Constraints:
# 
# 	1 <= k <= num.length <= 105
# 	num consists of only digits.
# 	num does not have any leading zeros except for the zero itself.
#####################################################################################################


class Solution:
    def removeKdigits(self, num: str, k: int) -> str:  # O(n * k) time and O(1) space
        # repeatedly remove the peak element
        for _ in range(k):
            for i in range(len(num) - 1):
                if num[i] > num[i + 1]:
                    num = num[:i] + num[i + 1:]
                    break
            else:
                num = num[:-1]
        return str(int(num)) if num else "0"

    def removeKdigits(self, num: str, k: int) -> str:  # O(n) time and O(n) space
        # we append all numbers from left to right,
        # removing the right most one whenever the next one after is smaller, k or more times
        stack = []
        for n in num:
            while k and stack and stack[-1] > n:
                stack.pop()
                k -= 1
            stack.append(n)
        return ''.join(stack[:-k or None]).lstrip('0') or '0'


def test():
    arguments = [
        ("1432219", 3),
        ("10200", 1),
        ("10", 2),
        ]
    expectations = ["1219", "200", "0"]
    for (num, k), expected in zip(arguments, expectations):
        solution = Solution().removeKdigits(num, k)
        assert solution == expected
test()