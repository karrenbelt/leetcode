### Source : https://leetcode.com/problems/smallest-value-of-the-rearranged-number/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-22

##################################################################################################### 
#
# You are given an integer num. Rearrange the digits of num such that its value is minimized and it 
# does not contain any leading zeros.
# 
# Return the rearranged number with minimal value.
# 
# Note that the sign of the number does not change after rearranging the digits.
# 
# Example 1:
# 
# Input: num = 310
# Output: 103
# Explanation: The possible arrangements for the digits of 310 are 013, 031, 103, 130, 301, 310. 
# The arrangement with the smallest value that does not contain any leading zeros is 103.
# 
# Example 2:
# 
# Input: num = -7605
# Output: -7650
# Explanation: Some possible arrangements for the digits of -7605 are -7650, -6705, -5076, -0567.
# The arrangement with the smallest value that does not contain any leading zeros is -7650.
# 
# Constraints:
# 
# 	-1015 <= num <= 1015
#####################################################################################################


class Solution:
    def smallestNumber(self, num: int) -> int:
        s, d = str(abs(num)), dict(zip("0123456789", "1023456789"))
        ns = "".join(sorted(s.replace("0", ""), key=d.get, reverse=num < 0))
        zeroes = "0" * s.count("0")
        return int(ns[0] + zeroes + ns[1:]) if num > 0 else - int(ns + zeroes)

    def smallestNumber(self, num: int) -> int:
        s = "".join(sorted(str(abs(num)).replace("0", ""), reverse=num < 0))
        zeroes = "0" * (len("%i" % num) - len(s) - (num < 0))
        return int(s[0] + zeroes + s[1:]) if num > 0 else - int(s + zeroes)

    def smallestNumber(self, num: int) -> int:

        digits, n = [0] * 10, abs(num)
        while n:
            n, i = divmod(n, 10)
            digits[i] += 1

        smallest_number = 0
        if num < 0:
            for i in reversed(range(10)):
                while digits[i]:
                    smallest_number = smallest_number * 10 + i
                    digits[i] -= 1
            return - smallest_number

        for i in range(1, 10):
            while digits[i]:
                smallest_number = smallest_number * 10 + i
                digits[i] -= 1
                while digits[0]:
                    smallest_number *= 10
                    digits[0] -= 1

        return smallest_number

    def smallestNumber(self, num: int) -> int:
        s = sorted(str(abs(num)))
        if num <= 0:
            return - int("".join(reversed(s)))
        i = next(i for i, n in enumerate(s) if n > "0")
        s[0], s[i] = s[i], s[0]
        return int("".join(s))


def test():
    arguments = [310, -7605]
    expectations = [103, -7650]
    for num, expected in zip(arguments, expectations):
        solution = Solution().smallestNumber(num)
        assert solution == expected
