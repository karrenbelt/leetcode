### Source : https://leetcode.com/problems/longest-univalue-path/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

##################################################################################################### 
#
# Given the root of a binary tree, return the length of the longest path, where each node in the path 
# has the same value. This path may or may not pass through the root.
# 
# The length of the path between two nodes is represented by the number of edges between them.
# 
# Example 1:
# 
# Input: root = [5,4,5,1,1,5]
# Output: 2
# 
# Example 2:
# 
# Input: root = [1,4,5,4,4,5]
# Output: 2
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 104].
# 	-1000 <= Node.val <= 1000
# 	The depth of the tree will not exceed 1000.
#####################################################################################################

from typing import Tuple
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def longestUnivaluePath(self, root: TreeNode) -> int:  # O(n) time

        def traverse(node: TreeNode):
            nonlocal longest
            if not node:
                return 0
            left_length, right_length = traverse(node.left), traverse(node.right)
            left = (left_length + 1) if node.left and node.left.val == node.val else 0
            right = (right_length + 1) if node.right and node.right.val == node.val else 0
            longest = max(longest, left + right)
            return max(left, right)

        longest = 0
        traverse(root)
        return longest

    def longestUnivaluePath(self, root: TreeNode) -> int:  # O(n) time

        def traverse(node: TreeNode) -> Tuple[int, int]:
            if not node:
                return 0, 0
            left_length, l2 = traverse(node.left)
            r1, r2 = traverse(node.right)
            l2 = l2 + 1 if node.left and node.left.val == node.val else 0
            r2 = r2 + 1 if node.right and node.right.val == node.val else 0
            return max(left_length, r1, l2 + r2), max(l2, r2)

        return traverse(root)[0]


def test():
    from python3 import Codec
    arguments = [
        "[5,4,5,1,1,5]",
        "[1,4,5,4,4,5]",
        "[5,4,5,4,4,5,3,4,4,null,null,null,4,null,null,4,null,null,4,null,4,4,null,null,4,4]",
    ]
    expectations = [2, 2, 6]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().longestUnivaluePath(root)
        assert solution == expected
test()