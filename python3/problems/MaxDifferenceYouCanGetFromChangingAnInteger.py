### Source : https://leetcode.com/problems/max-difference-you-can-get-from-changing-an-integer/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# You are given an integer num. You will apply the following steps exactly two times:
# 
# 	Pick a digit x (0 <= x <= 9).
# 	Pick another digit y (0 <= y <= 9). The digit y can be equal to x.
# 	Replace all the occurrences of x in the decimal representation of num by y.
# 	The new integer cannot have any leading zeros, also the new integer cannot be 0.
# 
# Let a and b be the results of applying the operations to num the first and second times, 
# respectively.
# 
# Return the max difference between a and b.
# 
# Example 1:
# 
# Input: num = 555
# Output: 888
# Explanation: The first time pick x = 5 and y = 9 and store the new integer in a.
# The second time pick x = 5 and y = 1 and store the new integer in b.
# We have now a = 999 and b = 111 and max difference = 888
# 
# Example 2:
# 
# Input: num = 9
# Output: 8
# Explanation: The first time pick x = 9 and y = 9 and store the new integer in a.
# The second time pick x = 9 and y = 1 and store the new integer in b.
# We have now a = 9 and b = 1 and max difference = 8
# 
# Example 3:
# 
# Input: num = 123456
# Output: 820000
# 
# Example 4:
# 
# Input: num = 10000
# Output: 80000
# 
# Example 5:
# 
# Input: num = 9288
# Output: 8700
# 
# Constraints:
# 
# 	1 <= num <= 108
#####################################################################################################


class Solution:
    def maxDiff(self, num: int) -> int:
        smallest = largest = num
        s = str(num)
        for i, j in ((i, j) for i in '0123456789' for j in '0123456789' if i != j):
            new = s.replace(str(i), str(j))
            if not new.startswith('0'):
                smallest, largest = min(smallest, int(new)), max(largest, int(new))
        return largest - smallest

    def maxDiff(self, num: int) -> int:
        s = str(num)
        # largest
        highest = '9'
        for x in s:
            if x == '9':
                continue
            highest = x
            break
        largest = int(s.replace(highest, '9'))
        # smallest
        lowest = new_lowest = '1'
        for i, x in enumerate(s):
            if i == 0:
                if x == '1':
                    continue
                lowest = x
                break
            elif x != s[0] and x != '0':
                lowest = x
                new_lowest = '0'
                break
        smallest = int(s.replace(lowest, new_lowest))
        return largest - smallest


def test():
    arguments = [555, 9, 123456, 10000, 9288]
    expectations = [888, 8, 820000, 80000, 8700]
    for num, expected in zip(arguments, expectations):
        solution = Solution().maxDiff(num)
        assert solution == expected
