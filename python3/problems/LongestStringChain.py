### Source : https://leetcode.com/problems/longest-string-chain/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-17

##################################################################################################### 
#
# Given a list of words, each word consists of English lowercase letters.
# 
# Let's say word1 is a predecessor of word2 if and only if we can add exactly one letter anywhere in 
# word1 to make it equal to word2. For example, "abc" is a predecessor of "abac".
# 
# A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >= 1, where word_1 is a 
# predecessor of word_2, word_2 is a predecessor of word_3, and so on.
# 
# Return the longest possible length of a word chain with words chosen from the given list of words.
# 
# Example 1:
# 
# Input: words = ["a","b","ba","bca","bda","bdca"]
# Output: 4
# Explanation: One of the longest word chain is "a","ba","bda","bdca".
# 
# Example 2:
# 
# Input: words = ["xbc","pcxbcf","xb","cxbc","pcxbc"]
# Output: 5
# 
# Constraints:
# 
# 	1 <= words.length <= 1000
# 	1 <= words[i].length <= 16
# 	words[i] only consists of English lowercase letters.
#####################################################################################################

from typing import List


class Solution:
    def longestStrChain(self, words: List[str]) -> int:

        def can_succeed(child: str, parent: str) -> bool:  # assumed same length as I group them
            i = j = mismatched = 0
            while i < len(parent):
                if parent[i] != child[j]:
                    if mismatched:  # at most 1 mis-match
                        return False
                    mismatched = 1
                    j += 1
                else:
                    i += 1
                    j += 1
            return True

        word_groups = {}
        for word in words:
            word_groups.setdefault(len(word), set()).add(word)

        longest = 0  # longest chain doesn't need to start with minimum length
        for word_length, queue in word_groups.items():
            chain_length = 0
            while queue:
                chain_length += 1
                queue = {child for child in word_groups.get(len(next(iter(queue))) + 1, set())
                         if any(can_succeed(child, node) for node in queue)}
                word_groups[word_length] -= queue  # shorten as to not iterate these nodes again
            longest = max(longest, chain_length)
        return longest

    def longestStrChain(self, words: List[str]) -> int:  # O(n * k) time
        words.sort(key=len)
        dp = {word: 1 for word in words}
        longest = 1
        for word in words:  # O(n)
            for i in range(len(word)):  # O(k)
                predecessor = word[:i] + word[i+1:]
                if predecessor in dp:
                    dp[word] = dp[predecessor] + 1
                    longest = max(longest, dp[word])
        return longest


def test():
    arguments = [
        ["a", "b", "ba", "bca", "bda", "bdca"],
        ["xbc", "pcxbcf", "xb", "cxbc", "pcxbc"],
        ["a", "b", "ab", "bac"],
        ["abcd", "dbqca"],
        ["ksqvsyq", "ks", "kss", "czvh", "zczpzvdhx", "zczpzvh", "zczpzvhx", "zcpzvh", "zczvh", "gr", "grukmj",
         "ksqvsq", "gruj", "kssq", "ksqsq", "grukkmj", "grukj", "zczpzfvdhx", "gru"],
        ["bdca", "bda", "ca", "dca", "a"],
    ]
    expectations = [4, 5, 2, 1, 7, 4]
    for words, expected in zip(arguments, expectations):
        solution = Solution().longestStrChain(words)
        assert solution == expected
