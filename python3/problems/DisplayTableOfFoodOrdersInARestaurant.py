### Source : https://leetcode.com/problems/display-table-of-food-orders-in-a-restaurant/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-20

##################################################################################################### 
#
# Given the array orders, which represents the orders that customers have done in a restaurant. ore 
# specifically orders[i]=[customerNamei,tableNumberi,foodItemi] where customerNamei is the name of 
# the customer, tableNumberi is the table customer sit at, and foodItemi is the item customer orders.
# 
# Return the restaurant's "display table&rdquo;. The "display table&rdquo; is a table whose row 
# entries denote how many of each food item each table ordered. The first column is the table number 
# and the remaining columns correspond to each food item in alphabetical order. The first row should 
# be a header whose first column is "Table&rdquo;, followed by the names of the food items. Note that 
# the customer names are not part of the table. Additionally, the rows should be sorted in 
# numerically increasing order.
# 
# Example 1:
# 
# Input: orders = [["David","3","Ceviche"],["Corina","10","Beef Burrito"],["David","3","Fried 
# Chicken"],["Carla","5","Water"],["Carla","5","Ceviche"],["Rous","3","Ceviche"]]
# Output: [["Table","Beef Burrito","Ceviche","Fried 
# Chicken","Water"],["3","0","2","1","0"],["5","0","1","0","1"],["10","1","0","0","0"]] 
# Explanation:
# The displaying table looks like:
# Table,Beef Burrito,Ceviche,Fried Chicken,Water
# 3    ,0           ,2      ,1            ,0
# 5    ,0           ,1      ,0            ,1
# 10   ,1           ,0      ,0            ,0
# For the table 3: David orders "Ceviche" and "Fried Chicken", and Rous orders "Ceviche".
# For the table 5: Carla orders "Water" and "Ceviche".
# For the table 10: Corina orders "Beef Burrito". 
# 
# Example 2:
# 
# Input: orders = [["James","12","Fried Chicken"],["Ratesh","12","Fried 
# Chicken"],["Amadeus","12","Fried Chicken"],["Adam","1","Canadian Waffles"],["Brianna","1","Canadian 
# Waffles"]]
# Output: [["Table","Canadian Waffles","Fried Chicken"],["1","2","0"],["12","0","3"]] 
# Explanation: 
# For the table 1: Adam and Brianna order "Canadian Waffles".
# For the table 12: James, Ratesh and Amadeus order "Fried Chicken".
# 
# Example 3:
# 
# Input: orders = [["Laura","2","Bean Burrito"],["Jhon","2","Beef Burrito"],["elissa","2","Soda"]]
# Output: [["Table","Bean Burrito","Beef Burrito","Soda"],["2","1","1","1"]]
# 
# Constraints:
# 
# 	1 <= orders.length <= 5 * 104
# 	orders[i].length == 3
# 	1 <= customerNamei.length, foodItemi.length <= 20
# 	customerNamei and foodItemi consist of lowercase and uppercase English letters and the 
# space character.
# 	tableNumberi is a valid integer between 1 and 500.
#####################################################################################################

from typing import List


class Solution:
    def displayTable(self, orders: List[List[str]]) -> List[List[str]]:
        from sortedcontainers import SortedDict, SortedSet

        table, foods = SortedDict(), SortedSet()
        for (_, table_number, food_item) in orders:
            items = table.setdefault(int(table_number), {})
            items[food_item] = items.get(food_item, 0) + 1
            foods.add(food_item)

        display_table = [["Table"] + list(foods)]
        for table_number, table_order in table.items():
            row = [str(table_number)]
            for food_item in foods:
                row.append(str(table_order.get(food_item, 0)))
            display_table.append(row)

        return display_table


def test():
    arguments = [
        [["David", "3", "Ceviche"], ["Corina", "10", "Beef Burrito"],
         ["David", "3", "Fried Chicken"], ["Carla", "5", "Water"],
         ["Carla", "5", "Ceviche"], ["Rous", "3", "Ceviche"]],
        [["James", "12", "Fried Chicken"], ["Ratesh", "12", "Fried Chicken"],
         ["Amadeus", "12", "Fried Chicken"], ["Adam", "1", "Canadian Waffles"],
         ["Brianna", "1", "Canadian Waffles"]],
        [["Laura", "2", "Bean Burrito"], ["Jhon", "2", "Beef Burrito"],
         ["Melissa", "2", "Soda"]],
        ]
    expectations = [
        [["Table", "Beef Burrito", "Ceviche", "Fried Chicken", "Water"],
         ["3", "0", "2", "1", "0"], ["5", "0", "1", "0", "1"],
         ["10", "1", "0", "0", "0"]],
        [["Table", "Canadian Waffles", "Fried Chicken"], ["1", "2", "0"],
         ["12", "0", "3"]],
        [["Table", "Bean Burrito", "Beef Burrito", "Soda"],
         ["2", "1", "1", "1"]]
    ]
    for orders, expected in zip(arguments, expectations):
        solution = Solution().displayTable(orders)
        assert solution == expected, (solution, expected)
