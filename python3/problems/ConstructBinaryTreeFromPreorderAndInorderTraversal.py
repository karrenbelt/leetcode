### Source : https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given preorder and inorder traversal of a tree, construct the binary tree.
# 
# Note:
# You may assume that duplicates do not exist in the tree.
# 
# For example, given
# 
# preorder = [3,9,20,15,7]
# inorder = [9,3,15,20,7]
# 
# Return the following binary tree:
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
#####################################################################################################

from typing import List, Deque
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        if not inorder:
            return None
        root_idx = inorder.index(preorder.pop(0))  # slow
        root = TreeNode(inorder[root_idx])
        root.left = self.buildTree(preorder, inorder[:root_idx])
        root.right = self.buildTree(preorder, inorder[root_idx + 1:])
        return root

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        from collections import deque

        def traverse(left: int, right: int):
            if left > right:
                return None
            val = preorder.popleft()
            index = idx_map[val]
            node = TreeNode(val)
            node.left = traverse(left, index - 1)
            node.right = traverse(index + 1, right)
            return node

        idx_map = {n: i for i, n in enumerate(inorder)}
        preorder = deque(preorder)
        return traverse(0, len(preorder) - 1)


def test():
    from python3 import Codec
    arrays_of_numbers = [
        ([3, 9, 20, 15, 7], [9, 3, 15, 20, 7]),
        ([-1], [-1]),
        ]
    expectations = [
        "[3,9,20,null,null,15,7]",
        "[-1]",
        ]
    for (preorder, inorder), expected in zip(arrays_of_numbers, expectations):
        solution = Solution().buildTree(preorder, inorder)
        assert solution == Codec.deserialize(expected)
