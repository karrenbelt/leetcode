### Source : https://leetcode.com/problems/search-a-2d-matrix-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Write an efficient algorithm that searches for a target value in an m x n integer matrix. The 
# matrix has the following properties:
# 
# 	Integers in each row are sorted in ascending from left to right.
# 	Integers in each column are sorted in ascending from top to bottom.
# 
# Example 1:
# 
# Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], 
# target = 5
# Output: true
# 
# Example 2:
# 
# Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], 
# target = 20
# Output: false
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= n, m <= 300
# 	-109 <= matix[i][j] <= 109
# 	All the integers in each row are sorted in ascending order.
# 	All the integers in each column are sorted in ascending order.
# 	-109 <= target <= 109
#####################################################################################################

from typing import List


class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        j = -1
        for row in matrix:
            while j + len(row) and row[j] > target:
                j -= 1
            if row[j] == target:
                return True
        return False

    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        if not len(matrix) or not len(matrix[0]):
            return False
        h, w = len(matrix), len(matrix[0])
        y, x = h - 1, 0
        while True:
            if y < 0 or x >= w:
                return False
            elif target < matrix[y][x]:
                y -= 1
            elif target > matrix[y][x]:
                x += 1
            else:
                return True

    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        return any(target in row for row in matrix)


def test():
    matrices = [
        [[1, 4, 7, 11, 15], [2, 5, 8, 12, 19], [3, 6, 9, 16, 22], [10, 13, 14, 17, 24], [18, 21, 23, 26, 30]],
        [[1, 4, 7, 11, 15], [2, 5, 8, 12, 19], [3, 6, 9, 16, 22], [10, 13, 14, 17, 24], [18, 21, 23, 26, 30]],
        ]
    targets = [5, 20]
    expectations = [True, False]
    for matrix, target, expected in zip(matrices, targets, expectations):
        solution = Solution().searchMatrix(matrix, target)
        assert solution == expected
