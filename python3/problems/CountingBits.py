### Source : https://leetcode.com/problems/counting-bits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-07

##################################################################################################### 
#
# Given an integer num, return an array of the number of 1's in the binary representation of every 
# number in the range [0, num].
# 
# Example 1:
# 
# Input: num = 2
# Output: [0,1,1]
# Explanation:
# 0 --> 0
# 1 --> 1
# 2 --> 10
# 
# Example 2:
# 
# Input: num = 5
# Output: [0,1,1,2,1,2]
# Explanation:
# 0 --> 0
# 1 --> 1
# 2 --> 10
# 3 --> 11
# 4 --> 100
# 5 --> 101
# 
# Constraints:
# 
# 	0 <= num <= 105
# 
# Follow up:
# 
# 	It is very easy to come up with a solution with run time O(32n). Can you do it in linear 
# time O(n) and possibly in a single pass?
# 	Could you solve it in O(n) space complexity?
# 	Can you do it without using any built-in function (i.e., like __builtin_popcount in C++)?
#####################################################################################################

from typing import List


class Solution:
    def countBits(self, num: int) -> List[int]:

        def count_bits(n: int):
            ctr = 0
            while n:
                ctr += n & 1
                n >>= 1
            return ctr

        return list(map(count_bits, range(num + 1)))

    def countBits(self, num: int) -> List[int]:
        dp = [0] * (num + 1)
        for i in range(num + 1):
            dp[i] = dp[i >> 1] + (i & 1)
        return dp


def test():
    arguments = [2, 5]
    expectations = [
        [0, 1, 1],
        [0, 1, 1, 2, 1, 2],
        ]
    for num, expected in zip(arguments, expectations):
        solution = Solution().countBits(num)
        assert solution == expected
