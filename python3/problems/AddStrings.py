### Source : https://leetcode.com/problems/add-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-14

##################################################################################################### 
#
# Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and 
# num2.
# 
# Note:
# 
# The length of both num1 and num2 is 
# Both num1 and num2 contains only digits 0-9.
# Both num1 and num2 does not contain any leading zero.
# You must not use any built-in BigInteger library or convert the inputs to integer directly.
# 
#####################################################################################################


class Solution:
    def addStrings(self, num1: str, num2: str) -> str:
        # without calling int...
        n1 = n2 = 0
        for i in range(len(num1)):
            n1 = n1*10 + ord(num1[i])-48  # 48 = ord('0')
        for i in range(len(num2)):
            n2 = n2*10 + ord(num2[i])-48
        return str(n1+n2)

    def addStrings(self, num1: str, num2: str) -> str:
        return str(int(num1) + int(num2))


def test():
    pairs_of_numeric_strings = [
        ("388", "13"),
        ]
    expectations = ["401"]
    for (num1, num2), expected in zip(pairs_of_numeric_strings, expectations):
        solution = Solution().addStrings(num1, num2)
        assert solution == expected
