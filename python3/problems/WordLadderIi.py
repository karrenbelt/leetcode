### Source : https://leetcode.com/problems/word-ladder-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# A transformation sequence from word beginWord to word endWord using a dictionary wordList is a 
# sequence of words beginWord -> s1 -> s2 -> ... -> sk such that:
# 
# 	Every adjacent pair of words differs by a single letter.
# 	Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in 
# wordList.
# 	sk == endWord
# 
# Given two words, beginWord and endWord, and a dictionary wordList, return all the shortest 
# transformation sequences from beginWord to endWord, or an empty list if no such sequence exists. 
# Each sequence should be returned as a list of the words [beginWord, s1, s2, ..., sk].
# 
# Example 1:
# 
# Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
# Output: [["hit","hot","dot","dog","cog"],["hit","hot","lot","log","cog"]]
# Explanation: There are 2 shortest transformation sequences:
# "hit" -> "hot" -> "dot" -> "dog" -> "cog"
# "hit" -> "hot" -> "lot" -> "log" -> "cog"
# 
# Example 2:
# 
# Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
# Output: []
# Explanation: The endWord "cog" is not in wordList, therefore there is no valid transformation 
# sequence.
# 
# Constraints:
# 
# 	1 <= beginWord.length <= 7
# 	endWord.length == beginWord.length
# 	1 <= wordList.length <= 5000
# 	wordList[i].length == beginWord.length
# 	beginWord, endWord, and wordList[i] consist of lowercase English letters.
# 	beginWord != endWord
# 	All the words in wordList are unique.
#####################################################################################################

from typing import List


class Solution:
    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:
        # same code submitted: apparently some extreme test case removed
        # 04/20/2021 13:37	Accepted	2248 ms	18.5 MB	python3
        # 07/25/2021 10:02	Accepted	84 ms	14.7 MB	python3
        words = set(wordList)
        letters = set(''.join(wordList))
        paths = []
        level = [(beginWord, [beginWord])]
        while level:
            if paths:
                break
            next_level = []
            for word, path in level:
                if word == endWord:
                    paths.append(path)
                for i in range(len(word)):
                    for c in letters:
                        next_word = word[:i] + c + word[i + 1:]
                        if next_word in words:
                            next_level.append((next_word, path + [next_word]))
            words -= set(map(lambda x: x[0], next_level))
            level = next_level
        return paths

    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:  # 72 ms
        words, letters = set(wordList), list(map(set, zip(*wordList)))
        queue, found = [[beginWord]], False
        paths = []
        while queue and endWord in words:
            next_queue = []
            for path in queue:
                word = path[-1]
                for i in range(len(word)):
                    for c in letters[i]:
                        next_word = word[:i] + c + word[i + 1:]
                        if next_word == endWord:
                            paths.append(path + [next_word])
                        elif next_word in words:
                            next_queue.append(path + [next_word])
            if paths:
                break
            queue = next_queue
            words -= set(map(lambda x: x[-1], queue))
        return paths


def test():
    from collections import Counter
    arguments = [
        ("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"]),
        ("hit", "cog", ["hot", "dot", "dog", "lot", "log"]),
        ("a", "c", ["a", "b", "c"]),
        ("hot", "dot", ["hot", "dot", "dog"])
        ]
    expectations = [
        [["hit", "hot", "dot", "dog", "cog"], ["hit", "hot", "lot", "log", "cog"]],
        [],
        [["a", "c"]],
        [["hot", "dot"]],
        ]
    for (beginWord, endWord, wordList), expected in zip(arguments, expectations):
        solution = Solution().findLadders(beginWord, endWord, wordList)
        print(solution)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected))
