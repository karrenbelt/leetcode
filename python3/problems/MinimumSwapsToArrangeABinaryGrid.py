### Source : https://leetcode.com/problems/minimum-swaps-to-arrange-a-binary-grid/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-07

##################################################################################################### 
#
# Given an n x n binary grid, in one step you can choose two adjacent rows of the grid and swap them.
# 
# A grid is said to be valid if all the cells above the main diagonal are zeros.
# 
# Return the minimum number of steps needed to make the grid valid, or -1 if the grid cannot be valid.
# 
# The main diagonal of a grid is the diagonal that starts at cell (1, 1) and ends at cell (n, n).
# 
# Example 1:
# 
# Input: grid = [[0,0,1],[1,1,0],[1,0,0]]
# Output: 3
# 
# Example 2:
# 
# Input: grid = [[0,1,1,0],[0,1,1,0],[0,1,1,0],[0,1,1,0]]
# Output: -1
# Explanation: All rows are similar, swaps have no effect on the grid.
# 
# Example 3:
# 
# Input: grid = [[1,0,0],[1,1,0],[1,1,1]]
# Output: 0
# 
# Constraints:
# 
# 	n == grid.length
# 	n == grid[i].length
# 	1 <= n <= 200
# 	grid[i][j] is 0 or 1
#####################################################################################################

from typing import List


class Solution:
    def minSwaps(self, grid: List[List[int]]) -> int:
        # we count the zeros per row at the right end, if we don't have 1 + i at each row_i, we return -1
        # otherwise we perform bubble sort and count the swaps

        def count_trailing_zeroes(matrix: List[List[int]]):
            array = [0] * len(matrix)
            for i, row in enumerate(matrix):
                j = 0
                while j < len(row) and row[-j - 1] == 0:
                    j += 1
                array[i] = j
            return array

        # def bubble_sort(nums: List[int]) -> int:
        #     # sort in place and return swap count
        #     ctr, swaps = 0, -1
        #     while swaps != 0:
        #         swaps = 0
        #         for i in range(len(nums) - 1):
        #             if nums[i] < nums[i + 1]:
        #                 swaps += 1
        #                 nums[i], nums[i + 1] = nums[i + 1], nums[i]
        #         ctr += swaps
        #     return ctr

        def bubble_sort(nums: List[int]) -> int:  # O(n^2) time
            # sort in place and return swap count or -1 if not possible
            ctr = 0
            for i in range(len(grid)):
                if nums[i] < len(grid) - i - 1:
                    j = i  # from this row, find the next with sufficient zeroes
                    while j < len(grid) and nums[j] < len(grid) - i - 1:
                        j += 1
                    if j == len(grid):
                        return -1
                    while j > i:  # flip rows from the row with sufficient zeroes (j) is it the current (i) place
                        nums[j], nums[j - 1] = nums[j - 1], nums[j]
                        j -= 1
                        ctr += 1
            return ctr

        trailing_zeroes = count_trailing_zeroes(grid)
        return bubble_sort(trailing_zeroes)


def test():
    arguments = [
        [[0, 0, 1], [1, 1, 0], [1, 0, 0]],
        [[0, 1, 1, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 1, 1, 0]],
        [[1, 0, 0], [1, 1, 0], [1, 1, 1]],
        [[0, 1, 1, 0], [1, 1, 1, 0], [1, 1, 1, 0], [1, 0, 0, 0]],
        [[1, 0, 0, 0, 0, 0], [0, 1, 0, 1, 0, 0], [1, 0, 0, 0, 0, 0], [1, 1, 1, 0, 0, 0], [1, 1, 0, 1, 0, 0],
         [1, 0, 0, 0, 0, 0]],
    ]
    expectations = [3, -1, 0, -1, 2]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().minSwaps(grid)
        assert solution == expected, (expected, solution)
