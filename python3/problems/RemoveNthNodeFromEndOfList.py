### Source : https://leetcode.com/problems/remove-nth-node-from-end-of-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-20

##################################################################################################### 
#
# Given the head of a linked list, remove the nth node from the end of the list and return its head.
# 
# Follow up: Could you do this in one pass?
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5], n = 2
# Output: [1,2,3,5]
# 
# Example 2:
# 
# Input: head = [1], n = 1
# Output: []
# 
# Example 3:
# 
# Input: head = [1,2], n = 1
# Output: [1]
# 
# Constraints:
# 
# 	The number of nodes in the list is sz.
# 	1 <= sz <= 30
# 	0 <= Node.val <= 100
# 	1 <= n <= sz
#####################################################################################################

from python3 import SinglyLinkedList as ListNode


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:  # O(n) time and O(n) space
        node, stack = head, []
        while node:
            stack.append(node)
            node = node.next
        index = len(stack) - n
        if index == 0:
            return head.next
        else:
            prev = stack[index - 1]
            prev.next = stack[index].next
        return head

    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:  # O(n) time and O(1) space
        dummy = fast = slow = ListNode(0, next=head)
        for _ in range(n):
            fast = fast.next
        while fast and fast.next:
            slow, fast = slow.next, fast.next
        slow.next = slow.next.next
        return dummy.next

    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:  # O(n) time and O(1) space - no dummy node
        fast = slow = head
        for _ in range(n):
            fast = fast.next
        if not fast:
            return head.next
        while fast.next:
            slow, fast = slow.next, fast.next
        slow.next = slow.next.next
        return head


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        [1, 2, 3, 4, 5],
        [1],
        [1, 2]
        ]
    nth_nodes = [2, 1, 1]
    expectations = [
        [1, 2, 3, 5],
        [],
        [1]
        ]
    for nums, n, expected in zip(arrays_of_numbers, nth_nodes, expectations):
        head = singly_linked_list_from_array(nums).head
        solution = Solution().removeNthFromEnd(head, n)
        assert solution == singly_linked_list_from_array(expected).head
