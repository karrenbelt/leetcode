### Source : https://leetcode.com/problems/contains-duplicate-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-28

##################################################################################################### 
#
# Given an integer array nums and two integers k and t, return true if there are two distinct indices 
# i and j in the array such that abs(nums[i] - nums[j]) <= t and abs(i - j) <= k.
# 
# Example 1:
# Input: nums = [1,2,3,1], k = 3, t = 0
# Output: true
# Example 2:
# Input: nums = [1,0,1,1], k = 1, t = 2
# Output: true
# Example 3:
# Input: nums = [1,5,9,1,5,9], k = 2, t = 3
# Output: false
# 
# Constraints:
# 
# 	0 <= nums.length <= 2 * 104
# 	-231 <= nums[i] <= 231 - 1
# 	0 <= k <= 104
# 	0 <= t <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def containsNearbyAlmostDuplicate(self, nums: List[int], k: int, t: int) -> bool:  # O(n) time and O(n) space
        # bucket sort: if two items have a difference <= t, they will fall in the same or neighboring buckets
        buckets = {}
        for i, n in enumerate(nums):
            bucket = n // (t + 1)
            if any(bucket + j in buckets and abs(buckets[bucket + j] - n) <= t for j in (-1, 0, 1)):
                return True
            buckets[bucket] = n
            if i >= k:
                expired = nums[i - k] // (t + 1)
                del buckets[expired]
        return False


def test():
    arguments = [
        ([1, 2, 3, 1], 3, 0),
        ([1, 0, 1, 1], 1, 2),
        ([1, 5, 9, 1, 5, 9], 2, 3)
    ]
    expectations = [True, True, False]
    for (nums, k, t), expected in zip(arguments, expectations):
        solution = Solution().containsNearbyAlmostDuplicate(nums, k, t)
        assert solution == expected
