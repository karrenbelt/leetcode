### Source : https://leetcode.com/problems/is-subsequence/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-12

##################################################################################################### 
#
# Given a string s and a string t, check if s is subsequence of t.
# 
# A subsequence of a string is a new string which is formed from the original string by deleting some 
# (can be none) of the characters without disturbing the relative positions of the remaining 
# characters. (ie, "ace" is a subsequence of "abcde" while "aec" is not).
# 
# Follow up:
# If there are lots of incoming S, say S1, S2, ... , Sk where k >= 1B, and you want to check one by 
# one to see if T has its subsequence. In this scenario, how would you change your code?
# 
# Credits:
# Special thanks to @pbrother for adding this problem and creating all test cases.
# 
# Example 1:
# Input: s = "abc", t = "ahbgdc"
# Output: true
# Example 2:
# Input: s = "axc", t = "ahbgdc"
# Output: false
# 
# Constraints:
# 
# 	0 <= s.length <= 100
# 	0 <= t.length <= 104
# 	Both strings consists only of lowercase characters.
#####################################################################################################


class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        index = 0
        for c in s:
            index = t.find(c, index) + 1
            if not index:
                return False
        return True

    def isSubsequence(self, s: str, t: str) -> bool:
        import re
        for c in s:
            try:
                t = t[re.search(c, t).start() + 1:]
            except AttributeError:
                return False
        return True

    def isSubsequence(self, s: str, t: str) -> bool:  # two-pointer: O(n) time & O(1) space
        p1 = p2 = 0
        while p1 < len(s) and p2 < len(t):
            p1 += int(s[p1] == t[p2])
            p2 += 1
        return p1 == len(s)

    def isSubsequence(self, s: str, t: str) -> bool:  # O(n) time and O(1) space
        t = iter(t)
        return all(c in t for c in s)

    def isSubsequence(self, s: str, t: str) -> bool:  # binary search
        import bisect
        if len(s) > len(t) or set(s).difference(t):
            return False

        indices = {}
        for i, c in enumerate(t):
            indices.setdefault(c, []).append(i)

        prev = -1
        for c in s:
            idx = bisect.bisect_right(indices[c], prev)
            if idx == len(indices[c]):
                return False
            prev = indices[c][idx]
        return True


def test():
    pairs_of_strings = [
        ("abc", "ahbgdc"),
        ("axc", "ahbgdc"),
        ]
    expectations = [True, False]
    for (s, t), expected in zip(pairs_of_strings, expectations):
        solution = Solution().isSubsequence(s, t)
        assert solution == expected
