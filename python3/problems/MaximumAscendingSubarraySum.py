### Source : https://leetcode.com/problems/maximum-ascending-subarray-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# Given an array of positive integers nums, return the maximum possible sum of an ascending subarray 
# in nums.
# 
# A subarray is defined as a contiguous sequence of numbers in an array.
# 
# A subarray [numsl, numsl+1, ..., numsr-1, numsr] is ascending if for all i where l <= i < r, numsi  
# < numsi+1. Note that a subarray of size 1 is ascending.
# 
# Example 1:
# 
# Input: nums = [10,20,30,5,10,50]
# Output: 65
# Explanation: [5,10,50] is the ascending subarray with the maximum sum of 65.
# 
# Example 2:
# 
# Input: nums = [10,20,30,40,50]
# Output: 150
# Explanation: [10,20,30,40,50] is the ascending subarray with the maximum sum of 150.
# 
# Example 3:
# 
# Input: nums = [12,17,15,13,10,11,12]
# Output: 33
# Explanation: [10,11,12] is the ascending subarray with the maximum sum of 33.
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	1 <= nums[i] <= 100
#####################################################################################################

from typing import List, Iterable


class Solution:
    def maxAscendingSum(self, nums: List[int]) -> int:  # O(n)
        maximum = running = 0
        for i, n in enumerate(nums):
            if i == 0 or nums[i - 1] < n:
                running += n
            else:
                running = n
            maximum = max(maximum, running)
        return maximum

    def maxAscendingSum(self, nums: List[int]) -> int:
        from itertools import groupby, starmap

        def pairwise(iterable: Iterable):
            items = iter(iterable)
            last = next(items, None)
            for item in items:
                yield last, item
                last = item

        i, m = 0, nums[0]
        for b, it in groupby(starmap(int.__lt__, pairwise(nums))):
            length = len(list(it))
            m, i = max(m, sum(nums[i: i + length + 1]) * b), i + length
        return m

    def maxAscendingSum(self, nums: List[int]) -> int:  # O(n)
        maximum = 0
        running = prev = nums.pop()
        while nums:
            n = nums.pop()
            if n < prev:
                running += n
            else:
                maximum = max(maximum, running)
                running = n
            prev = n
        return max(maximum, running)


def test():
    arguments = [
        [10, 20, 30, 5, 10, 50],
        [10, 20, 30, 40, 50],
        [12, 17, 15, 13, 10, 11, 12],
        [100, 10, 1],
    ]
    expectations = [65, 150, 33, 100]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maxAscendingSum(nums)
        assert solution == expected
