### Source : https://leetcode.com/problems/string-to-integer-atoi/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-17

##################################################################################################### 
#
# Implement atoi which converts a string to an integer.
# 
# The function first discards as many whitespace characters as necessary until the first 
# non-whitespace character is found. Then, starting from this character takes an optional initial 
# plus or minus sign followed by as many numerical digits as possible, and interprets them as a 
# numerical value.
# 
# The string can contain additional characters after those that form the integral number, which are 
# ignored and have no effect on the behavior of this function.
# 
# If the first sequence of non-whitespace characters in str is not a valid integral number, or if no 
# such sequence exists because either str is empty or it contains only whitespace characters, no 
# conversion is performed.
# 
# If no valid conversion could be performed, a zero value is returned.
# 
# Note:
# 
# 	Only the space character ' ' is considered a whitespace character.
# 	Assume we are dealing with an environment that could only store integers within the 32-bit 
# signed integer range: [&minus;231,  231 &minus; 1]. If the numerical value is out of the range of 
# representable values, 231 &minus; 1 or &minus;231 is returned.
# 
# Example 1:
# 
# Input: str = "42"
# Output: 42
# 
# Example 2:
# 
# Input: str = "   -42"
# Output: -42
# Explanation: The first non-whitespace character is '-', which is the minus sign. Then take as many 
# numerical digits as possible, which gets 42.
# 
# Example 3:
# 
# Input: str = "4193 with words"
# Output: 4193
# Explanation: Conversion stops at digit '3' as the next character is not a numerical digit.
# 
# Example 4:
# 
# Input: str = "words and 987"
# Output: 0
# Explanation: The first non-whitespace character is 'w', which is not a numerical digit or a +/- 
# sign. Therefore no valid conversion could be performed.
# 
# Example 5:
# 
# Input: str = "-91283472332"
# Output: -2147483648
# Explanation: The number "-91283472332" is out of the range of a 32-bit signed integer. Thefore 
# INT_IN (&minus;231) is returned.
# 
# Constraints:
# 
# 	0 <= s.length <= 200
# 	s consists of English letters (lower-case and upper-case), digits, ' ', '+', '-' and '.'.
#####################################################################################################


class Solution:
    def myAtoi(self, s: str) -> int:

        s = s.strip()
        if not s:
            return 0

        digits = set(map(str, range(10)))
        MIN_INT = -2 ** 31
        MAX_INT = 2 ** 31 - 1

        sign = -1 if s[0] == "-" else 1
        if s[0] in "+-":
            s = s[1:]

        res = 0
        for c in s:
            if c not in digits:
                break
            res *= 10
            res += int(c)

        res = sign * res
        return MIN_INT if res < MIN_INT else MAX_INT if res > MAX_INT else res

    def myAtoi(self, s: str) -> int:  # O(n) time and O(1) space
        n = 0
        sign = 1
        for i, c in enumerate(s.lstrip()):
            if c in ('-', '+') and i == 0:
                sign = -1 if c == '-' else 1
            elif '0' <= c <= '9':
                n *= 10
                n += ord(c) - ord('0')
            else:
                break
        n = sign * n
        return max(min(n, 2 ** 31 - 1),  -2 ** 31)


def test():
    strings = ["42", "   -42", "4193 with words", "words and 987", "-91283472332", "+1", "+-12"]
    expectations = [42, -42, 4193, 0, -2147483648, 1, 0]
    for s, expected in zip(strings, expectations):
        solution = Solution().myAtoi(s)
        assert solution == expected

