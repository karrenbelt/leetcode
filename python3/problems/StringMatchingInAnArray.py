### Source : https://leetcode.com/problems/string-matching-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an array of string words. Return all strings in words which is substring of another word in 
# any order. 
# 
# String words[i] is substring of words[j], if can be obtained removing some characters to left 
# and/or right side of words[j].
# 
# Example 1:
# 
# Input: words = ["mass","as","hero","superhero"]
# Output: ["as","hero"]
# Explanation: "as" is substring of "mass" and "hero" is substring of "superhero".
# ["hero","as"] is also a valid answer.
# 
# Example 2:
# 
# Input: words = ["leetcode","et","code"]
# Output: ["et","code"]
# Explanation: "et", "code" are substring of "leetcode".
# 
# Example 3:
# 
# Input: words = ["blue","green","bu"]
# Output: []
# 
# Constraints:
# 
# 	1 <= words.length <= 100
# 	1 <= words[i].length <= 30
# 	words[i] contains only lowercase English letters.
# 	It's guaranteed that words[i] will be unique.
#####################################################################################################

from typing import List


class Solution:
    def stringMatching(self, words: List[str]) -> List[str]:  # O(n^2 * s) time
        return words.sort(key=len) or [w for i, w in enumerate(words) if any(w in larger for larger in words[i + 1:])]

    def stringMatching(self, words: List[str]) -> List[str]:  # O(n^2) time
        return [word for word in words if super_string.count(word) > 1] if (super_string := ' '.join(words)) else []

    def stringMatching(self, words: List[str]) -> List[str]:  # O(n * s^2) time

        def add(word: str):
            node = trie
            for c in word:
                node = node.setdefault(c, {})
                node['#'] = node.get('#', 0) + 1

        def get(word: str) -> bool:
            node = trie
            for c in word:
                if (node := node.get(c)) is None:
                    return False
            return node['#'] > 1

        return (trie := {}) or any(add(w[i:]) for w in words for i in range(len(w))) or list(filter(get, words))


def test():
    arguments = [
        ["mass", "as", "hero", "superhero"],
        ["leetcode", "et", "code"],
        ["blue", "green", "bu"],
    ]
    expectations = [
        ["as", "hero"],
        ["et", "code"],
        [],
    ]
    for words, expected in zip(arguments, expectations):
        solution = Solution().stringMatching(words)
        assert solution == expected, solution
