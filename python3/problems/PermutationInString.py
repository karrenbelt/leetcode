### Source : https://leetcode.com/problems/permutation-in-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given two strings s1 and s2, return true if s2 contains the permutation of s1.
# 
# In other words, one of s1's permutations is the substring of s2.
# 
# Example 1:
# 
# Input: s1 = "ab", s2 = "eidbaooo"
# Output: true
# Explanation: s2 contains one permutation of s1 ("ba").
# 
# Example 2:
# 
# Input: s1 = "ab", s2 = "eidboaoo"
# Output: false
# 
# Constraints:
# 
# 	1 <= s1.length, s2.length <= 104
# 	s1 and s2 consist of lowercase English letters.
#####################################################################################################


class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:  # O(mn) time: 400 ms
        from collections import Counter

        target, counts = Counter(s1), Counter(s2[:len(s1) - 1])
        for i in range(len(s1) - 1, len(s2)):
            j = i - len(s1) + 1
            counts[s2[i]] += 1
            if not target - counts:  # this is slow, better pop old keys
                return True
            counts[s2[j]] -= 1
        return False

    def checkInclusion(self, s1: str, s2: str) -> bool:  # O(mn) time: 64 ms
        from collections import Counter

        target, counts = Counter(s1), Counter(s2[:len(s1) - 1])
        for i in range(len(s1) - 1, len(s2)):
            j = i - len(s1) + 1
            counts[s2[i]] += 1
            if target == counts:
                return True
            counts[s2[j]] -= 1
            if not counts[s2[j]]:
                counts.pop(s2[j])
        return False

    def checkInclusion(self, s1: str, s2: str) -> bool:  # O(mn) time
        from collections import Counter

        target, counts = Counter(s1), Counter(s2[:len(s1)])
        for i in range(len(s1), len(s2)):
            if target == counts:
                return True
            j = i - len(s1)
            counts[s2[i]] += 1
            counts[s2[j]] -= 1
            if not counts[s2[j]]:
                counts.pop(s2[j])
        return target == counts


def test():
    arguments = [
        ("ab", "eidbaooo"),
        ("ab", "eidboaoo"),
        ("aabb", "ebaabo"),
        ('aaab', "aab"),
        ("ab", "ab"),
    ]
    expectations = [True, False, True, False, True]
    for (s1, s2), expected in zip(arguments, expectations):
        solution = Solution().checkInclusion(s1, s2)
        assert solution == expected
