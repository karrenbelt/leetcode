### Source : https://leetcode.com/problems/word-search-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-04

##################################################################################################### 
#
# Given an m x n board of characters and a list of strings words, return all words on the board.
# 
# Each word must be constructed from letters of sequentially adjacent cells, where adjacent cells are 
# horizontally or vertically neighboring. The same letter cell may not be used more than once in a 
# word.
# 
# Example 1:
# 
# Input: board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]], words = 
# ["oath","pea","eat","rain"]
# Output: ["eat","oath"]
# 
# Example 2:
# 
# Input: board = [["a","b"],["c","d"]], words = ["abcb"]
# Output: []
# 
# Constraints:
# 
# 	m == board.length
# 	n == board[i].length
# 	1 <= m, n <= 12
# 	board[i][j] is a lowercase English letter.
# 	1 <= words.length <= 3 * 104
# 	1 <= words[i].length <= 10
# 	words[i] consists of lowercase English letters.
# 	All the strings of words are unique.
#####################################################################################################

from typing import List


class Solution:
    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:  # success but slow

        def build_trie():
            trie = {}
            for word in words:
                node = trie
                for c in word:
                    if c not in node:
                        node[c] = {}
                    node = node[c]
                node['.'] = None
            return trie

        def dfs(i: int, j: int, node: dict, prefix=''):  # backtracking
            if '.' in node and prefix not in found:
                found.append(prefix)
            if 0 <= i < len(board) and 0 <= j < len(board[0]) and not used[i][j] and board[i][j] in node:
                used[i][j] = True
                for x, y in [(0, 1), (0, -1), (1, 0), (-1, 0)]:  # mistake: [(1, 1), (1, -1),  (-1, 1) ,  (-1, -1)]
                    dfs(i + x, j + y, node[board[i][j]], prefix + board[i][j])
                used[i][j] = False

        found = []
        trie = build_trie()
        used = [[False] * len(board[0]) for _ in range(len(board))]
        for i in range(len(board)):
            for j in range(len(board[0])):
                dfs(i, j, trie)

        return found

    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:

        def build_trie():
            trie = {}
            for word in words:
                node = trie
                for c in word:
                    if c not in node:
                        node[c] = {}
                    node = node[c]
                node['.'] = None
            return trie

        def backtrack(i, j, node, prefix):
            if '.' in node:
                found.append(prefix)
                node.pop('.')  # prevent duplicates
            if i < 0 or i >= len(board) or j < 0 or j >= len(board[0]):
                return
            if not used[i][j] and board[i][j] in node:
                used[i][j] = True
                for x, y in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
                    backtrack(i + x, j + y, node[board[i][j]], prefix + board[i][j])
                used[i][j] = False

        found = []
        trie = build_trie()
        used = [[False] * len(board[0]) for _ in range(len(board))]
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] in trie:
                    backtrack(i, j, trie, '')

        return found

    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:

        def dfs(i: int, j: int, node: dict, path: str):
            if board[i][j] in node:
                letter, board[i][j] = board[i][j], ''
                node, path = node[letter], path + letter
                if '.' in node:
                    results.append(path)
                    node.pop('.')
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < len(board) and 0 <= y < len(board[0]):
                        dfs(x, y, node, path)
                board[i][j] = letter

        trie, results = {}, []
        for word in words:
            node = trie
            for letter in word + '.':
                node = node.setdefault(letter, {})

        [dfs(i, j, trie, '') for i in range(len(board)) for j in range(len(board[0]))]
        return results


def test():
    from collections import Counter
    boards = [
        [["o", "a", "a", "n"], ["e", "t", "a", "e"], ["i", "h", "k", "r"], ["i", "f", "l", "v"]],
        [["a", "b"], ["c", "d"]],
        [["o", "a", "a", "n"], ["e", "t", "a", "e"], ["i", "h", "k", "r"], ["i", "f", "l", "v"]],

    ]
    arrays_of_strings = [
        ["oath", "pea", "eat", "rain"],
        ["abcb"],
        ["oath", "pea", "eat", "rain", "hklf", "hf"],
    ]
    expectations = [
        ["eat", "oath"],
        [],
        ["oath", "eat", "hklf", "hf"],
    ]
    for board, words, expected in zip(boards, arrays_of_strings, expectations):
        solution = Solution().findWords(board, words)
        assert Counter(solution) == Counter(expected)


test()
