### Source : https://leetcode.com/problems/maximal-rectangle/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-13

##################################################################################################### 
#
# Given a rows x cols binary matrix filled with 0's and 1's, find the largest rectangle containing 
# only 1's and return its area.
# 
# Example 1:
# 
# Input: matrix = 
# [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
# Output: 6
# Explanation: The maximal rectangle is shown in the above picture.
# 
# Example 2:
# 
# Input: matrix = []
# Output: 0
# 
# Example 3:
# 
# Input: matrix = [["0"]]
# Output: 0
# 
# Example 4:
# 
# Input: matrix = [["1"]]
# Output: 1
# 
# Example 5:
# 
# Input: matrix = [["0","0"]]
# Output: 0
# 
# Constraints:
# 
# 	rows == matrix.length
# 	cols == matrix.length
# 	0 <= row, cols <= 200
# 	matrix[i][j] is '0' or '1'.
#####################################################################################################

from typing import List


class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:  # O(n^6) time
        area = 0
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                for p in range(i, len(matrix)):
                    for q in range(j, len(matrix[0])):
                        if any(matrix[x][y] == '0' for x in range(i, p + 1) for y in range(j, q + 1)):
                            continue
                        area = max(area, (p - i + 1) * (q - j + 1))
        return area

    def maximalRectangle(self, matrix: List[List[str]]) -> int:  # O(n^4) time
        area = 0
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                dp = [[0] * len(matrix[0]) for _ in range(len(matrix))]
                for p in range(i, len(matrix)):
                    for q in range(j, len(matrix[0])):
                        dp[p][q] = matrix[p][q] == '1'
                        if p > i:
                            dp[p][q] = dp[p][q] & dp[p - 1][q]
                        if q > j:
                            dp[p][q] = dp[p][q] & dp[p][q - 1]
                        if dp[p][q]:
                            area = max(area, (p - i + 1) * (q - j + 1))
        return area

    def maximalRectangle(self, matrix: List[List[str]]) -> int:  # O(n^3) time
        area = 0
        dp = [[0] * len(matrix[0]) for _ in range(len(matrix))]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == '1':
                    width = dp[i][j] = (dp[i][j - 1] if j else 0) + 1
                    for k in range(i, -1, -1):
                        width = min(dp[k][j], width)
                        area = max(area, width * (i - k + 1))
        return area

    def maximalRectangle(self, matrix: List[List[str]]) -> int:  # O(n^2) time
        if not matrix:
            return 0
        m, n, area = len(matrix), len(matrix[0]), 0
        left, right, height = [0] * n, [n] * n, [0] * n
        for i in range(m):
            current = 0
            for j in range(n):
                if matrix[i][j] == '0':
                    height[j] = left[j] = 0
                    current = j + 1
                else:
                    left[j] = max(left[j], current)
                    height[j] += 1
            current = n
            for j in range(n)[::-1]:
                if matrix[i][j] == '0':
                    current = j
                right[j] = n if matrix[i][j] == '0' else min(right[j], current)
                area = max(height[j] * (right[j] - left[j]), area)
        return area

    def maximalRectangle(self, matrix: List[List[str]]) -> int:  # O(n^2)
        from python3.problems import LargestRectangleInHistogram

        if not matrix:
            return 0

        heights = [0] * len(matrix[0])
        max_area = -1
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                heights[j] = heights[j] + 1 if matrix[i][j] == "1" else 0
            area = LargestRectangleInHistogram.Solution().largestRectangleArea(heights)
            max_area = max(max_area, area)
        return max_area


def test():
    matrices = [
        [["1", "0", "1", "0", "0"],
         ["1", "0", "1", "1", "1"],
         ["1", "1", "1", "1", "1"],
         ["1", "0", "0", "1", "0"]],
        [],
        [["0"]],
        [["1"]],
        [["0", "0"]],
        ]
    expectations = [6, 0, 0, 1, 0]
    for matrix, expected in zip(matrices, expectations):
        solution = Solution().maximalRectangle(matrix)
        assert solution == expected, (solution, expected)
