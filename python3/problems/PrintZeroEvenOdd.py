### Source : https://leetcode.com/problems/print-zero-even-odd/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-22

##################################################################################################### 
#
# Suppose you are given the following code:
# 
# class ZeroEvenOdd {
#   public ZeroEvenOdd(int n) { ... }      // constructor
#   public void zero(printNumber) { ... }  // only output 0's
#   public void even(printNumber) { ... }  // only output even numbers
#   public void odd(printNumber) { ... }   // only output odd numbers
# }
# 
# The same instance of ZeroEvenOdd will be passed to three different threads:
# 
# 	Thread A will call zero() which should only output 0's.
# 	Thread B will call even() which should only ouput even numbers.
# 	Thread C will call odd() which should only output odd numbers.
# 
# Each of the threads is given a printNumber method to output an integer. odify the given program to 
# output the series 010203040506... where the length of the series must be 2n.
# 
# Example 1:
# 
# Input: n = 2
# Output: "0102"
# Explanation: There are three threads being fired asynchronously. One of them calls zero(), the 
# other calls even(), and the last one calls odd(). "0102" is the correct output.
# 
# Example 2:
# 
# Input: n = 5
# Output: "0102030405"
#####################################################################################################


class ZeroEvenOdd:
    def __init__(self, n):
        from threading import Lock
        self.n = n
        self.t0 = Lock()
        self.t = [Lock(), Lock()]
        self.t[0].acquire()
        self.t[1].acquire()

    # printNumber(x) outputs "x", where x is an integer.
    def zero(self, printNumber: 'Callable[[int], None]') -> None:
        # Zero will be printed n times
        for i in range(self.n):
            self.t0.acquire()
            printNumber(0)
            # It needs to release odd numbers first so we use (i+1)%2
            self.t[(i + 1) % 2].release()

    def even(self, printNumber: 'Callable[[int], None]') -> None:
        for i in range(self.n // 2):
            self.t[0].acquire()
            printNumber((i + 1) * 2)
            self.t0.release()

    def odd(self, printNumber: 'Callable[[int], None]') -> None:
        for i in range((1 + self.n) // 2):
            self.t[1].acquire()
            printNumber(i * 2 + 1)
            self.t0.release()



def test():
    pass
