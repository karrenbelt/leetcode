### Source : https://leetcode.com/problems/maximum-product-of-word-lengths/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-06

##################################################################################################### 
#
# Given a string array words, return the maximum value of length(word[i]) * length(word[j]) where the 
# two words do not share common letters. If no such two words exist, return 0.
# 
# Example 1:
# 
# Input: words = ["abcw","baz","foo","bar","xtfn","abcdef"]
# Output: 16
# Explanation: The two words can be "abcw", "xtfn".
# 
# Example 2:
# 
# Input: words = ["a","ab","abc","d","cd","bcd","abcd"]
# Output: 4
# Explanation: The two words can be "ab", "cd".
# 
# Example 3:
# 
# Input: words = ["a","aa","aaa","aaaa"]
# Output: 0
# Explanation: No such pair of words.
# 
# Constraints:
# 
# 	2 <= words.length <= 1000
# 	1 <= words[i].length <= 1000
# 	words[i] consists only of lowercase English letters.
#####################################################################################################

from typing import List

class Solution:
    def maxProduct(self, words: List[str]) -> int:  # O(n^2) time and O(1) space
        max_product = 0
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                if not set(words[i]).intersection(words[j]):
                    max_product = max(max_product, len(words[i]) * len(words[j]))
        return max_product

    def maxProduct(self, words: List[str]) -> int:  # O(n^2) time and O(1) space
        return max(len(words[i]) * len(words[j]) if not set(words[i]).intersection(words[j]) else 0
                   for i in range(len(words)) for j in range(i + 1, len(words)))

    def maxProduct(self, words: List[str]) -> int:  # O(n^2) time and O(1) space
        return max((len(words[i]) * len(words[j]) for i in range(len(words)) for j in range(i + 1, len(words))
                    if not set(words[i]) & set(words[j])), default=0)

    def maxProduct(self, words: List[str]) -> int:  # O(n^2) time and O(n) space
        signature = {w: set(w) for w in words}
        max_product = 0
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                if not bool(signature[words[i]] & signature[words[j]]):
                    max_product = max(max_product, len(words[i]) * len(words[j]))
        return max_product

    def maxProduct(self, words: List[str]) -> int:  # O(n) time and O(n) space
        d = {}
        for word in words:
            mask = 0
            for c in set(word):
                mask |= (1 << (ord(c) - ord('a')))
            d[mask] = max(d.get(mask, 0), len(word))
        return max(d[x] * d[y] if not x & y else 0 for x in d for y in d)


def test():
    arguments = [
        ["abcw", "baz", "foo", "bar", "xtfn", "abcdef"],
        ["a", "ab", "abc", "d", "cd", "bcd", "abcd"],
        ["a", "aa", "aaa", "aaaa"],
        ]
    expectations = [16, 4, 0]
    for words, expected in zip(arguments, expectations):
        solution = Solution().maxProduct(words)
        assert solution == expected, (words, expected, solution)
test()