### Source : https://leetcode.com/problems/count-primes/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# Count the number of prime numbers less than a non-negative number, n.
# 
# Example 1:
# 
# Input: n = 10
# Output: 4
# Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
# 
# Example 2:
# 
# Input: n = 0
# Output: 0
# 
# Example 3:
# 
# Input: n = 1
# Output: 0
# 
# Constraints:
# 
# 	0 <= n <= 5 * 106
#####################################################################################################


class Solution:  # Sieve of Eratosthenes
    def countPrimes(self, n: int) -> int:  # dp
        if n < 2:
            return 0

        dp = [True] * n
        dp[0] = dp[1] = False
        for i in range(2, n):
            if dp[i]:
                for j in range(i * i, n, i):  # all multiples to False
                    dp[j] = False
        return sum(dp)

    def countPrimes(self, n: int) -> int:
        if n < 2:
            return 0
        dp = [True] * n
        dp[0] = dp[1] = False
        for i in range(int(n ** 0.5) + 1):
            if dp[i]:
                dp[i * i:n:i] = [0] * ((n - 1 - i * i) // i + 1)
        return sum(dp)


def test():
    numbers = [10, 0, 1]
    expectations = [4, 0, 0]
    for n, expected in zip(numbers, expectations):
        solution = Solution().countPrimes(n)
        assert solution == expected
test()