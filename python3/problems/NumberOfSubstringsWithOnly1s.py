### Source : https://leetcode.com/problems/number-of-substrings-with-only-1s/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-13

##################################################################################################### 
#
# Given a binary string s, return the number of substrings with all characters 1's. Since the answer 
# may be too large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: s = "0110111"
# Output: 9
# Explanation: There are 9 substring in total with only 1's characters.
# "1" -> 5 times.
# "11" -> 3 times.
# "111" -> 1 time.
# 
# Example 2:
# 
# Input: s = "101"
# Output: 2
# Explanation: Substring "1" is shown 2 times in s.
# 
# Example 3:
# 
# Input: s = "111111"
# Output: 21
# Explanation: Each substring contains only 1's characters.
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s[i] is either '0' or '1'.
#####################################################################################################


class Solution:
    def numSub(self, s: str) -> int:  # O(n) time & space
        from collections import Counter
        counts, prime = Counter(map(len, filter(None, s.split("0")))), 10**9 + 7
        return sum(sum(range(l + 1)) * n for l, n in counts.items()) % prime

    def numSub(self, s: str) -> int:  # O(n) time & space
        from collections import Counter
        counts, prime = Counter(map(len, filter(None, s.split("0")))), 10**9 + 7
        return sum(l * (l + 1) // 2 * n for l, n in counts.items()) % prime

    def numSub(self, s):  # O(n) time & space
        return sum(n * (n + 1) / 2 for n in map(len, s.split('0'))) % (10**9 + 7)


def test():
    arguments = [
        "0110111",
        "101",
        "111111",
    ]
    expectations = [9, 2, 21]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().numSub(nums)
        assert solution == expected
