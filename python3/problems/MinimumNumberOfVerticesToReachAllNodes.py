### Source : https://leetcode.com/problems/minimum-number-of-vertices-to-reach-all-nodes/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-14

##################################################################################################### 
#
# Given a directed acyclic graph, with n vertices numbered from 0 to n-1, and an array edges where 
# edges[i] = [fromi, toi] represents a directed edge from node fromi to node toi.
# 
# Find the smallest set of vertices from which all nodes in the graph are reachable. It's guaranteed 
# that a unique solution exists.
# 
# Notice that you can return the vertices in any order.
# 
# Example 1:
# 
# Input: n = 6, edges = [[0,1],[0,2],[2,5],[3,4],[4,2]]
# Output: [0,3]
# Explanation: It's not possible to reach all the nodes from a single vertex. From 0 we can reach 
# [0,1,2,5]. From 3 we can reach [3,4,2,5]. So we output [0,3].
# 
# Example 2:
# 
# Input: n = 5, edges = [[0,1],[2,1],[3,1],[1,4],[2,4]]
# Output: [0,2,3]
# Explanation: Notice that vertices 0, 3 and 2 are not reachable from any other node, so we must 
# include them. Also any of these vertices can reach nodes 1 and 4.
# 
# Constraints:
# 
# 	2 <= n <= 105
# 	1 <= edges.length <= min(105, n * (n - 1) / 2)
# 	edges[i].length == 2
# 	0 <= fromi, toi < n
# 	All pairs (fromi, toi) are distinct.
#####################################################################################################

from typing import List


class Solution:
    def findSmallestSetOfVertices(self, n: int, edges: List[List[int]]) -> List[int]:

        graph = {}
        for u, v in edges:
            graph.setdefault(u, []).append(v)

        reachable = {n for nodes in graph.values() for n in nodes}
        return sorted(set(range(n)) - reachable)

    def findSmallestSetOfVertices(self, n: int, edges: List[List[int]]) -> List[int]:
        return list(set(range(n)) - set(j for i, j in edges))

    def findSmallestSetOfVertices(self, n: int, edges: List[List[int]]) -> List[int]:  # O(n^2) time

        def dfs(node: int) -> int:  # union find
            if tree[node] != node:
                tree[node] = dfs(tree[node])
            return tree[node]

        tree = list(range(n))
        for u, v in edges:
            if dfs(u) != dfs(v):
                tree[v] = u

        return [i for i, n in enumerate(tree) if i == n]


def test():
    strings = [
        (6, [[0, 1], [0, 2], [2, 5], [3, 4], [4, 2]]),
        (5, [[0, 1], [2, 1], [3, 1], [1, 4], [2, 4]]),
        (4, [[1, 2], [3, 2], [1, 3], [1, 0], [0, 2], [0, 3]])
    ]
    expectations = [
        [0, 3],
        [0, 2, 3],
        [1],
    ]
    for (n, edges), expected in zip(strings, expectations):
        solution = Solution().findSmallestSetOfVertices(n, edges)
        assert solution == expected
