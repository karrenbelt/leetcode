### Source : https://leetcode.com/problems/angle-between-hands-of-a-clock/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given two numbers, hour and minutes. Return the smaller angle (in degrees) formed between the hour 
# and the minute hand.
# 
# Example 1:
# 
# Input: hour = 12, minutes = 30
# Output: 165
# 
# Example 2:
# 
# Input: hour = 3, minutes = 30
# Output: 75
# 
# Example 3:
# 
# Input: hour = 3, minutes = 15
# Output: 7.5
# 
# Example 4:
# 
# Input: hour = 4, minutes = 50
# Output: 155
# 
# Example 5:
# 
# Input: hour = 12, minutes = 0
# Output: 0
# 
# Constraints:
# 
# 	1 <= hour <= 12
# 	0 <= minutes <= 59
# 	Answers within 10-5 of the actual value will be accepted as correct.
#####################################################################################################


class Solution:
    def angleClock(self, hour: int, minutes: int) -> float:  # O(1) time and O(1) space
        hour_hand = 360 / 12 * (hour + minutes / 60)
        minute_hand = 360 / 60 * minutes
        angle = abs(hour_hand - minute_hand)
        return angle if angle <= 180 else 360 - angle


def test():
    arguments = [
        (12, 30),
        (3, 30),
        (3, 15),
        (4, 50),
        (12, 0),
        (1, 57),
    ]
    expectations = [165, 75, 7.5, 155, 0, 76.5]
    for (hour, minutes), expected in zip(arguments, expectations):
        solution = Solution().angleClock(hour, minutes)
        assert solution == expected
