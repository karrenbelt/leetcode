### Source : https://leetcode.com/problems/thousand-separator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

#####################################################################################################
#
# Given an integer n, add a dot (".") as the thousands separator and return it in string format.
#
# Example 1:
#
# Input: n = 987
# Output: "987"
#
# Example 2:
#
# Input: n = 1234
# Output: "1.234"
#
# Example 3:
#
# Input: n = 123456789
# Output: "123.456.789"
#
# Example 4:
#
# Input: n = 0
# Output: "0"
#
# Constraints:
#
# 	0 <= n < 231
#####################################################################################################


class Solution:
    def thousandSeparator(self, n: int) -> str:  # O(n) time, three passes
        return ''.join(reversed([x + ('.' if i and not i % 3 else '') for i, x in enumerate(reversed(str(n)))]))

    def thousandSeparator(self, n: int) -> str:  # O(n) time, one pass (only strip 1 char)
        s = str(n)
        return ''.join(c if (len(s) - i - 1) % 3 else c + '.' for i, c in enumerate(s)).rstrip('.')

    def thousandSeparator(self, n: int) -> str:  # O(n) time, one pass
        s = str(n)
        return "".join(c if (len(s) - i) % 3 or not len(s) - i else c + '.' for i, c in enumerate(s, 1))

    def thousandSeparator(self, n: int) -> str:  # O(n) time, two pass
        return f'{n:,}'.replace(',', '.')


def test():
    arguments = [987, 1234, 123456879]
    expectations = ["987", "1.234", "123.456.879"]
    for n, expected in zip(arguments, expectations):
        solution = Solution().thousandSeparator(n)
        print(solution, expected)
        assert solution == expected, (solution, expected)
