### Source : https://leetcode.com/problems/word-pattern/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-01

##################################################################################################### 
#
# Given a pattern and a string s, find if s follows the same pattern.
# 
# Here follow means a full match, such that there is a bijection between a letter in pattern and a 
# non-empty word in s.
# 
# Example 1:
# 
# Input: pattern = "abba", s = "dog cat cat dog"
# Output: true
# 
# Example 2:
# 
# Input: pattern = "abba", s = "dog cat cat fish"
# Output: false
# 
# Example 3:
# 
# Input: pattern = "aaaa", s = "dog cat cat dog"
# Output: false
# 
# Example 4:
# 
# Input: pattern = "abba", s = "dog dog dog dog"
# Output: false
# 
# Constraints:
# 
# 	1 <= pattern.length <= 300
# 	pattern contains only lower-case English letters.
# 	1 <= s.length <= 3000
# 	s contains only lower-case English letters and spaces ' '.
# 	s does not contain any leading or trailing spaces.
# 	All the words in s are separated by a single space.
#####################################################################################################


class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        s = s.split()
        if not len(s) == len(pattern):
            return False

        d = {}
        for i, c in enumerate(pattern):
            if c in d:
                if not d[c] == s[i]:
                    return False
            else:
                d[c] = s[i]
        return len(set(d.values())) == len(d.values())

    def wordPattern(self, pattern: str, s: str) -> bool:  # check number of uniques individually and zipped
        return len(set(pattern)) == len(set(s.split())) == len(set(zip(pattern, s.split())))

    def wordPattern(self, pattern: str, s: str) -> bool:  # beautiful - must cast map to list / tuple
        return tuple(map(pattern.find, pattern)) == tuple(map(s.split().index, s.split()))


def test():
    pairs_of_strings = [
        ("abba", "dog cat cat dog"),
        ("abba", "dog cat cat fish"),
        ("aaaa", "dog cat cat dog"),
        ("abba", "dog dog dog dog"),
        ]
    expectations = [True, False, False, False]
    for (pattern, s), expected in zip(pairs_of_strings, expectations):
        solution = Solution().wordPattern(pattern, s)
        assert solution == expected
