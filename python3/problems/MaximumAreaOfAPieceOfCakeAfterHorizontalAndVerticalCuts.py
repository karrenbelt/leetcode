### Source : https://leetcode.com/problems/maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-03

##################################################################################################### 
#
# Given a rectangular cake with height h and width w, and two arrays of integers horizontalCuts and 
# verticalCuts where horizontalCuts[i] is the distance from the top of the rectangular cake to the 
# ith horizontal cut and similarly, verticalCuts[j] is the distance from the left of the rectangular 
# cake to the jth vertical cut.
# 
# Return the maximum area of a piece of cake after you cut at each horizontal and vertical position 
# provided in the arrays horizontalCuts and verticalCuts. Since the answer can be a huge number, 
# return this modulo 109 + 7.
# 
# Example 1:
# 
# Input: h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
# Output: 4 
# Explanation: The figure above represents the given rectangular cake. Red lines are the horizontal 
# and vertical cuts. After you cut the cake, the green piece of cake has the maximum area.
# 
# Example 2:
# 
# Input: h = 5, w = 4, horizontalCuts = [3,1], verticalCuts = [1]
# Output: 6
# Explanation: The figure above represents the given rectangular cake. Red lines are the horizontal 
# and vertical cuts. After you cut the cake, the green and yellow pieces of cake have the maximum 
# area.
# 
# Example 3:
# 
# Input: h = 5, w = 4, horizontalCuts = [3], verticalCuts = [3]
# Output: 9
# 
# Constraints:
# 
# 	2 <= h, w <= 109
# 	1 <= horizontalCuts.length < min(h, 105)
# 	1 <= verticalCuts.length < min(w, 105)
# 	1 <= horizontalCuts[i] < h
# 	1 <= verticalCuts[i] < w
# 	It is guaranteed that all elements in horizontalCuts are distinct.
# 	It is guaranteed that all elements in verticalCuts are distinct.
#####################################################################################################

from typing import List


class Solution:
    def maxArea(self, h: int, w: int, horizontalCuts: List[int], verticalCuts: List[int]) -> int:  # O(n log n) time

        def max_distance(lines: List[int]) -> int:
            return max(b - a for a, b in zip(lines, lines[1:]))

        return max_distance(sorted(horizontalCuts + [0, h])) * max_distance(sorted(verticalCuts + [0, w])) % (10**9 + 7)

    def maxArea(self, h: int, w: int, horizontalCuts: List[int], verticalCuts: List[int]) -> int:
        return ((max_distance := lambda v: max(b - a for a, b in zip(v, v[1:])))(sorted(horizontalCuts + [0, h]))
                * max_distance(sorted(verticalCuts + [0, w]))) % (10**9 + 7)


def test():
    arguments = [
        (5, 4, [1, 2, 4], [1, 3]),
        (5, 4, [3, 1], [1]),
        (5, 4, [3], [3]),
    ]
    expectations = [4, 6, 9]
    for (h, w, horizontalCuts, verticalCuts), expected in zip(arguments, expectations):
        solution = Solution().maxArea(h, w, horizontalCuts, verticalCuts)
        assert solution == expected, solution
