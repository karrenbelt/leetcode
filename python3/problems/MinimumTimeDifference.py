### Source : https://leetcode.com/problems/minimum-time-difference/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-25

#####################################################################################################
#
# Given a list of 24-hour clock time points in "HH:" format, return the minimum minutes difference
# between any two time-points in the list.
#
# Example 1:
# Input: timePoints = ["23:59","00:00"]
# Output: 1
# Example 2:
# Input: timePoints = ["00:00","23:59","00:00"]
# Output: 0
#
# Constraints:
#
# 	2 <= timePoints <= 2 * 104
# 	timePoints[i] is in the format "HH:".
#####################################################################################################

from typing import List


class Solution:
    def findMinDifference(self, timePoints: List[str]) -> int:
        # we convert to integers first. Need to take care that 23:59 and 00:00 are close (wrap around)
        times = sorted(int(time_point[:2]) * 60 + int(time_point[3:5]) for time_point in timePoints)
        times.append(times[0] + 1440)
        return min(b - a for a, b in zip(times, times[1:]))


def test():
    arguments = [
        ["23:59", "00:00"],
        ["00:00", "23:59", "00:00"],
    ]
    expectations = [1, 0]
    for timePoints, expected in zip(arguments, expectations):
        solution = Solution().findMinDifference(timePoints)
        assert solution == expected
