### Source : https://leetcode.com/problems/maximum-erasure-value/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-28

##################################################################################################### 
#
# You are given an array of positive integers nums and want to erase a subarray containing unique 
# elements. The score you get by erasing the subarray is equal to the sum of its elements.
# 
# Return the maximum score you can get by erasing exactly one subarray.
# 
# An array b is called to be a subarray of a if it forms a contiguous subsequence of a, that is, if 
# it is equal to a[l],a[l+1],...,a[r] for some (l,r).
# 
# Example 1:
# 
# Input: nums = [4,2,4,5,6]
# Output: 17
# Explanation: The optimal subarray here is [2,4,5,6].
# 
# Example 2:
# 
# Input: nums = [5,2,1,2,5,2,1,2,5]
# Output: 8
# Explanation: The optimal subarray here is [5,2,1] or [1,2,5].
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	1 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def maximumUniqueSubarray(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        # maximum subarray problem, with the constraint of all values in it needing to be unique
        left = right = running = maximum = 0
        window = set()
        while right < len(nums):
            if nums[right] not in window:
                running += nums[right]
                maximum = max(maximum, running)
                window.add(nums[right])
                right += 1
            else:
                running -= nums[left]
                window.remove(nums[left])
                left += 1
        return max(maximum, running)


def test():
    arguments = [
        [4, 2, 4, 5, 6],
        [5, 2, 1, 2, 5, 2, 1, 2, 5],
    ]
    expectations = [17, 8]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maximumUniqueSubarray(nums)
        assert solution == expected
