### Source : https://leetcode.com/problems/search-suggestions-system/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-31

##################################################################################################### 
#
# Given an array of strings products and a string searchWord. We want to design a system that 
# suggests at most three product names from products after each character of searchWord is typed. 
# Suggested products should have common prefix with the searchWord. If there are more than three 
# products with a common prefix return the three lexicographically minimums products.
# 
# Return list of lists of the suggested products after each character of searchWord is typed. 
# 
# Example 1:
# 
# Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
# Output: [
# ["mobile","moneypot","monitor"],
# ["mobile","moneypot","monitor"],
# ["mouse","mousepad"],
# ["mouse","mousepad"],
# ["mouse","mousepad"]
# ]
# Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"]
# After typing m and mo all products match and we show user ["mobile","moneypot","monitor"]
# After typing mou, mous and mouse the system suggests ["mouse","mousepad"]
# 
# Example 2:
# 
# Input: products = ["havana"], searchWord = "havana"
# Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
# 
# Example 3:
# 
# Input: products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"
# Output: [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]
# 
# Example 4:
# 
# Input: products = ["havana"], searchWord = "tatiana"
# Output: [[],[],[],[],[],[],[]]
# 
# Constraints:
# 
# 	1 <= products.length <= 1000
# 	There are no repeated elements in products.
# 	1 <= &Sigma; products[i].length <= 2 * 104
# 	All characters of products[i] are lower-case English letters.
# 	1 <= searchWord.length <= 1000
# 	All characters of searchWord are lower-case English letters.
#####################################################################################################

from typing import List


class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:  # O(p log p + p * s) time
        suggestions = products.sort() or []
        for i in range(len(searchWord)):
            products = list(filter(lambda s: s.startswith(searchWord[:i + 1]), products))
            suggestions.append(products[:3])
        return suggestions

    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        import bisect
        products.sort()
        suggestions, prefix, i = [], '', 0
        for c in searchWord:
            prefix += c
            i = bisect.bisect_left(products, prefix, i)
            suggestions.append([w for w in products[i:i + 3] if w.startswith(prefix)])
        return suggestions

    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:

        class TrieNode(dict):
            def __init__(self, suggestions: List[str] = None, **kwargs):
                super().__init__(**kwargs)
                self.suggestions = [] if suggestions is None else suggestions

        trie = TrieNode()
        for product in products:
            node = trie
            for c in product:
                if c not in node:
                    node[c] = TrieNode()
                node = node[c]
                node.suggestions.append(product)
                node.suggestions.sort()
                if len(node.suggestions) > 3:
                    node.suggestions.pop()

        node, suggestions = trie, []
        for c in searchWord:
            node = node.get(c, TrieNode())
            suggestions.append(node.suggestions)
        return suggestions

    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        from collections import defaultdict

        class TrieNode(defaultdict):
            def __init__(self, **kwargs):
                super().__init__(**kwargs)
                self.default_factory = TrieNode
                self.suggestions = []

        trie = TrieNode()
        for product in products:
            node = trie
            for c in product:
                node.suggestions = sorted((node := node[c]).suggestions + [product])[:3]

        node, suggestions = trie, []
        for c in searchWord:
            suggestions.append((node := node.get(c, TrieNode())).suggestions)
        return suggestions


def test():
    arguments = [
        (["mobile", "mouse", "moneypot", "monitor", "mousepad"], "mouse"),
    ]
    expectations = [
        [
            ["mobile", "moneypot", "monitor"],
            ["mobile", "moneypot", "monitor"],
            ["mouse", "mousepad"],
            ["mouse", "mousepad"],
            ["mouse", "mousepad"],
        ],
        [["havana"], ["havana"], ["havana"], ["havana"], ["havana"], ["havana"]],
        [["baggage", "bags", "banner"], ["baggage", "bags", "banner"], ["baggage", "bags"], ["bags"]],
    ]
    for (products, searchWord), expected in zip(arguments, expectations):
        solution = Solution().suggestedProducts(products, searchWord)
        assert solution == expected
