### Source : https://leetcode.com/problems/keys-and-rooms/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-19

##################################################################################################### 
#
# There are N rooms and you start in room 0.  Each room has a distinct number in 0, 1, 2, ..., N-1, 
# and each room may have some keys to access the next room. 
# 
# Formally, each room i has a list of keys rooms[i], and each key rooms[i][j] is an integer in [0, 1, 
# ..., N-1] where N = rooms.length.  A key rooms[i][j] = v opens the room with number v.
# 
# Initially, all the rooms start locked (except for room 0). 
# 
# You can walk back and forth between rooms freely.
# 
# Return true if and only if you can enter every room.
# 
# Example 1:
# 
# Input: [[1],[2],[3],[]]
# Output: true
# Explanation:  
# We start in room 0, and pick up key 1.
# We then go to room 1, and pick up key 2.
# We then go to room 2, and pick up key 3.
# We then go to room 3.  Since we were able to go to every room, we return true.
# 
# Example 2:
# 
# Input: [[1,3],[3,0,1],[2],[0]]
# Output: false
# Explanation: We can't enter the room with number 2.
# 
# Note:
# 
# 	1 <= rooms.length <= 1000
# 	0 <= rooms[i].length <= 1000
# 	The number of keys in all rooms combined is at most 3000.
# 
#####################################################################################################

from typing import List


class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:  # O(n) time and O(n) space

        def dfs(keys):
            for k in set(keys).difference(seen):
                seen.add(k)
                dfs(rooms[k])

        seen = {0}
        dfs(rooms[0])
        return len(seen) == len(rooms)

    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:  # O(n) time and O(n) space
        seen = {0}
        keys = set(rooms[0])
        while keys:
            # new_keys = set(sum([rooms[key] for key in keys], [])).difference(seen)
            new_keys = {k for key in keys for k in rooms[key] if k not in seen}  # faster
            seen.update(keys)
            keys = new_keys
        return len(seen) == len(rooms)

    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:  # O(n) time and O(n) space
        from collections import deque
        visited = [True] + [False] * (len(rooms) - 1)
        queue = deque([0])  # bfs
        while queue:
            node = queue.popleft()
            for room in rooms[node]:
                if not visited[room]:
                    visited[room] = True
                    queue.append(room)
        return all(visited)


def test():
    arguments = [
        [[1], [2], [3], []],
        [[1, 3], [3, 0, 1], [2], [0]],
        ]
    expectations = [True, False]
    for rooms, expected in zip(arguments, expectations):
        solution = Solution().canVisitAllRooms(rooms)
        assert solution == expected
