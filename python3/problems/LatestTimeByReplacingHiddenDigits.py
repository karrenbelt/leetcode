### Source : https://leetcode.com/problems/latest-time-by-replacing-hidden-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-15

##################################################################################################### 
#
# You are given a string time in the form of  hh:mm, where some of the digits in the string are 
# hidden (represented by ?).
# 
# The valid times are those inclusively between 00:00 and 23:59.
# 
# Return the latest valid time you can get from time by replacing the hidden digits.
# 
# Example 1:
# 
# Input: time = "2?:?0"
# Output: "23:50"
# Explanation: The latest hour beginning with the digit '2' is 23 and the latest minute ending with 
# the digit '0' is 50.
# 
# Example 2:
# 
# Input: time = "0?:3?"
# Output: "09:39"
# 
# Example 3:
# 
# Input: time = "1?:22"
# Output: "19:22"
# 
# Constraints:
# 
# 	time is in the format hh:mm.
# 	It is guaranteed that you can produce a valid time from the given string.
#####################################################################################################


class Solution:
    def maximumTime(self, time: str) -> str:  # O(1) time and O(1) space
        hh, mm = map(list, time.split(':'))
        hh[0] = '2' if hh[0] == '?' and (hh[1] == '?' or '0' <= hh[1] <= '3') else '1' if hh[0] == '?' else hh[0]
        hh[1] = '3' if hh[1] == '?' and hh[0] == '2' else '9' if hh[1] == '?' else hh[1]
        mm[0] = '5' if mm[0] == '?' else mm[0]
        mm[1] = '9' if mm[1] == '?' else mm[1]
        return f"{''.join(hh)}:{''.join(mm)}"


def test():
    arguments = ["2?:?0", "0?:3?", "1?:22", "?4:03"]
    expectations = ["23:50", "09:39", "19:22", "14:03"]
    for time, expected in zip(arguments, expectations):
        solution = Solution().maximumTime(time)
        assert solution == expected
