### Source : https://leetcode.com/problems/combination-sum-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-23

##################################################################################################### 
#
# Find all valid combinations of k numbers that sum up to n such that the following conditions are 
# true:
# 
# 	Only numbers 1 through 9 are used.
# 	Each number is used at most once.
# 
# Return a list of all possible valid combinations. The list must not contain the same combination 
# twice, and the combinations may be returned in any order.
# 
# Example 1:
# 
# Input: k = 3, n = 7
# Output: [[1,2,4]]
# Explanation:
# 1 + 2 + 4 = 7
# There are no other valid combinations.
# 
# Example 2:
# 
# Input: k = 3, n = 9
# Output: [[1,2,6],[1,3,5],[2,3,4]]
# Explanation:
# 1 + 2 + 6 = 9
# 1 + 3 + 5 = 9
# 2 + 3 + 4 = 9
# There are no other valid combinations.
# 
# Example 3:
# 
# Input: k = 4, n = 1
# Output: []
# Explanation: There are no valid combinations. [1,2,1] is not valid because 1 is used twice.
# 
# Example 4:
# 
# Input: k = 3, n = 2
# Output: []
# Explanation: There are no valid combinations.
# 
# Example 5:
# 
# Input: k = 9, n = 45
# Output: [[1,2,3,4,5,6,7,8,9]]
# Explanation:
# 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 = 45
# -b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-KThere are no other valid combinations.
# 
# Constraints:
# 
# 	2 <= k <= 9
# 	1 <= n <= 60
#####################################################################################################

from typing import List


class Solution:
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        import itertools
        return [list(c) for c in itertools.combinations(range(1, 10), k) if sum(c) == n]

    def combinationSum3(self, k: int, n: int) -> List[List[int]]:  # O(2^9*k) time

        def backtracking(idx, remainder, path):
            if len(path) == k and remainder == 0:
                paths.append(path)
            else:
                for i in range(idx, min(remainder + 1, 10)):
                    backtracking(i + 1, remainder - i, path + [i])

        paths = []
        backtracking(1, n, [])
        return paths

    def combinationSum3(self, k, n):
        def combs(parts, remainder, cap):
            if not parts:
                return [[]] * (not remainder)
            return [comb + [last] for last in range(1, cap) for comb in combs(parts - 1, remainder - last, last)]
        return combs(k, n, 10)


def test():
    arguments = [
        (3, 7),
        (3, 9),
        (4, 1),
        (3, 2),
        (9, 45),
    ]
    expectations = [
        [[1, 2, 4]],
        [[1, 2, 6], [1, 3, 5], [2, 3, 4]],
        [],
        [],
        [[1, 2, 3, 4, 5, 6, 7, 8, 9]],
    ]
    for (k, n), expected in zip(arguments, expectations):
        solution = Solution().combinationSum3(k, n)
        assert set(map(frozenset, solution)) == set(map(frozenset, expected))
