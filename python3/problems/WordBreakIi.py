### Source : https://leetcode.com/problems/word-break-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given a string s and a dictionary of strings wordDict, add spaces in s to construct a sentence 
# where each word is a valid dictionary word. Return all such possible sentences in any order.
# 
# Note that the same word in the dictionary may be reused multiple times in the segmentation.
# 
# Example 1:
# 
# Input: s = "catsanddog", wordDict = ["cat","cats","and","sand","dog"]
# Output: ["cats and dog","cat sand dog"]
# 
# Example 2:
# 
# Input: s = "pineapplepenapple", wordDict = ["apple","pen","applepen","pine","pineapple"]
# Output: ["pine apple pen apple","pineapple pen apple","pine applepen apple"]
# Explanation: Note that you are allowed to reuse a dictionary word.
# 
# Example 3:
# 
# Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
# Output: []
# 
# Constraints:
# 
# 	1 <= s.length <= 20
# 	1 <= wordDict.length <= 1000
# 	1 <= wordDict[i].length <= 10
# 	s and wordDict[i] consist of only lowercase English letters.
# 	All the strings of wordDict are unique.
#####################################################################################################

from typing import List


class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:
        # all combinations any order, not shortest: we use recursive backtracking

        def backtracking(i, path):
            if i == len(s):
                paths.append(' '.join(path))
            else:
                for j, w in index_mapping.get(i, []):
                    backtracking(j, path + [w])

        index_mapping = {}
        for word in wordDict:
            idx = s.find(word)
            while idx != -1:
                index_mapping.setdefault(idx, []).append((idx + len(word), word))
                idx = s.find(word, idx + 1)

        paths = []
        backtracking(0, [])
        return paths


def test():
    arguments = [
        ("catsanddog", ["cat", "cats", "and", "sand", "dog"]),
        ("pineapplepenapple", ["apple", "pen", "applepen", "pine", "pineapple"]),
        ("catsandog", ["cats", "dog", "sand", "and", "cat"]),
        ]
    expectations = [
        ["cats and dog", "cat sand dog"],
        ["pine apple pen apple", "pineapple pen apple", "pine applepen apple"],
        [],
        ]
    for (s, wordDict), expected in zip(arguments, expectations):
        solution = Solution().wordBreak(s, wordDict)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
