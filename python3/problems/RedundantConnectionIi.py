### Source : https://leetcode.com/problems/redundant-connection-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-25

##################################################################################################### 
#
# In this problem, a rooted tree is a directed graph such that, there is exactly one node (the root) 
# for which all other nodes are descendants of this node, plus every node has exactly one parent, 
# except for the root node which has no parents.
# 
# The given input is a directed graph that started as a rooted tree with n nodes (with distinct 
# values from 1 to n), with one additional directed edge added. The added edge has two different 
# vertices chosen from 1 to n, and was not an edge that already existed.
# 
# The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [ui, vi] that 
# represents a directed edge connecting nodes ui and vi, where ui is a parent of child vi.
# 
# Return an edge that can be removed so that the resulting graph is a rooted tree of n nodes. If 
# there are multiple answers, return the answer that occurs last in the given 2D-array.
# 
# Example 1:
# 
# Input: edges = [[1,2],[1,3],[2,3]]
# Output: [2,3]
# 
# Example 2:
# 
# Input: edges = [[1,2],[2,3],[3,4],[4,1],[1,5]]
# Output: [4,1]
# 
# Constraints:
# 
# 	n == edges.length
# 	3 <= n <= 1000
# 	edges[i].length == 2
# 	1 <= ui, vi <= n
# 	ui != vi
#####################################################################################################

from typing import List


class Solution:
    def findRedundantDirectedConnection(self, edges: List[List[int]]) -> List[int]:
        # There are two distinct cases:
        # 1. You have a node with two parents
        # 2. You don't have a node with two parents, in which case you have added a parent to the root
        def cycle(x: int, y: int, seen: set) -> bool:
            return x not in seen and (x == y or any(cycle(node, y, seen | {x}) for node in children_of.get(x, [])))

        double_parent, children_of, parents_of = [], {}, {}
        for u, v in edges:
            children_of.setdefault(u, set()).add(v)
            if v in parents_of:  # two-parent case
                double_parent = [[u, v], [parents_of[v], v]]
            parents_of[v] = u

        if double_parent:
            u, v = double_parent.pop()  # tricky! if first candidate and cycle child to parent
            return [u, v] if cycle(v, u, set()) else double_parent.pop()

        # tricky! find last connection cycle by iterating backwards and detecting cycle from child to parent
        return next([u, v] for u, v in reversed(edges) if cycle(v, u, set()))


def test():
    arguments = [
        [[1, 2], [1, 3], [2, 3]],
        [[1, 2], [2, 3], [3, 4], [4, 1], [1, 5]],
        [[2, 1], [3, 1], [4, 2], [1, 4]],
        [[4, 1], [1, 5], [4, 2], [5, 1], [4, 3]],
    ]
    expectations = [
        [2, 3],
        [4, 1],
        [2, 1],
        [5, 1],
    ]
    for edges, expected in zip(arguments, expectations):
        solution = Solution().findRedundantDirectedConnection(edges)
        assert solution == expected, (solution, expected)
