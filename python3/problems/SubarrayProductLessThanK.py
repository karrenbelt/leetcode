### Source : https://leetcode.com/problems/subarray-product-less-than-k/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-17

##################################################################################################### 
#
# Your are given an array of positive integers nums.
# Count and print the number of (contiguous) subarrays where the product of all the elements in the 
# subarray is less than k.
# 
# Example 1:
# 
# Input: nums = [10, 5, 2, 6], k = 100
# Output: 8
# Explanation: The 8 subarrays that have product less than 100 are: [10], [5], [2], [6], [10, 5], [5, 
# 2], [2, 6], [5, 2, 6].
# Note that [10, 5, 2] is not included as the product of 100 is not strictly less than k.
# 
# Note:
# 0 .
# 0 .
# 0 .
#####################################################################################################

from typing import List


class Solution:
    def numSubarrayProductLessThanK(self, nums: List[int], k: int) -> int:
        # moving (expanding / shrinking) window. If we find a window < k, all smaller windows are also valid.
        # we expand on the right, while over the target shrink from the left, then add the size of the window
        i = ctr = 0
        product = 1
        for j in range(len(nums)):
            product *= nums[j]
            while product >= k and i <= j:
                product /= nums[i]
                i += 1
            ctr += j - i + 1
        return ctr


def test():
    arguments = [
        ([10, 5, 2, 6], 100),
        ]
    expectations = [8]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().numSubarrayProductLessThanK(nums, k)
        assert solution == expected
