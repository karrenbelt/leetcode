### Source : https://leetcode.com/problems/word-ladder/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# A transformation sequence from word beginWord to word endWord using a dictionary wordList is a 
# sequence of words beginWord -> s1 -> s2 -> ... -> sk such that:
# 
# 	Every adjacent pair of words differs by a single letter.
# 	Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in 
# wordList.
# 	sk == endWord
# 
# Given two words, beginWord and endWord, and a dictionary wordList, return the number of words in 
# the shortest transformation sequence from beginWord to endWord, or 0 if no such sequence exists.
# 
# Example 1:
# 
# Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
# Output: 5
# Explanation: One shortest transformation sequence is "hit" -> "hot" -> "dot" -> "dog" -> cog", 
# which is 5 words long.
# 
# Example 2:
# 
# Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
# Output: 0
# Explanation: The endWord "cog" is not in wordList, therefore there is no valid transformation 
# sequence.
# 
# Constraints:
# 
# 	1 <= beginWord.length <= 10
# 	endWord.length == beginWord.length
# 	1 <= wordList.length <= 5000
# 	wordList[i].length == beginWord.length
# 	beginWord, endWord, and wordList[i] consist of lowercase English letters.
# 	beginWord != endWord
# 	All the words in wordList are unique.
#####################################################################################################

from typing import List


class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        # from graph where we want the shortest path from start to end
        from collections import deque

        words = set(wordList)
        letters = set(''.join(words))
        queue = deque([(beginWord, 1)])
        while queue:
            node, distance = queue.popleft()
            if node == endWord:
                return distance
            for i in range(len(node)):
                for c in letters:
                    next_word = node[:i] + c + node[i+1:]
                    if next_word in words:
                        words.remove(next_word)
                        queue.append((next_word, distance + 1))
        return 0


def test():
    arguments = [
        ("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"]),
        ("hit", "cog", ["hot", "dot", "dog", "lot", "log"]),
        ("leet", "code", ["lest", "leet", "lose", "code", "lode", "robe", "lost"])
        ]
    expectations = [5, 0, 6]
    for (beginWord, endWord, wordList), expected in zip(arguments, expectations):
        solution = Solution().ladderLength(beginWord, endWord, wordList)
        assert solution == expected
