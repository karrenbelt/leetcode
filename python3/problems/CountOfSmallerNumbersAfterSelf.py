### Source : https://leetcode.com/problems/count-of-smaller-numbers-after-self/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-08

##################################################################################################### 
#
# You are given an integer array nums and you have to return a new counts array. The counts array has 
# the property where counts[i] is the number of smaller elements to the right of nums[i].
# 
# Example 1:
# 
# Input: nums = [5,2,6,1]
# Output: [2,1,1,0]
# Explanation:
# To the right of 5 there are 2 smaller elements (2 and 1).
# To the right of 2 there is only 1 smaller element (1).
# To the right of 6 there is 1 smaller element (1).
# To the right of 1 there is 0 smaller element.
# 
# Example 2:
# 
# Input: nums = [-1]
# Output: [0]
# 
# Example 3:
# 
# Input: nums = [-1,-1]
# Output: [0,0]
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-104 <= nums[i] <= 104
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode


class TreeNode(BinaryTreeNode):

    def __init__(self, value: int, left=None, right=None):
        super().__init__(value, left, right)
        self.count = 1
        self.left_tree_size = 0


class BinarySearchTree:

    def __init__(self):
        self.root = None

    def insert(self, value: int, node: TreeNode = None) -> int:
        if not self.root:
            self.root = TreeNode(value)
            return 0

        node = self.root if not node else node
        if value == node.value:
            node.count += 1
            return node.left_tree_size

        if value < node.value:
            node.left_tree_size += 1
            if not node.left:
                node.left = TreeNode(value)
                return 0
            return self.insert(value, node.left)

        if not node.right:
            node.right = TreeNode(value)
            return node.count + node.left_tree_size

        return node.count + node.left_tree_size + self.insert(value, node.right)


class BinaryIndexedTree:

    def __init__(self, n: int):
        self.size = n + 1
        self.tree = [0] * self.size

    def update(self, i: int):  # O(log n)
        while i < self.size:
            self.tree[i] += 1
            i += i & -i  # least significant bit

    def get_sum(self, i: int):  # O(log n)
        s = 0
        while i:
            s += self.tree[i]
            i -= i & -i
        return s


class Solution:
    def countSmaller(self, nums: List[int]) -> List[int]:  # brute force: O(n^2). Must give TLE
        return [sum(nums[j] < nums[i] for j in range(i + 1, len(nums))) for i in range(len(nums))]

    def countSmaller(self, nums: List[int]) -> List[int]:  # O(n^2) -> TLE
        counts, table, minimum = [], {}, min(nums)
        for n in reversed(nums):
            ctr = 0
            for target in range(minimum, n):
                ctr += table.get(target, 0)
            counts.append(ctr)
            table[n] = table.get(n, 0) + 1
        return counts[::-1]

    def countSmaller(self, nums: List[int]) -> List[int]:  # if balanced O(n log n), but it's not and hence O(n^2) time
        tree = BinarySearchTree()
        return [tree.insert(value) for value in reversed(nums)][::-1]

    def countSmaller(self, nums: List[int]) -> List[int]:  # O(n log n) time and O(n) space
        # https://www.hackerearth.com/practice/data-structures/advanced-data-structures/fenwick-binary-indexed-trees/tutorial/
        rank = {val: i + 1 for i, val in enumerate(sorted(nums))}
        fenwick_tree = BinaryIndexedTree(len(nums))
        count = []
        for n in reversed(nums):
            count.append(fenwick_tree.get_sum(rank[n] - 1))
            fenwick_tree.update(rank[n])
        return count[::-1]

    # def countSmaller(self, nums: List[int]) -> List[int]:  # segmentation tree
    #     return

    # def countSmaller(self, nums: List[int]) -> List[int]:  # merge-sort
    #     return

    # def countSmaller(self, nums):  # O(n^2) time. Faster than most O(n log n)
    #     import bisect
    #     counts, done = [], []
    #     for num in reversed(nums):
    #         counts.append(bisect.bisect_left(done, num))
    #         bisect.insort(done, num)
    #     return counts[::-1]


def test():
    arguments = [
        [5, 2, 6, 1],
        [-1],
        # [-1, -1],
        ]
    expectations = [
        [2, 1, 1, 0],
        [0],
        [0, 0],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().countSmaller(nums)
        assert solution == expected
test()
#
# t = [random.randint(-10_000, 10_000) for _ in range(100_000)]

