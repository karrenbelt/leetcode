### Source : https://leetcode.com/problems/non-negative-integers-without-consecutive-ones/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-25

##################################################################################################### 
#
# Given a positive integer n, return the number of the integers in the range [0, n] whose binary 
# representations do not contain consecutive ones.
# 
# Example 1:
# 
# Input: n = 5
# Output: 5
# Explanation:
# Here are the non-negative integers <= 5 with their corresponding binary representations:
# 0 : 0
# 1 : 1
# 2 : 10
# 3 : 11
# 4 : 100
# 5 : 101
# Among them, only integer 3 disobeys the rule (two consecutive ones) and the other 5 satisfy the 
# rule. 
# 
# Example 2:
# 
# Input: n = 1
# Output: 2
# 
# Example 3:
# 
# Input: n = 2
# Output: 3
# 
# Constraints:
# 
# 	1 <= n <= 109
#####################################################################################################


class Solution:
    def findIntegers(self, n: int) -> int:  # O(n log n) time and O(n) space
        # brute force will surely TLE, lets check if we see a pattern though
        numbers = [i for i in range(n + 1) if '11' not in bin(i)]
        return len(numbers)

    def findIntegers(self, n: int) -> int:  # log(n) time
        if n == 1:
            return 2

        fibonacci = [1, 2] + [0] * (n.bit_length() - 2)
        for i in range(2, n.bit_length()):
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2]

        flag, answer = 0, 0
        for i in range(n.bit_length()):
            print(i)
            if not flag and n & (1 << n.bit_length() - i - 1):  # s[i] == "1"
                flag = i > 0 and n & (1 << n.bit_length() - i)  # s[i - 1] == "1":
                answer += fibonacci[~i]
        return answer + int(not flag)

        flag, answer = 0, 0
        for i in range(n.bit_length() - 1, -1, -1):
            if not flag and n & (1 << i):
                flag = i < n.bit_length() - 1 and n & (1 << i + 1)
                answer += fibonacci[i]
        return answer + int(not flag)

    # def findIntegers(self, n: int) -> int:
    #     x, y = 1, 2
    #     res = 0
    #     n += 1
    #     while n:
    #         if n & 1 and n & 2:
    #             res = 0
    #         res += x * (n & 1)
    #         n >>= 1
    #         x, y = y, x + y
    #     return res
    #
    # def findIntegers(self, n: int) -> int:
    #     # finite-state machine
    #     one_less, zero_less, one_even, zero_even = 0, 0, 0, 1
    #     for b in bin(n)[2:]:
    #         one_less, zero_less, one_even, zero_even = (
    #             zero_less,
    #             one_less + zero_less + (zero_even + one_even) * (b == "1"),
    #             zero_even * (b == "1"),
    #             (zero_even + one_even) * (b != "1"))
    #
    #     return one_less + zero_less + one_even + zero_even


def test():
    def show_pattern(number: int = 1000):  # what does the pattern look like?
        d = {}
        for binary in filter(lambda b: not '11' in b, map(bin, range(number + 1))):
            d.setdefault(len(binary[2:]), []).append(binary)
        for k, v in sorted(d.items()):
            print(k, len(v))

    arguments = [5, 1, 2, 100, 1000, 3]
    expectations = [5, 2, 3, 34, 144, 3]
    for n, expected in zip(arguments, expectations):
        solution = Solution().findIntegers(n)
        assert solution == expected, (solution, expected)
test()
Solution().findIntegers(3)