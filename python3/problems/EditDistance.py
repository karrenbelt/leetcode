### Source : https://leetcode.com/problems/edit-distance/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

##################################################################################################### 
#
# Given two strings word1 and word2, return the minimum number of operations required to convert 
# word1 to word2.
# 
# You have the following three operations permitted on a word:
# 
# 	Insert a character
# 	Delete a character
# 	Replace a character
# 
# Example 1:
# 
# Input: word1 = "horse", word2 = "ros"
# Output: 3
# Explanation: 
# horse -> rorse (replace 'h' with 'r')
# rorse -> rose (remove 'r')
# rose -> ros (remove 'e')
# 
# Example 2:
# 
# Input: word1 = "intention", word2 = "execution"
# Output: 5
# Explanation: 
# intention -> inention (remove 't')
# inention -> enention (replace 'i' with 'e')
# enention -> exention (replace 'n' with 'x')
# exention -> exection (replace 'n' with 'c')
# exection -> execution (insert 'u')
# 
# Constraints:
# 
# 	0 <= word1.length, word2.length <= 500
# 	word1 and word2 consist of lowercase English letters.
#####################################################################################################


class Solution:
    def minDistance(self, word1: str, word2: str) -> int:  # O(mn) time and O(mn) space

        def compare(i, j):
            if i == len(word1) and j == len(word2):
                return 0
            if i == len(word1):
                return len(word2) - j
            if j == len(word2):
                return len(word1) - i
            if (i, j) in memo:
                return memo[i, j]
            if word1[i] == word2[j]:
                distance = compare(i + 1, j + 1)
            else:
                insert = compare(i, j + 1)
                delete = compare(i + 1,  j)
                replace = compare(i + 1, j + 1)
                distance = 1 + min(insert, delete, replace)
            memo[i, j] = distance
            return memo[i, j]

        memo = {}
        return compare(0, 0)

    def minDistance(self, word1: str, word2: str) -> int:
        from functools import lru_cache

        @lru_cache
        def compare(i: int, j: int) -> int:
            if i == len(word1) or j == len(word2):
                return (len(word2) - j) or (len(word1) - i)
            if word1[i] == word2[j]:
                return compare(i + 1, j + 1)
            insert = compare(i + 1, j)
            delete = compare(i, j + 1)
            replace = compare(i + 1, j + 1)
            return min(insert, delete, replace) + 1

        return compare(0, 0)

    def minDistance(self, word1: str, word2: str) -> int:  # O(mn) time and O(mn) space
        dp = [[0] * (len(word2) + 1) for _ in range(len(word1) + 1)]
        for i in range(len(word1) + 1):
            dp[i][0] = i
        for j in range(len(word2) + 1):
            dp[0][j] = j
        for i in range(1, len(word1) + 1):
            for j in range(1, len(word2) + 1):
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    replace = dp[i - 1][j]
                    insert = dp[i][j - 1]
                    delete = dp[i - 1][j - 1]
                    dp[i][j] = 1 + min(insert, delete, replace)
        return dp[-1][-1]


def test():
    arguments = [
        ("horse", "ros"),
        ("intention", "execution"),
        ]
    expectations = [3, 5]
    for (word1, word2), expected in zip(arguments, expectations):
        solution = Solution().minDistance(word1, word2)
        assert solution == expected
