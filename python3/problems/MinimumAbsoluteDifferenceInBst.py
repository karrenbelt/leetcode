### Source : https://leetcode.com/problems/minimum-absolute-difference-in-bst/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Given a binary search tree with non-negative values, find the minimum absolute difference between 
# values of any two nodes.
# 
# Example:
# 
# Input:
# 
#    1
#     \
#      3
#     /
#    2
# 
# Output:
# 1
# 
# Explanation:
# The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).
# 
# Note:
# 
# 	There are at least two nodes in this BST.
# 	This question is the same as 783: 
# https://leetcode.com/problems/minimum-distance-between-bst-nodes/
#####################################################################################################

# same question:
# 530: https://leetcode.com/problems/minimum-absolute-difference-in-bst/
# 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def getMinimumDifference(self, root: TreeNode) -> int:  # local vs global nightmare

        def traverse(node: TreeNode) -> None:
            nonlocal minimum, prev
            if not node:
                return
            traverse(node.left)
            minimum = min(minimum, node.val - prev)
            prev = node.val
            traverse(node.right)

        minimum = float('inf')
        prev = float('-inf')
        traverse(root)
        return minimum

    def getMinimumDifference(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, low: int, high: int) -> int:
            if not node:
                return high - low
            left = traverse(node.left, low, node.val)
            right = traverse(node.right, node.val, high)
            return min(left, right)

        return traverse(root, float('-inf'), float('inf'))

    def getMinimumDifference(self, root: TreeNode) -> int:
        stack = []
        node = root
        prev = None
        minimum = float('inf')
        while node or stack:
            if node:
                stack.append(node)
                node = node.left
            else:
                node = stack.pop()
                if prev:
                    minimum = min(minimum, abs(prev.val - node.val))
                prev = node
                node = node.right
        return minimum


def test():
    from python3 import Codec
    arguments = [
        "[4,2,6,1,3]",
        "[1,0,48,null,null,12,49]",
        ]
    expectations = [1, 1]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().getMinimumDifference(root)
        assert solution == expected
