### Source : https://leetcode.com/problems/valid-perfect-square/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given a positive integer num, write a function which returns True if num is a perfect square else 
# False.
# 
# Follow up: Do not use any built-in library function such as sqrt.
# 
# Example 1:
# Input: num = 16
# Output: true
# Example 2:
# Input: num = 14
# Output: false
# 
# Constraints:
# 
# 	1 <= num <= 231 - 1
#####################################################################################################


class Solution:
    def isPerfectSquare(self, num: int) -> bool:  # no built-in sqrt!
        return (num**0.5).is_integer()

    def isPerfectSquare(self, num: int) -> bool:  # newton's method: O(log n) time O(1) space
        r = num
        while r * r > num:
            r = (r + num / r) // 2
        return r * r == num

    def isPerfectSquare(self, num: int) -> bool:  # binary search: O(log n) time O(1) space
        low, high = 0, num // 2
        while low <= high:
            mid = low + (high - low) // 2
            square = mid * mid
            if square == num:
                return True
            elif square < num:
                low = mid + 1
            else:
                high = mid - 1
        return False


def test():
    numbers = [16, 14]
    expectations = [True, False]
    for num, expected in zip(numbers, expectations):
        solution = Solution().isPerfectSquare(num)
        assert solution == expected
