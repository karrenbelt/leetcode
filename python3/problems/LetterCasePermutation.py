### Source : https://leetcode.com/problems/letter-case-permutation/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-16

##################################################################################################### 
#
# Given a string S, we can transform every letter individually to be lowercase or uppercase to create 
# another string.
# 
# Return a list of all possible strings we could create. You can return the output in any order.
# 
# Example 1:
# 
# Input: S = "a1b2"
# Output: ["a1b2","a1B2","A1b2","A1B2"]
# 
# Example 2:
# 
# Input: S = "3z4"
# Output: ["3z4","3Z4"]
# 
# Example 3:
# 
# Input: S = "12345"
# Output: ["12345"]
# 
# Example 4:
# 
# Input: S = "0"
# Output: ["0"]
# 
# Constraints:
# 
# 	S will be a string with length between 1 and 12.
# 	S will consist only of letters or digits.
#####################################################################################################

from typing import List


class Solution:
    def letterCasePermutation(self, S: str) -> List[str]:  # too many calls, real slow

        def backtrack(index, path):
            if len(path) == len(S):
                permutations.add(path)
            else:
                for i in range(index, len(S)):
                    backtrack(i + 1, path + S[i].lower())
                    backtrack(i + 1, path + S[i].upper())

        permutations = set()
        backtrack(0, '')
        return list(permutations)

    def letterCasePermutation(self, S: str) -> List[str]:

        def backtrack(i: int, characters: List[str]):
            if i == len(characters):
                permutations.append(''.join(characters))
                return
            backtrack(i + 1, characters)
            if characters[i].isalpha():
                characters[i] = chr(ord(characters[i]) ^ (1 << 5))
                backtrack(i + 1, characters)

        permutations = []
        backtrack(0, list(S))
        return permutations

    def letterCasePermutation(self, S: str) -> List[str]:  # O(2^n) time
        permutations = ['']
        for c in S:
            if c.isalpha():
                permutations = [i + j for i in permutations for j in (c.upper(), c.lower())]
            else:
                permutations = [i + c for i in permutations]
        return permutations

    def letterCasePermutation(self, S: str) -> List[str]:  # O(2^n) time
        permutations = [S]
        for i, c in enumerate(S):
            if c.isalpha():
                permutations.extend([s[:i] + s[i].swapcase() + s[i + 1:] for s in permutations])
        return permutations


def test():
    from collections import Counter
    arguments = ["a1b2", "3z4", "12345", "0"]
    expectations = [
        ["a1b2", "a1B2", "A1b2", "A1B2"],
        ["3z4", "3Z4"],
        ["12345"],
        ["0"],
    ]
    for S, expected in zip(arguments, expectations):
        solution = Solution().letterCasePermutation(S)
        assert Counter(solution) == Counter(expected)
test()