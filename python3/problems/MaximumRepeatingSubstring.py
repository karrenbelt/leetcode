### Source : https://leetcode.com/problems/maximum-repeating-substring/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# For a string sequence, a string word is k-repeating if word concatenated k times is a substring of 
# sequence. The word's maximum k-repeating value is the highest value k where word is k-repeating in 
# sequence. If word is not a substring of sequence, word's maximum k-repeating value is 0.
# 
# Given strings sequence and word, return the maximum k-repeating value of word in sequence.
# 
# Example 1:
# 
# Input: sequence = "ababc", word = "ab"
# Output: 2
# Explanation: "abab" is a substring in "ababc".
# 
# Example 2:
# 
# Input: sequence = "ababc", word = "ba"
# Output: 1
# Explanation: "ba" is a substring in "ababc". "baba" is not a substring in "ababc".
# 
# Example 3:
# 
# Input: sequence = "ababc", word = "ac"
# Output: 0
# Explanation: "ac" is not a substring in "ababc". 
# 
# Constraints:
# 
# 	1 <= sequence.length <= 100
# 	1 <= word.length <= 100
# 	sequence and word contains only lowercase English letters.
#####################################################################################################


class Solution:
    def maxRepeating(self, sequence: str, word: str) -> int:  # O(n^2) time and O(n) space
        ctr = 0
        while word * (ctr + 1) in sequence:
            ctr += 1
        return ctr

    def maxRepeating(self, sequence: str, word: str) -> int:

        def kmpTable(s: str):
            kmp = [0] * len(s)
            for i in range(1, len(s)):
                idx = kmp[i - 1]
                while idx > 0 and s[idx] != s[i]:
                    idx = kmp[idx - 1]  # trace backwards to find the last matching char
                if s[i] == s[idx]:  # matches next
                    idx += 1
                kmp[i] = idx
            return kmp

        max_rep = word * (len(sequence) // len(word)) + '$'
        kmp = kmpTable(max_rep)

        repeats = j = 0
        for c in sequence:
            while j and max_rep[j] != c:
                j = kmp[j - 1]
            if max_rep[j] == c:
                j += 1
                repeats = max(repeats, j) // len(word)
        return repeats

    def maxRepeating(self, sequence: str, word: str) -> int:  # O(n) time
        # Knuth-Morris-Pratt algorithm
        if len(sequence) < len(word):
            return 0

        pattern = word * (len(sequence) // len(word))
        lps = [0]
        k = 0
        for i in range(1, len(pattern)):
            while k and pattern[k] != pattern[i]:
                k = lps[k - 1]
            if pattern[i] == pattern[k]:
                k += 1
            lps.append(k)

        ans = k = 0
        for i in range(len(sequence)):
            while k and pattern[k] != sequence[i]:
                k = lps[k - 1]
            if pattern[k] == sequence[i]:
                k += 1
            ans = max(ans, k // len(word))
            if k == len(pattern):
                break
        return ans


def test():
    arguments = [
        ("ababc", "ab"),
        ("ababc", "ba"),
        ("ababc", "ac"),
        ("aaabaaaabaaabaaaabaaaabaaaabaaaaba", "aaaba")
    ]
    expectations = [2, 1, 0, 5]
    for (sequence, word), expected in zip(arguments, expectations):
        solution = Solution().maxRepeating(sequence, word)
        assert solution == expected
test()