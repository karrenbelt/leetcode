### Source : https://leetcode.com/problems/nth-digit/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-19

##################################################################################################### 
#
# Given an integer n, return the nth digit of the infinite integer sequence [1, 2, 3, 4, 5, 6, 7, 8, 
# 9, 10, 11, ...].
# 
# Example 1:
# 
# Input: n = 3
# Output: 3
# 
# Example 2:
# 
# Input: n = 11
# Output: 0
# Explanation: The 11th digit of the sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... is a 0, which is 
# part of the number 10.
# 
# Constraints:
# 
# 	1 <= n <= 231 - 1
#####################################################################################################


class Solution:
    def findNthDigit(self, n: int) -> int:  # O(n) time, should yield TLE
        start = size = 1
        while n > size:
            n, start = n - size, start + 1
            size = len(str(start))
        return int(str(start)[n-1])

    def findNthDigit(self, n: int) -> int:  # O(log n) time and O(1) space
        # need to search ranges 1-9, 10-99, 100-999, etc.
        start, size, step = 1, 1, 9
        while n > size * step:
            n, size, step, start = n - (size * step), size + 1, step * 10, start * 10
        return int(str(start + (n - 1) // size)[(n - 1) % size])


def test():
    arguments = [3, 11]
    expectations = [3, 0]
    for n, expected in zip(arguments, expectations):
        solution = Solution().findNthDigit(n)
        assert solution == expected
