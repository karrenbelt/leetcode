### Source : https://leetcode.com/problems/min-cost-climbing-stairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-07

##################################################################################################### 
#
# You are given an integer array cost where cost[i] is the cost of ith step on a staircase. Once you 
# pay the cost, you can either climb one or two steps.
# 
# You can either start from the step with index 0, or the step with index 1.
# 
# Return the minimum cost to reach the top of the floor.
# 
# Example 1:
# 
# Input: cost = [10,15,20]
# Output: 15
# Explanation: Cheapest is: start on cost[1], pay that cost, and go to the top.
# 
# Example 2:
# 
# Input: cost = [1,100,1,1,1,100,1,1,100,1]
# Output: 6
# Explanation: Cheapest is: start on cost[0], and only step on 1s, skipping cost[3].
# 
# Constraints:
# 
# 	2 <= cost.length <= 1000
# 	0 <= cost[i] <= 999
#####################################################################################################

from typing import List


class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:  # O(n) time and O(n) space
        dp = [0] * (len(cost) + 1)
        for i in range(len(cost)):
            dp[i + 1] = min(dp[i - 1] + cost[i], dp[i] + cost[i])
        return min(dp[-2:])

    def minCostClimbingStairs(self, cost: List[int]) -> int:  # O(n) time and O(1) space
        a = b = 0
        for i in range(len(cost)):
            a, b = b, min(a, b) + cost[i]
        return min(a, b)


def test():
    arguments = [
        [10, 15, 20],
        [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
    ]
    expectations = [15, 6]
    for cost, expected in zip(arguments, expectations):
        solution = Solution().minCostClimbingStairs(cost)
        assert solution == expected, solution
test()