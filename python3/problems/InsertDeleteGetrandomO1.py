### Source : https://leetcode.com/problems/insert-delete-getrandom-o1/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Implement the RandomizedSet class:
# 
# 	bool insert(int val) Inserts an item val into the set if not present. Returns true if the 
# item was not present, false otherwise.
# 	bool remove(int val) Removes an item val from the set if present. Returns true if the item 
# was present, false otherwise.
# 	int getRandom() Returns a random element from the current set of elements (it's guaranteed 
# that at least one element exists when this method is called). Each element must have the same 
# probability of being returned.
# 
# Follow up: Could you implement the functions of the class with each function works in average O(1) 
# time?
# 
# Example 1:
# 
# Input
# ["RandomizedSet", "insert", "remove", "insert", "getRandom", "remove", "insert", "getRandom"]
# [[], [1], [2], [2], [], [1], [2], []]
# Output
# [null, true, false, true, 2, true, false, 2]
# 
# Explanation
# RandomizedSet randomizedSet = new RandomizedSet();
# randomizedSet.insert(1); // Inserts 1 to the set. Returns true as 1 was inserted successfully.
# randomizedSet.remove(2); // Returns false as 2 does not exist in the set.
# randomizedSet.insert(2); // Inserts 2 to the set, returns true. Set now contains [1,2].
# randomizedSet.getRandom(); // getRandom() should return either 1 or 2 randomly.
# randomizedSet.remove(1); // Removes 1 from the set, returns true. Set now contains [2].
# randomizedSet.insert(2); // 2 was already in the set, so return false.
# randomizedSet.getRandom(); // Since 2 is the only number in the set, getRandom() will always return 
# 2.
# 
# Constraints:
# 
# 	-231 <= val <= 231 - 1
# 	At most 105 calls will be made to insert, remove, and getRandom.
# 	There will be at least one element in the data structure when getRandom is called.
#####################################################################################################

import random


class RandomizedSet:

    def __init__(self):
        self.values = []
        self.locations = {}

    def insert(self, val: int) -> bool:
        """ Inserts a value to the set. Returns true if the set did not already contain the specified element. """
        if val in self.locations:  # O(1)
            return False
        self.values.append(val)  # O(1)
        self.locations[val] = len(self.values) - 1  # O(1)
        return True

    def remove(self, val: int) -> bool:
        """ Removes a value from the set. Returns true if the set contained the specified element. """
        if val in self.locations:  # O(1)
            last_value = self.values[-1]  # O(1)
            if last_value != val:
                pos = self.locations[val]  # O(1)
                self.values[pos], self.values[-1] = self.values[-1], self.values[pos]  # O(1)
                self.locations[last_value] = pos  # O(1)
            self.values.pop()  # O(1)
            self.locations.pop(val)  # O(1)
            return True
        return False

    def getRandom(self) -> int:
        """ Get a random element from the set. """
        return self.values[random.randint(0, len(self.values) - 1)]  # O(1)


class RandomizedSet:
    # kind of trivial solution
    def __init__(self):
        self.values = set()

    def insert(self, val: int) -> bool:
        """ Inserts a value to the set. Returns true if the set did not already contain the specified element. """
        return False if val in self.values else self.values.add(val) or True

    def remove(self, val: int) -> bool:
        """ Removes a value from the set. Returns true if the set contained the specified element. """
        return False if val not in self.values else self.values.remove(val) or True

    def getRandom(self) -> int:
        """ Get a random element from the set. """
        return random.sample(self.values, 1).pop()

# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()


def test():
    class RandomNumber:  # just for this single test case
        def __eq__(self, other):
            return other in {1, 2}
    operations = ["RandomizedSet", "insert", "remove", "insert", "getRandom", "remove", "insert", "getRandom"]
    arguments = [[], [1], [2], [2], [], [1], [2], []]
    expectations = [None, True, False, True, RandomNumber(), True, False, RandomNumber()]
    obj = RandomizedSet()
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
