### Source : https://leetcode.com/problems/wildcard-matching/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for 
# '?' and '*' where:
# 
# 	'?' atches any single character.
# 	'*' atches any sequence of characters (including the empty sequence).
# 
# The matching should cover the entire input string (not partial).
# 
# Example 1:
# 
# Input: s = "aa", p = "a"
# Output: false
# Explanation: "a" does not match the entire string "aa".
# 
# Example 2:
# 
# Input: s = "aa", p = "*"
# Output: true
# Explanation: '*' matches any sequence.
# 
# Example 3:
# 
# Input: s = "cb", p = "?a"
# Output: false
# Explanation: '?' matches 'c', but the second letter is 'a', which does not match 'b'.
# 
# Example 4:
# 
# Input: s = "adceb", p = "*a*b"
# Output: true
# Explanation: The first '*' matches the empty sequence, while the second '*' matches the substring 
# "dce".
# 
# Example 5:
# 
# Input: s = "acdcb", p = "a*c?b"
# Output: false
# 
# Constraints:
# 
# 	0 <= s.length, p.length <= 2000
# 	s contains only lowercase English letters.
# 	p contains only lowercase English letters, '?' or '*'.
#####################################################################################################

class Solution:
    # def isMatch(self, s: str, p: str) -> bool:  # wrong: 1546 / 1811 test cases passed.
    #     # TODO: need to deal with the notorious *? pattern
    #     i = j = 0
    #     while i < len(s) and j < len(p):
    #         if p[j] == '*':  # deal with one or many wild cards
    #             q_ctr = 0
    #             while j < len(p) and p[j] in {'*', '?'}:
    #                 if p[j] == '?':
    #                     q_ctr += 1
    #                 j += 1
    #             start = j
    #             while j < len(p) and p[j] not in {'*', '?'}:
    #                 j += 1
    #             if start == j:  # no pattern after
    #                 return True
    #             i = s.find(p[start: j])
    #             if i == -1:
    #                 return False
    #             i += j - start + q_ctr
    #         elif j < len(p) and p[j] == '?':  # simply increment both pointers
    #             i += 1
    #             j += 1
    #         else:
    #             if i >= len(s):
    #                 return False
    #             elif j >= len(p):
    #                 return i == len(s)
    #             elif s[i] != p[j]:
    #                 return False
    #             i += 1
    #             j += 1
    #     return i == len(s)

    def isMatch(self, s: str, p: str) -> bool:  # greedy dfs: O(s) time
        i = j = 0
        star = -1
        match = 0
        while i < len(s):
            if j < len(p) and (p[j] == s[i] or p[j] == '?'):
                i += 1
                j += 1
            elif j < len(p) and p[j] == '*':
                match = i
                star = j
                j += 1
            elif star != -1:
                j = star + 1
                match += 1
                i = match
            else:
                return False

        while j < len(p) and p[j] == '*':
            j += 1
        return j == len(p)

    def isMatch(self, s: str, p: str) -> bool:  # finite state machine: O(p * s) time
        transfer = {}
        state = 0
        for char in p:
            if char == '*':
                transfer[state, char] = state
            else:
                transfer[state, char] = state + 1
                state += 1

        accept = state
        states = {0}
        for char in s:
            states = {transfer.get((at, token)) for at in states if at is not None for token in {char, '*', '?'}}
        return accept in states

    def isMatch(self, s: str, p: str) -> bool:  # wtf
        nexts = 1
        end_bit = 1 << len(s)
        d = {}
        for i, c in enumerate(s):
            d[c] = d.get(c, 0) | 1 << i
        for c in p:
            if c == '*':
                nexts = (end_bit << 1) - (nexts & -nexts)
            elif c == '?':
                nexts <<= 1
            else:
                nexts = (nexts & d.get(c, 0)) << 1
            if nexts == 0:
                return False
        return nexts & end_bit != 0


def test():
    arguments = [
        ("aa", "a"),
        ("aa", "*"),
        ("cb", "?a"),
        ("adceb", "*a*b"),
        ("acdcb", "a*c?b"),
        ("mississippi",  # mis siss i ppi
         "m??*ss*?i*pi"),  # m?? *ss *? i*pi
    ]
    expectations = [False, True, False, True, False, False]
    for (s, p), expected in zip(arguments, expectations):
        solution = Solution().isMatch(s, p)
        assert solution == expected
