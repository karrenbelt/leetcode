### Source : https://leetcode.com/problems/flood-fill/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# 
# An image is represented by a 2-D array of integers, each integer representing the pixel value of 
# the image (from 0 to 65535).
# 
# Given a coordinate (sr, sc) representing the starting pixel (row and column) of the flood fill, and 
# a pixel value newColor, "flood fill" the image.
# 
# To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally 
# to the starting pixel of the same color as the starting pixel, plus any pixels connected 
# 4-directionally to those pixels (also with the same color as the starting pixel), and so on.  
# Replace the color of all of the aforementioned pixels with the newColor.
# 
# At the end, return the modified image.
# 
# Example 1:
# 
# Input: 
# image = [[1,1,1],[1,1,0],[1,0,1]]
# sr = 1, sc = 1, newColor = 2
# Output: [[2,2,2],[2,2,0],[2,0,1]]
# Explanation: 
# From the center of the image (with position (sr, sc) = (1, 1)), all pixels connected 
# by a path of the same color as the starting pixel are colored with the new color.
# Note the bottom corner is not colored 2, because it is not 4-directionally connected
# to the starting pixel.
# 
# Note:
# The length of image and image[0] will be in the range [1, 50].
# The given starting pixel will satisfy 0  and 0 .
# The value of each color in image[i][j] and newColor will be an integer in [0, 65535].
#####################################################################################################

from typing import List


class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:

        def dfs(i: int, j: int):
            if 0 <= i < len(image) and 0 <= j < len(image[0]) and image[i][j] == old_color:
                image[i][j] = newColor
                for x, y in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
                    dfs(i + x, j + y)

        old_color = image[sr][sc]
        return image if old_color == newColor else dfs(sr, sc) or image

    def floodFill(self, image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:
        from collections import deque

        queue, old_color = deque([(sr, sc)] if image[sr][sc] != newColor else []), image[sr][sc]
        while queue:
            i, j = queue.popleft()
            if 0 <= i < len(image) and 0 <= j < len(image[0]) and image[i][j] == old_color:
                image[i][j] = newColor
                queue.extend([(i + x, j + y) for x, y in [(0, 1), (1, 0), (0, -1), (-1, 0)]])
        return image


def test():
    arguments = [
        ([[1, 1, 1], [1, 1, 0], [1, 0, 1]], 1, 1, 2),
        ([[0, 0, 0], [0, 1, 1]], 1, 1, 1),
        ([[0, 0, 0], [1, 0, 0]], 1, 0, 2),
    ]
    expectations = [
        [[2, 2, 2], [2, 2, 0], [2, 0, 1]],
        [[0, 0, 0], [0, 1, 1]],
        [[0, 0, 0], [2, 0, 0]],
    ]
    for (image, sr, sc, newColor), expected in zip(arguments, expectations):
        solution = Solution().floodFill(image, sr, sc, newColor)
        assert solution == expected


test()
