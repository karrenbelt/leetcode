### Source : https://leetcode.com/problems/plus-one/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given a non-empty array of decimal digits representing a non-negative integer, increment one to the 
# integer.
# 
# The digits are stored such that the most significant digit is at the head of the list, and each 
# element in the array contains a single digit.
# 
# You may assume the integer does not contain any leading zero, except the number 0 itself.
# 
# Example 1:
# 
# Input: digits = [1,2,3]
# Output: [1,2,4]
# Explanation: The array represents the integer 123.
# 
# Example 2:
# 
# Input: digits = [4,3,2,1]
# Output: [4,3,2,2]
# Explanation: The array represents the integer 4321.
# 
# Example 3:
# 
# Input: digits = [0]
# Output: [1]
# 
# Constraints:
# 
# 	1 <= digits.length <= 100
# 	0 <= digits[i] <= 9
#####################################################################################################

from typing import List


class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        for i in range(len(digits)-1, -1, -1):
            if digits[i] == 9:
                digits[i] = 0
                if i == 0:
                    digits[0:0] = [1]
                    break
            else:
                digits[i] += 1
                break
        return digits

    def plusOne(self, digits: List[int]) -> List[int]:
        digits = digits or [0]
        last = digits.pop()
        if last == 9:
            return self.plusOne(digits) + [0]
        return digits + [last + 1]


def test():
    arrays_of_numbers = [
        [1, 2, 3],
        [4, 3, 2, 1],
        [0],
        ]
    expectations = [
        [1, 2, 4],
        [4, 3, 2, 2],
        [1]
        ]
    for digits, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().plusOne(digits)
        assert solution == expected
