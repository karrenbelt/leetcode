### Source : https://leetcode.com/problems/frequency-of-the-most-frequent-element/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

##################################################################################################### 
#
# The frequency of an element is the number of times it occurs in an array.
# 
# You are given an integer array nums and an integer k. In one operation, you can choose an index of 
# nums and increment the element at that index by 1.
# 
# Return the maximum possible frequency of an element after performing at most k operations.
# 
# Example 1:
# 
# Input: nums = [1,2,4], k = 5
# Output: 3
# Explanation: Increment the first element three times and the second element two times to make nums 
# = [4,4,4].
# 4 has a frequency of 3.
# 
# Example 2:
# 
# Input: nums = [1,4,8,13], k = 5
# Output: 2
# Explanation: There are multiple optimal solutions:
# - Increment the first element three times to make nums = [4,4,8,13]. 4 has a frequency of 2.
# - Increment the second element four times to make nums = [1,8,8,13]. 8 has a frequency of 2.
# - Increment the third element five times to make nums = [1,4,13,13]. 13 has a frequency of 2.
# 
# Example 3:
# 
# Input: nums = [3,9,6], k = 2
# Output: 1
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	1 <= nums[i] <= 105
# 	1 <= k <= 105
#####################################################################################################

from typing import List


class Solution:
    def maxFrequency(self, nums: List[int], k: int) -> int:  # O(n log n) time
        return  # sliding window

    def maxFrequency(self, nums: List[int], k: int) -> int:  # O(n log n) time
        return  # binary search

    def maxFrequency(self, nums: List[int], k: int) -> int:  # O(n log n) time
        import heapq
        nums.sort()
        ctr, maximum, prev, heap = 1, 1, nums[0], [nums[0]]
        for num in nums[1:]:
            heapq.heappush(heap, num)
            k -= ctr * (num - prev)
            ctr += 1
            prev = num
            while k < 0:
                cur = heapq.heappop(heap)
                k += prev - cur
                ctr -= 1
            maximum = max(maximum, ctr)
        return maximum


def test():
    arguments = [
        ([1, 2, 4], 5),
        ([1, 4, 8, 13], 5),
        ([3, 6, 9], 2),
    ]
    expectations = [3, 2, 1]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().maxFrequency(nums, k)
        assert solution == expected
