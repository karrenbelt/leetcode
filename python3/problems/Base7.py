### Source : https://leetcode.com/problems/base-7/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-14

##################################################################################################### 
#
# Given an integer, return its base 7 string representation.
# 
# Example 1:
# 
# Input: 100
# Output: "202"
# 
# Example 2:
# 
# Input: -7
# Output: "-10"
# 
# Note:
# The input will be in range of [-1e7, 1e7].
#####################################################################################################


class Solution:
    def convertToBase7(self, num: int) -> str:
        n, base7 = abs(num), ''
        while n:
            n, remainder = divmod(n, 7)
            base7 += str(remainder)
        return ('-' if num < 0 else '') + base7[::-1]

    def convertToBase7(self, num: int) -> str:
        if num < 0:
            return '-' + self.convertToBase7(-num)
        if num < 7:
            return str(num)
        return self.convertToBase7(num // 7) + str(num % 7)


def test():
    arguments = [100, -7]
    expectations = ["202", "-10"]
    for num, expected in zip(arguments, expectations):
        solution = Solution().convertToBase7(num)
        assert solution == expected
