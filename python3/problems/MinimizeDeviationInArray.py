### Source : https://leetcode.com/problems/minimize-deviation-in-array/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-24

##################################################################################################### 
#
# You are given an array nums of n positive integers.
# 
# You can perform two types of operations on any element of the array any number of times:
# 
# 	If the element is even, divide it by 2.
# 
# 		For example, if the array is [1,2,3,4], then you can do this operation on the last 
# element, and the array will be [1,2,3,2].
# 
# 	If the element is odd, multiply it by 2.
# 
# 		For example, if the array is [1,2,3,4], then you can do this operation on the first 
# element, and the array will be [2,2,3,4].
# 
# The deviation of the array is the maximum difference between any two elements in the array.
# 
# Return the minimum deviation the array can have after performing some number of operations.
# 
# Example 1:
# 
# Input: nums = [1,2,3,4]
# Output: 1
# Explanation: You can transform the array to [1,2,3,2], then to [2,2,3,2], then the deviation will 
# be 3 - 2 = 1.
# 
# Example 2:
# 
# Input: nums = [4,1,5,20,3]
# Output: 3
# Explanation: You can transform the array after two operations to [4,2,5,5,3], then the deviation 
# will be 5 - 2 = 3.
# 
# Example 3:
# 
# Input: nums = [2,10,8]
# Output: 3
# 
# Constraints:
# 
# 	n == nums.length
# 	2 <= n <= 5 * 104
# 	1 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def minimumDeviation(self, nums: List[int]) -> int:
        from sortedcontainers import SortedSet

        # 1. create array of maxima multiply uneven, keep even as is
        # 2. compute deviation max and min
        # 3. while maximum is even
        #    a. divide by two and update
        #    b. update minimum deviation
        #    c. repeat 2.

        s = SortedSet(n << 1 if n % 2 else n for n in nums)
        deviation = s[-1] - s[0]
        while s[-1] % 2 == 0:
            s.add(s.pop(-1) >> 1)
            deviation = min(deviation, s[-1] - s[0])

        return deviation

    def minimumDeviation(self, nums):
        import heapq

        heap = []
        for n in nums:
            heapq.heappush(heap, -n * 2 if n % 2 else -n)

        deviation, minimum = 1 << 31, -max(heap)
        while len(heap) == len(nums):
            n = -heapq.heappop(heap)
            deviation = min(deviation, n - minimum)
            if n % 2 == 0:
                minimum = min(minimum, n // 2)
                heapq.heappush(heap, -n // 2)
        return deviation


def test():
    arguments = [
        [1, 2, 3, 4],
        [4, 1, 5, 20, 3],
        [2, 10, 8],
    ]
    expectations = [1, 3, 3]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minimumDeviation(nums)
        assert solution == expected
