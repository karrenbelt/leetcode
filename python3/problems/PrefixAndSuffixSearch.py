### Source : https://leetcode.com/problems/prefix-and-suffix-search/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Design a special dictionary which has some words and allows you to search the words in it by a 
# prefix and a suffix.
# 
# Implement the WordFilter class:
# 
# 	WordFilter(string[] words) Initializes the object with the words in the dictionary.
# 	f(string prefix, string suffix) Returns the index of the word in the dictionary which has 
# the prefix prefix and the suffix suffix. If there is more than one valid index, return the largest 
# of them. If there is no such word in the dictionary, return -1.
# 
# Example 1:
# 
# Input
# ["WordFilter", "f"]
# [[["apple"]], ["a", "e"]]
# Output
# [null, 0]
# 
# Explanation
# WordFilter wordFilter = new WordFilter(["apple"]);
# wordFilter.f("a", "e"); // return 0, because the word at index 0 has prefix = "a" and suffix = 'e".
# 
# Constraints:
# 
# 	1 <= words.length <= 15000
# 	1 <= words[i].length <= 10
# 	1 <= prefix.length, suffix.length <= 10
# 	words[i], prefix and suffix consist of lower-case English letters only.
# 	At most 15000 calls will be made to the function f.
#####################################################################################################

from typing import List


class TrieNode:

    def __init__(self):
        self.children = {}
        self.weight = -1


class Trie:

    def __init__(self):
        self.root = TrieNode()

    def insert(self, word, i):
        node = self.root
        node.weight = i
        for char in word:
            if char not in node.children:
                node.children[char] = TrieNode()
            node = node.children[char]
            node.weight = i

    def search(self, word):
        node = self.root
        for char in word:
            if char not in node.children:
                return -1
            node = node.children[char]
        return node.weight


class WordFilter:

    def __init__(self, words: List[str]):  # This idea is awesome
        self.trie = Trie()
        for i, word in enumerate(words):
            for j in range(len(word) + 1):
                self.trie.insert(f'{word[j:]}.{words[i]}', i)

    def f(self, prefix: str, suffix: str) -> int:
        return self.trie.search(f'{suffix}.{prefix}')


class WordFilter:
    def __init__(self, words: List[str]):  # easiest to understand. At most 121 prefix-suffix combinations per word.
        self.dictionary = {}
        for i, word in enumerate(words):
            prefix = ''
            for c in word:
                prefix += c
                suffix = ''
                for c in word[::-1]:
                    suffix += c
                    self.dictionary[f'{prefix}.{suffix[::-1]}'] = i

    def f(self, prefix: str, suffix: str) -> int:
        return self.dictionary.get(f'{prefix}.{suffix}', -1)


class WordFilter:
    def __init__(self, words: List[str]):  # shortest, laziest, slowest
        master_string = ' '.join(w + '=' + w for w in words[::-1])
        self.f = lambda p, s: master_string.count('=', master_string.find(s + '=' + p)) - 1


class WordFilter:
    def __init__(self, words: List[str]):
        self.dictionary = {}
        for index, word in enumerate(words):
            suffixes = [word[i:] for i in range(len(word)+1)]
            for prefix in [word[:i] for i in range(len(word)+1)]:
                if prefix not in self.dictionary:
                    self.dictionary[prefix] = {}
                for suffix in suffixes:
                    if suffix not in self.dictionary[prefix]:
                        self.dictionary[prefix][suffix] = []
                    self.dictionary[prefix][suffix].append(index)

    def f(self, prefix: str, suffix: str) -> int:
        if prefix not in self.dictionary or suffix not in self.dictionary[prefix]:
            return -1
        return self.dictionary[prefix][suffix][-1]


class WordFilter:
    def __init__(self, words: List[str]):  # fastest and least memory consumption
        self.prefix = {}
        self.suffix = {}
        for word, i in {word: i for i, word in enumerate(words)}.items():  # prevents processing duplicates
            for j in range(len(word) + 1):
                if word[:j] not in self.prefix:
                    self.prefix[word[:j]] = set()
                if word[j:] not in self.suffix:
                    self.suffix[word[j:]] = set()
                self.prefix[word[:j]].add(i)
                self.suffix[word[j:]].add(i)

    def f(self, prefix: str, suffix: str) -> int:
        return max(self.prefix[prefix] & self.suffix[suffix] | {-1})


class WordFilter:

    def __init__(self, words: List[str]):
        self.d = {}
        for i, word in enumerate(words):
            for j in range(len(word)):
                for k in range(len(word) - 1, -1, -1):
                    self.d[f'{word[:j + 1]}:{word[k:]}'] = i

    def f(self, prefix: str, suffix: str) -> int:
        return self.d.get(f'{prefix}:{suffix}', -1)


# Your WordFilter object will be instantiated and called as such:
# obj = WordFilter(words)
# param_1 = obj.f(prefix,suffix)


def test():
    operations = ["WordFilter", "f", "f"]
    arguments = [[['apple', 'banana']], ["a", "e"], ["ba", "a"]]
    expectations = [None, 0, 1]
    obj = WordFilter(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
