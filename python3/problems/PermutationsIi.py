### Source : https://leetcode.com/problems/permutations-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given a collection of numbers, nums, that might contain duplicates, return all possible unique 
# permutations in any order.
# 
# Example 1:
# 
# Input: nums = [1,1,2]
# Output:
# [[1,1,2],
#  [1,2,1],
#  [2,1,1]]
# 
# Example 2:
# 
# Input: nums = [1,2,3]
# Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
# 
# Constraints:
# 
# 	1 <= nums.length <= 8
# 	-10 <= nums[i] <= 10
#####################################################################################################

from typing import List


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:

        def permute(nums: List[int], path: List[int]):
            if not nums:
                paths.add(tuple(path))
            for i, n in enumerate(nums):
                permute(nums[:i] + nums[i + 1:], path + [n])

        paths = set()
        permute(nums, [])
        return list(map(list, paths))

    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        paths = [[]]
        for n in nums:
            tmp = []
            for path in paths:
                for i in range(len(path)+1):
                    tmp.append(path[:i] + [n] + path[i:])
                    if i < len(path) and path[i] == n:
                        break
            paths = tmp
        return paths


def test():
    from collections import Counter
    arguments = [
        [1, 1, 2],
        [1, 2, 3],
        ]
    expectations = [
        [[1, 1, 2], [1, 2, 1], [2, 1, 1]],
        [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().permuteUnique(nums)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected))
