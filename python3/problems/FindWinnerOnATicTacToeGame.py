### Source : https://leetcode.com/problems/find-winner-on-a-tic-tac-toe-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-20

##################################################################################################### 
#
# Tic-tac-toe is played by two players A and B on a 3 x 3 grid.
# 
# Here are the rules of Tic-Tac-Toe:
# 
# 	Players take turns placing characters into empty squares (" ").
# 	The first player A always places "X" characters, while the second player B always places 
# "O" characters.
# 	"X" and "O" characters are always placed into empty squares, never on filled ones.
# 	The game ends when there are 3 of the same (non-empty) character filling any row, column, 
# or diagonal.
# 	The game also ends if all squares are non-empty.
# 	No more moves can be played if the game is over.
# 
# Given an array moves where each element is another array of size 2 corresponding to the row and 
# column of the grid where they mark their respective character in the order in which A and B play.
# 
# Return the winner of the game if it exists (A or B), in case the game ends in a draw return "Draw", 
# if there are still movements to play return "Pending".
# 
# You can assume that moves is valid (It follows the rules of Tic-Tac-Toe), the grid is initially 
# empty and A will play first.
# 
# Example 1:
# 
# Input: moves = [[0,0],[2,0],[1,1],[2,1],[2,2]]
# Output: "A"
# Explanation: "A" wins, he always plays first.
# "X  "    "X  "    "X  "    "X  "    "X  "
# "   " -> "   " -> " X " -> " X " -> " X "
# "   "    "O  "    "O  "    "OO "    "OOX"
# 
# Example 2:
# 
# Input: moves = [[0,0],[1,1],[0,1],[0,2],[1,0],[2,0]]
# Output: "B"
# Explanation: "B" wins.
# "X  "    "X  "    "XX "    "XXO"    "XXO"    "XXO"
# "   " -> " O " -> " O " -> " O " -> "XO " -> "XO " 
# "   "    "   "    "   "    "   "    "   "    "O  "
# 
# Example 3:
# 
# Input: moves = [[0,0],[1,1],[2,0],[1,0],[1,2],[2,1],[0,1],[0,2],[2,2]]
# Output: "Draw"
# Explanation: The game ends in a draw since there are no moves to make.
# "XXO"
# "OOX"
# "XOX"
# 
# Example 4:
# 
# Input: moves = [[0,0],[1,1]]
# Output: "Pending"
# Explanation: The game has not finished yet.
# "X  "
# " O "
# "   "
# 
# Constraints:
# 
# 	1 <= moves.length <= 9
# 	moves[i].length == 2
# 	0 <= moves[i][j] <= 2
# 	There are no repeated elements on moves.
# 	moves follow the rules of tic tac toe.
#####################################################################################################

from typing import List


class Solution:
    def tictactoe(self, moves: List[List[int]]) -> str:

        def check_winner():
            for row in board:
                if row in target:
                    return row[0]
            for col in map(list, zip(*board)):
                if col in target:
                    return col[0]
            if [board[p][p] for p in range(3)] in target or [board[p][~p] for p in range(3)] in target:
                return board[1][1]
            return None

        target = (['A', 'A', 'A'], ['B', 'B', 'B'])
        board = [[None] * 3 for _ in range(3)]
        for i, (x, y) in enumerate(moves):
            board[x][y] = 'B' if i & 1 else 'A'
            winner = check_winner()
            if winner:
                return winner
        return "Pending" if len(moves) < 9 else "Draw"

    def tictactoe(self, moves: List[List[int]]) -> str:
        possible_winner = "A" if len(moves) & 1 else "B"
        rows, cols, diags = [0, 0, 0], [0, 0, 0], [0, 0]
        for i, j in moves[len(moves) & 1 ^ 1::2]:
            rows[i] += 1
            cols[j] += 1
            diags[0] += i == j
            diags[1] += 2 - i == j
        return possible_winner if 3 in rows + cols + diags else "Draw" if len(moves) == 9 else "Pending"

    def tictactoe(self, moves: List[List[int]]) -> str:
        winning = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]
        b = ['.'] * 9
        for i, (x, y) in enumerate(moves):
            b[x * 3 + y] = 'AB'[i & 1]
        return next((b[x] for x, y, z in winning if b[x] == b[y] == b[z] != '.'), ["Pending", "Draw"][len(moves) == 9])


def test():
    arguments = [
        [[0, 0], [2, 0], [1, 1], [2, 1], [2, 2]],
        [[0, 0], [1, 1], [0, 1], [0, 2], [1, 0], [2, 0]],
        [[0, 0], [1, 1], [2, 0], [1, 0], [1, 2], [2, 1], [0, 1], [0, 2], [2, 2]],
        [[0, 0], [1, 1]],
        [[2, 0], [1, 1], [0, 2], [2, 1], [1, 2], [1, 0], [0, 0], [0, 1]],
        [[0, 0], [1, 1], [2, 0], [1, 0], [1, 2], [2, 1], [0, 1], [0, 2], [2, 2]],
    ]
    expectations = ["A", "B", "Draw", "Pending", "B", "Draw"]
    for moves, expected in zip(arguments, expectations):
        solution = Solution().tictactoe(moves)
        assert solution == expected
test()
