### Source : https://leetcode.com/problems/strong-password-checker/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-21

##################################################################################################### 
#
# A password is considered strong if the below conditions are all met:
# 
# 	It has at least 6 characters and at most 20 characters.
# 	It contains at least one lowercase letter, at least one uppercase letter, and at least one 
# digit.
# 	It does not contain three repeating characters in a row (i.e., "...aaa..." is weak, but 
# "...aa...a..." is strong, assuming other conditions are met).
# 
# Given a string password, return the minimum number of steps required to make password strong. if 
# password is already strong, return 0.
# 
# In one step, you can:
# 
# 	Insert one character to password,
# 	Delete one character from password, or
# 	Replace one character of password with another character.
# 
# Example 1:
# Input: password = "a"
# Output: 5
# Example 2:
# Input: password = "aA1"
# Output: 3
# Example 3:
# Input: password = "1337C0d3"
# Output: 0
# 
# Constraints:
# 
# 	1 <= password.length <= 50
# 	password consists of letters, digits, dot '.' or exclamation mark '!'.
#####################################################################################################


class Solution:
    def strongPasswordChecker(self, password: str) -> int:  # Not correct yet
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def backtracking(size: int, state: int, repeats):
            print(size, state, repeats)
            if 6 <= size <= 20 and state == 3 and not repeats:
                return 0
            if not repeats:
                if size > 20:  # simple decrease
                    moves = backtracking(size - 1, state, repeats)
                elif size < 6:  # we can always add a missing state if needed
                    moves = backtracking(size + 1, min(state + 1, 3), repeats)
                else:  # the only thing incorrect is the state, we replace some character
                    moves = backtracking(size, min(state + 1, 3), repeats)
            else:  # trickiest part
                if size < 6:  # repeat could require 1 or 2 inserts to break, can always add a missing state
                    repeat = repeats[-1]
                    new_repeat = (repeat[0] * 3, ) if len(repeats[0]) == 5 else ()
                    moves = backtracking(size + 1, min(state + 1, 3), new_repeat + repeats[:-1])
                elif size > 20:
                    repeat = repeats[-1]
                    new_repeat = (repeat[0] * (len(repeat) - 1), ) if len(repeat) > 3 else ()
                    moves = backtracking(size - 1, state, new_repeat + repeats[:-1])
                else:  # we break and reduce the size of the repeat by replacing, can always add a missing state
                    repeat = repeats[-1]
                    new_repeat = (repeat[0] * (len(repeat) - 3), ) if len(repeat) > 5 else ()
                    moves = backtracking(size, min(state + 1, 3), new_repeat + repeats[:-1])
            return moves + 1

        def analyse_password(string: str):
            lower = upper = digit = False  # 3 states
            repeats = []
            i = 0
            while i < len(string):
                j = i
                while j + 1 < len(string) and string[j] == string[j + 1]:
                    j += 1
                if string[i].islower():
                    lower = True
                elif string[i].isupper():
                    upper = True
                elif string[i].isdigit():
                    digit = True
                repeat_size = j + 1 - i
                if repeat_size > 2:
                    repeats.append(string[j] * repeat_size)
                i = j + 1
            return len(string), lower + upper + digit, tuple(repeats)

        return backtracking(*analyse_password(password))


def test():
    arguments = [
        "a",
        "aA1",
        "1337C0d3",
        "A1234567890aaabbbbccccc",
    ]
    expectations = [5, 3, 0, 4]
    for password, expected in zip(arguments, expectations):
        solution = Solution().strongPasswordChecker(password)
        assert solution == expected, (solution, expected)
test()
