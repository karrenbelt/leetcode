### Source : https://leetcode.com/problems/available-captures-for-rook/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-22

##################################################################################################### 
#
# On an 8 x 8 chessboard, there is exactly one white rook 'R' and some number of white bishops 'B', 
# black pawns 'p', and empty squares '.'.
# 
# When the rook moves, it chooses one of four cardinal directions (north, east, south, or west), then 
# moves in that direction until it chooses to stop, reaches the edge of the board, captures a black 
# pawn, or is blocked by a white bishop. A rook is considered attacking a pawn if the rook can 
# capture the pawn on the rook's turn. The number of available captures for the white rook is the 
# number of pawns that the rook is attacking.
# 
# Return the number of available captures for the white rook.
# 
# Example 1:
# 
# Input: board = 
# [[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","R",".",".",".","p
# "],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".",
# "."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 3
# Explanation: In this example, the rook is attacking all the pawns.
# 
# Example 2:
# 
# Input: board = 
# [[".",".",".",".",".",".",".","."],[".","p","p","p","p","p",".","."],[".","p","p","B","p","p",".",".
# "],[".","p","B","R","B","p",".","."],[".","p","p","B","p","p",".","."],[".","p","p","p","p","p",".",
# "."],[".",".",".",".",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 0
# Explanation: The bishops are blocking the rook from attacking any of the pawns.
# 
# Example 3:
# 
# Input: board = 
# [[".",".",".",".",".",".",".","."],[".",".",".","p",".",".",".","."],[".",".",".","p",".",".",".",".
# "],["p","p",".","R",".","p","B","."],[".",".",".",".",".",".",".","."],[".",".",".","B",".",".",".",
# "."],[".",".",".","p",".",".",".","."],[".",".",".",".",".",".",".","."]]
# Output: 3
# Explanation: The rook is attacking the pawns at positions b5, d6, and f5.
# 
# Constraints:
# 
# 	board.length == 8
# 	board[i].length == 8
# 	board[i][j] is either 'R', '.', 'B', or 'p'
# 	There is exactly one cell with board[i][j] == 'R'
#####################################################################################################

from typing import List


class Solution:
    def numRookCaptures(self, board: List[List[str]]) -> int:

        if any(R := (i, j) for i in range(8) for j in range(8) if board[i][j] == 'R'):
            ctr = 0
            for i in range(R[0] - 1, 0, -1):
                if board[i][R[1]] in 'pB':
                    ctr += board[i][R[1]] == 'p'
                    break
            for i in range(R[0] + 1, 8):
                if board[i][R[1]] in 'pB':
                    ctr += board[i][R[1]] == 'p'
                    break
            for j in range(R[1] - 1, 0, -1):
                if board[R[0]][j] in 'pB':
                    ctr += board[R[0]][j] == 'p'
                    break
            for j in range(R[1] + 1, 8):
                if board[R[0]][j] in 'pB':
                    ctr += board[R[0]][j] == 'p'
                    break
            return ctr
        return 0

    def numRookCaptures(self, B: List[List[str]]) -> int:
        y, x = next((i, j) for j in range(8) for i in range(8) if B[i][j] == 'R')
        row = ''.join(B[y][j] for j in range(8) if B[y][j] != '.')
        col = ''.join(B[i][x] for i in range(8) if B[i][x] != '.')
        return sum(a in b for a in ('Rp', 'pR') for b in (row, col))

    def numRookCaptures(self, board: List[List[str]]) -> int:
        return sum(''.join(r).replace('.', '').count('Rp') for r in board + list(zip(*board)) for r in [r, r[::-1]])


def test():
    arguments = [
        [[".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
         [".", ".", ".", "R", ".", ".", ".", "p"], [".", ".", ".", ".", ".", ".", ".", "."],
         [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
         [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]],

        [[".", ".", ".", ".", ".", ".", ".", "."], [".", "p", "p", "p", "p", "p", ".", "."],
         [".", "p", "p", "B", "p", "p", ".", "."], [".", "p", "B", "R", "B", "p", ".", "."],
         [".", "p", "p", "B", "p", "p", ".", "."], [".", "p", "p", "p", "p", "p", ".", "."],
         [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]],

        [[".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "p", ".", ".", ".", "."],
         [".", ".", ".", "p", ".", ".", ".", "."], ["p", "p", ".", "R", ".", "p", "B", "."],
         [".", ".", ".", ".", ".", ".", ".", "."], [".", ".", ".", "B", ".", ".", ".", "."],
         [".", ".", ".", "p", ".", ".", ".", "."], [".", ".", ".", ".", ".", ".", ".", "."]],
    ]
    expectations = [3, 0, 3]
    for board, expected in zip(arguments, expectations):
        solution = Solution().numRookCaptures(board)
        assert solution == expected, solution
