### Source : https://leetcode.com/problems/range-module/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

##################################################################################################### 
#
# A Range odule is a module that tracks ranges of numbers. Design a data structure to track the 
# ranges represented as half-open intervals and query about them.
# 
# A half-open interval [left, right) denotes all the real numbers x where left <= x < right.
# 
# Implement the Rangeodule class:
# 
# 	Rangeodule() Initializes the object of the data structure.
# 	void addRange(int left, int right) Adds the half-open interval [left, right), tracking 
# every real number in that interval. Adding an interval that partially overlaps with currently 
# tracked numbers should add any numbers in the interval [left, right) that are not already tracked.
# 
# 	boolean queryRange(int left, int right) Returns true if every real number in the interval 
# [left, right) is currently being tracked, and false otherwise.
# 
# 	void removeRange(int left, int right) Stops tracking every real number currently being 
# tracked in the half-open interval [left, right).
# 
# Example 1:
# 
# Input
# ["Rangeodule", "addRange", "removeRange", "queryRange", "queryRange", "queryRange"]
# [[], [10, 20], [14, 16], [10, 14], [13, 15], [16, 17]]
# Output
# [null, null, null, true, false, true]
# 
# Explanation
# Rangeodule rangeodule = new Rangeodule();
# rangeodule.addRange(10, 20);
# rangeodule.removeRange(14, 16);
# rangeodule.queryRange(10, 14); // return True,(Every number in [10, 14) is being tracked)
# rangeodule.queryRange(13, 15); // return False,(Numbers like 14, 14.03, 14.17 in [13, 15) are not 
# being tracked)
# rangeodule.queryRange(16, 17); // return True, (The number 16 in [16, 17) is still being tracked, 
# despite the remove operation)
# 
# Constraints:
# 
# 	1 <= left < right <= 109
# 	At most 104 calls will be made to addRange, queryRange, and removeRange.
#####################################################################################################

import bisect


class RangeModule:
    # important insight: ranges can be flattened: uneven marks the start even the end. We then use the bisect module
    def __init__(self):
        self.ranges = []

    def addRange(self, left: int, right: int) -> None:
        # we find the position, and merge with overlapping ranges
        i = bisect.bisect_left(self.ranges, left)
        j = bisect.bisect_right(self.ranges, right)
        domain = [left, right] if not i % 2 and not j % 2 else [left] if not i % 2 else [right] if not j % 2 else []
        self.ranges[i:j] = domain

    def queryRange(self, left: int, right: int) -> bool:
        # NOTE: bisect.right on left and bisect.left on right!
        i = bisect.bisect_right(self.ranges, left)
        j = bisect.bisect_left(self.ranges, right)
        return i == j and i % 2

    def removeRange(self, left: int, right: int) -> None:
        # find the overlapping ranges and partly (remove them)
        i = bisect.bisect_left(self.ranges, left)
        j = bisect.bisect_right(self.ranges, right)
        domain = [left, right] if i % 2 and j % 2 else [left] if i % 2 else [right] if j % 2 else []
        self.ranges[i:j] = domain

# Your RangeModule object will be instantiated and called as such:
# obj = RangeModule()
# obj.addRange(left,right)
# param_2 = obj.queryRange(left,right)
# obj.removeRange(left,right)


def test():
    operations = ["RangeModule", "addRange", "removeRange", "queryRange", "queryRange", "queryRange"]
    arguments = [[], [10, 20], [14, 16], [10, 14], [13, 15], [16, 17]]
    expectations = [None, None, None, True, False, True]
    obj = RangeModule(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
