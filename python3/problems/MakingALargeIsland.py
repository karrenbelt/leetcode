### Source : https://leetcode.com/problems/making-a-large-island/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# You are given an n x n binary matrix grid. You are allowed to change at most one 0 to be 1.
# 
# Return the size of the largest island in grid after applying this operation.
# 
# An island is a 4-directionally connected group of 1s.
# 
# Example 1:
# 
# Input: grid = [[1,0],[0,1]]
# Output: 3
# Explanation: Change one 0 to 1 and connect two 1s, then we get an island with area = 3.
# 
# Example 2:
# 
# Input: grid = [[1,1],[1,0]]
# Output: 4
# Explanation: Change the 0 to 1 and make the island bigger, only one island with area = 4.
# 
# Example 3:
# 
# Input: grid = [[1,1],[1,1]]
# Output: 4
# Explanation: Can't change any 0 to 1, only one island with area = 4.
# 
# Constraints:
# 
# 	n == grid.length
# 	n == grid[i].length
# 	1 <= n <= 500
# 	grid[i][j] is either 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def largestIsland(self, grid: List[List[int]]) -> int:  # O(mn) time and O(mn) space
        # imagine a reverse swastika, as below. Requires one flip (middle) to connect 4 islands
        # x x x . . x
        # . . x . . x
        # x x . x x x
        # x . x . . .
        # x . x x x x
        # 1. we find all islands and label them, also noting their size.
        # 2. Then we find the cell of water whose direct neighbors link to the largest sum of islands

        def dfs(x: int, y: int, cells: set):
            if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and grid[x][y] and (x, y) not in cells:
                cells.add((x, y))
                for nx, ny in [(x, y + 1), (x + 1, y), (x, y - 1), (x - 1, y)]:
                    dfs(nx, ny, cells)
            return cells

        islands = []
        seen = set()
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] and (i, j) not in seen:
                    island = dfs(i, j, set())
                    islands.append(island)
                    seen.update(island)

        labels = {(i, j): n for n, island in enumerate(islands) for i, j in island}
        sizes = {n: len(island) for n, island in enumerate(islands)}

        largest = max(sizes.values(), default=0) + any(0 in row for row in grid)
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if not grid[i][j]:
                    neighbors = set()
                    for ni, nj in [(i, j + 1), (i + 1, j), (i, j - 1), (i - 1, j)]:
                        if (ni, nj) in labels:
                            neighbors.add(labels.get((ni, nj)))
                    size = sum(sizes[island_id] for island_id in neighbors)
                    largest = max(largest, size + 1)
        return largest


def test():
    arguments = [
        [[1, 0], [0, 1]],
        [[1, 1], [1, 0]],
        [[1, 1], [1, 1]],
        [[0, 0], [0, 0]],

        [[1, 1, 1, 0, 1],
         [0, 0, 1, 0, 1],
         [1, 1, 0, 1, 1],
         [1, 0, 1, 0, 0],
         [1, 0, 1, 1, 1]],
    ]
    expectations = [3, 4, 4, 1, 17]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().largestIsland(grid)
        assert solution == expected
