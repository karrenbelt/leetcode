### Source : https://leetcode.com/problems/pascals-triangle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given an integer numRows, return the first numRows of Pascal's triangle.
# 
# In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:
# 
# Example 1:
# Input: numRows = 5
# Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
# Example 2:
# Input: numRows = 1
# Output: [[1]]
# 
# Constraints:
# 
# 	1 <= numRows <= 30
#####################################################################################################

from typing import List


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:  # dp

        # base cases
        triangle = [[] for _ in range(numRows)]
        if numRows == 0:
            return triangle
        triangle[0] = [1]
        if numRows == 1:
            return triangle
        triangle[1] = [1, 1]
        if numRows == 2:
            return triangle

        for i in range(2, numRows):
            prev = triangle[i-1]
            row = [1] + [sum(prev[j-1:j+1]) for j in range(1, len(prev))] + [1]
            triangle[i].extend(row)

        return triangle

    def generate(self, numRows: int) -> List[List[int]]:
        rows = [[1]]
        for _ in range(1, numRows):
            row = rows[-1]
            rows.append([1] + [row[i] + row[i + 1] for i in range(len(row) - 1)] + [1])
        return rows

    def generate(self, numRows: int) -> List[List[int]]:
        return rows if ((rows := [[1]]) and [rows.append([1] + [rows[-1][i] + rows[-1][i + 1] for i in range(len(rows[-1]) - 1)] + [1]) for _ in range(1, numRows)]) else rows

    def generate(self, numRows: int) -> List[List[int]]:
        from math import factorial
        return [[factorial(n) // (factorial(k) * factorial(n - k)) for k in range(n + 1)] for n in range(numRows)]


def test():
    numbers = [5, 1]
    expectations = [
        [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1]],
        [[1]],
        ]
    for numRows, expected in zip(numbers, expectations):
        solution = Solution().generate(numRows)
        assert solution == expected, solution
