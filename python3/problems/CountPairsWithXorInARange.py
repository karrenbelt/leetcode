### Source : https://leetcode.com/problems/count-pairs-with-xor-in-a-range/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-16

##################################################################################################### 
#
# Given a (0-indexed) integer array nums and two integers low and high, return the number of nice 
# pairs.
# 
# A nice pair is a pair (i, j) where 0 <= i < j < nums.length and low <= (nums[i] XOR nums[j]) <= 
# high.
# 
# Example 1:
# 
# Input: nums = [1,4,2,7], low = 2, high = 6
# Output: 6
# Explanation: All nice pairs (i, j) are as follows:
#     - (0, 1): nums[0] XOR nums[1] = 5 
#     - (0, 2): nums[0] XOR nums[2] = 3
#     - (0, 3): nums[0] XOR nums[3] = 6
#     - (1, 2): nums[1] XOR nums[2] = 6
#     - (1, 3): nums[1] XOR nums[3] = 3
#     - (2, 3): nums[2] XOR nums[3] = 5
# 
# Example 2:
# 
# Input: nums = [9,8,4,2,1], low = 5, high = 14
# Output: 8
# Explanation: All nice pairs (i, j) are as follows:
# -b-@-K-b-@-K-b-@-K-b-@-K-b-@-K    - (0, 2): nums[0] XOR nums[2] = 13
#     - (0, 3): nums[0] XOR nums[3] = 11
#     - (0, 4): nums[0] XOR nums[4] = 8
#     - (1, 2): nums[1] XOR nums[2] = 12
#     - (1, 3): nums[1] XOR nums[3] = 10
#     - (1, 4): nums[1] XOR nums[4] = 9
#     - (2, 3): nums[2] XOR nums[3] = 6
#     - (2, 4): nums[2] XOR nums[4] = 5
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	1 <= nums[i] <= 2 * 104
# 	1 <= low <= high <= 2 * 104
#####################################################################################################

from typing import List


class Solution:
    def countPairs(self, nums: List[int], low: int, high: int) -> int:  # O(n^2) -> TLE: 39 / 63 test cases passed.
        return sum(low <= nums[i] ^ nums[j] <= high for i in range(len(nums)) for j in range(i + 1, len(nums)))

    def countPairs(self, nums: List[int], low: int, high: int) -> int:  # O(n) time
        from collections import Counter
        counts = Counter(nums)  # returns 0 when key is missing
        high += 1
        ctr = 0
        while high:
            ctr += sum(counts[a] * counts[high - 1 ^ a] for a in counts) if high & 1 else 0
            ctr -= sum(counts[a] * counts[low - 1 ^ a] for a in counts) if low & 1 else 0
            counts = Counter({a >> 1: counts[a] + counts[a ^ 1] for a in counts})
            high >>= 1
            low >>= 1
        return ctr // 2


def test():
    arguments = [
        ([1, 4, 2, 7], 2, 6),
        ([9, 8, 4, 2, 1], 5, 14),
        ]
    expectations = [6, 8]
    for (nums, low, high), expected in zip(arguments, expectations):
        solutions = Solution().countPairs(nums, low, high)
        assert solutions == expected
