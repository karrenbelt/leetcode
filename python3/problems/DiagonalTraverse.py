### Source : https://leetcode.com/problems/diagonal-traverse/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-20

##################################################################################################### 
#
# Given a matrix of  x N elements ( rows, N columns), return all elements of the matrix in diagonal 
# order as shown in the below image.
# 
# Example:
# 
# Input:
# [
#  [ 1, 2, 3 ],
#  [ 4, 5, 6 ],
#  [ 7, 8, 9 ]
# ]
# 
# Output:  [1,2,4,7,5,3,6,8,9]
# 
# Explanation:
# 
# Note:
# 
# The total number of elements of the given matrix will not exceed 10,000.
#####################################################################################################

from typing import List


class Solution:
    def findDiagonalOrder(self, matrix: List[List[int]]) -> List[int]:  # O(n) time and O(n) space
        # sum of indices for each element on the same diagonal is the same
        if not matrix or not matrix[0]:
            return []
        forward = [[] for _ in range(len(matrix) + len(matrix[0]) - 1)]
        for r in range(len(matrix)):
            for c in range(len(matrix[0])):
                forward[r + c].append(matrix[r][c])
        return [y for i, x in enumerate(forward) for y in x[::[-1, 1][i % 2]]]


def test():
    arguments = [
        [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]],
        ]
    expectations = [
        [1, 2, 4, 7, 5, 3, 6, 8, 9]
        ]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().findDiagonalOrder(matrix)
        assert solution == expected
