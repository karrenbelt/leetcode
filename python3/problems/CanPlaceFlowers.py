### Source : https://leetcode.com/problems/can-place-flowers/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Suppose you have a long flowerbed in which some of the plots are planted and some are not. However, 
# flowers cannot be planted in adjacent plots - they would compete for water and both would die.
# 
# Given a flowerbed (represented as an array containing 0 and 1, where 0 means empty and 1 means not 
# empty), and a number n, return if n new flowers can be planted in it without violating the 
# no-adjacent-flowers rule.
# 
# Example 1:
# 
# Input: flowerbed = [1,0,0,0,1], n = 1
# Output: True
# 
# Example 2:
# 
# Input: flowerbed = [1,0,0,0,1], n = 2
# Output: False
# 
# Note:
# 
# The input array won't violate no-adjacent-flowers rule.
# The input array size is in the range of [1, 20000].
# n is a non-negative integer which won't exceed the input array size.
# 
#####################################################################################################

from typing import List


class Solution:
    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:  # O(n) time and O(1) space

        # corner cases
        if n == 0:
            return True
        if len(flowerbed) <= 2 and flowerbed.count(1) != 0:
            return False

        # now count consecutive zeroes
        c = 1  # start of sequence corner case
        for f in flowerbed:
            if f == 0:
                c += 1
            if f == 1:
                n -= (c - 1) // 2
                c = 0
        else:  # and the end-of sequence corner case
            n -= c // 2

        return n <= 0

    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:  # O(n) time and O(1) space
        x = 0
        flowerbed.append(0)
        for i in range(1, len(flowerbed)-1):
            if flowerbed[i] == 1:
                continue
            if not flowerbed[i - 1] + flowerbed[i + 1]:
                flowerbed[i] = 1
                x += 1
        return x >= n


def test():
    arguments = [
        ([1, 0, 0, 0, 1], 1),
        ([1, 0, 0, 0, 1], 2),
        ]
    expectations = [True, False]
    for (flowerbed, n), expected in zip(arguments, expectations):
        solution = Solution().canPlaceFlowers(flowerbed, n)
        assert solution == expected
