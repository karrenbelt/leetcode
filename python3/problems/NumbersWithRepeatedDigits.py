### Source : https://leetcode.com/problems/numbers-with-repeated-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-18

##################################################################################################### 
#
# Given a positive integer n, return the number of positive integers less than or equal to n that 
# have at least 1 repeated digit.
# 
# Example 1:
# 
# Input: n = 20
# Output: 1
# Explanation: The only positive number (<= 20) with at least 1 repeated digit is 11.
# 
# Example 2:
# 
# Input: n = 100
# Output: 10
# Explanation: The positive numbers (<= 100) with atleast 1 repeated digit are 11, 22, 33, 44, 55, 
# 66, 77, 88, 99, and 100.
# 
# Example 3:
# 
# Input: n = 1000
# Output: 262
# 
# Note:
# 
# 	1 <= n <= 109
#####################################################################################################


class Solution:
    def numDupDigitsAtMostN(self, n: int) -> int:  # O(log n) time
        # n = 10   -> 10 - 9 - 1 = 0
        # n = 100  -> 100 - 9 - 81 = 10
        # n = 1000 -> 1000 - 9 - 81 - 648 = 282
        # this is computed using n_digit_no_repeat

        def has_repeated(digit: int) -> bool:
            return len(set(str(digit))) != len(str(digit))

        def permutation(n: int, k: int) -> int:  # the number of permutations: n! / (n - r)!
            prod = 1
            for i in range(k):
                prod *= (n - i)
            return prod

        def n_digit_no_repeat(n_digits: int) -> int:  # number of repeats in numbers of n digits
            return 9 if n_digits == 1 else 9 * permutation(9, n_digits - 1)

        digits, result, prefix = list(map(int, str(n))), n - 1, 0
        for i in range(1, len(digits)):
            result -= n_digit_no_repeat(i)
        for i in range(len(digits)):
            start = 0 if i else 1  # cannot be zero for the most significant digit
            for j in range(start, digits[i]):
                if has_repeated(prefix * 10 + j):
                    continue  # 1 in line below == has_repeated(prefix * 10 + j)
                result -= 1 if i == len(digits) - 1 else permutation(10 - i - 1, len(digits) - 1 - i)
            prefix = prefix * 10 + digits[i]
        return result + has_repeated(n)


def test():
    arguments = [20, 100, 1000, 2222]
    expectations = [1, 10, 262, 868]
    for n, expected in zip(arguments, expectations):
        solution = Solution().numDupDigitsAtMostN(n)
        assert solution == expected
