### Source : https://leetcode.com/problems/as-far-from-land-as-possible/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-07

##################################################################################################### 
#
# Given an n x n grid containing only values 0 and 1, where 0 represents water and 1 represents land, 
# find a water cell such that its distance to the nearest land cell is maximized, and return the 
# distance. If no land or water exists in the grid, return -1.
# 
# The distance used in this problem is the anhattan distance: the distance between two cells (x0, 
# y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
# 
# Example 1:
# 
# Input: grid = [[1,0,1],[0,0,0],[1,0,1]]
# Output: 2
# Explanation: The cell (1, 1) is as far as possible from all the land with distance 2.
# 
# Example 2:
# 
# Input: grid = [[1,0,0],[0,0,0],[0,0,0]]
# Output: 4
# Explanation: The cell (2, 2) is as far as possible from all the land with distance 4.
# 
# Constraints:
# 
# 	n == grid.length
# 	n == grid[i].length
# 	1 <= n <= 100
# 	grid[i][j] is 0 or 1
####################################################################################################

from typing import List, Iterable, Generator


class Solution:
    def maxDistance(self, grid: List[List[int]]) -> int:  # O(n) time and O(n) space
        level = 0
        queue = [(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j]]
        while queue:
            new_queue = []
            for i, j in queue:
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and not grid[x][y]:
                        new_queue.append((x, y))
                        grid[x][y] = 1
            queue, level = new_queue, level + bool(new_queue)
        return level or -1

    def maxDistance(self, grid: List[List[int]]) -> int:
        from operator import add, sub
        from functools import reduce, partial, wraps
        from itertools import product, starmap, permutations, chain
        from collections import deque

        def fmap(*funcs: callable, x):
            return reduce(lambda fx, f: f(fx), funcs, x)

        def matrix_indices(m: List[List]) -> Iterable[tuple[int, int]]:
            return product(*map(lambda x: fmap(len, range, x=x), (m, m[0])))

        def star_filter(condition: callable, iterable: Iterable):
            return (args for args in iterable if condition(*args))

        def apply(f: callable, x):
            return f(x)

        def move(x: int, y: int) -> Generator[None, None, tuple[int, int]]:
            m = map(lambda f: permutations((f, lambda _: _), 2), (add1, sub1))
            return (tuple(map(apply, f, (x, y))) for f in chain(*m))

        def in_bound(x: int, y: int) -> bool:
            return 0 <= x < len(grid) and 0 <= y < len(grid[0])

        def empty(x: int, y: int) -> bool:
            return not grid[x][y]

        def mutate_grid(x: int, y: int):
            grid[x][y] = 1

        def consume(iterable: Iterable) -> None:
            deque(iterable, maxlen=0)

        def flip(func: callable) -> callable:
            @wraps(func)
            def wrapped(*args):
                return func(*reversed(args))
            return wrapped

        level, add1, sub1 = 0, partial(flip(add), 1), partial(flip(sub), 1)
        stack = set(star_filter(lambda i, j: grid[i][j], matrix_indices(grid)))
        filters = partial(star_filter, in_bound), partial(star_filter, empty)

        while stack:
            new_stack, next_coords = set(), starmap(move, stack)
            for nodes in map(lambda x: set(fmap(*filters, x=x)), next_coords):
                consume(starmap(mutate_grid, nodes)) or new_stack.update(nodes)
            stack, level = new_stack, level + bool(new_stack)
        return level or -1


def test():
    arguments = [
        [
            [1, 0, 1],
            [0, 0, 0],
            [1, 0, 1],
        ],
        [
            [1, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ],
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [0, 0, 1, 1, 1],
            [0, 1, 1, 0, 0],
            [0, 0, 1, 1, 0],
            [1, 0, 0, 0, 0],
            [1, 1, 0, 0, 1],
        ],
        [
            [1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 1, 0, 1, 1, 1, 0, 1, 1, 0],
            [0, 1, 1, 0, 1, 0, 0, 1, 0, 0],
            [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 1, 1, 0, 1, 1],
            [0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
            [0, 0, 0, 1, 1, 1, 1, 0, 0, 1],
            [0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
            [1, 1, 0, 1, 1, 1, 1, 1, 0, 0],
        ]
    ]
    expectations = [2, 4, -1, 2, 2]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().maxDistance(grid)
        assert solution == expected, (solution, expected)
