### Source : https://leetcode.com/problems/minimum-value-to-get-positive-step-by-step-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-11

##################################################################################################### 
#
# Given an array of integers nums, you start with an initial positive value startValue.
# 
# In each iteration, you calculate the step by step sum of startValue plus elements in nums (from 
# left to right).
# 
# Return the minimum positive value of startValue such that the step by step sum is never less than 1.
# 
# Example 1:
# 
# Input: nums = [-3,2,-3,4,2]
# Output: 5
# Explanation: If you choose startValue = 4, in the third iteration your step by step sum is less 
# than 1.
#                 step by step sum
#                 startValue = 4 | startValue = 5 | nums
#                   (4 -3 ) = 1  | (5 -3 ) = 2    |  -3
#                   (1 +2 ) = 3  | (2 +2 ) = 4    |   2
#                   (3 -3 ) = 0  | (4 -3 ) = 1    |  -3
#                   (0 +4 ) = 4  | (1 +4 ) = 5    |   4
#                   (4 +2 ) = 6  | (5 +2 ) = 7    |   2
# 
# Example 2:
# 
# Input: nums = [1,2]
# Output: 1
# Explanation: inimum start value should be positive. 
# 
# Example 3:
# 
# Input: nums = [1,-2,-3]
# Output: 5
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	-100 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def minStartValue(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        for i in range(1, len(nums)):
            nums[i] += nums[i - 1]
        return max(-min(nums) + 1, 1)

    def minStartValue(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        for i in range(1, len(nums)):
            nums[i] += nums[i - 1]
        return max(1, 1 - min(nums))

    def minStartValue(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        prefix_sum = minimum = 0
        for n in nums:
            prefix_sum += n
            minimum = min(minimum, prefix_sum)
        return -minimum + 1

    def minStartValue(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        return (prefix := 0) or max(-min(prefix := prefix + n for n in nums) + 1, 1)

    def minStartValue(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        from itertools import accumulate
        return max(-min(accumulate(nums)) + 1, 1)


def test():
    arguments = [
        [-3, 2, -3, 4, 2],
        [1, 2],
        [1, -2, -3],
    ]
    expectations = [5, 1, 5]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minStartValue(nums)
        assert solution == expected
