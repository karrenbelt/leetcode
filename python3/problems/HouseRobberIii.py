### Source : https://leetcode.com/problems/house-robber-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# The thief has found himself a new place for his thievery again. There is only one entrance to this 
# area, called root.
# 
# Besides the root, each house has one and only one parent house. After a tour, the smart thief 
# realized that all houses in this place form a binary tree. It will automatically contact the police 
# if two directly-linked houses were broken into on the same night.
# 
# Given the root of the binary tree, return the maximum amount of money the thief can rob without 
# alerting the police.
# 
# Example 1:
# 
# Input: root = [3,2,3,null,3,null,1]
# Output: 7
# Explanation: aximum amount of money the thief can rob = 3 + 3 + 1 = 7.
# 
# Example 2:
# 
# Input: root = [3,4,5,1,3,null,1]
# Output: 9
# Explanation: aximum amount of money the thief can rob = 4 + 5 = 9.
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	0 <= Node.val <= 104
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def rob(self, root: TreeNode) -> int:  # O(n) time

        def traverse(node: TreeNode) -> tuple:
            if not node:
                return 0, 0
            left, right = traverse(node.left), traverse(node.right)
            now = node.val + left[1] + right[1]
            later = max(left) + max(right)
            return now, later

        return max(traverse(root))


def test():
    from python3 import Codec
    arguments = [
        "[3,2,3,null,3,null,1]",
        "[3,4,5,1,3,null,1]",
        ]
    expectations = [7, 9]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().rob(root)
        assert solution == expected
