### Source : https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given the head of a singly linked list where elements are sorted in ascending order, convert it to 
# a height balanced BST.
# 
# For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of 
# the two subtrees of every node never differ by more than 1.
# 
# Example 1:
# 
# Input: head = [-10,-3,0,5,9]
# Output: [0,-3,9,-10,null,5]
# Explanation: One possible answer is [0,-3,9,-10,null,5], which represents the shown height balanced 
# BST.
# 
# Example 2:
# 
# Input: head = []
# Output: []
# 
# Example 3:
# 
# Input: head = [0]
# Output: [0]
# 
# Example 4:
# 
# Input: head = [1,3]
# Output: [3,1]
# 
# Constraints:
# 
# 	The number of nodes in head is in the range [0, 2 * 104].
# 	-105 <= Node.val <= 105
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def sortedListToBST(self, head: ListNode) -> TreeNode:  # O(n) time and O(n) space

        nums = []
        while head:
            nums.append(head.val)
            head = head.next

        def build_bst(left: int, right: int):
            if left > right:
                return
            mid = left + (right - left) // 2
            node = TreeNode(nums[mid])
            node.left = build_bst(left, mid - 1)
            node.right = build_bst(mid + 1, right)
            return node

        return build_bst(0, len(nums) - 1)

    def sortedListToBST(self, head: ListNode) -> TreeNode:  # O(n) time and O(1) space
        if head is None:
            return
        if not head.next:
            return TreeNode(head.val)

        # find the middle of the list
        slow, fast = head, head.next.next  # cannot set to the same, need one before middle
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next

        # divide and conquer
        tmp = slow.next
        slow.next = None
        root = TreeNode(tmp.val)
        root.left = self.sortedListToBST(head)
        root.right = self.sortedListToBST(tmp.next)
        return root


def test():
    from python3 import SinglyLinkedList
    from python3 import Codec
    arguments = [
        [-10, -3, 0, 5, 9],
        [1, 3],
        ]
    expectations = [
        ("[0,-3,9,-10,null,5]", "[0,-10,5,null,-3,null,9]"),
        ("[3,1]", "[1,null,3]"),
        ]
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().sortedListToBST(head)
        assert solution in set(map(Codec.deserialize, expected))
