### Source : https://leetcode.com/problems/range-sum-query-mutable/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-13

##################################################################################################### 
#
# Given an integer array nums, handle multiple queries of the following types:
# 
# 	Update the value of an element in nums.
# 	Calculate the sum of the elements of nums between indices left and right inclusive where 
# left <= right.
# 
# Implement the NumArray class:
# 
# 	NumArray(int[] nums) Initializes the object with the integer array nums.
# 	void update(int index, int val) Updates the value of nums[index] to be val.
# 	int sumRange(int left, int right) Returns the sum of the elements of nums between indices 
# left and right inclusive (i.e. nums[left] + nums[left + 1] + ... + nums[right]).
# 
# Example 1:
# 
# Input
# ["NumArray", "sumRange", "update", "sumRange"]
# [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
# Output
# [null, 9, null, 8]
# 
# Explanation
# NumArray numArray = new NumArray([1, 3, 5]);
# numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
# numArray.update(1, 2);   // nums = [1, 2, 5]
# numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8
# 
# Constraints:
# 
# 	1 <= nums.length <= 3 * 104
# 	-100 <= nums[i] <= 100
# 	0 <= index < nums.length
# 	-100 <= val <= 100
# 	0 <= left <= right < nums.length
# 	At most 3 * 104 calls will be made to update and sumRange.
#####################################################################################################

from typing import List


class NumArray(object):
    def __init__(self, nums):
        self.update = nums.__setitem__
        self.sumRange = lambda i, j: sum(nums[i:j+1])  # O(n) time


class NumArray:
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.n = int(len(nums) ** 0.5) + 1
        self.row_sums = [sum(nums[i * self.n: i * self.n + self.n]) for i in range(self.n)]

    def update(self, index: int, val: int) -> None:
        self.row_sums[index // self.n] += val - self.nums[index]
        self.nums[index] = val

    def sumRange(self, left: int, right: int) -> int:  # sqrt(n) time
        # return sum(self.nums[left:right + 1])  # TLE
        row_i, row_j = left // self.n, right // self.n
        return (sum(self.nums[left:right + 1])
                if row_i == row_j else
                sum((sum(self.nums[left: (row_i + 1) * self.n]),
                     sum(self.row_sums[row_i + 1: row_j]),
                     sum(self.nums[row_j * self.n: right + 1]))))


class NumArray:  # binary indexed tree
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.tree = [0] * (len(self) + 1)
        for j in range(1, len(self) + 1):
            self.tree[j] += self.nums[j - 1]
            if (j + (j & (-j))) <= len(self):
                self.tree[j + (j & (-j))] += self.tree[j]

    def update(self, index: int, val: int) -> None:
        diff = val - self.nums[index]
        self.nums[index] = val
        index += 1
        while index <= len(self):
            self.tree[index] += diff
            index += (index & (-index))

    def sumRange(self, left: int, right: int) -> int:
        return self.getSum(right) - self.getSum(left - 1)

    def getSum(self, i: int) -> int:
        sm = 0
        i += 1
        while i > 0:
            sm += self.tree[i]
            i -= (i & (-i))
        return sm

    def __len__(self) -> int:
        return len(self.nums)

# Your NumArray object will be instantiated and called as such:
# obj = NumArray(nums)
# obj.update(index,val)
# param_2 = obj.sumRange(left,right)


def test():
    operations = ["NumArray", "sumRange", "update", "sumRange"]
    arguments = [[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
    expectations = [None, 9, None, 8]

    # operations = ["NumArray", "update", "sumRange", "sumRange", "update", "sumRange"]
    # arguments = [[[9, -8]], [0, 3], [1, 1], [0, 1], [1, -3], [0, 1]]
    # expectations = [None, None, -8, -5, None, 0]

    operations = ["NumArray", "sumRange", "update", "sumRange", "sumRange", "update", "update", "sumRange", "sumRange",
                  "update", "update"]
    arguments = [[[-28, -39, 53, 65, 11, -56, -65, -39, -43, 97]],
                 [5, 6], [9, 27], [2, 3], [6, 7], [1, -82], [3, -72], [3, 7], [1, 8], [5, 13], [4, -67]]
    expectations = [None, -121, None, 118, -104, None, None, -221, -293, None, None]

    obj = NumArray(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
test()