### Source : https://leetcode.com/problems/fruit-into-baskets/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# In a row of trees, the i-th tree produces fruit with type tree[i].
# 
# You start at any tree of your choice, then repeatedly perform the following steps:
# 
# 	Add one piece of fruit from this tree to your baskets.  If you cannot, stop.
# 	ove to the next tree to the right of the current tree.  If there is no tree to the right, 
# stop.
# 
# Note that you do not have any choice after the initial choice of starting tree: you must perform 
# step 1, then step 2, then back to step 1, then step 2, and so on until you stop.
# 
# You have two baskets, and each basket can carry any quantity of fruit, but you want each basket to 
# only carry one type of fruit each.
# 
# What is the total amount of fruit you can collect with this procedure?
# 
# Example 1:
# 
# Input: [1,2,1]
# Output: 3
# Explanation: We can collect [1,2,1].
# 
# Example 2:
# 
# Input: [0,1,2,2]
# Output: 3
# Explanation: We can collect [1,2,2].
# If we started at the first tree, we would only collect [0, 1].
# 
# Example 3:
# 
# Input: [1,2,3,2,2]
# Output: 4
# Explanation: We can collect [2,3,2,2].
# If we started at the first tree, we would only collect [1, 2].
# 
# Example 4:
# 
# Input: [3,3,3,1,2,1,1,2,3,3,4]
# Output: 5
# Explanation: We can collect [1,2,1,1,2].
# If we started at the first tree or the eighth tree, we would only collect 4 fruits.
# 
# Note:
# 
# 	1 <= tree.length <= 40000
# 	0 <= tree[i] < tree.length
# 
#####################################################################################################

from typing import List


class Solution:
    def totalFruit(self, tree: List[int]) -> int:  # O(n) time and O(1) space
        baskets = [[None, 0], [None, 0]]
        maximum = 0
        for fruit in tree:
            if fruit == baskets[1][0]:
                baskets[1][1] += 1
            else:
                baskets.reverse()
                if fruit == baskets[1][0]:
                    baskets[0][1] += baskets[1][1]
                else:
                    maximum = max(maximum, baskets[0][1] + baskets[1][1])
                baskets[1] = [fruit, 1]
        return max(maximum, baskets[0][1] + baskets[1][1])

    def totalFruit(self, tree: List[int]) -> int:  # sliding window: O(n) time
        baskets = {}
        i = j = 0
        for j, fruit in enumerate(tree):
            baskets[fruit] = baskets.get(fruit, 0) + 1
            if len(baskets) > 2:
                baskets[tree[i]] -= 1
                if baskets[tree[i]] == 0:
                    baskets.pop(tree[i])
                i += 1
        return j - i + 1

    def totalFruit(self, tree: List[int]) -> int:  # sliding window: O(n) time
        i = j = m = 0
        x, y = tree[0], -1
        while j < len(tree):
            if tree[j] != x and y == -1:
                y = tree[j]
            elif tree[j] != x and tree[j] != y:
                x, y = tree[j - 1], tree[j]
                m = max(m, j - i)
                i = j - 1
                while tree[i - 1] == x:
                    i -= 1
            j += 1
        return max(m, j - i)


def test():
    arguments = [
        [1, 2, 1],
        [0, 1, 2, 2],
        [1, 2, 3, 2, 2],
        [3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4],
        [0],
        [0, 0, 1, 1],
    ]
    expectations = [3, 3, 4, 5, 1, 4]
    for tree, expected in zip(arguments, expectations):
        solution = Solution().totalFruit(tree)
        assert solution == expected
