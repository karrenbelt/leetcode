### Source : https://leetcode.com/problems/minimum-increment-to-make-array-unique/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# Given an array of integers A, a move consists of choosing any A[i], and incrementing it by 1.
# 
# Return the least number of moves to make every value in A unique.
# 
# Example 1:
# 
# Input: [1,2,2]
# Output: 1
# Explanation:  After 1 move, the array could be [1, 2, 3].
# 
# Example 2:
# 
# Input: [3,2,1,2,1,7]
# Output: 6
# Explanation:  After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
# It can be shown with 5 or less moves that it is impossible for the array to have all unique values.
# 
# Note:
# 
# 	0 <= A.length <= 40000
# 	0 <= A[i] < 40000
# 
#####################################################################################################

from typing import List


class Solution:
    def minIncrementForUnique(self, A: List[int]) -> int:  # TLE
        ctr = 0
        seen = set()
        for num in A:
            while num in seen:  # check the entire set every time
                num += 1
                ctr += 1
            seen.add(num)
        return ctr

    def minIncrementForUnique(self, A: List[int]) -> int:  # O(n log n) for sorting, O(1) space
        ctr = need = 0
        A.sort()
        for n in A:
            ctr += max(need - n, 0)
            need = max(need + 1, n + 1)  # compared to previous, needs to be at least + 1
        return ctr

    def minIncrementForUnique(self, A: List[int]) -> int:  # union-find: O(n)?? time and O(n) space

        def find(x):
            seen[x] = find(seen[x] + 1) if x in seen else x
            return seen[x]

        seen = {}
        return sum(find(a) - a for a in A)

    def minIncrementForUnique(self, A: List[int]) -> int:  # by far the easiest and best
        level = -1
        ctr = 0
        A.sort()
        for n in A:
            if level < n:
                level = n
            else:
                level += 1
                ctr += level - n
        return ctr


def test():
    arguments = [
        [1, 2, 2],
        [3, 2, 1, 2, 1, 7],
    ]
    expectations = [1, 6]
    for A, expected in zip(arguments, expectations):
        solution = Solution().minIncrementForUnique(A)
        assert solution == expected
