### Source : https://leetcode.com/problems/minimum-subsequence-in-non-increasing-order/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-28

##################################################################################################### 
#
# Given the array nums, obtain a subsequence of the array whose sum of elements is strictly greater 
# than the sum of the non included elements in such subsequence. 
# 
# If there are multiple solutions, return the subsequence with minimum size and if there still exist 
# multiple solutions, return the subsequence with the maximum total sum of all its elements. A 
# subsequence of an array can be obtained by erasing some (possibly zero) elements from the array. 
# 
# Note that the solution with the given constraints is guaranteed to be unique. Also return the 
# answer sorted in non-increasing order.
# 
# Example 1:
# 
# Input: nums = [4,3,10,9,8]
# Output: [10,9] 
# Explanation: The subsequences [10,9] and [10,8] are minimal such that the sum of their elements is 
# strictly greater than the sum of elements not included, however, the subsequence [10,9] has the 
# maximum total sum of its elements. 
# 
# Example 2:
# 
# Input: nums = [4,4,7,6,7]
# Output: [7,7,6] 
# Explanation: The subsequence [7,7] has the sum of its elements equal to 14 which is not strictly 
# greater than the sum of elements not included (14 = 4 + 4 + 6). Therefore, the subsequence [7,6,7] 
# is the minimal satisfying the conditions. Note the subsequence has to returned in non-decreasing 
# order.  
# 
# Example 3:
# 
# Input: nums = [6]
# Output: [6]
# 
# Constraints:
# 
# 	1 <= nums.length <= 500
# 	1 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def minSubsequence(self, nums: List[int]) -> List[int]:  # O(n log n) time and O(n) space
        nums.sort()
        elements, subsequence_sum, sum_not_included = [], 0, sum(nums)
        while subsequence_sum <= sum_not_included:
            elements.append(nums.pop())
            sum_not_included -= elements[-1]
            subsequence_sum += elements[-1]
        return elements

    def minSubsequence(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        from collections import Counter
        counts = Counter(nums)
        elements, subsequence_sum, sum_not_included = [], 0, sum(nums)
        for i in range(100, 0, -1):
            while counts[i]:
                if subsequence_sum > sum_not_included:
                    return elements
                counts[i] -= 1
                elements.append(i)
                subsequence_sum += i
                sum_not_included -= i
        return elements


def test():
    arguments = [
        [4, 3, 10, 9, 8],
        [4, 4, 7, 6, 7],
        [6],
    ]
    expectations = [
        [10, 9],
        [7, 7, 6],
        [6],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minSubsequence(nums)
        assert solution == expected
