### Source : https://leetcode.com/problems/guess-number-higher-or-lower/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# We are playing the Guess Game. The game is as follows:
# 
# I pick a number from 1 to n. You have to guess which number I picked.
# 
# Every time you guess wrong, I'll tell you whether the number is higher or lower.
# 
# You call a pre-defined API guess(int num) which returns 3 possible results (-1, 1, or 0):
# 
# -1 : y number is lower
#  1 : y number is higher
#  0 : Congrats! You got it!
# 
# Example :
# 
# Input: n = 10, pick = 6
# Output: 6
# 
#####################################################################################################


# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num):


class Solution(object):
    def guessNumber(self, n: int) -> int:  # brute force linear search
        for i in range(n):
            if guess(i) == 0:
                return i

    def guessNumber(self, n: int) -> int:  # binary search
        if n == 1:
            return 1
        low, high = 1, n
        while low <= high:
            mid = (low + high) // 2
            ans = guess(mid)
            if ans == 0:
                return mid
            elif ans == 1:
                low = mid + 1
            elif ans == -1:
                high = mid - 1
        return -1

    def guessNumber(self, n: int) -> int:  # ternary search
        if n == 1:
            return 1
        low, high = 1, n
        while low <= high:
            mid1 = low + (high - low) // 3
            mid2 = high - (high - low) // 3
            res1 = guess(mid1)
            res2 = guess(mid2)
            if res1 == 0 or res2 == 0:
                return mid1 if res1 == 0 else mid2
            elif res1 < 0:
                high = mid1 - 1
            elif res2 > 0:
                low = mid2 + 1
            else:
                low = mid1 + 1
                high = mid2 - 1
        return -1


def test():
    numbers = [10, 1, 2, 2]
    expectations = [6, 1, 1, 2]
    for n, expected in zip(numbers, expectations):
        global guess
        guess = lambda x: 1 if x < expected else -1 if x > expected else 0
        solution = Solution().guessNumber(n)
        assert solution == expected
