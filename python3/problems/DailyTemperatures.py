### Source : https://leetcode.com/problems/daily-temperatures/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# 
# Given a list of daily temperatures T, return a list such that, for each day in the input, tells you 
# how many days you would have to wait until a warmer temperature.  If there is no future day for 
# which this is possible, put 0 instead.
# 
# For example, given the list of temperatures T = [73, 74, 75, 71, 69, 72, 76, 73], your output 
# should be [1, 1, 4, 2, 1, 1, 0, 0].
# 
# Note:
# The length of temperatures will be in the range [1, 30000].
# Each temperature will be an integer in the range [30, 100].
#####################################################################################################

from typing import List


class Solution:
    def dailyTemperatures(self, T: List[int]) -> List[int]:  # O(n) time and O(n) space
        stack = []
        result = [0] * len(T)
        for i, t in enumerate(T):
            while stack and stack[-1][0] < t:  # next highest found
                _, j = stack.pop()
                result[j] = i - j
            stack.append((t, i))
        return result

    def dailyTemperatures(self, T: List[int]) -> List[int]:  # O(n) time and O(1) space
        right_max, result = -1 << 31, [0] * len(T)
        for i, t in reversed(list(enumerate(T))):
            if right_max <= t:
                right_max = t
            else:
                days = 1
                while T[i + days] <= t:
                    days += result[i + days]
                result[i] = days
        return result


def test():
    arguments = [
        [73, 74, 75, 71, 69, 72, 76, 73],
        [30, 40, 50, 60],
        [30, 60, 90],
        [34, 80, 80, 34, 34, 80, 80, 80, 80, 34],
        ]
    expectations = [
        [1, 1, 4, 2, 1, 1, 0, 0],
        [1, 1, 1, 0],
        [1, 1, 0],
        [1, 0, 0, 2, 1, 0, 0, 0, 0, 0],
        ]
    for T, expected in zip(arguments, expectations):
        solution = Solution().dailyTemperatures(T)
        assert solution == expected
