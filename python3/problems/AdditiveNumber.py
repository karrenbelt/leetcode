### Source : https://leetcode.com/problems/additive-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-13

##################################################################################################### 
#
# Additive number is a string whose digits can form additive sequence.
# 
# A valid additive sequence should contain at least three numbers. Except for the first two numbers, 
# each subsequent number in the sequence must be the sum of the preceding two.
# 
# Given a string containing only digits '0'-'9', write a function to determine if it's an additive 
# number.
# 
# Note: Numbers in the additive sequence cannot have leading zeros, so sequence 1, 2, 03 or 1, 02, 3 
# is invalid.
# 
# Example 1:
# 
# Input: "112358"
# Output: true
# Explanation: The digits can form an additive sequence: 1, 1, 2, 3, 5, 8. 
#              1 + 1 = 2, 1 + 2 = 3, 2 + 3 = 5, 3 + 5 = 8
# 
# Example 2:
# 
# Input: "199100199"
# Output: true
# Explanation: The additive sequence is: 1, 99, 100, 199. 
#              1 + 99 = 100, 99 + 100 = 199
# 
# Constraints:
# 
# 	num consists only of digits '0'-'9'.
# 	1 <= num.length <= 35
# 
# Follow up:
# How would you handle overflow for very large input integers?
#####################################################################################################


class Solution:
    # problem is the same as 842: SplitArrayIntoFibonacciSequence.py
    def isAdditiveNumber(self, num: str) -> bool:

        def backtracking(num1: str, num2: str, remainder: str) -> bool:
            if not remainder:
                return True
            target = str(int(num1) + int(num2))
            if remainder.startswith(target) and all(str(int(n)) == n for n in (num1, num2)):
                return backtracking(num2, target, remainder[len(target):])
            return False

        # iterate only as long as we leave enough digits for the remainder to fit the sum
        return any(backtracking(num[:j], num[j:i], num[i:])
                   for i in range(2, len(num)) for j in range(1, i)
                   if len(num) - i >= j and len(num) - i >= i - j)


def test():
    arguments = [
        "112358",
        "199100199",
        "0235813",
    ]
    expectations = [True, True, False]
    for num, expected in zip(arguments, expectations):
        solution = Solution().isAdditiveNumber(num)
        assert solution == expected
