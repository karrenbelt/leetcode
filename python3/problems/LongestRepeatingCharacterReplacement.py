### Source : https://leetcode.com/problems/longest-repeating-character-replacement/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-29

##################################################################################################### 
#
# You are given a string s and an integer k. You can choose any character of the string and change it 
# to any other uppercase English character. You can perform this operation at most k times.
# 
# Return the length of the longest substring containing the same letter you can get after performing 
# the above operations.
# 
# Example 1:
# 
# Input: s = "ABAB", k = 2
# Output: 4
# Explanation: Replace the two 'A's with two 'B's or vice versa.
# 
# Example 2:
# 
# Input: s = "AABABBA", k = 1
# Output: 4
# Explanation: Replace the one 'A' in the middle with 'B' and form "AABBBBA".
# The substring "BBBB" has the longest repeating letters, which is 4.
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s consists of only uppercase English letters.
# 	0 <= k <= s.length
#####################################################################################################


class Solution:
    def characterReplacement(self, s: str, k: int) -> int:  # O(n) time
        count = {}
        i = max_count = longest = 0
        for j in range(len(s)):
            count[s[j]] = count.get(s[j], 0) + 1
            max_count = max(max_count, count[s[j]])
            window_size = j - i + 1
            if window_size - max_count > k:  # must move the window to the right
                count[s[i]] -= 1
                i += 1
            longest = max(longest, j - i + 1)
        return longest


def test():
    arguments = [
        ("ABAB", 2),
        ("AABABBA", 1),
    ]
    expectations = [4, 4]
    for (s, k), expected in zip(arguments, expectations):
        solution = Solution().characterReplacement(s, k)
        assert solution == expected
