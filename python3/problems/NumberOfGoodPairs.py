### Source : https://leetcode.com/problems/number-of-good-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-21

##################################################################################################### 
#
# Given an array of integers nums.
# 
# A pair (i,j) is called good if nums[i] == nums[j] and i < j.
# 
# Return the number of good pairs.
# 
# Example 1:
# 
# Input: nums = [1,2,3,1,1,3]
# Output: 4
# Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
# 
# Example 2:
# 
# Input: nums = [1,1,1,1]
# Output: 6
# Explanation: Each pair in the array are good.
# 
# Example 3:
# 
# Input: nums = [1,2,3]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	1 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:  # O(n^2) time
        ctr = 0
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                if nums[i] == nums[j]:
                    ctr += 1
        return ctr

    def numIdenticalPairs(self, nums: List[int]) -> int:  # O(n^2) time
        return sum(nums[i] == nums[j] for i in range(len(nums)) for j in range(i + 1, len(nums)))

    def numIdenticalPairs(self, nums: List[int]) -> int:  # O(n) time and O(n) space, one pass
        ctr, d = 0, {}
        for n in nums:
            ctr += d.get(n, 0)
            d[n] = d.get(n, 0) + 1
        return ctr


def test():
    arguments = [
        [1, 2, 3, 1, 1, 3],
        [1, 1, 1, 1],
        [1, 2, 3],
    ]
    expectations = [4, 6, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().numIdenticalPairs(nums)
        assert solution == expected
