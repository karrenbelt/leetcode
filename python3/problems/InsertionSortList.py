### Source : https://leetcode.com/problems/insertion-sort-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-27

##################################################################################################### 
#
# Given the head of a singly linked list, sort the list using insertion sort, and return the sorted 
# list's head.
# 
# The steps of the insertion sort algorithm:
# 
# 	Insertion sort iterates, consuming one input element each repetition and growing a sorted 
# output list.
# 	At each iteration, insertion sort removes one element from the input data, finds the 
# location it belongs within the sorted list and inserts it there.
# 	It repeats until no input elements remain.
# 
# The following is a graphical example of the insertion sort algorithm. The partially sorted list 
# (black) initially contains only the first element in the list. One element (red) is removed from 
# the input data and inserted in-place into the sorted list with each iteration.
# 
# Example 1:
# 
# Input: head = [4,2,1,3]
# Output: [1,2,3,4]
# 
# Example 2:
# 
# Input: head = [-1,5,3,4,0]
# Output: [-1,0,3,4,5]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [1, 5000].
# 	-5000 <= Node.val <= 5000
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def insertionSortList(self, head: ListNode) -> ListNode:  # O(n^2) time and O(1) space: 1532 ms
        dummy = ListNode(-1 << 31)
        node = head
        while node:
            prev_walker = walker = dummy
            while walker and node.val > walker.val:
                prev_walker = walker
                walker = walker.next
            next_node = node.next
            node.next = walker
            prev_walker.next = node
            node = next_node
        return dummy.next

    def insertionSortList(self, head):  # O(n^2) time and O(1) space: 168 ms
        walker = dummy = ListNode(0)
        node = dummy.next = head
        while node and node.next:
            value = node.next.val
            if node.val < value:  # if we can just connect it at the end we do so and continue
                node = node.next
                continue
            if walker.next.val > value:  # refresh pointer if next value comes before it only
                walker = dummy
            while walker.next.val < value:
                walker = walker.next
            next_node = node.next
            node.next = next_node.next
            next_node.next = walker.next
            walker.next = next_node
        return dummy.next

    def insertionSortList(self, head: ListNode) -> ListNode:  # O(n^2) time and O(1) space:  2117 ms
        prev = dummy = ListNode(-1 << 31)
        node = head
        while node:
            while prev.next and prev.next.val < node.val:  # classic insertion sort to find position
                prev = prev.next
            prev.next, node.next, node = node, prev.next, node.next
            prev = dummy
        return dummy.next

    def insertionSortList(self, head: ListNode) -> ListNode:  # O(n^2) time and O(1) space: 226ms
        prev = dummy = ListNode(-1 << 31)
        node = head
        while node:
            if prev and prev.val > node.val:  # reset pointer only when new number is smaller than pointer value
                prev = dummy
            while prev.next and prev.next.val < node.val:
                prev = prev.next
            prev.next, node.next, node = node, prev.next, node.next
        return dummy.next

   def insertionSortList(self, head: ListNode) -> ListNode:  # O(n log n) time and O(n) space: 44 ms
        """
        For God's sake, don't try sorting a linked list during the interview
        http://steve-yegge.blogspot.nl/2008/03/get-that-job-at-google.html
        So it might be better to actually copy the values into an array and sort them there.
        """
        node, nodes = head, []
        while node:
            node = nodes.append(node) or node.next
        nodes.sort(key=lambda x: x.val)
        for i in range(len(nodes) - 1):
            nodes[i].next = nodes[i + 1]
        nodes[-1].next = None
        return nodes[0]


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [4, 2, 1, 3],
        [-1, 5, 3, 4, 0],
    ]
    expectations = [
        [1, 2, 3, 4],
        [-1, 0, 3, 4, 5],
    ]
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().insertionSortList(head)
        assert solution == SinglyLinkedList(expected).head
