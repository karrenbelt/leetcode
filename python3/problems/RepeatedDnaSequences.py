### Source : https://leetcode.com/problems/repeated-dna-sequences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# The DNA sequence is composed of a series of nucleotides abbreviated as 'A', 'C', 'G', and 'T'.
# 
# 	For example, "ACGAATTCCG" is a DNA sequence.
# 
# When studying DNA, it is useful to identify repeated sequences within the DNA.
# 
# Given a string s that represents a DNA sequence, return all the 10-letter-long sequences 
# (substrings) that occur more than once in a DNA molecule. You may return the answer in any order.
# 
# Example 1:
# Input: s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
# Output: ["AAAAACCCCC","CCCCCAAAAA"]
# Example 2:
# Input: s = "AAAAAAAAAAAAA"
# Output: ["AAAAAAAAAA"]
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s[i] is either 'A', 'C', 'G', or 'T'.
#####################################################################################################

from typing import List


class Solution:
    def findRepeatedDnaSequences(self, s: str) -> List[str]:  # O(n^2) -> TLE: 30 / 31 test cases passed.
        repeats = set()
        i = 0
        while i < len(s) - 10:
            idx = s.find(s[i: i+10], i + 1)  # linear search, pretty dumb
            if idx != -1:
                repeats.add(s[i: i+10])
            i += 1
        return list(repeats)

    def findRepeatedDnaSequences(self, s: str) -> List[str]:  # O(n) time and O(n) space
        seen = set()
        repeats = set()
        for i in range(len(s) - 9):
            sequence = s[i: i + 10]
            if sequence in seen:
                repeats.add(sequence)
            seen.add(sequence)
        return list(repeats)


def test():
    arguments = [
        "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT",
        "AAAAAAAAAAAAA",
        "AAAAAAAAAAA"
        ]
    expectations = [
        ["AAAAACCCCC", "CCCCCAAAAA"],
        ["AAAAAAAAAA"],
        ["AAAAAAAAAA"],
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().findRepeatedDnaSequences(s)
        assert set(solution) == set(expected)
