### Source : https://leetcode.com/problems/jump-game-iv/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-09

##################################################################################################### 
#
# Given an array of integers arr, you are initially positioned at the first index of the array.
# 
# In one step you can jump from index i to index:
# 
# 	i + 1 where: i + 1 < arr.length.
# 	i - 1 where: i - 1 >= 0.
# 	j where: arr[i] == arr[j] and i != j.
# 
# Return the minimum number of steps to reach the last index of the array.
# 
# Notice that you can not jump outside of the array at any time.
# 
# Example 1:
# 
# Input: arr = [100,-23,-23,404,100,23,23,23,3,404]
# Output: 3
# Explanation: You need three jumps from index 0 --> 4 --> 3 --> 9. Note that index 9 is the last 
# index of the array.
# 
# Example 2:
# 
# Input: arr = [7]
# Output: 0
# Explanation: Start index is the last index. You don't need to jump.
# 
# Example 3:
# 
# Input: arr = [7,6,9,6,9,6,9,7]
# Output: 1
# Explanation: You can jump directly from index 0 to index 7 which is last index of the array.
# 
# Example 4:
# 
# Input: arr = [6,1,9]
# Output: 2
# 
# Example 5:
# 
# Input: arr = [11,22,7,7,7,7,7,7,7,22,13]
# Output: 3
# 
# Constraints:
# 
# 	1 <= arr.length <= 5 * 104
# 	-108 <= arr[i] <= 108
#####################################################################################################

from typing import List


class Solution:
    def minJumps(self, arr: List[int]) -> int:  # O(n ^ 2) time and O(n) space -> TLE (26 / 31)
        jumps = {}
        for i, n in enumerate(arr):
            jumps.setdefault(n, []).append(i)

        stack, step, seen = [0], 0, [False] * len(arr)
        while stack:
            next_stack = []
            for node in stack:
                if node == len(arr) - 1:
                    return step
                for next_node in (node + 1, node - 1, *jumps.get(arr[node], [])):
                    if 0 <= next_node < len(arr) and not seen[next_node]:
                        seen[next_node] = True
                        next_stack.append(next_node)
            stack = next_stack
            step += 1
        return -1  # never reached

    def minJumps(self, arr: List[int]) -> int:  # O(n) time and O(n) space: still TLE?
        jumps = {}
        for i, n in enumerate(arr):
            jumps.setdefault(n, []).append(i)

        stack, step, seen, seen_groups = [0], 0, [False] * len(arr), set()
        while stack:
            next_stack = []
            for node in stack:
                if node == len(arr) - 1:
                    return step

                for next_node in (node + 1, node - 1):
                    if 0 <= next_node < len(arr) and not seen[next_node]:
                        seen[next_node] = True
                        next_stack.append(next_node)

                if arr[node] not in seen_groups:
                    seen_groups.add(arr[node])
                    for next_node in jumps.get(arr[node], []):
                        if next_node not in seen:
                            seen[next_node] = True
                            next_stack.append(next_node)
            stack = next_stack
            step += 1

    def minJumps(self, arr: List[int]) -> int:  # O(n)
        from collections import deque

        graph = {}
        for i, n in enumerate(arr):
            graph.setdefault(n, set()).add(i)

        queue, seen = deque([(0, 0)]), {-1, len(arr)}
        while queue:
            steps, i = queue.popleft()
            if i == len(arr) - 1:
                return steps
            indices = graph.pop(arr[i], set()) | {i - 1, i + 1} - seen
            seen.update(indices)
            queue.extend((steps + 1, j) for j in indices)


def test():
    arguments = [
        [100, -23, -23, 404, 100, 23, 23, 23, 3, 404],
        [7],
        [7, 6, 9, 6, 9, 6, 9, 7],
        [6, 1, 9],
        [11, 22, 7, 7, 7, 7, 7, 7, 7, 22, 13],
        [7] * 49999 + [11],

    ]
    expectations = [3, 0, 1, 2, 3]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().minJumps(arr)
        assert solution == expected
