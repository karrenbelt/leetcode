### Source : https://leetcode.com/problems/h-index-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-27

##################################################################################################### 
#
# Given an array of integers citations where citations[i] is the number of citations a researcher 
# received for their ith paper and citations is sorted in an ascending order, return compute the 
# researcher's h-index.
# 
# According to the definition of h-index on Wikipedia: A scientist has an index h if h of their n 
# papers have at least h citations each, and the other n &minus; h papers have no more than h 
# citations each.
# 
# If there are several possible values for h, the maximum one is taken as the h-index.
# 
# Example 1:
# 
# Input: citations = [0,1,3,5,6]
# Output: 3
# Explanation: [0,1,3,5,6] means the researcher has 5 papers in total and each of them had received 
# 0, 1, 3, 5, 6 citations respectively.
# Since the researcher has 3 papers with at least 3 citations each and the remaining two with no more 
# than 3 citations each, their h-index is 3.
# 
# Example 2:
# 
# Input: citations = [1,2,100]
# Output: 2
# 
# Constraints:
# 
# 	n == citations.length
# 	1 <= n <= 105
# 	0 <= citations[i] <= 1000
# 	citations is sorted in ascending order.
# 
# Follow up: Could you solve it in logarithmic time complexity?
#####################################################################################################

from typing import List


class Solution:
    def hIndex(self, citations: List[int]) -> int:
        # same as previous except already sorted. We do a binary search
        left, right = 0, len(citations) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if citations[mid] == len(citations) - mid:
                return citations[mid]
            elif citations[mid] < len(citations) - mid:
                left = mid + 1
            else:
                right = mid - 1
        return len(citations) - left


def test():
    arguments = [
        [0, 1, 3, 5, 6],
        [1, 2, 100],
        [1, 1, 3]
    ]
    expectations = [3, 2, 1]
    for citations, expected in zip(arguments, expectations):
        solution = Solution().hIndex(citations)
        assert solution == expected
