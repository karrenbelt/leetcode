### Source : https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-13

##################################################################################################### 
#
# You are given an array prices where prices[i] is the price of a given stock on the ith day.
# 
# Find the maximum profit you can achieve. You may complete as many transactions as you like (i.e., 
# buy one and sell one share of the stock multiple times) with the following restrictions:
# 
# 	After you sell your stock, you cannot buy stock on the next day (i.e., cooldown one day).
# 
# Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock 
# before you buy again).
# 
# Example 1:
# 
# Input: prices = [1,2,3,0,2]
# Output: 3
# Explanation: transactions = [buy, sell, cooldown, buy, sell]
# 
# Example 2:
# 
# Input: prices = [1]
# Output: 0
# 
# Constraints:
# 
# 	1 <= prices.length <= 5000
# 	0 <= prices[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:  # O(n^2) time and O(n) space
        dp = [0] * (len(prices) + 2)
        sell_i = len(prices) - 1
        for i in range(len(prices) - 2, -1, -1):
            dp[i] = dp[i + 1]
            for j in range(i + 1, sell_i + 1):
                dp[i] = max(dp[i], prices[j] - prices[i] + dp[j + 2])
            if prices[i] > prices[-1]:
                sell_i = i
        return dp[0]

    def maxProfit(self, prices: List[int]) -> int:  # O(n) time and O(n) space
        dp = [0] * (len(prices) + 2)
        sell_i = len(prices) - 1
        for i in range(len(prices) - 2, -1, -1):
            dp[i] = max(dp[i + 1], prices[sell_i] - prices[i] + dp[sell_i + 2])
            if prices[i] + dp[i + 2] > prices[sell_i] + dp[sell_i + 2]:
                sell_i = i
        return dp[0]

    def maxProfit(self, prices: List[int]) -> int:  # O(n) time and O(1) space
        prev = prices[-1]
        a = b = c = 0
        for i in range(len(prices) - 2, -1, -1):
            a = max(a, prev - prices[i])
            prev = max(prev, prices[i] + c)
            b, c = a, b
        return a

    def maxProfit(self, prices: List[int]) -> int:  # O(n) time and O(1) space
        a, b, c = prices[-1], 0, 0
        for i in range(len(prices) - 2, -1, -1):
            a, b, c = max(a, prices[i] + c), max(b, a - prices[i]), b
        return b


def test():
    arguments = [
        [1, 2, 3, 0, 2],
        [1],
        ]
    expectations = [3, 0]
    for prices, expected in zip(arguments, expectations):
        solution = Solution().maxProfit(prices)
        assert solution == expected
test()