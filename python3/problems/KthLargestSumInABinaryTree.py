### Source : https://leetcode.com/problems/kth-largest-sum-in-a-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-05

##################################################################################################### 
#
# You are given the root of a binary tree and a positive integer k.
# 
# The level sum in the tree is the sum of the values of the nodes that are on the same level.
# 
# Return the kth largest level sum in the tree (not necessarily distinct). If there are fewer than k 
# levels in the tree, return -1.
# 
# Note that two nodes are on the same level if they have the same distance from the root.
# 
# Example 1:
# 
# Input: root = [5,8,9,2,1,3,7,4,6], k = 2
# Output: 13
# Explanation: The level sums are the following:
# - Level 1: 5.
# - Level 2: 8 + 9 = 17.
# - Level 3: 2 + 1 + 3 + 7 = 13.
# - Level 4: 4 + 6 = 10.
# The 2nd largest level sum is 13.
# 
# Example 2:
# 
# Input: root = [1,2,null,3], k = 1
# Output: 3
# Explanation: The largest level sum is 3.
# 
# Constraints:
# 
# 	The number of nodes in the tree is n.
# 	2 <= n <= 105
# 	1 <= Node.val <= 106
# 	1 <= k <= n
#####################################################################################################

from typing import Optional
from python3 import Codec
from python3 import BinaryTreeNode as TreeNode

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def kthLargestLevelSum(self, root: Optional[TreeNode], k: int) -> int:
        from collections import defaultdict

        def dfs(node: Optional[TreeNode], i: int) -> None:
            if node:
                levels[i] += node.val
                dfs(node.left, i + 1)
                dfs(node.right, i + 1)

        levels = defaultdict(int)
        dfs(root, 0)
        return -1 if len(levels) < k else sorted(levels.values())[-k]


def test():
    arguments = [
        ("[5,8,9,2,1,3,7,4,6]", 2),
        ("[1,2,null,3]", 1),
        ("[5,8,9,2,1,3,7]", 4),
    ]
    expectations = [13, 3, -1]
    for (serialized_tree, k), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().kthLargestLevelSum(root, k)
        assert solution == expected
