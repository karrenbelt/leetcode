### Source : https://leetcode.com/problems/flip-binary-tree-to-match-preorder-traversal/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-01

##################################################################################################### 
#
# You are given the root of a binary tree with n nodes, where each node is uniquely assigned a value 
# from 1 to n. You are also given a sequence of n values voyage, which is the desired pre-order 
# traversal of the binary tree.
# 
# Any node in the binary tree can be flipped by swapping its left and right subtrees. For example, 
# flipping node 1 will have the following effect:
# 
# Flip the smallest number of nodes so that the pre-order traversal of the tree matches voyage.
# 
# Return a list of the values of all flipped nodes. You may return the answer in any order. If it is 
# impossible to flip the nodes in the tree to make the pre-order traversal match voyage, return the 
# list [-1].
# 
# Example 1:
# 
# Input: root = [1,2], voyage = [2,1]
# Output: [-1]
# Explanation: It is impossible to flip the nodes such that the pre-order traversal matches voyage.
# 
# Example 2:
# 
# Input: root = [1,2,3], voyage = [1,3,2]
# Output: [1]
# Explanation: Flipping node 1 swaps nodes 2 and 3, so the pre-order traversal matches voyage.
# 
# Example 3:
# 
# Input: root = [1,2,3], voyage = [1,2,3]
# Output: []
# Explanation: The tree's pre-order traversal already matches voyage, so no nodes need to be flipped.
# 
# Constraints:
# 
# 	The number of nodes in the tree is n.
# 	n == voyage.length
# 	1 <= n <= 100
# 	1 <= Node.val, voyage[i] <= n
# 	All the values in the tree are unique.
# 	All the values in voyage are unique.
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def flipMatchVoyage(self, root: TreeNode, voyage: List[int]) -> List[int]:

        def dfs(node: TreeNode):
            nonlocal i
            if not node or i > len(voyage):
                return
            if node.val != voyage[i]:
                answer.append(None)
                return
            i += 1
            if node.left and node.left.val != voyage[i]:
                answer.append(node.val)
                children = [node.right, node.left]
            else:
                children = [node.left, node.right]
            for child in children:
                dfs(child)

        i = 0
        answer = []
        dfs(root)
        return [-1] if None in answer else answer

    def flipMatchVoyage(self, root: TreeNode, voyage: List[int]) -> List[int]:

        def dfs(node: TreeNode) -> bool:
            nonlocal i
            if not node:
                return True
            elif node.val != voyage[i]:
                return False
            i += 1
            if dfs(node.left) and dfs(node.right):
                return True
            elif dfs(node.right) and dfs(node.left):
                answer.append(node.val)
                return True
            return False

        i = 0
        answer = []
        return answer if dfs(root) else [-1]

    def flipMatchVoyage(self, root: TreeNode, voyage: List[int]) -> List[int]:
        i = 0
        stack = [root]
        answer = []
        while stack:
            node = stack.pop()
            if node.val != voyage[i]:
                return [-1]
            elif node.right and node.right.val == voyage[i + 1]:
                if node.left:
                    answer.append(node.val)
                nodes = [node.left, node.right]
            else:
                nodes = [node.right, node.left]
            stack.extend(filter(None, nodes))
            i += 1
        return answer


def test():
    from python3 import Codec
    arguments = [
        ("[1,2]", [2, 1]),
        ("[1,2,3]", [1, 3, 2]),
        ("[1,2,3]", [1, 2, 3]),
        ("[1,null,2]", [1, 2])
        ]
    expectations = [
        [-1],
        [1],
        [],
        [],
        ]
    for (serialized_tree, voyage), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().flipMatchVoyage(root, voyage)
        assert set(solution) == set(expected)
