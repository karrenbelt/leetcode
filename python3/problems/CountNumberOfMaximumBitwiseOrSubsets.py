### Source : https://leetcode.com/problems/count-number-of-maximum-bitwise-or-subsets/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-12

##################################################################################################### 
#
# Given an integer array nums, find the maximum possible bitwise OR of a subset of nums and return 
# the number of different non-empty subsets with the maximum bitwise OR.
# 
# An array a is a subset of an array b if a can be obtained from b by deleting some (possibly zero) 
# elements of b. Two subsets are considered different if the indices of the elements chosen are 
# different.
# 
# The bitwise OR of an array a is equal to a[0] OR a[1] OR ... OR a[a.length - 1] (0-indexed).
# 
# Example 1:
# 
# Input: nums = [3,1]
# Output: 2
# Explanation: The maximum possible bitwise OR of a subset is 3. There are 2 subsets with a bitwise 
# OR of 3:
# - [3]
# - [3,1]
# 
# Example 2:
# 
# Input: nums = [2,2,2]
# Output: 7
# Explanation: All non-empty subsets of [2,2,2] have a bitwise OR of 2. There are 23 - 1 = 7 total 
# subsets.
# 
# Example 3:
# 
# Input: nums = [3,2,1,5]
# Output: 6
# Explanation: The maximum possible bitwise OR of a subset is 7. There are 6 subsets with a bitwise 
# OR of 7:
# - [3,5]
# - [3,1,5]
# - [3,2,5]
# - [3,2,1,5]
# - [2,5]
# - [2,1,5]
# 
# Constraints:
# 
# 	1 <= nums.length <= 16
# 	1 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def countMaxOrSubsets(self, nums: List[int]) -> int:  # O(2^n) time
        from operator import ior
        from functools import reduce

        def backtrack(i: int, subset: tuple[int, ...]) -> None:
            nonlocal ctr
            if i == len(nums):
                ctr += reduce(ior, subset, 0) == xor
                return
            backtrack(i + 1, subset)
            backtrack(i + 1, subset + (nums[i], ))

        ctr, xor = 0, reduce(ior, nums)
        backtrack(0, ())
        return ctr

    def countMaxOrSubsets(self, nums: List[int]) -> int:  # O(2^n) time
        from operator import ior
        from functools import reduce

        def backtrack(i: int, cur: tuple[int, ...]) -> int:
            if i == len(nums):
                return reduce(ior, cur, 0) == xor
            return backtrack(i + 1, cur) + backtrack(i + 1, cur + (nums[i],))

        xor = reduce(ior, nums)
        return backtrack(0, ())

    def countMaxOrSubsets(self, nums: List[int]) -> int:  # O(2^n) time
        from operator import ior
        from functools import reduce

        def is_max_xor(i: int) -> bool:
            f = filter(lambda j: i & (1 << j), range(n))
            return reduce(ior, (nums[j] for j in f)) == xor

        n, xor = len(nums), reduce(ior, nums)
        return sum(map(is_max_xor, range(1, 1 << n)))

    def countMaxOrSubsets(self, nums: List[int]) -> int:  # O(n^2)
        from functools import reduce, cache

        @cache
        def dp(i: int, mask: int) -> int:
            if mask == target:
                return 1 << len(nums) - i
            if i == len(nums):
                return 0
            return dp(i + 1, mask | nums[i]) + dp(i + 1, mask)

        target = reduce(lambda x, y: x | y, nums)
        return dp(0, 0)

    def countMaxOrSubsets(self, nums: List[int]) -> int:  # O(n^2) time
        from collections import Counter

        dp = Counter([0])
        for n in nums:
            for k, v in dp.items():
                dp[k | n] += v
        return dp[max(dp)]


def test():
    arguments = [
        [3, 1],
        [2, 2, 2],
        [3, 2, 1, 5],
    ]
    expectations = [2, 7, 6]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().countMaxOrSubsets(nums)
        assert solution == expected
