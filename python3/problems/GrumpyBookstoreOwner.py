### Source : https://leetcode.com/problems/grumpy-bookstore-owner/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-20

##################################################################################################### 
#
# Today, the bookstore owner has a store open for customers.length minutes.  Every minute, some 
# number of customers (customers[i]) enter the store, and all those customers leave after the end of 
# that minute.
# 
# On some minutes, the bookstore owner is grumpy.  If the bookstore owner is grumpy on the i-th 
# minute, grumpy[i] = 1, otherwise grumpy[i] = 0.  When the bookstore owner is grumpy, the customers 
# of that minute are not satisfied, otherwise they are satisfied.
# 
# The bookstore owner knows a secret technique to keep themselves not grumpy for minutes minutes 
# straight, but can only use it once.
# 
# Return the maximum number of customers that can be satisfied throughout the day.
# 
# Example 1:
# 
# Input: customers = [1,0,1,2,1,1,7,5], grumpy = [0,1,0,1,0,1,0,1], minutes = 3
# Output: 16
# Explanation: The bookstore owner keeps themselves not grumpy for the last 3 minutes. 
# The maximum number of customers that can be satisfied = 1 + 1 + 1 + 1 + 7 + 5 = 16.
# 
# Note:
# 
# 	1 <= minutes <= customers.length == grumpy.length <= 20000
# 	0 <= customers[i] <= 1000
# 	0 <= grumpy[i] <= 1
#####################################################################################################

from typing import List


class Solution:
    def maxSatisfied(self, customers: List[int], grumpy: List[int], minutes: int) -> int:
        # element-wise multiplication of grumpy and customers (or boolean filtering)
        # then find the maximum subarray of length minutes and subtract this from the sum
        dissatisfied = [c if g else 0 for c, g in zip(customers, grumpy)]
        window = (0, minutes)
        maximum = running = sum(dissatisfied[:minutes])
        for i in range(len(dissatisfied) - minutes):
            running += dissatisfied[i + minutes] - dissatisfied[i]
            if running > maximum:
                maximum, window = running, (i + 1, i + minutes + 1)
        grumpy[window[0]: window[1]] = [0] * minutes
        return sum(c if not g else 0 for c, g in zip(customers, grumpy))

    def maxSatisfied(self, customers: List[int], grumpy: List[int], minutes: int) -> int:
        window = (0, minutes)
        maximum = running = sum(customers[i] for i in range(minutes) if grumpy[i])
        for i in range(len(customers) - minutes):
            running += customers[i + minutes] * grumpy[i + minutes] - customers[i] * grumpy[i]
            if running > maximum:
                maximum, window = running, (i + 1, i + minutes + 1)
        grumpy[window[0]: window[1]] = [0] * minutes
        return sum(c if not g else 0 for c, g in zip(customers, grumpy))


def test():
    arguments = [
        ([1, 0, 1, 2, 1, 1, 7, 5], [0, 1, 0, 1, 0, 1, 0, 1], 3),
    ]
    expectations = [16]
    for (customers, grumpy, minutes), expected in zip(arguments, expectations):
        solution = Solution().maxSatisfied(customers, grumpy, minutes)
        assert solution == expected
