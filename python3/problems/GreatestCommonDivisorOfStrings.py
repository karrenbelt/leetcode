### Source : https://leetcode.com/problems/greatest-common-divisor-of-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-13

##################################################################################################### 
#
# For two strings s and t, we say "t divides s" if and only if s = t + ... + t  (t concatenated with 
# itself 1 or more times)
# 
# Given two strings str1 and str2, return the largest string x such that x divides both str1 and str2.
# 
# Example 1:
# Input: str1 = "ABCABC", str2 = "ABC"
# Output: "ABC"
# Example 2:
# Input: str1 = "ABABAB", str2 = "ABAB"
# Output: "AB"
# Example 3:
# Input: str1 = "LEET", str2 = "CODE"
# Output: ""
# Example 4:
# Input: str1 = "ABCDEF", str2 = "ABC"
# Output: ""
# 
# Constraints:
# 
# 	1 <= str1.length <= 1000
# 	1 <= str2.length <= 1000
# 	str1 and str2 consist of English uppercase letters.
#####################################################################################################


class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        if not str1 or not str2:
            return str1 or str2
        elif len(str1) < len(str2):
            return self.gcdOfStrings(str2, str1)
        elif str1[:len(str2)] == str2:
            return self.gcdOfStrings(str1[len(str2):], str2)
        return ""

    def gcdOfStrings(self, str1: str, str2: str) -> str:

        def gcd(a: int, b: int) -> int:
            return b if a == 0 else gcd(b % a, a)

        return str1[:gcd(len(str1), len(str2))] if str1 + str2 == str2 + str1 else ""

    def gcdOfStrings(self, str1: str, str2: str) -> str:
        from math import gcd
        if str1 + str2 != str2 + str1:
            return ""
        return str1[:gcd(len(str1), len(str2))]


def test():
    arguments = [
        ("ABCABC", "ABC"),
        ("ABABAB", "ABAB"),
        ("LEET", "CODE"),
        ("ABCDEF", "ABC"),
        ("ABCCBA", "ABC"),
        ("ABC", "AAAAAA"),
        ("TAUXXTAUXXTAUXXTAUXXTAUXX", "TAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXX"),
    ]
    expectations = ["ABC", "AB", "", "", "", "", "TAUXX"]
    for (str1, str2), expected in zip(arguments, expectations):
        solution = Solution().gcdOfStrings(str1, str2)
        assert solution == expected
test()