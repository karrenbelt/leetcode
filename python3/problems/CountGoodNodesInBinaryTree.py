### Source : https://leetcode.com/problems/count-good-nodes-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-10

##################################################################################################### 
#
# Given a binary tree root, a node X in the tree is named good if in the path from root to X there 
# are no nodes with a value greater than X.
# 
# Return the number of good nodes in the binary tree.
# 
# Example 1:
# 
# Input: root = [3,1,4,3,null,1,5]
# Output: 4
# Explanation: Nodes in blue are good.
# Root Node (3) is always a good node.
# Node 4 -> (3,4) is the maximum value in the path starting from the root.
# Node 5 -> (3,4,5) is the maximum value in the path
# Node 3 -> (3,1,3) is the maximum value in the path.
# 
# Example 2:
# 
# Input: root = [3,3,null,4,2]
# Output: 3
# Explanation: Node 2 -> (3, 3, 2) is not good, because "3" is higher than it.
# 
# Example 3:
# 
# Input: root = [1]
# Output: 1
# Explanation: Root is considered as good.
# 
# Constraints:
# 
# 	The number of nodes in the binary tree is in the range [1, 105].
# 	Each node's value is between [-104, 104].
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def goodNodes(self, root: TreeNode) -> int:  # O(n) time and O(1) space

        def traverse(node: TreeNode, value: int):  # pre-order
            nonlocal ctr
            if not node:
                return
            if node.val >= value:
                value = node.val
                ctr += 1
            traverse(node.left, value)
            traverse(node.right, value)

        ctr = 0
        traverse(root, root.val)
        return ctr

    def goodNodes(self, root: TreeNode) -> int:  # O(n) time and O(1) space

        def traverse(node: TreeNode, value: int) -> int:  # pre-order
            if not node:
                return 0
            if node.val >= value:
                value = node.val
            return (node.val >= value) + traverse(node.left, value) + traverse(node.right, value)

        return traverse(root, root.val)

    def goodNodes(self, root: TreeNode) -> int:
        # keep track of max along the way for counting
        def traverse(node: TreeNode, maximum: int):
            if not node:
                return 0
            left = traverse(node.left, max(node.val, maximum))
            right = traverse(node.right, max(node.val, maximum))
            return left + right + (node.val >= maximum)

        return traverse(root, root.val)


def test():
    from python3 import Codec
    arguments = [
        "[3,1,4,3,null,1,5]",
        "[3,3,null,4,2]",
        "[1]",
    ]
    expectations = [4, 3, 1]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().goodNodes(root)
        assert solution == expected
