### Source : https://leetcode.com/problems/minimum-operations-to-make-array-equal/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# You have an array arr of length n where arr[i] = (2 * i) + 1 for all valid values of i (i.e. 0 <= i 
# < n).
# 
# In one operation, you can select two indices x and y where 0 <= x, y < n and subtract 1 from arr[x] 
# and add 1 to arr[y] (i.e. perform arr[x] -=1 and arr[y] += 1). The goal is to make all the elements 
# of the array equal. It is guaranteed that all the elements of the array can be made equal using 
# some operations.
# 
# Given an integer n, the length of the array. Return the minimum number of operations needed to make 
# all the elements of arr equal.
# 
# Example 1:
# 
# Input: n = 3
# Output: 2
# Explanation: arr = [1, 3, 5]
# First operation choose x = 2 and y = 0, this leads arr to be [2, 3, 4]
# In the second operation choose x = 2 and y = 0 again, thus arr = [3, 3, 3].
# 
# Example 2:
# 
# Input: n = 6
# Output: 9
# 
# Constraints:
# 
# 	1 <= n <= 104
#####################################################################################################


class Solution:
    def minOperations(self, n: int) -> int:  # O(n) time and O(n) space
        # n = 1, then mean = 1
        # n = 2, then mean = 2
        # n = 3, then mean = 3, etc.
        return sum(n - 2 * i - 1 for i in range(n//2))

    def minOperations(self, n: int) -> int:  # O(n) time and O(n) space
        median = 2 * (n // 2) + 1 if n % 2 else 2 * (n // 2)
        return sum(median - (2 * i + 1) for i in range(n // 2))

    def minOperations(self, n: int) -> int:  # O(1) time and O(1) space
        # arithmetic sequence: (n/2)^2 for even n, and extra n // 2 steps for uneven.
        return ((n + 1) // 2) * (n // 2)

    def minOperations(self, n: int) -> int:  # O(1) time and O(1) space
        return n * n // 4


def test():
    arguments = [3, 6]
    expectations = [2, 9]
    for n, expected in zip(arguments, expectations):
        solution = Solution().minOperations(n)
        assert solution == expected
