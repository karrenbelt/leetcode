### Source : https://leetcode.com/problems/word-break/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a 
# space-separated sequence of one or more dictionary words.
# 
# Note that the same word in the dictionary may be reused multiple times in the segmentation.
# 
# Example 1:
# 
# Input: s = "leetcode", wordDict = ["leet","code"]
# Output: true
# Explanation: Return true because "leetcode" can be segmented as "leet code".
# 
# Example 2:
# 
# Input: s = "applepenapple", wordDict = ["apple","pen"]
# Output: true
# Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
# Note that you are allowed to reuse a dictionary word.
# 
# Example 3:
# 
# Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
# Output: false
# 
# Constraints:
# 
# 	1 <= s.length <= 300
# 	1 <= wordDict.length <= 1000
# 	1 <= wordDict[i].length <= 20
# 	s and wordDict[i] consist of only lowercase English letters.
# 	All the strings of wordDict are unique.
#####################################################################################################

from typing import List


class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        index_mappings = {}
        for word in wordDict:
            idx = s.find(word)
            while idx != -1:
                index_mappings.setdefault(idx, []).append(idx + len(word))
                idx = s.find(word, idx + 1)

        seen, next_nodes = set(), {0}
        while next_nodes:
            new_nodes = set()
            for node in next_nodes:
                if node == len(s):
                    return True
                seen.add(node)
                neighbors = index_mappings.get(node, [])
                for neighbor in neighbors:
                    if neighbor not in seen:
                        new_nodes.add(neighbor)
            next_nodes = new_nodes
        return False

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        ok = [True]
        for i in range(1, len(s)+1):
            ok += any(ok[j] and s[j:i] in wordDict for j in range(i)),
        return ok[-1]


def test():
    arguments = [
        ("leetcode", ["leet", "code"]),
        ("applepenapple", ["apple", "pen"]),
        ("catsandog", ["cats", "dog", "sand", "and", "cat"]),
    ]
    expectations = [True, True, False]
    for (s, wordDict), expected in zip(arguments, expectations):
        solution = Solution().wordBreak(s, wordDict)
        assert solution == expected
