### Source : https://leetcode.com/problems/design-hashmap/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# Design a Hashap without using any built-in hash table libraries.
# 
# To be specific, your design should include these functions:
# 
# 	put(key, value) : Insert a (key, value) pair into the Hashap. If the value already exists 
# in the Hashap, update the value.
# 	get(key): Returns the value to which the specified key is mapped, or -1 if this map 
# contains no mapping for the key.
# 	remove(key) : Remove the mapping for the value key if this map contains the mapping for the 
# key.
# 
# Example:
# 
# yHashap hashap = new yHashap();
# hashap.put(1, 1);          
# hashap.put(2, 2);         
# hashap.get(1);            // returns 1
# hashap.get(3);            // returns -1 (not found)
# hashap.put(2, 1);          // update the existing value
# hashap.get(2);            // returns 1 
# hashap.remove(2);          // remove the mapping for 2
# hashap.get(2);            // returns -1 (not found) 
# 
# Note:
# 
# 	All keys and values will be in the range of [0, 1000000].
# 	The number of operations will be in the range of [1, 10000].
# 	Please do not use the built-in Hashap library.
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as Node
#
#
# class Node(SinglyLinkedListNode):
#
#     def __init__(self, value, next=None):
#         super().__init__(value, next)


class MyHashMap:  # simplest but terrible implementation

    def __init__(self):
        self.array = [-1] * 1000001

    def put(self, key: int, value: int) -> None:
        self.array[key] = value

    def get(self, key: int) -> int:
        return self.array[key]

    def remove(self, key: int) -> None:
        self.array[key] = -1


class MyHashMap:  # arrays with tuples

    def __init__(self):
        self.array = [[] for _ in range(100)]

    @staticmethod
    def hash(key):
        return key % 99

    def put(self, key: int, value: int) -> None:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(list(zip(*self.array[hash_key]))[0])
            if key in keys:
                self.array[hash_key][keys.index(key)] = (key, value)
            else:
                self.array[hash_key].append((key, value))
        else:
            self.array[hash_key].append((key, value))

    def get(self, key: int) -> int:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(zip(*self.array[hash_key]))[0]
            if key in keys:
                return self.array[hash_key][keys.index(key)][1]
        return -1

    def remove(self, key: int) -> None:
        hash_key = self.hash(key)
        if self.array[hash_key]:
            keys = list(zip(*self.array[hash_key]))[0]
            if key in keys:
                del self.array[hash_key][keys.index(key)]


class MyHashMap:  # arrays with linked list

    def __init__(self):
        self.m = 997  # prime number
        self.data = [Node([None, None]) for _ in range(self.m)]

    def hash(self, key):
        return key % len(self.data)

    def put(self, key: int, value: int) -> None:
        hashcode = self.hash(key)
        head = self.data[hashcode]
        while head.next:
            if head.next.val[0] == key:
                head.next.val[1] = value
                return
            head = head.next
        head.next = Node([key, value])

    def get(self, key: int) -> int:
        hashcode = self.hash(key)
        head = self.data[hashcode]
        while head.next:
            if head.next.val[0] == key:
                return head.next.val[1]
            head = head.next
        return -1

    def remove(self, key: int) -> None:
        hashcode = self.hash(key)
        head = self.data[hashcode]
        while head.next:
            if head.next.val[0] == key:
                tmp = head.next
                head.next = tmp.next
                tmp.next = None
                return
            head = head.next


# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)


def test():
    operations = ["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
    arguments = [(), (1, 1), (2, 2), (1,), (3,), (2, 1), (2,), (2,), (2,)]
    expectations = [None, None, None, 1, -1, None, 1, None, -1]
    obj = MyHashMap()
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
