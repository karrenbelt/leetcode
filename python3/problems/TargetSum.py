### Source : https://leetcode.com/problems/target-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-20

##################################################################################################### 
#
# You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 
# symbols + and -. For each integer, you should choose one from + and - as its new symbol.
# 
# Find out how many ways to assign symbols to make sum of integers equal to target S.
# 
# Example 1:
# 
# Input: nums is [1, 1, 1, 1, 1], S is 3. 
# Output: 5
# Explanation: 
# 
# -1+1+1+1+1 = 3
# +1-1+1+1+1 = 3
# +1+1-1+1+1 = 3
# +1+1+1-1+1 = 3
# +1+1+1+1-1 = 3
# 
# There are 5 ways to assign symbols to make the sum of nums be target 3.
# 
# Constraints:
# 
# 	The length of the given array is positive and will not exceed 20.
# 	The sum of elements in the given array will not exceed 1000.
# 	Your output answer is guaranteed to be fitted in a 32-bit integer.
#####################################################################################################

from typing import List


class Solution:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:  # dp: O(ns) time

        def dp(i: int, s: int):
            if (i, s) in memo:
                return memo[(i, s)]
            if i < 0 and s == S:
                return 1
            if i < 0:
                return 0
            positive = dp(i - 1, s + nums[i])
            negative = dp(i - 1, s - nums[i])
            memo[(i, s)] = positive + negative
            return memo[(i, s)]

        memo = {}
        return dp(len(nums) - 1, 0)

    def findTargetSumWays(self, nums: List[int], S: int) -> int:  # shorter, and iterate from 0
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, s: int):
            return int(s == S) if i == len(nums) else dfs(i + 1, s + nums[i]) + dfs(i + 1, s - nums[i])

        return dfs(0, 0)

    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        import collections
        count = collections.Counter({0: 1})
        for x in nums:
            step = collections.Counter()
            for y in count:
                step[y + x] += count[y]
                step[y - x] += count[y]
            count = step
        return count[S]


def test():
    arguments = [
        ([1, 1, 1, 1, 1], 3),
        ]
    expectations = [3]
    for (nums, S), expected in zip(arguments, expectations):
        solution = Solution().findTargetSumWays(nums, S)
        assert solution == expected
