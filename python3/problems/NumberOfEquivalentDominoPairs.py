### Source : https://leetcode.com/problems/number-of-equivalent-domino-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] = [c, d] if and only if 
# either (a==c and b==d), or (a==d and b==c) - that is, one domino can be rotated to be equal to 
# another domino.
# 
# Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and dominoes[i] is 
# equivalent to dominoes[j].
# 
# Example 1:
# Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
# Output: 1
# 
# Constraints:
# 
# 	1 <= dominoes.length <= 40000
# 	1 <= dominoes[i][j] <= 9
#####################################################################################################

from typing import List


class Solution:
    def numEquivDominoPairs(self, dominoes: List[List[int]]) -> int:
        from collections import Counter  # frozenset works because at most 2, so [1, 1] -> {1}
        return sum(v * (v - 1) // 2 for v in Counter(map(frozenset, dominoes)).values())


def test():
    arguments = [
        [[1, 2], [2, 1], [3, 4], [5, 6]],
        [[1, 2], [1, 2], [1, 1], [1, 2], [2, 2]],
    ]
    expectations = [1, 3]
    for dominoes, expected in zip(arguments, expectations):
        solution = Solution().numEquivDominoPairs(dominoes)
        assert solution == expected
