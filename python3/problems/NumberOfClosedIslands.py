### Source : https://leetcode.com/problems/number-of-closed-islands/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

##################################################################################################### 
#
# Given a 2D grid consists of 0s (land) and 1s (water).  An island is a maximal 4-directionally 
# connected group of 0s and a closed island is an island totally (all left, top, right, bottom) 
# surrounded by 1s.
# 
# Return the number of closed islands.
# 
# Example 1:
# 
# Input: grid = 
# [[1,1,1,1,1,1,1,0],[1,0,0,0,0,1,1,0],[1,0,1,0,1,1,1,0],[1,0,0,0,0,1,0,1],[1,1,1,1,1,1,1,0]]
# Output: 2
# Explanation: 
# Islands in gray are closed because they are completely surrounded by water (group of 1s).
# 
# Example 2:
# 
# Input: grid = [[0,0,1,0,0],[0,1,0,1,0],[0,1,1,1,0]]
# Output: 1
# 
# Example 3:
# 
# Input: grid = [[1,1,1,1,1,1,1],
#                [1,0,0,0,0,0,1],
#                [1,0,1,1,1,0,1],
#                [1,0,1,0,1,0,1],
#                [1,0,1,1,1,0,1],
#                [1,0,0,0,0,0,1],
#                [1,1,1,1,1,1,1]]
# Output: 2
# 
# Constraints:
# 
# 	1 <= grid.length, grid[0].length <= 100
# 	0 <= grid[i][j] <=1
#####################################################################################################

from typing import List


class Solution:
    def closedIsland(self, grid: List[List[int]]) -> int:

        def dfs(i: int, j: int):
            if not 0 <= i < len(grid) or not 0 <= j < len(grid[0]) or grid[i][j] == 1:
                return
            grid[i][j] = 1
            for x, y in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
                dfs(i + x, j + y)

        # fill the boundaries first, these islands are not closed in
        for i in range(len(grid)):
            dfs(i, 0)
            dfs(i, len(grid[0]) - 1)
        for j in range(len(grid[0])):
            dfs(0, j)
            dfs(len(grid) - 1, j)
        # then we count the remainder, which must now be closed in
        n_islands = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 0:
                    dfs(i, j)
                    n_islands += 1
        return n_islands


def test():
    arguments = [
        [[1, 1, 1, 1, 1, 1, 1, 0],
         [1, 0, 0, 0, 0, 1, 1, 0],
         [1, 0, 1, 0, 1, 1, 1, 0],
         [1, 0, 0, 0, 0, 1, 0, 1],
         [1, 1, 1, 1, 1, 1, 1, 0]],

        [[0, 0, 1, 0, 0],
         [0, 1, 0, 1, 0],
         [0, 1, 1, 1, 0]],

        [[1, 1, 1, 1, 1, 1, 1],
         [1, 0, 0, 0, 0, 0, 1],
         [1, 0, 1, 1, 1, 0, 1],
         [1, 0, 1, 0, 1, 0, 1],
         [1, 0, 1, 1, 1, 0, 1],
         [1, 0, 0, 0, 0, 0, 1],
         [1, 1, 1, 1, 1, 1, 1]],

        [[0, 1, 1, 1, 0],
         [1, 0, 1, 0, 1],
         [1, 0, 1, 0, 1],
         [1, 0, 0, 0, 1],
         [0, 1, 1, 1, 0]],
        ]
    expectations = [2, 1, 2, 1]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().closedIsland(grid)
        assert solution == expected
