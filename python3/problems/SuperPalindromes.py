### Source : https://leetcode.com/problems/super-palindromes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-08

##################################################################################################### 
#
# Let's say a positive integer is a super-palindrome if it is a palindrome, and it is also the square 
# of a palindrome.
# 
# Given two positive integers left and right represented as strings, return the number of 
# super-palindromes integers in the inclusive range [left, right].
# 
# Example 1:
# 
# Input: left = "4", right = "1000"
# Output: 4
# Explanation: 4, 9, 121, and 484 are superpalindromes.
# Note that 676 is not a superpalindrome: 26 * 26 = 676, but 26 is not a palindrome.
# 
# Example 2:
# 
# Input: left = "1", right = "2"
# Output: 1
# 
# Constraints:
# 
# 	1 <= left.length, right.length <= 18
# 	left and right consist of only digits.
# 	left and right cannot have leading zeros.
# 	left and right represent integers in the range [1, 1018].
# 	left is less than or equal to right.
#####################################################################################################


class Solution:
    def superpalindromesInRange(self, left: str, right: str) -> int:
        import math

        def is_palindrome(n: int):
            return int(str(n)[::-1]) == n

        def backtracking(path):
            if 0 < len(path) and not path.startswith('0'):
                palindrome = int(path)
                root = palindrome ** 0.5
                if root.is_integer() and is_palindrome(int(root)):
                    paths.add(palindrome)
                square = palindrome ** 2
                if is_palindrome(square):
                    paths.add(square)
            if len(path) <= math.log10(upper_bound) // 2:  # e9 * e9 == e18
                for j in range(10):
                    backtracking(str(j) + path + str(j))

        paths, lower_bound, upper_bound = set(), int(left), int(right)
        backtracking('')
        for i in range(10):
            backtracking(str(i))

        # palindromes_in_range = list(filter(lambda x: int(left) <= x <= int(right), set(paths)))
        return sum(lower_bound <= n <= upper_bound for n in paths)

    def superpalindromesInRange(self, left: str, right: str) -> int:
        ctr, lower_bound, upper_bound = 0, int(left), int(right)
        for s in map(str, range(100_000)):
            for second_half in (s[-2::-1], s[::-1]):
                square = int(s + second_half) ** 2
                if square > upper_bound:
                    break
                ctr += square >= lower_bound and (int(str(square)[::-1]) == square)
        return ctr

    def superpalindromesInRange(self, left: str, right: str) -> int:
        # super palindromes exist of square roots that are palindromes containing only the digits 0, 1, 2 and 3
        # we can limit our search space to base 3. As per above, we only need to go to e9, which are
        def base3(n: int) -> str:
            nums = []
            while n:
                n, r = divmod(n, 3)
                nums.append(r)
            return ''.join(map(str, reversed(nums)))

        ctr = 1 if 9 >= int(left) and 9 <= int(right) else 0
        for i in range(1, int(str(1_000_000_000), 3)):
            num = base3(i)
            if num[::-1] == num:
                square = int(num) * int(num)
                if square > int(right):
                    return ctr
                ctr += square >= int(left) and (int(str(square)[::-1]) == square)
        return ctr


def test():
    arguments = [
        ("4", "1000"),
        ("1", "2"),
        ("40000000000000000", "50000000000000000"),
        ]
    expectations = [4, 1, 2]
    for (left, right), expected in zip(arguments, expectations):
        solution = Solution().superpalindromesInRange(left, right)
        assert solution == expected, (left, right, expected, solution)
