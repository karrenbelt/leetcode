### Source : https://leetcode.com/problems/valid-palindrome-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-30

##################################################################################################### 
#
# 
# Given a non-empty string s, you may delete at most one character.  Judge whether you can make it a 
# palindrome.
# 
# Example 1:
# 
# Input: "aba"
# Output: True
# 
# Example 2:
# 
# Input: "abca"
# Output: True
# Explanation: You could delete the character 'c'.
# 
# Note:
# 
# The string will only contain lowercase characters a-z.
# The maximum length of the string is 50000.
# 
#####################################################################################################


class Solution:
    def validPalindrome(self, s: str) -> bool:  # two pointer, O(n) time, O(1) space
        # two pointers, since we delete (not replace)
        left, right = 0, len(s) - 1
        while left < right:
            if s[left] != s[right]:
                one, two = s[left:right], s[left + 1:right + 1]
                return one == one[::-1] or two == two[::-1]
            left += 1
            right -= 1
        return True

    def validPalindrome(self, s: str) -> bool:  # more concise
        left, right = 0, len(s) - 1
        while left < right and s[left] == s[right]:
            left += 1
            right -= 1
        one, two = s[left:right], s[left + 1:right + 1]
        return one == one[::-1] or two == two[::-1]


def test():
    arguments = ["aba", "abca", "abbcca"]
    expectations = [True, True, False]
    for s, expected in zip(arguments, expectations):
        solution = Solution().validPalindrome(s)
        assert solution == expected
