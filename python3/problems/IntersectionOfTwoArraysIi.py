### Source : https://leetcode.com/problems/intersection-of-two-arrays-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given two arrays, write a function to compute their intersection.
# 
# Example 1:
# 
# Input: nums1 = [1,2,2,1], nums2 = [2,2]
# Output: [2,2]
# 
# Example 2:
# 
# Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
# Output: [4,9]
# 
# Note:
# 
# 	Each element in the result should appear as many times as it shows in both arrays.
# 	The result can be in any order.
# 
# Follow up:
# 
# 	What if the given array is already sorted? How would you optimize your algorithm?
# 	What if nums1's size is small compared to nums2's size? Which algorithm is better?
# 	What if elements of nums2 are stored on disk, and the memory is limited such that you 
# cannot load all elements into the memory at once?
# 
#####################################################################################################

from typing import List


class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        nums1.sort()
        nums2.sort()
        ans = []
        p1 = p2 = 0
        while p1 < len(nums1) and p2 < len(nums2):
            if nums1[p1] == nums2[p2]:
                ans.append(nums1[p1])
                p1 += 1
                p2 += 1
            elif nums1[p1] < nums2[p2]:
                p1 += 1
            else:
                p2 += 1

        return ans

    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        from collections import Counter
        return [n for n, count in (Counter(nums1) & Counter(nums2)).items() for _ in range(count)]


def test():
    from collections import Counter
    pairs_of_arrays_of_numbers = [
        ([1, 2, 2, 1], [2, 2]),
        ([4, 9, 5], [9, 4, 9, 8, 4]),
        ]
    expectations = [
        [2, 2],
        [4, 9],
        ]
    for (nums1, nums2), expected in zip(pairs_of_arrays_of_numbers, expectations):
        solution = Solution().intersect(nums1, nums2)
        assert Counter(solution) == Counter(expected)
