### Source : https://leetcode.com/problems/insert-into-a-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# You are given the root node of a binary search tree (BST) and a value to insert into the tree. 
# Return the root node of the BST after the insertion. It is guaranteed that the new value does not 
# exist in the original BST.
# 
# Notice that there may exist multiple valid ways for the insertion, as long as the tree remains a 
# BST after insertion. You can return any of them.
# 
# Example 1:
# 
# Input: root = [4,2,7,1,3], val = 5
# Output: [4,2,7,1,3,5]
# Explanation: Another accepted tree is:
# 
# Example 2:
# 
# Input: root = [40,20,60,10,30,50,70], val = 25
# Output: [40,20,60,10,30,50,70,null,null,25]
# 
# Example 3:
# 
# Input: root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
# Output: [4,2,7,1,3,5]
# 
# Constraints:
# 
# 	The number of nodes in the tree will be in the range [0, 104].
# 	-108 <= Node.val <= 108
# 	All the values Node.val are unique.
# 	-108 <= val <= 108
# 	It's guaranteed that val does not exist in the original BST.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def insertIntoBST(self, root: TreeNode, val: int) -> TreeNode:
        if not root:
            return TreeNode(val)
        node = prev = root
        while node:
            prev = node
            node = node.left if node.val > val else node.right
        if prev.val > val:
            prev.left = TreeNode(val)
        else:
            prev.right = TreeNode(val)
        return root

    def insertIntoBST(self, root: TreeNode, val: int) -> TreeNode:

        def insert(node: TreeNode) -> TreeNode:
            if not node:
                return TreeNode(val)
            elif node.val < val:
                node.right = insert(node.right)
            elif node.val > val:
                node.left = insert(node.left)
            return node

        return insert(root)


def test():
    from python3 import Codec
    arguments = [
        ("[4,2,7,1,3]", 5),
        ("[40,20,60,10,30,50,70]", 25),
        ("[4,2,7,1,3,null,null,null,null,null,null]", 5),
    ]
    expectations = [
        "[4,2,7,1,3,5]",
        "[40,20,60,10,30,50,70,null,null,25]",
        "[4,2,7,1,3,5]",
    ]
    for (serialized_tree, val), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().insertIntoBST(root, val)
        assert solution == Codec.deserialize(expected)
