### Source : https://leetcode.com/problems/closest-dessert-cost/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-22

##################################################################################################### 
#
# You would like to make dessert and are preparing to buy the ingredients. You have n ice cream base 
# flavors and m types of toppings to choose from. You must follow these rules when making your 
# dessert:
# 
# 	There must be exactly one ice cream base.
# 	You can add one or more types of topping or have no toppings at all.
# 	There are at most two of each type of topping.
# 
# You are given three inputs:
# 
# 	baseCosts, an integer array of length n, where each baseCosts[i] represents the price of 
# the ith ice cream base flavor.
# 	toppingCosts, an integer array of length m, where each toppingCosts[i] is the price of one 
# of the ith topping.
# 	target, an integer representing your target price for dessert.
# 
# You want to make a dessert with a total cost as close to target as possible.
# 
# Return the closest possible cost of the dessert to target. If there are multiple, return the lower 
# one.
# 
# Example 1:
# 
# Input: baseCosts = [1,7], toppingCosts = [3,4], target = 10
# Output: 10
# Explanation: Consider the following combination (all 0-indexed):
# - Choose base 1: cost 7
# - Take 1 of topping 0: cost 1 x 3 = 3
# - Take 0 of topping 1: cost 0 x 4 = 0
# Total: 7 + 3 + 0 = 10.
# 
# Example 2:
# 
# Input: baseCosts = [2,3], toppingCosts = [4,5,100], target = 18
# Output: 17
# Explanation: Consider the following combination (all 0-indexed):
# - Choose base 1: cost 3
# - Take 1 of topping 0: cost 1 x 4 = 4
# - Take 2 of topping 1: cost 2 x 5 = 10
# - Take 0 of topping 2: cost 0 x 100 = 0
# Total: 3 + 4 + 10 + 0 = 17. You cannot make a dessert with a total cost of 18.
# 
# Example 3:
# 
# Input: baseCosts = [3,10], toppingCosts = [2,5], target = 9
# Output: 8
# Explanation: It is possible to make desserts with cost 8 and 10. Return 8 as it is the lower cost.
# 
# Example 4:
# 
# Input: baseCosts = [10], toppingCosts = [1], target = 1
# Output: 10
# Explanation: Notice that you don't have to have any toppings, but you must have exactly one base.
# 
# Constraints:
# 
# 	n == baseCosts.length
# 	m == toppingCosts.length
# 	1 <= n, m <= 10
# 	1 <= baseCosts[i], toppingCosts[i] <= 104
# 	1 <= target <= 104
#####################################################################################################

from typing import List


class Solution:
    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:  # O(n * m^3) time
        # 404 ms
        def backtracking(i: int, cost: int):  # O(m^3) time and O(3^n) space for the recursion stack
            nonlocal closest
            if abs(target - cost) < abs(target - closest) or abs(target - cost) == abs(target - closest) and cost < closest:
                closest = cost
            if i < len(toppingCosts) and cost < target:
                backtracking(i + 1, cost)
                backtracking(i + 1, cost + toppingCosts[i])
                backtracking(i + 1, cost + toppingCosts[i] * 2)

        closest = baseCosts[0]
        for base in baseCosts:  # O(n) time
            backtracking(0, base)
        return closest

    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:
        # 48ms
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, cost: int) -> int:
            if cost > target or i == len(toppingCosts):
                return cost
            return min((dfs(i + 1, cost + toppingCosts[i] * j) for j in range(3)), key=closest)

        closest = lambda x: (abs(target - x), x)
        return min((dfs(0, base) for base in baseCosts), key=closest)

    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:
        top_combo = {0}
        for x in toppingCosts:
            for y in set(top_combo):
                top_combo.add(y + x)
                top_combo.add(y + 2 * x)

        ans = dif = 2**32 - 1
        for x in baseCosts:
            for y in top_combo:
                cost = x + y
                diff = abs(target - cost)
                if diff < dif:
                    ans, dif = cost, diff
                elif diff == dif and cost < ans:
                    ans, dif = cost, diff

        return ans

    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:
        # 268 ms
        top_combo, closest, diff = {0}, 2**32 - 1, 2**32 - 1
        any(top_combo.update({y + x, y + 2 * x}) for x in toppingCosts for y in set(top_combo))  # call set to copy
        for cost in (x + y for x in baseCosts for y in top_combo):
            if abs(target - cost) < diff or abs(target - cost) == diff and cost < closest:
                closest, diff = cost, abs(target - cost)
        return closest

    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:  # O(n * 2^(2n) + mn) time
        # 92ms
        import bisect

        top_combo = {0}
        for cost in toppingCosts * 2:
            top_combo |= {cost + x for x in top_combo}
        top_combo = sorted(top_combo)

        ans = 2**32 - 1
        for base in baseCosts:
            i = bisect.bisect_left(top_combo, target - base)
            if i < len(top_combo):
                ans = min(ans, base + top_combo[i], key=lambda x: (abs(x - target), x))
            if i:  # i did not think of this condition
                ans = min(ans, base + top_combo[i - 1], key=lambda x: (abs(x - target), x))
        return ans


def test():
    arguments = [
        ([1, 7], [3, 4], 10),
        ([2, 3], [4, 5, 100], 18),
        ([3, 10], [2, 5], 9),
        ([10], [1], 1),
    ]
    expectations = [10, 17, 8, 10]
    for (baseCosts, toppingCosts, target), expected in zip(arguments, expectations):
        solution = Solution().closestCost(baseCosts, toppingCosts, target)
        assert solution == expected, (expected, solution)
