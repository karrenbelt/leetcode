### Source : https://leetcode.com/problems/integer-to-english-words/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-27

##################################################################################################### 
#
# Convert a non-negative integer num to its English words representation.
# 
# Example 1:
# Input: num = 123
# Output: "One Hundred Twenty Three"
# Example 2:
# Input: num = 12345
# Output: "Twelve Thousand Three Hundred Forty Five"
# Example 3:
# Input: num = 1234567
# Output: "One illion Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
# Example 4:
# Input: num = 1234567891
# Output: "One Billion Two Hundred Thirty Four illion Five Hundred Sixty Seven Thousand Eight 
# Hundred Ninety One"
# 
# Constraints:
# 
# 	0 <= num <= 231 - 1
#####################################################################################################


class Solution:
    def numberToWords(self, num: int) -> str:
        if num == 0:
            return "Zero"

        words = {
            1000000000: "Billion", 1000000: "Million",
            1000: "Thousand", 100: "Hundred", 90: "Ninety", 80: "Eighty",
            70: "Seventy", 60: "Sixty", 50: "Fifty", 40: "Forty", 30: "Thirty",
            20: "Twenty", 19: "Nineteen", 18: "Eighteen", 17: "Seventeen",
            16: "Sixteen", 15: "Fifteen", 14: "Fourteen", 13: "Thirteen",
            12: "Twelve", 11: "Eleven", 10: "Ten", 9: "Nine", 8: "Eight",
            7: "Seven", 6: "Six", 5: "Five", 4: "Four", 3: "Three", 2: "Two", 1: "One"
        }
        nums, english = [1000000000, 1000000, 1000, 1], ""
        for i in nums:
            val = num // i
            num = num - val * i
            hundreds, tens, ones, temp = val // 100, (val % 100) // 10, val % 10, ""
            if hundreds > 0:
                temp += words[hundreds] + " Hundred "
            if tens >= 2:
                temp += words[tens * 10] + " "
                if ones > 0:
                    temp += words[ones] + " "
            elif tens > 0 or ones > 0:
                temp += words[tens * 10 + ones] + " "
            if val > 0 and i != 1:
                temp += words[i] + " "
            english += temp
        return english.strip()


def test():
    arguments = [123, 12345, 1234567, 1234567891, 20, 50868]
    expectations = [
        "One Hundred Twenty Three",
        "Twelve Thousand Three Hundred Forty Five",
        "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
        "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One",
        "Twenty",
        "Fifty Thousand Eight Hundred Sixty Eight",
    ]
    for num, expected in zip(arguments, expectations):
        solution = Solution().numberToWords(num)
        assert solution == expected
