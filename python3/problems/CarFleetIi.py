### Source : https://leetcode.com/problems/car-fleet-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-29

##################################################################################################### 
#
# There are n cars traveling at different speeds in the same direction along a one-lane road. You are 
# given an array cars of length n, where cars[i] = [positioni, speedi] represents:
# 
# 	positioni is the distance between the ith car and the beginning of the road in meters. It 
# is guaranteed that positioni < positioni+1.
# 	speedi is the initial speed of the ith car in meters per second.
# 
# For simplicity, cars can be considered as points moving along the number line. Two cars collide 
# when they occupy the same position. Once a car collides with another car, they unite and form a 
# single car fleet. The cars in the formed fleet will have the same position and the same speed, 
# which is the initial speed of the slowest car in the fleet.
# 
# Return an array answer, where answer[i] is the time, in seconds, at which the ith car collides with 
# the next car, or -1 if the car does not collide with the next car. Answers within 10-5 of the 
# actual answers are accepted.
# 
# Example 1:
# 
# Input: cars = [[1,2],[2,1],[4,3],[7,2]]
# Output: [1.00000,-1.00000,3.00000,-1.00000]
# Explanation: After exactly one second, the first car will collide with the second car, and form a 
# car fleet with speed 1 m/s. After exactly 3 seconds, the third car will collide with the fourth 
# car, and form a car fleet with speed 2 m/s.
# 
# Example 2:
# 
# Input: cars = [[3,4],[5,4],[6,3],[9,1]]
# Output: [2.00000,1.00000,1.50000,-1.00000]
# 
# Constraints:
# 
# 	1 <= cars.length <= 105
# 	1 <= positioni, speedi <= 106
# 	positioni < positioni+1
#####################################################################################################

from typing import List


class Solution:
    def getCollisionTimes(self, cars: List[List[int]]) -> List[float]:
        import heapq

        heap = []
        for i, ((position_a, speed_a), (position_b, speed_b)) in enumerate(zip(cars, cars[1:])):
            time = (position_b - position_a) / (speed_a - speed_b) if speed_a > speed_b else float('inf')
            heapq.heappush(heap, (time, i, i + 1))

        predecessor, successor = map(list, zip(*((i - 1, i + 1) for i in range(len(cars)))))
        collision_times = [-1.0] * len(cars)
        while heap:
            time, i, next_car = heapq.heappop(heap)
            if successor[i] != next_car:
                continue
            if time < float('inf'):  # collision happens and need to update the "linkage" and potential collisions
                collision_times[i] = time
                predecessor[next_car] = predecessor[i]
                successor[predecessor[i]] = next_car
                (position_a, speed_a), (position_b, speed_b) = cars[predecessor[i]], cars[next_car]
                if predecessor[i] >= 0 and speed_a > speed_b:  # insert the potential collision for car "prev"
                    time = (position_b - position_a) / (speed_a - speed_b)
                    heapq.heappush(heap, (time, predecessor[i], next_car))

        return collision_times


def test():
    arguments = [
        [[1, 2], [2, 1], [4, 3], [7, 2]],
        [[3, 4], [5, 4], [6, 3], [9, 1]],
    ]
    expectations = [
        [1.00000, -1.00000, 3.00000, -1.00000],
        [2.00000, 1.00000, 1.50000, -1.00000],
    ]
    for cars, expected in zip(arguments, expectations):
        solution = Solution().getCollisionTimes(cars)
        assert solution == expected, (solution, expected)
