### Source : https://leetcode.com/problems/path-sum/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up 
# all the values along the path equals the given sum.
# 
# Note: A leaf is a node with no children.
# 
# Example:
# 
# Given the below binary tree and sum = 22,
# 
#       5
#      / \
#     4   8
#    /   / \
#   11  13  4
#  /  \      \
# 7    2      1
# 
# return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def hasPathSum(self, root: TreeNode, target: int) -> bool:  # O(n) time and O(h) space

        def check(node, total):
            if not node:
                return False
            total += node.val
            if not node.left and not node.right and total == target:
                return True
            return False or (check(node.left, total) or check(node.right, total))

        return check(root, 0)

    def hasPathSum(self, root: TreeNode, target: int) -> bool:  # O(n) time and O(h) space

        def check(node, total):
            if node and not node.left and not node.right and target == total + node.val:
                return True
            return False if not node else (check(node.left, total + node.val) or check(node.right, total + node.val))

        return check(root, 0)

    def hasPathSum(self, root: TreeNode, target: int) -> bool:  # O(n) time and O(h) space
        stack = [(root, 0)]
        while stack:
            node, total = stack.pop()
            if node:
                total += node.val
                if node.left is node.right and total == target:
                    return True
                stack.extend([(node.left, total), (node.right, total)])
        return False


def test():
    from python3 import Codec
    deserialized_trees = [
        "[5,4,8,11,null,13,4,7,2,null,null,null,1]",
        "[1,2,3]",
        "[1,2]"
        "[]"
        ]
    targets = [22, 5, 0, 0]
    expectations = [True, False, False, False]
    for deserialized_tree, target, expected in zip(deserialized_trees, targets, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().hasPathSum(root, target)
        assert solution == expected
