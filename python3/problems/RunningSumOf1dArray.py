### Source : https://leetcode.com/problems/running-sum-of-1d-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-03

##################################################################################################### 
#
# Given an array nums. We define a running sum of an array as runningSum[i] = 
# sum(nums[0]&hellip;nums[i]).
# 
# Return the running sum of nums.
# 
# Example 1:
# 
# Input: nums = [1,2,3,4]
# Output: [1,3,6,10]
# Explanation: Running sum is obtained as follows: [1, 1+2, 1+2+3, 1+2+3+4].
# 
# Example 2:
# 
# Input: nums = [1,1,1,1,1]
# Output: [1,2,3,4,5]
# Explanation: Running sum is obtained as follows: [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1].
# 
# Example 3:
# 
# Input: nums = [3,1,2,10,1]
# Output: [3,4,6,16,17]
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	-106 <= nums[i] <= 106
#####################################################################################################

from typing import List


class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        for i in range(1, len(nums)):  # prefix sum
            nums[i] += nums[i - 1]
        return nums

    def runningSum(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        from itertools import accumulate
        return list(accumulate(nums))


def test():
    arguments = [
        [1, 2, 3, 4],
        [1, 1, 1, 1, 1],
        [3, 1, 2, 10, 1],
        ]
    expectations = [
        [1, 3, 6, 10],
        [1, 2, 3, 4, 5],
        [3, 4, 6, 16, 17],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().runningSum(nums)
        assert solution == expected
