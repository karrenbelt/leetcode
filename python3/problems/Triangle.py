### Source : https://leetcode.com/problems/triangle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given a triangle array, return the minimum path sum from top to bottom.
# 
# For each step, you may move to an adjacent number of the row below. ore formally, if you are on 
# index i on the current row, you may move to either index i or index i + 1 on the next row.
# 
# Example 1:
# 
# Input: triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
# Output: 11
# Explanation: The triangle looks like:
#    2
#   3 4
#  6 5 7
# 4 1 8 3
# The minimum path sum from top to bottom is 2 + 3 + 5 + 1 = 11 (underlined above).
# 
# Example 2:
# 
# Input: triangle = [[-10]]
# Output: -10
# 
# Constraints:
# 
# 	1 <= triangle.length <= 200
# 	triangle[0].length == 1
# 	triangle[i].length == triangle[i - 1].length + 1
# 	-104 <= triangle[i][j] <= 104
# 
# Follow up: Could you do this using only O(n) extra space, where n is the total number of rows in 
# the triangle?
#####################################################################################################

from typing import List


class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:  # dp: O(n^2 ) time and O(n^2) space: TLE

        def dfs(i: int, j: int) -> int:
            return 0 if i == len(triangle) else triangle[i][j] + min(dfs(i + 1, j), dfs(i + 1, j + 1))

        return dfs(0, 0)

    def minimumTotal(self, triangle: List[List[int]]) -> int:  # dp: O(n) time and O(n) space

        def dfs(i: int, j: int) -> int:
            if i == len(triangle):
                return 0
            if (i, j) not in memo:
                memo[i, j] = triangle[i][j] + min(dfs(i + 1, j), dfs(i + 1, j + 1))
            return memo[i, j]

        memo = {}
        return dfs(0, 0)

    def minimumTotal(self, triangle: List[List[int]]) -> int:  # dp: O(n) time and O(n)
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> int:
            return 0 if i == len(triangle) else triangle[i][j] + min(dfs(i + 1, j), dfs(i + 1, j + 1))

        return dfs(0, 0)

    def minimumTotal(self, triangle: List[List[int]]) -> int:  # dp: O(n) time and O(1) space
        for i in range(len(triangle) - 2, -1, -1):
            for j in range(len(triangle[i])):
                triangle[i][j] += min(triangle[i + 1][j], triangle[i + 1][j + 1])
        return triangle[0][0]


def test():
    arguments = [
        [[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]],
        [[-10]],
    ]
    expectations = [11, -10]
    for triangle, expected in zip(arguments, expectations):
        solution = Solution().minimumTotal(triangle)
        assert solution == expected
