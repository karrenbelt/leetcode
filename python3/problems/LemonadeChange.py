### Source : https://leetcode.com/problems/lemonade-change/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-22

##################################################################################################### 
#
# At a lemonade stand, each lemonade costs $5. Customers are standing in a queue to buy from you, and 
# order one at a time (in the order specified by bills). Each customer will only buy one lemonade and 
# pay with either a $5, $10, or $20 bill. You must provide the correct change to each customer so 
# that the net transaction is that the customer pays $5.
# 
# Note that you don't have any change in hand at first.
# 
# Given an integer array bills where bills[i] is the bill the ith customer pays, return true if you 
# can provide every customer with correct change, or false otherwise.
# 
# Example 1:
# 
# Input: bills = [5,5,5,10,20]
# Output: true
# Explanation: 
# From the first 3 customers, we collect three $5 bills in order.
# From the fourth customer, we collect a $10 bill and give back a $5.
# From the fifth customer, we give a $10 bill and a $5 bill.
# Since all customers got correct change, we output true.
# 
# Example 2:
# 
# Input: bills = [5,5,10,10,20]
# Output: false
# Explanation: 
# From the first two customers in order, we collect two $5 bills.
# For the next two customers in order, we collect a $10 bill and give back a $5 bill.
# For the last customer, we can not give change of $15 back because we only have two $10 bills.
# Since not every customer received correct change, the answer is false.
# 
# Example 3:
# 
# Input: bills = [5,5,10]
# Output: true
# 
# Example 4:
# 
# Input: bills = [10,10]
# Output: false
# 
# Constraints:
# 
# 	1 <= bills.length <= 105
# 	bills[i] is either 5, 10, or 20.
#####################################################################################################

from typing import List


class Solution:
    def lemonadeChange(self, bills: List[int]) -> bool:  # O(n) time and O(1) space
        wallet = {5: 0, 10: 0, 20: 0}
        for n in bills:
            if n == 5:
                pass
            elif n == 10 and wallet[5]:
                wallet[5] -= 1
            elif n == 20 and (wallet[10] and wallet[5] or wallet[5] > 2):
                if wallet[10]:
                    wallet[10] -= 1
                    wallet[5] -= 1
                else:
                    wallet[5] -= 3
            else:
                return False
            wallet[n] += 1
        return True

    def lemonadeChange(self, bills: List[int]) -> bool:  # O(n) time and O(1) space
        five = ten = 0
        for n in bills:
            if n == 5:
                five += 1
            elif n == 10 and five:
                five, ten = five - 1, ten + 1
            elif n == 20 and five and ten:
                five, ten = five - 1, ten - 1
            elif n == 20 and five > 2:
                five -= 3
            else:
                return False
        return True

    def lemonadeChange(self, bills: List[int]) -> bool:  # O(n) time and O(1) space
        five = ten = 0
        for i in bills:
            if i == 5:
                five += 1
            elif i == 10:
                five, ten = five - 1, ten + 1
            elif ten > 0:
                five, ten = five - 1, ten - 1
            else:
                five -= 3
            if five < 0:
                return False
        return True


def test():
    arguments = [
        [5, 5, 5, 10, 20],
        [5, 5, 10, 10, 20],
        [5, 5, 10],
        [10, 10],
        [5, 5, 5, 5, 20, 20, 5, 5, 20, 5],
        [5, 5, 10, 5, 20, 5, 5, 5, 5, 5, 20, 5, 10, 5, 5, 5, 5, 20, 20, 5],
    ]
    expectations = [True, False, True, False, False, True]
    for bills, expected in zip(arguments, expectations):
        solution = Solution().lemonadeChange(bills)
        assert solution == expected
