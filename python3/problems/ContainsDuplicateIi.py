### Source : https://leetcode.com/problems/contains-duplicate-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-04

##################################################################################################### 
#
# Given an array of integers and an integer k, find out whether there are two distinct indices i and 
# j in the array such that nums[i] = nums[j] and the absolute difference between i and j is at most k.
# 
# Example 1:
# 
# Input: nums = [1,2,3,1], k = 3
# Output: true
# 
# Example 2:
# 
# Input: nums = [1,0,1,1], k = 1
# Output: true
# 
# Example 3:
# 
# Input: nums = [1,2,3,1,2,3], k = 2
# Output: false
# 
#####################################################################################################

from typing import List


class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:  # O(n ^ 2) -> TLE
        for i, n in enumerate(nums):
            if n in set(nums[i + 1:i + k + 1]):
                return True
        return False

    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:  # O(n)
        d = {}
        for i, v in enumerate(nums):
            if v in d and i - d[v] <= k:
                return True
            d[v] = i
        return False

    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:  # O(n)
        if len(nums) <= k:
            return len(nums) > len(set(nums))

        s = set(nums[:k])
        if len(s) < k:
            return True

        for i in range(k, len(nums)):
            s.add(nums[i])
            if len(s) == k:
                return True
            s.remove(nums[i - k])
        return False


def test():
    arrays_of_numbers = [
        ([1, 2, 3, 1], 3),
        ([1, 0, 1, 1], 1),
        ([1, 2, 3, 1, 2, 3], 2),
        ]
    expectations = [True, True, False]
    for (nums, k), expected in zip(arrays_of_numbers, expectations):
        solution = Solution().containsNearbyDuplicate(nums, k)
        assert solution == expected
