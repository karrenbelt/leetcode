### Source : https://leetcode.com/problems/maximum-average-subarray-i/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an array consisting of n integers, find the contiguous subarray of given length k that has 
# the maximum average value. And you need to output the maximum average value.
# 
# Example 1:
# 
# Input: [1,12,-5,-6,50,3], k = 4
# Output: 12.75
# Explanation: aximum average is (12-5-6+50)/4 = 51/4 = 12.75
# 
# Note:
# 
# 	1 <= k <= n <= 30,000.
# 	Elements of the given array will be in the range [-10,000, 10,000].
# 
#####################################################################################################
from typing import List


class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:  # O(n) time and O(n) space
        prefix_sum = [0] + nums
        for i in range(1, len(prefix_sum)):
            prefix_sum[i] += prefix_sum[i - 1]
        return max(prefix_sum[i + k] - prefix_sum[i] for i in range(len(prefix_sum) - k)) / k

    def findMaxAverage(self, nums: List[int], k: int) -> float:  # O(n) time and O(1) space
        window_sum = sum(nums[:k])
        maximum = window_sum
        for i in range(k, len(nums)):
            window_sum += nums[i] - nums[i - k]
            if maximum < window_sum:
                maximum = window_sum
        return maximum / k

def test():
    arguments = [
        ([1, 12, -5, -6, 50, 3], 4),
    ]
    expectations = [12.75]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().findMaxAverage(nums, k)
        assert solution == expected
