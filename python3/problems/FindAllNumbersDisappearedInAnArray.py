### Source : https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-12

##################################################################################################### 
#
# Given an array of integers where 1 &le; a[i] &le; n (n = size of array), some elements appear twice 
# and others appear once.
# 
# Find all the elements of [1, n] inclusive that do not appear in this array.
# 
# Could you do it without extra space and in O(n) runtime? You may assume the returned list does not 
# count as extra space.
# 
# Example:
# 
# Input:
# [4,3,2,7,8,2,3,1]
# 
# Output:
# [5,6]
# 
#####################################################################################################

from typing import List


class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        return list(set(range(1, len(nums) + 1)).difference(nums))

    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        for n in nums:
            i = abs(n) - 1
            nums[i] = -abs(nums[i])
        return [i + 1 for i, n in enumerate(nums) if n > 0]


def test():
    arguments = [
        [4, 3, 2, 7, 8, 2, 3, 1],
        [2, 1],
        ]
    expectations = [
        [5, 6],
        [],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findDisappearedNumbers(nums)
        assert solution == expected
