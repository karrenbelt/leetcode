### Source : https://leetcode.com/problems/couples-holding-hands/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-07

##################################################################################################### 
#
# There are n couples sitting in 2n seats arranged in a row and want to hold hands.
# 
# The people and seats are represented by an integer array row where row[i] is the ID of the person 
# sitting in the ith seat. The couples are numbered in order, the first couple being (0, 1), the 
# second couple being (2, 3), and so on with the last couple being (2n - 2, 2n - 1).
# 
# Return the minimum number of swaps so that every couple is sitting side by side. A swap consists of 
# choosing any two people, then they stand up and switch seats.
# 
# Example 1:
# 
# Input: row = [0,2,1,3]
# Output: 1
# Explanation: We only need to swap the second (row[1]) and third (row[2]) person.
# 
# Example 2:
# 
# Input: row = [3,2,0,1]
# Output: 0
# Explanation: All couples are already seated side by side.
# 
# Constraints:
# 
# 	2n == row.length
# 	2 <= n <= 30
# 	n is even.
# 	0 <= row[i] < 2n
# 	All the elements of row are unique.
#####################################################################################################

from typing import List


class Solution:
    def minSwapsCouples(self, row: List[int]) -> int:
        # ideas: merge sort with counting inversion swaps, or comparing the original to a sorted array somehow

        # while (diff := [abs(x - y) for x, y in zip(row, row[1:])]) and any(d > 1 for d in diff):
        #     row.sort()

        return


def test():
    arguments = [
        [0, 2, 1, 3],
        [3, 2, 0, 1],
    ]
    expectations = [1, 0]
    for row, expected in zip(arguments, expectations):
        solution = Solution().minSwapsCouples(row)
        assert solution == expected
