### Source : https://leetcode.com/problems/squares-of-a-sorted-array/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given an array of integers A sorted in non-decreasing order, return an array of the squares of each 
# number, also in sorted non-decreasing order.
# 
# Example 1:
# 
# Input: [-4,-1,0,3,10]
# Output: [0,1,9,16,100]
# 
# Example 2:
# 
# Input: [-7,-3,2,3,11]
# Output: [4,9,9,49,121]
# 
# Note:
# 
# 	1 <= A.length <= 10000
# 	-10000 <= A[i] <= 10000
# 	A is sorted in non-decreasing order.
# 
#####################################################################################################

from typing import List


class Solution:
    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n log n) time
        return sorted(x ** 2 for x in A)

    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n log n) time
        for i in range(len(A)):
            A[i] *= A[i]
        A.sort()
        return A

    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n) time
        from collections import deque
        result, queue = [], deque(A)
        while queue:
            if abs(queue[0]) > queue[-1]:
                result.append(queue.popleft()**2)
            else:
                result.append(queue.pop()**2)
        return result[::-1]

    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n) time
        from collections import deque
        result, queue = deque(), deque(A)
        while queue:
            result.appendleft((queue.popleft() if abs(queue[0]) > queue[-1] else queue.pop())**2)
        return result

    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n) time
        result = []
        left, right = 0, len(A) - 1
        while left <= right:
            if abs(A[left]) > A[right]:
                result.append(A[left] ** 2)
                left += 1
            else:
                result.append(A[right] ** 2)
                right -= 1
        return reversed(result)

    def sortedSquares(self, A: List[int]) -> List[int]:  # O(n) time
        left, right, answer = 0, len(A) - 1, [0] * len(A)
        while left <= right:
            left_val, right_val = abs(A[left]), abs(A[right])
            if left_val > right_val:
                answer[right - left] = left_val * left_val
                left += 1
            else:
                answer[right - left] = right_val * right_val
                right -= 1
        return answer

    def sortedSquares(self, A):  # O(n) time
        left, right, answer = 0, len(A) - 1, [0] * len(A)
        while left <= right:
            left_val, right_val = abs(A[left]), abs(A[right])
            answer[right - left] = max(left_val, right_val) ** 2
            left, right = left + (left_val > right_val), right - (left_val <= right_val)
        return answer


def test():
    arguments = [
        [-4, -1, 0, 3, 10],
        [-7, -3, 2, 3, 11],
    ]
    expectations = [
        [0, 1, 9, 16, 100],
        [4, 9, 9, 49, 121],
    ]
    for A, expected in zip(arguments, expectations):
        solution = Solution().sortedSquares(A)
        assert solution == expected
