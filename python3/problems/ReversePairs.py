### Source : https://leetcode.com/problems/reverse-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-26

##################################################################################################### 
#
# Given an integer array nums, return the number of reverse pairs in the array.
# 
# A reverse pair is a pair (i, j) where 0 <= i < j < nums.length and nums[i] > 2 * nums[j].
# 
# Example 1:
# Input: nums = [1,3,2,3,1]
# Output: 2
# Example 2:
# Input: nums = [2,4,3,5,1]
# Output: 3
# 
# Constraints:
# 
# 	1 <= nums.length <= 5 * 104
# 	-231 <= nums[i] <= 231 - 1
#####################################################################################################

from typing import List


class BinaryIndexTree:
    def __init__(self, n):
        self.n = n + 1
        self.sums = [0] * self.n

    def update(self, i, delta):
        while i < self.n:
            self.sums[i] += delta
            i += i & (-i)

    def query(self, i):
        res = 0
        while i > 0:
            res += self.sums[i]
            i -= i & (-i)
        return res


class Solution:
    def reversePairs(self, nums: List[int]) -> int:  # O(n^2) time -> should yield TLE
        return sum(nums[i] > 2 * nums[j] for i in range(len(nums)) for j in range(i + 1, len(nums)))

    def reversePairs(self, nums: List[int]) -> int:  # O(n log n) time

        def merge_sort(left, right):
            if left >= right:
                return 0
            mid = left + (right - left) // 2
            ctr = merge_sort(left, mid) + merge_sort(mid + 1, right)

            j = mid + 1
            for i in range(left, mid + 1):
                while j <= right and nums[i] > 2 * nums[j]:
                    j += 1
                ctr += j - mid - 1

            nums[left:right + 1] = sorted(nums[left:right + 1])
            return ctr

        return merge_sort(0, len(nums) - 1)

    def reversePairs(self, nums: List[int]) -> int:  # O(n log n) time
        new_nums = nums + [n * 2 for n in nums]
        sorted_set = sorted(list(set(new_nums)))
        tree = BinaryIndexTree(len(sorted_set))
        ctr, ranks = 0, {}
        for i, n in enumerate(sorted_set):
            ranks[n] = i + 1

        for n in nums[::-1]:
            ctr += tree.query(ranks[n] - 1)
            tree.update(ranks[n * 2], 1)

        return ctr


def test():
    arguments = [
        [1, 3, 2, 3, 1],
        [2, 4, 3, 5, 1],
    ]
    expectations = [2, 3]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().reversePairs(nums)
        assert solution == expected
test()