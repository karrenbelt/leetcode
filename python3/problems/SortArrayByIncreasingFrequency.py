### Source : https://leetcode.com/problems/sort-array-by-increasing-frequency/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-26

##################################################################################################### 
#
# Given an array of integers nums, sort the array in increasing order based on the frequency of the 
# values. If multiple values have the same frequency, sort them in decreasing order.
# 
# Return the sorted array.
# 
# Example 1:
# 
# Input: nums = [1,1,2,2,2,3]
# Output: [3,1,1,2,2,2]
# Explanation: '3' has a frequency of 1, '1' has a frequency of 2, and '2' has a frequency of 3.
# 
# Example 2:
# 
# Input: nums = [2,3,1,3,2]
# Output: [1,3,3,2,2]
# Explanation: '2' and '3' both have a frequency of 2, so they are sorted in decreasing order.
# 
# Example 3:
# 
# Input: nums = [-1,1,-6,4,5,-6,1,4,1]
# Output: [5,-1,4,4,-6,-6,1,1,1]
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	-100 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def frequencySort(self, nums: List[int]) -> List[int]:  # O(n^2) time and O(1) space (could sort in-place as well)
        return sorted(nums, key=lambda x: (nums.count(x), -x))

    def frequencySort(self, nums: List[int]) -> List[int]:  # O(n log n) time and O(n) space
        counts = {}
        for n in nums:
            counts[n] = counts.get(n, 0) + 1
        return sum([[k] * v for k, v in sorted(counts.items(), key=lambda x: (x[1], -x[0]))], [])


def test():
    arguments = [
        [1, 1, 2, 2, 2, 3],
        [2, 3, 1, 3, 2],
        [-1, 1, -6, 4, 5, -6, 1, 4, 1],
        ]
    expectations = [
        [3, 1, 1, 2, 2, 2],
        [1, 3, 3, 2, 2],
        [5, -1, 4, 4, -6, -6, 1, 1, 1],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().frequencySort(nums)
        assert solution == expected
