### Source : https://leetcode.com/problems/knight-probability-in-chessboard/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# On an NxN chessboard, a knight starts at the r-th row and c-th column and attempts to make exactly 
# K moves. The rows and columns are 0 indexed, so the top-left square is (0, 0), and the bottom-right 
# square is (N-1, N-1).
# 
# A chess knight has 8 possible moves it can make, as illustrated below. Each move is two squares in 
# a cardinal direction, then one square in an orthogonal direction.
# 
# Each time the knight is to move, it chooses one of eight possible moves uniformly at random (even 
# if the piece would go off the chessboard) and moves there.
# 
# The knight continues moving until it has made exactly K moves or has moved off the chessboard. 
# Return the probability that the knight remains on the board after it has stopped moving.
# 
# Example:
# 
# Input: 3, 2, 0, 0
# Output: 0.0625
# Explanation: There are two moves (to (1,2), (2,1)) that will keep the knight on the board.
# From each of those positions, there are also two moves that will keep the knight on the board.
# The total probability the knight stays on the board is 0.0625.
# 
# Note:
# 
# 	N will be between 1 and 25.
# 	K will be between 0 and 100.
# 	The knight always initially starts on the board.
# 
#####################################################################################################


class Solution:
    def knightProbability(self, N: int, K: int, r: int, c: int) -> float:  # TLE

        def dfs(i, j, level):
            if not 0 <= i < N or not 0 <= j < N:
                return 0
            if level == 0:
                return 1
            return sum(0.125 * dfs(i + x, j + y, level - 1) for y, x in directions)

        directions = [(1, 2), (2, 1), (-1, 2), (2, -1), (1, -2), (-2, 1), (-1, -2), (-2, -1)]
        probability = dfs(r, c, K)
        return probability

    def knightProbability(self, N: int, K: int, r: int, c: int) -> float:  # O(8^K) time and O(N^2) space

        def dfs(i, j, level):
            if not 0 <= i < N or not 0 <= j < N:
                return 0
            if level == 0:
                return 1
            if (i, j, level) in cache:
                return cache[(i, j, level)]
            cache[(i, j, level)] = sum(0.125 * dfs(i + x, j + y, level - 1) for y, x in directions)
            return cache[(i, j, level)]

        cache = {}
        directions = [(1, 2), (2, 1), (-1, 2), (2, -1), (1, -2), (-2, 1), (-1, -2), (-2, -1)]
        probability = dfs(r, c, K)
        return probability

    def knightProbability(self, N: int, K: int, r: int, c: int) -> float:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i, j, level):
            if not 0 <= i < N or not 0 <= j < N:
                return 0
            if level == 0:
                return 1
            return sum(dfs(i + x, j + y, level - 1) for y, x in directions)

        directions = [(1, 2), (2, 1), (-1, 2), (2, -1), (1, -2), (-2, 1), (-1, -2), (-2, -1)]
        return dfs(r, c, K) / 8 ** K

    def knightProbability(self, N: int, K: int, r: int, c: int) -> float:  # dp: O(KN^2) time O(KN^2) space
        p = {(r, c): 1}
        for _ in range(K):
            p = {(r, c): sum(p.get((r + i, c + j), 0) + p.get((r + j, c + i), 0) for i in (1, -1) for j in (2, -2)) / 8
                 for r in range(N) for c in range(N)}
        return sum(p.values())


def test():
    quadruplet_numbers = [
        (3, 2, 0, 0),
        (3, 3, 0, 0),
        (4, 2, 1, 1),
        (8, 30, 6, 4),  # TLE case
        ]
    expectations = [
        0.0625,
        0.015625,
        0.15625,
        0.0001905256629833365,
        ]
    for args, expected in zip(quadruplet_numbers, expectations):
        solution = Solution().knightProbability(*args)
        assert round(solution, 10) == round(expected, 10)
