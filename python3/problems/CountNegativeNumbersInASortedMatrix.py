### Source : https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given a m x n matrix grid which is sorted in non-increasing order both row-wise and column-wise, 
# return the number of negative numbers in grid.
# 
# Example 1:
# 
# Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
# Output: 8
# Explanation: There are 8 negatives number in the matrix.
# 
# Example 2:
# 
# Input: grid = [[3,2],[1,0]]
# Output: 0
# 
# Example 3:
# 
# Input: grid = [[1,-1],[-1,-1]]
# Output: 3
# 
# Example 4:
# 
# Input: grid = [[-1]]
# Output: 1
# 
# Constraints:
# 
# 	m == grid.length
# 	n == grid[i].length
# 	1 <= m, n <= 100
# 	-100 <= grid[i][j] <= 100
# 
# Follow up: Could you find an O(n + m) solution?
#####################################################################################################

from typing import List


class Solution:
    def countNegatives(self, grid: List[List[int]]) -> int:  # (m * n) time and O(1) space
        return sum(element < 0 for row in grid for element in row)

    def countNegatives(self, grid: List[List[int]]) -> int:  # O(m * n) time
        return str(grid).count('-')

    def countNegatives(self, grid: List[List[int]]) -> int:  # O(m * log(n)) time and O(1) space

        def binary_search(array: List[int]) -> int:
            left, right = 0, len(array)
            while left < right:
                mid = left + (right - left) // 2
                if array[mid] >= 0:
                    left = mid + 1
                else:
                    right = mid
            return len(array) - left

        return sum(binary_search(row) for row in grid)

    def countNegatives(self, grid: List[List[int]]) -> int:  # O(m * log n) time
        import bisect
        return sum(bisect.bisect_left(row[::-1], 0) for row in grid)


def test():
    arguments = [
        [[4, 3, 2, -1],
         [3, 2, 1, -1],
         [1, 1, -1, -2],
         [-1, -1, -2, -3]],

        [[3, 2],
         [1, 0]],

        [[1, -1], [-1, -1]],

        [[-1]],
    ]
    expectations = [8, 0, 3, 1]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().countNegatives(grid)
        assert solution == expected
