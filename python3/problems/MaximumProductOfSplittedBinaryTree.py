### Source : https://leetcode.com/problems/maximum-product-of-splitted-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-19

##################################################################################################### 
#
# Given the root of a binary tree, split the binary tree into two subtrees by removing one edge such 
# that the product of the sums of the subtrees is maximized.
# 
# Return the maximum product of the sums of the two subtrees. Since the answer may be too large, 
# return it modulo 109 + 7.
# 
# Note that you need to maximize the answer before taking the mod and not after taking it.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,6]
# Output: 110
# Explanation: Remove the red edge and get 2 binary trees with sum 11 and 10. Their product is 110 
# (11*10)
# 
# Example 2:
# 
# Input: root = [1,null,2,3,4,null,null,5,6]
# Output: 90
# Explanation: Remove the red edge and get 2 binary trees with sum 15 and 6.Their product is 90 (15*6)
# 
# Example 3:
# 
# Input: root = [2,3,9,10,7,8,6,5,4,11,1]
# Output: 1025
# 
# Example 4:
# 
# Input: root = [1,1]
# Output: 1
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 5 * 104].
# 	1 <= Node.val <= 104
#####################################################################################################

from typing import Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def maxProduct(self, root: Optional[TreeNode]) -> int:  # O(n) time and O(h) space
        # we find all the subtree sums, and compute the product of all the splits after
        def traverse(node: TreeNode) -> int:
            if not node:
                return 0
            subtree_sums.append(node.val + traverse(node.left) + traverse(node.right))
            return subtree_sums[-1]

        subtree_sums = []
        total = traverse(root)
        return max((n * (total - n)) for n in subtree_sums) % (10**9 + 7)


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,5,6]",
        "[1,null,2,3,4,null,null,5,6]",
        "[2,3,9,10,7,8,6,5,4,11,1]",
        "[1,1]",
    ]
    expectations = [110, 90, 1025, 1]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().maxProduct(root)
        assert solution == expected
