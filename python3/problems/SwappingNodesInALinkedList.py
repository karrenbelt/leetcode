### Source : https://leetcode.com/problems/swapping-nodes-in-a-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-14

##################################################################################################### 
#
# You are given the head of a linked list, and an integer k.
# 
# Return the head of the linked list after swapping the values of the kth node from the beginning and 
# the kth node from the end (the list is 1-indexed).
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5], k = 2
# Output: [1,4,3,2,5]
# 
# Example 2:
# 
# Input: head = [7,9,6,6,7,8,3,0,9,5], k = 5
# Output: [7,9,6,6,8,7,3,0,9,5]
# 
# Example 3:
# 
# Input: head = [1], k = 1
# Output: [1]
# 
# Example 4:
# 
# Input: head = [1,2], k = 1
# Output: [2,1]
# 
# Example 5:
# 
# Input: head = [1,2,3], k = 2
# Output: [1,2,3]
# 
# Constraints:
# 
# 	The number of nodes in the list is n.
# 	1 <= k <= n <= 105
# 	0 <= Node.val <= 100
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def swapNodes(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(1) space, but not swapping nodes...
        k -= 1  # one-indexed to zero-indexed
        first = second = head
        for _ in range(k):
            first = first.next
        null_checker = first
        while null_checker.next:
            null_checker = null_checker.next
            second = second.next
        first.val, second.val = second.val, first.val  # disgusting
        return head

    def swapNodes(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(1) space, swapping nodes
        # quite a few things to consider
        # 1. corner case where k = 1
        # 2. case where +k and -k are the same node
        # 3. case where +k and -k are neighbors
        # 4. the generic case
        k -= 1  # one-indexed to zero-indexed
        dummy = ListNode(None, head)
        first_prev = second_prev = dummy
        for _ in range(k):
            first_prev = first_prev.next
        null_checker = first_prev.next
        while null_checker.next:
            null_checker = null_checker.next
            second_prev = second_prev.next
        # swap - set the requires references
        first, second = first_prev.next, second_prev.next
        first_next, second_next = first.next, second.next
        if first is second:
            return head
        elif second_prev is first:
            first_prev.next = second
            second.next = first
            first.next = second_next
        elif second_next is first:
            second_prev.next = first
            first.next = second
            second.next = first_next
        else:  # generic case
            first_prev.next = second
            second.next = first_next
            second_prev.next = first
            first.next = second_next
        return dummy.next


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([1, 2, 3, 4, 5], 2),
        ([7, 9, 6, 6, 7, 8, 3, 0, 9, 5], 5),
        ([1], 1),
        ([1, 2], 1),
        ([1, 2, 3], 2),
        ]
    expectations = [
        [1, 4, 3, 2, 5],
        [7, 9, 6, 6, 8, 7, 3, 0, 9, 5],
        [1],
        [2, 1],
        [1, 2, 3],
        ]
    for (nums, k), expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().swapNodes(head, k)
        assert solution == SinglyLinkedList(expected).head
