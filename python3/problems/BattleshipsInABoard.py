### Source : https://leetcode.com/problems/battleships-in-a-board/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given an 2D board, count how many battleships are in it. The battleships are represented with 'X's, 
# empty slots are represented with '.'s. You may assume the following rules:
# 
# You receive a valid board, made of only battleships or empty slots.
# Battleships can only be placed horizontally or vertically. In other words, they can only be made of 
# the shape 1xN (1 row, N columns) or Nx1 (N rows, 1 column), where N can be of any size.
# At least one horizontal or vertical cell separates between two battleships - there are no adjacent 
# battleships.
# 
# Example:
# X..X
# ...X
# ...X
# 
# In the above board there are 2 battleships.
# 
# Invalid Example:
# ...X
# XXXX
# ...X
# 
# This is an invalid board that you will not receive - as battleships will always have a cell 
# separating between them.
# 
# Follow up:Could you do it in one-pass, using only O(1) extra memory and without modifying the value 
# of the board?
#####################################################################################################

from typing import List


class Solution:
    def countBattleships(self, board: List[List[str]]) -> int:  # O(n) time and O(1) space, but modifying the board

        def dfs(i: int, j: int):
            if 0 <= i < len(board) and 0 <= j < len(board[0]) and board[i][j] == 'X':
                board[i][j] = '.'
                for y, x in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
                    dfs(i + x, j + y)

        ctr = 0
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == 'X':
                    ctr += 1
                    dfs(i, j)
        return ctr

    def countBattleships(self, board: List[List[str]]) -> int:  # O(n) time and O(1) space, no modification
        ctr = 0
        for i in range(len(board)):
            for j in range(len(board[0])):
                up_empty = i == 0 or board[i - 1][j] == '.'
                left_empty = j == 0 or board[i][j - 1] == '.'
                if board[i][j] == 'X' and up_empty and left_empty:
                    ctr += 1
        return ctr

    def countBattleships(self, board: List[List[str]]) -> int:  # just because we can
        return sum(1 for j in range(len(board[0])) for i in range(len(board)) if board[i][j] == 'X' and
                   (i == 0 or board[i - 1][j] == '.') and (j == 0 or board[i][j - 1] == '.'))


def test():
    boards = [
        [['X', '.', '.', 'X'],
         ['.', '.', '.', 'X'],
         ['.', '.', '.', 'X']]
        ]
    expectations = [2]
    for board, expected in zip(boards, expectations):
        solution = Solution().countBattleships(board)
        assert solution == expected
