### Source : https://leetcode.com/problems/cat-and-mouse-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

#####################################################################################################
#
# A game is played by a cat and a mouse named Cat and ouse.
#
# The environment is represented by a grid of size rows x cols, where each element is a wall, floor,
# player (Cat, ouse), or food.
#
# 	Players are represented by the characters 'C'(Cat),''(ouse).
# 	Floors are represented by the character '.' and can be walked on.
# 	Walls are represented by the character '#' and cannot be walked on.
# 	Food is represented by the character 'F' and can be walked on.
# 	There is only one of each character 'C', '', and 'F' in grid.
#
# ouse and Cat play according to the following rules:
#
# 	ouse moves first, then they take turns to move.
# 	During each turn, Cat and ouse can jump in one of the four directions (left, right, up,
# down). They cannot jump over the wall nor outside of the grid.
# 	catJump, mouseJump are the maximum lengths Cat and ouse can jump at a time, respectively.
# Cat and ouse can jump less than the maximum length.
# 	Staying in the same position is allowed.
# 	ouse can jump over Cat.
#
# The game can end in 4 ways:
#
# 	If Cat occupies the same position as ouse, Cat wins.
# 	If Cat reaches the food first, Cat wins.
# 	If ouse reaches the food first, ouse wins.
# 	If ouse cannot get to the food within 1000 turns, Cat wins.
#
# Given a rows x cols matrix grid and two integers catJump and mouseJump, return true if ouse can
# win the game if both Cat and ouse play optimally, otherwise return false.
#
# Example 1:
#
# Input: grid = ["####F","#C...","...."], catJump = 1, mouseJump = 2
# Output: true
# Explanation: Cat cannot catch ouse on its turn nor can it get the food before ouse.
#
# Example 2:
#
# Input: grid = [".C...F"], catJump = 1, mouseJump = 4
# Output: true
#
# Example 3:
#
# Input: grid = [".C...F"], catJump = 1, mouseJump = 3
# Output: false
#
# Example 4:
#
# Input: grid = ["C...#","...#F","....#","...."], catJump = 2, mouseJump = 5
# Output: false
#
# Example 5:
#
# Input: grid = ["....","..#..","#..#.","C#.#.","...#F"], catJump = 3, mouseJump = 1
# Output: true
#
# Constraints:
#
# 	rows == grid.length
# 	cols = grid[i].length
# 	1 <= rows, cols <= 8
# 	grid[i][j] consist only of characters 'C', '', 'F', '.', and '#'.
# 	There is only one of each character 'C', '', and 'F' in grid.
# 	1 <= catJump, mouseJump <= 8
#####################################################################################################

from typing import List


class Solution:
    def canMouseWin(self, grid: List[str], catJump: int, mouseJump: int) -> bool:
        # 178 / 178 test cases passed, but took too long.
        MAX_MOVES = 68
        from functools import lru_cache

        def new_positions(i: int, j: int, jump: int):
            locations = [(i, j)]
            for di, dj in directions:
                for step in range(1, jump + 1):
                    new_i, new_j = i + di * step, j + dj * step
                    if 0 <= new_i < len(grid) and 0 <= new_j < len(grid[0]) and grid[new_i][new_j] != '#':
                        locations.append((new_i, new_j))
                    else:
                        break
            return locations

        @lru_cache(maxsize=None)
        def mouse_wins(cat_i, cat_j, mouse_i, mouse_j, n_moves) -> bool:
            if grid[cat_i][cat_j] == 'F' or (cat_i, cat_j) == (mouse_i, mouse_j) or n_moves > MAX_MOVES:
                return False
            if grid[mouse_i][mouse_j] == 'F':
                return True
            if n_moves % 2:  # cat's turn
                for new_i, new_j in new_positions(cat_i, cat_j, catJump):
                    if not mouse_wins(new_i, new_j, mouse_i, mouse_j, n_moves + 1):
                        return False
                return True
            else:  # mouse's turn
                for new_i, new_j in new_positions(mouse_i, mouse_j, mouseJump):
                    if mouse_wins(cat_i, cat_j, new_i, new_j, n_moves + 1):
                        return True
                return False

        # find starting position of mouse and cat
        cat_i = cat_j = mouse_i = mouse_j = -1
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 'M':
                    mouse_i, mouse_j = i, j
                if grid[i][j] == 'C':
                    cat_i, cat_j, = i, j

        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        return mouse_wins(cat_i, cat_j, mouse_i, mouse_j, 0)


def test():
    arguments = [
        (["####F", "#C...", "M...."], 1, 2),
        (["M.C...F"], 1, 4),
        (["M.C...F"], 1, 3),
        (["C...#", "...#F", "....#", "M...."], 2, 5),
    ]
    expectations = [True, True, False, False]
    for (grid, catJump, mouseJump), expected in zip(arguments, expectations):
        solution = Solution().canMouseWin(grid, catJump, mouseJump)
        assert solution == expected
