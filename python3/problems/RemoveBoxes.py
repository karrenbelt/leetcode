### Source : https://leetcode.com/problems/remove-boxes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-11

##################################################################################################### 
#
# You are given several boxes with different colors represented by different positive numbers.
# 
# You may experience several rounds to remove boxes until there is no box left. Each time you can 
# choose some continuous boxes with the same color (i.e., composed of k boxes, k >= 1), remove them 
# and get k * k points.
# 
# Return the maximum points you can get.
# 
# Example 1:
# 
# Input: boxes = [1,3,2,2,2,3,4,3,1]
# Output: 23
# Explanation:
# [1, 3, 2, 2, 2, 3, 4, 3, 1] 
# ----> [1, 3, 3, 4, 3, 1] (3*3=9 points) 
# ----> [1, 3, 3, 3, 1] (1*1=1 points) 
# ----> [1, 1] (3*3=9 points) 
# ----> [] (2*2=4 points)
# 
# Example 2:
# 
# Input: boxes = [1,1,1]
# Output: 9
# 
# Example 3:
# 
# Input: boxes = [1]
# Output: 1
# 
# Constraints:
# 
# 	1 <= boxes.length <= 100
# 	1 <= boxes[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def removeBoxes(self, boxes: List[int]) -> int:  # brute force O(n!) time, should yield TLE
        i = points = 0
        while i < len(boxes):
            j = i + 1
            while j < len(boxes) and boxes[j] == boxes[j - 1]:
                j += 1
            remaining_boxes = boxes[:i] + boxes[j:]
            points = max(points, (j - i)**2 + self.removeBoxes(remaining_boxes))
            i = j
        return points

    def removeBoxes(self, boxes: List[int]) -> int:  # O(n^4) time and O(n^3) space
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(left: int, right: int, streak: int) -> int:
            if left > right:
                return 0

            while right > left and boxes[right] == boxes[right - 1]:
                right -= 1
                streak += 1

            points = dfs(left, right - 1, 0) + (streak + 1)**2
            for i in range(left, right):
                if boxes[i] == boxes[right]:
                    points = max(points, dfs(left, i, streak + 1) + dfs(i + 1, right - 1, 0))
            return points

        return dfs(0, len(boxes) - 1, 0)


def test():
    arguments = [
        [1, 3, 2, 2, 2, 3, 4, 3, 1],
        [1, 1, 1],
        [1],
        [1, 2, 2, 1, 2, 2, 1],
        [9, 3, 6, 8, 8, 1, 2, 5, 5, 6],
    ]
    expectations = [23, 9, 1, 21, 16]
    for boxes, expected in zip(arguments, expectations):
        solution = Solution().removeBoxes(boxes)
        assert solution == expected, expected
