### Source : https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# You are given a list of songs where the ith song has a duration of time[i] seconds.
# 
# Return the number of pairs of songs for which their total duration in seconds is divisible by 60. 
# Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.
# 
# Example 1:
# 
# Input: time = [30,20,150,100,40]
# Output: 3
# Explanation: Three pairs have a total duration divisible by 60:
# (time[0] = 30, time[2] = 150): total duration 180
# (time[1] = 20, time[3] = 100): total duration 120
# (time[1] = 20, time[4] = 40): total duration 60
# 
# Example 2:
# 
# Input: time = [60,60,60]
# Output: 3
# Explanation: All three pairs have a total duration of 120, which is divisible by 60.
# 
# Constraints:
# 
# 	1 <= time.length <= 6 * 104
# 	1 <= time[i] <= 500
#####################################################################################################

from typing import List


class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:  # should TLE
        from itertools import combinations
        return sum(not (a + b) % 60 for a, b in combinations(time, 2))

    def numPairsDivisibleBy60(self, time: List[int]) -> int:  # O(n) time
        import math
        from collections import Counter
        counts = Counter(t % 60 for t in time)
        number_of_pairs = sum(math.comb(counts.pop(t, 0), 2) for t in (0, 30))  # 0 and 30 are special cases
        for v, count in sorted(counts.items()):
            if v > 30:
                break
            number_of_pairs += count * counts.get(60 - v, 0)
        return number_of_pairs

    def numPairsDivisibleBy60(self, time: List[int]) -> int:  # O(n) time
        counts = [0] * 60
        number_of_pairs = 0
        for t in time:
            number_of_pairs += counts[-t % 60]
            counts[t % 60] += 1
        return number_of_pairs


def test():
    arguments = [
        [30, 20, 150, 100, 40],
        [60, 60, 60],
        [15, 63, 451, 213, 37, 209, 343, 319],
    ]
    expectations = [3, 3, 1]
    for time, expected in zip(arguments, expectations):
        solution = Solution().numPairsDivisibleBy60(time)
        assert solution == expected
