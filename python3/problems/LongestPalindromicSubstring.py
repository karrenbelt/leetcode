### Source : https://leetcode.com/problems/longest-palindromic-substring/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-14

##################################################################################################### 
#
# Given a string s, return the longest palindromic substring in s.
# 
# Example 1:
# 
# Input: s = "babad"
# Output: "bab"
# Note: "aba" is also a valid answer.
# 
# Example 2:
# 
# Input: s = "cbbd"
# Output: "bb"
# 
# Example 3:
# 
# Input: s = "a"
# Output: "a"
# 
# Example 4:
# 
# Input: s = "ac"
# Output: "a"
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s consist of only digits and English letters (lower-case and/or upper-case),
#####################################################################################################


class Solution:
    def longestPalindrome(self, s: str) -> str:  # two-pointer: O(n^2) time and O(1) space
        # for every index, find the longest palindromic subsequence,
        # starting from i (odd case), or i and i+1 (even case)
        def lsp(left, right):
            while 0 <= left and right < len(s) and s[left] == s[right]:
                left -= 1
                right += 1
            return s[left + 1:right]

        ans = ""
        for i in range(len(s)):
            ans = max(lsp(i, i), lsp(i, i + 1), ans, key=len)
        return ans

    def longestPalindrome(self, s: str) -> str:  # dp: bottom-up: O(n^2) time and space
        # iterate backwards (i) with a nested forward iteration (j) use an two-D array to mark nodes (i, j) visited
        # dp[i+1][j-1] represents the middle of the current considered substring. If True, then it is a palindrome.
        # as a result of above, i is decremented, and j is incremented.
        # if the middle is a palindrome and the endpoints equal each other, it follows that s[i:j+1] is a palindrome.
        dp = [[False] * len(s) for _ in range(len(s))]
        ans = ""
        for i in range(len(s) - 1, -1, -1):
            for j in range(i, len(s)):
                if s[i] == s[j] and (j - i < 2 or dp[i+1][j-1]):
                    dp[i][j] = True
                    if ans == "" or len(ans) < j - i + 1:
                        ans = s[i:j+1]
        return ans

    def longestPalindrome(self, s: str) -> str:  # Manacher’s algorithm: O(n) time and O(n) space
        # http://en.wikipedia.org/wiki/Longest_palindromic_substring
        # the idea is to avoid redundant otherwise performed when dealing with overlapping palindromic subsequences
        # transform string; interleave '#' and place sentinels at the start (^) and end ($)
        # interleaving of '#' allows us to deal with uneven and even palindromes, sentinels to avoid bound checking
        transformed = '#'.join('^{}$'.format(s))
        longest_pal_pos = [0] * len(transformed)  # length of longest palindromic substring starting at position i in T
        center = right = 0
        for i in range(1, len(transformed) - 1):
            i_mirror = center - (i - center)
            longest_pal_pos[i] = max(0, min(right - i, longest_pal_pos[i_mirror]))
            # attempt to expand palindrome centered at i
            while transformed[i + 1 + longest_pal_pos[i]] == transformed[i - 1 - longest_pal_pos[i]]:
                longest_pal_pos[i] += 1
            # if palindrome centered at i expand past right, adjust center based on expanded palindrome.
            if i + longest_pal_pos[i] > right:
                center, right = i, i + longest_pal_pos[i]
        max_length, center_index = max((n, i) for i, n in enumerate(longest_pal_pos))
        return s[(center_index - max_length) // 2: (center_index + max_length) // 2]


def test():
    strings = ["babad", "cbbd", "a", "ac", "aaabaaaa"]
    expectations = [{"bab", "aba"}, "bb", "a", {"a", "c"}, "aaabaaa"]
    for s, expected in zip(strings, expectations):
        solution = Solution().longestPalindrome(s)
        assert solution == expected if isinstance(expected, str) else solution in expected
