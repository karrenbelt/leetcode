### Source : https://leetcode.com/problems/largest-plus-sign/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# 
# In a 2D grid from (0, 0) to (N-1, N-1), every cell contains a 1, except those cells in the given 
# list mines which are 0.  What is the largest axis-aligned plus sign of 1s contained in the grid?  
# Return the order of the plus sign.  If there is none, return 0.
# 
# An "axis-aligned plus sign of 1s of order k" has some center grid[x][y] = 1 along with 4 arms of 
# length k-1 going up, down, left, and right, and made of 1s.  This is demonstrated in the diagrams 
# below.  Note that there could be 0s or 1s beyond the arms of the plus sign, only the relevant area 
# of the plus sign is checked for 1s.
# 
# Examples of Axis-Aligned Plus Signs of Order k:
# Order 1:
# 000
# 010
# 000
# 
# Order 2:
# 00000
# 00100
# 01110
# 00100
# 00000
# 
# Order 3:
# 0000000
# 0001000
# 0001000
# 0111110
# 0001000
# 0001000
# 0000000
# 
# Example 1:
# Input: N = 5, mines = [[4, 2]]
# Output: 2
# Explanation:
# 11111
# 11111
# 11111
# 11111
# 11011
# In the above grid, the largest plus sign can only be order 2.  One of them is marked in bold.
# 
# Example 2:
# Input: N = 2, mines = []
# Output: 1
# Explanation:
# There is no plus sign of order 2, but there is of order 1.
# 
# Example 3:
# Input: N = 1, mines = [[0, 0]]
# Output: 0
# Explanation:
# There is no plus sign, so return 0.
# 
# Note:
# N will be an integer in the range [1, 500].
# mines will have length at most 5000.
# mines[i] will be length 2 and consist of integers in the range [0, N-1].
# (Additionally, programs submitted in C, C++, or C# will be judged with a slightly smaller time 
# limit.)
#####################################################################################################

from typing import List


class Solution:
    def orderOfLargestPlusSign(self, N: int, mines: List[List[int]]) -> int:   # O(n * mines) time: 8000 ms
        g = [[min(i, N-1-i, j, N-1-j) + 1 for j in range(N)] for i in range(N)]
        for x, y in set(map(tuple, mines)):
            for i in range(N):
                g[i][y] = min(g[i][y], abs(i - x))
                g[x][i] = min(g[x][i], abs(i - y))
        return max(max(row) for row in g)

    def orderOfLargestPlusSign(self, N: int, mines: List[List[int]]) -> int:  # O(n^2) time: 3000 ms
        dp = [[N] * N for _ in range(N)]
        for right, c in mines:
            dp[right][c] = 0
        for i in range(N):
            left, right, top, bottom = 0, 0, 0, 0
            for j, k in zip(range(N), reversed(range(N))):
                left = 0 if dp[i][j] == 0 else left + 1
                dp[i][j] = min(dp[i][j], left)
                right = 0 if dp[i][k] == 0 else right + 1
                dp[i][k] = min(dp[i][k], right)
                top = 0 if dp[j][i] == 0 else top + 1
                dp[j][i] = min(dp[j][i], top)
                bottom = 0 if dp[k][i] == 0 else bottom + 1
                dp[k][i] = min(dp[k][i], bottom)
        return max(map(max, dp))

    def orderOfLargestPlusSign(self, N: int, mines: List[List[int]]) -> int:  # O(n log n) time: 266 ms
        import bisect

        rows = [[-1, N] for _ in range(N)]
        cols = [[-1, N] for _ in range(N)]
        for r, c in mines:
            rows[r].append(c)
            cols[c].append(r)
        for i in range(N):
            rows[i].sort()
            cols[i].sort()
        largest = 0
        for r in range(N):
            for i in range(len(rows[r]) - 1):
                left_b = rows[r][i]
                right_b = rows[r][i + 1]
                # only explore larger possible plus signs, iterate their possible centers
                for c in range(left_b + largest + 1, right_b - largest):
                    idx = bisect.bisect_right(cols[c], r) - 1  # find where current row is in columns
                    up_b = cols[c][idx]
                    down_b = cols[c][idx + 1]
                    largest = max(largest, min(c - left_b, right_b - c, r - up_b, down_b - r))
        return largest


def test():
    arguments = [
        (5, [[4, 2]]),
        (2, []),
        (1, [[0, 0]])
        ]
    expectations = [2, 1, 0]
    for (N, mines), expected in zip(arguments, expectations):
        solution = Solution().orderOfLargestPlusSign(N, mines)
        assert solution == expected
