### Source : https://leetcode.com/problems/largest-perimeter-triangle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# Given an integer array nums, return the largest perimeter of a triangle with a non-zero area, 
# formed from three of these lengths. If it is impossible to form any triangle of a non-zero area, 
# return 0.
# 
# Example 1:
# Input: nums = [2,1,2]
# Output: 5
# Example 2:
# Input: nums = [1,2,1]
# Output: 0
# Example 3:
# Input: nums = [3,2,3,4]
# Output: 10
# Example 4:
# Input: nums = [3,6,2,3]
# Output: 8
# 
# Constraints:
# 
# 	3 <= nums.length <= 104
# 	1 <= nums[i] <= 106
#####################################################################################################

from typing import List


class Solution:
    def largestPerimeter(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        nums.sort()
        for i in range(len(nums) - 1, 1, -1):
            if nums[i] < nums[i - 1] + nums[i - 2]:
                return sum(nums[i - 2: i + 1])
        return 0

    def largestPerimeter(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        return nums.sort(reverse=True) or next((a + b + c for a, b, c in zip(nums, nums[1:], nums[2:]) if a < b + c), 0)


def test():
    arguments = [
        [2, 1, 2],
        [1, 2, 1],
        [3, 2, 3, 4],
    ]
    expectations = [5, 0, 10]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().largestPerimeter(nums)
        assert solution == expected
