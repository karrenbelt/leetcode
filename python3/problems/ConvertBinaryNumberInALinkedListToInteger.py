### Source : https://leetcode.com/problems/convert-binary-number-in-a-linked-list-to-integer/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-07

##################################################################################################### 
#
# Given head which is a reference node to a singly-linked list. The value of each node in the linked 
# list is either 0 or 1. The linked list holds the binary representation of a number.
# 
# Return the decimal value of the number in the linked list.
# 
# Example 1:
# 
# Input: head = [1,0,1]
# Output: 5
# Explanation: (101) in base 2 = (5) in base 10
# 
# Example 2:
# 
# Input: head = [0]
# Output: 0
# 
# Example 3:
# 
# Input: head = [1]
# Output: 1
# 
# Example 4:
# 
# Input: head = [1,0,0,1,0,0,1,1,1,0,0,0,0,0,0]
# Output: 18880
# 
# Example 5:
# 
# Input: head = [0,0]
# Output: 0
# 
# Constraints:
# 
# 	The Linked List is not empty.
# 	Number of nodes will not exceed 30.
# 	Each node's value is either 0 or 1.
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def getDecimalValue(self, head: ListNode) -> int:  # O(n) time and O(1) space
        node, n = head, 0
        while node:
            n <<= 1
            n |= node.val
            node = node.next
        return n

    def getDecimalValue(self, head: ListNode) -> int:  # O(n) time and O(1) space
        node, n = head, head.val
        while node := node.next:
            n = n << 1 | node.val
        return n


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [1, 0, 1],
        [0],
        [1],
        [1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [0, 0],
    ]
    expectations = [5, 0, 1, 18880, 0]
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().getDecimalValue(head)
        assert solution == expected
