### Source : https://leetcode.com/problems/reverse-nodes-in-k-group/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.
# 
# k is a positive integer and is less than or equal to the length of the linked list. If the number 
# of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.
# 
# Follow up:
# 
# 	Could you solve the problem in O(1) extra memory space?
# 	You may not alter the values in the list's nodes, only nodes itself may be changed.
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5], k = 2
# Output: [2,1,4,3,5]
# 
# Example 2:
# 
# Input: head = [1,2,3,4,5], k = 3
# Output: [3,2,1,4,5]
# 
# Example 3:
# 
# Input: head = [1,2,3,4,5], k = 1
# Output: [1,2,3,4,5]
# 
# Example 4:
# 
# Input: head = [1], k = 1
# Output: [1]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range sz.
# 	1 <= sz <= 5000
# 	0 <= Node.val <= 1000
# 	1 <= k <= sz
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def reverseKGroup(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(n) space
        node, nodes = head, []
        while node:
            node = nodes.append(node) or node.next
        q, r = divmod(len(nodes), k)
        ordered = sum([nodes[i:i + k][::-1] for i in range(0, q * k, k)] + [nodes[-r:] if r else []], [])
        new_head = ordered[0]
        for i in range(len(ordered) - 1):
            ordered[i].next = ordered[i + 1]
        ordered[-1].next = None
        return new_head

    # def reverseKGroup(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(1) space
    #
    #     def reverse(start: ListNode, end: ListNode):
    #         prev, current = None, start
    #         while prev != end:
    #             current.next, prev, current = prev, current, current.next
    #         return prev, start
    #
    #     dummy = ListNode(0)
    #     dummy.next = head
    #     outside_head = dummy
    #
    #     i = 1
    #     node = head
    #     while node:
    #         if i % k == 0:
    #             outside_tail, node.next = node.next, None
    #             window_head, window_tail = reverse(outside_head.next, node)
    #             outside_head.next, window_tail.next = window_head, outside_tail
    #             outside_head, node = window_tail, window_tail.next
    #         else:
    #             node = node.next
    #         i += 1
    #     return dummy.next


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([1, 2, 3, 4, 5], 2),
        ([1, 2, 3, 4, 5], 3),
        ([1, 2, 3, 4, 5], 1),
        ([1], 1),
        ([1, 2], 2)
        ]
    expectations = [
        [2, 1, 4, 3, 5],
        [3, 2, 1, 4, 5],
        [1, 2, 3, 4, 5],
        [1],
        [2, 1],
        ]
    for (nums, k), expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().reverseKGroup(head, k)
        assert solution == SinglyLinkedList(expected).head
