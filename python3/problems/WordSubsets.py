### Source : https://leetcode.com/problems/word-subsets/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-27

##################################################################################################### 
#
# We are given two arrays A and B of words.  Each word is a string of lowercase letters.
# 
# Now, say that word b is a subset of word a if every letter in b occurs in a, including 
# multiplicity.  For example, "wrr" is a subset of "warrior", but is not a subset of "world".
# 
# Now say a word a from A is universal if for every b in B, b is a subset of a. 
# 
# Return a list of all universal words in A.  You can return the words in any order.
# 
# Example 1:
# 
# Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["e","o"]
# Output: ["facebook","google","leetcode"]
# 
# Example 2:
# 
# Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["l","e"]
# Output: ["apple","google","leetcode"]
# 
# Example 3:
# 
# Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["e","oo"]
# Output: ["facebook","google"]
# 
# Example 4:
# 
# Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["lo","eo"]
# Output: ["google","leetcode"]
# 
# Example 5:
# 
# Input: A = ["amazon","apple","facebook","google","leetcode"], B = ["ec","oc","ceo"]
# Output: ["facebook","leetcode"]
# 
# Note:
# 
# 	1 <= A.length, B.length <= 10000
# 	1 <= A[i].length, B[i].length <= 10
# 	A[i] and B[i] consist only of lowercase letters.
# 	All words in A[i] are unique: there isn't i != j with A[i] == A[j].
# 
#####################################################################################################

from typing import List


class Solution:
    def wordSubsets(self, A: List[str], B: List[str]) -> List[str]:  # O(a * b) time: TLE
        from collections import Counter
        ans = []
        for a in A:
            counts = Counter(a)
            if not any(any(Counter(b) - counts) for b in set(B)):
                ans.append(a)
        return ans

    def wordSubsets(self, A: List[str], B: List[str]) -> List[str]:  # O(a + b) time
        from collections import Counter
        count = Counter()
        for b in set(B):
            count |= Counter(b)  # union: max(c[x], d[x])
        return [a for a in A if not count - Counter(a)]


def test():
    arguments = [
        (["amazon", "apple", "facebook", "google", "leetcode"], ["e", "o"]),
        (["amazon", "apple", "facebook", "google", "leetcode"], ["l", "e"]),
        (["amazon", "apple", "facebook", "google", "leetcode"], ["e", "oo"]),
        (["amazon", "apple", "facebook", "google", "leetcode"], ["lo", "eo"]),
        (["amazon", "apple", "facebook", "google", "leetcode"], ["ec", "oc", "ceo"]),
        ]
    expectations = [
        ["facebook", "google", "leetcode"],
        ["apple", "google", "leetcode"],
        ["facebook", "google"],
        ["google", "leetcode"],
        ["facebook", "leetcode"],
        ]
    for (A, B), expected in zip(arguments, expectations):
        solution = Solution().wordSubsets(A, B)
        assert solution == expected
