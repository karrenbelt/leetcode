### Source : https://leetcode.com/problems/design-memory-allocator/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-03

##################################################################################################### 
#
# You are given an integer n representing the size of a 0-indexed memory array. All memory units are 
# initially free.
# 
# You have a memory allocator with the following functionalities:
# 
# 	Allocate a block of size consecutive free memory units and assign it the id mID.
# 	Free all memory units with the given id mID.
# 
# Note that:
# 
# 	ultiple blocks can be allocated to the same mID.
# 	You should free all the memory units with mID, even if they were allocated in different 
# blocks.
# 
# Implement the Allocator class:
# 
# 	Allocator(int n) Initializes an Allocator object with a memory array of size n.
# 	int allocate(int size, int mID) Find the leftmost block of size consecutive free memory 
# units and allocate it with the id mID. Return the block's first index. If such a block does not 
# exist, return -1.
# 	int free(int mID) Free all memory units with the id mID. Return the number of memory units 
# you have freed.
# 
# Example 1:
# 
# Input
# ["Allocator", "allocate", "allocate", "allocate", "free", "allocate", "allocate", "allocate", 
# "free", "allocate", "free"]
# [[10], [1, 1], [1, 2], [1, 3], [2], [3, 4], [1, 1], [1, 1], [1], [10, 2], [7]]
# Output
# [null, 0, 1, 2, 1, 3, 1, 6, 3, -1, 0]
# 
# Explanation
# Allocator loc = new Allocator(10); // Initialize a memory array of size 10. All memory units are 
# initially free.
# loc.allocate(1, 1); // The leftmost block's first index is 0. The memory array becomes 
# [1,_,_,_,_,_,_,_,_,_]. We return 0.
# loc.allocate(1, 2); // The leftmost block's first index is 1. The memory array becomes 
# [1,2,_,_,_,_,_,_,_,_]. We return 1.
# loc.allocate(1, 3); // The leftmost block's first index is 2. The memory array becomes 
# [1,2,3,_,_,_,_,_,_,_]. We return 2.
# loc.free(2); // Free all memory units with mID 2. The memory array becomes [1,_, 3,_,_,_,_,_,_,_]. 
# We return 1 since there is only 1 unit with mID 2.
# loc.allocate(3, 4); // The leftmost block's first index is 3. The memory array becomes 
# [1,_,3,4,4,4,_,_,_,_]. We return 3.
# loc.allocate(1, 1); // The leftmost block's first index is 1. The memory array becomes 
# [1,1,3,4,4,4,_,_,_,_]. We return 1.
# loc.allocate(1, 1); // The leftmost block's first index is 6. The memory array becomes 
# [1,1,3,4,4,4,1,_,_,_]. We return 6.
# loc.free(1); // Free all memory units with mID 1. The memory array becomes [_,_,3,4,4,4,_,_,_,_]. 
# We return 3 since there are 3 units with mID 1.
# loc.allocate(10, 2); // We can not find any free block with 10 consecutive free memory units, so we 
# return -1.
# loc.free(7); // Free all memory units with mID 7. The memory array remains the same since there is 
# no memory unit with mID 7. We return 0.
# 
# Constraints:
# 
# 	1 <= n, size, mID <= 1000
# 	At most 1000 calls will be made to allocate and free.
#####################################################################################################

from typing import Optional


class Allocator:

    def __init__(self, n: int):
        self.array = [0] * n

    def allocate(self, size: int, mID: int) -> int:
        from itertools import groupby
        i = 0
        for g, it in groupby(self.array):
            n = len(list(it))
            if g == 0 and n >= size:
                self.array[i: i + size] = [mID] * size
                return i
            i += n
        return -1

    def free(self, mID: int) -> int:
        ctr = 0
        for i, n in enumerate(self.array):
            if n == mID:
                self.array[i] = 0
                ctr += 1
        return ctr


class Allocator:

    def __init__(self, n: int):
        self.data = chr(0) * n

    def allocate(self, size: int, mID: int) -> int:
        i = self.data.find(chr(0) * size)
        if i != -1:
            self.data = self.data[:i] + chr(mID) * size + self.data[i + size:]
        return i

    def free(self, mID: int) -> int:
        freed = self.data.count(chr(mID))
        self.data = self.data.replace(chr(mID), chr(0))
        return freed


class Allocator:

    def __init__(self, n: int):
        from collections import defaultdict
        from sortedcontainers import SortedList
        self._mem_map = defaultdict(SortedList)
        self._mem_map[0].update(((-1, -1), (0, n), (n + 1, n + 1)))

    @property
    def free_blocks(self):
        return self._mem_map[0]

    def allocate(self, size: int, mID: int) -> int:
        for (start, stop) in self.free_blocks:
            if stop - start < size:
                continue
            self.free_blocks.remove((start, stop))
            self._mem_map[mID].add((start, start + size))
            if start + size != stop:
                self.free_blocks.add((start + size, stop))
            return start
        return -1

    def free(self, mID: int) -> int:

        def merge(*blocks: tuple[int, int]) -> list[tuple[int, int]]:
            merged = []
            for i, b in enumerate(blocks):
                if merged and merged[-1][1] == b[0]:
                    merged[-1] = (merged[-1][0], b[1])
                else:
                    merged.append(b)
            return merged

        freed = 0
        for block in self._mem_map.pop(mID, set()):
            idx = self.free_blocks.bisect(block)
            after = self.free_blocks.pop(idx)
            before = self.free_blocks.pop(idx - 1)
            self.free_blocks.update(merge(before, block, after))
            freed += block[1] - block[0]
        return freed

    def __repr__(self):
        return "\n".join(f"{k:4d}: {list(v)}" for k, v in self._mem_map.items())


class Allocator:

    from dataclasses import dataclass

    @dataclass
    class Node:
        start: int
        end: int
        _next: Optional['Node'] = None
        _prev: Optional['Node'] = None

        @property
        def next(self) -> Optional['Node']:
            return self._next

        @next.setter
        def next(self, node: 'Node') -> None:
            self._next, node._prev = node, self

        @property
        def prev(self) -> Optional['Node']:
            return self._prev

        def delete(self):
            if self.prev:
                self.prev._next = self.next
            if self.next:
                self.next._prev = self.prev

    def __init__(self, n: int):
        from collections import defaultdict
        self.head = self.Node(-1, -1, self.Node(n, n))
        self.sections = defaultdict(list)

    def allocate(self, size: int, mID: int) -> int:
        node = self.head
        while node.next:
            if node.next.start - node.end - 1 >= size:
                new_node = self.Node(node.end + 1, node.end + size)
                new_node.next, node.next = node.next, new_node
                self.sections[mID].append(new_node)
                return new_node.start
            node = node.next
        return -1

    def free(self, mID: int) -> int:
        freed = 0
        while self.sections[mID]:
            node = self.sections[mID].pop()
            freed += node.end - node.start + 1
            node.delete()
        return freed

# Your Allocator object will be instantiated and called as such:
# obj = Allocator(n)
# param_1 = obj.allocate(size,mID)
# param_2 = obj.free(mID)


def test():
    i = 1
    operations = [
        ["Allocator", "allocate", "allocate", "allocate", "free", "allocate",
         "allocate", "allocate", "free", "allocate", "free"],
        ["Allocator", "allocate", "allocate", "allocate", "allocate", "free",
         "free", "free", "allocate", "allocate", "allocate", "allocate", "free",
         "free", "free", "free", "free", "free", "free", "allocate", "free",
         "free", "allocate", "free", "allocate", "allocate", "free", "free",
         "free", "allocate", "allocate", "allocate", "allocate", "free",
         "allocate", "free", "free", "allocate", "allocate", "allocate",
         "allocate", "allocate", "allocate", "allocate", "free", "free", "free",
         "free"],
    ][i]
    arguments = [
        [[10], [1, 1], [1, 2], [1, 3], [2], [3, 4], [1, 1], [1, 1], [1],
         [10, 2], [7]],
        [[50], [12, 6], [28, 16], [17, 23], [50, 23], [6], [10], [10], [16, 8],
         [17, 41], [44, 27], [12, 45], [33], [8], [16], [23], [23], [23], [29],
         [38, 32], [29], [6], [40, 11], [16], [22, 33], [27, 5], [3], [10],
         [29], [16, 14], [46, 47], [48, 9], [36, 17], [33], [14, 24], [16], [8],
         [2, 50], [31, 36], [17, 45], [46, 31], [2, 6], [16, 2], [39, 30], [33],
         [45], [30], [27]],
    ][i]
    expectations = [
        [None, 0, 1, 2, 1, 3, 1, 6, 3, -1, 0],
        [None, 0, 12, -1, -1, 12, 0, 0, -1, -1, -1, 0, 0, 0, 28, 0, 0, 0, 0, 12,
         0, 0, -1, 0, -1, -1, 0, 0, 0, -1, -1, -1, -1, 0, -1, 0, 0, -1, -1, -1,
         -1, -1, -1, -1, 0, 12, 0, 0]
    ][i]

    obj = Allocator(*arguments[0])
    for (method, args, expected) in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
