### Source : https://leetcode.com/problems/evaluate-reverse-polish-notation/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Evaluate the value of an arithmetic expression in Reverse Polish Notation.
# 
# Valid operators are +, -, *, /. Each operand may be an integer or another expression.
# 
# Note:
# 
# 	Division between two integers should truncate toward zero.
# 	The given RPN expression is always valid. That means the expression would always evaluate 
# to a result and there won't be any divide by zero operation.
# 
# Example 1:
# 
# Input: ["2", "1", "+", "3", "*"]
# Output: 9
# Explanation: ((2 + 1) * 3) = 9
# 
# Example 2:
# 
# Input: ["4", "13", "5", "/", "+"]
# Output: 6
# Explanation: (4 + (13 / 5)) = 6
# 
# Example 3:
# 
# Input: ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
# Output: 22
# Explanation: 
#   ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
# = ((10 * (6 / (12 * -11))) + 17) + 5
# = ((10 * (6 / -132)) + 17) + 5
# = ((10 * 0) + 17) + 5
# = (0 + 17) + 5
# = 17 + 5
# = 22
# 
#####################################################################################################

from typing import List


class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack, operators = [], {'+': int.__add__, '-': int.__sub__, '*': int.__mul__, '/': int.__truediv__}
        for token in tokens:
            if token in operators:
                item = stack.pop()
                stack[-1] = int(operators[token](stack[-1], item))
            else:
                stack.append(int(token))
        return stack.pop()

    def evalRPN(self, tokens: List[str]) -> int:
        stack, operations = [], {'+': int.__add__, '-': int.__sub__, '*': int.__mul__, '/': int.__truediv__}
        for token in tokens:
            if token.isnumeric() or token[1:].isnumeric():
                stack.append(int(token))
            else:
                stack.append(int(operations[token](*reversed((stack.pop(), stack.pop())))))
        return stack.pop()


def test():
    arrays_of_tokens = [
        ["2", "1", "+", "3", "*"],
        ["4", "13", "5", "/", "+"],
        ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"],
        ]
    expectations = [9, 6, 22]
    for tokens, expected in zip(arrays_of_tokens, expectations):
        solution = Solution().evalRPN(tokens)
        assert solution == expected
