### Source : https://leetcode.com/problems/reverse-bits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-01

##################################################################################################### 
#
# Reverse bits of a given 32 bits unsigned integer.
# 
# Note:
# 
# 	Note that in some languages such as Java, there is no unsigned integer type. In this case, 
# both input and output will be given as a signed integer type. They should not affect your 
# implementation, as the integer's internal binary representation is the same, whether it is signed 
# or unsigned.
# 	In Java, the compiler represents the signed integers using 2's complement notation. 
# Therefore, in Example 2 above, the input represents the signed integer -3 and the output represents 
# the signed integer -1073741825.
# 
# Follow up:
# 
# If this function is called many times, how would you optimize it?
# 
# Example 1:
# 
# Input: n = 00000010100101000001111010011100
# Output:    964176192 (00111001011110000010100101000000)
# Explanation: The input binary string 00000010100101000001111010011100 represents the unsigned 
# integer 43261596, so return 964176192 which its binary representation is 
# 00111001011110000010100101000000.
# 
# Example 2:
# 
# Input: n = 11111111111111111111111111111101
# Output:   3221225471 (10111111111111111111111111111111)
# Explanation: The input binary string 11111111111111111111111111111101 represents the unsigned 
# integer 4294967293, so return 3221225471 which its binary representation is 
# 10111111111111111111111111111111.
# 
# Constraints:
# 
# 	The input must be a binary string of length 32
#####################################################################################################


class Solution:
    def reverseBits(self, n: int) -> int:  # O(32) time and O(n) space
        return int(bin(n)[2:].zfill(32)[::-1], 2)

    def reverseBits(self, n: int) -> int:  # O(32) time and O(1) space
        reverse_n = 0
        for _ in range(32):  # cannot use n.bit_length() because leading zeroes matter
            reverse_n <<= 1
            reverse_n += n & 1
            n >>= 1
        return reverse_n

    def reverseBits(self, n: int) -> int:  # O(32) time and O(1) space
        reverse_n = 0
        for _ in range(32):  # cannot use n.bit_length() because leading zeroes matter
            reverse_n = (reverse_n << 1) + (n & 1)
            n >>= 1
        return reverse_n

    def reverseBits(self, n: int) -> int:  # O(32) time and O(1) space
        reverse_n = 0
        for _ in range(32):
            reverse_n <<= 1
            reverse_n |= n & 1
            n >>= 1
        return reverse_n


def test():
    numbers = [
        int("00000010100101000001111010011100", 2),
        int("11111111111111111111111111111101", 2),
        ]
    expectations = [
        int("00111001011110000010100101000000", 2),
        int("10111111111111111111111111111111", 2),
        ]
    for n, expected in zip(numbers, expectations):
        solution = Solution().reverseBits(n)
        assert solution == expected
test()