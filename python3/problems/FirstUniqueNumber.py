# You have a queue of integers, you need to retrieve the first unique integer in the queue.
#
# Implement the FirstUnique class:
#
# - FirstUnique(int[] nums)
#   Initializes the object with the numbers in the queue.
# - int showFirstUnique()
#   returns the value of the first unique integer of the queue, and returns -1 if there is no such integer.
# - void add(int value)
#   insert value to the queue.

from typing import List


class FirstUnique:

    def __init__(self, nums: List[int]):
        self.d = {}
        self.seen = set()
        for n in nums:
            self.add(n)

    def showFirstUnique(self) -> int:
        return min(self.d, key=self.d.get) if self.d else -1

    def add(self, value: int) -> None:
        if value not in self.seen:
            self.d[value] = len(self.d)
            self.seen.add(value)
        elif value in self.d:
            del self.d[value]

# Your FirstUnique object will be instantiated and called as such:
# nums = [2, 3, 5]
# obj = FirstUnique(nums)
# print(obj.showFirstUnique())  # 2
# obj.add(5)  # 2, 3, 5, 5
# obj.add(2)  # 2, 3, 5, 5, 2
# print(obj.showFirstUnique())  # 3
# obj.add(3)
# print(obj.showFirstUnique())  # -1


def test():
    operations = ["FirstUnique", "showFirstUnique", "add", "add", "showFirstUnique", "add", "showFirstUnique"]
    arguments = [[[2, 3, 5]], [], [2], [5], [], [3], []]
    expectations = [None, 2, None, None, 3, None, -1]
    obj = FirstUnique(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
