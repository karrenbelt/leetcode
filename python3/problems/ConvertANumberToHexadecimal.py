### Source : https://leetcode.com/problems/convert-a-number-to-hexadecimal/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-01

##################################################################################################### 
#
# Given an integer num, return a string representing its hexadecimal representation. For negative 
# integers, two&rsquo;s complement method is used.
# 
# All the letters in the answer string should be lowercase characters, and there should not be any 
# leading zeros in the answer except for the zero itself.
# 
# Note: You are not allowed to use any built-in library method to directly solve this problem.
# 
# Example 1:
# Input: num = 26
# Output: "1a"
# Example 2:
# Input: num = -1
# Output: "ffffffff"
# 
# Constraints:
# 
# 	-231 <= num <= 231 - 1
#####################################################################################################


class Solution:
    def toHex(self, num: int) -> str:  # O(8) time and O(n) space
        translate, hexadecimal = '0123456789abcdef', ''
        for i in range(8):
            hexadecimal += translate[num & 15]  # 15 == int('1111', 2)
            num //= 16
        return hexadecimal[::-1].lstrip('0') or '0'

    def toHex(self, num: int) -> str:  # O(8) time and O(n) space
        translate, hexadecimal, num = '0123456789abcdef', '', num + 2 ** 32 * (num < 0)
        while num:
            num, remainder = divmod(num, 16)
            hexadecimal += translate[remainder]
        return hexadecimal[::-1].lstrip('0') or '0'


def test():
    arguments = [26, -1, 0]
    expectations = ["1a", "ffffffff", "0"]
    for num, expected in zip(arguments, expectations):
        solution = Solution().toHex(num)
        assert solution == expected, solution
