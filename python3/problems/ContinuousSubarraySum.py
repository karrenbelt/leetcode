### Source : https://leetcode.com/problems/continuous-subarray-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-17

##################################################################################################### 
#
# Given an integer array nums and an integer k, return true if nums has a continuous subarray of size 
# at least two whose elements sum up to a multiple of k, or false otherwise.
# 
# An integer x is a multiple of k if there exists an integer n such that x = n * k. 0 is always a 
# multiple of k.
# 
# Example 1:
# 
# Input: nums = [23,2,4,6,7], k = 6
# Output: true
# Explanation: [2, 4] is a continuous subarray of size 2 whose elements sum up to 6.
# 
# Example 2:
# 
# Input: nums = [23,2,6,4,7], k = 6
# Output: true
# Explanation: [23, 2, 6, 4, 7] is an continuous subarray of size 5 whose elements sum up to 42.
# 42 is a multiple of 6 because 42 = 7 * 6 and 7 is an integer.
# 
# Example 3:
# 
# Input: nums = [23,2,6,4,7], k = 13
# Output: false
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	0 <= nums[i] <= 109
# 	0 <= sum(nums[i]) <= 231 - 1
# 	1 <= k <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def checkSubarraySum(self, nums: List[int], k: int) -> bool:  # O(n^2) time --> TLE: 75 / 93 test cases passed.
        # if any sum(nums[i:j]) % k == 0  for i < j return True, else False
        for i in range(len(nums) - 1):
            for j in range(i + 1, len(nums)):
                if sum(nums[i: j + 1]) % k == 0:
                    return True
        return False

    def checkSubarraySum(self, nums: List[int], k: int) -> bool:  # O(n^2) time
        return any(sum(nums[i: j + 1]) % k == 0 for i in range(len(nums) - 1) for j in range(i + 1, len(nums)))

    def checkSubarraySum(self, nums: List[int], k: int) -> bool:  # O(n) time
        # moving window not possible, since multiple of k, not simply equal
        # given that sum(nums[:j]) % k == sum(nums[:i]) % k, track sum(nums[:i]) % k and the corresponding index i
        if k == 0:
            return ''.join(map(str, nums)).find('00') != -1

        current = 0
        seen = {0: -1}
        for i, n in enumerate(nums):
            current += n
            remainder = current % k
            if remainder in seen:
                if i - seen[remainder] > 1:
                    return True
            else:
                seen[remainder] = i
        return False

    def checkSubarraySum(self, nums: List[int], k: int) -> bool:  # O(n) time
        seen, prefix = {0: -1}, 0
        for i, n in enumerate(nums):
            prefix = (prefix + n) % k
            if i - seen.setdefault(prefix, i) > 1:
                return True
        return False


def test():
    arguments = [
        ([23, 2, 4, 6, 7], 6),
        ([23, 2, 6, 4, 7], 6),
        ([23, 2, 6, 4, 7], 13),
        ([23, 6, 9], 6),
        ]
    expectations = [True, True, False, False]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().checkSubarraySum(nums, k)
        assert solution == expected
