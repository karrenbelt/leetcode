### Source : https://leetcode.com/problems/spiral-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-22

##################################################################################################### 
#
# Given an m x n matrix, return all elements of the matrix in spiral order.
# 
# Example 1:
# 
# Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
# Output: [1,2,3,6,9,8,7,4,5]
# 
# Example 2:
# 
# Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
# Output: [1,2,3,4,8,12,11,10,9,5,6,7]
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 10
# 	-100 <= matrix[i][j] <= 100
#####################################################################################################

from typing import List


class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:

        result = []
        while matrix:
            if matrix[0]:  # top left to right
                result.extend(matrix.pop(0))
            if matrix and matrix[0]:  # top right to bottom
                for row in matrix:
                    result.append(row.pop())
            if matrix:  # bottom right to left
                result.extend(matrix.pop()[::-1])
            if matrix and matrix[0]:  # bottom left to top
                for row in matrix[::-1]:
                    result.append(row.pop(0))
        return result

    def spiralOrder(self, matrix):
        if not matrix or not matrix[0]:
            return []
        ans = []
        m, n = len(matrix), len(matrix[0])
        up, down, left, right = 0, m - 1, 0, n - 1
        while left < right and up < down:
            ans.extend([matrix[up][j] for j in range(left, right)])
            ans.extend([matrix[i][right] for i in range(up, down)])
            ans.extend([matrix[down][j] for j in range(right, left, -1)])
            ans.extend([matrix[i][left] for i in range(down, up, -1)])
            up, down, left, right = up + 1, down - 1, left + 1, right - 1
        if left == right:
            ans.extend([matrix[i][right] for i in range(up, down + 1)])
        elif up == down:
            ans.extend([matrix[up][j] for j in range(left, right + 1)])
        return ans

    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        return matrix and [*matrix.pop(0)] + self.spiralOrder([*zip(*matrix)][::-1])

    def spiralOrder(self, matrix):
        result = []
        while matrix:
            result.extend(matrix.pop(0))
            matrix = list(zip(*matrix))[::-1]
        return result


def test():
    matrices = [
        [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
        [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]],
        ]
    expectations = [
        [1, 2, 3, 6, 9, 8, 7, 4, 5],
        [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7],
        ]
    for matrix, expected in zip(matrices, expectations):
        solution = Solution().spiralOrder(matrix)
        assert solution == expected
