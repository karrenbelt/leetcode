### Source : https://leetcode.com/problems/substring-with-concatenation-of-all-words/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-03

##################################################################################################### 
#
# You are given a string s and an array of strings words of the same length. Return all starting 
# indices of substring(s) in s that is a concatenation of each word in words exactly once, in any 
# order, and without any intervening characters.
# 
# You can return the answer in any order.
# 
# Example 1:
# 
# Input: s = "barfoothefoobarman", words = ["foo","bar"]
# Output: [0,9]
# Explanation: Substrings starting at index 0 and 9 are "barfoo" and "foobar" respectively.
# The output order does not matter, returning [9,0] is fine too.
# 
# Example 2:
# 
# Input: s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]
# Output: []
# 
# Example 3:
# 
# Input: s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]
# Output: [6,9,12]
# 
# Constraints:
# 
# 	1 <= s.length <= 104
# 	s consists of lower-case English letters.
# 	1 <= words.length <= 5000
# 	1 <= words[i].length <= 30
# 	words[i] consists of lower-case English letters.
#####################################################################################################

from typing import List


class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:  # O(n*k) time -> TLE: 151/176 passed
        from collections import Counter
        from itertools import permutations
        word_string = ''.join(words)
        if Counter(word_string) - Counter(s):
            return []
        indices = set()
        for perm in permutations(words):
            index = s.find(''.join(perm))
            while index != -1:
                indices.add(index)
                index = s.find(''.join(perm), index + 1)
        return list(indices)

    def findSubstring(self, s: str, words: List[str]) -> List[int]:

        class TrieNode(dict):
            def __init__(self, **kwargs):
                super(TrieNode, self).__init__(**kwargs)
                self.count = 0

        def build_trie() -> TrieNode:
            trie = TrieNode()
            for word in words:
                node = trie
                for c in word:
                    if c not in node:
                        node[c] = TrieNode()
                    node = node[c]
                node.count += 1
            return trie

        def is_substring(start: int, end: int) -> bool:
            if start == end:
                return True
            node = trie
            for j in range(start, end):
                c = s[j]
                if c not in node:
                    return False
                node = node[c]
                if node.count > 0:
                    node.count -= 1
                    if is_substring(j + 1, end):
                        node.count += 1
                        return True
                    node.count += 1
            return False

        indices = []
        trie = build_trie()
        substr_length = sum(map(len, words))
        for i in range(len(s) - substr_length + 1):
            if is_substring(i, i + substr_length):
                indices.append(i)
        return indices

    def findSubstring(self, s: str, words: List[str]) -> List[int]:

        def search(start, substr_length):
            word_start = start
            match_count = {}
            while start + substr_length <= len(s):
                word = s[word_start:word_start + len(words[0])]
                word_start += len(words[0])
                if word not in word_count:
                    start = word_start
                    match_count.clear()
                else:
                    match_count[word] = match_count.get(word, 0) + 1
                    while match_count[word] > word_count[word]:
                        match_count[s[start:start + len(words[0])]] -= 1
                        start += len(words[0])
                    if word_start - start == substr_length:
                        indices.append(start)

        word_count = {}
        for word in words:
            word_count[word] = word_count.get(word, 0) + 1

        indices = []
        substr_length = len(''.join(words))
        for i in range(min(len(words[0]), len(s) - substr_length + 1)):
            search(i, substr_length)
        return indices

    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        import collections
        indices = []
        n_words = len(words)
        word_length = len(words[0])  # important hint in the problem description
        word_count = collections.Counter(words)
        for i in range(word_length):
            match_count = {}
            window = collections.deque()
            for j in range(i, len(s), word_length):
                word = s[j:j + word_length]
                if word in word_count:
                    match_count[word] = match_count.get(word, 0) + 1
                    window.append(word)
                    while match_count[word] > word_count[word]:
                        match_count[window.popleft()] -= 1
                    if len(window) == n_words:
                        indices.append(j - (n_words - 1) * word_length)
                else:
                    match_count.clear()
                    window.clear()
        return indices


def test():
    arguments = [
        ("barfoothefoobarman", ["foo", "bar"]),
        ("wordgoodgoodgoodbestword", ["word", "good", "best", "word"]),
        ("barfoofoobarthefoobarman", ["bar", "foo", "the"]),
        ("wordgoodgoodgoodbestword", ["word", "good", "best", "good"]),
    ]
    expectations = [
        [0, 9],
        [],
        [6, 9, 12],
        [8],
    ]
    for (s, words), expected in zip(arguments, expectations):
        solution = Solution().findSubstring(s, words)
        assert set(expected) == set(solution)
test()