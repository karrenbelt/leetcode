### Source : https://leetcode.com/problems/verifying-an-alien-dictionary/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# In an alien language, surprisingly they also use english lowercase letters, but possibly in a 
# different order. The order of the alphabet is some permutation of lowercase letters.
# 
# Given a sequence of words written in the alien language, and the order of the alphabet, return true 
# if and only if the given words are sorted lexicographicaly in this alien language.
# 
# Example 1:
# 
# Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
# Output: true
# Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.
# 
# Example 2:
# 
# Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
# Output: false
# Explanation: As 'd' comes after 'l' in this language, then words[0] > words[1], hence the sequence 
# is unsorted.
# 
# Example 3:
# 
# Input: words = ["apple","app"], order = "abcdefghijklmnopqrstuvwxyz"
# Output: false
# Explanation: The first three characters "app" match, and the second string is shorter (in size.) 
# According to lexicographical rules "apple" > "app", because 'l' > '&empty;', where '&empty;' is 
# defined as the blank character which is less than any other character (ore info).
# 
# Constraints:
# 
# 	1 <= words.length <= 100
# 	1 <= words[i].length <= 20
# 	order.length == 26
# 	All characters in words[i] and order are English lowercase letters.
#####################################################################################################

from typing import List


class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:  # O(n * w) time and O(w) space
        mapping = dict(zip(order, range(26)))
        value = tuple(mapping[c] for c in words[0])
        for i in range(1, len(words)):
            next_value = tuple(mapping[c] for c in words[i])
            if next_value < value:
                return False
            value = next_value
        return True

    def isAlienSorted(self, words: List[str], order: str) -> bool:
        table = str.maketrans(dict(zip(order, sorted(order))))
        word = words.pop()
        while words:
            next_word = words.pop()
            if word.translate(table) < next_word.translate(table):
                return False
            word = next_word
        return True

    def isAlienSorted(self, words: List[str], order: str) -> bool:
        table = str.maketrans(dict(zip(order, sorted(order))))
        for i in range(1, len(words)):
            if words[i - 1].translate(table) > words[i].translate(table):
                return False
        return True

    def isAlienSorted(self, words: List[str], order: str) -> bool:
        table = str.maketrans(dict(zip(order, sorted(order))))
        return all(a.translate(table) <= b.translate(table) for a, b in zip(words, words[1:]))


def test():
    arguments = [
        (["hello", "leetcode"], "hlabcdefgijkmnopqrstuvwxyz"),
        (["word", "world", "row"], "worldabcefghijkmnpqstuvxyz"),
        (["apple", "app"], "abcdefghijklmnopqrstuvwxyz"),
        ]
    expectations = [True, False, False]
    for (words, order), expected in zip(arguments, expectations):
        solution = Solution().isAlienSorted(words, order)
        assert solution == expected
