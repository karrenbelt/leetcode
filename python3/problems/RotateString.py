### Source : https://leetcode.com/problems/rotate-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-16

##################################################################################################### 
#
# We are given two strings, A and B.
# 
# A shift on A consists of taking string A and moving the leftmost character to the rightmost 
# position. For example, if A = 'abcde', then it will be 'bcdea' after one shift on A. Return True if 
# and only if A can become B after some number of shifts on A.
# 
# Example 1:
# Input: A = 'abcde', B = 'cdeab'
# Output: true
# 
# Example 2:
# Input: A = 'abcde', B = 'abced'
# Output: false
# 
# Note:
# 
# 	A and B will have length at most 100.
# 
#####################################################################################################


class Solution:
    def rotateString(self, A: str, B: str) -> bool:  # O(n^2) time and O(n) space
        if not set(A) == set(B):
            return False

        for i in range(len(A)):
            if A == B:
                return True
            A = A[1:] + A[0]

        return A == B  # empty string corner case

    def rotateString(self, A: str, B: str) -> bool:  # O(n) time and O(n) space
        return len(A) == len(B) and B in A + A


def test():
    arguments = [
        ('abcde', 'cdeab'),
        ('abcde', 'abced'),
    ]
    expectations = [True, False]
    for (A, B), expected in zip(arguments, expectations):
        solution = Solution().rotateString(A, B)
        assert solution == expected
