### Source : https://leetcode.com/problems/partition-array-into-disjoint-intervals/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-22

##################################################################################################### 
#
# Given an array nums, partition it into two (contiguous) subarrays left and right so that:
# 
# 	Every element in left is less than or equal to every element in right.
# 	left and right are non-empty.
# 	left has the smallest possible size.
# 
# Return the length of left after such a partitioning.  It is guaranteed that such a partitioning 
# exists.
# 
# Example 1:
# 
# Input: nums = [5,0,3,8,6]
# Output: 3
# Explanation: left = [5,0,3], right = [8,6]
# 
# Example 2:
# 
# Input: nums = [1,1,1,0,6,12]
# Output: 4
# Explanation: left = [1,1,1,0], right = [6,12]
# 
# Note:
# 
# 	2 <= nums.length <= 30000
# 	0 <= nums[i] <= 106
# 	It is guaranteed there is at least one way to partition nums as described.
# 
#####################################################################################################

from typing import List


class Solution:
    def partitionDisjoint(self, nums: List[int]) -> int:  # O(n) time and O(n) space, double pass
        # 1. record maxima from the left, minima from the right
        # 2. find the intersection index of maxima from the left and minima from the right
        left_maxima = [nums[0]] * len(nums)
        right_minima = [nums[-1]] * len(nums)
        for i in range(1, len(nums)):
            left_maxima[i] = max(left_maxima[i - 1], nums[i])
            right_minima[~i] = min(right_minima[~i + 1], nums[~i])
        return next(i for i, (left, right) in enumerate(zip(left_maxima, right_minima[1:])) if left <= right) + 1

    def partitionDisjoint(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        disjoint = 0
        current_maximum = left_maximum = nums[0]
        for i, n in enumerate(nums):
            current_maximum = max(current_maximum, n)
            if n < left_maximum:
                left_maximum = current_maximum
                disjoint = i
        return disjoint + 1


def test():
    arguments = [
        [5, 0, 3, 8, 6],
        [1, 1, 1, 0, 6, 12],
        [1, 1, 6, 0, 12],
        []
    ]
    expectations = [3, 4, 4]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().partitionDisjoint(nums)
        assert solution == expected
