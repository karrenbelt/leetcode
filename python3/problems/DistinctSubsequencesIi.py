### Source : https://leetcode.com/problems/distinct-subsequences-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-14

##################################################################################################### 
#
# Given a string S, count the number of distinct, non-empty subsequences of S .
# 
# Since the result may be large, return the answer modulo 109 + 7.
# 
# Example 1:
# 
# Input: "abc"
# Output: 7
# Explanation: The 7 distinct subsequences are "a", "b", "c", "ab", "ac", "bc", and "abc".
# 
# Example 2:
# 
# Input: "aba"
# Output: 6
# Explanation: The 6 distinct subsequences are "a", "b", "ab", "ba", "aa" and "aba".
# 
# Example 3:
# 
# Input: "aaa"
# Output: 3
# Explanation: The 3 distinct subsequences are "a", "aa" and "aaa".
# 
# Note:
# 
# 	S contains only lowercase letters.
# 	1 <= S.length <= 2000
# 
#####################################################################################################


class Solution:
    def distinctSubseqII(self, S: str) -> int:  # this is correct but will yield TLE, haven't submitted.

        def backtrack(i, path):
            subsequences.add(path)
            for j in range(i, len(S)):
                backtrack(j + 1, path + S[j])

        subsequences = set()
        backtrack(0, '')
        subsequences.remove('')
        return len(subsequences) % (10 ** 9 + 7)

    def distinctSubseqII(self, S: str) -> int:  # O(n) time and O(n) space
        dp = [1] * len(S)
        last_location = {S[0]: 0}
        large_prime = 10 ** 9 + 7
        for i in range(1, len(S)):
            character = S[i]
            if character not in last_location:
                dp[i] = sum(dp[:i]) + 1 % large_prime
            else:
                dp[i] = sum(dp[last_location[character]: i]) % large_prime
            last_location[character] = i
        return sum(dp) % large_prime

    def distinctSubseqII(self, S: str) -> int:  # O(n) time and O(n) space
        from collections import Counter
        res, end = 0, Counter()
        for c in S:
            res, end[c] = res * 2 + 1 - end[c], res + 1
        return res % (10**9 + 7)

    def distinctSubseqII(self, S: str) -> int:  # O(n) time and O(1) space
        endswith = [0] * 26
        for c in S:
            endswith[ord(c) - ord('a')] = sum(endswith) + 1
        return sum(endswith) % (10 ** 9 + 7)


def test():
    arguments = ["abc", "aba", "aaa", "abcd" * 4]
    expectations = [7, 6, 3]
    for S, expected in zip(arguments, expectations):
        solution = Solution().distinctSubseqII(S)
        assert solution == expected
