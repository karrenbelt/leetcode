### Source : https://leetcode.com/problems/2-keys-keyboard/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-19

##################################################################################################### 
#
# Initially on a notepad only one character 'A' is present. You can perform two operations on this 
# notepad for each step:
# 
# 	Copy All: You can copy all the characters present on the notepad (partial copy is not 
# allowed).
# 	Paste: You can paste the characters which are copied last time.
# 
# Given a number n. You have to get exactly n 'A' on the notepad by performing the minimum number of 
# steps permitted. Output the minimum number of steps to get n 'A'.
# 
# Example 1:
# 
# Input: 3
# Output: 3
# Explanation:
# Intitally, we have one character 'A'.
# In step 1, we use Copy All operation.
# In step 2, we use Paste operation to get 'AA'.
# In step 3, we use Paste operation to get 'AAA'.
# 
# Note:
# 
# 	The n will be in the range [1, 1000].
# 
#####################################################################################################


class Solution:
    def minSteps(self, n: int) -> int:  # O(n) time and O(1) space
        ans = 0
        for i in range(2, n + 1):
            while n and n % i == 0:
                ans += i
                n //= i
        return ans

    def minSteps(self, n: int) -> int:  # integer factorization: O(sqrt(n) * log(n)) time
        for i in range(2, int(n**0.5) + 1):
            if n % i == 0:
                return self.minSteps(n // i) + i
        return 0 if n == 1 else n

    def minSteps(self, n: int) -> int:  # top down dp: O(n^2) time
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def func(n: int):
            if n == 1:
                return 0
            return min(func(i) + n // i for i in range(1, n // 2 + 1) if n % i == 0)

        return func(n)

    def minSteps(self, n: int) -> int:  # dp: O(n) time and O(n) space
        if n == 1:
            return 0
        dp = [0] * (n + 1)
        for i in range(2, n + 1):
            dp[i] = i
            for j in range(i // 2, 1, -1):
                if i % j == 0:
                    dp[i] = dp[j] + (i // j)
                    break
        return dp[n]


def test():
    arguments = [3]
    expectations = [3]
    for n, expected in zip(arguments, expectations):
        solution = Solution().minSteps(n)
        assert solution == expected
