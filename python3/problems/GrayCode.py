### Source : https://leetcode.com/problems/gray-code/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-16

##################################################################################################### 
#
# The gray code is a binary numeral system where two successive values differ in only one bit.
# 
# Given an integer n representing the total number of bits in the code, return any sequence of gray 
# code.
# 
# A gray code sequence must begin with 0.
# 
# Example 1:
# 
# Input: n = 2
# Output: [0,1,3,2]
# Explanation:
# 00 - 0
# 01 - 1
# 11 - 3
# 10 - 2
# [0,2,3,1] is also a valid gray code sequence.
# 00 - 0
# 10 - 2
# 11 - 3
# 01 - 1
# 
# Example 2:
# 
# Input: n = 1
# Output: [0,1]
# 
# Constraints:
# 
# 	1 <= n <= 16
#####################################################################################################

from typing import List


class Solution:
    # 0 - 1
    # 00 - 01 - 11 - 10
    # 000 - 001 - 010 - 011 - 111 - 110 - 100 - 101 - 110
    # 0000 - 0001 - 0011 - 0111 - 1111 - 1110 - 1100 - 1000 - 1001 - 1011 - 1010
    def grayCode(self, n: int) -> List[int]:  # O(2^n) time

        def dfs(path):
            if len(path) == n:
                return [path]
            return dfs(path + '0') + list(reversed(dfs(path + '1')))

        return list(map(lambda x: int(x, 2), dfs('')))

    def grayCode(self, n: int) -> List[int]:  # O(2^n) time
        sequence = [0]
        for i in range(n):
            for j in range(len(sequence) - 1, -1, -1):
                sequence.append(1 << i | sequence[j])
        return sequence

    def grayCode(self, n: int) -> List[int]:  # O(2^n) time
        sequence = [0]
        for i in range(n):
            sequence += [x + pow(2, i) for x in reversed(sequence)]
        return sequence

    def grayCode(self, n: int) -> List[int]:  # O(2^n) time
        return (s := []) or s.append(0) or any(s.extend([x + pow(2, i) for x in reversed(s)]) for i in range(n)) or s

    def grayCode(self, n: int) -> List[int]:  # O(2^n) time
        return (lambda r: r + [x | 1 << n - 1 for x in r[::-1]])(self.grayCode(n - 1)) if n else [0]

    def grayCode(self, n: int) -> List[int]:  # O(2^n) time
        return [(x >> 1) ^ x for x in range(1 << n)]


def test():
    arguments = [2, 1, 3]
    expectations = [
        ([0, 1, 3, 2], [0, 2, 3, 1]),
        ([0, 1],),
        ([0, 1, 3, 2, 6, 7, 5, 4],),
        ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().grayCode(n)
        assert solution in expected, solution
test()