### Source : https://leetcode.com/problems/count-numbers-with-unique-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Given an integer n, return the count of all numbers with unique digits, x, where 0 <= x < 10n.
# 
# Example 1:
# 
# Input: n = 2
# Output: 91
# Explanation: The answer should be the total numbers in the range of 0 &le; x < 100, excluding 
# 11,22,33,44,55,66,77,88,99
# 
# Example 2:
# 
# Input: n = 0
# Output: 1
# 
# Constraints:
# 
# 	0 <= n <= 8
#####################################################################################################


class Solution:
    def countNumbersWithUniqueDigits(self, n: int) -> int:
        # 0 = 1
        # 1 = 10
        # 2 = 9 * 9
        # 3 = 9 * 9 * 8
        # 4 = 9 * 9 * 8 * 7
        # 10 and over:
        choices = [9, 9, 8, 7, 6, 5, 4, 3, 2, 1]
        ctr, product = 1, 1
        for i in range(min(n, 10)):
            product *= choices[i]
            ctr += product
        return ctr

    def countNumbersWithUniqueDigits(self, n: int) -> int:
        if n == 0:
            return 1
        ctr = tmp = 9
        for i in range(2, min(n, 10) + 1):
            tmp *= 10 - i + 1
            ctr += tmp
        return ctr + 1


def test():
    arguments = [2, 0, 1, 50]  # anything over 9 is the same: 8877691
    expectations = [91, 1, 10, 8877691]
    for n, expected in zip(arguments, expectations):
        solution = Solution().countNumbersWithUniqueDigits(n)
        assert solution == expected
test()