### Source : https://leetcode.com/problems/goal-parser-interpretation/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

##################################################################################################### 
#
# You own a Goal Parser that can interpret a string command. The command consists of an alphabet of 
# "G", "()" and/or "(al)" in some order. The Goal Parser will interpret "G" as the string "G", "()" 
# as the string "o", and "(al)" as the string "al". The interpreted strings are then concatenated in 
# the original order.
# 
# Given the string command, return the Goal Parser's interpretation of command.
# 
# Example 1:
# 
# Input: command = "G()(al)"
# Output: "Goal"
# Explanation: The Goal Parser interprets the command as follows:
# G -> G
# () -> o
# (al) -> al
# The final concatenated result is "Goal".
# 
# Example 2:
# 
# Input: command = "G()()()()(al)"
# Output: "Gooooal"
# 
# Example 3:
# 
# Input: command = "(al)G(al)()()G"
# Output: "alGalooG"
# 
# Constraints:
# 
# 	1 <= command.length <= 100
# 	command consists of "G", "()", and/or "(al)" in some order.
#####################################################################################################


class Solution:
    def interpret(self, command: str) -> str:  # O(n) time and O(1) space
        return command.replace("()", "o").replace("(al)", "al")

    def interpret(self, command: str) -> str:  # O(n) time and O(n) space
        import re
        mapping = {"()": "o", "(al)": "al"}
        pattern = re.compile("|".join(map(re.escape, mapping)))
        return pattern.sub(lambda match: mapping[match.group(0)], command)

    def interpret(self, command: str) -> str:  # O(n) time
        import re
        return re.sub(r'\(\)', 'o', re.sub(r'\(al\)', 'al', command))

    def interpret(self, command: str) -> str:  # O(n) time and O(n) space, single pass
        s, i = '', 0
        while i < len(command):
            s, i = (s+'G', i+1) if command[i] == 'G' else (s+'o', i+2) if command[i:i+2] == '()' else (s+'al', i+4)
        return s


def test():
    arguments = ["G()(al)", "G()()()()(al)", "(al)G(al)()()G"]
    expectations = ["Goal", "Gooooal", "alGalooG"]
    for command, expected in zip(arguments, expectations):
        solution = Solution().interpret(command)
        assert solution == expected
