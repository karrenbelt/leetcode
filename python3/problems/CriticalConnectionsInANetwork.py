### Source : https://leetcode.com/problems/critical-connections-in-a-network/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-24

##################################################################################################### 
#
# There are n servers numbered from 0 to n-1 connected by undirected server-to-server connections 
# forming a network where connections[i] = [a, b] represents a connection between servers a and b. 
# Any server can reach any other server directly or indirectly through the network.
# 
# A critical connection is a connection that, if removed, will make some server unable to reach some 
# other server.
# 
# Return all critical connections in the network in any order.
# 
# Example 1:
# 
# Input: n = 4, connections = [[0,1],[1,2],[2,0],[1,3]]
# Output: [[1,3]]
# Explanation: [[3,1]] is also accepted.
# 
# Constraints:
# 
# 	1 <= n <= 105
# 	n-1 <= connections.length <= 105
# 	connections[i][0] != connections[i][1]
# 	There are no repeated connections.
#####################################################################################################

from typing import List


class Solution:
    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:

        graph = {}
        for a, b in connections:
            graph.setdefault(a, set()).add(b)
            graph.setdefault(b, set()).add(a)

        def dfs(lowest_rank, seen, current_rank, prev_vertex, current_vertex):
            seen.add(current_vertex)
            lowest_rank[current_vertex] = current_rank
            for next_vertex in graph.get(current_vertex, []):
                if next_vertex == prev_vertex:
                    continue  # don't move back: might be critical
                if next_vertex not in seen:
                    dfs(lowest_rank, seen, current_rank + 1, current_vertex, next_vertex)
                lowest_rank[current_vertex] = min(lowest_rank[current_vertex], lowest_rank[next_vertex])
                if lowest_rank[next_vertex] >= current_vertex + 1:
                    critical_connections.append([current_vertex, next_vertex])

        critical_connections = []
        dfs(list(range(n)), set(), 0, -1, 0)
        return critical_connections

    def criticalConnections(self, n: int, connections: List[List[int]]) -> List[List[int]]:

        def dfs(rank: int, node: int, prev: int):
            lowest_rank[node], result = rank, []
            for neighbor in graph[node]:
                if neighbor == prev:
                    continue
                if not lowest_rank[neighbor]:
                    result += dfs(rank + 1, neighbor, node)
                lowest_rank[node] = min(lowest_rank[node], lowest_rank[neighbor])
                if lowest_rank[neighbor] >= rank + 1:
                    result.append([node, neighbor])
            return result

        lowest_rank = [0] * n
        graph = {}
        for a, b in connections:
            graph.setdefault(a, set()).add(b)
            graph.setdefault(b, set()).add(a)
        return dfs(1, 0, -1)


def test():
    arguments = [
        (4, [[0, 1], [1, 2], [2, 0], [1, 3]]),
        (6, [[0, 1], [1, 2], [2, 0], [0, 3], [3, 4], [4, 5], [5, 3]]),  # two cycles connected by a crucial connection
        (6,  [[0, 1], [1, 2], [2, 0], [0, 3], [1, 4], [3, 4], [4, 5], [5, 3]]),  # two connections, none crucial
        (1000, ),
        ]
    expectations = [
        [[1, 3]],  # [[3,1]]
        [[0, 3]],
        []
    ]
    for (n, connections), expected in zip(arguments, expectations):
        solution = Solution().criticalConnections(n, connections)
        assert set(map(frozenset, solution)) == set(map(frozenset, expected))
