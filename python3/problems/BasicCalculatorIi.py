### Source : https://leetcode.com/problems/basic-calculator-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-09

##################################################################################################### 
#
# Given a string s which represents an expression, evaluate this expression and return its value. 
# 
# The integer division should truncate toward zero.
# 
# Example 1:
# Input: s = "3+2*2"
# Output: 7
# Example 2:
# Input: s = " 3/2 "
# Output: 1
# Example 3:
# Input: s = " 3+5 / 2 "
# Output: 5
# 
# Constraints:
# 
# 	1 <= s.length <= 3 * 105
# 	s consists of integers and operators ('+', '-', '*', '/') separated by some number of 
# spaces.
# 	s represents a valid expression.
# 	All the integers in the expression are non-negative integers in the range [0, 231 - 1].
# 	The answer is guaranteed to fit in a 32-bit integer.
#####################################################################################################


class Solution:
    def calculate(self, s: str) -> int:
        return eval(s.replace('/', '//'))

    def calculate(self, s: str) -> int:  # O(n) time and O(n) space
        num, stack, sign = 0, [], "+"
        for i, c in enumerate(s):
            if c.isdigit():
                num = num * 10 + int(c)
            if c in "+-*/" or i == len(s) - 1:
                if sign == "+":
                    stack.append(num)
                elif sign == "-":
                    stack.append(-num)
                elif sign == "*":
                    stack.append(stack.pop() * num)
                else:
                    stack.append(stack.pop() // num)
                num, sign = 0, c
        return sum(stack)


def test():
    arguments = [
        "3+2*2",
        " 3/2 ",
        " 3+5 / 2 ",
        "14/3*2",
    ]
    expectations = [7, 1, 5, 8]
    for s, expected in zip(arguments, expectations):
        solution = Solution().calculate(s)
        assert solution == expected
