### Source : https://leetcode.com/problems/text-justification/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-13

##################################################################################################### 
#
# Given an array of words and a width maxWidth, format the text such that each line has exactly 
# maxWidth characters and is fully (left and right) justified.
# 
# You should pack your words in a greedy approach; that is, pack as many words as you can in each 
# line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
# 
# Extra spaces between words should be distributed as evenly as possible. If the number of spaces on 
# a line do not divide evenly between words, the empty slots on the left will be assigned more spaces 
# than the slots on the right.
# 
# For the last line of text, it should be left justified and no extra space is inserted between words.
# 
# Note:
# 
# 	A word is defined as a character sequence consisting of non-space characters only.
# 	Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
# 	The input array words contains at least one word.
# 
# Example 1:
# 
# Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
# Output:
# [
#    "This    is    an",
#    "example  of text",
#    "justification.  "
# ]
# 
# Example 2:
# 
# Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
# Output:
# [
#   "What   must   be",
#   "acknowledgment  ",
#   "shall be        "
# ]
# Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last 
# line must be left-justified instead of fully-justified.
# Note that the second line is also left-justified becase it contains only one word.
# 
# Example 3:
# 
# Input: words = 
# ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","
# is","everything","else","we","do"], maxWidth = 20
# Output:
# [
#   "Science  is  what we",
#   "understand      well",
#   "enough to explain to",
#   "a  computer.  Art is",
#   "everything  else  we",
#   "do                  "
# ]
# 
# Constraints:
# 
# 	1 <= words.length <= 300
# 	1 <= words[i].length <= 20
# 	words[i] consists of only English letters and symbols.
# 	1 <= maxWidth <= 100
# 	words[i].length <= maxWidth
#####################################################################################################

from typing import List


class Solution:
    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        line, lines, i = [], [], 0
        while i < len(words):
            # length of all words, + len of words (spaces) + next word, minus space at start and end
            while i < len(words) and sum(map(len, line)) + len(line) + len(words[i]) - 1 < maxWidth:
                line.append(words[i])
                i += 1
            if len(line) == 1 or i == len(words):  # left justify
                formatted = ' '.join(line)
                formatted += ' ' * (maxWidth - len(formatted))
            else:
                n_spaces = maxWidth - sum(map(len, line))
                q, r = divmod(n_spaces, len(line) - 1)
                formatted = []
                for j in range(len(line) - 1):
                    formatted.append(line[j] + ' ' * (q + (r > 0)))
                    r -= 1
                formatted.append(line[-1])
            lines.append(''.join(formatted))
            line.clear()
        return lines

    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        lines, line, num_of_letters = [], [], 0
        for word in words:
            if num_of_letters + len(word) + len(line) > maxWidth:
                for i in range(maxWidth - num_of_letters):
                    line[i % (len(line) - 1 or 1)] += ' '
                lines.append(''.join(line))
                line, num_of_letters = [], 0
            line += [word]
            num_of_letters += len(word)
        return lines + [' '.join(line).ljust(maxWidth)]


def test():
    arguments = [
        (["This", "is", "an", "example", "of", "text", "justification."], 16),
        (["What", "must", "be", "acknowledgment", "shall", "be"], 16),
        (["Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art",
          "is", "everything", "else", "we", "do"], 20),
        ]
    expectations = [
        [
            "This    is    an",
            "example  of text",
            "justification.  "
        ],
        [
            "What   must   be",
            "acknowledgment  ",
            "shall be        "
        ],
        [
            "Science  is  what we",
            "understand      well",
            "enough to explain to",
            "a  computer.  Art is",
            "everything  else  we",
            "do                  "
        ],
    ]
    for (words, maxWidth), expected in zip(arguments, expectations):
        solution = Solution().fullJustify(words, maxWidth)
        assert solution == expected
