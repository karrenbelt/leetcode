### Source : https://leetcode.com/problems/number-of-sub-arrays-with-odd-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-19

##################################################################################################### 
#
# Given an array of integers arr. Return the number of sub-arrays with odd sum.
# 
# As the answer may grow large, the answer must be computed modulo 109 + 7.
# 
# Example 1:
# 
# Input: arr = [1,3,5]
# Output: 4
# Explanation: All sub-arrays are [[1],[1,3],[1,3,5],[3],[3,5],[5]]
# All sub-arrays sum are [1,4,9,3,8,5].
# Odd sums are [1,9,3,5] so the answer is 4.
# 
# Example 2:
# 
# Input: arr = [2,4,6]
# Output: 0
# Explanation: All sub-arrays are [[2],[2,4],[2,4,6],[4],[4,6],[6]]
# All sub-arrays sum are [2,6,12,4,10,6].
# All sub-arrays have even sum and the answer is 0.
# 
# Example 3:
# 
# Input: arr = [1,2,3,4,5,6,7]
# Output: 16
# 
# Example 4:
# 
# Input: arr = [100,100,99,99]
# Output: 4
# 
# Example 5:
# 
# Input: arr = [7]
# Output: 1
# 
# Constraints:
# 
# 	1 <= arr.length <= 105
# 	1 <= arr[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def numOfSubarrays(self, arr: List[int]) -> int:  # O(n^3) time, should yield TLE
        return sum(sum(arr[i:j]) % 2 for i in range(len(arr)) for j in range(i + 1, len(arr) + 1)) % (10 ** 9 + 7)

    def numOfSubarrays(self, arr: List[int]) -> int:  # O(n) time and O(n) space
        # initialize at 0, which is even and hence prefix sum counter is initialized at 1 for even prefixes, 0 for odd
        subarray_ctr = 0
        even_ctr, odd_ctr = 1, 0
        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in arr]
        for prefix in prefix_sum:
            if prefix % 2:
                subarray_ctr += even_ctr
                odd_ctr += 1
            else:
                subarray_ctr += odd_ctr
                even_ctr += 1
        return subarray_ctr % (10 ** 9 + 7)

    def numOfSubarrays(self, arr: List[int]) -> int:  # O(n) time and O(1) space, single pass
        # we start with 0, which is even and initialize the parity (prefix) sum counter for even numbers at one
        parity_ctr = [1, 0]
        subarray_ctr = parity = 0
        for n in arr:
            parity ^= n & 1
            subarray_ctr += parity_ctr[1 - parity]
            parity_ctr[parity] += 1
        return subarray_ctr % (10 ** 9 + 7)

    def numOfSubarrays(self, arr: List[int]) -> int:  # O(n) time and O(1) space
        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in arr]
        odd = sum(prefix % 2 for prefix in prefix_sum)
        return odd * (len(arr) - odd + 1) % (10 ** 9 + 7)

    def numOfSubarrays(self, arr: List[int]) -> int:  # O(n) time and O(1) space
        return o * (len(arr) - o + 1) % (10 ** 9 + 7) if (o := (p := 0) or sum(p := (p + n) % 2 for n in arr)) else 0


def test():
    arguments = [
        [1, 3, 5],
        [2, 4, 6],
        [1, 2, 3, 4, 5, 6, 7],
        [100, 100, 99, 99],
        [7],
    ]
    expectations = [4, 0, 16, 4, 1]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().numOfSubarrays(arr)
        assert solution == expected
