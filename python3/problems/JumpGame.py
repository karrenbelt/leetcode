### Source : https://leetcode.com/problems/jump-game/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-25

##################################################################################################### 
#
# Given an array of non-negative integers, you are initially positioned at the first index of the 
# array.
# 
# Each element in the array represents your maximum jump length at that position.
# 
# Determine if you are able to reach the last index.
# 
# Example 1:
# 
# Input: [2,3,1,1,4]
# Output: true
# Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
# 
# Example 2:
# 
# Input: [3,2,1,0,4]
# Output: false
# Explanation: You will always arrive at index 3 no matter what. Its maximum
#              jump length is 0, which makes it impossible to reach the last index.
# 
#####################################################################################################
from typing import List


class Solution:
    def canJump(self, nums: List[int]) -> bool:  # O(n) time and O(1) space
        max_jump = 0
        for i, n in enumerate(nums):
            if i <= max_jump < i + n:
                max_jump = i + n
        return max_jump >= len(nums) - 1

    def canJump(self, nums: List[int]) -> bool:  # O(n) time and O(1) space
        max_jump = 0
        for i, n in enumerate(nums):
            if i > max_jump:
                return False
            max_jump = max(max_jump, i + n)
        return True

    def canJump(self, nums: List[int]) -> bool:  # O(n) time and O(1) space
        goal = len(nums) - 1
        for i in range(len(nums))[::-1]:
            if i + nums[i] >= goal:
                goal = i
        return not goal


def test():
    arrays_of_numbers = [
        [2, 3, 1, 1, 4],
        [3, 2, 1, 0, 4],
        ]
    expectations = [True, False]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().canJump(nums)
        assert solution == expected
