### Source : https://leetcode.com/problems/palindrome-partitioning-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-23

##################################################################################################### 
#
# Given a string s, partition s such that every substring of the partition is a palindrome.
# 
# Return the minimum cuts needed for a palindrome partitioning of s.
# 
# Example 1:
# 
# Input: s = "aab"
# Output: 1
# Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
# 
# Example 2:
# 
# Input: s = "a"
# Output: 0
# 
# Example 3:
# 
# Input: s = "ab"
# Output: 1
# 
# Constraints:
# 
# 	1 <= s.length <= 2000
# 	s consists of lower-case English letters only.
#####################################################################################################

from typing import List


class Solution:
    def minCut(self, s: str) -> int:  # wrong: 30 / 33 test cases passed.
        # idea; repeatedly find the largest palindromic substring
        from python3.problems.LongestPalindromicSubstring import Solution as Previous  # Manacher: O(n) time

        ctr = 0
        sequences = [s]
        while sequences:
            sub_sequences = []
            for sequence in sequences:
                lps = Previous().longestPalindrome(sequence)
                ctr += sequence.count(lps)
                sub_sequences.extend(filter(None, sequence.split(lps)))
            sequences = sub_sequences
        return ctr - 1

    # def minCut(self, s: str) -> int:  # O(n^3) time and O(n) space
    #     cut = list(range(-1, len(s)))
    #     for i in range(len(s)):
    #         for j in range(i, len(s)):
    #             if s[i:j] == s[j:i:-1]:
    #                 cut[j + 1] = min(cut[j + 1], cut[i] + 1)
    #     return cut[-1]
    #
    def minCut(self, s: str) -> int:  # O(n^2) time and O(n) space

        if s == s[::-1]:
            return 0
        for i in range(1, len(s)):
            if s[:i] == s[:i][::-1] and s[i:] == s[i:][::-1]:
                return 1

        cut = [x for x in range(-1, len(s))]
        for i in range(len(s)):
            r1, r2 = 0, 0
            # use i as origin, and gradually enlarge radius if a palindrome exists
            while i - r1 >= 0 and i + r1 < len(s) and s[i - r1] == s[i + r1]:  # odd palindrome
                cut[i + r1 + 1] = min(cut[i + r1 + 1], cut[i - r1] + 1)
                r1 += 1
            while i - r2 >= 0 and i + r2 + 1 < len(s) and s[i - r2] == s[i + r2 + 1]:  # even palindrome
                cut[i + r2 + 2] = min(cut[i + r2 + 2], cut[i - r2] + 1)
                r2 += 1
        return cut[-1]


def test():
    # what if abbaabbba?
    # longest  bbaabb  , leaves ['a', 'ba'] -> need a second cut
    # else:   abba abbba -> done in one cut
    arguments = ["aab", "a", "ab", "ababbbabbaba", "abbaabbba",
                 "adabdcaebdcebdcacaaaadbbcadabcbeabaadcbcaaddebdbddcbdacdbbaedbdaaecabdceddccbdeeddccdaabbabbdedaaabcdadbdabeacbeadbaddcbaacdbabcccbaceedbcccedbeecbccaecadccbdbdccbcbaacccbddcccbaedbacdbcaccdcaadcbaebebcceabbdcdeaabdbabadeaaaaedbdbcebcbddebccacacddebecabccbbdcbecbaeedcdacdcbdbebbacddddaabaedabbaaabaddcdaadcccdeebcabacdadbaacdccbeceddeebbbdbaaaaabaeecccaebdeabddacbedededebdebabdbcbdcbadbeeceecdcdbbdcbdbeeebcdcabdeeacabdeaedebbcaacdadaecbccbededceceabdcabdeabbcdecdedadcaebaababeedcaacdbdacbccdbcece"
        ]
    expectations = [1, 0, 1, 3, 1, 273]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minCut(s)
        assert solution == expected, (s, expected, solution)

test()
