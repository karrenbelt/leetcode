### Source : https://leetcode.com/problems/check-if-there-is-a-valid-path-in-a-grid/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-10

##################################################################################################### 
#
# Given a m x n grid. Each cell of the grid represents a street. The street of grid[i][j] can be:
# 
# 	1 which means a street connecting the left cell and the right cell.
# 	2 which means a street connecting the upper cell and the lower cell.
# 	3 which means a street connecting the left cell and the lower cell.
# 	4 which means a street connecting the right cell and the lower cell.
# 	5 which means a street connecting the left cell and the upper cell.
# 	6 which means a street connecting the right cell and the upper cell.
# 
# You will initially start at the street of the upper-left cell (0,0). A valid path in the grid is a 
# path which starts from the upper left cell (0,0) and ends at the bottom-right cell (m - 1, n - 1). 
# The path should only follow the streets.
# 
# Notice that you are not allowed to change any street.
# 
# Return true if there is a valid path in the grid or false otherwise.
# 
# Example 1:
# 
# Input: grid = [[2,4,3],[6,5,2]]
# Output: true
# Explanation: As shown you can start at cell (0, 0) and visit all the cells of the grid to reach (m 
# - 1, n - 1).
# 
# Example 2:
# 
# Input: grid = [[1,2,1],[1,2,1]]
# Output: false
# Explanation: As shown you the street at cell (0, 0) is not connected with any street of any other 
# cell and you will get stuck at cell (0, 0)
# 
# Example 3:
# 
# Input: grid = [[1,1,2]]
# Output: false
# Explanation: You will get stuck at cell (0, 1) and you cannot reach cell (0, 2).
# 
# Example 4:
# 
# Input: grid = [[1,1,1,1,1,1,3]]
# Output: true
# 
# Example 5:
# 
# Input: grid = [[2],[2],[2],[2],[2],[2],[6]]
# Output: true
# 
# Constraints:
# 
# 	m == grid.length
# 	n == grid[i].length
# 	1 <= m, n <= 300
# 	1 <= grid[i][j] <= 6
#####################################################################################################

from typing import List


class Solution:
    def hasValidPath(self, grid: List[List[int]]) -> bool:
        moves = {
            1: [(0, 1), (0, -1)],
            2: [(1, 0), (-1, 0)],
            3: [(1, 0), (0, -1)],
            4: [(0, 1), (1, 0)],
            5: [(-1, 0), (0, -1)],
            6: [(-1, 0), (0, 1)],
        }
        stack, seen = [(0, 0)], set()
        while stack:
            i, j = stack.pop()
            if (i, j) == (len(grid) - 1, len(grid[0]) - 1):
                return True
            for di, dj in moves[grid[i][j]]:
                x, y = i + di, j + dj
                in_bound = 0 <= x < len(grid) and 0 <= y < len(grid[0])
                if in_bound and (x, y) not in seen and (-di, -dj) in moves[grid[x][y]]:
                    seen.add((x, y))
                    stack.append((x, y))
        return False

    def hasValidPath(self, grid: List[List[int]]) -> bool:
        left, right, up, down = (0, -1, {1, 4, 6}), (0, 1, {1, 3, 5}), (-1, 0, {2, 3, 4}), (1, 0, {2, 5, 6})
        directions = {
            1: [right, left],
            2: [up, down],
            3: [left, down],
            4: [right, down],
            5: [left, up],
            6: [right, up]
        }

        def dfs(i: int, j: int) -> bool:
            if i == len(grid) - 1 and j == len(grid[0]) - 1:
                return True

            visited.add((i, j))
            for di, dj, valid_connections in directions[grid[i][j]]:
                x, y = i + di, j + dj
                on_grid = 0 <= x < len(grid) and 0 <= y < len(grid[0])
                if on_grid and grid[x][y] in valid_connections and (x, y) not in visited:
                    if dfs(x, y):
                        return True
            return False

        visited = set()
        return dfs(0, 0)


def test():
    arguments = [
        [[2, 4, 3], [6, 5, 2]],
        [[1, 2, 1], [1, 2, 1]],
        [[1, 1, 2]],
        [[1, 1, 1, 1, 1, 1, 3]],
        [[2], [2], [2], [2], [2], [2], [6]],
        [[2, 6]]
    ]
    expectations = [True, False, False, True, True, False]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().hasValidPath(grid)
        assert solution == expected
