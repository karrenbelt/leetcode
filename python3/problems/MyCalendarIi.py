### Source : https://leetcode.com/problems/my-calendar-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# You are implementing a program to use as your calendar. We can add a new event if adding the event 
# will not cause a triple booking.
# 
# A triple booking happens when three events have some non-empty intersection (i.e., some moment is 
# common to all the three events.).
# 
# The event can be represented as a pair of integers start and end that represents a booking on the 
# half-open interval [start, end), the range of real numbers x such that start <= x < end.
# 
# Implement the yCalendarTwo class:
# 
# 	yCalendarTwo() Initializes the calendar object.
# 	boolean book(int start, int end) Returns true if the event can be added to the calendar 
# successfully without causing a triple booking. Otherwise, return false and do not add the event to 
# the calendar.
# 
# Example 1:
# 
# Input
# ["yCalendarTwo", "book", "book", "book", "book", "book", "book"]
# [[], [10, 20], [50, 60], [10, 40], [5, 15], [5, 10], [25, 55]]
# Output
# [null, true, true, true, false, true, true]
# 
# Explanation
# yCalendarTwo myCalendarTwo = new yCalendarTwo();
# myCalendarTwo.book(10, 20); // return True, The event can be booked. 
# myCalendarTwo.book(50, 60); // return True, The event can be booked. 
# myCalendarTwo.book(10, 40); // return True, The event can be double booked. 
# myCalendarTwo.book(5, 15);  // return False, The event ca not be booked, because it would result in 
# a triple booking.
# myCalendarTwo.book(5, 10); // return True, The event can be booked, as it does not use time 10 
# which is already double booked.
# myCalendarTwo.book(25, 55); // return True, The event can be booked, as the time in [25, 40) will 
# be double booked with the third event, the time [40, 50) will be single booked, and the time [50, 
# 55) will be double booked with the second event.
# 
# Constraints:
# 
# 	0 <= start < end <= 109
# 	At most 1000 calls will be made to book.
#####################################################################################################

class MyCalendarTwo:

    def __init__(self):
        self.events = []
        self.overlaps = []

    def book(self, start: int, end: int) -> bool:  # O(n^2) time
        if any(start < j and end > i for i, j in self.overlaps):
            return False
        for i, j in self.events:
            if start < j and end > i:
                self.overlaps.append((max(start, i), min(end, j)))
        self.events.append((start, end))
        return True


class MyCalendarTwo:
    def __init__(self):
        self.events = []

    def book(self, start: int, end: int) -> bool:  # O(n^2) time
        import bisect
        bisect.insort(self.events, (start, 1))
        bisect.insort(self.events, (end, -1))
        cur = 0
        for end, point in self.events:
            cur += point
            if cur >= 3:
                self.events.remove((start, 1))
                self.events.remove((end, -1))
                return False
        return True


class MyCalendarTwo:

    def __init__(self):
        self.events = [(-1 << 31, 0), ((1 << 31) - 1, 0)]

    def book(self, start: int, end: int) -> bool:
        import bisect
        i = bisect.bisect_right(self.events, (start, 1))
        j = bisect.bisect_left(self.events, (end, 1))
        if i % 2 and i == j:
            self.events[i:i] = [(start, 1), (end, 1)]
            return True
        # some overlap exists


# Your MyCalendarTwo object will be instantiated and called as such:
# obj = MyCalendarTwo()
# param_1 = obj.book(start,end)


def test():
    operations = ["MyCalendarTwo", "book", "book", "book", "book", "book", "book"]
    arguments = [[], [10, 20], [50, 60], [10, 40], [5, 15], [5, 10], [25, 55]]
    expectations = [None, True, True, True, False, True, True]
    self = obj = MyCalendarTwo(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
