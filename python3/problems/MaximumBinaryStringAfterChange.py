### Source : https://leetcode.com/problems/maximum-binary-string-after-change/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-03

##################################################################################################### 
#
# You are given a binary string binary consisting of only 0's or 1's. You can apply each of the 
# following operations any number of times:
# 
# 	Operation 1: If the number contains the substring "00", you can replace it with "10".
# 
# 		For example, "00010" -> "10010"
# 
# 	Operation 2: If the number contains the substring "10", you can replace it with "01".
# 
# 		For example, "00010" -> "00001"
# 
# Return the maximum binary string you can obtain after any number of operations. Binary string x is 
# greater than binary string y if x's decimal representation is greater than y's decimal 
# representation.
# 
# Example 1:
# 
# Input: binary = "000110"
# Output: "111011"
# Explanation: A valid transformation sequence can be:
# "000110" -> "000101" 
# "000101" -> "100101" 
# "100101" -> "110101" 
# "110101" -> "110011" 
# "110011" -> "111011"
# 
# Example 2:
# 
# Input: binary = "01"
# Output: "01"
# Explanation: "01" cannot be transformed any further.
# 
# Constraints:
# 
# 	1 <= binary.length <= 105
# 	binary consist of '0' and '1'.
#####################################################################################################


class Solution:
    def maximumBinaryString(self, binary: str) -> str:  # O(n) time and O(n) space
        i_zero, n_zeroes = binary.find('0'), binary.count('0')
        return '1' * (i_zero + n_zeroes - 1) + '0' + '1' * (len(binary) - n_zeroes - i_zero) if n_zeroes else binary

    def maximumBinaryString(self, binary: str) -> str:
        k = binary.count('1', binary.find('0'))
        return '1' * (len(binary) - k - 1) + '0' + '1' * k if '0' in binary else binary


def test():
    arguments = ["000110", "01", "11111"]
    expectations = ["111011", "01", "11111"]
    for binary, expected in zip(arguments, expectations):
        solution = Solution().maximumBinaryString(binary)
        assert solution == expected
