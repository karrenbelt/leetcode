### Source : https://leetcode.com/problems/merge-nodes-in-between-zeros/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-22

##################################################################################################### 
#
# You are given the head of a linked list, which contains a series of integers separated by 0's. The 
# beginning and end of the linked list will have Node.val == 0.
# 
# For every two consecutive 0's, merge all the nodes lying in between them into a single node whose 
# value is the sum of all the merged nodes. The modified list should not contain any 0's.
# 
# Return the head of the modified linked list.
# 
# Example 1:
# 
# Input: head = [0,3,1,0,4,5,2,0]
# Output: [4,11]
# Explanation: 
# The above figure represents the given linked list. The modified list contains
# - The sum of the nodes marked in green: 3 + 1 = 4.
# - The sum of the nodes marked in red: 4 + 5 + 2 = 11.
# 
# Example 2:
# 
# Input: head = [0,1,0,3,0,2,2,0]
# Output: [1,3,4]
# Explanation: 
# The above figure represents the given linked list. The modified list contains
# - The sum of the nodes marked in green: 1 = 1.
# - The sum of the nodes marked in red: 3 = 3.
# - The sum of the nodes marked in yellow: 2 + 2 = 4.
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [3, 2 * 105].
# 	0 <= Node.val <= 1000
# 	There are no two consecutive nodes with Node.val == 0.
# 	The beginning and end of the linked list have Node.val == 0.
#####################################################################################################

from typing import Optional
from python3 import SinglyLinkedList
from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def mergeNodes(self, head: Optional[ListNode]) -> Optional[ListNode]:
        zero = node = head
        while node.next:
            if node.val == 0:
                zero = node
            zero.val += node.val
            zero.next = node = node.next
        zero.next = None
        return head

    def mergeNodes(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head.next:
            return
        while head.next and head.next.val:
            head.val += head.next.val
            head.next = head.next.next
        head.next = self.mergeNodes(head.next)
        return head


def test():
    arguments = [
        [0, 3, 1, 0, 4, 5, 2, 0],
        [0, 1, 0, 3, 0, 2, 2, 0],
    ]
    expectations = [
        [4, 11],
        [1, 3, 4],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().mergeNodes(SinglyLinkedList(nums).head)
        assert solution == SinglyLinkedList(expected).head
