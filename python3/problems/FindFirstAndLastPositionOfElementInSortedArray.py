### Source : https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-21

##################################################################################################### 
#
# Given an array of integers nums sorted in ascending order, find the starting and ending position of 
# a given target value.
# 
# If target is not found in the array, return [-1, -1].
# 
# Follow up: Could you write an algorithm with O(log n) runtime complexity?
# 
# Example 1:
# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]
# Example 2:
# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]
# Example 3:
# Input: nums = [], target = 0
# Output: [-1,-1]
# 
# Constraints:
# 
# 	0 <= nums.length <= 105
# 	-109 <= nums[i] <= 109
# 	nums is a non-decreasing array.
# 	-109 <= target <= 109
#####################################################################################################

from typing import List


class Solution:

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # brute force
        if target not in nums:
            return [-1, -1]
        idx = nums.index(target)
        start = idx
        while idx + 1 < len(nums) and nums[idx + 1] == target:
            idx += 1
        return [start, idx]

    def searchRange(self, nums: List[int], target: int) -> List[int]:
        # find one, if found then move left and right
        if not nums:
            return [-1, -1]

        left, right = 0, len(nums) - 1
        while left <= right:
            mid = (left + right) // 2
            if nums[mid] == target:
                break
            elif nums[mid] < target:
                left = mid + 1
            else:
                right = mid - 1

        if nums[mid] != target:
            return [-1, -1]

        result = [mid, mid]

        i = 1
        while mid - i >= 0 and nums[mid - i] == target:
            result[0] -= 1
            i += 1
        i = 1
        while mid + i < len(nums) and nums[mid + i] == target:
            result[1] += 1
            i += 1

        return result

    def searchRange(self, nums: List[int], target: int) -> List[int]:  # double binary search: find start and end

        if not nums:
            return [-1, -1]

        def search(nums, target, first=True):
            left, right = 0, len(nums) - 1
            while left < right - 1:
                mid = left + (right - left) // 2
                if nums[mid] == target:
                    if first:  # first of range
                        right = mid
                    else:  # last of range
                        left = mid
                elif nums[mid] < target:
                    left = mid + 1
                else:
                    right = mid - 1

            if first:
                return left if nums[left] == target else right if nums[right] == target else -1
            else:
                return right if nums[right] == target else left if nums[left] == target else -1

        start = search(nums, target, first=True)
        end = search(nums, target, first=False)
        return [start, end]

    def searchRange(self, nums: List[int], target: int) -> List[int]:
        return [nums.index(target), (len(nums) - 1) - list(reversed(nums)).index(target)] if target in nums else [-1, -1]

    def searchRange(self, nums: List[int], target: int) -> List[int]:
        import bisect
        left = bisect.bisect_left(nums, target)
        return [left, bisect.bisect_right(nums, target) - 1] if target in nums[left:left + 1] else [-1, -1]


def test():
    arrays_of_numbers = [
        [5, 7, 7, 8, 8, 10],
        [5, 7, 7, 8, 8, 10],
        [],
        ]
    targets = [8, 6, 0]
    expectations = [
        [3, 4],
        [-1, -1],
        [-1, -1],
        ]
    for nums, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().searchRange(nums, target)
        assert solution == expected
test()