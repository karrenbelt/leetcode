### Source : https://leetcode.com/problems/shifting-letters/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-14

##################################################################################################### 
#
# We have a string S of lowercase letters, and an integer array shifts.
# 
# Call the shift of a letter, the next letter in the alphabet, (wrapping around so that 'z' becomes 
# 'a'). 
# 
# For example, shift('a') = 'b', shift('t') = 'u', and shift('z') = 'a'.
# 
# Now for each shifts[i] = x, we want to shift the first i+1 letters of S, x times.
# 
# Return the final string after all such shifts to S are applied.
# 
# Example 1:
# 
# Input: S = "abc", shifts = [3,5,9]
# Output: "rpl"
# Explanation: 
# We start with "abc".
# After shifting the first 1 letters of S by 3, we have "dbc".
# After shifting the first 2 letters of S by 5, we have "igc".
# After shifting the first 3 letters of S by 9, we have "rpl", the answer.
# 
# Note:
# 
# 	1 <= S.length = shifts.length <= 20000
# 	0 <= shifts[i] <= 10  9
# 
#####################################################################################################
from typing import List


# letter_map = dict(zip('abcdefghijklmnopqrstuvwxyz', range(26)))
# letter_map.update({v: k for k, v in letter_map.items()})


class Solution:
    def shiftingLetters(self, S: str, shifts: List[int]) -> str:  # O(n^2) time -> TLE
        letters = 'abcdefghijklmnopqrstuvwxyz'
        letter_map = dict(zip(letters, range(len(letters))))
        letter_map.update({v: k for k, v in letter_map.items()})
        numbers = [letter_map[s] for s in S]
        for i, shift in enumerate(shifts):
            for j in range(i + 1):
                numbers[j] = (numbers[j] + shift) % 26
        return ''.join(letter_map[n] for n in numbers)

    def shiftingLetters(self, S: str, shifts: List[int]) -> str:  # O(n) time and O(n) space
        # trick is to use 'ord' to convert char to int, and 'chr' to perform int to char
        a = ord('a')
        ans = ""
        total_shift = sum(shifts)
        for idx in range(len(S)):
            ans += chr((ord(S[idx]) + total_shift - a) % 26 + a)
            total_shift -= shifts[idx]
        return ans

    def shiftingLetters(self, S: str, shifts: List[int]) -> str:  # O(n) time and O(n) space
        # first we combine all shifts so we know exactly how much every index is shifted
        for i in range(len(shifts) - 2, -1, -1):
            shifts[i] += shifts[i + 1]
        return "".join(chr((ord(c) - 97 + s) % 26 + 97) for c, s in zip(S, shifts))


def test():
    arguments = [
        ("abc", [3, 5, 9]),
    ]
    expectations = ["rpl"]
    for (S, shifts), expected in zip(arguments, expectations):
        solution = Solution().shiftingLetters(S, shifts)
        assert solution == expected
