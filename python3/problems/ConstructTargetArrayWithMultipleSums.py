### Source : https://leetcode.com/problems/construct-target-array-with-multiple-sums/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-09

#####################################################################################################
#
# Given an array of integers target. From a starting array, A consisting of all 1's, you may perform
# the following procedure :
#
# 	let x be the sum of all elements currently in your array.
# 	choose index i, such that 0 <= i < target.size and set the value of A at index i to x.
# 	You may repeat this procedure as many times as needed.
#
# Return True if it is possible to construct the target array from A otherwise return False.
#
# Example 1:
#
# Input: target = [9,3,5]
# Output: true
# Explanation: Start with [1, 1, 1]
# [1, 1, 1], sum = 3 choose index 1
# [1, 3, 1], sum = 5 choose index 2
# [1, 3, 5], sum = 9 choose index 0
# [9, 3, 5] Done
#
# Example 2:
#
# Input: target = [1,1,1,2]
# Output: false
# Explanation: Impossible to create target array from [1,1,1,1].
#
# Example 3:
#
# Input: target = [8,5]
# Output: true
#
# Constraints:
#
# 	N == target.length
# 	1 <= target.length <= 5 * 104
# 	1 <= target[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    # NOTE: there are a lot of issues on the leetcode discussion forum with this question.
    # - wrong solutions got accepted due to missing test cases
    # - solutions that now yield TLE got accepted

    def isPossible(self, target: List[int]) -> bool:
        # idea is to reverse engineer from target to an array of ones: [1, ..., 1]
        # e.g. [n0, n1, n2]. If n0 >> n1 and n2, we reduce it with sum(n1, n2) many (x) times.
        # This can be simplified by realizing that: n0 - x * (n1 + n2) = n0 % (n1 + n2)
        import heapq
        total = sum(target)
        heapq.heapify(target := [-n for n in target])  # max heap
        while True:
            largest = -heapq.heappop(target)
            total -= largest
            if largest == 1 or total == 1:
                return True
            if largest < total or total == 0 or largest % total == 0:
                return False
            largest %= total  # the modulo operator is crucial for preventing TLE
            total += largest
            heapq.heappush(target, -largest)

    # def isPossible(self, target: List[int]) -> bool:  # TODO: 68 / 71 test cases passed.
    #     import heapq
    #     total = max(target) - sum(target)
    #     heapq.heapify(target := [-n for n in target])  # max heap
    #     while target[0] != -1:
    #         if target[0] >= total:  # largest value cannot be reduced any further
    #             return False
    #         largest = target[0] % min(-1, total)  # the modulo operator is crucial for preventing TLE
    #         heapq.heapreplace(target, largest)
    #         total += largest - target[0]
    #     return True

    def isPossible(self, target: List[int]) -> bool:  # O(n + log k * log n) time and O(n) memory
        # n = number of subtraction to get largest below 2nd largest element.
        import heapq
        if len(target) == 1:
            return target[0] == 1
        total = sum(target)
        heapq.heapify(heap := [-n for n in target])  # max heap
        while heap[0] != -1:
            largest, second_largest, rest_sum = -heap[0], -heap[1], total + heap[0]
            n = max(1, (largest - second_largest) // rest_sum)
            largest -= rest_sum * n
            heapq.heappushpop(heap, -largest)
            total = rest_sum + largest
            if largest < 1:
                return False
        return True

    def isPossible(self, target: List[int]) -> bool:  # O(n log n) time and O(1) memory
        if len(target) == 1:
            return target[0] == 1
        total, i = sum(target), target.index(max(target))
        while total and target[i] > total / 2:
            total -= target[i]
            if total <= 1:
                return bool(total)
            target[i] = target[i] % total
            total, i = total + target[i], target.index(max(target))
        return total == len(target)


def test():
    arguments = [
        [9, 3, 5],  # [9, 3, 5] -> [1, 3, 5] -> [1, 3, 1] -> [1, 1, 1] -> True
        [1, 1, 1, 2],
        [8, 5],  # [8, 5] -> [3, 5] -> [3, 2] -> [1, 2] -> [1, 1] -> True
        [1, 1000000000],
        [1, 1, 999999999],
        [2, 900000002],
        [1],
        [2],
        [1, 1, 10],  # -> [1, 1, 8] -> ... -> [1, 1, 2] -> [1, 1, 0] -> False
        [1, 1, 3]
        ]
    expectations = [True, False, True, True, True, False, True, False, False]
    for target, expected in zip(arguments, expectations):
        solution = Solution().isPossible(target)
        assert solution == expected, (target)
