### Source : https://leetcode.com/problems/zuma-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-16

##################################################################################################### 
#
# Think about Zuma Game. You have a row of balls on the table, colored red(R), yellow(Y), blue(B), 
# green(G), and white(W). You also have several balls in your hand.
# 
# Each time, you may choose a ball in your hand, and insert it into the row (including the leftmost 
# place and rightmost place). Then, if there is a group of 3 or more balls in the same color 
# touching, remove these balls. Keep doing this until no more balls can be removed.
# 
# Find the minimal balls you have to insert to remove all the balls on the table. If you cannot 
# remove all the balls, output -1.
# 
# Example 1:
# 
# Input: board = "WRRBBW", hand = "RB"
# Output: -1
# Explanation: WRRBBW -> WRR[R]BBW -> WBBW -> WBB[B]W -> WW
# 
# Example 2:
# 
# Input: board = "WWRRBBWW", hand = "WRBRW"
# Output: 2
# Explanation: WWRRBBWW -> WWRR[R]BBWW -> WWBBWW -> WWBB[B]WW -> WWWW -> empty
# 
# Example 3:
# 
# Input: board = "G", hand = "GGGGG"
# Output: 2
# Explanation: G -> G[G] -> GG[G] -> empty 
# 
# Example 4:
# 
# Input: board = "RBYYBBRRB", hand = "YRBGB"
# Output: 3
# Explanation: RBYYBBRRB -> RBYY[Y]BBRRB -> RBBBRRB -> RRRB -> B -> B[B] -> BB[B] -> empty 
# 
# Constraints:
# 
# 	You may assume that the initial row of balls on the table won&rsquo;t have any 3 or more 
# consecutive balls with the same color.
# 	1 <= board.length <= 16
# 	1 <= hand.length <= 5
# 	Both input strings will be non-empty and only contain characters 'R','Y','B','G','W'.
#####################################################################################################


class Solution:
    def findMinStep(self, board: str, hand: str) -> int:

        def count(items) -> dict:
            hashmap = {}
            for item in items:
                hashmap[item] = hashmap.get(item, 0) + 1
            return hashmap

        def backtracking(remaining, counter):
            # remove one group at a time, ranging from i to j
            if not remaining:
                return 0

            i, j = 0, 0
            steps = float('inf')
            while j < len(remaining):
                while j < len(remaining) and remaining[i] == remaining[j]:
                    j += 1
                # if more than 2 consecutive balls, we don't need to add one and can just remove: 0 extra steps
                balls_needed = 3 - min(3, j - i)
                if counter.get(remaining[i], 0) >= balls_needed:
                    counter[remaining[i]] -= balls_needed
                    steps = min(steps, balls_needed + backtracking(remaining[:i] + remaining[j:], counter))
                    counter[remaining[i]] += balls_needed
                i = j
            return steps if remaining else 0

        min_steps = backtracking(board, count(hand))
        return min_steps if min_steps < float('inf') else -1


def test():
    arguments = [
        ("WRRBBW", "RB"),
        ("WWRRBBWW", "WRBRW"),
        ("G", "GGGGG"),
        ("RBYYBBRRB", "YRBGB")
        ]
    expectations = [-1, 2, 2, 3]
    for (board, hand), expected in zip(arguments, expectations):
        solution = Solution().findMinStep(board, hand)
        assert solution == expected
