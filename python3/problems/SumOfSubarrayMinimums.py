### Source : https://leetcode.com/problems/sum-of-subarray-minimums/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-21

##################################################################################################### 
#
# Given an array of integers arr, find the sum of min(b), where b ranges over every (contiguous) 
# subarray of arr. Since the answer may be large, return the answer modulo 109 + 7.
# 
# Example 1:
# 
# Input: arr = [3,1,2,4]
# Output: 17
# Explanation: 
# Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4]. 
# inimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
# Sum is 17.
# 
# Example 2:
# 
# Input: arr = [11,81,94,43,3]
# Output: 444
# 
# Constraints:
# 
# 	1 <= arr.length <= 3 * 104
# 	1 <= arr[i] <= 3 * 104
#####################################################################################################

from typing import List


class Solution:
    def sumSubarrayMins(self, arr: List[int]) -> int:  # O(n) time

        left, right, monotonic = [-1] * len(arr), [-1] * len(arr), []

        for i in range(len(arr)):
            while monotonic and arr[i] < arr[monotonic[-1]]:
                monotonic.pop()
            left[i] = monotonic[-1] if monotonic else -1
            monotonic.append(i)

        monotonic.clear()
        for i in range(len(arr)):
            while monotonic and arr[i] < arr[monotonic[-1]]:
                right[monotonic.pop()] = i
            left[i] = monotonic[-1] if monotonic else -1
            monotonic.append(i)

        for i in range(len(arr)):
            left[i] = i + 1 if left[i] == -1 else i - left[i]
        for i in range(len(arr)):
            right[i] = len(arr) - i if right[i] == -1 else right[i] - i

        return sum(a * l * r for a, l, r in zip(arr, left, right)) % (10 ** 9 + 7)

    def sumSubarrayMins(self, arr: List[int]) -> int:  # O(n) time

        left, right, monotonic = [-1] * len(arr), [-1] * len(arr), []

        for i in range(len(arr)):
            while monotonic and arr[i] < arr[monotonic[-1]]:
                right[monotonic.pop()] = i
            left[i] = i - monotonic[-1] if monotonic else i + 1
            monotonic.append(i)

        for i in range(len(arr)):
            right[i] = len(arr) - i if right[i] == -1 else right[i] - i

        return sum(a * l * r for a, l, r in zip(arr, left, right)) % (10 ** 9 + 7)

    # def sumSubarrayMins(self, arr: List[int]) -> int:
    #     nums, stack, result = [float('-inf')] + arr + [float('-inf')], [0], 0
    #     for i in range(1, len(nums)):
    #         while nums[stack[-1]] > nums[i]:
    #             j = stack.pop()
    #             result += nums[j] * (i - j) * (j - stack[-1])  # val * number of subarray combinations
    #         stack.append(i)
    #     return result % (10 ** 9 + 7)


def test():
    arguments = [
        [3, 1, 2, 4],
        [11, 81, 94, 43, 3],
    ]
    expectations = [17, 444]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().sumSubarrayMins(arr)
        assert solution == expected, solution


test()
