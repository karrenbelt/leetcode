### Source : https://leetcode.com/problems/delete-node-in-a-bst/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-12

##################################################################################################### 
#
# Given a root node reference of a BST and a key, delete the node with the given key in the BST. 
# Return the root node reference (possibly updated) of the BST.
# 
# Basically, the deletion can be divided into two stages:
# 
# 	Search for a node to remove.
# 	If the node is found, delete the node.
# 
# Follow up: Can you solve it with time complexity O(height of tree)?
# 
# Example 1:
# 
# Input: root = [5,3,6,2,4,null,7], key = 3
# Output: [5,4,6,2,null,null,7]
# Explanation: Given key to delete is 3. So we find the node with value 3 and delete it.
# One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
# Please notice that another valid answer is [5,2,6,null,4,null,7] and it's also accepted.
# 
# Example 2:
# 
# Input: root = [5,3,6,2,4,null,7], key = 0
# Output: [5,3,6,2,4,null,7]
# Explanation: The tree does not contain a node with value = 0.
# 
# Example 3:
# 
# Input: root = [], key = 0
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 104].
# 	-105 <= Node.val <= 105
# 	Each node has a unique value.
# 	root is a valid binary search tree.
# 	-105 <= key <= 105
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def deleteNode(self, root: TreeNode, key: int) -> TreeNode:  # O(height of BST) time O(1) space
        if not root:
            return root
        elif key > root.val:
            root.right = self.deleteNode(root.right, key)
        elif key < root.val:
            root.left = self.deleteNode(root.left, key)
        else:
            if not root.left:
                return root.right
            else:
                tmp = root.left
                while tmp.right:
                    tmp = tmp.right
                root.val = tmp.val
                root.left = self.deleteNode(root.left, tmp.val)
        return root

    def deleteNode(self, root: TreeNode, key: int) -> TreeNode:
        # in this solution we actually don't move the data from one node to the other, but move the actual node
        def del_node(node: TreeNode):
            if not node:
                return None
            if not node.left:
                return node.right
            if not node.right:
                return node.left

            n = node.right
            while n.left:
                n = n.left
            n.left = node.left
            return node.right

        # find the node
        node, prev = root, None
        while node:
            if node.val == key:
                break
            prev = node
            node = node.left if node.val > key else node.right

        # delete the node
        if node:
            if not prev:
                return del_node(node)
            elif prev.left is node:
                prev.left = del_node(node)
            else:
                prev.right = del_node(node)
        return root


def test():
    from python3 import Codec
    arguments = [
        ("[5,3,6,2,4,null,7]", 3),
        ("[5,3,6,2,4,null,7]", 0),
        ("[]", 0),
        ]
    expectations = [
        "[5,4,6,2,null,null,7]",
        "[5,3,6,2,4,null,7]",
        "[]",
        ]
    for (deserialized_tree, key), expected in zip(arguments, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().deleteNode(root, key)
        assert solution == Codec.deserialize(expected)
