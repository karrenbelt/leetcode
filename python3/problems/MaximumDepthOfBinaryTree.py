### Source : https://leetcode.com/problems/maximum-depth-of-binary-tree/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree, find its maximum depth.
# 
# The maximum depth is the number of nodes along the longest path from the root node down to the 
# farthest leaf node.
# 
# Note: A leaf is a node with no children.
# 
# Example:
# 
# Given binary tree [3,9,20,null,null,15,7],
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
# return its depth = 3.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:

    def maxDepth(self, root: TreeNode, depth=0) -> int:
        if root is None:
            return depth
        return max(self.maxDepth(root.left, depth + 1), self.maxDepth(root.right, depth + 1))

    def maxDepth(self, root: TreeNode) -> int:
        return 1 + max(map(self.maxDepth, (root.left, root.right))) if root else 0


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3,9,20,null,null,15,7]",
        "[1,null,2]",
        "[]",
        "[0]",
        ]
    expectations = [3, 2, 0, 1]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().maxDepth(root)
        assert solution == expected
