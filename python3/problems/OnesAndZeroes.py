### Source : https://leetcode.com/problems/ones-and-zeroes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-02

##################################################################################################### 
#
# You are given an array of binary strings strs and two integers m and n.
# 
# Return the size of the largest subset of strs such that there are at most m 0's and n 1's in the 
# subset.
# 
# A set x is a subset of a set y if all elements of x are also elements of y.
# 
# Example 1:
# 
# Input: strs = ["10","0001","111001","1","0"], m = 5, n = 3
# Output: 4
# Explanation: The largest subset with at most 5 0's and 3 1's is {"10", "0001", "1", "0"}, so the 
# answer is 4.
# Other valid but smaller subsets include {"0001", "1"} and {"10", "1", "0"}.
# {"111001"} is an invalid subset because it contains 4 1's, greater than the maximum of 3.
# 
# Example 2:
# 
# Input: strs = ["10","0","1"], m = 1, n = 1
# Output: 2
# Explanation: The largest subset is {"0", "1"}, so the answer is 2.
# 
# Constraints:
# 
# 	1 <= strs.length <= 600
# 	1 <= strs[i].length <= 100
# 	strs[i] consists only of digits '0' and '1'.
# 	1 <= m, n <= 100
#####################################################################################################

from typing import List


class Solution:
    def findMaxForm(self, strs: List[str], m: int, n: int) -> int:  # unsurprisingly: TLE

        def backtracking(idx, path: List[str]):
            nonlocal longest
            seq = ''.join(path)
            n_zeroes = seq.count('0')
            if n_zeroes > m or len(seq) - n_zeroes > n:
                return
            if len(path) > longest:
                longest = len(path)
            for i in range(idx, len(strs)):
                path.append(strs[i])
                backtracking(i + 1, path)
                path.pop()

        longest = 0
        backtracking(0, [])
        return longest

    def findMaxForm(self, strs: List[str], m: int, n: int) -> int:  # knapsack: O(m*n*l) time and O(m*n*l) space
        # candidates = sorted(map(lambda x: (x.count('0'), x.count('1')), strs))  #, key=lambda x: (x[0], -x[1]))

        def count_zeroes_ones(s: str):
            n_zeroes = s.count('0')
            return n_zeroes, len(s) - n_zeroes

        dp = [[[0] * (n + 1) for _ in range(m + 1)] for _ in range(len(strs) + 1)]
        for i in range(len(strs) + 1):
            nums = [(0, 0)]
            if i > 0:
                nums = count_zeroes_ones(strs[i - 1])
            for j in range(m + 1):
                for k in range(n + 1):
                    if i == 0:
                        dp[i][j][k] = 0
                    elif j >= nums[0] and k >= nums[1]:
                        dp[i][j][k] = max(dp[i - 1][j][k], dp[i - 1][j - nums[0]][k - nums[1]] + 1)
                    else:
                        dp[i][j][k] = dp[i - 1][j][k]
        return dp[len(strs)][m][n]

    def findMaxForm(self, strs: List[str], m: int, n: int) -> int:  # knapsack: O(m*n*l) time and O(m*n) space

        def count_zeroes_ones(s: str):
            n_zeroes = s.count('0')
            return n_zeroes, len(s) - n_zeroes

        dp = [[0] * (n + 1) for _ in range(m + 1)]
        for i in range(1, len(strs) + 1):
            n_zeroes, n_ones = count_zeroes_ones(strs[i - 1])
            for j in range(m, -1, -1):
                for k in range(n, -1, -1):
                    if j >= n_zeroes and k >= n_ones:
                        dp[j][k] = max(dp[j][k], dp[j - n_zeroes][k - n_ones] + 1)
        return dp[m][n]



def test():
    arguments = [
        (["10", "0001", "111001", "1", "0"], 5, 3),
        (["10", "0", "1"], 1, 1),
        (["0", "11", "1000", "01", "0", "101", "1", "1", "1", "0", "0", "0", "0", "1", "0", "0110101", "0", "11", "01",
         "00", "01111", "0011", "1", "1000", "0", "11101", "1", "0", "10", "0111"], 9, 80),
        ]
    expectations = [4, 2, 17]
    for (strs, m, n), expected in zip(arguments, expectations):
        solution = Solution().findMaxForm(strs, m, n)
        assert solution == expected
test()