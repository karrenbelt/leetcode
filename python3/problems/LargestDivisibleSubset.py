### Source : https://leetcode.com/problems/largest-divisible-subset/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-19

##################################################################################################### 
#
# Given a set of distinct positive integers nums, return the largest subset answer such that every 
# pair (answer[i], answer[j]) of elements in this subset satisfies:
# 
# 	answer[i] % answer[j] == 0, or
# 	answer[j] % answer[i] == 0
# 
# If there are multiple solutions, return any of them.
# 
# Example 1:
# 
# Input: nums = [1,2,3]
# Output: [1,2]
# Explanation: [1,3] is also accepted.
# 
# Example 2:
# 
# Input: nums = [1,2,4,8]
# Output: [1,2,4,8]
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	1 <= nums[i] <= 2 * 109
# 	All the integers in nums are unique.
#####################################################################################################

from typing import List


class Solution:
    def largestDivisibleSubset(self, nums: List[int]) -> List[int]:  # O(n^2) time
        dp = [[]]
        for n in sorted(nums):
            dp.append(max((s + [n] for s in dp if not s or n % s[-1] == 0), key=len))
        return max(dp, key=len)


def test():
    arguments = [
        [1, 2, 3],
        [1, 2, 4, 8],
    ]
    expectations = [
        [1, 2],  # [1, 3] is also a valid solution
        [1, 2, 4, 8],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().largestDivisibleSubset(nums)
        assert solution == expected
test()