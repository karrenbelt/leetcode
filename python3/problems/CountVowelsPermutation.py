### Source : https://leetcode.com/problems/count-vowels-permutation/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-04

##################################################################################################### 
#
# Given an integer n, your task is to count how many strings of length n can be formed under the 
# following rules:
# 
# 	Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
# 	Each vowel 'a' may only be followed by an 'e'.
# 	Each vowel 'e' may only be followed by an 'a' or an 'i'.
# 	Each vowel 'i' may not be followed by another 'i'.
# 	Each vowel 'o' may only be followed by an 'i' or a 'u'.
# 	Each vowel 'u' may only be followed by an 'a'.
# 
# Since the answer may be too large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: n = 1
# Output: 5
# Explanation: All possible strings are: "a", "e", "i" , "o" and "u".
# 
# Example 2:
# 
# Input: n = 2
# Output: 10
# Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io", "iu", "oi", "ou" and 
# "ua".
# 
# Example 3: 
# 
# Input: n = 5
# Output: 68
# 
# Constraints:
# 
# 	1 <= n <= 2 * 104
#####################################################################################################


class Solution:
    def countVowelPermutation(self, n: int) -> int:  # brute force: O(n) time and O(2^n) space
        from functools import lru_cache

        @lru_cache(maxsize=None)  # maximum recursion depth exceeded from n = 248 onwards
        def dfs(previous_vowel: str, size: int) -> int:
            return 1 if size == n else sum(dfs(vowel, size + 1) for vowel in next_vowel[previous_vowel])

        next_vowel = {'a': 'e', 'e': 'ai', 'i': 'aeou', 'o': 'iu', 'u': 'a'}
        return sum(dfs(v, 1) for v in 'aeiou') % (10**9 + 7)

    def countVowelPermutation(self, n: int) -> int:  # O(n) time and O(n) space
        a, e, i, o, u = range(5)
        dp = [[1] * 5] + [[0] * 5 for _ in range(n - 1)]
        for j in range(1, n):
            dp[j][a] = dp[j - 1][e]
            dp[j][e] = dp[j - 1][a] + dp[j - 1][i]
            dp[j][i] = dp[j - 1][a] + dp[j - 1][e] + dp[j - 1][o] + dp[j - 1][u]
            dp[j][o] = dp[j - 1][i] + dp[j - 1][u]
            dp[j][u] = dp[j - 1][a]
        return sum(dp[-1]) % (10**9 + 7)

    def countVowelPermutation(self, n: int) -> int:  # O(n) time and O(1) space
        a, e, i, o, u = range(5)
        dp = [1] * 5
        for _ in range(1, n):
            prev = dp[:]
            dp[a] = prev[e]
            dp[e] = prev[a] + prev[i]
            dp[i] = prev[a] + prev[e] + prev[o] + prev[u]
            dp[o] = prev[i] + prev[u]
            dp[u] = prev[a]
        return sum(dp) % (10**9 + 7)

    def countVowelPermutation(self, n: int) -> int:  # O(n) time and O(1) space
        a, e, i, o, u = range(5)
        dp = [1] * 5
        for _ in range(1, n):
            dp[:] = dp[e], dp[a] + dp[i], dp[a] + dp[e] + dp[o] + dp[u], dp[i] + dp[u], dp[a]
        return sum(dp) % (10**9 + 7)

    def countVowelPermutation(self, n: int) -> int:  # O(n) time and O(1) space
        # think state machine, we just need 5 states
        a = e = i = o = u = 1
        for _ in range(n - 1):
            a, e, i, o, u = e, a + i, a + e + o + u, i + u, a
        return (a + e + i + o + u) % (10**9 + 7)


def test():
    arguments = [1, 2, 3, 4, 5, 200]
    expectations = [5, 10, 19, 35, 68, 670333618]
    for n, expected in zip(arguments, expectations):
        solution = Solution().countVowelPermutation(n)
        assert solution == expected, (solution, expected)
