### Source : https://leetcode.com/problems/maximum-product-subarray/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an integer array nums, find a contiguous non-empty subarray within the array that has the 
# largest product, and return the product.
# 
# It is guaranteed that the answer will fit in a 32-bit integer.
# 
# A subarray is a contiguous subsequence of the array.
# 
# Example 1:
# 
# Input: nums = [2,3,-2,4]
# Output: 6
# Explanation: [2,3] has the largest product 6.
# 
# Example 2:
# 
# Input: nums = [-2,0,-1]
# Output: 0
# Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	-10 <= nums[i] <= 10
# 	The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
#####################################################################################################

from typing import List


class Solution:
    def maxProduct(self, nums: List[int]) -> int:  # Kadane's algorithm: O(n) time and O(1) space
        prev_minimum = prev_maximum = maximum = nums[0]
        for i in range(1, len(nums)):
            mini = min(nums[i], prev_maximum * nums[i], prev_minimum * nums[i])
            maxi = max(nums[i], prev_maximum * nums[i], prev_minimum * nums[i])
            maximum, prev_minimum, prev_maximum = max(maximum, maxi), mini, maxi
        return maximum

    def maxProduct(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        # prefix and suffix product
        B = nums[::-1]
        for i in range(1, len(nums)):
            nums[i] *= nums[i - 1] or 1
            B[i] *= B[i - 1] or 1
        return max(nums + B)

    def maxProduct(self, nums: List[int]) -> int:  # Kadane's algorithm: O(n) time and O(1) space
        prefix, suffix, maximum = 0, 0, nums[0]
        for i in range(len(nums)):
            prefix = (prefix or 1) * nums[i]
            suffix = (suffix or 1) * nums[~i]
            maximum = max(maximum, prefix, suffix)
        return maximum


def test():
    arguments = [
        [2, 3, -2, 4],
        [-2, 0, -1],
    ]
    expectations = [6, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maxProduct(nums)
        assert solution == expected
