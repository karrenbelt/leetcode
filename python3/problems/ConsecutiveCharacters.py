### Source : https://leetcode.com/problems/consecutive-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-02

##################################################################################################### 
#
# Given a string s, the power of the string is the maximum length of a non-empty substring that 
# contains only one unique character.
# 
# Return the power of the string.
# 
# Example 1:
# 
# Input: s = "leetcode"
# Output: 2
# Explanation: The substring "ee" is of length 2 with the character 'e' only.
# 
# Example 2:
# 
# Input: s = "abbcccddddeeeeedcba"
# Output: 5
# Explanation: The substring "eeeee" is of length 5 with the character 'e' only.
# 
# Example 3:
# 
# Input: s = "triplepillooooow"
# Output: 5
# 
# Example 4:
# 
# Input: s = "hooraaaaaaaaaaay"
# Output: 11
# 
# Example 5:
# 
# Input: s = "tourist"
# Output: 1
# 
# Constraints:
# 
# 	1 <= s.length <= 500
# 	s contains only lowercase English letters.
#####################################################################################################


class Solution:
    def maxPower(self, s: str) -> int:  # O(n) time
        maximum = ctr = 1
        for i in range(len(s) - 1):
            ctr = ctr + 1 if s[i] == s[i + 1] else 1
            maximum = max(maximum, ctr)
        return maximum

    def maxPower(self, s: str) -> int:  # O(n) time
        from itertools import groupby
        return max(len(tuple(group)) for _, group in groupby(s))


def test():
    arguments = ["leetcode", "abbcccddddeeeeedcba", "triplepillooooow", "hooraaaaaaaaaaay", "tourist"]
    expectations = [2, 5, 5, 11, 1]
    for s, expected in zip(arguments, expectations):
        solution = Solution().maxPower(s)
        assert solution == expected, (solution, expected)
