### Source : https://leetcode.com/problems/frog-jump/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-05

##################################################################################################### 
#
# A frog is crossing a river. The river is divided into some number of units, and at each unit, there 
# may or may not exist a stone. The frog can jump on a stone, but it must not jump into the water.
# 
# Given a list of stones' positions (in units) in sorted ascending order, determine if the frog can 
# cross the river by landing on the last stone. Initially, the frog is on the first stone and assumes 
# the first jump must be 1 unit.
# 
# If the frog's last jump was k units, its next jump must be either k - 1, k, or k + 1 units. The 
# frog can only jump in the forward direction.
# 
# Example 1:
# 
# Input: stones = [0,1,3,5,6,8,12,17]
# Output: true
# Explanation: The frog can jump to the last stone by jumping 1 unit to the 2nd stone, then 2 units 
# to the 3rd stone, then 2 units to the 4th stone, then 3 units to the 6th stone, 4 units to the 7th 
# stone, and 5 units to the 8th stone.
# 
# Example 2:
# 
# Input: stones = [0,1,2,3,4,8,9,11]
# Output: false
# Explanation: There is no way to jump to the last stone as the gap between the 5th and 6th stone is 
# too large.
# 
# Constraints:
# 
# 	2 <= stones.length <= 2000
# 	0 <= stones[i] <= 231 - 1
# 	stones[0] == 0
#####################################################################################################

from typing import List


class Solution:
    def canCross(self, stones: List[int]) -> bool:
        # this will surpass the recursion limit when len(stones) is far over 200
        from functools import lru_cache
        import bisect

        @lru_cache(maxsize=None)
        def dfs(i: int, last_jump: int):
            if i == len(stones) - 1:
                return True
            for jump in (last_jump - 1, last_jump, last_jump + 1):
                for j in range(1, 4):  # since jump is in (-1, 0, +1) we need to check at most 3 positions
                    # however it doesn't need to be +1, 2 or 3, can be further as jump can be large. Need bisect
                    if i + j < len(stones) and stones[i] + jump == stones[i + j]:
                        if dfs(i + j, jump):
                            return True
            return False

        return dfs(0, 0)


def test():
    arguments = [
        [0, 1, 3, 5, 6, 8, 12, 17],
        [0, 1, 2, 3, 4, 8, 9, 11],
    ]
    expectations = [True, False]
    for stones, expected in zip(arguments, expectations):
        solution = Solution().canCross(stones)
        assert solution == expected
test()