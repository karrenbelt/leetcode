### Source : https://leetcode.com/problems/number-of-students-unable-to-eat-lunch/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-01

##################################################################################################### 
#
# The school cafeteria offers circular and square sandwiches at lunch break, referred to by numbers 0 
# and 1 respectively. All students stand in a queue. Each student either prefers square or circular 
# sandwiches.
# 
# The number of sandwiches in the cafeteria is equal to the number of students. The sandwiches are 
# placed in a stack. At each step:
# 
# 	If the student at the front of the queue prefers the sandwich on the top of the stack, they 
# will take it and leave the queue.
# 	Otherwise, they will leave it and go to the queue's end.
# 
# This continues until none of the queue students want to take the top sandwich and are thus unable 
# to eat.
# 
# You are given two integer arrays students and sandwiches where sandwiches[i] is the type of the 
# i-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-Kth sandwich in the stack (i = 0 is the top of the stack) and students[j] is the 
# preference of the j-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-Kth student in the initial queue (j = 0 is the front of the 
# queue). Return the number of students that are unable to eat.
# 
# Example 1:
# 
# Input: students = [1,1,0,0], sandwiches = [0,1,0,1]
# Output: 0 
# Explanation:
# - Front student leaves the top sandwich and returns to the end of the line making students = 
# [1,0,0,1].
# - Front student leaves the top sandwich and returns to the end of the line making students = 
# [0,0,1,1].
# - Front student takes the top sandwich and leaves the line making students = [0,1,1] and sandwiches 
# = [1,0,1].
# - Front student leaves the top sandwich and returns to the end of the line making students = 
# [1,1,0].
# - Front student takes the top sandwich and leaves the line making students = [1,0] and sandwiches = 
# [0,1].
# - Front student leaves the top sandwich and returns to the end of the line making students = [0,1].
# - Front student takes the top sandwich and leaves the line making students = [1] and sandwiches = 
# [1].
# - Front student takes the top sandwich and leaves the line making students = [] and sandwiches = [].
# Hence all students are able to eat.
# 
# Example 2:
# 
# Input: students = [1,1,1,0,0,1], sandwiches = [1,0,0,0,1,1]
# Output: 3
# 
# Constraints:
# 
# 	1 <= students.length, sandwiches.length <= 100
# 	students.length == sandwiches.length
# 	sandwiches[i] is 0 or 1.
# 	students[i] is 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def countStudents(self, students: List[int], sandwiches: List[int]) -> int:
        # we rotate until no more sandwiches of the desired type are available to the students that are left
        # hence we can simply count and subtract, stopping whenever a sandwich is on top that no one will tke
        counts = {}
        for student in students:
            counts[student] = counts.get(student, 0) + 1
        for sandwich in sandwiches:
            if not counts.get(sandwich, 0):
                break
            counts[sandwich] -= 1
        return sum(counts.values())


def test():
    arguments = [
        ([1, 1, 0, 0], [0, 1, 0, 1]),
        ([1, 1, 1, 0, 0, 1], [1, 0, 0, 0, 1, 1])
    ]
    expectations = [0, 3]
    for (students, sandwiches), expected in zip(arguments, expectations):
        solution = Solution().countStudents(students, sandwiches)
        assert solution == expected
