### Source : https://leetcode.com/problems/day-of-the-week/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-07

##################################################################################################### 
#
# Given a date, return the corresponding day of the week for that date.
# 
# The input is given as three integers representing the day, month and year respectively.
# 
# Return the answer as one of the following values {"Sunday", "onday", "Tuesday", "Wednesday", 
# "Thursday", "Friday", "Saturday"}.
# 
# Example 1:
# 
# Input: day = 31, month = 8, year = 2019
# Output: "Saturday"
# 
# Example 2:
# 
# Input: day = 18, month = 7, year = 1999
# Output: "Sunday"
# 
# Example 3:
# 
# Input: day = 15, month = 8, year = 1993
# Output: "Sunday"
# 
# Constraints:
# 
# 	The given dates are valid dates between the years 1971 and 2100.
#####################################################################################################


class Solution:
    def dayOfTheWeek(self, day: int, month: int, year: int) -> str:
        from datetime import datetime
        return datetime.fromisoformat(f"{year}-{month:02}-{day:02}").strftime('%A')

    def dayOfTheWeek(self, day: int, month: int, year: int) -> str:  # O(1) time and O(1) space
        # https://en.wikipedia.org/wiki/Zeller's_congruence
        if month < 3:
            month += 12
            year -= 1
        c, y = year // 100, year % 100
        w = (c // 4 - 2 * c + y + y // 4 + 13 * (month + 1) // 5 + day - 1) % 7
        return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][w]


def test():
    arguments = [
        (31, 8, 2019),
        (18, 7, 1999),
        (15, 8, 1993),
    ]
    expectations = ["Saturday", "Sunday", "Sunday"]
    for (day, month, year), expected in zip(arguments, expectations):
        solution = Solution().dayOfTheWeek(day, month, year)
        assert solution == expected
