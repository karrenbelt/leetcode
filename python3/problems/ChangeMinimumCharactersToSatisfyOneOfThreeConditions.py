### Source : https://leetcode.com/problems/change-minimum-characters-to-satisfy-one-of-three-conditions/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-05

##################################################################################################### 
#
# You are given two strings a and b that consist of lowercase letters. In one operation, you can 
# change any character in a or b to any lowercase letter.
# 
# Your goal is to satisfy one of the following three conditions:
# 
# 	Every letter in a is strictly less than every letter in b in the alphabet.
# 	Every letter in b is strictly less than every letter in a in the alphabet.
# 	Both a and b consist of only one distinct letter.
# 
# Return the minimum number of operations needed to achieve your goal.
# 
# Example 1:
# 
# Input: a = "aba", b = "caa"
# Output: 2
# Explanation: Consider the best way to make each condition true:
# 1) Change b to "ccc" in 2 operations, then every letter in a is less than every letter in b.
# 2) Change a to "bbb" and b to "aaa" in 3 operations, then every letter in b is less than every 
# letter in a.
# 3) Change a to "aaa" and b to "aaa" in 2 operations, then a and b consist of one distinct letter.
# The best way was done in 2 operations (either condition 1 or condition 3).
# 
# Example 2:
# 
# Input: a = "dabadd", b = "cda"
# Output: 3
# Explanation: The best way is to make condition 1 true by changing b to "eee".
# 
# Constraints:
# 
# 	1 <= a.length, b.length <= 105
# 	a and b consist only of lowercase letters.
#####################################################################################################


class Solution:
    def minCharacters(self, a: str, b: str) -> int:  # O(m + n) time and O(1) space
        from collections import Counter
        counts_a, counts_b = map(lambda s: Counter(ord(c) - 97 for c in s), (a, b))
        cond1 = cond2 = 1 << 31 - 1
        cond3 = len(a) + len(b) - max((counts_a + counts_b).values())
        for i in range(25):
            counts_a[i + 1] += counts_a[i]
            counts_b[i + 1] += counts_b[i]
            cond1 = min(cond1, counts_a[i] + len(b) - counts_b[i])
            cond2 = min(cond2, counts_b[i] + len(a) - counts_a[i])
        return min(cond1, cond2, cond3)


def test():
    arguments = [
        ("aba", "caa"),
        ("dabadd", "cda"),
        ("dee", "a"),
    ]
    expectations = [2, 3, 0]
    for (a, b), expected in zip(arguments, expectations):
        solution = Solution().minCharacters(a, b)
        assert solution == expected
