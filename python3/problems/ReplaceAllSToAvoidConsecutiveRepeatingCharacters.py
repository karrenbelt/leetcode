### Source : https://leetcode.com/problems/replace-all-s-to-avoid-consecutive-repeating-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-13

##################################################################################################### 
#
# Given a string s containing only lowercase English letters and the '?' character, convert all the 
# '?' characters into lowercase letters such that the final string does not contain any consecutive 
# repeating characters. You cannot modify the non '?' characters.
# 
# It is guaranteed that there are no consecutive repeating characters in the given string except for 
# '?'.
# 
# Return the final string after all the conversions (possibly zero) have been made. If there is more 
# than one solution, return any of them. It can be shown that an answer is always possible with the 
# given constraints.
# 
# Example 1:
# 
# Input: s = "?zs"
# Output: "azs"
# Explanation: There are 25 solutions for this problem. From "azs" to "yzs", all are valid. Only "z" 
# is an invalid modification as the string will consist of consecutive repeating characters in "zzs".
# 
# Example 2:
# 
# Input: s = "ubv?w"
# Output: "ubvaw"
# Explanation: There are 24 solutions for this problem. Only "v" and "w" are invalid modifications as 
# the strings will consist of consecutive repeating characters in "ubvvw" and "ubvww".
# 
# Constraints:
# 
# 	1 <= s.length <= 100
# 	s consist of lowercase English letters and '?'.
#####################################################################################################


class Solution:
    def modifyString(self, s: str) -> str:
        while (i := s.find("?") + 1):
            neighbors = {s[i - 2:i - 1], s[i: i + 1]}
            s = s.replace("?", (set("abc") - neighbors).pop(), 1)
        return s

    def modifyString(self, s: str) -> str:
        for i, c in filter(lambda x: x[1] == "?", enumerate(s)):
            neighbors = {s[i - 1:i], s[i + 1: i + 2]}
            s = s[:i] + (set("abc") - neighbors).pop() + s[i + 1:]
        return s


def test():
    import re
    arguments = [
        "?zs",
        "ubv?w",
    ]
    expectations = [
        r"[^z]zs",
        r"ubv[^vw]w",
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().modifyString(s)
        assert re.match(expected, solution)
