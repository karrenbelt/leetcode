### Source : https://leetcode.com/problems/regular-expression-matching/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-19

##################################################################################################### 
#
# Given an input string (s) and a pattern (p), implement regular expression matching with support for 
# '.' and '*' where: 
# 
# 	'.' atches any single character.-b-@-K-b-@-K-b-@-K-b-@-K
# 	'*' atches zero or more of the preceding element.
# 
# The matching should cover the entire input string (not partial).
# 
# Example 1:
# 
# Input: s = "aa", p = "a"
# Output: false
# Explanation: "a" does not match the entire string "aa".
# 
# Example 2:
# 
# Input: s = "aa", p = "a*"
# Output: true
# Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' 
# once, it becomes "aa".
# 
# Example 3:
# 
# Input: s = "ab", p = ".*"
# Output: true
# Explanation: ".*" means "zero or more (*) of any character (.)".
# 
# Example 4:
# 
# Input: s = "aab", p = "c*a*b"
# Output: true
# Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches "aab".
# 
# Example 5:
# 
# Input: s = "mississippi", p = "mis*is*p*."
# Output: false
# 
# Constraints:
# 
# 	0 <= s.length <= 20
# 	0 <= p.length <= 30
# 	s contains only lowercase English letters.
# 	p contains only lowercase English letters, '.', and '*'.
# 	It is guaranteed for each appearance of the character '*', there will be a previous valid 
# character to match.
#####################################################################################################


class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        import re
        return bool(re.match(p+"$", s))

    def isMatch(self, s: str, p: str) -> bool:  # bottom-up dp: O(m * n) time and O(m * n) space
        s, p = ' ' + s, ' ' + p
        dp = [[0] * len(p) for _ in range(len(s))]
        dp[0][0] = 1

        for j in range(1, len(p)):
            if p[j] == '*':
                dp[0][j] = dp[0][j-2]

        for i in range(1, len(s)):
            for j in range(1, len(p)):
                if p[j] in {s[i], '.'}:
                    dp[i][j] = dp[i - 1][j - 1]
                elif p[j] == "*":
                    dp[i][j] = dp[i][j - 2] or int(dp[i - 1][j] and p[j - 1] in {s[i], '.'})

        return bool(dp[-1][-1])

    def isMatch(self, s: str, p: str) -> bool:  # top-down dp: O(m * n) time and O(m * n) space

        def dp(si, pi):
            if pi >= len(p):  # if end of pattern, check if we are at the end of the string
                return si == len(s)
            if si >= len(s):  # if end of string but more pattern, check if it's optional (followed by *)
                return pi + 1 < len(p) and p[pi + 1] == '*' and dp(si, pi + 2)
            if (si, pi) not in memo:
                matched = p[pi] == s[si] or p[pi] == '.'
                if pi + 1 < len(p) and p[pi + 1] == '*':  # if wildcard follows there's two options
                    memo[si, pi] = dp(si, pi + 2) or (matched and dp(si + 1, pi))
                else:  # move to next character
                    memo[si, pi] = matched and dp(si + 1, pi + 1)
            return memo[si, pi]

        memo = {}
        return dp(0, 0)


def test():
    arguments = [
        ("aa", "a"),
        ("aa", "a*"),
        ("ab", ".*"),
        ("aab", "c*a*b"),
        ("mississippi", "mis*is*p*."),
        ]
    expectations = [False, True, True, True, False]
    for (s, p), expected in zip(arguments, expectations):
        solution = Solution().isMatch(s, p)
        assert solution == expected
