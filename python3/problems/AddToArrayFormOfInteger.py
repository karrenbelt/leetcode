### Source : https://leetcode.com/problems/add-to-array-form-of-integer/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# The array-form of an integer num is an array representing its digits in left to right order.
# 
# 	For example, for num = 1321, the array form is [1,3,2,1].
# 
# Given num, the array-form of an integer, and an integer k, return the array-form of the integer num 
# + k.
# 
# Example 1:
# 
# Input: num = [1,2,0,0], k = 34
# Output: [1,2,3,4]
# Explanation: 1200 + 34 = 1234
# 
# Example 2:
# 
# Input: num = [2,7,4], k = 181
# Output: [4,5,5]
# Explanation: 274 + 181 = 455
# 
# Example 3:
# 
# Input: num = [2,1,5], k = 806
# Output: [1,0,2,1]
# Explanation: 215 + 806 = 1021
# 
# Example 4:
# 
# Input: num = [9,9,9,9,9,9,9,9,9,9], k = 1
# Output: [1,0,0,0,0,0,0,0,0,0,0]
# Explanation: 9999999999 + 1 = 10000000000
# 
# Constraints:
# 
# 	1 <= num.length <= 104
# 	0 <= num[i] <= 9
# 	num does not contain any leading zeros except for the zero itself.
# 	1 <= k <= 104
#####################################################################################################

from typing import List


class Solution:
    def addToArrayForm(self, num: List[int], k: int) -> List[int]:
        return list(map(int, list(str(int(''.join(map(str, num))) + k))))

    def addToArrayForm(self, num: List[int], k: int) -> List[int]:
        return [int(s) for s in str(int(''.join(map(str, num))) + k)]

    def addToArrayForm(self, num: List[int], k: int) -> List[int]:  # O(n) time
        for i in reversed(range(len(num))):
            k, num[i] = divmod(num[i] + k, 10)
        return list(map(int, str(k))) + num if k else num


def test():
    arguments = [
        ([1, 2, 0, 0], 34),
        ([2, 7, 4], 181),
        ([2, 1, 5], 806),
        ([9, 9, 9, 9, 9, 9, 9, 9, 9, 9], 1),
    ]
    expectations = [
        [1, 2, 3, 4],
        [4, 5, 5],
        [1, 0, 2, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
    for (num, k), expected in zip(arguments, expectations):
        solution = Solution().addToArrayForm(num, k)
        assert solution == expected
