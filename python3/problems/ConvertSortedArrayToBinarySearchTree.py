### Source : https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-12

##################################################################################################### 
#
# Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
# 
# For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of 
# the two subtrees of every node never differ by more than 1.
# 
# Example:
# 
# Given the sorted array: [-10,-3,0,5,9],
# 
# One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:
# 
#       0
#      / \
#    -3   9
#    /   /
#  -10  5
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:

        def build(left: int, right: int):
            if left > right:
                return
            mid = left + (right - left) // 2
            node = TreeNode(nums[mid])
            node.left = build(left, mid - 1)
            node.right = build(mid + 1, right)
            return node

        return build(0, len(nums) - 1)


def test():
    from python3 import Codec
    arrays_of_numbers = [
        [-10, -3, 0, 5, 9],
        [1, 3],
        ]
    expectations = [
        ("[0,-3,9,-10,null,5]", "[0,-10,5,null,-3,null,9]"),
        ("[1,null,3]", "[3,1]"),
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().sortedArrayToBST(nums)
        assert solution in set(map(Codec.deserialize, expected))
