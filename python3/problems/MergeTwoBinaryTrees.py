### Source : https://leetcode.com/problems/merge-two-binary-trees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-14

##################################################################################################### 
#
# You are given two binary trees root1 and root2.
# 
# Imagine that when you put one of them to cover the other, some nodes of the two trees are 
# overlapped while the others are not. You need to merge the two trees into a new binary tree. The 
# merge rule is that if two nodes overlap, then sum node values up as the new value of the merged 
# node. Otherwise, the NOT null node will be used as the node of the new tree.
# 
# Return the merged tree.
# 
# Note: The merging process must start from the root nodes of both trees.
# 
# Example 1:
# 
# Input: root1 = [1,3,2,5], root2 = [2,1,3,null,4,null,7]
# Output: [3,4,5,5,4,null,7]
# 
# Example 2:
# 
# Input: root1 = [1], root2 = [1,2]
# Output: [2,2]
# 
# Constraints:
# 
# 	The number of nodes in both trees is in the range [0, 2000].
# 	-104 <= Node.val <= 104
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def mergeTrees(self, root1: TreeNode, root2: TreeNode) -> TreeNode:

        def dfs(node1: TreeNode, node2: TreeNode):
            if not node1 or not node2:
                return node1 or node2
            node1.val += node2.val
            node1.left = self.mergeTrees(node1.left, node2.left)
            node1.right = self.mergeTrees(node1.right, node2.right)
            return node1

        return dfs(root1, root2)

    def mergeTrees(self, root1: TreeNode, root2: TreeNode) -> TreeNode:

        def dfs(node1: TreeNode, node2: TreeNode):
            if node1 or node2:
                root = TreeNode(getattr(node1, 'val', 0) + getattr(node2, 'val', 0))
                root.left = self.mergeTrees(node1 and node1.left, node2 and node2.left)
                root.right = self.mergeTrees(node1 and node1.right, node2 and node2.right)
                return root

        return dfs(root1, root2)

    def mergeTrees(self, root1: TreeNode, root2: TreeNode) -> TreeNode:
        if root1:
            queue = [(root1, root2)]
            while queue:
                node1, node2 = queue.pop()
                if not node2:
                    continue
                node1.val += node2.val
                if not node1.left: node1.left = node2.left
                else: queue.append((node1.left, node2.left))
                if not node1.right: node1.right = node2.right
                else: queue.append((node1.right, node2.right))
        return root1 or root2


def test():
    from python3 import Codec
    arguments = [
        ("[1,3,2,5]", "[2,1,3,null,4,null,7]"),
        ("[1]", "[1,2]"),
        ]
    expectations = [
        "[3,4,5,5,4,null,7]",
        "[2, 2]",
        ]
    for serialized_trees, expected in zip(arguments, expectations):
        root1, root2 = map(Codec.deserialize, serialized_trees)
        solution = Solution().mergeTrees(root1, root2)
        assert solution == Codec.deserialize(expected)
