### Source : https://leetcode.com/problems/finding-the-users-active-minutes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-13

##################################################################################################### 
#
# You are given the logs for users' actions on LeetCode, and an integer k. The logs are represented 
# by a 2D integer array logs where each logs[i] = [IDi, timei] indicates that the user with IDi 
# performed an action at the minute timei.
# 
# ultiple users can perform actions simultaneously, and a single user can perform multiple actions 
# in the same minute.
# 
# The user active minutes (UA) for a given user is defined as the number of unique minutes in which 
# the user performed an action on LeetCode. A minute can only be counted once, even if multiple 
# actions occur during it.
# 
# You are to calculate a 1-indexed array answer of size k such that, for each j (1 <= j <= k), 
# answer[j] is the number of users whose UA equals j.
# 
# Return the array answer as described above.
# 
# Example 1:
# 
# Input: logs = [[0,5],[1,2],[0,2],[0,5],[1,3]], k = 5
# Output: [0,2,0,0,0]
# Explanation:
# The user with ID=0 performed actions at minutes 5, 2, and 5 again. Hence, they have a UA of 2 
# (minute 5 is only counted once).
# The user with ID=1 performed actions at minutes 2 and 3. Hence, they have a UA of 2.
# Since both users have a UA of 2, answer[2] is 2, and the remaining answer[j] values are 0.
# 
# Example 2:
# 
# Input: logs = [[1,1],[2,2],[2,3]], k = 4
# Output: [1,1,0,0]
# Explanation:
# The user with ID=1 performed a single action at minute 1. Hence, they have a UA of 1.
# The user with ID=2 performed actions at minutes 2 and 3. Hence, they have a UA of 2.
# There is one user with a UA of 1 and one with a UA of 2.
# Hence, answer[1] = 1, answer[2] = 1, and the remaining values are 0.
# 
# Constraints:
# 
# 	1 <= logs.length <= 104
# 	0 <= IDi <= 109
# 	1 <= timei <= 105
# 	k is in the range [The maximum UA for a user, 105].
#####################################################################################################

from typing import List


class Solution:
    def findingUsersActiveMinutes(self, logs: List[List[int]], k: int) -> List[int]:  # O(n) time and O(k) space
        active_user_minutes = [0] * k
        user_times = {}
        for user_id, time in logs:
            user_times.setdefault(user_id, set()).add(time)
        for n in map(len, user_times.values()):
            active_user_minutes[n - 1] += 1
        return active_user_minutes


def test():
    arguments = [
        ([[0, 5], [1, 2], [0, 2], [0, 5], [1, 3]], 5),
        ([[1, 1], [2, 2], [2, 3]], 4),
    ]
    expectations = [
        [0, 2, 0, 0, 0],
        [1, 1, 0, 0],
    ]
    for (logs, k), expected in zip(arguments, expectations):
        solution = Solution().findingUsersActiveMinutes(logs, k)
        assert solution == expected
