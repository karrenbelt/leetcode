### Source : https://leetcode.com/problems/license-key-formatting/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-10

##################################################################################################### 
#
# You are given a license key represented as a string s that consists of only alphanumeric characters 
# and dashes. The string is separated into n + 1 groups by n dashes. You are also given an integer k.
# 
# We want to reformat the string s such that each group contains exactly k characters, except for the 
# first group, which could be shorter than k but still must contain at least one character. 
# Furthermore, there must be a dash inserted between two groups, and you should convert all lowercase 
# letters to uppercase.
# 
# Return the reformatted license key.
# 
# Example 1:
# 
# Input: s = "5F3Z-2e-9-w", k = 4
# Output: "5F3Z-2E9W"
# Explanation: The string s has been split into two parts, each part has 4 characters.
# Note that the two extra dashes are not needed and can be removed.
# 
# Example 2:
# 
# Input: s = "2-5g-3-J", k = 2
# Output: "2-5G-3J"
# Explanation: The string s has been split into three parts, each part has 2 characters except the 
# first part as it could be shorter as mentioned above.
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s consists of English letters, digits, and dashes '-'.
# 	1 <= k <= 104
#####################################################################################################


class Solution:
    def licenseKeyFormatting(self, s: str, k: int) -> str:  # O(n) time and O(n) space
        stack = []
        s = ''.join(s.upper().split('-'))
        for i in range(len(s)):
            if i > 0 and i % k == 0:
                stack.append('-')
            stack.append(s[-i - 1])
        return ''.join(reversed(stack))

    def licenseKeyFormatting(self, s: str, k: int) -> str:  # O(n) time and O(n) space
        return ''.join([c+'-' if i and not i % k else c for i, c in enumerate(s.upper().replace('-', '')[::-1])][::-1])

    def licenseKeyFormatting(self, s: str, k: int) -> str:  # O(n) time and O(n) space
        s = s.replace('-', '').upper()
        return '-'.join(([s[:len(s) % k]] + [s[i:i + k] for i in range(len(s) % k, len(s), k)])).lstrip('-')


def test():
    arguments = [
        ("5F3Z-2e-9-w", 4),
        ("2-5g-3-J", 2),
        ("a-bc-defghi", 3)
        ]
    expectations = ["5F3Z-2E9W", "2-5G-3J", "ABC-DEF-GHI"]
    for (s, k), expected in zip(arguments, expectations):
        solution = Solution().licenseKeyFormatting(s, k)
        assert solution == expected
test()