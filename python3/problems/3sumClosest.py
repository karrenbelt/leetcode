### Source : https://leetcode.com/problems/3sum-closest/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-11

##################################################################################################### 
#
# Given an array nums of n integers and an integer target, find three integers in nums such that the 
# sum is closest to target. Return the sum of the three integers. You may assume that each input 
# would have exactly one solution.
# 
# Example 1:
# 
# Input: nums = [-1,2,1,-4], target = 1
# Output: 2
# Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
# 
# Constraints:
# 
# 	3 <= nums.length <= 103
# 	-103 <= nums[i] <= 103
# 	-104 <= target <= 104
#####################################################################################################

from typing import List


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:  # O(n^2)
        nums.sort()
        closest = sum(nums[:3])
        for i in range(len(nums) - 2):   # iterate all but last two
            j, k = i + 1, len(nums) - 1  # two-pointer to explore right side of array, not including i
            while j < k:
                s = nums[i] + nums[j] + nums[k]
                if s == target:  # early stopping if we find the target
                    return target
                if abs(s - target) < abs(closest - target):
                    closest = s
                if s < target:
                    j += 1
                else:
                    k -= 1
        return closest


def test():
    arrays_of_numbers = [
        [-1, 2, 1, -4],
    ]
    targets = [1, ]
    expectations = [2, ]
    for nums, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().threeSumClosest(nums, target)
        assert solution == expected
