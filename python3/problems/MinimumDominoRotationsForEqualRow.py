### Source : https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# In a row of dominoes, A[i] and B[i] represent the top and bottom halves of the ith domino.  (A 
# domino is a tile with two numbers from 1 to 6 - one on each half of the tile.)
# 
# We may rotate the ith domino, so that A[i] and B[i] swap values.
# 
# Return the minimum number of rotations so that all the values in A are the same, or all the values 
# in B are the same.
# 
# If it cannot be done, return -1.
# 
# Example 1:
# 
# Input: A = [2,1,2,4,2,2], B = [5,2,6,2,3,2]
# Output: 2
# Explanation: 
# The first figure represents the dominoes as given by A and B: before we do any rotations.
# If we rotate the second and fourth dominoes, we can make every value in the top row equal to 2, as 
# indicated by the second figure.
# 
# Example 2:
# 
# Input: A = [3,5,1,2,3], B = [3,6,3,3,4]
# Output: -1
# Explanation: 
# In this case, it is not possible to rotate the dominoes to make one row of values equal.
# 
# Constraints:
# 
# 	2 <= A.length == B.length <= 2 * 104
# 	1 <= A[i], B[i] <= 6
#####################################################################################################

from typing import List


class Solution:
    def minDominoRotations(self, A: List[int], B: List[int]) -> int:  # O(n) time and O(1) space
        for val in {A[0], B[0]}:
            if all(val in pair for pair in zip(A, B)):
                return len(A) - max(A.count(val), B.count(val))
        return -1

    def minDominoRotations(self, A: List[int], B: List[int]) -> int:  # O(n) time and O(1) space
        return next((len(A) - max(A.count(v), B.count(v)) for v in {A[0], B[0]} if all(v in p for p in zip(A, B))), -1)

    def minDominoRotations(self, A: List[int], B: List[int]) -> int:  # O(A + B) time and O(A + B) space
        from collections import Counter
        if len(A) != len(B):
            return -1
        same, countA, countB = Counter(), Counter(A), Counter(B)
        for a, b in zip(A, B):
            if a == b:
                same[a] += 1
        for i in range(1, 7):
            if countA[i] + countB[i] - same[i] == len(A):
                return min(countA[i], countB[i]) - same[i]
        return -1

    def minDominoRotations(self, A: List[int], B: List[int]) -> int:  # O(A + B) time and O(A + B) space
        from collections import Counter
        ctrA, ctrB = Counter(A), Counter(B); same = Counter(a for a, b in zip(A, B) if a == b and len(A) == len(B))
        return next((min(ctrA[i], ctrB[i]) - same[i] for i in range(1, 7) if ctrA[i] + ctrB[i] - same[i] == len(A)), -1)


def test():
    arguments = [
        ([2, 1, 2, 4, 2, 2], [5, 2, 6, 2, 3, 2]),
        ([3, 5, 1, 2, 3], [3, 6, 3, 3, 4]),
        ([2, 5, 5, 2, 4], [5, 2, 2, 4, 2]),
        ([1, 2, 1, 1, 1, 2, 2, 2], [2, 1, 2, 2, 2, 2, 2, 2]),
    ]
    expectations = [2, -1, 2, 1]
    for (A, B), expected in zip(arguments, expectations):
        solution = Solution().minDominoRotations(A, B)
        assert solution == expected
