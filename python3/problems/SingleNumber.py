### Source : https://leetcode.com/problems/single-number/
### Author : karrenbelt
### Date   : 2019-05-15

##################################################################################################### 
#
# Given a non-empty array of integers, every element appears twice except for one. Find that single 
# one.
# 
# Note:
# 
# Your algorithm should have a linear runtime complexity. Could you implement it without using extra 
# memory?
# 
# Example 1:
# 
# Input: [2,2,1]
# Output: 1
# 
# Example 2:
# 
# Input: [4,1,2,1,2]
# Output: 4
# 
#####################################################################################################

from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        seen = set()
        for n in nums:
            if n in seen:
                seen.remove(n)
            else:
                seen.add(n)
        return seen.pop()

    def singleNumber(self, nums: List[int]) -> int:
        return 2 * sum(set(nums)) - sum(nums)

    def singleNumber(self, nums: List[int]) -> int:
        from collections import Counter
        return Counter(nums).most_common().pop()[0]

    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        digits = 0
        for n in nums:
            digits ^= n  # binary xor
        return digits

    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        while len(nums) > 1:
            nums[0] ^= nums.pop()
        return nums.pop()

    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        from functools import reduce
        return reduce(lambda x, y: x ^ y, nums)


def test():
    numbers = [
        [2, 2, 1],
        [4, 1, 2, 1, 2],
        [1],
    ]
    expectations = [1, 4, 1]
    for nums, expected in zip(numbers, expectations):
        solution = Solution().singleNumber(nums)
        assert solution == expected
