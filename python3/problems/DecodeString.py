### Source : https://leetcode.com/problems/decode-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given an encoded string, return its decoded string.
# 
# The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is 
# being repeated exactly k times. Note that k is guaranteed to be a positive integer.
# 
# You may assume that the input string is always valid; No extra white spaces, square brackets are 
# well-formed, etc.
# 
# Furthermore, you may assume that the original data does not contain any digits and that digits are 
# only for those repeat numbers, k. For example, there won't be input like 3a or 2[4].
# 
# Example 1:
# Input: s = "3[a]2[bc]"
# Output: "aaabcbc"
# Example 2:
# Input: s = "3[a2[c]]"
# Output: "accaccacc"
# Example 3:
# Input: s = "2[abc]3[cd]ef"
# Output: "abcabccdcdcdef"
# Example 4:
# Input: s = "abc3[cd]xyz"
# Output: "abccdcdcdxyz"
# 
# Constraints:
# 
# 	1 <= s.length <= 30
# 	s consists of lowercase English letters, digits, and square brackets '[]'.
# 	s is guaranteed to be a valid input.
# 	All the integers in s are in the range [1, 300].
#####################################################################################################


class Solution:
    def decodeString(self, s: str) -> str:  # O(n) time
        stack = []
        for char in s:
            if char == ']':
                sub = stack.pop()
                while sub[0] != '[':
                    sub = stack.pop() + sub
                digit = stack.pop()
                while stack and stack[-1].isdigit():
                    digit = stack.pop() + digit
                stack.extend(sub[1:] * int(digit))
            else:
                stack.append(char)
        return ''.join(stack)


def test():
    strings = [
        "3[a]2[bc]",
        "3[a2[c]]",
        "2[abc]3[cd]ef",
        "abc3[cd]xyz",
        ]
    expectations = [
        "aaabcbc",
        "accaccacc",
        "abcabccdcdcdef",
        "abccdcdcdxyz",
        ]
    for s, expected in zip(strings, expectations):
        solution = Solution().decodeString(s)
        assert solution == expected

