### Source : https://leetcode.com/problems/n-ary-tree-postorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given an n-ary tree, return the postorder traversal of its nodes' values.
# 
# For example, given a 3-ary tree:
# 
# Return its postorder traversal as: [5,6,3,2,4,1].
# 
# Note:
# 
# Recursive solution is trivial, could you do it iteratively?
#####################################################################################################

from typing import List
from python3 import NAryTreeNode as Node


class Solution:
    def postorder(self, root: 'Node') -> List[int]:
        # Top Down:
        # 1. return specific value for null node
        # 2. update the answer if needed                              // answer <-- params
        # 3. for each child node root.children[k]:
        # 4.      ans[k] = top_down(root.children[k], new_params[k])  // new_params <-- root.val, params
        # 5. return the answer if needed

        def traverse(node: 'Node'):
            if not node:
                return
            for child in node.children:
                traverse(child)
            ordered.append(node.val)

        ordered = []
        traverse(root)
        return ordered


def test():
    from python3 import NAryTreeCodec
    arguments = [
        "[1,null,3,2,4,null,5,6]",
        "[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]",
        ]
    expectations = [
        [5, 6, 3, 2, 4, 1],
        [2, 6, 14, 11, 7, 3, 12, 8, 4, 13, 9, 10, 5, 1],
        ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = NAryTreeCodec.deserialize(serialized_tree)
        solution = Solution().postorder(root)
        assert solution == expected
