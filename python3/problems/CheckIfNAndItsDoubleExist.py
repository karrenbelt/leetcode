### Source : https://leetcode.com/problems/check-if-n-and-its-double-exist/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-14

##################################################################################################### 
#
# Given an array arr of integers, check if there exists two integers N and  such that N is the 
# double of  ( i.e. N = 2 * ).
# 
# ore formally check if there exists two indices i and j such that :
# 
# 	i != j
# 	0 <= i, j < arr.length
# 	arr[i] == 2 * arr[j]
# 
# Example 1:
# 
# Input: arr = [10,2,5,3]
# Output: true
# Explanation: N = 10 is the double of  = 5,that is, 10 = 2 * 5.
# 
# Example 2:
# 
# Input: arr = [7,1,14,11]
# Output: true
# Explanation: N = 14 is the double of  = 7,that is, 14 = 2 * 7.
# 
# Example 3:
# 
# Input: arr = [3,1,7,11]
# Output: false
# Explanation: In this case does not exist N and , such that N = 2 * .
# 
# Constraints:
# 
# 	2 <= arr.length <= 500
# 	-103 <= arr[i] <= 103
#####################################################################################################

from typing import List


class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:  # O(n^2) time
        for i in range(len(arr)):
            if arr[i]*2 in set(arr[:i] + arr[i+1:]):
                return True
        return False

    def checkIfExist(self, arr: List[int]) -> bool:  # O(n) time
        return (nums := set(arr)) and any(arr.count(n) > 1 if n == 0 else n * 2 in nums for n in nums)


def test():
    arguments = [
        [10, 2, 5, 3],
        [7, 1, 14, 11],
        [3, 1, 7, 11],
        [-2, 0, 10, -19, 4, 6, -8],
    ]
    expectations = [True, True, False, False]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().checkIfExist(arr)
        assert solution == expected
