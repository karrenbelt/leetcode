### Source : https://leetcode.com/problems/maximum-subarray/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an integer array nums, find the contiguous subarray (containing at least one number) which 
# has the largest sum and return its sum.
# 
# Example:
# 
# Input: [-2,1,-3,4,-1,2,1,-5,4],
# Output: 6
# Explanation: [4,-1,2,1] has the largest sum = 6.
# 
# Follow up:
# 
# If you have figured out the O(n) solution, try coding another solution using the divide and conquer 
# approach, which is more subtle.
#####################################################################################################

from typing import List


class Solution:  # Kadane's algorithm
    # https://en.wikipedia.org/wiki/Maximum_subarray_problem#Kadane's_algorithm
    def maxSubArray(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        current = maximum = -10**4
        for n in nums:
            current = max(n, current + n)
            maximum = max(maximum, current)
        return maximum

    def maxSubArray(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        for i in range(1, len(nums)):
            if nums[i - 1] > 0:
                nums[i] += nums[i - 1]
        return max(nums)

    def maxSubArray(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        from itertools import accumulate
        return max(accumulate(nums, lambda x, y: max(x + y, y)))
        # return max(accumulate(nums, lambda x, y: x + y if x > 0 else y))  # alternatively

    def maxSubArray(self, nums: List[int]) -> int:

        def divide_and_conquer(l: int, r: int) -> int:

            if l == r:
                return nums[l]

            mid = l + (r - l) // 2
            left_max = divide_and_conquer(l, mid)
            right_max = divide_and_conquer(mid + 1, r)

            ans, left, right = 0, float('-inf'), float('-inf')
            for i in range(mid, l - 1, -1):
                ans += nums[i]
                left = max(left, ans)

            ans = 0
            for j in range(mid + 1, r + 1):
                ans += nums[j]
                right = max(ans, right)

            return max(left_max, right_max, left + right)

        return divide_and_conquer(0, len(nums) - 1)

    def maxSubArray(self, nums: List[int]) -> int:  # O(n) time

        def divide_and_conquer(i: int, j: int):
            if i == j - 1:
                return nums[i], nums[i], nums[i], nums[i]

            # a which is max contiguous sum in nums[i:j] including the first value
            # m which is max contiguous sum in nums[i:j] anywhere
            # b which is max contiguous sum in nums[i:j] including the last value
            # s which is the sum of all values in nums[i:j]
            mid = i + (j - i) // 2
            a1, m1, b1, s1 = divide_and_conquer(i, mid)
            a2, m2, b2, s2 = divide_and_conquer(mid, j)

            # combine a, m, b, s values from left and right halves to form a, m, b, s for whole array (bottom up)
            a = max(a1, s1 + a2)
            b = max(b2, s2 + b1)
            m = max(m1, m2, b1 + a2)
            s = s1 + s2
            return a, m, b, s

        return divide_and_conquer(0, len(nums))[1]


def test():
    arrays_of_numbers = [
        [-2, 1, -3, 4, -1, 2, 1, -5, 4],
        [1],
        [0],
        ]
    expectations = [6, 1, 0]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().maxSubArray(nums)
        assert solution == expected
