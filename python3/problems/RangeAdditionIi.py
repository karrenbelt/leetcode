### Source : https://leetcode.com/problems/range-addition-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

#####################################################################################################
#
# Given an m * n matrix  initialized with all 0's and several update operations.
# Operations are represented by a 2D array, and each operation is represented by an array with two
# positive integers a and b, which means [i][j] should be added by one for all 0 <= i < a and 0 <= j < b.
# You need to count and return the number of maximum integers in the matrix after performing all the
# operations.
#
# Example 1:
#
# Input:
# m = 3, n = 3
# operations = [[2,2],[3,3]]
# Output: 4
# Explanation:
# Initially,  =
# [[0, 0, 0],
#  [0, 0, 0],
#  [0, 0, 0]]
#
# After performing [2,2],  =
# [[1, 1, 0],
#  [1, 1, 0],
#  [0, 0, 0]]
#
# After performing [3,3],  =
# [[2, 2, 1],
#  [2, 2, 1],
#  [1, 1, 1]]
#
# So the maximum integer in  is 2, and there are four of it in . So return 4.
#
# Note:
#
# The range of m and n is [1,40000].
# The range of a is [1,m], and the range of b is [1,n].
# The range of operations size won't exceed 10,000.
#
#####################################################################################################
from typing import List


class Solution:
    def maxCount(self, m: int, n: int, ops: List[List[int]]) -> int:  # brute force: O(x * m * n) time, O(m * n) space
        matrix = [[0] * n for _ in range(m)]
        for op_n, op_m in ops:
            for i in range(op_n):
                for j in range(op_m):
                    matrix[i][j] += 1
        return sum(1 for i in range(n) for j in range(m) if matrix[i][j] == matrix[0][0])

    def maxCount(self, m: int, n: int, ops: List[List[int]]) -> int:  # O(ops) time and O(1) space
        # and this is the obvious solution
        for (i, j) in ops:
            m = min(m, i)
            n = min(n, j)
        return m * n


def test():
    arguments = [
        (3, 3, [[2, 2], [3, 3]]),
        (3, 3, [[2, 2], [3, 3], [3, 3], [3, 3], [2, 2], [3, 3], [3, 3], [3, 3], [2, 2], [3, 3], [3, 3], [3, 3]]),
        (3, 3, []),
        ]
    expectations = [4, 4, 9]
    for (m, n, ops), expected in zip(arguments, expectations):
        solution = Solution().maxCount(m, n, ops)
        assert solution == expected
