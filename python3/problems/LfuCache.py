### Source : https://leetcode.com/problems/lfu-cache/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-02

##################################################################################################### 
#
# Design and implement a data structure for a Least Frequently Used (LFU) cache.
# 
# Implement the LFUCache class:
# 
# 	LFUCache(int capacity) Initializes the object with the capacity of the data structure.
# 	int get(int key) Gets the value of the key if the key exists in the cache. Otherwise, 
# returns -1.
# 	void put(int key, int value) Update the value of the key if present, or inserts the key if 
# not already present. When the cache reaches its capacity, it should invalidate and remove the least 
# frequently used key before inserting a new item. For this problem, when there is a tie (i.e., two 
# or more keys with the same frequency), the least recently used key would be invalidated.
# 
# To determine the least frequently used key, a use counter is maintained for each key in the cache. 
# The key with the smallest use counter is the least frequently used key.
# 
# When a key is first inserted into the cache, its use counter is set to 1 (due to the put 
# operation). The use counter for a key in the cache is incremented either a get or put operation is 
# called on it.
# 
# Example 1:
# 
# Input
# ["LFUCache", "put", "put", "get", "put", "get", "get", "put", "get", "get", "get"]
# [[2], [1, 1], [2, 2], [1], [3, 3], [2], [3], [4, 4], [1], [3], [4]]
# Output
# [null, null, null, 1, null, -1, 3, null, -1, 3, 4]
# 
# Explanation
# // cnt(x) = the use counter for key x
# // cache=[] will show the last used order for tiebreakers (leftmost element is  most recent)
# LFUCache lfu = new LFUCache(2);
# lfu.put(1, 1);   // cache=[1,_], cnt(1)=1
# lfu.put(2, 2);   // cache=[2,1], cnt(2)=1, cnt(1)=1
# lfu.get(1);      // return 1
#                  // cache=[1,2], cnt(2)=1, cnt(1)=2
# lfu.put(3, 3);   // 2 is the LFU key because cnt(2)=1 is the smallest, invalidate 2.
#                  // cache=[3,1], cnt(3)=1, cnt(1)=2
# lfu.get(2);      // return -1 (not found)
# lfu.get(3);      // return 3
#                  // cache=[3,1], cnt(3)=2, cnt(1)=2
# lfu.put(4, 4);   // Both 1 and 3 have the same cnt, but 1 is LRU, invalidate 1.
#                  // cache=[4,3], cnt(4)=1, cnt(3)=2
# lfu.get(1);      // return -1 (not found)
# lfu.get(3);      // return 3
#                  // cache=[3,4], cnt(4)=1, cnt(3)=3
# lfu.get(4);      // return 4
#                  // cache=[3,4], cnt(4)=2, cnt(3)=3
# 
# Constraints:
# 
# 	0 <= capacity, key, value <= 104
# 	At most 105 calls will be made to get and put.
# 
# Follow up: Could you do both operations in O(1) time complexity? 
#####################################################################################################

import heapq


class LFUCache:
    # we need frequency, and usage
    def __init__(self, capacity: int):
        self.capacity = capacity
        # self.cache = [[0, 0, 0] for _ in range(capacity)]  # frequency, last use, value
        self.cache = {}  # frequency
        self.frequency = {}
        self.heap = []
        self.time = 0

    def get(self, key: int) -> int:  # O(1)
        if key in self.cache:
            self.frequency[key][0] += 1
            self.frequency[key][1] = self.time = self.time + 1
            return self.cache[key]
        return -1

    def put(self, key: int, value: int) -> None:  # O(log k)
        if not self.capacity:
            return  # herp-a-derp such a fantastic test case :D
        self.time += 1
        if key in self.cache:
            self.frequency[key][0] += 1
            self.frequency[key][1] = self.time
            self.cache[key] = value
            return
        elif len(self.frequency) == self.capacity:
            while True:
                count, recency, elem = heapq.heappop(self.heap)
                if self.frequency[elem] == [count, recency]:
                    break
                heapq.heappush(self.heap, (*self.frequency[elem], elem))
            del self.frequency[elem]
            del self.cache[elem]
        self.frequency[key] = [1, self.time]
        self.cache[key] = value
        heapq.heappush(self.heap, (1, self.time, key))


# Your LFUCache object will be instantiated and called as such:
# obj = LFUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)


def test():
    operations = ["LFUCache", "put", "put", "get", "put", "get", "get", "put", "get", "get", "get"]
    arguments = [[2], [1, 1], [2, 2], [1], [3, 3], [2], [3], [4, 4], [1], [3], [4]]
    expectations = [None, None, None, 1, None, -1, 3, None, -1, 3, 4]
    obj = LFUCache(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected, expected
