### Source : https://leetcode.com/problems/first-unique-character-in-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given a string, find the first non-repeating character in it and return its index. If it doesn't 
# exist, return -1.
# 
# Examples:
# 
# s = "leetcode"
# return 0.
# 
# s = "loveleetcode"
# return 2.
# 
# Note: You may assume the string contains only lowercase English letters.
#####################################################################################################


class Solution:
    def firstUniqChar(self, s: str) -> int:
        d = {}
        for i, c in enumerate(s):
            d.setdefault(c, [0, i])[0] += 1
        least = min(d.values(), key=lambda x: x[0]) if s else [-1, -1]
        return least[1] if least[0] == 1 else -1

    def firstUniqChar(self, s: str) -> int:
        seen = {}
        candidates = set(list(s))
        for i, c in enumerate(s):
            if c not in seen:
                seen[c] = i
            elif c in candidates:
                candidates.remove(c)
        return min(seen[c] for c in candidates) if candidates else -1

    def firstUniqChar(self, s: str) -> int:  # O(26n)
        return min((s.index(c) for c in set(s) if s.count(c) == 1), default=-1)


def test():
    strings = ["leetcode", "loveleetcode"]
    expectations = [0, 2]
    for s, expected in zip(strings, expectations):
        solution = Solution().firstUniqChar(s)
        assert solution == expected
