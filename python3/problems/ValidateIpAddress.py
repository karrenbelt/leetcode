### Source : https://leetcode.com/problems/validate-ip-address/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-10

##################################################################################################### 
#
# Given a string IP, return "IPv4" if IP is a valid IPv4 address, "IPv6" if IP is a valid IPv6 
# address or "Neither" if IP is not a correct IP of any type.
# 
# A valid IPv4 address is an IP in the form "x1.x2.x3.x4" where 0 <= xi <= 255 and xi cannot contain 
# leading zeros. For example, "192.168.1.1" and "192.168.1.0" are valid IPv4 addresses but 
# "192.168.01.1", while "192.168.1.00" and "192.168@1.1" are invalid IPv4 addresses.
# 
# A valid IPv6 address is an IP in the form "x1:x2:x3:x4:x5:x6:x7:x8" where:
# 
# 	1 <= xi.length <= 4
# 	xi is a hexadecimal string which may contain digits, lower-case English letter ('a' to 'f') 
# and upper-case English letters ('A' to 'F').
# 	Leading zeros are allowed in xi.
# 
# For example, "2001:0db8:85a3:0000:0000:8a2e:0370:7334" and "2001:db8:85a3:0:0:8A2E:0370:7334" are 
# valid IPv6 addresses, while "2001:0db8:85a3::8A2E:037j:7334" and 
# "02001:0db8:85a3:0000:0000:8a2e:0370:7334" are invalid IPv6 addresses.
# 
# Example 1:
# 
# Input: IP = "172.16.254.1"
# Output: "IPv4"
# Explanation: This is a valid IPv4 address, return "IPv4".
# 
# Example 2:
# 
# Input: IP = "2001:0db8:85a3:0:0:8A2E:0370:7334"
# Output: "IPv6"
# Explanation: This is a valid IPv6 address, return "IPv6".
# 
# Example 3:
# 
# Input: IP = "256.256.256.256"
# Output: "Neither"
# Explanation: This is neither a IPv4 address nor a IPv6 address.
# 
# Example 4:
# 
# Input: IP = "2001:0db8:85a3:0:0:8A2E:0370:7334:"
# Output: "Neither"
# 
# Example 5:
# 
# Input: IP = "1e1.4.5.6"
# Output: "Neither"
# 
# Constraints:
# 
# 	IP consists only of English letters, digits and the characters '.' and ':'.
#####################################################################################################


class Solution:
    def validIPAddress(self, IP: str) -> str:
        import string
        if IP.count('.') == 3:
            if all([part == str(int(part)) and 0 <= int(part) <= 255
                    if part.isnumeric() else False for part in IP.split('.')]):
                return "IPv4"
        if IP.count(':') == 7:  # chr(int(hex(ord('a')), 0))
            if all([int(c, 16) >= 0 if c in string.hexdigits else False
                    if 0 < len(part) <= 4 else 'g' for part in IP.split(':') for c in part]):
                return "IPv6"
        return "Neither"

    def validIPAddress(self, IP: str) -> str:
        import string

        def is_ipv4():
            ls = IP.split('.')
            if len(ls) == 4 and all(g.isdigit() and str(int(g)) == g and 0 <= int(g) <= 255 for g in ls):
                return True
            return False

        def is_ipv6():
            ls = IP.split(':')
            if len(ls) == 8 and all(0 < len(g) <= 4 and all(c in string.hexdigits for c in g) for g in ls):
                return True
            return False

        if is_ipv4():
            return 'IPv4'
        elif is_ipv6():
            return 'IPv6'
        return 'Neither'


def test():
    arguments = [
        "172.16.254.1",
        "2001:0db8:85a3:0:0:8A2E:0370:7334",
        "256.256.256.256",
        "2001:0db8:85a3:0:0:8A2E:0370:7334:",
        "1e1.4.5.6",
        "2001:0db8:85a3:00000:0:8A2E:0370:7334",
        "2001:db8:85a3:0::8a2E:0370:7334",
    ]
    expectations = ["IPv4", "IPv6", "Neither", "Neither", "Neither", "Neither", "Neither"]
    for IP, expected in zip(arguments, expectations):
        solution = Solution().validIPAddress(IP)
        assert solution == expected

