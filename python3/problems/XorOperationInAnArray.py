### Source : https://leetcode.com/problems/xor-operation-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-24

##################################################################################################### 
#
# Given an integer n and an integer start.
# 
# Define an array nums where nums[i] = start + 2*i (0-indexed) and n == nums.length.
# 
# Return the bitwise XOR of all elements of nums.
# 
# Example 1:
# 
# Input: n = 5, start = 0
# Output: 8
# Explanation: Array nums is equal to [0, 2, 4, 6, 8] where (0  2  4  6  8) = 8.
# Where "" corresponds to bitwise XOR operator.
# 
# Example 2:
# 
# Input: n = 4, start = 3
# Output: 8
# Explanation: Array nums is equal to [3, 5, 7, 9] where (3  5  7  9) = 8.
# 
# Example 3:
# 
# Input: n = 1, start = 7
# Output: 7
# 
# Example 4:
# 
# Input: n = 10, start = 5
# Output: 2
# 
# Constraints:
# 
# 	1 <= n <= 1000
# 	0 <= start <= 1000
# 	n == nums.length
#####################################################################################################


class Solution:
    def xorOperation(self, n: int, start: int) -> int:  # brute force O(n) time
        res = 0
        for i in range(n):
            res ^= start + 2 * i
        return res

    def xorOperation(self, n: int, start: int) -> int:  # O(1) time and O(1) space

        last = start + 2 * (n - 1)

        if start % 4 <= 1:
            if n % 4 == 1:
                return last
            elif n % 4 == 2:
                return 2
            elif n % 4 == 3:
                return 2 ^ last
            else:
                return 0
        else:
            if n % 4 == 1:
                return start
            elif n % 4 == 2:
                return start ^ last
            elif n % 4 == 3:
                return start ^ 2
            else:
                return start ^ 2 ^ last

    # def xorOperation(self, n: int, start: int) -> int:  # recursive - incorrect
    #     def f(x: int):
    #         if x < 0:
    #             x = 0
    #         return {0: x,
    #                 1: 1,
    #                 2: x+1,
    #                 3: 0}[x % 4]
    #
    #     return 2 * f(start//2+n-1) ** f(start // 2 - 1) ^ (start & n & 1)

    def xorOperation(self, n: int, start: int) -> int:  # O(log n) time
        last = start + 2 * (n - 1)
        if start % 4 < 2:
            start = 0
        else:
            n -= 1
        return start ^ (n & 2) if n % 2 == 0 else start ^ last ^ (n & 2)

    def xorOperation(self, n: int, start: int) -> int:  # O(n) time
        import operator
        from functools import reduce
        return reduce(operator.xor, range(start, start + 2 * n, 2))

    def xorOperation(self, n: int, start: int) -> int:
        # The last bit is 1 after all XOR operations iff start is odd and n is odd, otherwise 0: (start % 2) * (n % 2)
        # Note that when we divide all numbers by 2, we are effectively calculating
        # (0^1^2^...^(a-1) ) ^ (0^1^2^...^b), where a and b are the start and end position of the array divided by 2
        # Now how do we calculate (0^1^2^...^a) in O(1) time?
        # (0^1^2^...^a) would be 1 or 0 for the first a//2 pairs of numbers: (a//2)%2 will be 1, otherwise 0.
        # last part (a-1) * (a%2) is for the remaining term
        # part 1: (a//2)%2 ^ (a-1)*(a%2)
        # part 2: (b//2)%2 ^ (b-1)*(b%2)
        # which combines as follows: part 1 + part 2 * 2
        end = start + n * 2
        a, b = start // 2, end // 2
        return (start % 2) * (n % 2) + \
               ((((a - 1) * (a % 2)) ^ ((a // 2) % 2)) ^ ((b - 1) * (b % 2)) ^ ((b // 2) % 2)) * 2

    def xorOperation(self, n: int, start: int) -> int:

        def is_even(i):
            return i % 2 == 0

        def is_odd(i):
            return i % 2 == 1

        ans = 0

        # if n is odd and start is odd, then the last bit of ans will be 1 otherwise, the last bit of ans is 0
        ans ^= is_odd(n) and is_odd(start)

        # now we can only consider the right-shifted number sequence consider the case 5, 6, 7, 8, 9, 10
        # (6, 7) is a pair which produces a 1, same as (8, 9)
        # thus, we need to take care of the 5
        new_start = start // 2
        ans ^= new_start << 1 if is_odd(new_start) else 0

        # and we have to take care of the 10
        new_end = new_start + n - 1
        ans ^= new_end << 1 if is_even(new_end) else 0

        # finally we have to calculate how many pairs
        num_pairs = n // 2 - int(is_even(n) and is_odd(new_start))
        ans ^= is_odd(num_pairs) << 1

        return ans


def test():
    arguments = [
        (5, 0),
        (4, 3),
        (1, 7),
        (10, 5),
    ]
    expectations = [8, 8, 7, 2]
    for (n, start), expected in zip(arguments, expectations):
        solution = Solution().xorOperation(n, start)
        assert solution == expected
