### Source : https://leetcode.com/problems/find-and-replace-pattern/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

##################################################################################################### 
#
# Given a list of strings words and a string pattern, return a list of words[i] that match pattern. 
# You may return the answer in any order.
# 
# A word matches the pattern if there exists a permutation of letters p so that after replacing every 
# letter x in the pattern with p(x), we get the desired word.
# 
# Recall that a permutation of letters is a bijection from letters to letters: every letter maps to 
# another letter, and no two letters map to the same letter.
# 
# Example 1:
# 
# Input: words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
# Output: ["mee","aqq"]
# Explanation: "mee" matches the pattern because there is a permutation {a -> m, b -> e, ...}. 
# "ccc" does not match the pattern because {a -> c, b -> c, ...} is not a permutation, since a and b 
# map to the same letter.
# 
# Example 2:
# 
# Input: words = ["a","b","c"], pattern = "a"
# Output: ["a","b","c"]
# 
# Constraints:
# 
# 	1 <= pattern.length <= 20
# 	1 <= words.length <= 50
# 	words[i].length == pattern.length
# 	pattern and words[i] are lowercase English letters.
#####################################################################################################

from typing import List


class Solution:  # all solutions are O(mn) time and O(m + n) space
    def findAndReplacePattern(self, words: List[str], pattern: str) -> List[str]:  # 44 ms
        matching = []
        for word in words:
            word_mapping, pattern_mapping = {}, {}
            for c1, p1 in zip(word, pattern):
                if word_mapping.get(c1, p1) != p1 or pattern_mapping.get(p1, c1) != c1:
                    break
                word_mapping[c1], pattern_mapping[p1] = p1, c1
            else:
                matching.append(word)
        return matching

    def findAndReplacePattern(self, words: List[str], pattern: str) -> List[str]:  # 40 ms
        return [word for word in words
                if word.translate(word.maketrans(word, pattern)) == pattern
                and pattern.translate(word.maketrans(pattern, word)) == word]

    def findAndReplacePattern(self, words: List[str], pattern: str) -> List[str]:  # 44 ms
        def f(word: str) -> List[int]:
            mapping = {}
            return [mapping.setdefault(char, len(mapping)) for char in word]
        f_pattern = f(pattern)
        return [word for word in words if f(word) == f_pattern]

    def findAndReplacePattern(self, words: List[str], pattern: str) -> List[str]:  # 32 ms
        return [word for word in words if (len(set(pattern)) == len(set(word)) == len(set(zip(word, pattern))))]

    def findAndReplacePattern(self, words: List[str], pattern: str) -> List[str]:  # 36 ms
        return [w for w in words if tuple(w.index(c) for c in w) == tuple(pattern.index(c) for c in pattern)]


def test():
    arguments = [
        (["abc", "deq", "mee", "aqq", "dkd", "ccc"], "abb"),
        (["a", "b", "c"], "a"),
    ]
    expectations = [
        ["mee", "aqq"],
        ["a", "b", "c"],
    ]
    for (words, pattern), expected in zip(arguments, expectations):
        solution = Solution().findAndReplacePattern(words, pattern)
        assert solution == expected, solution
