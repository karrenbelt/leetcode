### Source : https://leetcode.com/problems/subsets-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-16

##################################################################################################### 
#
# Given an integer array nums that may contain duplicates, return all possible subsets (the power 
# set).
# 
# The solution set must not contain duplicate subsets. Return the solution in any order.
# 
# Example 1:
# Input: nums = [1,2,2]
# Output: [[],[1],[1,2],[1,2,2],[2],[2,2]]
# Example 2:
# Input: nums = [0]
# Output: [[],[0]]
# 
# Constraints:
# 
# 	1 <= nums.length <= 10
# 	-10 <= nums[i] <= 10
#####################################################################################################

from typing import List


class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:

        def backtrack(index, path):
            power_set.append(path[:])
            for i in range(index, len(nums)):
                if i > index and nums[i] == nums[i - 1]:
                    continue
                path.append(nums[i])
                backtrack(i + 1, path)
                path.pop()

        nums.sort()
        power_set = []
        backtrack(0, [])
        return power_set

    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:  # dp
        nums.sort()
        dp = {0: [[], [nums[0]]]}
        for i in range(1, len(nums)):
            dp[i] = dp[i - 1] + [x + [nums[i]] for x in dp[i - 1] if x + [nums[i]] not in dp[i - 1]]
        return dp[len(nums) - 1]

    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        power_set, pos = [[]], {}
        for n in nums:
            start, l = pos.get(n, 0), len(power_set)
            power_set += [r + [n] for r in power_set[start:]]
            pos[n] = l
        return power_set

    def subsetsWithDup(self, nums):
        nums.sort()
        power_set, next_subsets = [[]], []
        for i in range(len(nums)):
            if i > 0 and nums[i] == nums[i - 1]:
                next_subsets = [item + [nums[i]] for item in next_subsets]
            else:
                next_subsets = [item + [nums[i]] for item in power_set]
            power_set += next_subsets
        return power_set if nums else []


def test():
    arrays_of_numbers = [
        [1, 2, 2],
        [0],
        [1, 2, 2, 2]
        ]
    expectations = [
        [[], [1], [1, 2], [1, 2, 2], [2], [2, 2]],
        [[], [0]],
        [[], [1], [2], [1, 2], [2, 2], [1, 2, 2], [2, 2, 2], [1, 2, 2, 2]],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().subsetsWithDup(nums)
        assert set(map(frozenset, solution)) == set(map(frozenset, expected))
