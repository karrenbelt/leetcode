### Source : https://leetcode.com/problems/reverse-linked-list-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-01

##################################################################################################### 
#
# Reverse a linked list from position m to n. Do it in one-pass.
# 
# Note: 1 &le; m &le; n &le; length of list.
# 
# Example:
# 
# Input: 1->2->3->4->5->NULL, m = 2, n = 4
# Output: 1->4->3->2->5->NULL
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def reverseBetween(self, head: ListNode, m: int, n: int) -> ListNode:  # O(n) time and O(1) space

        # find the node before inversion
        first, before = head, None
        for i in range(m - 1):
            before = first
            first = first.next

        # reverse between m and n
        prev, curr, end = None, first, first
        while m <= n:
            curr.next, prev, curr = prev, curr, curr.next
            m += 1

        # if there is a node before, we attach the reversed list there, otherwise it is the head
        if before:
            before.next = prev
        else:
            head = prev

        # we append the remainder of the linked list that was not reversed
        end.next = curr
        return head

    def reverseBetween(self, head: ListNode, left: int, right: int) -> ListNode:
        dummy = ListNode(0)
        dummy.next = head

        node, prev = head, dummy
        for _ in range(left - 1):
            node = node.next
            prev = prev.next

        for _ in range(right - left):
            temp = node.next
            node.next = temp.next
            temp.next = prev.next
            prev.next = temp

        return dummy.next


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        ([1, 2, 3, 4, 5], 2, 4),
        ([5], 1, 1),
        ([3, 5], 1, 1)
        ]
    expectations = [
        [1, 4, 3, 2, 5],
        [5],
        [3, 5],
        ]
    for (nums, m, n), expected in zip(arrays_of_numbers, expectations):
        head = singly_linked_list_from_array(nums).head
        Solution().reverseBetween(head, m, n)
        assert head == singly_linked_list_from_array(expected).head
