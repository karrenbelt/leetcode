### Source : https://leetcode.com/problems/k-th-symbol-in-grammar/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-30

##################################################################################################### 
#
# We build a table of n rows (1-indexed). We start by writing 0 in the 1st row. Now in every 
# subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each 
# occurrence of 1 with 10.
# 
# 	For example, for n = 3, the 1st row is 0, the 2nd row is 01, and the 3rd row is 0110.
# 
# Given two integer n and k, return the kth (1-indexed) symbol in the nth row of a table of n rows.
# 
# Example 1:
# 
# Input: n = 1, k = 1
# Output: 0
# Explanation: row 1: 0
# 
# Example 2:
# 
# Input: n = 2, k = 1
# Output: 0
# Explanation:
# row 1: 0
# row 2: 01
# 
# Example 3:
# 
# Input: n = 2, k = 2
# Output: 1
# Explanation:
# row 1: 0
# row 2: 01
# 
# Example 4:
# 
# Input: n = 3, k = 1
# Output: 0
# Explanation:
# row 1: 0
# row 2: 01
# row 3: 0110
# 
# Constraints:
# 
# 	1 <= n <= 30
# 	1 <= k <= 2n - 1
#####################################################################################################


class Solution:
    def kthGrammar(self, N: int, K: int) -> int:  # O(n) time and O(n) space
        return 0 if N == 1 else (1 - K % 2) ^ self.kthGrammar(N - 1, (K + 1) // 2)

    def kthGrammar(self, N: int, K: int) -> int:   # O(n) time and O(log N) space
        return bin(K - 1).count('1') & 1

    def kthGrammar(self, N: int, K: int) -> int:  # O(log K) time and O(1) space
        res = 0
        while K > 1:
            K = K + 1 if K % 2 else K // 2
            res ^= 1
        return res


def test():
    arguments = [
        (1, 1),
        (2, 1),
        (2, 2),
        (3, 1),
    ]
    expectations = [0, 0, 1, 0]
    for (N, K), expected in zip(arguments, expectations):
        solution = Solution().kthGrammar(N, K)
        assert solution == expected
