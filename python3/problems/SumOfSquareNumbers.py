### Source : https://leetcode.com/problems/sum-of-square-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-07

##################################################################################################### 
#
# Given a non-negative integer c, decide whether there're two integers a and b such that a2 + b2 = c.
# 
# Example 1:
# 
# Input: c = 5
# Output: true
# Explanation: 1 * 1 + 2 * 2 = 5
# 
# Example 2:
# 
# Input: c = 3
# Output: false
# 
# Example 3:
# 
# Input: c = 4
# Output: true
# 
# Example 4:
# 
# Input: c = 2
# Output: true
# 
# Example 5:
# 
# Input: c = 1
# Output: true
# 
# Constraints:
# 
# 	0 <= c <= 231 - 1
#####################################################################################################


class Solution:
    def judgeSquareSum(self, c: int) -> bool:  # O(sqrt c) time and O(sqrt c) space
        squares = {i ** 2 for i in range(int(c ** 0.5) + 1)}
        for n in squares:
            if c - n in squares:
                return True
        return False

    def judgeSquareSum(self, c: int) -> bool:  # O(sqrt c) time and O(1) space
        # a*a = c - b*b <= c, because b*b >= 0, and 0 <= a <= sqrt(c)
        def is_square(n: int):
            return int(n ** .5) ** 2 == n

        return any(is_square(c - a * a) for a in range(int(c ** .5) + 1))

    def judgeSquareSum(self, c: int) -> bool:  # O(sqrt c) time and O(1) space
        return any(((c - n * n) ** .5).is_integer() for n in range(int(c ** .5) + 1))


def test():
    arguments = [5, 3, 4, 2, 1, 1000]
    expectations = [True, False, True, True, True, True]
    for c, expected in zip(arguments, expectations):
        solution = Solution().judgeSquareSum(c)
        assert solution == expected
