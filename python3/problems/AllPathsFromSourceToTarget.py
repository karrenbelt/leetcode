### Source : https://leetcode.com/problems/all-paths-from-source-to-target/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# Given a directed acyclic graph (DAG) of n nodes labeled from 0 to n - 1, find all possible paths 
# from node 0 to node n - 1, and return them in any order.
# 
# The graph is given as follows: graph[i] is a list of all nodes you can visit from node i (i.e., 
# there is a directed edge from node i to node graph[i][j]).
# 
# Example 1:
# 
# Input: graph = [[1,2],[3],[3],[]]
# Output: [[0,1,3],[0,2,3]]
# Explanation: There are two paths: 0 -> 1 -> 3 and 0 -> 2 -> 3.
# 
# Example 2:
# 
# Input: graph = [[4,3,1],[3,2,4],[3],[4],[]]
# Output: [[0,4],[0,3,4],[0,1,3,4],[0,1,2,3,4],[0,1,4]]
# 
# Example 3:
# 
# Input: graph = [[1],[]]
# Output: [[0,1]]
# 
# Example 4:
# 
# Input: graph = [[1,2,3],[2],[3],[]]
# Output: [[0,1,2,3],[0,2,3],[0,3]]
# 
# Example 5:
# 
# Input: graph = [[1,3],[2],[3],[]]
# Output: [[0,1,2,3],[0,3]]
# 
# Constraints:
# 
# 	n == graph.length
# 	2 <= n <= 15
# 	0 <= graph[i][j] < n
# 	graph[i][j] != i (i.e., there will be no self-loops).
# 	The input graph is guaranteed to be a DAG.
#####################################################################################################

from typing import List


class Solution:
    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:  # O(2^n) time

        def backtrack(i: int, path: List[int]) -> None:
            if i == len(graph) - 1:
                paths.append(path)
            for n in graph[i]:
                backtrack(n, path + [n])

        paths = []
        backtrack(0, [0])
        return paths

    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:  # O(2^n) time

        def backtrack(i: int, path: List[int]) -> None:
            paths.append(path) if i == len(graph) - 1 else any(backtrack(n, path + [n]) for n in graph[i])

        return (paths := []) or backtrack(0, [0]) or paths

    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:  # iterative
        paths, stack = [], [[0]]
        while stack:
            path = stack.pop()
            if path[-1] == len(graph) - 1:
                paths.append(path)
            else:
                for i in graph[path[-1]]:
                    stack.append(path + [i])
        return paths

    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:
        paths, stack = [], [[0]]
        while stack and (path := stack.pop()):
            paths.append(path) if path[-1] == len(graph) - 1 else any(stack.append(path + [i]) for i in graph[path[-1]])
        return paths


def test():
    from collections import Counter
    graphs = [
        [[1, 2], [3], [3], []],
        [[4, 3, 1], [3, 2, 4], [3], [4], []],
        [[1], []],
        [[1, 2, 3], [2], [3], []],
        [[1, 3], [2], [3], []],
        ]
    expectations = [
        [[0, 1, 3], [0, 2, 3]],
        [[0, 4], [0, 3, 4], [0, 1, 3, 4], [0, 1, 2, 3, 4], [0, 1, 4]],
        [[0, 1]],
        [[0, 1, 2, 3], [0, 2, 3], [0, 3]],
        [[0, 1, 2, 3], [0, 3]],
        ]
    for graph, expected in zip(graphs, expectations):
        solution = Solution().allPathsSourceTarget(graph)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected))
