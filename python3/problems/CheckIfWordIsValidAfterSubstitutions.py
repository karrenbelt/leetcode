### Source : https://leetcode.com/problems/check-if-word-is-valid-after-substitutions/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-09

##################################################################################################### 
#
# Given a string s, determine if it is valid.
# 
# A string s is valid if, starting with an empty string t = "", you can transform t into s after 
# performing the following operation any number of times:
# 
# 	Insert string "abc" into any position in t. ore formally, t becomes tleft + "abc" + 
# tright, where t == tleft + tright. Note that tleft and tright may be empty.
# 
# Return true if s is a valid string, otherwise, return false.
# 
# Example 1:
# 
# Input: s = "aabcbc"
# Output: true
# Explanation:
# "" -> "abc" -> "aabcbc"
# Thus, "aabcbc" is valid.
# 
# Example 2:
# 
# Input: s = "abcabcababcc"
# Output: true
# Explanation:
# "" -> "abc" -> "abcabc" -> "abcabcabc" -> "abcabcababcc"
# Thus, "abcabcababcc" is valid.
# 
# Example 3:
# 
# Input: s = "abccba"
# Output: false
# Explanation: It is impossible to get "abccba" using the operation.
# 
# Example 4:
# 
# Input: s = "cababc"
# Output: false
# Explanation: It is impossible to get "cababc" using the operation.
# 
# Constraints:
# 
# 	1 <= s.length <= 2 * 104
# 	s consists of letters 'a', 'b', and 'c'
#####################################################################################################


class Solution:
    def isValid(self, s: str) -> bool:  # TLE: 60 / 94 test cases passed.
        import re
        stack, letters = [s], "abc"
        while stack:
            node = stack.pop()
            if node == letters:
                return True
            indices = [m.start() for m in re.finditer(letters, node)]
            stack.extend([node[:i] + node[i + 3:] for i in indices])
        return False

    def isValid(self, s: str) -> bool:  # TLE: 61 / 94 test cases passed.
        import re
        stack, letters = [s], "abc"
        seen = set()
        while stack:
            node = stack.pop()
            if node == letters:
                return True
            indices = [m.start() for m in re.finditer(letters, node)]
            for i in indices:
                next_node = node[:i] + node[i + 3:]
                if next_node not in seen:
                    seen.add(next_node)
                    stack.append(next_node)
        return False

    def isValid(self, s: str) -> bool:
        stack = []
        for c in s:
            stack.append(c)
            if stack[-3:] == ['a', 'b', 'c']:
                stack.pop()
                stack.pop()
                stack.pop()
        return not stack

    def isValid(self, s: str) -> bool:  # O(n) time and O(n) space, early stopping
        stack = []
        for c in s:
            if c == 'c':
                if stack[-2:] != ['a', 'b']:
                    return False
                stack.pop()
                stack.pop()
            else:
                stack.append(c)
        return not stack

    def isValid(self, s: str) -> bool:  # O(n) time and O(n) space
        prev_s = ""
        while s != prev_s:
            s, prev_s = s.replace("abc", ""), s
        return not s


def test():
    arguments = [
        "aabcbc",
        "abcabcababcc",
        "abccba",
        "cababc",
        "abc" * 2500,
    ]
    expectations = [True, True, False, False, True]
    for s, expected in zip(arguments, expectations):
        solution = Solution().isValid(s)
        assert solution == expected
