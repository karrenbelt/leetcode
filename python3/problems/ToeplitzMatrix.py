### Source : https://leetcode.com/problems/toeplitz-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-26

##################################################################################################### 
#
# Given an m x n matrix, return true if the matrix is Toeplitz. Otherwise, return false.
# 
# A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same elements.
# 
# Example 1:
# 
# Input: matrix = [[1,2,3,4],[5,1,2,3],[9,5,1,2]]
# Output: true
# Explanation:
# In the above grid, the diagonals are:
# "[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]".
# In each diagonal all elements are the same, so the answer is True.
# 
# Example 2:
# 
# Input: matrix = [[1,2],[2,2]]
# Output: false
# Explanation:
# The diagonal "[1, 2]" has different elements.
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 20
# 	0 <= matrix[i][j] <= 99
# 
# Follow up:
# 
# 	What if the matrix is stored on disk, and the memory is limited such that you can only load 
# at most one row of the matrix into the memory at once?
# 	What if the matrix is so large that you can only load up a partial row into the memory at 
# once?
#####################################################################################################

from typing import List


class Solution:
    def isToeplitzMatrix(self, matrix: List[List[int]]) -> bool:  # O(mn) time and O(n) space
        prev_row = matrix[0][1:]
        for i, row in enumerate(matrix):
            for j in range(len(row) - 1):
                if prev_row[j] != row[j + 1]:
                    return False
            prev_row = row
        return True

    def isToeplitzMatrix(self, matrix: List[List[int]]) -> bool:  # O(mn) time and O(1) space
        for i in range(len(matrix) - 1):
            for j in range(len(matrix[0]) - 1):
                if matrix[i][j] != matrix[i + 1][j + 1]:
                    return False
        return True

    def isToeplitzMatrix(self, matrix: List[List[int]]) -> bool:  # O(mn) time and O(1) space
        return all(matrix[i][j] == matrix[i+1][j+1] for i in range(len(matrix)-1) for j in range(len(matrix[0])-1))

    def isToeplitzMatrix(self, matrix: List[List[int]]) -> bool:  # O(mn) time and O(m + n) space (for slicing)
        return all(row_i[:-1] == row_j[1:] for row_i, row_j in zip(matrix, matrix[1:]))


def test():
    arguments = [
        [[1, 2, 3, 4], [5, 1, 2, 3], [9, 5, 1, 2]],
        [[1, 2], [2, 2]],
        ]
    expectations = [True, False]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().isToeplitzMatrix(matrix)
        assert solution == expected
