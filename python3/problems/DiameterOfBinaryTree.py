### Source : https://leetcode.com/problems/diameter-of-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# 
# Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a 
# binary tree is the length of the longest path between any two nodes in a tree. This path may or may 
# not pass through the root.
# 
# Example:
# Given a binary tree 
# 
#           1
#          / \
#         2   3
#        / \     
#       4   5    
# 
# Return 3, which is the length of the path [4,2,1,3] or [5,2,1,3].
# 
# Note:
# The length of path between two nodes is represented by the number of edges between them.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def diameterOfBinaryTree(self, root: TreeNode) -> int:

        def dfs(node):
            nonlocal diameter
            if not node:
                return 0
            left, right = dfs(node.left), dfs(node.right)
            diameter = max(left + right, diameter)
            return 1 + max(left, right)

        diameter = 0
        dfs(root)
        return diameter

    def diameterOfBinaryTree(self, root: TreeNode) -> int:

        def dfs(node: TreeNode) -> tuple:  # return tuple diameter and height
            if not node:
                return 0, -1
            left, right = dfs(node.left), dfs(node.right)
            return max(left[0], right[0], left[1] + right[1] + 2), max(left[1], right[1]) + 1

        return dfs(root)[0]

    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        max_length = 0
        depth = {None: -1}
        stack = [(root, 0)]
        while stack:
            node, visited = stack.pop()
            if node is None:
                continue
            if visited == 0:
                stack.extend([(node, 1), (node.left, 0), (node.right, 0)])
            else:
                left_d = depth[node.left] + 1
                right_d = depth[node.right] + 1
                depth[node] = max(left_d, right_d)
                max_length = max(max_length, left_d + right_d)
        return max_length


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,5]",
        "[1,2]",
        "[4,-7,-3,null,null,-9,-3,9,-7,-4,null,6,null,-6,-6,null,null,0,6,5,null,9,null,null,-1,-4,null,null,null,-2]",
    ]
    expectations = [3, 1, 8]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().diameterOfBinaryTree(root)
        assert solution == expected
