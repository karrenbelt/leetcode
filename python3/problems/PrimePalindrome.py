### Source : https://leetcode.com/problems/prime-palindrome/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# Given an integer n, return the smallest prime palindrome greater than or equal to n.
# 
# An integer is prime if it has exactly two divisors: 1 and itself. Note that 1 is not a prime number.
# 
# 	For example, 2, 3, 5, 7, 11, and 13 are all primes.
# 
# An integer is a palindrome if it reads the same from left to right as it does from right to left.
# 
# 	For example, 101 and 12321 are palindromes.
# 
# The test cases are generated so that the answer always exists and is in the range [2, 2 * 108].
# 
# Example 1:
# Input: n = 6
# Output: 7
# Example 2:
# Input: n = 8
# Output: 11
# Example 3:
# Input: n = 13
# Output: 101
# 
# Constraints:
# 
# 	1 <= n <= 108
#####################################################################################################


def palindromes(path: str = '') -> int:
    if not path:
        for i in map(str, range(10)):
            yield from palindromes(i)
            yield from palindromes(i + i)
    else:
        value = int(path)
        if len(path) < 10 and value < 2 * 10 ** 8:
            if not path.startswith('0'):
                yield value
            for i in map(str, range(10)):
                yield from palindromes(i + path + i)


def is_prime(n: int) -> bool:  # O(sqrt n)
    if n == 1:
        return False
    i = 2
    while i * i <= n:
        if n % i == 0:
            return False
        i += 1
    return True


def is_prime(num: int) -> bool:  # sqrt(n)
    if num < 2 or num % 2 == 0:
        return num == 2
    for i in range(3, int(num ** 0.5) + 1, 2):
        if num % i == 0:
            return False
    return True


def is_prime(n: int) -> bool:  # even faster
    if n < 2:
        return False
    if n in [2, 3]:
        return True
    if n % 6 != 5 and n % 6 != 1:
        return False
    for i in range(5, int(n ** 0.5) + 1, 6):
        if not n % i or not n % (i + 2):
            return False
    return True


class Solution:
    prime_palindromes = sorted(filter(is_prime, palindromes()))
    def primePalindrome(self, n: int) -> int:
        return next(prime for prime in self.prime_palindromes if prime >= n)

    def primePalindrome(self, n: int) -> int:
        # all even length palindromes are devisable by 11
        if 8 <= n <= 11:
            return 11

        for x in range(10**(len(str(n))//2), 10**5):
            y = int(str(x) + str(x)[-2::-1])
            if y >= n and is_prime(y):
                return y


def test():
    arguments = [6, 8, 13, 2, 9989900]
    expectations = [7, 11, 101, 2, 100030001]
    for n, expected in zip(arguments, expectations):
        solution = Solution().primePalindrome(n)
        assert solution == expected
