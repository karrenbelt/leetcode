### Source : https://leetcode.com/problems/split-array-into-fibonacci-sequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-01

##################################################################################################### 
#
# You are given a string of digits num, such as "123456579". We can split it into a Fibonacci-like 
# sequence [123, 456, 579].
# 
# Formally, a Fibonacci-like sequence is a list f of non-negative integers such that:
# 
# 	0 <= f[i] < 231, (that is, each integer fits in a 32-bit signed integer type),
# 	f.length >= 3, and
# 	f[i] + f[i + 1] == f[i + 2] for all 0 <= i < f.length - 2.
# 
# Note that when splitting the string into pieces, each piece must not have extra leading zeroes, 
# except if the piece is the number 0 itself.
# 
# Return any Fibonacci-like sequence split from num, or return [] if it cannot be done.
# 
# Example 1:
# 
# Input: num = "123456579"
# Output: [123,456,579]
# 
# Example 2:
# 
# Input: num = "11235813"
# Output: [1,1,2,3,5,8,13]
# 
# Example 3:
# 
# Input: num = "112358130"
# Output: []
# Explanation: The task is impossible.
# 
# Example 4:
# 
# Input: num = "0123"
# Output: []
# Explanation: Leading zeroes are not allowed, so "01", "2", "3" is not valid.
# 
# Example 5:
# 
# Input: num = "1101111"
# Output: [11,0,11,11]
# Explanation: The output [11, 0, 11, 11] would also be accepted.
#
# Constraints:
#
# 	1 <= num.length <= 200
# 	num contains only digits.
#####################################################################################################

from typing import List


class Solution:
    # problem is the same as 306: AdditiveNumber.py
    def splitIntoFibonacci(self, num: str) -> List[int]:  # O(n) time and O(n) space
        # 1. find the first two putative numbers of the sequence
        # 2. perform an iterative dfs

        def is_valid(x: str) -> bool:
            return x and str(int(x)) == x and int(x) < 2 ** 31

        queue = []  # why O(n) time? because the maximum size of this queue is 100 for num = '1' * 200, and never grows
        for i in range(1, len(num) // 2 + 1):
            if is_valid(num[:i]):
                for j in range(i, i + len(num) // 2):
                    if is_valid(num[i:j]):
                        queue.append(([int(num[:i]), int(num[i:j])], j))

        while queue:  # iterative backtracking
            path, i = queue.pop()
            if i == len(num):
                return path
            j = i + 1
            while j <= len(num) and is_valid(num[i:j]):
                n = int(num[i:j])
                if path[-2] + path[-1] == n:
                    queue.append((path + [n], j))
                j += 1
        return []

    def splitIntoFibonacci(self, num: str) -> List[int]:  # O(n) time and O(n) space
        # backtracking with early pruning
        def backtracking(i: int) -> bool:
            if i == len(num) and len(path) >= 3:
                return True
            for j in range(i + 1, len(num) + 1):
                n = int(num[i:j])
                if j - i > 1 and num[i] == '0' or n > 2 ** 31 - 1:
                    return False
                elif len(path) >= 2 and (n > path[-2] + path[-1]):
                    return False
                elif len(path) <= 1 or path[-2] + path[-1] == n:
                    path.append(n)
                    if backtracking(j):
                        return True
                    path.pop()
            return False

        return path if (path := []) or backtracking(0) else path


def test():
    def is_valid_solution(num: str, path) -> bool:
        fit_32bit = all(n < 2 ** 31 for n in path)
        fib_like = all(path[i] + path[i + 1] == path[i + 2] for i in range(len(path) - 2))
        complete = ''.join(map(str, solution)) == num
        return len(path) >= 3 and fit_32bit and fib_like and complete

    arguments = [
        "123456579",
        "11235813",
        "112358130",
        "0123",
        "1011",
        "1101111",
        ("36115373839853435918344412703521047933751454799388550714335002319007375250760715149824021158955352"
         "57195564161509167334647108949738176284385285234123461518508746752631120827113919550237703163294909"),
    ]
    expectations = [
        [[123, 456, 579]],
        [[1, 1, 2, 3, 5, 8, 13]],
        [[]],
        [[]],
        [[1, 0, 1, 1]],
        [[110, 1, 111], [11, 0, 11, 11]],
        [[]],
    ]
    for num, expected in zip(arguments, expectations):
        solution = Solution().splitIntoFibonacci(num)
        assert tuple(solution) in set(map(tuple, expected)), (solution, expected)
