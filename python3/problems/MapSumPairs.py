### Source : https://leetcode.com/problems/map-sum-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Implement the apSum class:
# 
# 	apSum() Initializes the apSum object.
# 	void insert(String key, int val) Inserts the key-val pair into the map. If the key already 
# existed, the original key-value pair will be overridden to the new one.
# 	int sum(string prefix) Returns the sum of all the pairs' value whose key starts with the 
# prefix.
# 
# Example 1:
# 
# Input
# ["apSum", "insert", "sum", "insert", "sum"]
# [[], ["apple", 3], ["ap"], ["app", 2], ["ap"]]
# Output
# [null, null, 3, null, 5]
# 
# Explanation
# apSum mapSum = new apSum();
# mapSum.insert("apple", 3);  
# mapSum.sum("ap");           // return 3 (apple = 3)
# mapSum.insert("app", 2);    
# mapSum.sum("ap");           // return 5 (apple + app = 3 + 2 = 5)
# 
# Constraints:
# 
# 	1 <= key.length, prefix.length <= 50
# 	key and prefix consist of only lowercase English letters.
# 	1 <= val <= 1000
# 	At most 50 calls will be made to insert and sum.
#####################################################################################################


class MapSum:
    def __init__(self):
        self.trie = {}

    def insert(self, key: str, val: int) -> None:
        node = self.trie
        for c in key:
            if c not in node:
                node[c] = {}
            node = node[c]
        node['.'] = val

    def sum(self, prefix: str) -> int:  # bfs

        node = self.trie
        for c in prefix:
            if c not in node:
                return 0
            node = node[c]

        total = 0
        stack = [node]
        while stack:
            node = stack.pop()
            if isinstance(node, int):
                total += node
            else:
                stack.extend(node.values())
        return total

    def sum(self, prefix: str) -> int:  # dfs

        def dfs(node: dict) -> int:
            return node if isinstance(node, int) else sum(map(dfs, node.values()))

        return dfs(node) if (node := self.trie) and all(node := node.get(c, {}) for c in prefix) else 0


class MapSum:
    def __init__(self):
        self.d = {}

    def insert(self, key: str, val: int) -> None:
        self.d[key] = val

    def sum(self, prefix: str) -> int:
        return sum(self.d[i] for i in self.d if i.startswith(prefix))

# Your MapSum object will be instantiated and called as such:
# obj = MapSum()
# obj.insert(key,val)
# param_2 = obj.sum(prefix)


def test():
    operations = ["MapSum", "insert", "sum", "insert", "sum"]
    arguments = [[], ["apple", 3], ["ap"], ["app", 2], ["ap"]]
    expectations = [None, None, 3, None, 5]
    obj = MapSum(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
