### Source : https://leetcode.com/problems/symmetric-tree/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
# 
# For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
# 
#     1
#    / \
#   2   2
#  / \ / \
# 3  4 4  3
# 
# But the following [1,2,2,null,3,null,3]  is not:
# 
#     1
#    / \
#   2   2
#    \   \
#    3    3
# 
# Note:
# Bonus points if you could solve it both recursively and iteratively.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:

        def traverse(left: TreeNode, right: TreeNode) -> bool:
            if left and right and left.val == right.val:
                return traverse(left.left, right.right) & traverse(left.right, right.left)
            return left is right

        return traverse(root.left, root.right) if root else True

    def isSymmetric(self, root: TreeNode) -> bool:
        stack = [root]
        while stack:
            values = [node.val if node else None for node in stack]
            if values != values[::-1]:
                return False
            stack = [child for node in stack if node for child in (node.left, node.right)]
        return True


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,2,2,3,4,4,3]",
        "[1,2,2,null,3,null,3]",
        ]
    expectations = [True, False]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().isSymmetric(root)
        assert solution == expected
