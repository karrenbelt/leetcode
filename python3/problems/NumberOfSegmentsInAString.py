### Source : https://leetcode.com/problems/number-of-segments-in-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-01

##################################################################################################### 
#
# You are given a string s, return the number of segments in the string. 
# 
# A segment is defined to be a contiguous sequence of non-space characters.
# 
# Example 1:
# 
# Input: s = "Hello, my name is John"
# Output: 5
# Explanation: The five segments are ["Hello,", "my", "name", "is", "John"]
# 
# Example 2:
# 
# Input: s = "Hello"
# Output: 1
# 
# Example 3:
# 
# Input: s = "love live! mu'sic forever"
# Output: 4
# 
# Example 4:
# 
# Input: s = ""
# Output: 0
# 
# Constraints:
# 
# 	0 <= s.length <= 300
# 	s consists of lower-case and upper-case English letters, digits or one of the following 
# characters "!@#$%&*()_+-=',.:".
# 	The only space character in s is ' '.
#####################################################################################################


class Solution:
    def countSegments(self, s: str) -> int:  # multiple white space is an issue, don't want to use regex
        s = s.strip(' ')
        if not s:
            return 0
        ctr = 1
        last_c = ''
        for c in s:
            if c == ' ' and not last_c == ' ':
                ctr += 1
            last_c = c
        return ctr

    def countSegments(self, s: str) -> int:  # split also on \n, \r, \t, but there are no slashes
        return len(s.split())


def test():
    arguments = [
        "Hello, my name is John",
        "Hello",
        "love live! mu'sic forever",
        "",
        "Of all the gin joints in all the towns in all the world,   "
        ", , , ,        a, eaefa",
        ]
    expectations = [5, 1, 4, 0, 19, 6]
    for s, expected in zip(arguments, expectations):
        solution = Solution().countSegments(s)
        assert solution == expected
