### Source : https://leetcode.com/problems/binary-tree-postorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree, return the postorder traversal of its nodes' values.
# 
# Example:
# 
# Input: [1,null,2,3]
#    1
#     \
#      2
#     /
#    3
# 
# Output: [3,2,1]
# 
# Follow up: Recursive solution is trivial, could you do it iteratively?
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def postorderTraversal(self, root: TreeNode) -> List[int]:

        def traverse(node: TreeNode):
            if node:
                traverse(node.left)
                traverse(node.right)
                order.append(node.val)

        order = []
        traverse(root)
        return order

    def postorderTraversal(self, root: TreeNode) -> List[int]:
        # reverse pre-order traversal, then reverse
        order = []
        stack = [root]
        while stack:
            node = stack.pop()
            if node:
                order.append(node.val)
                stack.append(node.left)
                stack.append(node.right)
        return order[::-1]

    def postorderTraversal(self, root):  # Morris traversal: O(n) time and O(1) space

        def reverseOrder(left, right):
            while left < right:
                order[left], order[right] = order[right], order[left]
                left += 1
                right -= 1

        order = []
        node = TreeNode(None)  # dummy node
        node.left = root
        while node:
            if not node.left:
                node = node.right
            else:
                pre = node.left
                while pre.right and pre.right != node:
                    pre = pre.right
                if not pre.right:
                    pre.right = node
                    node = node.left
                else:
                    pre = node.left
                    count = 1
                    while pre.right and pre.right != node:
                        order.append(pre.val)
                        pre = pre.right
                        count += 1
                    order.append(pre.val)
                    pre.right = None
                    reverseOrder(len(order) - count, len(order) - 1)
                    node = node.right
        return order


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,null,2,3]",
        "[]",
        "[1]",
        ]
    expectations = [
        [3, 2, 1],
        [],
        [1],
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().postorderTraversal(root)
        assert solution == expected
