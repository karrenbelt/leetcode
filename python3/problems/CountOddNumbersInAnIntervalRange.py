### Source : https://leetcode.com/problems/count-odd-numbers-in-an-interval-range/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-13

##################################################################################################### 
#
# Given two non-negative integers low and high. Return the count of odd numbers between low and high 
# (inclusive).
# 
# Example 1:
# 
# Input: low = 3, high = 7
# Output: 3
# Explanation: The odd numbers between 3 and 7 are [3,5,7].
# 
# Example 2:
# 
# Input: low = 8, high = 10
# Output: 1
# Explanation: The odd numbers between 8 and 10 are [9].
# 
# Constraints:
# 
# 	0 <= low <= high <= 109
#####################################################################################################


class Solution:
    def countOdds(self, low: int, high: int) -> int:  # O(1) time & space
        return (high - low) // 2 + (low % 2 or high % 2)


def test():
    arguments = [
        (3, 7),
        (8, 10),
        (8, 11),
        (8, 8),
        (9, 9),
    ]
    expectations = [3, 1, 2, 0, 1]
    for (low, high), expected in zip(arguments, expectations):
        solution = Solution().countOdds(low, high)
        assert solution == expected
