### Source : https://leetcode.com/problems/four-divisors/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-28

##################################################################################################### 
#
# Given an integer array nums, return the sum of divisors of the integers in that array that have 
# exactly four divisors. If there is no such integer in the array, return 0.
# 
# Example 1:
# 
# Input: nums = [21,4,7]
# Output: 32
# Explanation: 
# 21 has 4 divisors: 1, 3, 7, 21
# 4 has 3 divisors: 1, 2, 4
# 7 has 2 divisors: 1, 7
# The answer is the sum of divisors of 21 only.
# 
# Example 2:
# 
# Input: nums = [21,21]
# Output: 64
# 
# Example 3:
# 
# Input: nums = [1,2,3,4,5]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	1 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def sumFourDivisors(self, nums: List[int]) -> int:  # O(n * sqrt(m)) time

        sum_of_divisors = 0
        for n in nums:
            divisors = set()
            for divisor in range(1, int(n ** 0.5) + 1):  # "\U0001F914"
                if n % divisor == 0:
                    divisors.add(divisor)
                    divisors.add(n // divisor)
                if len(divisors) > 4:
                    break
            if len(divisors) == 4:
                sum_of_divisors += sum(divisors)

        return sum_of_divisors

    def sumFourDivisors(self, nums: List[int]) -> int:  # O(n + m + k^2) time

        def sieve_of_eratosthenes():
            is_prime = [True] * (max_n + 1)
            p = 2
            while p * p <= max_n:
                if is_prime[p]:
                    i = p + p
                    while i <= max_n:
                        is_prime[i] = False
                        i += p
                p += 1
            return [p for p in range(2, max_n + 1) if is_prime[p]]

        def update_four_distinct_factors_sums():

            primes = sieve_of_eratosthenes()

            for i, p in enumerate(primes):

                # case a: 1, p, p**2, p**3
                case1 = p**3
                if case1 <= max_n:
                    four_div[case1] = True;
                    sums[case1] = 1 + p + p**2 + case1

                # case b: 1, p, q, p * q
                for j in range(i + 1, len(primes)):
                    q = primes[j]
                    case2 = p * q
                    if case2 > max_n:
                        break
                    four_div[case2] = True
                    sums[case2] = 1 + p + q + case2

        max_n = max(nums)
        four_div = [False] * (max_n + 1)
        sums = [0] * (max_n + 1)

        update_four_distinct_factors_sums()
        return sum(sums[n] for n in nums if four_div[n])


def test():
    arguments = [
        [21, 4, 7],
        [21, 21],
        [1, 2, 3, 4, 5],
    ]
    expectations = [32, 64, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().sumFourDivisors(nums)
        assert solution == expected
test()