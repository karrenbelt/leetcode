### Source : https://leetcode.com/problems/burst-balloons/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-29

##################################################################################################### 
#
# You are given n balloons, indexed from 0 to n - 1. Each balloon is painted with a number on it 
# represented by an array nums. You are asked to burst all the balloons.
# 
# If you burst the ith balloon, you will get nums[i - 1] * nums[i] * nums[i + 1] coins. If i - 1 or i 
# + 1 goes out of bounds of the array, then treat it as if there is a balloon with a 1 painted on it.
# 
# Return the maximum coins you can collect by bursting the balloons wisely.
# 
# Example 1:
# 
# Input: nums = [3,1,5,8]
# Output: 167
# Explanation:
# nums = [3,1,5,8] --> [3,5,8] --> [3,8] --> [8] --> []
# coins =  3*1*5    +   3*5*8   +  1*3*8  + 1*8*1 = 167
# 
# Example 2:
# 
# Input: nums = [1,5]
# Output: 10
# 
# Constraints:
# 
# 	n == nums.length
# 	1 <= n <= 500
# 	0 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def maxCoins(self, nums):  # O(n^3) time and O(n^2) space
        from functools import lru_cache
        nums = [1] + nums + [1]

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> int:
            return max([nums[i] * nums[k] * nums[j] + dfs(i, k) + dfs(k, j) for k in range(i + 1, j)], default=0)

        return dfs(0, len(nums) - 1)

    def maxCoins(self, nums: List[int]) -> int:  # O(n^3) time and O(n^2) space
        nums = [1] + nums + [1]
        dp = [[0] * (len(nums)) for _ in range(len(nums))]

        for gap in range(2, len(nums)):
            for i in range(len(nums) - gap):
                j = i + gap
                for k in range(i + 1, j):
                    dp[i][j] = max(dp[i][j], nums[i] * nums[k] * nums[j] + dp[i][k] + dp[k][j])

        return dp[0][-1]

    def maxCoins(self, nums):  # O(n^3) time and O(n^2) space
        nums = [1] + nums + [1]
        dp = [[0] * len(nums) for _ in range(len(nums))]
        for i in range(len(nums) - 2, -1, -1):
            for j in range(i + 2, len(nums)):
                dp[i][j] = max(nums[i] * nums[k] * nums[j] + dp[i][k] + dp[k][j] for k in range(i + 1, j))
        return dp[0][len(nums) - 1]


def test():
    arguments = [
        [3, 1, 5, 8],
        [1, 5],
    ]
    expectations = [167, 10]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maxCoins(nums)
        assert solution == expected
