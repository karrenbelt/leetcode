### Source : https://leetcode.com/problems/unique-paths-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-02

##################################################################################################### 
#
# On a 2-dimensional grid, there are 4 types of squares:
# 
# 	1 represents the starting square.  There is exactly one starting square.
# 	2 represents the ending square.  There is exactly one ending square.
# 	0 represents empty squares we can walk over.
# 	-1 represents obstacles that we cannot walk over.
# 
# Return the number of 4-directional walks from the starting square to the ending square, that walk 
# over every non-obstacle square exactly once.
# 
# Example 1:
# 
# Input: [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
# Output: 2
# Explanation: We have the following two paths: 
# 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
# 2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)
# 
# Example 2:
# 
# Input: [[1,0,0,0],[0,0,0,0],[0,0,0,2]]
# Output: 4
# Explanation: We have the following four paths: 
# 1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
# 2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
# 3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
# 4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
# 
# Example 3:
# 
# Input: [[0,1],[2,0]]
# Output: 0
# Explanation: 
# There is no path that walks over every empty square exactly once.
# Note that the starting and ending square can be anywhere in the grid.
# 
# Note:
# 
# 	1 <= grid.length * grid[0].length <= 20
#####################################################################################################

from typing import List


class Solution:
    def uniquePathsIII(self, grid: List[List[int]]) -> int:  # O(3^n) time: 72 ms

        def backtracking(i: int, j: int) -> int:
            if not 0 <= i < len(grid) or not 0 <= j < len(grid[0]) or grid[i][j] == -1 or (i, j) in seen:
                return 0
            elif grid[i][j] == 2:
                return len(seen) == number_of_zeroes + 1  # we visit the start (1) as well
            seen.add((i, j))
            number_of_walks = sum(backtracking(x, y) for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)])
            seen.remove((i, j))
            return number_of_walks

        number_of_zeroes, seen = sum(cell == 0 for row in grid for cell in row), set()
        return backtracking(*next((i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == 1))

    def uniquePathsIII(self, grid: List[List[int]]) -> int:  # O(3^n) time: 64 ms
        number_of_zeroes, number_of_walks = sum(cell == 0 for row in grid for cell in row), 0
        start = next((i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == 1)
        queue = [(*start, set())]
        while queue:
            i, j, path = queue.pop()
            if grid[i][j] == 2:
                number_of_walks += len(path) == number_of_zeroes + 1
                continue  # speed-up
            for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and grid[x][y] != -1 and (x, y) not in path:
                    queue.append((x, y, path | {(i, j)}))
        return number_of_walks


def test():
    arguments = [
        [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 2, -1]],
        [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]],
        [[0, 1], [2, 0]],
    ]
    expectations = [2, 4, 0]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().uniquePathsIII(grid)
        assert solution == expected
