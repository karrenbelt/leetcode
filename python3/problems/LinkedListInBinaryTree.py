### Source : https://leetcode.com/problems/linked-list-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-08

##################################################################################################### 
#
# Given a binary tree root and a linked list with head as the first node. 
# 
# Return True if all the elements in the linked list starting from the head correspond to some 
# downward path connected in the binary tree otherwise return False.
# 
# In this context downward path means a path that starts at some node and goes downwards.
# 
# Example 1:
# 
# Input: head = [4,2,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
# Output: true
# Explanation: Nodes in blue form a subpath in the binary Tree.  
# 
# Example 2:
# 
# Input: head = [1,4,2,6], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
# Output: true
# 
# Example 3:
# 
# Input: head = [1,4,2,6,8], root = [1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]
# Output: false
# Explanation: There is no path in the binary tree that contains all the elements of the linked list 
# from head.
# 
# Constraints:
# 
# 	The number of nodes in the tree will be in the range [1, 2500].
# 	The number of nodes in the list will be in the range [1, 100].
# 	1 <= Node.val <= 100 for each node in the linked list and binary tree.
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode
from python3 import BinaryTreeNode as TreeNode


# https://leetcode.com/problems/implement-strstr/discuss/589972/Implement-of-KMP-with-python-(in-line-explanation)
class Solution:  # LinkedList, BinaryTree, DFS, BFS, Pre-order traversal
    # possible approaches: DFS + BFS, DFS + DFS, BFS + BFS or DP
    def isSubPath(self, head: ListNode, root: TreeNode) -> bool:  # dfs + dfs: O(n * h) time
        from functools import lru_cache

        # @lru_cache(maxsize=None)  # TODO: make SinglyLinkedListNode hashable
        def dfs(l_node: ListNode, t_node: TreeNode):
            if not l_node:
                return True
            if not t_node:
                return False
            return dfs(head, t_node.left) or dfs(head, t_node.right) or \
                   t_node.val == l_node.val and (dfs(l_node.next, t_node.left) or dfs(l_node.next, t_node.right))

        return dfs(head, root)

    def isSubPath(self, head: ListNode, root: TreeNode) -> bool:  # dfs + bfs
        from collections import deque

        def dfs(l_node: ListNode, t_node: TreeNode):
            if not t_node:
                return False
            if l_node.val == t_node.val and bfs(l_node, t_node):
                return True
            return dfs(l_node, t_node.left) or dfs(l_node, t_node.right)

        def bfs(head: ListNode, t_node: TreeNode):
            queue = deque([t_node])
            curr = head.next
            while queue and curr:
                size = len(queue)
                for i in range(size):
                    node = queue.popleft()
                    if node.left and node.left.val == curr.val:
                        queue.append(node.left)
                    if node.right and node.right.val == curr.val:
                        queue.append(node.right)
                if queue:
                    curr = curr.next
            return curr is None  # found it

        return dfs(head, root)

    def isSubPath(self, head: ListNode, root: TreeNode) -> bool:  # dfs + dfs - fastest

        def dfs(l_node: ListNode, t_node: TreeNode):
            if not t_node:
                return False
            if matches(l_node, t_node):
                return True
            return dfs(l_node, t_node.left) or dfs(l_node, t_node.right)

        def matches(l_node: ListNode, t_node: TreeNode):
            if not l_node:
                return True
            if not t_node or t_node.val != l_node.val:
                return False
            return matches(l_node.next, t_node.left) or matches(l_node.next, t_node.right)

        return dfs(head, root)


def test():
    from python3 import SinglyLinkedList, Codec
    arrays_of_numbers = [
        [4, 2, 8],
        [1, 4, 2, 6],
        [1, 4, 2, 6, 8],
        [1, 2, 1, 2, 3],  # the new matching path starts somewhere in the already matching path
        ]
    serialized_trees = [
        "[1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]",
        "[1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]",
        "[1,4,4,null,2,2,null,1,null,6,8,null,null,null,null,1,3]",
        "[1,null,2,null,1,2,null,1,null,null,2,null,3]",
        ]
    expectations = [True, True, False]
    for nums, serialized_tree, expected in zip(arrays_of_numbers, serialized_trees, expectations):
        head = SinglyLinkedList(nums).head
        root = Codec.deserialize(serialized_tree)
        solution = Solution().isSubPath(head, root)
        assert solution == expected
