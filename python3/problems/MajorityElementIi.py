### Source : https://leetcode.com/problems/majority-element-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given an integer array of size n, find all elements that appear more than &lfloor; n/3 &rfloor; 
# times.
# 
# Follow-up: Could you solve the problem in linear time and in O(1) space?
# 
# Example 1:
# 
# Input: nums = [3,2,3]
# Output: [3]
# 
# Example 2:
# 
# Input: nums = [1]
# Output: [1]
# 
# Example 3:
# 
# Input: nums = [1,2]
# Output: [1,2]
# 
# Constraints:
# 
# 	1 <= nums.length <= 5 * 104
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def majorityElement(self, nums: List[int]) -> List[int]:
        counts = dict.fromkeys(nums, 0)
        for n in nums:
            counts[n] += 1
        return [n for n, count in counts.items() if count > len(nums) // 3]

    def majorityElement(self, nums):  # O(n) time and O(1) space
        # Boyer-Moore Majority Voting Algorithm
        # since n // 3, we can at most have 2 such elements
        if not nums:
            return []
        count1, count2, candidate1, candidate2 = 0, 0, 0, 1
        for n in nums:
            if n == candidate1:
                count1 += 1
            elif n == candidate2:
                count2 += 1
            elif count1 == 0:
                candidate1, count1 = n, 1
            elif count2 == 0:
                candidate2, count2 = n, 1
            else:
                count1, count2 = count1 - 1, count2 - 1
        return [n for n in (candidate1, candidate2) if nums.count(n) > len(nums) // 3]

    def majorityElement(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        import collections
        ctr = collections.Counter()
        for n in nums:
            ctr[n] += 1
            if len(ctr) == 3:  # one too many, remove an element from the counter for O(1) space complexity
                ctr -= collections.Counter(set(ctr))
        return [n for n in ctr if nums.count(n) > len(nums) // 3]


def test():
    arguments = [
        [3, 2, 3],
        [1],
        [1, 2],
    ]
    expectations = [
        [3],
        [1],
        [1, 2],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().majorityElement(nums)
        assert solution == expected
