### Source : https://leetcode.com/problems/candy/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-27

##################################################################################################### 
#
# There are n children standing in a line. Each child is assigned a rating value given in the integer 
# array ratings.
# 
# You are giving candies to these children subjected to the following requirements:
# 
# 	Each child must have at least one candy.
# 	Children with a higher rating get more candies than their neighbors.
# 
# Return the minimum number of candies you need to have to distribute the candies to the children.
# 
# Example 1:
# 
# Input: ratings = [1,0,2]
# Output: 5
# Explanation: You can allocate to the first, second and third child with 2, 1, 2 candies 
# respectively.
# 
# Example 2:
# 
# Input: ratings = [1,2,2]
# Output: 4
# Explanation: You can allocate to the first, second and third child with 1, 2, 1 candies 
# respectively.
# The third child gets 1 candy because it satisfies the above two conditions.
# 
# Constraints:
# 
# 	n == ratings.length
# 	1 <= n <= 2 * 104
# 	0 <= ratings[i] <= 2 * 104
#####################################################################################################

from typing import List


class Solution:
    def candy(self, ratings: List[int]) -> int:  # O(n log n) time and O(n) space
        # we build up from the local minima
        candies = [1] * len(ratings)
        sorted_ratings = sorted((rating, i) for i, rating in enumerate(ratings))[::-1]
        while sorted_ratings:
            rating, i = sorted_ratings.pop()
            left = candies[i - 1] + 1 if i > 0 and ratings[i - 1] < ratings[i] else 1
            right = candies[i + 1] + 1 if i < len(ratings) - 1 and ratings[i + 1] < ratings[i] else 1
            candies[i] = max(left, right)
        return sum(candies)

    def candy(self, ratings: List[int]) -> int:  # O(n) time and O(n) space
        # two-scan approach similar to question 42. TrappingRainwater.py
        candies = [1] * len(ratings)
        left = right = 1
        for i in range(1, len(ratings)):
            left = left + 1 if ratings[i - 1] < ratings[i] else 1
            candies[i] = left
        for i in range(len(ratings) - 2, -1, -1):
            right = right + 1 if ratings[i + 1] < ratings[i] else 1
            candies[i] = max(candies[i], right)
        return sum(candies)


def test():
    arguments = [
        [1, 0, 2],
        [1, 2, 2],
        [1, 2, 87, 87, 87, 2, 1],
    ]
    expectations = [5, 4, 13]
    for ratings, expected in zip(arguments, expectations):
        solution = Solution().candy(ratings)
        assert solution == expected, (solution, expected)
test()