### Source : https://leetcode.com/problems/lexicographical-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-09

##################################################################################################### 
#
# Given an integer n, return all the numbers in the range [1, n] sorted in lexicographical order.
# 
# Example 1:
# Input: n = 13
# Output: [1,10,11,12,13,2,3,4,5,6,7,8,9]
# Example 2:
# Input: n = 2
# Output: [1,2]
# 
# Constraints:
# 
# 	1 <= n <= 5 * 104
# 
# Follow up: Could you optimize your solution to use O(n) runtime and O(1) space?
#####################################################################################################

from typing import List


class Solution:
    def lexicalOrder(self, n: int) -> List[int]:  # O(n) time and O(n) space
        return sorted(range(1, n + 1), key=str)

    def lexicalOrder(self, n: int) -> List[int]:  # O(n) time and O(n) space
        ans = [1]
        while len(ans) < n:
            new = ans[-1] * 10
            while new > n:
                new //= 10
                new += 1
                while new % 10 == 0:
                    new //= 10
            ans.append(new)
        return ans


def test():
    arguments = [13, 2]
    expectations = [
        [1, 10, 11, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2],
        ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().lexicalOrder(n)
        assert solution == expected
