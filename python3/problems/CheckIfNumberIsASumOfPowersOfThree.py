### Source : https://leetcode.com/problems/check-if-number-is-a-sum-of-powers-of-three/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

#####################################################################################################
#
# Given an integer n, return true if it is possible to represent n as the sum of distinct powers of
# three. Otherwise, return false.
#
# An integer y is a power of three if there exists an integer x such that y == 3x.
#
# Example 1:
#
# Input: n = 12
# Output: true
# Explanation: 12 = 31 + 32
#
# Example 2:
#
# Input: n = 91
# Output: true
# Explanation: 91 = 30 + 32 + 34
#
# Example 3:
#
# Input: n = 21
# Output: false
#
# Constraints:
#
# 	1 <= n <= 107
#####################################################################################################


class Solution:
    def checkPowersOfThree(self, n: int) -> bool:

        def backtrack(i: int, num: int):
            if num == 0:
                return True
            return any(backtrack(j + 1, num - powers[j]) for j in range(i, len(powers)) if num - powers[j] >= 0)

        powers = [3 ** i for i in range(15)]
        return backtrack(0, n)

    def checkPowersOfThree(self, n: int) -> bool:  # 1196 ms

        def backtrack(i: int, num: int):
            return num == 0 or any(backtrack(j + 1, num - 3 ** j) for j in range(i, 15) if num - 3 ** j >= 0)

        return backtrack(0, n)

    def checkPowersOfThree(self, n: int) -> bool:  # 748 ms

        def backtrack(i: int, num: int):
            return num == 0 or any(backtrack(j - 1, num - 3 ** j) for j in range(i, -1, -1) if num - 3 ** j >= 0)

        return backtrack(15, n)

    def checkPowersOfThree(self, n: int) -> bool:  # 36 ms
        while n > 1:
            n, r = divmod(n, 3)
            if r == 2:
                return False
        return True


def test():
    arguments = [12, 91, 21, 6378022]
    expectations = [True, True, False, True]
    for n, expected in zip(arguments, expectations):
        solution = Solution().checkPowersOfThree(n)
        assert solution == expected
