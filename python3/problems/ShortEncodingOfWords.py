### Source : https://leetcode.com/problems/short-encoding-of-words/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# A valid encoding of an array of words is any reference string s and array of indices indices such 
# that:
# 
# 	words.length == indices.length
# 	The reference string s ends with the '#' character.
# 	For each index indices[i], the substring of s starting from indices[i] and up to (but not 
# including) the next '#' character is equal to words[i].
# 
# Given an array of words, return the length of the shortest reference string s possible of any valid 
# encoding of words.
# 
# Example 1:
# 
# Input: words = ["time", "me", "bell"]
# Output: 10
# Explanation: A valid encoding would be s = "time#bell#" and indices = [0, 2, 5].
# words[0] = "time", the substring of s starting from indices[0] = 0 to the next '#' is underlined in 
# "time#bell#"
# words[1] = "me", the substring of s starting from indices[1] = 2 to the next '#' is underlined in 
# "time#bell#"
# words[2] = "bell", the substring of s starting from indices[2] = 5 to the next '#' is underlined in 
# "time#bell#"
# 
# Example 2:
# 
# Input: words = ["t"]
# Output: 2
# Explanation: A valid encoding would be s = "t#" and indices = [0].
# 
# Constraints:
# 
# 	1 <= words.length <= 2000
# 	1 <= words[i].length <= 7
# 	words[i] consists of only lowercase letters.
#####################################################################################################

from typing import List


class Solution:
    def minimumLengthEncoding(self, words: List[str]) -> int:  # O(n log n) time and O(n) space
        # idea: use a trie
        # sort words by length
        # for each word
        # reverse it
        # see if you can match the word already as prefix (reverse suffix)
        # if yes, continue (no need to create the list of indices)
        # if no,
        #   insert word (with its index as end-of-word marker if you want to create the actual encoding; we don't)
        #   ctr += len(word) + 1  (no need to create the encoded string)
        encoding_length = 0
        words.sort(key=len, reverse=True)
        trie = {}
        for word in words:
            node, new_entry = trie, False
            for i in range(len(word) - 1, -1, -1):
                c = word[i]
                if c not in node:
                    new_entry = True
                    node[c] = dict()
                node = node[c]
            if new_entry:
                encoding_length += len(word) + 1
        return encoding_length

    def minimumLengthEncoding(self, words: List[str]) -> int:  # O(nk^2) time and O(nk) space
        s = set(words)
        for word in words:
            for i in range(1, len(word)):
                s.discard(word[i:])
        return sum(len(word) + 1 for word in s)

    def minimumLengthEncoding(self, words: List[str]) -> int:  # O(n log n) time and O(1) space
        ws = [w[::-1] for w in words]
        ws.sort()
        encoding_length = 0
        for i in range(len(ws)):
            if i + 1 != len(ws) and ws[i + 1].startswith(ws[i]):
                continue
            encoding_length += len(ws[i]) + 1
        return encoding_length

    def minimumLengthEncoding(self, words: List[str]) -> int:  # O(n)
        import collections
        import functools
        words = list(set(words))
        Trie = lambda: collections.defaultdict(Trie)
        trie = Trie()
        nodes = [functools.reduce(dict.__getitem__, word[::-1], trie) for word in words]
        return sum(len(word) + 1 for i, word in enumerate(words) if len(nodes[i]) == 0)  # no word encompassing it

    def minimumLengthEncoding(self, words: List[str]) -> int:
        words = sorted([word[::-1] for word in set(words)])
        encoding_length = len(words[-1]) + 1
        for i in range(len(words) - 1):
            word, next_word = words[i], words[i + 1]
            if not next_word.startswith(word):
                encoding_length += len(word) + 1
        return encoding_length


def test():
    arrays_of_strings = [
        ["time", "me", "bell"],
        ["t"],
        ]
    expectations = [10, 2]
    for words, expected in zip(arrays_of_strings, expectations):
        solution = Solution().minimumLengthEncoding(words)
        assert solution == expected
