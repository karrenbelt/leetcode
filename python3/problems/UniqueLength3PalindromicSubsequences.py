### Source : https://leetcode.com/problems/unique-length-3-palindromic-subsequences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-23

##################################################################################################### 
#
# Given a string s, return the number of unique palindromes of length three that are a subsequence of 
# s.
# 
# Note that even if there are multiple ways to obtain the same subsequence, it is still only counted 
# once.
# 
# A palindrome is a string that reads the same forwards and backwards.
# 
# A subsequence of a string is a new string generated from the original string with some characters 
# (can be none) deleted without changing the relative order of the remaining characters.
# 
# 	For example, "ace" is a subsequence of "abcde".
# 
# Example 1:
# 
# Input: s = "aabca"
# Output: 3
# Explanation: The 3 palindromic subsequences of length 3 are:
# - "aba" (subsequence of "aabca")
# - "aaa" (subsequence of "aabca")
# - "aca" (subsequence of "aabca")
# 
# Example 2:
# 
# Input: s = "adc"
# Output: 0
# Explanation: There are no palindromic subsequences of length 3 in "adc".
# 
# Example 3:
# 
# Input: s = "bbcbaba"
# Output: 4
# Explanation: The 4 palindromic subsequences of length 3 are:
# - "bbb" (subsequence of "bbcbaba")
# - "bcb" (subsequence of "bbcbaba")
# - "bab" (subsequence of "bbcbaba")
# - "aba" (subsequence of "bbcbaba")
# 
# Constraints:
# 
# 	3 <= s.length <= 105
# 	s consists of only lowercase English letters.
#####################################################################################################


class Solution:
    def countPalindromicSubsequence(self, s: str) -> int:  # O(n) time and O(1) space
        ctr = 0
        for c in set(s):
            i, j = s.find(c), s.rfind(c)
            if i != j:
                ctr += len(set(s[i + 1:j]))
        return ctr

    def countPalindromicSubsequence(self, s: str) -> int:  # O(n) time and O(1) space
        return sum(len(set(s[s.find(c) + 1: s.rfind(c)])) for c in set(s))


def test():
    arguments = [
        "aabca",
        "adc",
        "bbcbaba",
    ]
    expectations = [3, 0, 4]
    for s, expected in zip(arguments, expectations):
        solution = Solution().countPalindromicSubsequence(s)
        assert solution == expected
