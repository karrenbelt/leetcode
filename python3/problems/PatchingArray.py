### Source : https://leetcode.com/problems/patching-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given a sorted integer array nums and an integer n, add/patch elements to the array such that any 
# number in the range [1, n] inclusive can be formed by the sum of some elements in the array.
# 
# Return the minimum number of patches required.
# 
# Example 1:
# 
# Input: nums = [1,3], n = 6
# Output: 1
# Explanation:
# Combinations of nums are [1], [3], [1,3], which form possible sums of: 1, 3, 4.
# Now if we add/patch 2 to nums, the combinations are: [1], [2], [3], [1,3], [2,3], [1,2,3].
# Possible sums are 1, 2, 3, 4, 5, 6, which now covers the range [1, 6].
# So we only need 1 patch.
# 
# Example 2:
# 
# Input: nums = [1,5,10], n = 20
# Output: 2
# Explanation: The two patches can be [2, 4].
# 
# Example 3:
# 
# Input: nums = [1,2,2], n = 5
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	1 <= nums[i] <= 104
# 	nums is sorted in ascending order.
# 	1 <= n <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def minPatches(self, nums: List[int], n: int) -> int:
        missing = 1
        ctr = i = 0
        while missing <= n:  # we can build [0, missing)
            if i < len(nums) and nums[i] <= missing:  # we add it to the array to build [0, missing + num)
                missing += nums[i]
                i += 1
            else:  # otherwise we add missing itself to maximize the reach
                missing += missing
                ctr += 1
        return ctr


def test():
    arguments = [
        ([1, 3], 6),  # add 2
        ([1, 5, 10], 20),  # add 2, 4
        ([1, 2, 2], 5),
        ([1, 2, 4, 13, 43], 100),  # add 8, 29
        ]
    expectations = [1, 2, 0, 2]
    for (nums, n), expected in zip(arguments, expectations):
        solution = Solution().minPatches(nums, n)
        assert solution == expected
