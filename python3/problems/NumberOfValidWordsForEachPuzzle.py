### Source : https://leetcode.com/problems/number-of-valid-words-for-each-puzzle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-09

##################################################################################################### 
#
# With respect to a given puzzle string, a word is valid if both the following conditions are 
# satisfied:
# 
# 	word contains the first letter of puzzle.
# 	For each letter in word, that letter is in puzzle.
# 
# 		For example, if the puzzle is "abcdefg", then valid words are "faced", "cabbage", 
# and "baggage", while
# 		invalid words are "beefed" (does not include 'a') and "based" (includes 's' which 
# is not in the puzzle).
# 
# Return an array answer, where answer[i] is the number of words in the given word list words that is 
# valid with respect to the puzzle puzzles[i].
# 
# Example 1:
# 
# Input: words = ["aaaa","asas","able","ability","actt","actor","access"], puzzles = 
# ["aboveyz","abrodyz","abslute","absoryz","actresz","gaswxyz"]
# Output: [1,1,3,2,4,0]
# Explanation: 
# 1 valid word for "aboveyz" : "aaaa" 
# 1 valid word for "abrodyz" : "aaaa"
# 3 valid words for "abslute" : "aaaa", "asas", "able"
# 2 valid words for "absoryz" : "aaaa", "asas"
# 4 valid words for "actresz" : "aaaa", "asas", "actt", "access"
# There are no valid words for "gaswxyz" cause none of the words in the list contains letter 'g'.
# 
# Example 2:
# 
# Input: words = ["apple","pleas","please"], puzzles = 
# ["aelwxyz","aelpxyz","aelpsxy","saelpxy","xaelpsy"]
# Output: [0,1,3,2,0]
# 
# Constraints:
# 
# 	1 <= words.length <= 105
# 	4 <= words[i].length <= 50
# 	1 <= puzzles.length <= 104
# 	puzzles[i].length == 7
# 	words[i] and puzzles[i] consist of lowercase English letters.
# 	Each puzzles[i] does not contain repeated characters.
#####################################################################################################

from typing import List


class Solution:
    def findNumOfValidWords(self, words: List[str], puzzles: List[str]) -> List[int]:  # O(mns) time --> TLE
        answer, (puzzle_sets, word_sets) = [], (list(map(set, items)) for items in (puzzles, words))
        for puzzle, puzzle_set in zip(puzzles, puzzle_sets):
            ctr = 0
            for word_set in word_sets:
                if puzzle[0] in word_set:
                    ctr += all(letter in puzzle_set for letter in word_set)
            answer.append(ctr)
        return answer

    def findNumOfValidWords(self, words: List[str], puzzles: List[str]) -> List[int]:  # O(m + ns log s) time

        def dfs(node: dict, first_letter_found=False) -> int:
            ctr = node.get('.', 0) if first_letter_found else 0
            for c, next_node in node.items():
                if c in puzzle_set:
                    ctr += dfs(next_node, puzzle[0] == c or first_letter_found)
            return ctr

        def build_trie() -> dict:
            root = {}
            for word in words:
                node = root
                for c in set(word):
                    node = node.setdefault(c, {})
                node['.'] = node.get('.', 0) + 1
            return root

        trie, answer = build_trie(), []
        for puzzle, puzzle_set in zip(puzzles, map(set, puzzles)):
            answer.append(dfs(trie))
        return answer


def test():
    arguments = [
        (["aaaa", "asas", "able", "ability", "actt", "actor", "access"],
         ["aboveyz", "abrodyz", "abslute", "absoryz", "actresz", "gaswxyz"]),
        (["apple", "pleas", "please"],
         ["aelwxyz", "aelpxyz", "aelpsxy", "saelpxy", "xaelpsy"]),
    ]
    expectations = [
        [1, 1, 3, 2, 4, 0],
        [0, 1, 3, 2, 0],
    ]
    for (words, puzzles), expected in zip(arguments, expectations):
        solution = Solution().findNumOfValidWords(words, puzzles)
        assert solution == expected, (solution, expected)

