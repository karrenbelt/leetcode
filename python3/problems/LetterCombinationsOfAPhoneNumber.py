### Source : https://leetcode.com/problems/letter-combinations-of-a-phone-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-21

##################################################################################################### 
#
# Given a string containing digits from 2-9 inclusive, return all possible letter combinations that 
# the number could represent. Return the answer in any order.
# 
# A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does 
# not map to any letters.
# 
# Example 1:
# 
# Input: digits = "23"
# Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]
# 
# Example 2:
# 
# Input: digits = ""
# Output: []
# 
# Example 3:
# 
# Input: digits = "2"
# Output: ["a","b","c"]
# 
# Constraints:
# 
# 	0 <= digits.length <= 4
# 	digits[i] is a digit in the range ['2', '9'].
#####################################################################################################

from typing import List


class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        import itertools

        if not digits:
            return []

        mapping = {
            '2': {'a', 'b', 'c'},
            '3': {'d', 'e', 'f'},
            '4': {'g', 'h', 'i'},
            '5': {'j', 'k', 'l'},
            '6': {'m', 'n', 'o'},
            '7': {'p', 'q', 'r', 's'},
            '8': {'t', 'u', 'v'},
            '9': {'w', 'x', 'y', 'z'},
        }

        sets = [mapping[digit] for digit in digits]
        return [''.join(c) for c in itertools.product(*sets)]

    def letterCombinations(self, digits: str) -> List[str]:  # without itertools

        mapping = {
            '2': {'a', 'b', 'c'},
            '3': {'d', 'e', 'f'},
            '4': {'g', 'h', 'i'},
            '5': {'j', 'k', 'l'},
            '6': {'m', 'n', 'o'},
            '7': {'p', 'q', 'r', 's'},
            '8': {'t', 'u', 'v'},
            '9': {'w', 'x', 'y', 'z'},
        }

        ans = mapping[digits[0]]
        for c in digits[1:]:
            tmp = []
            for y in ans:
                for x in mapping[c]:
                    tmp.append(y + x)
            ans = tmp

        return ans

    def letterCombinations(self, digits: str) -> List[str]:  # shorter
        mapping = {"2": "abc", "3": "def", "4": "ghi", "5": "jkl", "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}
        cmb = [''] if digits else []
        for d in digits:
            cmb = [p + q for p in cmb for q in mapping[d]]
        return cmb

    def letterCombinations(self, digits: str) -> List[str]:  # backtracking
        mapping = {"2": "abc", "3": "def", "4": "ghi", "5": "jkl", "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}
        ans = []

        def make_combinations(i, cur):
            if i == len(digits):
                if len(cur) > 0:
                    ans.append(''.join(cur))
                return
            for c in mapping[digits[i]]:
                cur.append(c)
                make_combinations(i + 1, cur)
                cur.pop()

        make_combinations(0, [])
        return ans


def test():
    array_of_digits = ["23", ""]
    expectations = [
        ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"],
        [],
        ]
    for digits, expected in zip(array_of_digits, expectations):
        solution = Solution().letterCombinations(digits)
        assert solution == expected
