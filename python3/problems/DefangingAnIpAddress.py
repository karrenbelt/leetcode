### Source : https://leetcode.com/problems/defanging-an-ip-address/
### Author : Karrenbelt
### Date   : 2020-04-14

##################################################################################################### 
#
# Given a valid (IPv4) IP address, return a defanged version of that IP address.
# 
# A defanged IP address replaces every period "." with "[.]".
# 
# Example 1:
# Input: address = "1.1.1.1"
# Output: "1[.]1[.]1[.]1"
# Example 2:
# Input: address = "255.100.50.0"
# Output: "255[.]100[.]50[.]0"
# 
# Constraints:
# 
# 	The given address is a valid IPv4 address.
#####################################################################################################


class Solution:
    def defangIPaddr(self, address: str) -> str:
        return address.replace('.', '[.]')


def test():
    arguments = [
        "1.1.1.1",
        "255.100.50.0",
    ]
    expectations = [
        "1[.]1[.]1[.]1",
        "255[.]100[.]50[.]0",
    ]
    for address, expected in zip(arguments, expectations):
        solution = Solution().defangIPaddr(address)
        assert solution == expected
