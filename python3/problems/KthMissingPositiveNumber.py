### Source : https://leetcode.com/problems/kth-missing-positive-number/description/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-06

#####################################################################################################
#
# Given an array arr of positive integers sorted in a strictly increasing order, and an integer k.
#
# Return the kth positive integer that is missing from this array.
#
# Example 1:
#
# Input: arr = [2,3,4,7,11], k = 5
# Output: 9
# Explanation: The missing positive integers are [1,5,6,8,9,10,12,13,...]. The 5th missing positive
# integer is 9.
#
# Example 2:
#
# Input: arr = [1,2,3,4], k = 2
# Output: 6
# Explanation: The missing positive integers are [5,6,7,...]. The 2nd missing positive integer is 6.
#
# Constraints:
#
# 	1 <= arr.length <= 1000
# 	1 <= arr[i] <= 1000
# 	1 <= k <= 1000
# 	arr[i] < arr[j] for 1 <= i < j <= arr.length
#
# Follow up:
#
# Could you solve this problem in less than O(n) complexity?
#####################################################################################################

from typing import List


class Solution:
    def findKthPositive(self, arr: List[int], k: int) -> int:
        i, nums = 0, set(arr)
        missing_nums = filter(lambda x: x not in nums, range(1, arr[-1]))
        for i, n in enumerate(missing_nums, start=1):
            if i == k:
                return n
        return arr[-1] + k - i

    def findKthPositive(self, arr: List[int], k: int) -> int:  # 56 ms
        return next((i for i, n in enumerate(arr) if n > i + k), len(arr)) + k

    def findKthPositive(self, arr: List[int], k: int) -> int:  # 47 ms

        def binary_search(left: int, right: int, fn: callable) -> int:
            while left < right:
                mid = left + right >> 1
                left, right = (mid + 1, right) if fn(mid) else (left, mid)
            return left

        def condition(mid: int) -> bool:
            return arr[mid] - mid <= k

        return binary_search(0, len(arr), condition) + k

    def findKthPositive(self, arr: List[int], k: int) -> int:
        from collections import UserList
        import bisect

        class ItemGetter(UserList):
            def __getitem__(self, i):
                return arr[i] - i - 1

        nums = ItemGetter(arr)
        return k + bisect.bisect_left(nums, k, 0, len(nums))

    def findKthPositive(self, arr: List[int], k: int) -> int:  # 43 ms
        import bisect
        nums = [n - i - 1 for i, n in enumerate(arr)]
        return k + bisect.bisect_left(nums, k, 0, len(arr))

    def findKthPositive(self, arr: List[int], k: int) -> int:
        from bisect import bisect_left
        return bisect_left(range(len(arr)), k, key=lambda i: arr[i] - i - 1) + k


def test():
    arguments = [
        ([2, 3, 4, 7, 11], 5),
        ([1, 2, 3, 4], 2),
        ([5, 6, 7, 8, 9], 9),
    ]
    expectations = [9, 6, 14]
    for (arr, k), expected in zip(arguments, expectations):
        solution = Solution().findKthPositive(arr, k)
        assert solution == expected, (solution, expected)
