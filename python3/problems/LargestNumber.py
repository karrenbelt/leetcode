### Source : https://leetcode.com/problems/largest-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# Given a list of non-negative integers nums, arrange them such that they form the largest number.
# 
# Note: The result may be very large, so you need to return a string instead of an integer.
# 
# Example 1:
# 
# Input: nums = [10,2]
# Output: "210"
# 
# Example 2:
# 
# Input: nums = [3,30,34,5,9]
# Output: "9534330"
# 
# Example 3:
# 
# Input: nums = [1]
# Output: "1"
# 
# Example 4:
# 
# Input: nums = [10]
# Output: "10"
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Comparable:
    def __init__(self, num):
        self.value = str(num)

    def __lt__(self, other):
        return self.value + other.value > other.value + self.value


class Solution:
    def largestNumber(self, nums: List[int]) -> str:  # O(n log n) time
        return str(int(''.join(map(str, sorted(nums, key=lambda x: x / (10**len(str(x)) - 1), reverse=True)))))

    def largestNumber(self, nums: List[int]) -> str:
        return ''.join(map(str, sorted(nums, key=lambda n: Comparable(n)))).lstrip('0') or '0'


def test():
    arguments = [
        [10, 2],
        [3, 30, 34, 5, 9],
        [1],
        [10],
        [34323, 3432],
        [0, 0]
    ]
    expectations = ["210", "9534330", "1", "10", "343234323", "0"]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().largestNumber(nums)
        assert solution == expected
