### Source : https://leetcode.com/problems/find-k-th-smallest-pair-distance/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# Given an integer array, return the k-th smallest distance among all the pairs. The distance of a 
# pair (A, B) is defined as the absolute difference between A and B. 
# 
# Example 1:
# 
# Input:
# nums = [1,3,1]
# k = 1
# Output: 0 
# Explanation:
# Here are all the pairs:
# (1,3) -> 2
# (1,1) -> 0
# (3,1) -> 2
# Then the 1st smallest distance pair is (1,1), and its distance is 0.
# 
# Note:
# 
# 2 .
# 0 .
# 1 .
# 
#####################################################################################################

from typing import List


class Solution:
    def smallestDistancePair(self, nums: List[int], k: int) -> int:
        nums.sort()
        left, right = 0, nums[-1] - nums[0]  # max distance
        while left < right:
            mid = (left + right) // 2  # guess
            j = ctr = 0
            for i in range(len(nums)):
                while j < len(nums) and nums[j] <= nums[i] + mid:   # if distance j - i is less than guess, increment j
                    j += 1
                ctr += j - i - 1  # all that satisfy the distance cut off criterion
            if ctr >= k:
                right = mid   # not mid - 1, since >= k, thus if mid == k is also correct
            else:
                left = mid + 1
        return left  # because bs ends when 2nd to last did not produce enough results


def test():
    arguments = [
        ([1, 3, 1], 1),
        (),
    ]
    expectations = [0]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().smallestDistancePair(nums, k)
        assert solution == expected
