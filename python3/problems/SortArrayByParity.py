### Source : https://leetcode.com/problems/sort-array-by-parity/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-04

##################################################################################################### 
#
# Given an array nums of non-negative integers, return an array consisting of all the even elements 
# of nums, followed by all the odd elements of nums.
# 
# You may return any answer array that satisfies this condition.
# 
# Example 1:
# 
# Input: nums = [3,1,2,4]
# Output: [2,4,3,1]
# The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
# 
# Note:
# 
# 	1 <= nums.length <= 5000
# 	0 <= nums[i] <= 5000
#####################################################################################################

from typing import List


class Solution:
    def sortArrayByParity(self, A: List[int]) -> List[int]:  # O(n) time and O(1) space
        i, j = 0, 0
        while j < len(A):
            if A[j] % 2 == 0:  # uneven
                A[i], A[j] = A[j], A[i]
                i += 1
            j += 1
        return A

    def sortArrayByParity(self, A: List[int]) -> List[int]:  # O(n) time and O(1) space
        return A.sort(key=lambda x: x & 1) or A


def test():
    arguments = [
        [3, 1, 2, 4],
    ]
    expectations = [
        [2, 4, 3, 1]  # [4,2,3,1], [2,4,1,3], and [4,2,1,3] are also accepted
    ]
    for A, expected in zip(arguments, expectations):
        solution = Solution().sortArrayByParity(A)
        assert solution == expected
