### Source : https://leetcode.com/problems/the-k-weakest-rows-in-a-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-15

##################################################################################################### 
#
# Given a m * n matrix mat of ones (representing soldiers) and zeros (representing civilians), return 
# the indexes of the k weakest rows in the matrix ordered from the weakest to the strongest.
# 
# A row i is weaker than row j, if the number of soldiers in row i is less than the number of 
# soldiers in row j, or they have the same number of soldiers but i is less than j. Soldiers are 
# always stand in the frontier of a row, that is, always ones may appear first and then zeros.
# 
# Example 1:
# 
# Input: mat = 
# [[1,1,0,0,0],
#  [1,1,1,1,0],
#  [1,0,0,0,0],
#  [1,1,0,0,0],
#  [1,1,1,1,1]], 
# k = 3
# Output: [2,0,3]
# Explanation: 
# The number of soldiers for each row is: 
# row 0 -> 2 
# row 1 -> 4 
# row 2 -> 1 
# row 3 -> 2 
# row 4 -> 5 
# Rows ordered from the weakest to the strongest are [2,0,3,1,4]
# 
# Example 2:
# 
# Input: mat = 
# [[1,0,0,0],
#  [1,1,1,1],
#  [1,0,0,0],
#  [1,0,0,0]], 
# k = 2
# Output: [0,2]
# Explanation: 
# The number of soldiers for each row is: 
# row 0 -> 1 
# row 1 -> 4 
# row 2 -> 1 
# row 3 -> 1 
# Rows ordered from the weakest to the strongest are [0,2,3,1]
# 
# Constraints:
# 
# 	m == mat.length
# 	n == mat[i].length
# 	2 <= n, m <= 100
# 	1 <= k <= m
# 	matrix[i][j] is either 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:  # O(m * (n + log m)) time and O(m) space
        weakest = list(map(sum, mat))
        return sorted(range(len(weakest)), key=lambda x: weakest[x])[:k]

    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:  # O(m * (n + log m)) time and O(m) space
        return sorted(range(len(mat)), key=lambda x: sum(mat[x]))[:k]

    def kWeakestRows(self, mat: List[List[int]], k: int) -> List[int]:  # O(m * (n + log k) + k log k) time, O(k) space
        import heapq
        return [x[1] for x in heapq.nsmallest(k, ((sum(s), i) for i, s in enumerate(mat)))]


def test():
    arguments = [
        ([[1, 1, 0, 0, 0], [1, 1, 1, 1, 0], [1, 0, 0, 0, 0], [1, 1, 0, 0, 0], [1, 1, 1, 1, 1]], 3),
        ([[1, 0, 0, 0], [1, 1, 1, 1], [1, 0, 0, 0], [1, 0, 0, 0]], 2),
    ]
    expectations = [[2, 0, 3], [0, 2]]
    for (mat, k), expected in zip(arguments, expectations):
        solution = Solution().kWeakestRows(mat, k)
        assert solution == expected
