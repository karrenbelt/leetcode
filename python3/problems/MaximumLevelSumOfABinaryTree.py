### Source : https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so 
# on.
# 
# Return the smallest level x such that the sum of all the values of nodes at level x is maximal.
# 
# Example 1:
# 
# Input: root = [1,7,0,7,-8,null,null]
# Output: 2
# Explanation: 
# Level 1 sum = 1.
# Level 2 sum = 7 + 0 = 7.
# Level 3 sum = 7 + -8 = -1.
# So we return the level with the maximum sum which is level 2.
# 
# Example 2:
# 
# Input: root = [989,null,10250,98693,-89388,null,null,null,-32127]
# Output: 2
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-105 <= Node.val <= 105
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def maxLevelSum(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, level: int):
            if not node:
                return
            if level == len(levels):
                levels.append([])
            levels[level].append(node.val)
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root, 0)
        return max(enumerate(levels), key=lambda x: sum(x[1]) - x[0])[0] + 1

    def maxLevelSum(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, level: int):
            if not node:
                return
            if level == len(levels):
                levels.append(0)
            levels[level] += node.val
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root, 0)
        return max(enumerate(levels), key=lambda x: x[1] - x[0])[0] + 1

    def maxLevelSum(self, root: TreeNode) -> int:
        from collections import deque

        queue = deque([(root, 0)])
        levels = []
        while queue:
            node, level = queue.popleft()
            if level == len(levels):
                levels.append(0)
            levels[level] += node.val
            if node.left:
                queue.append((node.left, level + 1))
            if node.right:
                queue.append((node.right, level + 1))

        return max(enumerate(levels), key=lambda x: x[1] - x[0])[0] + 1


def test():
    from python3 import Codec
    arguments = [
        "[1,7,0,7,-8,null,null]",
        "[989,null,10250,98693,-89388,null,null,null,-32127]",
        ]
    expectations = [2, 2]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().maxLevelSum(root)
        assert solution == expected
