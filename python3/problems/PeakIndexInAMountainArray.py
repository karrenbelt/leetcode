### Source : https://leetcode.com/problems/peak-index-in-a-mountain-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-04

##################################################################################################### 
#
# Let's call an array arr a mountain if the following properties hold:
# 
# 	arr.length >= 3
# 	There exists some i with 0 < i < arr.length - 1 such that:
# 
# 		arr[0] < arr[1] < ... arr[i-1] < arr[i] 
# 		arr[i] > arr[i+1] > ... > arr[arr.length - 1]
# 
# Given an integer array arr that is guaranteed to be a mountain, return any i such that arr[0] < 
# arr[1] < ... arr[i - 1] < arr[i] > arr[i + 1] > ... > arr[arr.length - 1].
# 
# Example 1:
# Input: arr = [0,1,0]
# Output: 1
# Example 2:
# Input: arr = [0,2,1,0]
# Output: 1
# Example 3:
# Input: arr = [0,10,5,2]
# Output: 1
# Example 4:
# Input: arr = [3,4,5,1]
# Output: 2
# Example 5:
# Input: arr = [24,69,100,99,79,78,67,36,26,19]
# Output: 2
# 
# Constraints:
# 
# 	3 <= arr.length <= 104
# 	0 <= arr[i] <= 106
# 	arr is guaranteed to be a mountain array.
#####################################################################################################

from typing import List


class Solution:
    def peakIndexInMountainArray(self, arr: List[int]) -> int:  # O(n) time
        return arr.index(max(arr))

    def peakIndexInMountainArray(self, arr: List[int]) -> int:  # O(log n) time
        left, right = 0, len(arr) - 1
        while left < right:
            mid = left + (right - left) // 2
            if arr[mid] < arr[mid + 1]:
                left = mid + 1
            else:
                right = mid
        return left


def test():
    arguments = [
        [0, 1, 0],
        [0, 2, 1, 0],
        [0, 10, 5, 2],
        [24, 69, 100, 99, 79, 78, 67, 36, 26, 19],
        ]
    expectations = [1, 1, 1, 2, 2]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().peakIndexInMountainArray(arr)
        assert solution == expected
test()