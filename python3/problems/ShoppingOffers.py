### Source : https://leetcode.com/problems/shopping-offers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-02

##################################################################################################### 
#
# In LeetCode Store, there are n items to sell. Each item has a price. However, there are some 
# special offers, and a special offer consists of one or more different kinds of items with a sale 
# price.
# 
# You are given an integer array price where price[i] is the price of the ith item, and an integer 
# array needs where needs[i] is the number of pieces of the ith item you want to buy.
# 
# You are also given an array special where special[i] is of size n + 1 where special[i][j] is the 
# number of pieces of the jth item in the ith offer and special[i][n] (i.e., the last integer in the 
# array) is the price of the ith offer.
# 
# Return the lowest price you have to pay for exactly certain items as given, where you could make 
# optimal use of the special offers. You are not allowed to buy more items than you want, even if 
# that would lower the overall price. You could use any of the special offers as many times as you 
# want.
# 
# Example 1:
# 
# Input: price = [2,5], special = [[3,0,5],[1,2,10]], needs = [3,2]
# Output: 14
# Explanation: There are two kinds of items, A and B. Their prices are $2 and $5 respectively. 
# In special offer 1, you can pay $5 for 3A and 0B
# In special offer 2, you can pay $10 for 1A and 2B. 
# You need to buy 3A and 2B, so you may pay $10 for 1A and 2B (special offer #2), and $4 for 2A.
# 
# Example 2:
# 
# Input: price = [2,3,4], special = [[1,1,0,4],[2,2,1,9]], needs = [1,2,1]
# Output: 11
# Explanation: The price of A is $2, and $3 for B, $4 for C. 
# You may pay $4 for 1A and 1B, and $9 for 2A ,2B and 1C. 
# You need to buy 1A ,2B and 1C, so you may pay $4 for 1A and 1B (special offer #1), and $3 for 1B, 
# $4 for 1C. 
# You cannot add more items, though only $9 for 2A ,2B and 1C.
# 
# Constraints:
# 
# 	n == price.length
# 	n == needs.length
# 	1 <= n <= 6
# 	0 <= price[i] <= 10
# 	0 <= needs[i] <= 10
# 	1 <= special.length <= 100
# 	special[i].length == n + 1
# 	0 <= special[i][j] <= 50
#####################################################################################################

from typing import List


class Solution:
    def shoppingOffers(self, price: List[int], special: List[List[int]], needs: List[int]) -> int:
        # we make sure that the special offers can be used, and that it offers a discount, otherwise we remove it
        good_offers = set()
        for offer in special:
            can_use = all(a - b >= 0 for a, b in zip(needs, offer))
            discount = sum(p * quantity for p, quantity in zip(price, offer)) > offer[-1]
            if can_use and discount:
                good_offers.add(tuple(offer))

        # first we get all the specials
        paths = {(tuple(needs), 0)}
        seen = set()
        while True:
            new_paths = set()
            for path, cost in paths:
                if path in seen:
                    continue
                seen.add(path)
                for offer in good_offers:
                    if all(a - b >= 0 for a, b in zip(path, offer)):
                        new_path = tuple(path[i] - offer[i] for i in range(len(path)))
                        new_paths.add((new_path, cost + offer[-1]))
            if not new_paths:
                break
            paths.update(new_paths)

        # fill the remainder of the offer, return the minimum
        return min(sum(path[i] * price[i] for i in range(len(path))) + cost for path, cost in paths) if paths else 0

    # def shoppingOffers(self, price: List[int], special: List[List[int]], needs: List[int]) -> int:
    #
    #     def dfs(need):
    #         if need not in memo:
    #             res = sum(p * n for p, n in zip(price, need))
    #             for offer in special:
    #                 new_need = tuple(a - b for a, b in zip(need, offer))
    #                 if min(new_need) >= 0:
    #                     res = min(res, offer[-1] + dfs(new_need))
    #             memo[tuple(need)] = res
    #         return memo[tuple(need)]
    #
    #     memo = {}
    #     return dfs(tuple(needs))


def test():
    arguments = [
        ([2, 5], [[3, 0, 5], [1, 2, 10]], [3, 2]),
        ([2, 3, 4], [[1, 1, 0, 4], [2, 2, 1, 9]], [1, 2, 1]),
        ([2, 3, 4], [[1, 1, 0, 4], [2, 2, 1, 9]], [0, 0, 0]),
        ([1, 1, 1], [[1, 1, 1, 0], [2, 2, 1, 1]], [1, 1, 0]),
        ([3, 10, 5, 7, 7, 3],
         [[3, 2, 2, 6, 2, 5, 30], [4, 6, 5, 2, 3, 2, 12], [1, 0, 4, 6, 3, 3, 23], [0, 5, 3, 2, 4, 2, 8],
          [4, 1, 2, 6, 3, 2, 33], [3, 0, 6, 5, 5, 2, 33], [1, 4, 4, 0, 2, 6, 10], [5, 1, 0, 5, 6, 3, 16],
          [3, 4, 5, 6, 3, 5, 5], [6, 0, 6, 0, 5, 5, 22], [3, 0, 4, 4, 2, 6, 15], [5, 4, 2, 3, 5, 0, 9],
          [4, 4, 5, 5, 6, 1, 23], [2, 6, 5, 6, 6, 4, 4], [5, 0, 2, 1, 0, 4, 21], [1, 4, 6, 6, 1, 6, 4],
          [4, 0, 5, 6, 2, 2, 17], [1, 3, 5, 1, 3, 3, 25], [0, 1, 4, 5, 6, 4, 7], [0, 2, 0, 6, 4, 4, 17],
          [3, 2, 3, 5, 2, 2, 27], [2, 2, 6, 1, 5, 4, 31], [6, 6, 0, 0, 5, 2, 6], [4, 6, 6, 2, 6, 6, 2],
          [1, 2, 4, 4, 4, 6, 22], [4, 6, 4, 4, 1, 4, 2], [6, 6, 5, 6, 3, 5, 31], [6, 5, 0, 4, 2, 5, 8],
          [2, 2, 4, 3, 4, 6, 24], [6, 5, 6, 1, 4, 4, 25], [5, 6, 1, 3, 3, 6, 2], [5, 2, 4, 4, 0, 2, 6],
          [3, 0, 4, 3, 3, 6, 27], [5, 5, 3, 3, 4, 3, 10], [1, 5, 1, 5, 2, 2, 9], [3, 2, 1, 1, 4, 5, 8],
          [3, 5, 3, 6, 6, 5, 13], [0, 4, 6, 5, 3, 1, 17], [5, 2, 4, 6, 5, 5, 23], [0, 5, 3, 1, 0, 4, 20],
          [3, 0, 0, 4, 6, 1, 28], [3, 0, 1, 3, 4, 0, 3], [5, 6, 6, 3, 1, 1, 22], [6, 4, 3, 4, 2, 0, 10],
          [1, 2, 0, 2, 1, 5, 28], [5, 6, 5, 6, 6, 6, 8], [2, 4, 6, 0, 4, 3, 4], [3, 2, 5, 1, 6, 1, 15],
          [0, 6, 2, 1, 2, 2, 7], [1, 0, 5, 4, 5, 3, 21], [4, 2, 3, 6, 4, 2, 29], [1, 4, 2, 3, 6, 0, 28],
          [4, 1, 0, 4, 1, 3, 22], [1, 3, 4, 0, 2, 2, 27], [0, 3, 0, 2, 3, 3, 18], [0, 3, 5, 0, 5, 0, 31],
          [5, 0, 0, 0, 6, 5, 33], [5, 1, 0, 6, 3, 2, 32], [3, 4, 1, 1, 5, 6, 29], [1, 2, 1, 1, 4, 6, 34],
          [2, 6, 4, 5, 4, 2, 24], [4, 1, 2, 2, 0, 0, 22], [3, 2, 4, 4, 4, 4, 7], [2, 3, 2, 3, 4, 1, 8],
          [1, 1, 2, 3, 5, 5, 25], [3, 0, 5, 0, 6, 1, 17], [0, 1, 5, 1, 2, 3, 16], [6, 5, 2, 0, 6, 2, 32],
          [3, 4, 4, 2, 5, 0, 23], [3, 3, 4, 6, 3, 2, 17], [1, 3, 3, 1, 3, 1, 14], [5, 2, 4, 2, 0, 0, 11],
          [0, 2, 4, 1, 6, 1, 16], [2, 6, 1, 4, 2, 5, 20], [4, 0, 6, 5, 2, 0, 19], [3, 1, 2, 1, 4, 0, 8],
          [2, 5, 4, 0, 0, 1, 19], [5, 0, 4, 5, 5, 5, 23], [5, 1, 4, 2, 1, 1, 34], [1, 1, 2, 4, 0, 3, 3],
          [2, 3, 5, 5, 1, 2, 24], [6, 1, 5, 2, 1, 2, 18], [4, 5, 3, 3, 6, 5, 26], [6, 6, 2, 0, 0, 2, 32],
          [6, 5, 6, 1, 6, 4, 15], [0, 6, 3, 5, 5, 6, 5], [5, 2, 6, 2, 5, 3, 24], [5, 6, 0, 4, 6, 2, 7],
          [4, 2, 5, 3, 1, 5, 8], [5, 2, 2, 6, 0, 3, 34], [5, 3, 0, 3, 3, 0, 5], [1, 1, 0, 6, 0, 0, 21],
          [6, 0, 4, 2, 3, 2, 13], [2, 0, 1, 0, 1, 4, 32], [1, 0, 4, 4, 1, 5, 2], [4, 5, 3, 6, 6, 1, 34],
          [6, 2, 5, 4, 2, 2, 12], [3, 2, 2, 3, 3, 4, 28], [2, 6, 4, 1, 4, 2, 25], [6, 0, 2, 1, 3, 3, 11]],
         [2, 5, 4, 6, 5, 2]),
    ]
    expectations = [14, 11, 0, 2, 54]
    for (price, special, needs), expected in zip(arguments, expectations):
        solution = Solution().shoppingOffers(price, special, needs)
        assert solution == expected, needs
