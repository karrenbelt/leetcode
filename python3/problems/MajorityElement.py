### Source : https://leetcode.com/problems/majority-element/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given an array of size n, find the majority element. The majority element is the element that 
# appears more than &lfloor; n/2 &rfloor; times.
# 
# You may assume that the array is non-empty and the majority element always exist in the array.
# 
# Example 1:
# 
# Input: [3,2,3]
# Output: 3
# 
# Example 2:
# 
# Input: [2,2,1,1,1,2,2]
# Output: 2
# 
#####################################################################################################

from typing import List


class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        from collections import Counter
        return max(Counter(nums).items(), key=lambda x: x[1])[0]

    def majorityElement(self, nums: List[int]) -> int:
        d = {}.fromkeys(nums, 0)
        for n in nums:
            d[n] += 1
            if d[n] > len(nums) // 2:
                return n


def test():
    arrays_of_numbers = [
        [3, 2, 3],
        [2, 2, 1, 1, 1, 2, 2],
        ]
    expectations = [3, 2]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().majorityElement(nums)
        assert solution == expected
