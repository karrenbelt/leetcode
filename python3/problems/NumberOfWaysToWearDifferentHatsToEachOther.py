### Source : https://leetcode.com/problems/number-of-ways-to-wear-different-hats-to-each-other/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-18

##################################################################################################### 
#
# There are n people and 40 types of hats labeled from 1 to 40.
# 
# Given a list of list of integers hats, where hats[i] is a list of all hats preferred by the i-th 
# person.
# 
# Return the number of ways that the n people wear different hats to each other.
# 
# Since the answer may be too large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: hats = [[3,4],[4,5],[5]]
# Output: 1
# Explanation: There is only one way to choose hats given the conditions. 
# First person choose hat 3, Second person choose hat 4 and last one hat 5.
# 
# Example 2:
# 
# Input: hats = [[3,5,1],[3,5]]
# Output: 4
# Explanation: There are 4 ways to choose hats
# (3,5), (5,3), (1,3) and (1,5)
# 
# Example 3:
# 
# Input: hats = [[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]
# Output: 24
# Explanation: Each person can choose hats labeled from 1 to 4.
# Number of Permutations of (1,2,3,4) = 24.
# 
# Example 4:
# 
# Input: hats = [[1,2,3],[2,3,5,6],[1,3,7,9],[1,8,9],[2,5,7]]
# Output: 111
# 
# Constraints:
# 
# 	n == hats.length
# 	1 <= n <= 10
# 	1 <= hats[i].length <= 40
# 	1 <= hats[i][j] <= 40
# 	hats[i] contains a list of unique integers.
#####################################################################################################

from typing import List


class Solution:
    def numberWays(self, hats: List[List[int]]) -> int:
        # many more hats than people, so assign hats to people, not people to hats
        from functools import lru_cache

        preferences = [[] for _ in range(41)]
        for person, pref in enumerate(hats):
            for h in pref:
                preferences[h].append(person)

        @lru_cache(maxsize=None)
        def count_ways(i: int, used: int):
            if bin(used).count('1') == len(hats):
                return 1
            if i == 41:
                return 0

            ways = count_ways(i + 1, used)
            for p in preferences[i]:
                if used & 1 << p == 0:
                    ways += count_ways(i + 1, used | 1 << p) % (10**9 + 7)
            return ways % (10**9 + 7)

        return count_ways(0, 0)


def test():
    arguments = [
        [[3, 4], [4, 5], [5]],
        [[3, 5, 1], [3, 5]],
        [[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]],
    ]
    expectations = [1, 4, 24]
    for hats, expected in zip(arguments, expectations):
        solution = Solution().numberWays(hats)
        assert solution == expected
