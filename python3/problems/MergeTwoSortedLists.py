### Source : https://leetcode.com/problems/merge-two-sorted-lists/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-21

##################################################################################################### 
#
# erge two sorted linked lists and return it as a sorted list. The list should be made by splicing 
# together the nodes of the first two lists.
# 
# Example 1:
# 
# Input: l1 = [1,2,4], l2 = [1,3,4]
# Output: [1,1,2,3,4,4]
# 
# Example 2:
# 
# Input: l1 = [], l2 = []
# Output: []
# 
# Example 3:
# 
# Input: l1 = [], l2 = [0]
# Output: [0]
# 
# Constraints:
# 
# 	The number of nodes in both lists is in the range [0, 50].
# 	-100 <= Node.val <= 100
# 	Both l1 and l2 are sorted in non-decreasing order.
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(n) time and O(n) space

        def merge(n1: ListNode, n2: ListNode):
            if n1 and n2:
                if n1.val > n2.val:
                    stack.append(n2)
                    merge(n1, n2.next)
                else:
                    stack.append(n1)
                    merge(n1.next, n2)
            elif n1 or n2:
                stack.append(n1 if n1 else n2)

        stack = []
        merge(l1, l2)
        for i in range(len(stack) - 1):
            stack[i].next = stack[i + 1]
        return stack[0] if stack else l1 or l2

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(n) time and O(n) space

        def merge(n1: ListNode, n2: ListNode) -> ListNode:
            if not n1 or not n2:
                return n1 or n2
            elif n1.val < n2.val:
                n1.next = merge(n1.next, n2)
            else:
                n2.next = merge(n1, n2.next)
            return n1 if n1.val < n2.val else n2

        return merge(l1, l2)

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(n) time and O(1) space
        dummy = node = ListNode(None)
        while l1 and l2:
            if l1.val <= l2.val:
                tmp, l1 = l1, l1.next
                tmp.next, node.next = None, tmp
            else:
                tmp, l2 = l2, l2.next
                tmp.next, node.next = None, tmp
            node = node.next
        node.next = l1 or l2
        return dummy.next

    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(n + m) time and O(1) space
        node = dummy = ListNode(-(1 << 31) - 1)
        while l1 and l2:
            if l1.val < l2.val:
                node.next, l1 = l1, l1.next
            else:
                node.next, l2 = l2, l2.next
            node = node.next
        node.next = l1 or l2
        return dummy.next


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        ([1, 2, 4], [1, 3, 4]),
        ([], []),
        ([], [0]),
        ]
    expected = [
        [1, 1, 2, 3, 4, 4],
        [],
        [0],
        ]
    for (nums1, nums2), expected in zip(arrays_of_numbers, expected):
        l1 = singly_linked_list_from_array(nums1).head
        l2 = singly_linked_list_from_array(nums2).head
        solution = Solution().mergeTwoLists(l1, l2)
        assert solution == singly_linked_list_from_array(expected).head
