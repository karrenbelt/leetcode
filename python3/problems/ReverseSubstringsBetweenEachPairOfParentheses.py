### Source : https://leetcode.com/problems/reverse-substrings-between-each-pair-of-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-31

##################################################################################################### 
#
# You are given a string s that consists of lower case English letters and brackets. 
# 
# Reverse the strings in each pair of matching parentheses, starting from the innermost one.
# 
# Your result should not contain any brackets.
# 
# Example 1:
# 
# Input: s = "(abcd)"
# Output: "dcba"
# 
# Example 2:
# 
# Input: s = "(u(love)i)"
# Output: "iloveu"
# Explanation: The substring "love" is reversed first, then the whole string is reversed.
# 
# Example 3:
# 
# Input: s = "(ed(et(oc))el)"
# Output: "leetcode"
# Explanation: First, we reverse the substring "oc", then "etco", and finally, the whole string.
# 
# Example 4:
# 
# Input: s = "a(bcdefghijkl(mno)p)q"
# Output: "apmnolkjihgfedcbq"
# 
# Constraints:
# 
# 	0 <= s.length <= 2000
# 	s only contains lower case English characters and parentheses.
# 	It's guaranteed that all parentheses are balanced.
#####################################################################################################


class Solution:
    def reverseParentheses(self, s: str) -> str:  # O(n^2) time and O(n) space
        stack = ['']
        for c in s:
            if c == '(':
                stack.append('')
            elif c == ')':
                part = stack.pop()[::-1]
                stack[-1] += part
            else:
                stack[-1] += c
        return stack.pop()

    def reverseParentheses(self, s: str) -> str:  # O(n) time
        opened, pair, result = [], {}, ''
        for i, c in enumerate(s):
            if c == '(':
                opened.append(i)
            elif c == ')':
                j = opened.pop()
                pair[i], pair[j] = j, i

        i, direction = 0, 1
        while i < len(s):
            if s[i] in '()':
                i = pair[i]
                direction = -direction
            else:
                result += s[i]
            i += direction
        return result


def test():
    arguments = [
        "(abcd)",
        "(u(love)i)",
        "(ed(et(oc))el)",
        "a(bcdefghijkl(mno)p)q",
    ]
    expectations = [
        "dcba",
        "iloveu",
        "leetcode",
        "apmnolkjihgfedcbq",
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().reverseParentheses(s)
        assert solution == expected
