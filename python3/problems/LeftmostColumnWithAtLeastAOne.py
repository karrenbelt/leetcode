# (This problem is an interactive problem.)
#
# A binary matrix means that all elements are 0 or 1. For each individual row of the matrix,
# this row is sorted in non-decreasing order.
#
# Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with at least a 1 in it.
# If such index doesn't exist, return -1.
#
# You can't access the Binary Matrix directly.  You may only access the matrix using a BinaryMatrix interface:
#
#     BinaryMatrix.get(x, y) returns the element of the matrix at index (x, y) (0-indexed).
#     BinaryMatrix.dimensions() returns a list of 2 elements [n, m], which means the matrix is n * m.
#
# Submissions making more than 1000 calls to BinaryMatrix.get will be judged Wrong Answer.
# Also, any solutions that attempt to circumvent the judge will result in disqualification.
#
# For custom testing purposes you're given the binary matrix mat as input in the following four examples.
# You will not have access the binary matrix directly.
#
# Constraints:
#
#     1 <= mat.length, mat[i].length <= 100
#     mat[i][j] is either 0 or 1.
#     mat[i] is sorted in a non-decreasing way.

# """
# This is BinaryMatrix's API interface.
# You should not implement it, or speculate about its implementation
# """
#class BinaryMatrix(object):
#    def get(self, x: int, y: int) -> int:
#    def dimensions(self) -> list[]:

from typing import List


class BinaryMatrix:

    def __init__(self, matrix: List[List[int]]):
        self.matrix = matrix

    def get(self, x: int, y: int):
        return self.matrix[x][y]

    def dimensions(self) -> List[int]:
        return [len(self.matrix), len(self.matrix[0])]


class Solution:
    def leftMostColumnWithOne(self, binaryMatrix: 'BinaryMatrix') -> int:
        n, m = binaryMatrix.dimensions()
        i = 0
        m -= 1
        found = False

        while True:
            num = binaryMatrix.get(i, m)
            if num == 0 and i < n - 1:
                i += 1  # move down
            elif num == 1:
                found = True
                if m > 0:
                    m -= 1  # move left
                else:
                    return m
            else:
                return -1 if not found else m + 1


def test():
    arguments = [
        [[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1]],
        [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 1]],
        [[0]],
        [[0, 0], [1, 1]],
        [[0], [0], [1], [1]],
    ]
    expectations = [1, 2, -1, 0, 0]
    for binaryMatrix, expected in zip(arguments, expectations):
        solution = Solution().leftMostColumnWithOne(BinaryMatrix(binaryMatrix))
        assert solution == expected, (solution, expected)
