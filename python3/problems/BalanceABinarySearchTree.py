### Source : https://leetcode.com/problems/balance-a-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# Given a binary search tree, return a balanced binary search tree with the same node values.
# 
# A binary search tree is balanced if and only if the depth of the two subtrees of every node never 
# differ by more than 1.
# 
# If there is more than one answer, return any of them.
# 
# Example 1:
# 
# Input: root = [1,null,2,null,3,null,4,null,null]
# Output: [2,1,3,null,null,null,4]
# Explanation: This is not the only correct answer, [3,1,4,null,2,null,null] is also correct.
# 
# Constraints:
# 
# 	The number of nodes in the tree is between 1 and 104.
# 	The tree nodes will have distinct values between 1 and 105.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def balanceBST(self, root: TreeNode) -> TreeNode:
        # easiest way to do this is to traverse the tree and construct a new one

        def traverse(node: TreeNode):  # in-order
            if not node:
                return
            traverse(node.left)
            values.append(node.val)
            traverse(node.right)

        def build(values: list):  # slicing list is slow and takes time
            if not values:
                return
            mid = len(values) // 2
            node = TreeNode(values[mid])
            node.left = build(values[:mid])
            node.right = build(values[mid + 1:])
            return node

        values = []
        traverse(root)
        new_root = build(values)
        return new_root

    def balanceBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode):  # 1. now we save the node, not the values, so we can reuse them
            if not node:
                return
            traverse(node.left)
            values.append(node)
            traverse(node.right)

        def build(left, right):  # 2. slicing list is slow and takes space (it copies part of the list)
            if left > right:
                return
            mid = (left + right) // 2
            node = values[mid]
            node.left = build(left, mid - 1)
            node.right = build(mid + 1, right)
            return node

        values = []
        traverse(root)
        new_root = build(0, len(values) - 1)
        return new_root


def test():
    from python3 import Codec
    arguments = [
        "[1,null,2,null,3,null,4,null,null]",
    ]
    expectations = [
        "[2,1,3,null,null,null,4]",
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().balanceBST(root)
        assert solution == Codec.deserialize(expected)
