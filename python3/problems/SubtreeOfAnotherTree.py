### Source : https://leetcode.com/problems/subtree-of-another-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Given two non-empty binary trees s and t, check whether tree t has exactly the same structure and 
# node values with a subtree of s. A subtree of s is a tree consists of a node in s and all of this 
# node's descendants. The tree s could also be considered as a subtree of itself.
# 
# Example 1:
# Given tree s:
# 
#      3
#     / \
#    4   5
#   / \
#  1   2
# 
# Given tree t:
# 
#    4 
#   / \
#  1   2
# 
# Return true, because t has the same structure and node values with a subtree of s.
# 
# Example 2:
# Given tree s:
# 
#      3
#     / \
#    4   5
#   / \
#  1   2
#     /
#    0
# 
# Given tree t:
# 
#    4
#   / \
#  1   2
# 
# Return false.
# 
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:  # O(s * t) time

        def is_match(node_s, node_t):
            if not node_s or not node_t:
                return node_s is node_t
            return node_s.val == node_t.val and \
                   is_match(node_s.left, node_t.left) and is_match(node_s.right, node_t.right)

        if is_match(s, t):
            return True
        elif not s:
            return False
        return self.isSubtree(s.left, t) or self.isSubtree(s.right, t)

    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:  # O(s + t) time
        from hashlib import sha256

        def hash_(x: str):
            sha = sha256()
            sha.update(x.encode('utf-8'))
            return sha.hexdigest()

        def merkle(node):
            if not node:
                return '#'
            m_left = merkle(node.left)
            m_right = merkle(node.right)
            node.merkle = hash_(m_left + str(node.val) + m_right)
            return node.merkle

        merkle(s)
        merkle(t)

        def dfs(node):
            if not node:
                return False
            return node.merkle == t.merkle or dfs(node.left) or dfs(node.right)

        return dfs(s)


def test():
    from python3 import Codec
    arguments = [
        ("[3,4,5,1,2]", "[4,1,2]"),
        ("[3,4,5,1,2,null,null,null,null,0]", "[4,1,2]"),
        ]
    expectations = [True, False]
    for serialized_trees, expected in zip(arguments, expectations):
        s, t = map(Codec.deserialize, serialized_trees)
        solution = Solution().isSubtree(s, t)
        assert solution == expected
