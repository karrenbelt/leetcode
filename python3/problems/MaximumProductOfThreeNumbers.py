### Source : https://leetcode.com/problems/maximum-product-of-three-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-16

##################################################################################################### 
#
# Given an integer array, find three numbers whose product is maximum and output the maximum product.
# 
# Example 1:
# 
# Input: [1,2,3]
# Output: 6
# 
# Example 2:
# 
# Input: [1,2,3,4]
# Output: 24
# 
# Note:
# 
# 1. The length of the given array will be in range [3,104] and all elements are in the range
# [-1000, 1000].
# 2. Multiplication of any three numbers in the input won't exceed the range of 32-bit signed
# integer.
# 
#####################################################################################################

from typing import List


class Solution:
    def maximumProduct(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        # can also include two negatives
        nums.sort()
        return max(nums[0]*nums[1]*nums[-1], nums[-3]*nums[-2]*nums[-1])

    def maximumProduct(self, nums: List[int]) -> int:  # O(n log k) time and O(1) space
        import heapq
        return max(nums) * max(a * b for a, b in [heapq.nsmallest(2, nums), heapq.nlargest(3, nums)[1:]])

    def maximumProduct(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        smallest_two = [1000 ** 3] * 2
        largest_three = [-1000 ** 3] * 3
        for num in nums:
            if num <= smallest_two[0]:
                smallest_two[0] = num
                smallest_two.sort(reverse=True)
            if num >= largest_three[0]:
                largest_three[0] = num
                largest_three.sort()
        return max(smallest_two[0]*smallest_two[1]*largest_three[2], largest_three[0]*largest_three[1]*largest_three[2])


def test():
    arguments = [
        [1, 2, 3],
        [1, 2, 3, 4],
        [-1, -2, -3]
    ]
    expectations = [6, 24, -6]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maximumProduct(nums)
        assert solution == expected
