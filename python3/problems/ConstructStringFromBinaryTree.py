### Source : https://leetcode.com/problems/construct-string-from-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given the root of a binary tree, construct a string consists of parenthesis and integers from a 
# binary tree with the preorder traversing way, and return it.
# 
# Omit all the empty parenthesis pairs that do not affect the one-to-one mapping relationship between 
# the string and the original binary tree.
# 
# Example 1:
# 
# Input: root = [1,2,3,4]
# Output: "1(2(4))(3)"
# Explanation: Originallay it needs to be "1(2(4)())(3()())", but you need to omit all the 
# unnecessary empty parenthesis pairs. And it will be "1(2(4))(3)"
# 
# Example 2:
# 
# Input: root = [1,2,3,null,4]
# Output: "1(2()(4))(3)"
# Explanation: Almost the same as the first example, except we cannot omit the first parenthesis pair 
# to break the one-to-one mapping relationship between the input and the output.
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-1000 <= Node.val <= 1000
#####################################################################################################


from python3 import BinaryTreeNode as TreeNode


class Solution:
    def tree2str(self, root: TreeNode) -> str:

        def traverse(node) -> str:
            if not node:
                return ""
            left = traverse(node.left)
            right = traverse(node.right)
            return str(node.val) + (f"({left})" if left or right else "") + (f"({right})" if right else "")

        return traverse(root)


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4]",
        "[1,2,3,null,4]",
    ]
    expectations = [
        "1(2(4))(3)",
        "1(2()(4))(3)",
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().tree2str(root)
        assert solution == expected
test()