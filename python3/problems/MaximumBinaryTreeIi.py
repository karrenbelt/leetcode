### Source : https://leetcode.com/problems/maximum-binary-tree-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# We are given the root node of a maximum tree: a tree where every node has a value greater than any 
# other value in its subtree.
# 
# Just as in the previous problem, the given tree was constructed from an list A (root = 
# Construct(A)) recursively with the following Construct(A) routine:
# 
# 	If A is empty, return null.
# 	Otherwise, let A[i] be the largest element of A.  Create a root node with value A[i].
# 	The left child of root will be Construct([A[0], A[1], ..., A[i-1]])
# 	The right child of root will be Construct([A[i+1], A[i+2], ..., A[A.length - 1]])
# 	Return root.
# 
# Note that we were not given A directly, only a root node root = Construct(A).
# 
# Suppose B is a copy of A with the value val appended to it.  It is guaranteed that B has unique 
# values.
# 
# Return Construct(B).
# 
# Example 1:
# 
# Input: root = [4,1,3,null,null,2], val = 5
# Output: [5,4,null,1,3,null,null,2]
# Explanation: A = [1,4,2,3], B = [1,4,2,3,5]
# 
# Example 2:
# 
# Input: root = [5,2,4,null,1], val = 3
# Output: [5,2,4,null,1,null,3]
# Explanation: A = [2,1,5,4], B = [2,1,5,4,3]
# 
# Example 3:
# 
# Input: root = [5,2,3,null,1], val = 4
# Output: [5,2,4,null,1,3]
# Explanation: A = [2,1,5,3], B = [2,1,5,3,4]
# 
# Constraints:
# 
# 	1 <= B.length <= 100
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def insertIntoMaxTree(self, root: TreeNode, val: int) -> TreeNode:  # O(n) time and O(n) space
        # overly complicated phrasing of the question? method name is most revealing
        if not root:
            return TreeNode(val)
        if root.val < val:
            node = TreeNode(val)
            node.left = root
            return node
        root.right = self.insertIntoMaxTree(root.right, val)
        return root

    def insertIntoMaxTree(self, root: TreeNode, val: int) -> TreeNode:
        if root and root.val > val:
            root.right = self.insertIntoMaxTree(root.right, val)
            return root
        node = TreeNode(val)
        node.left = root
        return node

    def insertIntoMaxTree(self, root: TreeNode, val: int) -> TreeNode:
        pre, cur = None, root
        while cur and cur.val > val:
            pre, cur = cur, cur.right
        node = TreeNode(val)
        node.left = cur
        if pre:
            pre.right = node
        return root if root.val > val else node


def test():
    from python3 import Codec
    serialized_trees = [
        "[4,1,3,null,null,2]",
        "[5,2,4,null,1]",
        ]
    values = [5, 3]
    expectations = [
        "[5,4,null,1,3,null,null,2]",
        "[5,2,4,null,1,null,3]",
        ]
    for serialized_tree, val, expected in zip(serialized_trees, values, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().insertIntoMaxTree(root, val)
        assert solution == Codec.deserialize(expected)
