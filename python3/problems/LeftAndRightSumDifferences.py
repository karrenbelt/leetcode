### Source : https://leetcode.com/problems/left-and-right-sum-differences/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# Given a 0-indexed integer array nums, find a 0-indexed integer array answer where:
# 
#     answer.length == nums.length.
#     answer[i] = |leftSum[i] - rightSum[i]|.
# 
# Where:
# 
#     leftSum[i] is the sum of elements to the left of the index i in the array nums. If there is no 
# such element, leftSum[i] = 0.
#     rightSum[i] is the sum of elements to the right of the index i in the array nums. If there is 
# no such element, rightSum[i] = 0.
# 
# Return the array answer.
# 
# Example 1:
# 
# Input: nums = [10,4,8,3]
# Output: [15,1,11,22]
# Explanation: The array leftSum is [0,10,14,22] and the array rightSum is [15,11,3,0].
# The array answer is [|0 - 15|,|10 - 11|,|14 - 3|,|22 - 0|] = [15,1,11,22].
# 
# Example 2:
# 
# Input: nums = [1]
# Output: [0]
# Explanation: The array leftSum is [0] and the array rightSum is [0].
# The array answer is [|0 - 0|] = [0].
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	1 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def leftRigthDifference(self, nums: List[int]) -> List[int]:
        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in nums]
        suffix_sum = (suffix := 0) or [suffix := suffix + n for n in nums[::-1]]
        return [abs(a - b) for a, b in zip(prefix_sum, reversed(suffix_sum))]

    def leftRigthDifference(self, nums: List[int]) -> List[int]:
        from itertools import accumulate
        prefix, suffix = accumulate(nums), accumulate(reversed(nums))
        return [abs(a - b) for a, b in zip(prefix, reversed(list(suffix)))]

    def leftRigthDifference(self, nums: List[int]) -> List[int]:
        from operator import sub
        from itertools import starmap
        prefix, suffix = ([0] * len(nums) for _ in range(2))
        for i in range(len(nums)):
            prefix[i] += nums[i] + (prefix[i - 1] if i else 0)
            suffix[~i] += nums[~i] + (suffix[(~i) + 1] if i < len(nums) else 0)
        return list(map(abs, starmap(sub, zip(prefix, suffix))))

    def leftRigthDifference(self, nums: List[int]) -> List[int]:
        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in nums]
        ans = [0] + prefix_sum
        return [abs(prefix - ans[i + 1] - ans[i]) for i in range(len(ans) - 1)]


def test():
    arguments = [
        [10, 4, 8, 3],
        [1],
    ]
    expectations = [
        [15, 1, 11, 22],
        [0]
    ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().leftRigthDifference(n)
        assert solution == expected
    arguments = [
        [10, 4, 8, 3],
        [1],
    ]
    expectations = [
        [15, 1, 11, 22],
        [0]
    ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().leftRigthDifference(n)
        assert solution == expected
