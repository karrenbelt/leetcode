### Source : https://leetcode.com/problems/check-if-a-string-can-break-another-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-25

##################################################################################################### 
#
# Given two strings: s1 and s2 with the same size, check if some permutation of string s1 can break 
# some permutation of string s2 or vice-versa. In other words s2 can break s1 or vice-versa.
# 
# A string x can break string y (both of size n) if x[i] >= y[i] (in alphabetical order) for all i 
# between 0 and n-1.
# 
# Example 1:
# 
# Input: s1 = "abc", s2 = "xya"
# Output: true
# Explanation: "ayx" is a permutation of s2="xya" which can break to string "abc" which is a 
# permutation of s1="abc".
# 
# Example 2:
# 
# Input: s1 = "abe", s2 = "acd"
# Output: false 
# Explanation: All permutations for s1="abe" are: "abe", "aeb", "bae", "bea", "eab" and "eba" and all 
# permutation for s2="acd" are: "acd", "adc", "cad", "cda", "dac" and "dca". However, there is not 
# any permutation from s1 which can break some permutation from s2 and vice-versa.
# 
# Example 3:
# 
# Input: s1 = "leetcodee", s2 = "interview"
# Output: true
# 
# Constraints:
# 
# 	s1.length == n
# 	s2.length == n
# 	1 <= n <= 105
# 	All strings consist of lowercase English letters.
#####################################################################################################


class Solution:
    def checkIfCanBreak(self, s1: str, s2: str) -> bool:  # O(n log n) time and O(n) space: 224ms
        pairs = list(zip(sorted(s1), sorted(s2)))
        return all(c1 >= c2 for c1, c2 in pairs) or all(c1 <= c2 for c1, c2 in pairs)

    def checkIfCanBreak(self, s1: str, s2: str) -> bool:  # O(n log n) time and O(n) space: 196ms
        return all(x <= y for x, y in zip(*sorted([sorted(s1), sorted(s2)])))

    def checkIfCanBreak(self, s1: str, s2: str) -> bool:  # O(n log n) time and O(n) space: 184ms
        pairs = zip(sorted(s1), sorted(s2))
        ctr1 = ctr2 = 0
        for c1, c2 in pairs:
            if c1 == c2:
                ctr1 += 1
                ctr2 += 1
            elif c1 > c2:
                ctr1 += 1
            else:
                ctr2 += 1
        return len(s1) == ctr1 or len(s2) == ctr2


def test():
    arguments = [
        ("abc", "xya"),
        ("abe", "acd"),
        ("leetcodee", "interview"),
    ]
    expectations = [True, False, True]
    for (s1, s2), expected in zip(arguments, expectations):
        solution = Solution().checkIfCanBreak(s1, s2)
        assert solution == expected
