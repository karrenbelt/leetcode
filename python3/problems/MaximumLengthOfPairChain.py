### Source : https://leetcode.com/problems/maximum-length-of-pair-chain/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-21

##################################################################################################### 
#
# You are given an array of n pairs pairs where pairs[i] = [lefti, righti] and lefti < righti.
# 
# A pair p2 = [c, d] follows a pair p1 = [a, b] if b < c. A chain of pairs can be formed in this 
# fashion.
# 
# Return the length longest chain which can be formed.
# 
# You do not need to use up all the given intervals. You can select pairs in any order.
# 
# Example 1:
# 
# Input: pairs = [[1,2],[2,3],[3,4]]
# Output: 2
# Explanation: The longest chain is [1,2] -> [3,4].
# 
# Example 2:
# 
# Input: pairs = [[1,2],[7,8],[4,5]]
# Output: 3
# Explanation: The longest chain is [1,2] -> [4,5] -> [7,8].
# 
# Constraints:
# 
# 	n == pairs.length
# 	1 <= n <= 1000
# 	-1000 <= lefti < righti < 1000
#####################################################################################################

from typing import List


class Solution:
    def findLongestChain(self, pairs: List[List[int]]) -> int:
        pairs.sort()
        ctr, last = 1, pairs[0]
        for pair in pairs[1:]:
            if last[1] > pair[1]:  # replace previous
                last = pair
            elif pair[0] > last[1]:  # extend
                ctr += 1
                last = pair
        return ctr

    def findLongestChain(self, pairs: List[List[int]]) -> int:
        pairs.sort()
        ctr, last = 1, pairs[0]
        for pair in pairs[1:]:
            last, ctr = (pair, ctr) if last[1] > pair[1] else (pair, ctr + 1) if pair[0] > last[1] else (last, ctr)
        return ctr

    def findLongestChain(self, pairs: List[List[int]]) -> int:
        ctr, prev = 1, 2 ** 32 - 1
        for chain in sorted(pairs):
            ctr, prev = (ctr + 1, chain[1]) if chain[0] > prev else (ctr, min(prev, chain[1]))
        return ctr


def test():
    arguments = [
        [[1, 2], [2, 3], [3, 4]],
        [[1, 2], [7, 8], [4, 5]],
        [[-10, -8], [8, 9], [-5, 0], [6, 10], [-6, -4], [1, 7], [9, 10], [-4, 7]],
    ]
    expectations = [2, 3, 4]
    for pairs, expected in zip(arguments, expectations):
        solution = Solution().findLongestChain(pairs)
        assert solution == expected
