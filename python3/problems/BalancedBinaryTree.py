### Source : https://leetcode.com/problems/balanced-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-12

##################################################################################################### 
#
# Given a binary tree, determine if it is height-balanced.
# 
# For this problem, a height-balanced binary tree is defined as:
# 
# a binary tree in which the left and right subtrees of every node differ in height by no more than 1.
# 
# Example 1:
# 
# Input: root = [3,9,20,null,null,15,7]
# Output: true
# 
# Example 2:
# 
# Input: root = [1,2,2,3,3,null,null,4,4]
# Output: false
# 
# Example 3:
# 
# Input: root = []
# Output: true
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 5000].
# 	-104 <= Node.val <= 104
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isBalanced(self, root: TreeNode) -> bool:  # top-down O(n log n); too many recursive calls

        def traverse(node: TreeNode) -> int:
            if not node:
                return 0
            return max(traverse(node.left), traverse(node.right)) + 1

        return (-1 <= traverse(root.left) - traverse(root.right) <= 1
                and self.isBalanced(root.left) and self.isBalanced(root.right)
                if root else True)

    def isBalanced(self, root: TreeNode) -> bool:  # bottom-up, single pass: O(n)

        def find_height(node: TreeNode) -> int:
            if not node:
                return 0
            height_left = find_height(node.left)  # can return -1 here if you find it without checking right: faster
            height_right = find_height(node.right)
            if abs(height_left - height_right) > 1 or height_left == -1 or height_right == -1:
                return -1
            return max(height_left, height_right) + 1

        return find_height(root) != -1

    def isBalanced(self, root: TreeNode) -> bool:  # single pass: O(n), break out early if imbalanced

        def check(node: TreeNode) -> int:
            if not node:
                return 0
            left = check(node.left)
            if left == -1:
                return -1
            right = check(node.right)
            if right == -1:
                return -1
            if abs(left - right) > 1:
                return -1
            return max(left, right) + 1

        return check(root) != -1

    def isBalanced(self, root: TreeNode) -> bool:  # iterative post-order dfs using a hash map
        if not root:
            return True

        height_dict = {None: 0}
        stack = [root]
        while stack:
            node = stack.pop()
            if node.left not in height_dict or node.right not in height_dict:
                stack.extend(filter(None, [node, node.left, node.right]))
            else:
                left = height_dict[node.left]
                right = height_dict[node.right]
                if abs(left - right) > 1:
                    return False
                else:
                    height_dict[node] = max(left, right) + 1
        return True


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3,9,20,null,null,15,7]",
        "[1,2,2,3,3,null,null,4,4]",
        "[]",
        ]
    expectations = [True, False, True]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().isBalanced(root)
        assert solution == expected
