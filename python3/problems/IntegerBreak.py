### Source : https://leetcode.com/problems/integer-break/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-01

##################################################################################################### 
#
# Given an integer n, break it into the sum of k positive integers, where k >= 2, and maximize the 
# product of those integers.
# 
# Return the maximum product you can get.
# 
# Example 1:
# 
# Input: n = 2
# Output: 1
# Explanation: 2 = 1 + 1, 1 &times; 1 = 1.
# 
# Example 2:
# 
# Input: n = 10
# Output: 36
# Explanation: 10 = 3 + 3 + 4, 3 &times; 3 &times; 4 = 36.
# 
# Constraints:
# 
# 	2 <= n <= 58
#####################################################################################################


class Solution:
    def integerBreak(self, n: int) -> int:  # O(n) time and O(n) space
        dp = [0, 0, 1, 2, 4, 6, 9]
        for i in range(7, n + 1):
            dp.append(3 * dp[i - 3])
        return dp[-1] if n > 6 else dp[n]

    def integerBreak(self, n: int) -> int:  # O(n) time and O(1) space
        dp = [0, 0, 1, 2, 4, 6, 9]
        for i in range(7, n + 1):
            idx = ((i - 4) % 3 - 3)
            dp[idx] = (3 * dp[idx])
        return dp[((n - 4) % 3 - 3)] if n > 6 else dp[n]

    def integerBreak(self, n: int) -> int:  # O(log n) time and O(1) space
        if n == 2 or n == 3:
            return n - 1
        if n % 3 == 0:
            return 3**(n//3)
        if n % 3 == 1:
            return 3**(n//3 - 1)*4
        if n % 3 == 2:
            return 3**(n//3)*2

    def integerBreak(self, n: int) -> int:
        return n - 1 if n in {2, 3} else 3**(n//3) if n % 3 == 0 else 3**(n//3 - 1)*4 if n % 3 == 1 else 3**(n//3)*2

    def integerBreak(self, n: int) -> int:  # O(log n) time and O(1) space
        return n - 1 if n <= 3 else (3**(n//3), 3**(n//3 - 1)*4, 3**(n//3)*2)[n % 3]


def test():
    arguments = [2, 10, 7, 8, 9, 11, 12]
    expectations = [1, 36, 12, 18, 27, 54, 81]
    for n, expected in zip(arguments, expectations):
        solution = Solution().integerBreak(n)
        assert solution == expected, (n, solution)
