### Source : https://leetcode.com/problems/maximum-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-14

##################################################################################################### 
#
# You are given an integer array nums with no duplicates. A maximum binary tree can be built 
# recursively from nums using the following algorithm:
# 
# 	Create a root node whose value is the maximum value in nums.
# 	Recursively build the left subtree on the subarray prefix to the left of the maximum value.
# 	Recursively build the right subtree on the subarray suffix to the right of the maximum 
# value.
# 
# Return the maximum binary tree built from nums.
# 
# Example 1:
# 
# Input: nums = [3,2,1,6,0,5]
# Output: [6,3,5,null,2,0,null,null,1]
# Explanation: The recursive calls are as follow:
# - The largest value in [3,2,1,6,0,5] is 6. Left prefix is [3,2,1] and right suffix is [0,5].
#     - The largest value in [3,2,1] is 3. Left prefix is [] and right suffix is [2,1].
#         - Empty array, so no child.
#         - The largest value in [2,1] is 2. Left prefix is [] and right suffix is [1].
#             - Empty array, so no child.
#             - Only one element, so child is a node with value 1.
#     - The largest value in [0,5] is 5. Left prefix is [0] and right suffix is [].
#         - Only one element, so child is a node with value 0.
#         - Empty array, so no child.
# 
# Example 2:
# 
# Input: nums = [3,2,1]
# Output: [3,null,2,null,1]
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	0 <= nums[i] <= 1000
# 	All integers in nums are unique.
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def constructMaximumBinaryTree(self, nums: List[int]) -> TreeNode:  # O(n^2) time and O(n) space

        def traverse(i: int, j: int) -> TreeNode:
            if i == j:
                return
            node = TreeNode(max(nums[i:j]))  # slicing is dumb
            index = nums.index(node.val)
            node.left = traverse(i, index)
            node.right = traverse(index + 1, j)
            return node

        return traverse(0, len(nums))

    def constructMaximumBinaryTree(self, nums: List[int]) -> TreeNode:  # O(n) time and O(n) space
        # monotone stack
        stack = [TreeNode(1 << 31)]
        for n in nums:
            node = TreeNode(n)
            while stack[-1].val < n:
                node.left = stack.pop()
            stack[-1].right = node
            stack.append(node)
        return stack[0].right


def test():
    from python3 import Codec
    arguments = [
        [3, 2, 1, 6, 0, 5],
        [3, 2, 1],
    ]
    expectations = [
        "[6,3,5,null,2,0,null,null,1]",
        "[3,null,2,null,1]",
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().constructMaximumBinaryTree(nums)
        assert solution == Codec.deserialize(expected)
