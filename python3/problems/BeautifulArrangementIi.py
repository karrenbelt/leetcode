### Source : https://leetcode.com/problems/beautiful-arrangement-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-12

##################################################################################################### 
#
# 
# Given two integers n and k, you need to construct a list which contains n different positive 
# integers ranging from 1 to n and obeys the following requirement: 
# 
# Suppose this list is [a1, a2, a3, ... , an], then the list [|a1 - a2|, |a2 - a3|, |a3 - a4|, ... , 
# |an-1 - an|] has exactly k distinct integers.
# 
# If there are multiple answers, print any of them.
# 
# Example 1:
# 
# Input: n = 3, k = 1
# Output: [1, 2, 3]
# Explanation: The [1, 2, 3] has three different positive integers ranging from 1 to 3, and the [1, 
# 1] has exactly 1 distinct integer: 1.
# 
# Example 2:
# 
# Input: n = 3, k = 2
# Output: [1, 3, 2]
# Explanation: The [1, 3, 2] has three different positive integers ranging from 1 to 3, and the [2, 
# 1] has exactly 2 distinct integers: 1 and 2.
# 
# Note:
# 
# The n and k are in the range 1 4.
# 
#####################################################################################################

from typing import List


class Solution:
    def constructArray(self, n: int, k: int) -> List[int]:  # O(n) time
        array = list(range(1, n + 1))
        for i in range(1, k):
            array[i:] = array[:i - 1:-1]
        return array

    def constructArray(self, n: int, k: int) -> List[int]:  # O(n) time
        answer = [1]
        sign = 1
        for diff in reversed(range(1, k + 1)):
            answer.append(answer[-1] + sign * diff)
            sign = -1 if sign == 1 else 1
        return answer + list(range(k + 2, n + 1))


def test():
    arguments = [
        (3, 1),
        (3, 2),
        (5, 2),
        (10, 4)
        ]
    expectations = [
        [1, 2, 3],
        [1, 3, 2],
        [1, 5, 4, 3, 2],
        [1],
        ]
    for (n, k), expected in zip(arguments, expectations):
        solution = Solution().constructArray(n, k)
        assert len(set(abs(a - b) for a, b in zip(solution, solution[1:]))) == k
        # assert solution == expected
