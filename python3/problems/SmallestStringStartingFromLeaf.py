### Source : https://leetcode.com/problems/smallest-string-starting-from-leaf/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-22

##################################################################################################### 
#
# You are given the root of a binary tree where each node has a value in the range [0, 25] 
# representing the letters 'a' to 'z'.
# 
# Return the lexicographically smallest string that starts at a leaf of this tree and ends at the 
# root.
# 
# As a reminder, any shorter prefix of a string is lexicographically smaller.
# 
# 	For example, "ab" is lexicographically smaller than "aba".
# 
# A leaf of a node is a node that has no children.
# 
# Example 1:
# 
# Input: root = [0,1,2,3,4,3,4]
# Output: "dba"
# 
# Example 2:
# 
# Input: root = [25,1,3,1,3,0,2]
# Output: "adz"
# 
# Example 3:
# 
# Input: root = [2,2,1,null,1,0,null,0]
# Output: "abc"
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 8500].
# 	0 <= Node.val <= 25
#####################################################################################################

from typing import Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def smallestFromLeaf(self, root: Optional[TreeNode]) -> str:  # O(n) time: 62 ms

        def traverse(node: TreeNode, path: str):
            nonlocal smallest
            if node:
                if not node.left and not node.right:
                    smallest = min(smallest, chr(node.val + ord("a")) + path)
                traverse(node.left, chr(node.val + ord("a")) + path)
                traverse(node.right, chr(node.val + ord("a")) + path)

        smallest = chr(ord("z") + 1)
        traverse(root, "")
        return smallest

    def smallestFromLeaf(self, root: Optional[TreeNode]) -> str:  # O(n) time: 59 ms

        def traverse(node: TreeNode, path: str):
            if node:
                if not node.left and not node.right:
                    return chr(node.val + ord("a")) + path
                left = traverse(node.left, chr(node.val + ord("a")) + path)
                right = traverse(node.right, chr(node.val + ord("a")) + path)
                return min(left, right)
            return chr(ord("z") + 1)

        return traverse(root, "")

    def smallestFromLeaf(self, root: Optional[TreeNode]) -> str:  # O(n) time: 40 ms

        def dfs(node: TreeNode, path: str):
            if not node or node.left is node.right:
                return '{' if not node else alphabet[node.val] + path
            return min(dfs(node.left, alphabet[node.val] + path), dfs(node.right, alphabet[node.val] + path))

        alphabet = 'abcdefghijklmnopqrstuvwxyz'  # can use string.ascii_lowercase as well
        return dfs(root, "")

    def smallestFromLeaf(self, root: Optional[TreeNode]) -> str:  # O(n) time: 43 ms
        smallest, stack, alphabet = '{', [(root, "")], 'abcdefghijklmnopqrstuvwxyz'
        while stack:
            node, path = stack.pop()
            path = alphabet[node.val] + path
            if not node.left and not node.right:
                smallest = min(smallest, path)
            if node.left:
                stack.append((node.left, path))
            if node.right:
                stack.append((node.right, path))
        return smallest

    def smallestFromLeaf(self, root: Optional[TreeNode]) -> str:  # O(n) time: 56 ms
        smallest, stack, alphabet = '{', [(root, "")], 'abcdefghijklmnopqrstuvwxyz'
        while stack:
            node, path = stack.pop()
            path = alphabet[node.val] + path
            if not node.left and not node.right:
                smallest = min(smallest, path)
            stack.extend((child, path) for child in filter(None, (node.left, node.right)))
        return smallest


def test():
    from python3 import Codec
    arguments = [
        "[0,1,2,3,4,3,4]",
        "[25,1,3,1,3,0,2]",
        "[2,2,1,null,1,0,null,0]",
        "[25,1,null,0,0,1,null,null,null,0]",
    ]
    expectations = ["dba", "adz", "abc", "ababz"]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().smallestFromLeaf(root)
        assert solution == expected
