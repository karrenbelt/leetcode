### Source : https://leetcode.com/problems/bus-routes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-26

##################################################################################################### 
#
# You are given an array routes representing bus routes where routes[i] is a bus route that the ith 
# bus repeats forever.
# 
# 	For example, if routes[0] = [1, 5, 7], this means that the 0th bus travels in the sequence 
# 1 -> 5 -> 7 -> 1 -> 5 -> 7 -> 1 -> ... forever.
# 
# You will start at the bus stop source (You are not on any bus initially), and you want to go to the 
# bus stop target. You can travel between bus stops by buses only.
# 
# Return the least number of buses you must take to travel from source to target. Return -1 if it is 
# not possible.
# 
# Example 1:
# 
# Input: routes = [[1,2,7],[3,6,7]], source = 1, target = 6
# Output: 2
# Explanation: The best strategy is take the first bus to the bus stop 7, then take the second bus to 
# the bus stop 6.
# 
# Example 2:
# 
# Input: routes = [[7,12],[4,5,15],[6],[15,19],[9,12,13]], source = 15, target = 12
# Output: -1
# 
# Constraints:
# 
# 	1 <= routes.length <= 500.
# 	1 <= routes[i].length <= 105
# 	All the values of routes[i] are unique.
# 	sum(routes[i].length) <= 105
# 	0 <= routes[i][j] < 106
# 	0 <= source, target < 106
#####################################################################################################

from typing import List


class Solution:
    def numBusesToDestination(self, routes: List[List[int]], source: int, target: int) -> int:  # O(m + n) time
        # graph with bfs. Note that it's not the shortest path in terms of bus stops, but of busses taken
        if source == target:
            return 0

        # we need a mapping bus -> stop, and a mapping stop -> bus
        bus_stations, departing_busses, queue = {}, {}, []
        for bus, stations in enumerate(routes):
            bus_stations[bus] = set(stations)
            for node in stations:
                departing_busses.setdefault(node, set()).add(bus)
            if source in bus_stations[bus]:
                queue.append(bus)

        busses_taken = 1
        seen = set(queue)
        while queue:
            new_queue = []
            if target in set().union(*(bus_stations[bus] for bus in queue)):  # station on one of the bus lines
                return busses_taken
            busses_taken += 1
            for bus in queue:
                next_busses = set().union(*(departing_busses.get(station, []) for station in bus_stations.get(bus, [])))
                for next_bus in next_busses:
                    if next_bus not in seen:
                        seen.add(next_bus)
                        new_queue.append(next_bus)
            queue = new_queue
        return -1


def test():
    arguments = [
        ([[1, 2, 7], [3, 6, 7]], 1, 6),
        ([[7, 12], [4, 5, 15], [6], [15, 19], [9, 12, 13]], 15, 12),
        ([[2], [2, 8]], 8, 2),
        ([[1, 9, 12, 20, 23, 24, 35, 38], [10, 21, 24, 31, 32, 34, 37, 38, 43], [10, 19, 28, 37], [8], [14, 19],
          [11, 17, 23, 31, 41, 43, 44], [21, 26, 29, 33], [5, 11, 33, 41], [4, 5, 8, 9, 24, 44]],
         37, 28),
    ]
    expectations = [2, -1, 1, 1]
    for (routes, source, target), expected in zip(arguments, expectations):
        solution = Solution().numBusesToDestination(routes, source, target)
        assert solution == expected
