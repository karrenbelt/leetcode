### Source : https://leetcode.com/problems/number-of-provinces/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# There are n cities. Some of them are connected, while some are not. If city a is connected directly 
# with city b, and city b is connected directly with city c, then city a is connected indirectly with 
# city c.
# 
# A province is a group of directly or indirectly connected cities and no other cities outside of the 
# group.
# 
# You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth 
# city are directly connected, and isConnected[i][j] = 0 otherwise.
# 
# Return the total number of provinces.
# 
# Example 1:
# 
# Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]]
# Output: 2
# 
# Example 2:
# 
# Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]]
# Output: 3
# 
# Constraints:
# 
# 	1 <= n <= 200
# 	n == isConnected.length
# 	n == isConnected[i].length
# 	isConnected[i][j] is 1 or 0.
# 	isConnected[i][i] == 1
# 	isConnected[i][j] == isConnected[j][i]
#####################################################################################################

from typing import List


class Solution:
    def findCircleNum(self, M: List[List[int]]) -> int:  # O(n^2) time and O(n) space

        def dfs(node):
            for nei, adj in enumerate(M[node]):
                if adj and nei not in seen:
                    seen.add(nei)
                    dfs(nei)

        seen = set()
        provinces = 0
        for i in range(len(M)):
            if i not in seen:
                dfs(i)
                provinces += 1
        return provinces

    def findCircleNum(self, M: List[List[int]]) -> int:  # iterative dfs
        provinces, seen = 0, set()
        for i in range(len(M)):
            if i in seen:
                continue
            stack = [i]
            while stack:
                node = stack.pop()
                if node not in seen:
                    seen.add(node)
                    stack = [j for j, v in enumerate(M[node]) if v and j not in seen] + stack
            provinces += 1
        return provinces

    def findCircleNum(self, M: List[List[int]]) -> int:  # union find

        def union(i: int, j: int):
            parents[get_parent(i)] = get_parent(j)

        def get_parent(i: int):
            while not parents[i] == i:
                parents[i] = parents[parents[i]]  # compress
                i = parents[i]
            return i

        parents = list(range(len(M)))
        for i in range(len(M)):
            for j in range(i + 1, len(M)):
                if M[i][j]:
                    union(i, j)
        return sum(i == parent for i, parent in enumerate(parents))


def test():
    arguments = [
        [[1, 1, 0], [1, 1, 0], [0, 0, 1]],
        [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
        ]
    expectations = [2, 3]
    for M, expected in zip(arguments, expectations):
        solution = Solution().findCircleNum(M)
        assert solution == expected
