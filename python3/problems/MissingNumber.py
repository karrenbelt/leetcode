### Source : https://leetcode.com/problems/missing-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is 
# missing from the array.
# 
# Example 1:
# 
# Input: [3,0,1]
# Output: 2
# 
# Example 2:
# 
# Input: [9,6,4,2,3,5,7,0,1]
# Output: 8
# 
# Note:
# Your algorithm should run in linear runtime complexity. Could you implement it using only constant 
# extra space complexity?
#####################################################################################################

from typing import List


class Solution:
    def missingNumber(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        missing = set(range(len(nums))).difference(nums)
        if missing:
            return missing.pop()
        return len(nums)

    def missingNumber(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        missing = 0
        for i, n in enumerate(nums):
            missing ^= i ^ n
        return missing ^ len(nums)

    def missingNumber(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        from functools import reduce
        return reduce(lambda x, y: x ^ y, nums + list(range(len(nums))), len(nums))

    def missingNumber(self, nums: List[int]) -> int:  # cyclic sort: O(n log n) time
        i = 0
        while i < len(nums):
            n = nums[i]
            if n >= len(nums) or n == i:
                i += 1
            else:
                nums[i], nums[n] = nums[n], nums[i]
        return next((i for i in range(len(nums)) if nums[i] != i), len(nums))


def test():
    arrays_of_numbers = [
        [3, 0, 1],
        [0, 1],
        [9, 6, 4, 2, 3, 5, 7, 0, 1],
        [0],
        ]
    expectations = [2, 2, 8, 1]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().missingNumber(nums)
        assert solution == expected
