### Source : https://leetcode.com/problems/stickers-to-spell-word/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-12

##################################################################################################### 
#
# We are given n different types of stickers. Each sticker has a lowercase English word on it.
# 
# You would like to spell out the given string target by cutting individual letters from your 
# collection of stickers and rearranging them. You can use each sticker more than once if you want, 
# and you have infinite quantities of each sticker.
# 
# Return the minimum number of stickers that you need to spell out target. If the task is impossible, 
# return -1.
# 
# Note: In all test cases, all words were chosen randomly from the 1000 most common US English words, 
# and target was chosen as a concatenation of two random words.
# 
# Example 1:
# 
# Input: stickers = ["with","example","science"], target = "thehat"
# Output: 3
# Explanation:
# We can use 2 "with" stickers, and 1 "example" sticker.
# After cutting and rearrange the letters of those stickers, we can form the target "thehat".
# Also, this is the minimum number of stickers necessary to form the target string.
# 
# Example 2:
# 
# Input: stickers = ["notice","possible"], target = "basicbasic"
# Output: -1
# Explanation:
# We cannot form the target "basicbasic" from cutting letters from the given stickers.
# 
# Constraints:
# 
# 	n == stickers.length
# 	1 <= n <= 50
# 	1 <= stickers[i].length <= 10
# 	1 <= target <= 15
# 	stickers[i] and target consist of lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def minStickers(self, stickers: List[str], target: str) -> int:  # TLE -> 40 / 101 test cases passed.
        from collections import Counter

        target = Counter(target)
        stickers = list(filter(None, (Counter(s) & target for s in stickers)))
        queue = [(target, 0)]
        seen = set()
        while queue:
            new_queue = []
            for remainder, ctr in queue:
                for s in stickers:
                    new_target = remainder - s
                    key = tuple(sorted(new_target.items()))
                    if key not in seen:
                        seen.add(key)
                        if not new_target:
                            return ctr + 1
                        if new_target != remainder:
                            new_queue.append((new_target, ctr + 1))
            queue = new_queue
        return -1


def test():
    arguments = [
        (["with", "example", "science"], "thehat"),
        (["notice", "possible"], "basicbasic"),
        (["and", "pound", "force", "human", "fair", "back", "sign", "course", "sight", "world", "close", "saw", "best",
          "fill", "late", "silent", "open", "noon", "seat", "cell", "take", "between", "it", "hundred", "hat", "until",
          "either", "play", "triangle", "stay", "separate", "season", "tool", "direct", "part", "student", "path",
          "ear", "grow", "ago", "main", "was", "rule", "element", "thing", "place", "common", "led", "support", "mean"],
         "quietchord"
         )
    ]
    expectations = [3, -1, -1]
    for (stickers, target), expected in zip(arguments, expectations):
        solution = Solution().minStickers(stickers, target)
        assert solution == expected
