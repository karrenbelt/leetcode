### Source : https://leetcode.com/problems/domino-and-tromino-tiling/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-10

##################################################################################################### 
#
# You have two types of tiles: a 2 x 1 domino shape and a tromino shape. You may rotate these shapes.
# 
# Given an integer n, return the number of ways to tile an 2 x n board. Since the answer may be very 
# large, return it modulo 109 + 7.
# 
# In a tiling, every square must be covered by a tile. Two tilings are different if and only if there 
# are two 4-directionally adjacent cells on the board such that exactly one of the tilings has both 
# squares occupied by a tile.
# 
# Example 1:
# 
# Input: n = 3
# Output: 5
# Explanation: The five different ways are show above.
# 
# Example 2:
# 
# Input: n = 1
# Output: 1
# 
# Constraints:
# 
# 	1 <= n <= 1000
#####################################################################################################


class Solution:
    def numTilings(self, n: int) -> int:  # O(n) time and O(n) space
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dp(i: int, full=True):
            if i in base_case[full]:
                return base_case[full][i]
            elif full:
                return dp(i - 2) + dp(i - 1) + dp(i - 1, False)
            return 2 * dp(i - 2) + dp(i - 1, False)

        base_case = {True: {1: 1, 2: 2, 3: 5}, False: {1: 0, 2: 2, 3: 4}}
        return dp(n) % (10 ** 9 + 7)

    def numTilings(self, n: int) -> int:  # O(n) time and O(n) space
        # double recursive sequence
        # dominos  = [0, 1, 1, 2, 4, 9, ...]
        # trominos = [1, 1, 2, 11, 24, 53, ...]
        dominos = [0] * (n + 1)
        trominos = [1, 1] + [0] * (n - 1)
        for i in range(2, n + 1):
            dominos[i] = trominos[i - 2] + dominos[i - 1]
            trominos[i] = trominos[i - 1] + trominos[i - 2] + dominos[i - 1] * 2
        return trominos[n] % (10**9 + 7)

    def numTilings(self, n: int) -> int:  # O(n) time and O(n) space
        # dp[n] = dp[n-1] + dp[n-2] + 2 * (dp[n-3] + ... + dp[0])
        # dp[n-1] = dp[n-2] + dp[n-3] + 2 * (dp[n-4] + ... + dp[0])
        # then
        # dp[n] - dp[n-1] = dp[n-1] + dp[n-3] --> dp[n] = 2*dp[n-1] + dp[n-3]
        dp = [0, 1, 2, 5] + [0] * (n - 3)
        for i in range(4, n + 1):
            dp[i] = 2 * dp[i - 1] + dp[i - 3]
        return dp[n]

    def numTilings(self, n: int) -> int:  # O(n) time and O(1) space
        p1, p2, p3 = 1, 0, -1
        for _ in range(1, n + 1):
            p1, p2, p3 = 2 * p1 + p3, p1, p2
        return p1 % (10**9 + 7)


def test():
    arguments = [3, 1, 2, 5]
    expectations = [5, 1, 2, 24]
    for n, expected in zip(arguments, expectations):
        solution = Solution().numTilings(n)
        assert solution == expected, (n, solution, expected)
