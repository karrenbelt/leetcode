### Source : https://leetcode.com/problems/jump-game-v/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-11

##################################################################################################### 
#
# Given an array of integers arr and an integer d. In one step you can jump from index i to index:
# 
# 	i + x where: i + x < arr.length and  0 < x <= d.
# 	i - x where: i - x >= 0 and  0 < x <= d.
# 
# In addition, you can only jump from index i to index j if arr[i] > arr[j] and arr[i] > arr[k] for 
# all indices k between i and j (ore formally min(i, j) < k < max(i, j)).
# 
# You can choose any index of the array and start jumping. Return the maximum number of indices you 
# can visit.
# 
# Notice that you can not jump outside of the array at any time.
# 
# Example 1:
# 
# Input: arr = [6,4,14,6,8,13,9,7,10,6,12], d = 2
# Output: 4
# Explanation: You can start at index 10. You can jump 10 --> 8 --> 6 --> 7 as shown.
# Note that if you start at index 6 you can only jump to index 7. You cannot jump to index 5 because 
# 13 > 9. You cannot jump to index 4 because index 5 is between index 4 and 6 and 13 > 9.
# Similarly You cannot jump from index 3 to index 2 or index 1.
# 
# Example 2:
# 
# Input: arr = [3,3,3,3,3], d = 3
# Output: 1
# Explanation: You can start at any index. You always cannot jump to any index.
# 
# Example 3:
# 
# Input: arr = [7,6,5,4,3,2,1], d = 1
# Output: 7
# Explanation: Start at index 0. You can visit all the indicies. 
# 
# Example 4:
# 
# Input: arr = [7,1,7,1,7,1], d = 2
# Output: 2
# 
# Example 5:
# 
# Input: arr = [66], d = 1
# Output: 1
# 
# Constraints:
# 
# 	1 <= arr.length <= 1000
# 	1 <= arr[i] <= 105
# 	1 <= d <= arr.length
#####################################################################################################

from typing import List


class Solution:
    def maxJumps(self, arr: List[int], d: int) -> int:  # top down: time O(n * d), space O(n)
        # For each step arr[i], check arr[j] on its left and right, until it meet the bound or meet the bigger step.
        def top_down_dp(i: int) -> int:
            if paths[i]:
                return paths[i]
            paths[i] = 1
            for step in (-1, 1):
                for j in range(i + step, i + d * step + step, step):
                    if not (0 <= j < len(arr) and arr[j] < arr[i]):
                        break
                    paths[i] = max(paths[i], top_down_dp(j) + 1)
            return paths[i]

        paths = [0] * len(arr)
        return max(map(top_down_dp, range(len(arr))))

    def maxJumps(self, arr: List[int], d: int) -> int:  # bottom up: time O(n * log(n) + n * d), space O(n)
        # We can only jump lower, and one step needs the result from its lower step.
        # So we sort A[i] do the dp starting from the smallest.
        # For each A[i], we check the lower step on the left and right.
        # This process is O(D) on both side.
        paths = [1] * len(arr)
        for a, i in sorted([a, i] for i, a in enumerate(arr)):
            for step in (-1, 1):
                for j in range(i + step, i + d * step + step, step):
                    if not (0 <= j < len(arr) and arr[j] < arr[i]):
                        break
                    paths[i] = max(paths[i], paths[j] + 1)
        return max(paths)

    def maxJumps(self, arr: List[int], d: int) -> int:  # O(n), O(n)
        # decreasing stack + dp
        # Use a stack to keep the index of decreasing elements.
        # Iterate the array A. When we meet a bigger element a,
        # we pop out the all indices j from the top of stack, where A[j] have the same value.
        # Then we update dp[i] and dp[stack[-1]].
        # Since all indices will be pushed and popped in stack,
        # appended and iterated in L once,
        # the whole complexity is guaranteed to be O(N).
        # All element are pushed in and popped from stack1&2 only once, strict O(N) time.
        path = [1] * (len(arr) + 1)
        stack = []
        for i, a in enumerate(arr + [float('inf')]):
            while stack and arr[stack[-1]] < a:
                L = [stack.pop()]  # second stack
                while stack and arr[stack[-1]] == arr[L[0]]:
                    L.append(stack.pop())
                for j in L:
                    if i - j <= d:
                        path[i] = max(path[i], path[j] + 1)
                    if stack and j - stack[-1] <= d:
                        path[stack[-1]] = max(path[stack[-1]], path[j] + 1)
            stack.append(i)
        return max(path[:-1])

    def maxJumps(self, arr: List[int], d: int) -> int:
        cache = {}

        def jump(i):
            if i in cache:
                return cache[i]
            res = 0
            for direction in [-1, 1]:
                for x in range(1, d + 1):
                    j = i + x * direction
                    if 0 <= j < len(arr) and arr[j] < arr[i]:
                        res = max(res, jump(j))
                    else:
                        break
            cache[i] = res + 1
            return res + 1

        return max(jump(i) for i in range(len(arr)))


def test():
    arguments = [
        ([6, 4, 14, 6, 8, 13, 9, 7, 10, 6, 12], 2),
        ([3, 3, 3, 3, 3], 3),
        ([7, 6, 5, 4, 3, 2, 1], 1),
        ([7, 1, 7, 1, 7, 1], 2),
        ([66], 1),
    ]
    expectations = [4, 1, 7, 2, 1]
    for (arr, d), expected in zip(arguments, expectations):
        solution = Solution().maxJumps(arr, d)
        assert solution == expected
