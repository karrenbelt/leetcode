### Source : https://leetcode.com/problems/binary-tree-paths/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given the root of a binary tree, return all root-to-leaf paths in any order.
# 
# A leaf is a node with no children.
# 
# Example 1:
# 
# Input: root = [1,2,3,null,5]
# Output: ["1->2->5","1->3"]
# 
# Example 2:
# 
# Input: root = [1]
# Output: ["1"]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 100].
# 	-100 <= Node.val <= 100
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def binaryTreePaths(self, root: TreeNode) -> List[str]:

        def traverse(node: TreeNode, path: List[int]):
            if not node:
                return
            if not node.left and not node.right:
                paths.append('->'.join(map(str, path + [node.val])))
            traverse(node.left, path + [node.val])
            traverse(node.right, path + [node.val])

        paths = []
        traverse(root, [])
        return paths


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,null,5]",
        "[1]",
        ]
    expectations = [
        ["1->2->5", "1->3"],
        ["1"]
        ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().binaryTreePaths(root)
        assert set(solution) == set(expected)
