### Source : https://leetcode.com/problems/design-hashset/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# Design a HashSet without using any built-in hash table libraries.
# 
# Implement yHashSet class:
# 
# 	void add(key) Inserts the value key into the HashSet.
# 	bool contains(key) Returns whether the value key exists in the HashSet or not.
# 	void remove(key) Removes the value key in the HashSet. If key does not exist in the 
# HashSet, do nothing.
# 
# Example 1:
# 
# Input
# ["yHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
# [[], [1], [2], [1], [3], [2], [2], [2], [2]]
# Output
# [null, null, null, true, false, null, true, null, false]
# 
# Explanation
# yHashSet myHashSet = new yHashSet();
# myHashSet.add(1);      // set = [1]
# myHashSet.add(2);      // set = [1, 2]
# myHashSet.contains(1); // return True
# myHashSet.contains(3); // return False, (not found)
# myHashSet.add(2);      // set = [1, 2]
# myHashSet.contains(2); // return True
# myHashSet.remove(2);   // set = [1]
# myHashSet.contains(2); // return False, (already removed)
# 
# Constraints:
# 
# 	0 <= key <= 106
# 	At most 104 calls will be made to add, remove, and contains.
# 
# Follow up: Could you solve the problem without using the built-in HashSet library?
#####################################################################################################


class MyHashSet:

    def __init__(self):
        self.array = [[] for _ in range(100)]

    @staticmethod
    def hash(number):
        return number % 99

    def add(self, key: int) -> None:
        if not self.contains(key):
            self.array[self.hash(key)].append(key)

    def remove(self, key: int) -> None:
        if self.contains(key):
            self.array[self.hash(key)].remove(key)

    def contains(self, key: int) -> bool:
        return key in self.array[self.hash(key)]


# Your MyHashSet object will be instantiated and called as such:
# obj = MyHashSet()
# obj.add(key)
# obj.remove(key)
# param_3 = obj.contains(key)

def test():
    operations = ["MyHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
    arguments = [[], [1], [2], [1], [3], [2], [2], [2], [2]]
    expectations = [None, None, None, True, False, None, True, None, False]
    obj = MyHashSet(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
