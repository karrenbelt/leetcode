### Source : https://leetcode.com/problems/wiggle-subsequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-18

##################################################################################################### 
#
# Given an integer array nums, return the length of the longest wiggle sequence.
# 
# A wiggle sequence is a sequence where the differences between successive numbers strictly alternate 
# between positive and negative. The first difference (if one exists) may be either positive or 
# negative. A sequence with fewer than two elements is trivially a wiggle sequence.
# 
# 	For example, [1, 7, 4, 9, 2, 5] is a wiggle sequence because the differences (6, -3, 5, -7, 
# 3) are alternately positive and negative.
# 	In contrast, [1, 4, 7, 2, 5] and [1, 7, 4, 5, 5] are not wiggle sequences, the first 
# because its first two differences are positive and the second because its last difference is zero.
# 
# A subsequence is obtained by deleting some elements (eventually, also zero) from the original 
# sequence, leaving the remaining elements in their original order.
# 
# Example 1:
# 
# Input: nums = [1,7,4,9,2,5]
# Output: 6
# Explanation: The entire sequence is a wiggle sequence.
# 
# Example 2:
# 
# Input: nums = [1,17,5,10,13,15,10,5,16,8]
# Output: 7
# Explanation: There are several subsequences that achieve this length. One is [1,17,10,13,10,16,8].
# 
# Example 3:
# 
# Input: nums = [1,2,3,4,5,6,7,8,9]
# Output: 2
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	0 <= nums[i] <= 1000
# 
# Follow up: Could you solve this in O(n) time?
#####################################################################################################

from typing import List


class Solution:
    def wiggleMaxLength(self, nums: List[int]) -> int:  # naive dp: O(n^2) time and O(n) space
        if not nums:
            return 0
        down = [1] * len(nums)
        up = [1] * len(nums)
        for i in range(1, len(nums)):
            for j in range(i):
                if nums[i] > nums[j]:
                    up[i] = max(up[i], down[j] + 1)
                elif nums[i] < nums[j]:
                    down[i] = max(down[i], up[j] + 1)
        return max(down[-1], up[-1])

    def wiggleMaxLength(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        if len(nums) < 3:
            return len(set(nums))
        size = 1
        flag = None
        for i in range(len(nums) - 1):
            if nums[i] < nums[i + 1] and flag is not True:
                size += 1
                flag = True
            elif nums[i] > nums[i + 1] and flag is not False:
                size += 1
                flag = False
        return size

    def wiggleMaxLength(self, nums: List[int]) -> int:  # O(n) time and O(1) space (generator)
        # we can use first difference of differences
        trend = peak = 0
        for diff in (prev - curr for prev, curr in zip(nums, nums[1:])):
            if trend * diff < 0:
                peak += 1
            if diff:
                trend = diff
        return peak + 2 if trend else int(bool(nums))

    def wiggleMaxLength(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        diff = [prev - curr for prev, curr in zip(nums, nums[1:]) if prev != curr]
        peak = sum(prev * curr < 0 for prev, curr in zip(diff, diff[1:]))
        return peak + 2 if any(diff) else int(bool(nums))


def test():
    arguments = [
        [1, 7, 4, 9, 2, 5],
        [1, 17, 5, 10, 13, 15, 10, 5, 16, 8],
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        ]
    expectations = [6, 7, 2]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().wiggleMaxLength(nums)
        assert solution == expected
