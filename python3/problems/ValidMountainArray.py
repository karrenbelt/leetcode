### Source : https://leetcode.com/problems/valid-mountain-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-04

##################################################################################################### 
#
# Given an array of integers arr, return true if and only if it is a valid mountain array.
# 
# Recall that arr is a mountain array if and only if:
# 
# 	arr.length >= 3
# 	There exists some i with 0 < i < arr.length - 1 such that:
# 
# 		arr[0] < arr[1] < ... < arr[i - 1] < arr[i] 
# 		arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
# 
# Example 1:
# Input: arr = [2,1]
# Output: false
# Example 2:
# Input: arr = [3,5,5]
# Output: false
# Example 3:
# Input: arr = [0,3,2,1]
# Output: true
# 
# Constraints:
# 
# 	1 <= arr.length <= 104
# 	0 <= arr[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def validMountainArray(self, A: List[int]) -> bool:  # O(n) time and O(1) space

        if not len(A) >= 3:
            return False

        if A[0] > A[1]:
            return False

        pivot = False
        for i in range(len(A) - 1):
            if A[i] < A[i + 1]:
                if pivot is True:
                    return False
            elif A[i] > A[i + 1]:
                pivot = True
            else:
                return False

        return pivot

    def validMountainArray(self, A: List[int]) -> bool:  # two pointer: O(n) time and O(1) space
        left, right = 0, len(A) - 1
        while left < len(A) - 1 and A[left] < A[left + 1]:
            left += 1
        while right > 0 and A[right - 1] > A[right]:
            right -= 1
        return 0 < left == right < len(A) - 1


def test():
    arguments = [
        [2, 1],
        [3, 5, 5],
        [0, 3, 2, 1],
    ]
    expectations = [False, False, True]
    for A, expected in zip(arguments, expectations):
        solution = Solution().validMountainArray(A)
        assert solution == expected
