### Source : https://leetcode.com/problems/range-sum-query-2d-immutable/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-12

##################################################################################################### 
#
# Given a 2D matrix matrix, handle multiple queries of the following type:
# 
# 	Calculate the sum of the elements of matrix inside the rectangle defined by its upper left 
# corner (row1, col1) and lower right corner (row2, col2).
# 
# Implement the Numatrix class:
# 
# 	Numatrix(int[][] matrix) Initializes the object with the integer matrix matrix.
# 	int sumRegion(int row1, int col1, int row2, int col2) Returns the sum of the elements of 
# matrix inside the rectangle defined by its upper left corner (row1, col1) and lower right corner 
# (row2, col2).
# 
# Example 1:
# 
# Input
# ["Numatrix", "sumRegion", "sumRegion", "sumRegion"]
# [[[[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]], [2, 1, 4, 
# 3], [1, 1, 2, 2], [1, 2, 2, 4]]
# Output
# [null, 8, 11, 12]
# 
# Explanation
# Numatrix numatrix = new Numatrix([[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 
# 1, 7], [1, 0, 3, 0, 5]]);
# numatrix.sumRegion(2, 1, 4, 3); // return 8 (i.e sum of the red rectangle)
# numatrix.sumRegion(1, 1, 2, 2); // return 11 (i.e sum of the green rectangle)
# numatrix.sumRegion(1, 2, 2, 4); // return 12 (i.e sum of the blue rectangle)
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 200
# 	-105 <= matrix[i][j] <= 105
# 	0 <= row1 <= row2 < m
# 	0 <= col1 <= col2 < n
# 	At most 104 calls will be made to sumRegion.
#####################################################################################################

from typing import List


class NumMatrix:

    def __init__(self, matrix: List[List[int]]):
        self.matrix = matrix
        # precompute 2d prefix sum
        self.dp = [[0] * (len(matrix[0]) + 1) for _ in range(len(matrix) + 1)]  # O(mn) time to precompute
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                self.dp[i + 1][j + 1] = self.dp[i + 1][j] + self.dp[i][j + 1] + matrix[i][j] - self.dp[i][j]

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:  # O(mn) time per query and O(1) space
        return sum(self.matrix[i][j] for i in range(row1, row2 + 1) for j in range(col1, col2 + 1))

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:  # O(1) per query and O(mn) space
        # idea (O is origin here): OD - OB - OC + OA
        return self.dp[row2 + 1][col2 + 1] - self.dp[row1][col2 + 1] - self.dp[row2 + 1][col1] + self.dp[row1][col1]


# Your NumMatrix object will be instantiated and called as such:
# obj = NumMatrix(matrix)
# param_1 = obj.sumRegion(row1,col1,row2,col2)


def test():
    operations = ["NumMatrix", "sumRegion", "sumRegion", "sumRegion"]
    arguments = [
        [[[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]],
        [2, 1, 4, 3],
        [1, 1, 2, 2],
        [1, 2, 2, 4]
    ]
    expectations = [None, 8, 11, 12]
    obj = NumMatrix(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
