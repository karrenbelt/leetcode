### Source : https://leetcode.com/problems/minimum-falling-path-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-19

##################################################################################################### 
#
# Given an n x n array of integers matrix, return the minimum sum of any falling path through matrix.
# 
# A falling path starts at any element in the first row and chooses the element in the next row that 
# is either directly below or diagonally left/right. Specifically, the next element from position 
# (row, col) will be (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).
# 
# Example 1:
# 
# Input: matrix = [[2,1,3],[6,5,4],[7,8,9]]
# Output: 13
# Explanation: There are two falling paths with a minimum sum underlined below:
# [[2,1,3],      [[2,1,3],
#  [6,5,4],       [6,5,4],
#  [7,8,9]]       [7,8,9]]
# 
# Example 2:
# 
# Input: matrix = [[-19,57],[-40,-5]]
# Output: -59
# Explanation: The falling path with a minimum sum is underlined below:
# [[-19,57],
#  [-40,-5]]
# 
# Example 3:
# 
# Input: matrix = [[-48]]
# Output: -48
# 
# Constraints:
# 
# 	n == matrix.length
# 	n == matrix[i].length
# 	1 <= n <= 100
# 	-100 <= matrix[i][j] <= 100
#####################################################################################################

from typing import List


class Solution:
    def minFallingPathSum(self, matrix: List[List[int]]) -> int:  # O(n ^ 2) time and O(n) space
        # bottom-up
        dp = [matrix[-1][0]] + matrix[-1].copy() + [matrix[-1][-1]]
        for i in range(len(matrix) - 2, -1, -1):
            new = [0] * len(dp)
            for j in range(len(matrix[i])):
                new[j + 1] = min(dp[j: j + 3]) + matrix[i][j]
            new[0], new[-1] = new[1], new[-2]
            dp = new
        return min(dp)

    def minFallingPathSum(self, matrix: List[List[int]]) -> int:  # O(n ^ 2) time and O(n) space
        dp = [matrix[-1][0]] + matrix[-1].copy() + [matrix[-1][-1]]
        for i in range(len(matrix) - 2, -1, -1):
            new = [min(dp[j: j + 3]) + matrix[i][j] for j in range(len(matrix[i]))]
            dp[:] = [new[0]] + new + [new[-1]]
        return min(dp)

    def minFallingPathSum(self, matrix: List[List[int]]) -> int:  # O(n ^ 2) time and O(n) space
        dp = matrix[0]
        for row in matrix[1:]:
            dp = [v + min([dp[c], dp[max(c - 1, 0)], dp[min(len(matrix) - 1, c + 1)]]) for c, v in enumerate(row)]
        return min(dp)


def test():
    arguments = [
        [[2, 1, 3],
         [6, 5, 4],
         [7, 8, 9]],

        [[-19, 57],
         [-40, -5]],

        [[-48]],
        ]
    expectations = [13, -59, -48]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().minFallingPathSum(matrix)
        assert solution == expected
