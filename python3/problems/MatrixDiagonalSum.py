### Source : https://leetcode.com/problems/matrix-diagonal-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# Given a square matrix mat, return the sum of the matrix diagonals.
# 
# Only include the sum of all the elements on the primary diagonal and all the elements on the 
# secondary diagonal that are not part of the primary diagonal.
# 
# Example 1:
# 
# Input: mat = [[1,2,3],
#               [4,5,6],
#               [7,8,9]]
# Output: 25
# Explanation: Diagonals sum: 1 + 5 + 9 + 3 + 7 = 25
# Notice that element mat[1][1] = 5 is counted only once.
# 
# Example 2:
# 
# Input: mat = [[1,1,1,1],
#               [1,1,1,1],
#               [1,1,1,1],
#               [1,1,1,1]]
# Output: 8
# 
# Example 3:
# 
# Input: mat = [[5]]
# Output: 5
# 
# Constraints:
# 
# 	n == mat.length == mat[i].length
# 	1 <= n <= 100
# 	1 <= mat[i][j] <= 100
#####################################################################################################

from typing import List


class Solution:
    def diagonalSum(self, mat: List[List[int]]) -> int:  # time O(n) and space O(1)
        diag_sum = sum(mat[i][i] + mat[i][- i - 1] for i in range(len(mat)))
        return diag_sum - mat[len(mat) // 2][len(mat) // 2] if len(mat) % 2 else diag_sum

    def diagonalSum(self, mat: List[List[int]]) -> int:  # time O(n) and space O(1)
        return sum(sum(row[j] for j in {i, len(row) - i - 1}) for i, row in enumerate(mat))


def test():
    arguments = [
        [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]],

        [[1, 1, 1, 1],
         [1, 1, 1, 1],
         [1, 1, 1, 1],
         [1, 1, 1, 1]],

        [[5]],

        [[7, 3, 1, 9],
         [3, 4, 6, 9],
         [6, 9, 6, 6],
         [9, 5, 8, 5]],
    ]
    expectations = [25, 8, 5, 55]
    for mat, expected in zip(arguments, expectations):
        solution = Solution().diagonalSum(mat)
        assert solution == expected
