### Source : https://leetcode.com/problems/split-linked-list-in-parts/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# Given the head of a singly linked list and an integer k, split the linked list into k consecutive 
# linked list parts.
# 
# The length of each part should be as equal as possible: no two parts should have a size differing 
# by more than one. This may lead to some parts being null.
# 
# The parts should be in the order of occurrence in the input list, and parts occurring earlier 
# should always have a size greater than or equal to parts occurring later.
# 
# Return an array of the k parts.
# 
# Example 1:
# 
# Input: head = [1,2,3], k = 5
# Output: [[1],[2],[3],[],[]]
# Explanation:
# The first element output[0] has output[0].val = 1, output[0].next = null.
# The last element output[4] is null, but its string representation as a ListNode is [].
# 
# Example 2:
# 
# Input: head = [1,2,3,4,5,6,7,8,9,10], k = 3
# Output: [[1,2,3,4],[5,6,7],[8,9,10]]
# Explanation:
# The input has been split into consecutive parts with size difference at most 1, and earlier parts 
# are a larger size than the later parts.
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 1000].
# 	0 <= Node.val <= 1000
# 	1 <= k <= 50
#####################################################################################################

from typing import List
from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def splitListToParts(self, head: ListNode, k: int) -> List[ListNode]:  # O(n) time
        length, node = 0, head
        while node:
            node = node.next
            length += 1

        node = head
        parts = []
        for i in range(k):
            quotient, remainder = divmod(length, k)
            part_length = quotient + (1 if i < remainder else 0)
            for j in range(part_length):
                if j == 0:
                    parts.append(node)
                if j == part_length - 1:
                    next_node = node.next
                    node.next, node = None, next_node
                else:
                    node = node.next
        return parts + [None] * (k - len(parts))

    def splitListToParts(self, head: ListNode, k: int) -> List[ListNode]:  # O(n) time: 32 ms
        length, node = 0, head
        while node:
            node, length = node.next, length + 1
        parts, node, (q, r) = [], head, divmod(length, k)
        for _ in range(k):
            parts.append(node)
            for _ in range(q + bool(r) - 1):
                node = node.next
            if node:
                next_node = node.next
                node.next, node = None, next_node
            r -= bool(r)
        return parts + [None] * (k - len(parts))


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([1, 2, 3], 5),
        ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3),
    ]
    expectations = [
        [[1], [2], [3], [], []],
        [[1, 2, 3, 4], [5, 6, 7], [8, 9, 10]],
    ]
    for (nums, k), expected in zip(arguments, expectations):
        root = SinglyLinkedList(nums).head
        solution = Solution().splitListToParts(root, k)
        assert solution == list(map(lambda x: SinglyLinkedList(x).head, expected))
