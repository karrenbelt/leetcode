### Source : https://leetcode.com/problems/combinations/
### Author : M.A.P. Karrenbelt
### Date   : 2021-01-03

##################################################################################################### 
#
# Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
# 
# You may return the answer in any order.
# 
# Example 1:
# 
# Input: n = 4, k = 2
# Output:
# [
#   [2,4],
#   [3,4],
#   [2,3],
#   [1,2],
#   [1,3],
#   [1,4],
# ]
# 
# Example 2:
# 
# Input: n = 1, k = 1
# Output: [[1]]
# 
# Constraints:
# 
# 	1 <= n <= 20
# 	1 <= k <= n
#####################################################################################################

from typing import List


class Solution:

    def combine(self, n: int, k: int) -> List[List[int]]:

        def backtrack(i: int, arr: list):
            if len(arr) == k:
                ans.append(arr.copy())  # derp-a-derp
                return
            for i in range(i, n):
                arr.append(i + 1)
                backtrack(i + 1, arr)
                arr.pop()

        ans = []
        backtrack(0, [])
        return ans

    def combine(self, n: int, k: int) -> List[List[int]]:

        def dfs(nums, k, i, path):
            if k == 0:
                ans.append(path)
                return
            for j in range(i, len(nums)):
                dfs(nums, k - 1, j + 1, path + [nums[j]])

        ans = []
        dfs(range(1, n + 1), k, 0, [])
        return ans

    def combine(self, n: int, k: int) -> List[List[int]]:

        def dfs(i: int, path: list):
            if len(path) == k:
                paths.append(path)
            else:
                for j in range(i + 1, n + 1):
                    dfs(j, path + [j])

        paths = []
        dfs(0, [])
        return paths

    def combine(self, n: int, k: int) -> List[List[int]]:

        def dfs(i: int, path: list, paths: list):
            if len(path) == k:
                paths.append(path)
            else:
                for j in range(i + 1, n + 1):
                    dfs(j, path + [j], paths)
            return paths

        return dfs(0, [], [])

    def combine(self, n: int, k: int) -> List[List[int]]:  # recursive
        return [pre + [i] for i in range(k, n + 1) for pre in self.combine(i - 1, k - 1)] if k else [[]]

    def combine(self, n: int, k: int) -> List[List[int]]:  # iterative
        combs = [[]]
        for _ in range(k):
            combs = [[i] + c for c in combs for i in range(1, c[0] if c else n + 1)]
        return combs

    def combine(self, n: int, k: int) -> List[List[int]]:
        from itertools import combinations
        return list(map(list, combinations(range(1, n + 1), k)))


def test():
    numbers = [
        (4, 2),
        (1, 1),
        (2, 1),
        (3, 3),
        ]
    expectations = [
        [[2, 4], [3, 4], [2, 3], [1, 2], [1, 3], [1, 4]],
        [[1]],
        [[1], [2]],
        [[1, 2, 3]],
        ]
    for (n, k), expected in zip(numbers, expectations):
        solution = Solution().combine(n, k)
        assert set(map(frozenset, solution)) == set(map(frozenset, expected))
test()