### Source : https://leetcode.com/problems/maximum-absolute-sum-of-any-subarray/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-02

##################################################################################################### 
#
# You are given an integer array nums. The absolute sum of a subarray [numsl, numsl+1, ..., numsr-1, 
# numsr] is abs(numsl + numsl+1 + ... + numsr-1 + numsr).
# 
# Return the maximum absolute sum of any (possibly empty) subarray of nums.
# 
# Note that abs(x) is defined as follows:
# 
# 	If x is a negative integer, then abs(x) = -x.
# 	If x is a non-negative integer, then abs(x) = x.
# 
# Example 1:
# 
# Input: nums = [1,-3,2,3,-4]
# Output: 5
# Explanation: The subarray [2,3] has absolute sum = abs(2+3) = abs(5) = 5.
# 
# Example 2:
# 
# Input: nums = [2,-5,1,-4,3,-2]
# Output: 8
# Explanation: The subarray [-5,1,-4] has absolute sum = abs(-5+1-4) = abs(-8) = 8.
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-104 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def maxAbsoluteSum(self, nums: List[int]) -> int:  # O(n^2) time and O(n^2) space (slicing), probably yields TLE
        return max(abs(sum(nums[i: j])) for i in range(len(nums)) for j in range(i + 1, len(nums)))

    def maxAbsoluteSum(self, nums: List[int]) -> int:  # O(n) time and O(n) space, two pass

        def subarray(func: max):
            prefix_sum = [nums[0]] + [0 for _ in range(len(nums) - 1)]
            for i in range(1, len(nums)):
                prefix_sum[i] = func(prefix_sum[i - 1] + nums[i], nums[i])
            return func(prefix_sum)

        return max(subarray(max), abs(subarray(min)))

    def maxAbsoluteSum(self, nums: List[int]) -> int:  # O(n) time and O(1) space, two pass

        def subarray(func: max):
            prefix_sum = optimum = 0
            for n in nums:
                prefix_sum += n
                optimum = func(optimum, prefix_sum)
            return optimum

        return subarray(max) - subarray(min)

    def maxAbsoluteSum(self, nums: List[int]) -> int:  # O(n) time and O(1) space, single pass
        prefix_sum = minimum = maximum = 0
        for n in nums:
            prefix_sum += n
            minimum, maximum = min(minimum, prefix_sum), max(maximum, prefix_sum)
        return maximum - minimum


def test():
    arguments = [
        [1, -3, 2, 3, -4],
        [2, -5, 1, -4, 3, -2],
    ]
    expectations = [5, 8]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maxAbsoluteSum(nums)
        assert solution == expected
