### Source : https://leetcode.com/problems/subarray-sums-divisible-by-k/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-17

##################################################################################################### 
#
# Given an array A of integers, return the number of (contiguous, non-empty) subarrays that have a 
# sum divisible by K.
# 
# Example 1:
# 
# Input: A = [4,5,0,-2,-3,1], K = 5
# Output: 7
# Explanation: There are 7 subarrays with a sum divisible by K = 5:
# [4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
# 
# Note:
# 
# 	1 <= A.length <= 30000
# 	-10000 <= A[i] <= 10000
# 	2 <= K <= 10000
# 
#####################################################################################################

from typing import List


class Solution:

    def subarraysDivByK(self, A: List[int], K: int) -> int:  # O(n^2) time --> TLE: 38 / 73 test cases passed.
        return sum(1 for i in range(len(A)) for j in range(i + 1, len(A) + 1) if sum(A[i: j]) % K == 0)

    def subarraysDivByK(self, A: List[int], K: int) -> int:  # prefix sum (dp): O(n) time O(k) space
        prefix = ctr = 0
        counts = [1] + [0] * K
        for n in A:
            prefix = (prefix + n) % K
            ctr += counts[prefix]
            counts[prefix] += 1
        return ctr


def test():
    arguments = [
        ([4, 5, 0, -2, -3, 1], 5),
        ]
    expectations = [7, ]
    for (A, K), expected in zip(arguments, expectations):
        solution = Solution().subarraysDivByK(A, K)
        assert solution == expected
