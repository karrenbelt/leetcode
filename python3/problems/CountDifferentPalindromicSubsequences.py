### Source : https://leetcode.com/problems/count-different-palindromic-subsequences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# Given a string s, return the number of different non-empty palindromic subsequences in s. Since the 
# answer may be very large, return it modulo 109 + 7.
# 
# A subsequence of a string is obtained by deleting zero or more characters from the string.
# 
# A sequence is palindromic if it is equal to the sequence reversed.
# 
# Two sequences a1, a2, ... and b1, b2, ... are different if there is some i for which ai != bi.
# 
# Example 1:
# 
# Input: s = "bccb"
# Output: 6
# Explanation: The 6 different non-empty palindromic subsequences are 'b', 'c', 'bb', 'cc', 'bcb', 
# 'bccb'.
# Note that 'bcb' is counted only once, even though it occurs twice.
# 
# Example 2:
# 
# Input: s = "abcdabcdabcdabcdabcdabcdabcdabcddcbadcbadcbadcbadcbadcbadcbadcba"
# Output: 104860361
# Explanation: There are 3104860382 different non-empty palindromic subsequences, which is 104860361 
# modulo 109 + 7.
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s[i] is either 'a', 'b', 'c', or 'd'.
#####################################################################################################


class Solution:
    def countPalindromicSubsequences(self, s: str) -> int:  # O(n^2) time and O(n) space
        # counting the same subsequence twice is prevented by finding the start and end of a potential palindrome,
        # and then moving inwards to generate the sub-windows.
        # e.g. take: c aba c aba c
        # the leftmost "c" is always matched with the rightmost c in any given segment (e.g. start = 0; end = 9)
        # and therefore all distinct palindromic subsequences between the start and end (exclusive) are counted once
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(start: int, end: int) -> int:
            if start >= end:
                return 0
            ctr = 0
            for c in "abcd":
                left, right = s.find(c, start, end), s.rfind(c, start, end)
                if left == -1 or right == -1:
                    continue
                ctr += 1 if left == right else 2 + dfs(left + 1, right)
            return ctr

        return dfs(0, len(s)) % (10**9 + 7)


def test():
    arguments = [
        "bccb",
        "abcdabcdabcdabcdabcdabcdabcdabcddcbadcbadcbadcbadcbadcbadcbadcba",
    ]
    expectations = []
    for s, expected in zip(arguments, expectations):
        solution = Solution().countPalindromicSubsequences(s)
        assert solution == expected
