### Source : https://leetcode.com/problems/smallest-subtree-with-all-the-deepest-nodes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-10

##################################################################################################### 
#
# Given the root of a binary tree, the depth of each node is the shortest distance to the root.
# 
# Return the smallest subtree such that it contains all the deepest nodes in the original tree.
# 
# A node is called the deepest if it has the largest depth possible among any node in the entire tree.
# 
# The subtree of a node is tree consisting of that node, plus the set of all descendants of that node.
# 
# Note: This question is the same as 1123: 
# https://leetcode.com/problems/lowest-common-ancestor-of-deepest-leaves/
# 
# Example 1:
# 
# Input: root = [3,5,1,6,2,0,8,null,null,7,4]
# Output: [2,7,4]
# Explanation: We return the node with value 2, colored in yellow in the diagram.
# The nodes coloured in blue are the deepest nodes of the tree.
# Notice that nodes 5, 3 and 2 contain the deepest nodes in the tree but node 2 is the smallest 
# subtree among them, so we return it.
# 
# Example 2:
# 
# Input: root = [1]
# Output: [1]
# Explanation: The root is the deepest node in the tree.
# 
# Example 3:
# 
# Input: root = [0,1,3,null,2]
# Output: [2]
# Explanation: The deepest node in the tree is 2, the valid subtrees are the subtrees of nodes 2, 1 
# and 0 but the subtree of node 2 is the smallest.
# 
# Constraints:
# 
# 	The number of nodes in the tree will be in the range [1, 500].
# 	0 <= Node.val <= 500
# 	The values of the nodes in the tree are unique.
#####################################################################################################


from python3 import BinaryTreeNode as TreeNode


class Solution:
    # question is the same as 1123. Lowest Common Ancestor of Deepest Leaves
    def subtreeWithAllDeepest(self, root: TreeNode) -> TreeNode:
        # the maximum distance (width) between two deepest nodes is directly informative of its LCA
        def traverse(node: TreeNode, level: int):
            if node:
                if level == len(levels):
                    levels.append([])
                levels[level].append(node)
                traverse(node.left, level + 1)
                traverse(node.right, level + 1)

        def find_lca(node: TreeNode, deepest_nodes):  # 236. Lowest Common Ancestor of a Binary Tree
            if not node or any(node == n for n in deepest_nodes):  # root found
                return node
            left = find_lca(node.left, deepest_nodes)
            right = find_lca(node.right, deepest_nodes)
            return node if left and right else (left or right)

        return (levels := []) or traverse(root, 0) or find_lca(root, levels[-1])


def test():
    from python3 import Codec
    arguments = [
        "[3,5,1,6,2,0,8,null,null,7,4]",
        "[1]",
        "[0,1,3,null,2]",
    ]
    expectations = [
        "[2,7,4]",
        "[1]",
        "[2]",
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().subtreeWithAllDeepest(root)
        assert solution == Codec.deserialize(expected)
test()