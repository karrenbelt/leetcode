### Source : https://leetcode.com/problems/minimum-deletions-to-make-character-frequencies-unique/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-18

##################################################################################################### 
#
# A string s is called good if there are no two different characters in s that have the same 
# frequency.
# 
# Given a string s, return the minimum number of characters you need to delete to make s good.
# 
# The frequency of a character in a string is the number of times it appears in the string. For 
# example, in the string "aab", the frequency of 'a' is 2, while the frequency of 'b' is 1.
# 
# Example 1:
# 
# Input: s = "aab"
# Output: 0
# Explanation: s is already good.
# 
# Example 2:
# 
# Input: s = "aaabbbcc"
# Output: 2
# Explanation: You can delete two 'b's resulting in the good string "aaabcc".
# Another way it to delete one 'b' and one 'c' resulting in the good string "aaabbc".
# 
# Example 3:
# 
# Input: s = "ceabaacb"
# Output: 2
# Explanation: You can delete both 'c's resulting in the good string "eabaab".
# Note that we only care about characters that are still in the string at the end (i.e. frequency of 
# 0 is ignored).
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s contains only lowercase English letters.
#####################################################################################################


class Solution:
    def minDeletions(self, s: str) -> int:  # O(n) time and O(1) space
        from collections import Counter
        counts, seen, ctr = Counter(s), set(), 0  # counter is O(n) time but at most 26 characters so O(1) space
        for _, count in reversed(counts.most_common()):
            while count and count in seen:  # at most 26 items
                ctr += 1
                count -= 1
            seen.add(count)
        return ctr


def test():
    arguments = [
        "aab",
        "aaabbbcc",
        "ceabaacb",
    ]
    expectations = [0, 2, 2]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minDeletions(s)
        assert solution == expected
