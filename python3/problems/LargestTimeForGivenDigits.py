### Source : https://leetcode.com/problems/largest-time-for-given-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given an array arr of 4 digits, find the latest 24-hour time that can be made using each digit 
# exactly once.
# 
# 24-hour times are formatted as "HH:", where HH is between 00 and 23, and  is between 00 and 59. 
# The earliest 24-hour time is 00:00, and the latest is 23:59.
# 
# Return the latest 24-hour time in "HH:" format.  If no valid time can be made, return an empty 
# string.
# 
# Example 1:
# 
# Input: A = [1,2,3,4]
# Output: "23:41"
# Explanation: The valid 24-hour times are "12:34", "12:43", "13:24", "13:42", "14:23", "14:32", 
# "21:34", "21:43", "23:14", and "23:41". Of these times, "23:41" is the latest.
# 
# Example 2:
# 
# Input: A = [5,5,5,5]
# Output: ""
# Explanation: There are no valid 24-hour times as "55:55" is not valid.
# 
# Example 3:
# 
# Input: A = [0,0,0,0]
# Output: "00:00"
# 
# Example 4:
# 
# Input: A = [0,0,1,0]
# Output: "10:00"
# 
# Constraints:
# 
# 	arr.length == 4
# 	0 <= arr[i] <= 9
#####################################################################################################

from typing import List


class Solution:
    def largestTimeFromDigits(self, arr: List[int]) -> str:

        def permutations(nums: List[int], path: List[int]):
            if len(path) == len(arr):
                yield path
            else:
                for j in range(len(nums)):
                    yield from permutations(nums[:j] + nums[j + 1:], path + [nums[j]])

        arr.sort(reverse=True)
        for (n1, n2, n3, n4) in permutations(arr, []):
            hh = n1 * 10 + n2
            mm = n3 * 10 + n4
            if hh < 24 and mm < 60:
                return f"{n1}{n2}:{n3}{n4}"
        return ""


def test():
    arguments = [
        [1, 2, 3, 4],
        [5, 5, 5, 5],
        [0, 0, 0, 0],
        [0, 0, 1, 0],
        ]
    expectations = ["23:41", "", "00:00", "10:00"]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().largestTimeFromDigits(arr)
        assert solution == expected
