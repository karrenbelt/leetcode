### Source : https://leetcode.com/problems/search-a-2d-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

##################################################################################################### 
#
# Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the 
# following properties:
# 
# 	Integers in each row are sorted from left to right.
# 	The first integer of each row is greater than the last integer of the previous row.
# 
# Example 1:
# 
# Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
# Output: true
# 
# Example 2:
# 
# Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
# Output: false
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 100
# 	-104 <= matrix[i][j], target <= 104
#####################################################################################################

from typing import List


class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:  # O(m + n)
        i = j = 0
        while 0 <= i < len(matrix) or 0 <= j < len(matrix[0]):
            if matrix[i][j] == target:
                return True
            elif i < len(matrix) - 1 and matrix[i + 1][j] <= target:
                i += 1
            elif j < len(matrix[0]) - 1 and matrix[i][j + 1] <= target:
                j += 1
            else:
                return False

    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:  # O(m + n)
        return any(target in row for row in matrix)

    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:  # O(log mn) time and O(1) space
        n = len(matrix[0])
        left, right = 0, len(matrix) * n
        while left < right:
            mid = left + (right - left) // 2
            if matrix[mid // n][mid % n] < target:
                left = mid + 1
            elif matrix[mid // n][mid % n] > target:
                right = mid
            else:
                return True
        return False

    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:  # O(log mn) time and O(1) space
        import bisect
        return bool(matrix) and target in matrix[bisect.bisect(matrix, [target + 0.5]) - 1]


def test():
    arguments = [
        ([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]], 3),
        ([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]], 13),
        ]
    expectations = [True, False]
    for (matrix, target), expected in zip(arguments, expectations):
        solution = Solution().searchMatrix(matrix, target)
        assert solution == expected
