### Source : https://leetcode.com/problems/count-and-say/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# The count-and-say sequence is the sequence of integers with the first five terms as following:
# 
# 1.     1
# 2.     11
# 3.     21
# 4.     1211
# 5.     111221
# 
# 1 is read off as "one 1" or 11.
# 11 is read off as "two 1s" or 21.
# 21 is read off as "one 2, then one 1" or 1211.
# 
# Given an integer n where 1 &le; n &le; 30, generate the nth term of the count-and-say sequence. You 
# can do so recursively, in other words from the previous member read off the digits, counting the 
# number of digits in groups of the same digit.
# 
# Note: Each term of the sequence of integers will be represented as a string.
# 
# Example 1:
# 
# Input: 1
# Output: "1"
# Explanation: This is the base case.
# 
# Example 2:
# 
# Input: 4
# Output: "1211"
# Explanation: For n = 3 the term was "21" in which we have two groups "2" and "1", "2" can be read 
# as "12" which means frequency = 1 and value = 2, the same way "1" is read as "11", so the answer is 
# the concatenation of "12" and "11" which is "1211".
#####################################################################################################


class Solution:
    def countAndSay(self, n: int) -> str:

        # iterative solution
        for i in range(n):
            if i == 0:
                a = str(1)
            if i == n - 1:
                return a
            else:
                new_a = ''
                prev_c = a[0]
                ctr = 0
                for c in a:
                    if c == prev_c:
                        ctr += 1
                    else:
                        new_a += str(ctr) + prev_c
                        ctr = 1
                        prev_c = c
                new_a += str(ctr) + prev_c
                a = new_a


def test():
    numbers = [1, 4]
    expectations = ["1", "1211"]
    for n, expected in zip(numbers, expectations):
        solution = Solution().countAndSay(n)
        assert solution == expected
