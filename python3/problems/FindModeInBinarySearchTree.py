### Source : https://leetcode.com/problems/find-mode-in-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-27

##################################################################################################### 
#
# Given the root of a binary search tree (BST) with duplicates, return all the mode(s) (i.e., the 
# most frequently occurred element) in it.
# 
# If the tree has more than one mode, return them in any order.
# 
# Assume a BST is defined as follows:
# 
# 	The left subtree of a node contains only nodes with keys less than or equal to the node's 
# key.
# 	The right subtree of a node contains only nodes with keys greater than or equal to the 
# node's key.
# 	Both the left and right subtrees must also be binary search trees.
# 
# Example 1:
# 
# Input: root = [1,null,2,2]
# Output: [2]
# 
# Example 2:
# 
# Input: root = [0]
# Output: [0]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-105 <= Node.val <= 105
# 
# Follow up: Could you do that without using any extra space? (Assume that the implicit stack space 
# incurred due to recursion does not count).
#####################################################################################################

from typing import List, Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def findMode(self, root: Optional[TreeNode]) -> List[int]:
        def traverse(node: TreeNode):
            if not node:
                return
            counts[node.val] = counts.get(node.val, 0) + 1
            traverse(node.left)
            traverse(node.right)

        counts = {}
        traverse(root)
        max_frequency = max(counts.values())
        return [k for k, v in counts.items() if v == max_frequency]


def test():
    from python3 import Codec
    arguments = [
        "[1,null,2,2]",
        "[0]",
    ]
    expectations = [
        [2],
        [0],
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().findMode(root)
        assert solution == expected
