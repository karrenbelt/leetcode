### Source : https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from 
# left to right, level by level from leaf to root).
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
# return its bottom-up level order traversal as:
# 
# [
#   [15,7],
#   [9,20],
#   [3]
# ]
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:  # iterative
        from collections import deque

        levels = []
        if not root:
            return levels

        queue = deque([root])
        while queue:
            level = []
            for i in range(len(queue)):
                node = queue.popleft()
                level.append(node.val)
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
            levels.append(level)
        return levels[::-1]

    def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:  # recursive

        def traverse(node: TreeNode, level: int = 0):  # pre-order / in-order / post-order is all OK -> per level
            if not node:
                return
            if len(levels) == level:
                levels.append([])
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)
            levels[level].append(node.val)

        levels = []
        traverse(root)
        return levels[::-1]


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3,9,20,null,null,15,7]",
        "[1]",
        "[]",
        ]
    expectations = [
        [[15, 7], [9, 20], [3]],
        [[1]],
        []
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().levelOrderBottom(root)
        assert solution == expected
