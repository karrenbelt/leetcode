### Source : https://leetcode.com/problems/contiguous-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 
# 1. 
# 
# Example 1:
# 
# Input: [0,1]
# Output: 2
# Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
# 
# Example 2:
# 
# Input: [0,1,0]
# Output: 2
# Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
# 
# Note:
# The length of the given binary array will not exceed 50,000.
#####################################################################################################

from typing import List


class Solution:
    def findMaxLength(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        # the trick is to realize zero's can be treated as -1,
        # and then a contiguous subarray must sum to zero
        if len(nums) < 2:
            return 0
        h = {0: -1}  # a hash map that stores all indices of consecutive sub arrays
        max_length = running_sum = 0
        for i, n in enumerate(nums):
            running_sum += 1 if n else -1
            if running_sum in h:  # subarray is equal to zero
                max_length = max(max_length, i - h[running_sum])
            else:  # don't update the hash map to maintain the oldest index
                h[running_sum] = i
        return max_length


def test():
    arguments = [
        [0],
        [0, 1],
        [0, 1, 0],
        [0, 1, 1, 0, 1, 0, 0, 1, 1],
        [1, 1, 1, 0, 0, 0],
        ]
    expectations = [0, 2, 2, 8, 6]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findMaxLength(nums)
        assert solution == expected
