### Source : https://leetcode.com/problems/contain-virus/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# 
# A virus is spreading rapidly, and your task is to quarantine the infected area by installing walls.
# 
# The world is modeled as a 2-D array of cells, where 0 represents uninfected cells, and 1 represents 
# cells contaminated with the virus.  A wall (and only one wall) can be installed between any two 
# 4-directionally adjacent cells, on the shared boundary.
# 
# Every night, the virus spreads to all neighboring cells in all four directions unless blocked by a 
# wall.
# Resources are limited. Each day, you can install walls around only one region -- the affected area 
# (continuous block of infected cells) that threatens the most uninfected cells the following night. 
# There will never be a tie.
# 
# Can you save the day? If so, what is the number of walls required? If not, and the world becomes 
# fully infected, return the number of walls used.
# 
# Example 1:
# 
# Input: grid = 
# [[0,1,0,0,0,0,0,1],
#  [0,1,0,0,0,0,0,1],
#  [0,0,0,0,0,0,0,1],
#  [0,0,0,0,0,0,0,0]]
# Output: 10
# Explanation:
# There are 2 contaminated regions.
# On the first day, add 5 walls to quarantine the viral region on the left. The board after the virus 
# spreads is:
# 
# [[0,1,0,0,0,0,1,1],
#  [0,1,0,0,0,0,1,1],
#  [0,0,0,0,0,0,1,1],
#  [0,0,0,0,0,0,0,1]]
# 
# On the second day, add 5 walls to quarantine the viral region on the right. The virus is fully 
# contained.
# 
# Example 2:
# 
# Input: grid = 
# [[1,1,1],
#  [1,0,1],
#  [1,1,1]]
# Output: 4
# Explanation: Even though there is only one cell saved, there are 4 walls built.
# Notice that walls are only built on the shared boundary of two different cells.
# 
# Example 3:
# 
# Input: grid = 
# [[1,1,1,0,0,0,0,0,0],
#  [1,0,1,0,1,1,1,1,1],
#  [1,1,1,0,0,0,0,0,0]]
# Output: 13
# Explanation: The region on the left only builds two new walls.
# 
# Note:
# 
# The number of rows and columns of grid will each be in the range [1, 50].
# Each grid[i][j] will be either 0 or 1.
# Throughout the described process, there is always a contiguous viral region that will infect 
# strictly more uncontaminated squares in the next round.
# 
#####################################################################################################

from typing import List


class Solution:
    def containVirus(self, grid: List[List[int]]) -> int:  # TODO: not finished, too tired
        # we use dfs to find the area and their surrounding 0's
        # however, we don't know which area to contain first, so we go for all combinations
        # we mark cells contained, so we don't visit those anymore
        from copy import deepcopy
        grid = [[0, 1, 0, 0, 0, 0, 0, 1],
                [0, 1, 0, 0, 0, 0, 0, 1],
                [0, 0, 0, 0, 0, 0, 0, 1],
                [0, 0, 0, 0, 0, 0, 0, 0]]

        def get_island_and_neighbors(matrix, contained):

            def dfs(i, j, path):
                if 0 <= i < len(matrix) and 0 <= j < len(matrix[0]) and matrix[i][j] == 1 \
                        and (i, j) not in seen and (i, j) not in contained:
                    path[0].append((i, j))
                    seen.add((i, j))
                    for x, y in directions:
                        nr, nc = i + x, j + y
                        if 0 <= nr < len(matrix) and 0 <= nc < len(matrix[0]) and matrix[nr][nc] == 0:
                            path[1].append((nr, nc))
                            seen.add((nr, nc))
                        dfs(i + x, j + y, path)
                    return path

            seen = set()
            return list(filter(None, (dfs(i, j, [[], []]) for i in range(len(matrix)) for j in range(len(matrix[0])))))

        def backtracking(matrix, n_walls: int, contained: set):
            new_matrix = deepcopy(matrix)
            island_and_neighbors = get_island_and_neighbors(new_matrix, contained)
            for i in range(len(island_and_neighbors)):
                island, neighbors = island_and_neighbors[i]
                for j in range(len(island_and_neighbors)):
                    if i == j:
                        continue
                    for (x, y) in island_and_neighbors[j][1]:
                        new_matrix[x][y] = 1
                    backtracking(new_matrix, n_walls + len(neighbors), contained | set(island))

        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        backtracking(grid, 0, set())
        return


def test():
    arguments = [
        [[0, 1, 0, 0, 0, 0, 0, 1],
         [0, 1, 0, 0, 0, 0, 0, 1],
         [0, 0, 0, 0, 0, 0, 0, 1],
         [0, 0, 0, 0, 0, 0, 0, 0]],

        [[1, 1, 1],
         [1, 0, 1],
         [1, 1, 1]],

        [[1, 1, 1, 0, 0, 0, 0, 0, 0],
         [1, 0, 1, 0, 1, 1, 1, 1, 1],
         [1, 1, 1, 0, 0, 0, 0, 0, 0]],
        ]
    expectations = [10, 4, 13]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().containVirus(grid)
        assert solution == expected
