### Source : https://leetcode.com/problems/longest-uncommon-subsequence-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-27

##################################################################################################### 
#
# Given an array of strings strs, return the length of the longest uncommon subsequence between them. 
# If the longest uncommon subsequence does not exist, return -1.
# 
# An uncommon subsequence between an array of strings is a string that is a subsequence of one string 
# but not the others.
# 
# A subsequence of a string s is a string that can be obtained after deleting any number of 
# characters from s.
# 
# 	For example, "abc" is a subsequence of "aebdc" because you can delete the underlined 
# characters in "aebdc" to get "abc". Other subsequences of "aebdc" include "aebdc", "aeb", and "" 
# (empty string).
# 
# Example 1:
# Input: strs = ["aba","cdc","eae"]
# Output: 3
# Example 2:
# Input: strs = ["aaa","aaa","aa"]
# Output: -1
# 
# Constraints:
# 
# 	1 <= strs.length <= 50
# 	1 <= strs[i].length <= 10
# 	strs[i] consists of lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def findLUSlength(self, strs: List[str]) -> int:  # O(n^2) time

        def is_subsequence(candidate: str, sequence: str) -> bool:
            t = iter(sequence)
            return all(c in t for c in candidate)

        strs.sort(key=len, reverse=True)
        for i, word in enumerate(strs):
            if not any(is_subsequence(word, other_word) for j, other_word in enumerate(strs) if i != j):
                return len(word)
        return -1

    def findLUSlength(self, strs: List[str]) -> int:  # O(n^2) time
        return max([len(s1) for s1 in strs if sum(all(c in it for c in s1) for it in map(iter, strs)) == 1] or [-1])


def test():
    arguments = [
        ["aba", "cdc", "eae"],
        ["aaa", "aaa", "aa"],
        ["aaa", "acb"],
        ["aabbcc", "aabbcc", "cb", "abc"],
    ]
    expectations = [3, -1, 3, 2]
    for strs, expected in zip(arguments, expectations):
        solution = Solution().findLUSlength(strs)
        assert solution == expected
