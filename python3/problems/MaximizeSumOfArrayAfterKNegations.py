### Source : https://leetcode.com/problems/maximize-sum-of-array-after-k-negations/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-09

##################################################################################################### 
#
# Given an integer array nums and an integer k, modify the array in the following way:
# 
# 	choose an index i and replace nums[i] with -nums[i].
# 
# You should apply this process exactly k times. You may choose the same index i multiple times.
# 
# Return the largest possible sum of the array after modifying it in this way.
# 
# Example 1:
# 
# Input: nums = [4,2,3], k = 1
# Output: 5
# Explanation: Choose index 1 and nums becomes [4,-2,3].
# 
# Example 2:
# 
# Input: nums = [3,-1,0,2], k = 3
# Output: 6
# Explanation: Choose indices (1, 2, 2) and nums becomes [3,1,0,2].
# 
# Example 3:
# 
# Input: nums = [2,-3,-1,5,-4], k = 2
# Output: 13
# Explanation: Choose indices (1, 4) and nums becomes [2,3,-1,5,4].
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-100 <= nums[i] <= 100
# 	1 <= k <= 104
#####################################################################################################

from typing import List


class Solution:
    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:  # O(n + 2k * log(n)) time and O(1) space
        import heapq
        heapq.heapify(nums)
        for _ in range(k):
            heapq.heappush(nums, -heapq.heappop(nums))
        return sum(nums)

    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:  # O(n + k * log(n)) time and O(1) space
        import heapq
        heapq.heapify(nums)
        while k:
            heapq.heapreplace(nums, -nums[0])
            k -= 1
        return sum(nums)

    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:  # break early
        import heapq
        heapq.heapify(nums)
        while k and nums[0] < 0:
            heapq.heapreplace(nums, -nums[0])
            k -= 1
        if k % 2:
            heapq.heapreplace(nums, -nums[0])
        return sum(nums)

    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:  # O(n log n) time and O(1) space
        nums.sort()
        i = 0
        while i < len(nums) and i < k and nums[i] < 0:
            nums[i] *= -1
            i += 1
        return sum(nums) - (k - i) % 2 * min(nums) * 2  # if odd, remove smallest number twice since we already summed


def test():
    arguments = [
        ([4, 2, 3], 1),
        ([3, -1, 0, 2], 3),
        ([2, -3, -1, 5, -4], 2),
    ]
    expectations = [5, 6, 13]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().largestSumAfterKNegations(nums, k)
        assert solution == expected
