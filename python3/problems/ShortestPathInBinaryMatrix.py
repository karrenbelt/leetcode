### Source : https://leetcode.com/problems/shortest-path-in-binary-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-13

##################################################################################################### 
#
# In an N by N square grid, each cell is either empty (0) or blocked (1).
# 
# A clear path from top-left to bottom-right has length k if and only if it is composed of cells C_1, 
# C_2, ..., C_k such that:
# 
# 	Adjacent cells C_i and C_{i+1} are connected 8-directionally (ie., they are different and 
# share an edge or corner)
# 	C_1 is at location (0, 0) (ie. has value grid[0][0])
# 	C_k is at location (N-1, N-1) (ie. has value grid[N-1][N-1])
# 	If C_i is located at (r, c), then grid[r][c] is empty (ie. grid[r][c] == 0).
# 
# Return the length of the shortest such clear path from top-left to bottom-right.  If such a path 
# does not exist, return -1.
# 
# Example 1:
# 
# Input: [[0,1],[1,0]]
# 
# Output: 2
# 
# Example 2:
# 
# Input: [[0,0,0],[1,1,0],[1,1,0]]
# 
# Output: 4
# 
# Note:
# 
# 	1 <= grid.length == grid[0].length <= 100
# 	grid[r][c] is 0 or 1
#####################################################################################################

from typing import List


class Solution:
    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:  # -> TLE (unsurprisingly) - 39 / 84 passed
        # dijkstra's algorithm. I don't know it so I try backtracking

        def backtrack(i, j, length):
            if i < 0 or j < 0 or i > len(grid) - 1 or j > len(grid[0]) - 1 or seen[i][j] or grid[i][j]:
                return
            elif i == j == n:
                paths.append(length)
            else:
                seen[i][j] = True
                for y, x in directions:
                    backtrack(i + x, j + y, length + 1)
                seen[i][j] = False

        paths = []
        n = len(grid[0]) - 1
        directions = [(-1, 0), (0, -1), (1, 0), (0, 1), (1, 1), (-1, 1), (1, -1), (-1, -1)]
        seen = [[False] * len(grid[0]) for _ in range(len(grid))]
        backtrack(0, 0, 1)
        return min(paths) if paths else -1

    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:  # -> TLE - 60 / 84 passed

        def backtrack(i, j, length):
            nonlocal shortest_path
            if not 0 <= i <= n or not 0 <= j <= n or seen[i][j] or length >= shortest_path:
                return
            elif i == j == n:
                shortest_path = min(shortest_path, length)
            else:
                seen[i][j] = 1
                for y, x in directions:
                    backtrack(i + x, j + y, length + 1)
                seen[i][j] = 0

        n = len(grid) - 1
        if grid[n][n]:  # quick check if exit blocked
            return -1
        shortest_path = float('inf')
        directions = [(1, 1), (1, 0), (0, 1), (-1, 1), (1, -1), (0, -1), (-1, 0), (-1, -1)]  # order matters
        seen = grid.copy()
        backtrack(0, 0, 1)
        return shortest_path if shortest_path != float('inf') else -1

    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        # even though I intuitively knew it was bfs, I coded up dfs above. Also it's N x N, not N x M
        # we don't need all paths, only shortest, so no need to backtrack ever (since bfs)
        from collections import deque  # I think this effectively is dijkstra's algorithm

        n = len(grid) - 1
        if grid[0][0] or grid[n][n]:
            return -1
        queue = deque([(0, 0, 1)])  # can also iterate list directly since size change upon iteration is allowed
        grid[0][0] = 1
        directions = [(1, 1), (1, 0), (0, 1), (-1, 1), (1, -1), (0, -1), (-1, 0), (-1, -1)]
        while queue:
            i, j, path_length = queue.popleft()
            if i == n and j == n:
                return path_length
            for direction in directions:
                x, y = map(sum, zip((i, j), direction))
                if 0 <= x <= n and 0 <= y <= n and not grid[x][y]:
                    grid[x][y] = 1
                    queue.append((x, y, path_length + 1))
        return -1

    # def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
    #     # dp - doesn't work on cyclic graph problems (goes per row per col -> when visiting 1,2 doesn't know 2,2 yet)
    #     # [[0,1,0,0,0],
    #     #  [0,1,0,1,0],
    #     #  [0,1,0,1,0],
    #     #  [0,1,0,1,0],
    #     #  [0,0,0,1,0],]
    #     pass

    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        # dijkstra with priority queue: A*
        import heapq

        n = len(grid) - 1
        if grid[0][0] or grid[n][n]:
            return -1

        queue = [(1, n, n)]  # since min heap, reverse order
        grid[n][n] = -1      # and start at the end of the grid
        directions = [(1, 1), (1, 0), (0, 1), (-1, 1), (1, -1), (0, -1), (-1, 0), (-1, -1)]
        while queue:
            path_length, i, j = heapq.heappop(queue)
            if i == 0 and j == 0:
                return path_length
            for direction in directions:
                x, y = map(sum, zip((i, j), direction))
                if 0 <= x <= n and 0 <= y <= n and grid[x][y] < 1:
                    new_path_length = path_length + 1
                    if not grid[x][y] or grid[x][y] < - new_path_length:
                        grid[x][y] = - new_path_length
                        heapq.heappush(queue, (new_path_length, x, y))
        return -1


def test():
    arguments = [
        [[0, 0, 0],
         [1, 1, 0],
         [1, 1, 0]],

        [[1, 0, 0],
         [1, 1, 0],
         [1, 1, 0]],

        [[0, 1],
         [1, 0]],

        [[0, 1, 0, 1, 0],
         [1, 0, 0, 0, 1],
         [0, 0, 1, 1, 1],
         [0, 0, 0, 0, 0],
         [1, 0, 1, 0, 0]],

        [[0, 0, 1, 1, 1, 0],
         [1, 0, 1, 1, 0, 0],
         [1, 0, 1, 0, 1, 0],
         [1, 1, 1, 1, 0, 0],
         [1, 1, 1, 1, 0, 1],
         [0, 0, 0, 0, 0, 0]],

        [[0, 1, 0, 0, 0],
         [0, 1, 0, 1, 0],
         [0, 1, 0, 1, 0],
         [0, 1, 0, 1, 0],
         [0, 0, 0, 1, 0]],

        [[1, 1]],
    ]
    expectations = [4, -1, 2, 6, -1, 13, -1]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().shortestPathBinaryMatrix(grid)
        assert solution == expected
