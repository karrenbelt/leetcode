### Source : https://leetcode.com/problems/brick-wall/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# There is a brick wall in front of you. The wall is rectangular and has several rows of bricks. The 
# bricks have the same height but different width. You want to draw a vertical line from the top to 
# the bottom and cross the least bricks.
# 
# The brick wall is represented by a list of rows. Each row is a list of integers representing the 
# width of each brick in this row from left to right.
# 
# If your line go through the edge of a brick, then the brick is not considered as crossed. You need 
# to find out how to draw the line to cross the least bricks and return the number of crossed bricks.
# 
# You cannot draw a line just along one of the two vertical edges of the wall, in which case the line 
# will obviously cross no bricks. 
# 
# Example:
# 
# Input: [[1,2,2,1],
#         [3,1,2],
#         [1,3,2],
#         [2,4],
#         [3,1,2],
#         [1,3,1,1]]
# 
# Output: 2
# 
# Explanation: 
# 
# Note:
# 
# 	The width sum of bricks in different rows are the same and won't exceed INT_AX.
# 	The number of bricks in each row is in range [1,10,000]. The height of wall is in range 
# [1,10,000]. Total number of bricks of the wall won't exceed 20,000.
# 
#####################################################################################################

from typing import List


class Solution:
    def leastBricks(self, wall: List[List[int]]) -> int:  # TLE: 4 / 85 test cases passed.
        width = sum(wall[0]) - 1
        dp = [[0] * width for _ in range(len(wall))]
        for i, row in enumerate(wall):
            j = 0
            for brick in row:
                while brick > 1:
                    dp[i][j] = int(bool(brick - 1))
                    j += 1
                    brick -= 1
                j += 1
        return min(sum(dp[i][j] for i in range(len(dp))) for j in range(len(dp[0]))) if width else len(wall)

    def leastBricks(self, wall: List[List[int]]) -> int:  # O(n * m)
        d = {}
        for row in wall:
            column_i = 0
            for brick in row[:-1]:
                column_i += brick
                d[column_i] = d.get(column_i, 0) + 1
        return len(wall) - max(list(d.values()) + [0])


def test():
    arguments = [
        [[1, 2, 2, 1],
         [3, 1, 2],
         [1, 3, 2],
         [2, 4],
         [3, 1, 2],
         [1, 3, 1, 1]],

        [[1], [1], [1]],
        [[1, 1], [2], [1, 1]],

        [[100000000], [100000000], [100000000]],
        ]
    expectations = [2, 3, 1, 3]
    for wall, expected in zip(arguments, expectations):
        solution = Solution().leastBricks(wall)
        assert solution == expected
