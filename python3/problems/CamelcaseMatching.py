### Source : https://leetcode.com/problems/camelcase-matching/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# A query word matches a given pattern if we can insert lowercase letters to the pattern word so that 
# it equals the query. (We may insert each character at any position, and may insert 0 characters.)
# 
# Given a list of queries, and a pattern, return an answer list of booleans, where answer[i] is true 
# if and only if queries[i] matches the pattern.
# 
# Example 1:
# 
# Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FB"
# Output: [true,false,true,true,false]
# Explanation: 
# "FooBar" can be generated like this "F" + "oo" + "B" + "ar".
# "FootBall" can be generated like this "F" + "oot" + "B" + "all".
# "FrameBuffer" can be generated like this "F" + "rame" + "B" + "uffer".
# 
# Example 2:
# 
# Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FoBa"
# Output: [true,false,true,false,false]
# Explanation: 
# "FooBar" can be generated like this "Fo" + "o" + "Ba" + "r".
# "FootBall" can be generated like this "Fo" + "ot" + "Ba" + "ll".
# 
# Example 3:
# 
# Input: queries = ["FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"], pattern = "FoBaT"
# Output: [false,true,false,false,false]
# Explanation: 
# "FooBarTest" can be generated like this "Fo" + "o" + "Ba" + "r" + "T" + "est".
# 
# Note:
# 
# 	1 <= queries.length <= 100
# 	1 <= queries[i].length <= 100
# 	1 <= pattern.length <= 100
# 	All strings consists only of lower and upper case English letters.
# 
#####################################################################################################

from typing import List


class Solution:
    def camelMatch(self, queries: List[str], pattern: str) -> List[bool]:
        import re

        def matcher(query):
            pat = word_splitter(pattern)
            word_list = word_splitter(query)
            if len(pat) == len(word_list):
                for i in range(len(pat)):
                    idx = 0
                    for c in pat[i]:
                        remainder = word_list[i][idx:]
                        if c in remainder:
                            idx = word_list[i].index(c) + 1
                        else:
                            return False
                return True
            return False

        word_splitter = lambda s: re.findall('[A-Z][^A-Z]*', s)
        return list(map(matcher, queries))

    def camelMatch(self, queries: List[str], pattern: str) -> List[bool]:

        def upper_characters(s: str) -> List[str]:
            return [c for c in s if c.isupper()]

        def is_super_string(s: str) -> bool:
            it = iter(s)
            return all(c in it for c in pattern)

        return [is_super_string(query) and upper_characters(pattern) == upper_characters(query) for query in queries]

    def camelMatch(self, queries: List[str], pattern: str) -> List[bool]:

        def match(query: str) -> bool:
            i = 0
            for j, c in enumerate(query):
                if i < len(pattern) and pattern[i] == query[j]:
                    i += 1
                elif query[j].isupper():
                    return False
            return i == len(pattern)

        return list(map(match, queries))


def test():
    arguments = [
        (["FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"], "FB"),
        (["FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"], "FoBa"),
        (["FooBar", "FooBarTest", "FootBall", "FrameBuffer", "ForceFeedBack"], "FoBaT"),
    ]
    expectatons = [
        [True, False, True, True, False],
        [True, False, True, False, False],
        [False, True, False, False, False],
    ]
    for (queries, pattern), expected in zip(arguments, expectatons):
        solution = Solution().camelMatch(queries, pattern)
        assert solution == expected
