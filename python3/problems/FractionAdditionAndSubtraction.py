### Source : https://leetcode.com/problems/fraction-addition-and-subtraction/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-19

##################################################################################################### 
#
# Given a string expression representing an expression of fraction addition and subtraction, return 
# the calculation result in string format.
# 
# The final result should be an irreducible fraction. If your final result is an integer, say 2, you 
# need to change it to the format of a fraction that has a denominator 1. So in this case, 2 should 
# be converted to 2/1.
# 
# Example 1:
# 
# Input: expression = "-1/2+1/2"
# Output: "0/1"
# 
# Example 2:
# 
# Input: expression = "-1/2+1/2+1/3"
# Output: "1/3"
# 
# Example 3:
# 
# Input: expression = "1/3-1/2"
# Output: "-1/6"
# 
# Example 4:
# 
# Input: expression = "5/3+1/3"
# Output: "2/1"
# 
# Constraints:
# 
# 	The input string only contains '0' to '9', '/', '+' and '-'. So does the output.
# 	Each fraction (input and output) has the format &plusmn;numerator/denominator. If the first 
# input fraction or the output is positive, then '+' will be omitted.
# 	The input only contains valid irreducible fractions, where the numerator and denominator of 
# each fraction will always be in the range [1, 10]. If the denominator is 1, it means this fraction 
# is actually an integer in a fraction format defined above.
# 	The number of given fractions will be in the range [1, 10].
# 	The numerator and denominator of the final result are guaranteed to be valid and in the 
# range of 32-bit int.
#####################################################################################################


class Solution:
    def fractionAddition(self, expression: str) -> str:  # O(n) time, triple pass
        import re
        from fractions import Fraction
        parts = [Fraction(*map(int, element.split('/'))) for element in filter(None, re.split('[+-]', expression))]
        summation, signs = 0, ['+'] + re.findall('[+-]', expression)
        for sign, frac in zip(reversed(signs), reversed(parts)):
            summation += frac if sign == '+' else -frac
        return f"{summation.numerator}/{summation.denominator}"

    def fractionAddition(self, expression: str) -> str:  # O(n) time, single pass
        # can do it straight from strings
        from fractions import Fraction
        summation = sum(map(Fraction, expression.replace('+', ' +').replace('-', ' -').split()))
        return f"{summation.numerator}/{summation.denominator}"

    def fractionAddition(self, expression: str) -> str:  # O(n) time

        def gcd(a: int, b: int) -> int:
            while b:
                a, b = b, a % b
            return a

        fractions = expression.replace("+", " +").replace("-", " -").split()
        numerator, denominator = 0, 1
        for fraction in fractions:
            num, den = map(int, fraction.split("/"))
            numerator = numerator * den + denominator * num
            denominator *= den
            divisor = gcd(numerator, denominator)
            numerator //= divisor
            denominator //= divisor
        return f"{numerator}/{denominator}"


def test():
    arguments = [
        "-1/2+1/2",
        "-1/2+1/2+1/3",
        "1/3-1/2",
        "5/3+1/3",
    ]
    expectations = [
        "0/1",
        "1/3",
        "-1/6",
        "2/1",
    ]
    for expression, expected in zip(arguments, expectations):
        solution = Solution().fractionAddition(expression)
        assert solution == expected, (solution, expected)
