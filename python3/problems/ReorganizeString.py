### Source : https://leetcode.com/problems/reorganize-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# Given a string s, rearrange the characters of s so that any two adjacent characters are not the 
# same.
# 
# Return any possible rearrangement of s or return "" if not possible.
# 
# Example 1:
# Input: s = "aab"
# Output: "aba"
# Example 2:
# Input: s = "aaab"
# Output: ""
# 
# Constraints:
# 
# 	1 <= s.length <= 500
# 	s consists of lowercase English letters.
#####################################################################################################


class Solution:
    def reorganizeString(self, s: str) -> str:
        import heapq
        from collections import Counter

        counts = Counter(s)
        if max(counts.values()) > len(s) // 2 + (len(s) % 2):
            return ""

        heap = [(-v, k) for k, v in counts.items()]
        heapq.heapify(heap)
        reorganized_string = ''
        while len(heap) > 1:
            ctr1, char1 = heapq.heappop(heap)
            ctr2, char2 = heapq.heappop(heap)
            reorganized_string += char1 + char2
            if ctr1 + 1 < 0:
                heapq.heappush(heap, (ctr1 + 1, char1))
            if ctr2 + 1 < 0:
                heapq.heappush(heap, (ctr2 + 1, char2))
        if heap:
            reorganized_string += heapq.heappop(heap)[1]
        return reorganized_string

    def reorganizeString(self, s: str) -> str:
        a = sorted(sorted(s), key=s.count)
        a[1::2], a[::2] = a[:len(a) // 2], a[len(a) // 2:]
        return ''.join(a) * (a[-1:] != a[-2:-1])


def test():
    arguments = [
        "aab",
        "aaab",
        "a",
    ]
    expectations = ["aba", "", "a"]
    for s, expected in zip(arguments, expectations):
        solution = Solution().reorganizeString(s)
        assert solution == expected
