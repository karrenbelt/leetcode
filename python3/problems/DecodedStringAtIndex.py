### Source : https://leetcode.com/problems/decoded-string-at-index/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-02

##################################################################################################### 
#
# An encoded string s is given.  To find and write the decoded string to a tape, the encoded string 
# is read one character at a time and the following steps are taken:
# 
# 	If the character read is a letter, that letter is written onto the tape.
# 	If the character read is a digit (say d), the entire current tape is repeatedly written d-1 
# more times in total.
# 
# Now for some encoded string s, and an index k, find and return the k-th letter (1 indexed) in the 
# decoded string.
# 
# Example 1:
# 
# Input: s = "leet2code3", k = 10
# Output: "o"
# Explanation: 
# The decoded string is "leetleetcodeleetleetcodeleetleetcode".
# The 10th letter in the string is "o".
# 
# Example 2:
# 
# Input: s = "ha22", k = 5
# Output: "h"
# Explanation: 
# The decoded string is "hahahaha".  The 5th letter is "h".
# 
# Example 3:
# 
# Input: s = "a2345678999999999999999", k = 1
# Output: "a"
# Explanation: 
# The decoded string is "a" repeated 8301530446056247680 times.  The 1st letter is "a".
# 
# Constraints:
# 
# 	2 <= s.length <= 100
# 	s will only contain lowercase letters and digits 2 through 9.
# 	s starts with a letter.
# 	1 <= k <= 109
# 	It's guaranteed that k is less than or equal to the length of the decoded string.
# 	The decoded string is guaranteed to have less than 263 letters.
#####################################################################################################

class Solution:
    def decodeAtIndex(self, S: str, K: int) -> str:
        # there's some length of characters, then some multiplication factor with which to multiple this length
        # we can use modulo K to shorten the multiplication factor
        # integers in python can be arbitrarily large making this easier as well
        array = []  # length of arrays: "leet2code3" -> [1,2,3,4,8,9,10,11,12,36]
        for i, c in enumerate(S):
            array += [1] if not array else [array[-1] * int(c)] if c.isdigit() else [array[-1] + 1]
            if array[-1] >= K:
                break

        for i in reversed(range(len(array))):
            K %= array[i]
            if not K and S[i].isalpha():
                return S[i]


def test():
    arguments = [
        ("leet2code3", 10),
        ("ha22", 5),
        ("a2345678999999999999999", 1),
        ("a23", 6),
    ]
    expectations = ["o", "h", "a", "a"]
    for (S, K), expected in zip(arguments, expectations):
        solution = Solution().decodeAtIndex(S, K)
        assert solution == expected
