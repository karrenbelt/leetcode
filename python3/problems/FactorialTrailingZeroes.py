### Source : https://leetcode.com/problems/factorial-trailing-zeroes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given an integer n, return the number of trailing zeroes in n!.
# 
# Follow up: Could you write a solution that works in logarithmic time complexity?
# 
# Example 1:
# 
# Input: n = 3
# Output: 0
# Explanation: 3! = 6, no trailing zero.
# 
# Example 2:
# 
# Input: n = 5
# Output: 1
# Explanation: 5! = 120, one trailing zero.
# 
# Example 3:
# 
# Input: n = 0
# Output: 0
# 
# Constraints:
# 
# 	0 <= n <= 104
#####################################################################################################


class Solution:
    def trailingZeroes(self, n: int) -> int:
        import math
        number = str(math.factorial(n))
        return len(number) - len(number.rstrip('0'))

    def trailingZeroes(self, n: int) -> int:

        def factorial(num: int):
            if num == 0:
                return 1
            return num * factorial(num - 1)

        number = str(factorial(n))
        return len(number) - len(number.rstrip('0'))


def test():
    arguments = [3, 5, 0]
    expectations = [0, 1, 0]
    for n, expected in zip(arguments, expectations):
        solution = Solution().trailingZeroes(n)
        assert solution == expected
