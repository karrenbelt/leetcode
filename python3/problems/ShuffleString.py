### Source : https://leetcode.com/problems/shuffle-string/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-13

##################################################################################################### 
#
# You are given a string s and an integer array indices of the same length. The string s will be 
# shuffled such that the character at the ith position moves to indices[i] in the shuffled string.
# 
# Return the shuffled string.
# 
# Example 1:
# 
# Input: s = "codeleet", indices = [4,5,6,7,0,2,1,3]
# Output: "leetcode"
# Explanation: As shown, "codeleet" becomes "leetcode" after shuffling.
# 
# Example 2:
# 
# Input: s = "abc", indices = [0,1,2]
# Output: "abc"
# Explanation: After shuffling, each character remains in its position.
# 
# Constraints:
# 
# 	s.length == indices.length == n
# 	1 <= n <= 100
# 	s consists of only lowercase English letters.
# 	0 <= indices[i] < n
# 	All values of indices are unique.
#####################################################################################################

from typing import List


class Solution:
    def restoreString(self, s: str, indices: List[int]) -> str:  # O(n log n)
        return "".join(c for i, c in sorted(zip(indices, s)))

    def restoreString(self, s: str, indices: List[int]) -> str:  # O(n)
        restored = [""] * len(s)
        for i, c in zip(indices, s):
            restored[i] = c
        return "".join(restored)

    def restoreString(self, s: str, indices: List[int]) -> str:  # O(n)
        s = list(s)
        for i, c in enumerate(s):
            idx = indices[i]
            while idx != indices[idx]:
                s[idx], c = c, s[idx]
                indices[idx], idx = idx, indices[idx]
        return "".join(s)


def test():
    arguments = [
        ("codeleet", [4, 5, 6, 7, 0, 2, 1, 3]),
        ("abc", [0, 1, 2]),
        ("", [])
    ]
    expectations = ["leetcode", "abc", ""]
    for (s, indices), expected in zip(arguments, expectations):
        solution = Solution().restoreString(s, indices)
        assert solution == expected
test()