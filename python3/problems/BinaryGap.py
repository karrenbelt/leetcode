### Source : https://leetcode.com/problems/binary-gap/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-09

##################################################################################################### 
#
# Given a positive integer n, find and return the longest distance between any two adjacent 1's in 
# the binary representation of n. If there are no two adjacent 1's, return 0.
# 
# Two 1's are adjacent if there are only 0's separating them (possibly no 0's). The distance between 
# two 1's is the absolute difference between their bit positions. For example, the two 1's in "1001" 
# have a distance of 3.
# 
# Example 1:
# 
# Input: n = 22
# Output: 2
# Explanation: 22 in binary is "10110".
# The first adjacent pair of 1's is "10110" with a distance of 2.
# The second adjacent pair of 1's is "10110" with a distance of 1.
# The answer is the largest of these two distances, which is 2.
# Note that "10110" is not a valid pair since there is a 1 separating the two 1's underlined.
# 
# Example 2:
# 
# Input: n = 5
# Output: 2
# Explanation: 5 in binary is "101".
# 
# Example 3:
# 
# Input: n = 6
# Output: 1
# Explanation: 6 in binary is "110".
# 
# Example 4:
# 
# Input: n = 8
# Output: 0
# Explanation: 8 in binary is "1000".
# There aren't any adjacent pairs of 1's in the binary representation of 8, so we return 0.
# 
# Example 5:
# 
# Input: n = 1
# Output: 0
# 
# Constraints:
# 
# 	1 <= n <= 109
#####################################################################################################


class Solution:
    def binaryGap(self, n: int) -> int:
        ctr, max_gap = 1, 0
        s = bin(n)[3:]  # already know 0b1 is the start
        for i in range(len(s)):
            if s[i] == '0':
                ctr += 1
            else:
                max_gap = max(max_gap, ctr)
                ctr = 1
        return max_gap

    def binaryGap(self, n: int) -> int:

        def to_binary(x: int):  # convert int to binary string
            if x // 2 < 2:
                return str(x // 2) + str(x % 2)
            else:
                return to_binary(x // 2) + str(x % 2)

        n_binary = to_binary(n)
        indices = [i for i in range(len(n_binary)) if n_binary[i] == '1']
        gaps = [indices[i] - indices[i - 1] for i in range(1, len(indices))]
        return max(gaps, default=0)

    def binaryGap(self, n: int) -> int:
        a = bin(n)[2:].split('1')[1: -1]
        return max(map(len, a)) + 1 if a else 0

    def binaryGap(self, N: int) -> int:
        return int(N & (N - 1)) and max(map(len, bin(N)[2:].split('1')[1: -1])) + 1


def test():
    arguments = [22, 5, 6, 8, 1]
    expectations = [2, 2, 1, 0, 0]
    for n, expected in zip(arguments, expectations):
        solution = Solution().binaryGap(n)
        assert solution == expected

