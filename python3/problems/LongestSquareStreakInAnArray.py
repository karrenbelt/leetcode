### Source : https://leetcode.com/problems/longest-square-streak-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-03

##################################################################################################### 
#
# You are given an integer array nums. A subsequence of nums is called a square streak if:
# 
# 	The length of the subsequence is at least 2, and
# 	after sorting the subsequence, each element (except the first element) is the square of the 
# previous number.
# 
# Return the length of the longest square streak in nums, or return -1 if there is no square streak.
# 
# A subsequence is an array that can be derived from another array by deleting some or no elements 
# without changing the order of the remaining elements.
# 
# Example 1:
# 
# Input: nums = [4,3,6,16,8,2]
# Output: 3
# Explanation: Choose the subsequence [4,16,2]. After sorting it, it becomes [2,4,16].
# - 4 = 2 * 2.
# - 16 = 4 * 4.
# Therefore, [4,16,2] is a square streak.
# It can be shown that every subsequence of length 4 is not a square streak.
# 
# Example 2:
# 
# Input: nums = [2,3,5,6,7]
# Output: -1
# Explanation: There is no square streak in nums so return -1.
# 
# Constraints:
# 
# 	2 <= nums.length <= 105
# 	2 <= nums[i] <= 105
#####################################################################################################

from typing import List, Generator


class Solution:
    def longestSquareStreak(self, nums: List[int]) -> int:

        def get_squares(num: int) -> Generator:
            while num <= limit:
                yield num
                num *= num

        longest, limit, nums_set = 0, max(nums), set(nums)
        for n in sorted(filter(lambda x: x < limit**0.5 + 1, nums_set)):
            length = 0
            for sq in get_squares(n):
                length = length + 1 if sq in nums_set else 0
                longest = max(longest, length)

        return longest if longest > 1 else -1

    def longestSquareStreak(self, nums: List[int]) -> int:

        s = set(nums)
        nums, longest = sorted(s), 0
        s = {n for n in s if n % 4 < 2}

        for n in nums:
            if n > nums[-1] ** 0.5 + 1:
                break

            length = 1
            while (n := n * n) in s:
                s.remove(n)
                length += 1
            longest = max(longest, length)

        return longest if longest > 1 else -1

    def longestSquareStreak(self, nums: List[int]) -> int:
        longest, nums = -1, set(nums)
        for n in nums:
            for i in range(2, 5):
                if (n := n * n) not in nums:
                    break
                longest = max(longest, i)
        return longest


def test():
    arguments = [
        [4, 3, 6, 16, 8, 2],
        [2, 3, 5, 6, 7],
        [10, 2, 13, 16, 8, 9, 13],
        [3, 9, 81, 6561],
    ]
    expectations = [3, -1, -1, 4]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().longestSquareStreak(nums)
        assert solution == expected
