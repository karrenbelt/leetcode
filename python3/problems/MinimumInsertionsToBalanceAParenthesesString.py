### Source : https://leetcode.com/problems/minimum-insertions-to-balance-a-parentheses-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-01

##################################################################################################### 
#
# Given a parentheses string s containing only the characters '(' and ')'. A parentheses string is 
# balanced if:
# 
# 	Any left parenthesis '(' must have a corresponding two consecutive right parenthesis '))'.
# 	Left parenthesis '(' must go before the corresponding two consecutive right parenthesis 
# '))'.
# 
# In other words, we treat '(' as openning parenthesis and '))' as closing parenthesis.
# 
# For example, "())", "())(())))" and "(())())))" are balanced, ")()", "()))" and "(()))" are not 
# balanced.
# 
# You can insert the characters '(' and ')' at any position of the string to balance it if needed.
# 
# Return the minimum number of insertions needed to make s balanced.
# 
# Example 1:
# 
# Input: s = "(()))"
# Output: 1
# Explanation: The second '(' has two matching '))', but the first '(' has only ')' matching. We need 
# to to add one more ')' at the end of the string to be "(())))" which is balanced.
# 
# Example 2:
# 
# Input: s = "())"
# Output: 0
# Explanation: The string is already balanced.
# 
# Example 3:
# 
# Input: s = "))())("
# Output: 3
# Explanation: Add '(' to match the first '))', Add '))' to match the last '('.
# 
# Example 4:
# 
# Input: s = "(((((("
# Output: 12
# Explanation: Add 12 ')' to balance the string.
# 
# Example 5:
# 
# Input: s = ")))))))"
# Output: 5
# Explanation: Add 4 '(' at the beginning of the string and one ')' at the end. The string becomes 
# "(((())))))))".
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s consists of '(' and ')' only.
#####################################################################################################


class Solution:
    def minInsertions(self, s: str) -> int:  # O(n) time and O(1) space
        open_ctr = ctr = i = 0
        while i < len(s):
            c = s[i]
            if c == '(':
                open_ctr += 1
            else:
                if open_ctr > 0:
                    open_ctr -= 1
                else:
                    ctr += 1
                if i + 1 < len(s) and s[i + 1] == ')':
                    i += 1
                else:
                    ctr += 1
            i += 1
        return ctr + open_ctr * 2

    def minInsertions(self, s: str) -> int:  # O(n) time and O(1) space
        ctr = open_bracket_count = 0
        for c in s.replace('))', '}'):
            if c == '(':
                open_bracket_count += 1
            else:  # for ) you need to add 1 char to get ))
                ctr += c == ')'
                if open_bracket_count:  # Matching ( for ) or ))
                    open_bracket_count -= 1
                else:   # No Matching ( for ) or )). Need to insert ( to balance string
                    ctr += 1
        return ctr + open_bracket_count * 2


def test():
    arguments = [
        "(()))",
        "())",
        "))())(",
        "((((((",
        ")))))))",
    ]
    expectations = [1, 0, 3, 12, 5]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minInsertions(s)
        assert solution == expected
