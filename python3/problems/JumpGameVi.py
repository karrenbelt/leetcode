### Source : https://leetcode.com/problems/jump-game-vi/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-09

##################################################################################################### 
#
# You are given a 0-indexed integer array nums and an integer k.
# 
# You are initially standing at index 0. In one move, you can jump at most k steps forward without 
# going outside the boundaries of the array. That is, you can jump from index i to any index in the 
# range [i + 1, min(n - 1, i + k)] inclusive.
# 
# You want to reach the last index of the array (index n - 1). Your score is the sum of all nums[j] 
# for each index j you visited in the array.
# 
# Return the maximum score you can get.
# 
# Example 1:
# 
# Input: nums = [1,-1,-2,4,-7,3], k = 2
# Output: 7
# Explanation: You can choose your jumps forming the subsequence [1,-1,4,3] (underlined above). The 
# sum is 7.
# 
# Example 2:
# 
# Input: nums = [10,-5,-2,4,0,3], k = 3
# Output: 17
# Explanation: You can choose your jumps forming the subsequence [10,4,3] (underlined above). The sum 
# is 17.
# 
# Example 3:
# 
# Input: nums = [1,-5,-20,4,-1,3,-6,-3], k = 2
# Output: 0
# 
# Constraints:
# 
# 	 1 <= nums.length, k <= 105
# 	-104 <= nums[i] <= 104
#####################################################################################################


from typing import List


class Solution:
    def maxResult(self, nums: List[int], k: int) -> int:  # O(n log n) time and O(n) space
        import heapq
        heap = []
        for i in reversed(range(len(nums))):
            while heap and heap[0][1] - i > k:
                heapq.heappop(heap)
            maximum = nums[i] - heap[0][0] if heap else nums[i]
            heapq.heappush(heap, (-maximum, i))
        return maximum

    def maxResult(self, nums: List[int], k: int) -> int:  # O(n) time and O(k) space
        from collections import deque
        queue = deque()
        for i in reversed(range(len(nums))):
            if queue and queue[0][1] - i > k:
                queue.popleft()
            maximum = nums[i] + queue[0][0] if queue else nums[i]
            while queue and queue[-1][0] <= maximum:
                queue.pop()
            queue.append((maximum, i))
        return maximum


def test():
    arguments = [
        ([1, -1, -2, 4, -7, 3], 2),
        ([10, -5, -2, 4, 0, 3], 3),
        ([1, -5, -20, 4, -1, 3, -6, -3], 2),
    ]
    expectations = [7, 17, 0]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().maxResult(nums, k)
        assert solution == expected
