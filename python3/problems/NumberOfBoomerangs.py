### Source : https://leetcode.com/problems/number-of-boomerangs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-14

##################################################################################################### 
#
# You are given n points in the plane that are all distinct, where points[i] = [xi, yi]. A boomerang 
# is a tuple of points (i, j, k) such that the distance between i and j equals the distance between i 
# and k (the order of the tuple matters).
# 
# Return the number of boomerangs.
# 
# Example 1:
# 
# Input: points = [[0,0],[1,0],[2,0]]
# Output: 2
# Explanation: The two boomerangs are [[1,0],[0,0],[2,0]] and [[1,0],[2,0],[0,0]].
# 
# Example 2:
# 
# Input: points = [[1,1],[2,2],[3,3]]
# Output: 2
# 
# Example 3:
# 
# Input: points = [[1,1]]
# Output: 0
# 
# Constraints:
# 
# 	n == points.length
# 	1 <= n <= 500
# 	points[i].length == 2
# 	-104 <= xi, yi <= 104
# 	All the points are unique.
#####################################################################################################

from typing import List


class Solution:
    def numberOfBoomerangs(self, points: List[List[int]]) -> int:  # O(n^3) time and O(1) space
        # naive approach
        def distance(point_a, point_b):  # no need to take a square root
            return (point_a[0] - point_b[0]) ** 2 + (point_a[1] - point_b[1]) ** 2

        ctr = 0
        for p1 in points:
            for p2 in points:
                for p3 in points:
                    if p1 != p2 and p2 != p3 and p1 != p3:
                        if distance(p1, p2) == distance(p1, p3):
                            ctr += 1
        return ctr

    def numberOfBoomerangs(self, points: List[List[int]]) -> int:  # equally useless: O(n^3) time and O(1) space
        return sum((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2 == (p1[0] - p3[0])**2 + (p1[1] - p3[1])**2
                   for p1 in points for p2 in points for p3 in points if p1 != p2 and p2 != p3 and p1 != p3)

    def numberOfBoomerangs(self, points: List[List[int]]) -> int:  # O(n^2) time and O(n) space
        # for a triplet [a, b, c], we need to consider all 6 permutations
        # note that they come in pairs: if a, b, c forms a boomerang, then a, c, b is also one
        ctr = 0
        for p1 in points:
            counts = {}
            for p2 in points:
                distance = (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2
                if distance not in counts:
                    counts[distance] = 1
                else:
                    ctr += counts[distance]
                    counts[distance] += 1
        return ctr * 2  # since order matters

    def numberOfBoomerangs(self, points: List[List[int]]) -> int:  # O(n^2) time and O(n) space
        from collections import Counter

        def distance(point_a, point_b):  # no need to take a square root
            return (point_a[0] - point_b[0]) ** 2 + (point_a[1] - point_b[1]) ** 2

        ctr = 0
        for p1 in points:
            counts = Counter(distance(p1, p2) for p2 in points)
            ctr += sum(k * (k - 1) for k in counts.values())  # counting permutations
        return ctr

    def numberOfBoomerangs(self, points: List[List[int]]) -> int:  # O(n^2) time and O(n) space
        from collections import Counter
        return sum(sum(k*(k-1) for k in Counter((a-x)**2+(b-y)**2 for x, y in points).values()) for a, b in points)


def test():
    arguments = [
        [[0, 0], [1, 0], [2, 0]],
        [[1, 1], [2, 2], [3, 3]],
        [[1, 1]],
        ]
    expectations = [2, 2, 0]
    for points, expected in zip(arguments, expectations):
        solution = Solution().numberOfBoomerangs(points)
        assert solution == expected, (expected, solution)
test()