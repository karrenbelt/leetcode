### Source : https://leetcode.com/problems/copy-list-with-random-pointer/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-10

##################################################################################################### 
#
# A linked list of length n is given such that each node contains an additional random pointer, which 
# could point to any node in the list, or null.
# 
# Construct a deep copy of the list. The deep copy should consist of exactly n brand new nodes, where 
# each new node has its value set to the value of its corresponding original node. Both the next and 
# random pointer of the new nodes should point to new nodes in the copied list such that the pointers 
# in the original list and copied list represent the same list state. None of the pointers in the new 
# list should point to nodes in the original list.
# 
# For example, if there are two nodes X and Y in the original list, where X.random --> Y, then for 
# the corresponding two nodes x and y in the copied list, x.random --> y.
# 
# Return the head of the copied linked list.
# 
# The linked list is represented in the input/output as a list of n nodes. Each node is represented 
# as a pair of [val, random_index] where:
# 
# 	val: an integer representing Node.val
# 	random_index: the index of the node (range from 0 to n-1) that the random pointer points 
# to, or null if it does not point to any node.
# 
# Your code will only be given the head of the original linked list.
# 
# Example 1:
# 
# Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
# Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]
# 
# Example 2:
# 
# Input: head = [[1,1],[2,1]]
# Output: [[1,1],[2,1]]
# 
# Example 3:
# 
# Input: head = [[3,null],[3,0],[3,null]]
# Output: [[3,null],[3,0],[3,null]]
# 
# Example 4:
# 
# Input: head = []
# Output: []
# Explanation: The given linked list is empty (null pointer), so return null.
# 
# Constraints:
# 
# 	0 <= n <= 1000
# 	-10000 <= Node.val <= 10000
# 	Node.random is null or is pointing to some node in the linked list.
#####################################################################################################

from python3 import SinglyLinkedListNode


class Node(SinglyLinkedListNode):
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        super(Node, self).__init__(x, next)
        self.random = random


class Solution:

    def copyRandomList(self, head: Node) -> Node:  # real-life
        from copy import deepcopy
        return deepcopy(head)

    def copyRandomList(self, head: Node) -> Node:  # using two arrays: O(n) time O(n) space
        if not head:
            return head

        node, ordered, copy = head, [], []
        while node:
            ordered.append(node)
            copy.append(Node(node.val))
            node = node.next

        for i in range(len(copy)-1):
            copy[i].next = copy[i+1]
            if ordered[i].random:
                copy[i].random = copy[ordered.index(ordered[i].random)]
        if ordered[-1].random:
            copy[-1].random = copy[ordered.index(ordered[-1].random)]
        return copy[0]

    # def copyRandomList(self, head: 'Node') -> 'Node':  # with a hash table, faster lookup than list.index
    #     if not head:
    #         return head
    #
    #     hash_table = {}
    #     node = head
    #     while node:
    #         hash_table[node] = Node(node.val)
    #         node = node.next
    #
    #     node = head
    #     while node:
    #         hash_table[node].next = hash_table.get(node.next)
    #         hash_table[node].random = hash_table.get(node.random)
    #         node = node.next
    #
    #     return hash_table[head]

    # def copyRandomList(self, head: 'Node') -> 'Node':  # O(n) time and O(1) space (if we ignore space the copy takes)
    #     if not head:
    #         return head
    #
    #     node = head
    #     while node:
    #         new_node = Node(node.val)
    #         new_node.next = node.next
    #         node.next = new_node
    #         node = node.next.next
    #
    #     node = head
    #     dummy = Node(-1)
    #     while node:
    #         if node.random:
    #             node.next.random = node.random.next
    #         dummy.next = node.next
    #         dummy = dummy.next
    #         node = node.next.next
    #     return dummy.next


def test():
    from python3 import SinglyLinkedList
    SinglyLinkedList.Node = Node
    null = None
    arrays_of_nums = [
        [[7, null], [13, 0], [11, 4], [10, 2], [1, 0]],
        [[1, 1], [2, 1]],
        [[3, null], [3, 0], [3, null]],
        [],
    ]
    for nums in arrays_of_nums:
        next_pointers, random_pointers = zip(*nums)
        llist = SinglyLinkedList()
        llist.extend(next_pointers)
        llist2 = llist.copy()
        for i, j in enumerate(random_pointers):
            llist[i].random = llist[j] if j else None
        assert llist != llist2
        solution = Solution().copyRandomList(llist.head)
        # assert solution is not llist.head
        # assert solution == llist.head
