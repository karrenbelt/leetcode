### Source : https://leetcode.com/problems/minimum-cost-to-move-chips-to-the-same-position/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-06

##################################################################################################### 
#
# We have n chips, where the position of the ith chip is position[i].
# 
# We need to move all the chips to the same position. In one step, we can change the position of the 
# ith chip from position[i] to:
# 
# 	position[i] + 2 or position[i] - 2 with cost = 0.
# 	position[i] + 1 or position[i] - 1 with cost = 1.
# 
# Return the minimum cost needed to move all the chips to the same position.
# 
# Example 1:
# 
# Input: position = [1,2,3]
# Output: 1
# Explanation: First step: ove the chip at position 3 to position 1 with cost = 0.
# Second step: ove the chip at position 2 to position 1 with cost = 1.
# Total cost is 1.
# 
# Example 2:
# 
# Input: position = [2,2,2,3,3]
# Output: 2
# Explanation: We can move the two chips at position  3 to position 2. Each move has cost = 1. The 
# total cost = 2.
# 
# Example 3:
# 
# Input: position = [1,1000000000]
# Output: 1
# 
# Constraints:
# 
# 	1 <= position.length <= 100
# 	1 <= position[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def minCostToMoveChips(self, position: List[int]) -> int:  # O(n) time and O(n) space
        from collections import Counter
        return min(Counter(p % 2 for p in position).values()) * (len(set(position)) > 1)

    def minCostToMoveChips(self, position: List[int]) -> int:  # O(n) time and O(1) space
        return min((odds := sum(p % 2 for p in position)), len(position) - odds)


def test():
    arguments = [
        [1, 2, 3],
        [2, 2, 2, 3, 3],
        [1, 1000000000],
        [11],
    ]
    expectations = [1, 2, 1, 0]
    for position, expected in zip(arguments, expectations):
        solution = Solution().minCostToMoveChips(position)
        assert solution == expected
