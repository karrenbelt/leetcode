### Source : https://leetcode.com/problems/maximum-profit-in-job-scheduling/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-28

##################################################################################################### 
#
# We have n jobs, where every job is scheduled to be done from startTime[i] to endTime[i], obtaining 
# a profit of profit[i].
# 
# You're given the startTime, endTime and profit arrays, return the maximum profit you can take such 
# that there are no two jobs in the subset with overlapping time range.
# 
# If you choose a job that ends at time X you will be able to start another job that starts at time X.
# 
# Example 1:
# 
# Input: startTime = [1,2,3,3], endTime = [3,4,5,6], profit = [50,10,40,70]
# Output: 120
# Explanation: The subset chosen is the first and fourth job. 
# Time range [1-3]+[3-6] , we get profit of 120 = 50 + 70.
# 
# Example 2:
# 
# Input: startTime = [1,2,3,4,6], endTime = [3,5,10,6,9], profit = [20,20,100,70,60]
# Output: 150
# Explanation: The subset chosen is the first, fourth and fifth job. 
# Profit obtained 150 = 20 + 70 + 60.
# 
# Example 3:
# 
# Input: startTime = [1,1,1], endTime = [2,3,4], profit = [5,6,4]
# Output: 6
# 
# Constraints:
# 
# 	1 <= startTime.length == endTime.length == profit.length <= 5 * 104
# 	1 <= startTime[i] < endTime[i] <= 109
# 	1 <= profit[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:  # O(n log n) time
        import heapq

        jobs = sorted(zip(startTime, endTime, profit))
        heap, total_profit = [], 0
        for start, end, value in jobs:
            while heap and heap[0][0] <= start:
                _, prev_profit = heapq.heappop(heap)
                total_profit = max(total_profit, prev_profit)
            heapq.heappush(heap, (end, total_profit + value))

        while heap:
            _, prev_profit = heapq.heappop(heap)
            total_profit = max(total_profit, prev_profit)
        return total_profit

    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:  # O(n log n) time
        import bisect

        jobs = sorted(zip(endTime, startTime, profit))
        max_profit = [[0, 0]]
        for end, start, value in jobs:
            if max_profit:
                i = bisect.bisect(max_profit, [start + 1])
                temp = max_profit[i - 1][1] + value
                if temp > max_profit[-1][1]:
                    max_profit.append([end, temp])
            else:
                max_profit.append([end, value])
        return max_profit[-1][1]


def test():
    arguments = [
        ([1, 2, 3, 3], [3, 4, 5, 6], [50, 10, 40, 70]),
        ([1, 2, 3, 4, 6], [3, 5, 10, 6, 9], [20, 20, 100, 70, 60]),
        ([1, 1, 1], [2, 3, 4], [5, 6, 4]),
    ]
    expectations = [120, 150, 6]
    for (startTime, endTime, profit), expected in zip(arguments, expectations):
        solution = Solution().jobScheduling(startTime, endTime, profit)
        assert solution == expected
