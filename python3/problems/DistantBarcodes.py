### Source : https://leetcode.com/problems/distant-barcodes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-20

##################################################################################################### 
#
# In a warehouse, there is a row of barcodes, where the ith barcode is barcodes[i].
# 
# Rearrange the barcodes so that no two adjacent barcodes are equal. You may return any answer, and 
# it is guaranteed an answer exists.
# 
# Example 1:
# Input: barcodes = [1,1,1,2,2,2]
# Output: [2,1,2,1,2,1]
# Example 2:
# Input: barcodes = [1,1,1,1,2,2,3,3]
# Output: [1,3,1,3,1,2,1,2]
# 
# Constraints:
# 
# 	1 <= barcodes.length <= 10000
# 	1 <= barcodes[i] <= 10000
#####################################################################################################

from typing import List


class Solution:
    def rearrangeBarcodes(self, barcodes: List[int]) -> List[int]:

        def count(array: List[int]) -> dict:
            d = {}
            for n in array:
                d[n] = d.get(n, 0) + 1
            return d

        freq = [k for k, v in sorted(count(barcodes).items(), key=lambda x: x[1]) for _ in range(v)]
        for i in range(0, len(barcodes), 2):
            barcodes[i] = freq.pop()
        for i in range(1, len(barcodes), 2):
            barcodes[i] = freq.pop()
        return barcodes

    def rearrangeBarcodes(self, barcodes: List[int]) -> List[int]:
        from collections import Counter
        count = Counter(barcodes)
        barcodes.sort(key=lambda a: (count[a], a))
        barcodes[1::2], barcodes[::2] = barcodes[0:len(barcodes) // 2], barcodes[len(barcodes) // 2:]
        return barcodes


def test():
    arguments = [
        [1, 1, 1, 2, 2, 2],
        [1, 1, 1, 1, 2, 2, 3, 3],
        [1, 1, 1, 2, 3],
        [1, 2, 3, 3, 3],
        [1, 2, 3, 1, 1],
        [1, 1, 1, 2, 2, 2],
        [2, 2, 1, 3],
    ]
    expectations = [
        [2, 1, 2, 1, 2, 1],
        [1, 3, 1, 3, 1, 2, 1, 2],
        [1, 2, 1, 3, 1],
        [3, 1, 3, 2, 3],
        [1, 2, 1, 3, 1],
        [2, 1, 2, 1, 2, 1],
        [1],
        [2, 1, 2, 3]
    ]
    for barcodes, expected in zip(arguments, expectations):
        solution = Solution().rearrangeBarcodes(barcodes)
        # assert solution == expected
        assert all(solution[i] != solution[i + 1] for i in range(len(solution) - 1))
