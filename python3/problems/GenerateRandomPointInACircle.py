### Source : https://leetcode.com/problems/generate-random-point-in-a-circle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-17

##################################################################################################### 
#
# Given the radius and x-y positions of the center of a circle, write a function randPoint which 
# generates a uniform random point in the circle.
# 
# Note:
# 
# 	input and output values are in floating-point.
# 	radius and x-y position of the center of the circle is passed into the class constructor.
# 	a point on the circumference of the circle is considered to be in the circle.
# 	randPoint returns a size 2 array containing x-position and y-position of the random point, 
# in that order.
# 
# Example 1:
# 
# Input: 
# ["Solution","randPoint","randPoint","randPoint"]
# [[1,0,0],[],[],[]]
# Output: [null,[-0.72939,-0.65505],[-0.78502,-0.28626],[-0.83119,-0.19803]]
# 
# Example 2:
# 
# Input: 
# ["Solution","randPoint","randPoint","randPoint"]
# [[10,5,-7.5],[],[],[]]
# Output: [null,[11.52438,-8.33273],[2.46992,-16.21705],[11.13430,-12.42337]]
# 
# Explanation of Input Syntax:
# 
# The input is two lists: the subroutines called and their arguments. Solution's constructor has 
# three arguments, the radius, x-position of the center, and y-position of the center of the circle. 
# randPoint has no arguments. Arguments are always wrapped with a list, even if there aren't any.
# 
#####################################################################################################

from typing import List
import random
global seed


class Solution:

    def __init__(self, radius: float, x_center: float, y_center: float):
        self.radius = radius
        self.x_center = x_center
        self.y_center = y_center

    def randPoint(self) -> List[float]:
        # we need two random points: a random angle and a random radius
        import math
        angle = 2 * math.pi * random.random()
        radius = self.radius * math.sqrt(random.random())
        x = radius * math.cos(angle) + self.x_center
        y = radius * math.sin(angle) + self.y_center
        return [x, y]

    # def randPoint(self) -> List[float]:  # NOTE: seed does not correspond with this implementation
    #     while True:
    #         a, b = random.uniform(-self.radius, self.radius), random.uniform(-self.radius, self.radius)
    #         if a ** 2 + b ** 2 <= self.radius ** 2:
    #             return [a + self.x_center, b + self.y_center]

# Your Solution object will be instantiated and called as such:
# obj = Solution(radius, x_center, y_center)
# param_1 = obj.randPoint()


def test():  # TODO: seed
    seed = 123
    operations = ["Solution", "randPoint", "randPoint", "randPoint"]
    arguments = [[10, 5, -7.5], [], [], []]
    expectations = [
        None,
        [7.7943618387487845, -6.545950909854348],
        [2.2599983701699156, -5.69378446403194],
        [6.588853539961072, -8.636182660836457],
        ]
    obj = Solution(*arguments[0])
    random.seed(seed)
    for i, (method, args, expected) in enumerate(zip(operations[1:], arguments[1:], expectations[1:])):
        assert getattr(obj, method)(*args) == expected
