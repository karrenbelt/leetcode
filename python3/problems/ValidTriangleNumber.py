### Source : https://leetcode.com/problems/valid-triangle-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-15

##################################################################################################### 
#
# Given an integer array nums, return the number of triplets chosen from the array that can make 
# triangles if we take them as side lengths of a triangle.
# 
# Example 1:
# 
# Input: nums = [2,2,3,4]
# Output: 3
# Explanation: Valid combinations are: 
# 2,3,4 (using the first 2)
# 2,3,4 (using the second 2)
# 2,2,3
# 
# Example 2:
# 
# Input: nums = [4,2,3,4]
# Output: 4
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	0 <= nums[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    # similar to leetcode 259. 3Sum Smaller (premium)
    def triangleNumber(self, nums: List[int]) -> int:  # O(n^3) time: TLE -> 219 / 233 test cases passed.
        # triangle inequality: sum of smaller two sides should be greater than the third
        return nums.sort() or sum(nums[i] + nums[j] > nums[k] for i in range(len(nums) - 2)
                                  for j in range(i + 1, len(nums) - 1) for k in range(j + 1, len(nums)))

    def triangleNumber(self, nums: List[int]) -> int:  # O(n^2) time and O(1) space
        # moving window
        ctr = nums.sort() or 0
        for i in range(2, len(nums)):
            left, right = 0, i - 1
            while left < right:
                if nums[left] + nums[right] > nums[i]:
                    ctr += (right - left)
                    right -= 1
                else:
                    left += 1
        return ctr


def test():
    arguments = [
        [2, 2, 3, 4],
        [4, 2, 3, 4],
    ]
    expectations = [3, 4]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().triangleNumber(nums)
        assert solution == expected
