### Source : https://leetcode.com/problems/binary-tree-right-side-view/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-09

##################################################################################################### 
#
# Given a binary tree, imagine yourself standing on the right side of it, return the values of the 
# nodes you can see ordered from top to bottom.
# 
# Example:
# 
# Input: [1,2,3,null,5,null,4]
# Output: [1, 3, 4]
# Explanation:
# 
#    1            <---
#  /   \
# 2     3         <---
#  \     \
#   5     4       <---
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:

        def traverse(node: TreeNode, level: int = 0):
            if not node:
                return
            if level > len(levels) - 1:
                levels.append([])
            levels[level].append(node.val)
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root)
        return [level[-1] for level in levels]


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,2,3,null,5,null,4]",
        "[1,null,3]",
        "[]",
        ]
    expectations = [
        [1, 3, 4],
        [1, 3],
        [],
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().rightSideView(root)
        assert solution == expected
