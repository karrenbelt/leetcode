### Source : https://leetcode.com/problems/house-robber-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# You are a professional robber planning to rob houses along a street. Each house has a certain 
# amount of money stashed. All houses at this place are arranged in a circle. That means the first 
# house is the neighbor of the last one. eanwhile, adjacent houses have a security system connected, 
# and it will automatically contact the police if two adjacent houses were broken into on the same 
# night.
# 
# Given an integer array nums representing the amount of money of each house, return the maximum 
# amount of money you can rob tonight without alerting the police.
# 
# Example 1:
# 
# Input: nums = [2,3,2]
# Output: 3
# Explanation: You cannot rob house 1 (money = 2) and then rob house 3 (money = 2), because they are 
# adjacent houses.
# 
# Example 2:
# 
# Input: nums = [1,2,3,1]
# Output: 4
# Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
# Total amount you can rob = 1 + 3 = 4.
# 
# Example 3:
# 
# Input: nums = [0]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def rob(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        # cannot rob first and last house
        def robbery(i: int, j: int):
            prev = current = 0
            for idx in range(i, j):
                prev, current = current, max(prev + nums[idx], current)
            return current

        return max(robbery(0, len(nums) - 1), robbery(1, len(nums))) if len(nums) > 1 else nums[0] if nums else 0


def test():
    arguments = [
        [2, 3, 2],
        [1, 2, 3, 1],
        [0],
        ]
    expectations = [3, 4, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().rob(nums)
        assert solution == expected
