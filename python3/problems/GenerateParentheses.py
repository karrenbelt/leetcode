### Source : https://leetcode.com/problems/generate-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-21
#####################################################################################################
#
# Given n pairs of parentheses, write a function to generate all combinations of well-formed
# parentheses.
#
# Example 1:
# Input: n = 3
# Output: ["((()))","(()())","(())()","()(())","()()()"]
# Example 2:
# Input: n = 1
# Output: ["()"]
#
# Constraints:
#
# 	1 <= n <= 8
#####################################################################################################

from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def is_well_formed(s: str) -> bool:
            stack = []
            for c in s:
                if c == '(':
                    stack.append('(')
                elif stack:
                    stack.pop()
                else:
                    return False
            return not stack

        def backtracking(path: str):
            if len(path) == n * 2:
                if is_well_formed(path):
                    paths.append(path)
            else:
                backtracking(path + ')')
                backtracking(path + '(')

        paths = []
        backtracking('')
        return paths

    def generateParenthesis(self, n: int) -> List[str]:  # catalan(n) * O(n) which is O(4^n / n^1.5) * n)
        # always open first, close after in all combinations or order
        # 1: ()
        # 2: (()), ()()
        # 3: ()()(), (())(), (()()), ()(()), ((()))

        def dfs(p, left, right, ans):
            if left:
                dfs(p + '(', left - 1, right, ans)
            if right > left:
                dfs(p + ')', left, right - 1, ans)
            if not right:  # all brackets closed, append the solution
                ans.append(p)
            return ans

        return dfs('', n, n, [])

    def generateParenthesis(self, n: int) -> List[str]:  # iterative
        ans = ['()']
        for i in range(2, n + 1):
            new = set()
            for each in ans:
                for j in range(len(each) + 1):
                    new.add(each[:j] + '()' + each[j:])
            ans = list(new)
        return ans

    def generateParenthesis(self, n: int) -> List[str]:  # iterative
        ans = []
        stack = [("(", 1, 0)]
        while stack:
            p, left, right = stack.pop()
            if left - right < 0 or left > n or right > n:
                continue
            if left == right == n:
                ans.append(p)
            stack.append((p + "(", left + 1, right))
            stack.append((p + ")", left, right + 1))
        return ans

    def generateParenthesis(self, n: int) -> List[str]:  # dp
        dp = [[] for _ in range(n + 1)]
        dp[0].append('')
        for i in range(n + 1):
            for j in range(i):
                dp[i] += ['(' + x + ')' + y for x in dp[j] for y in dp[i - j - 1]]
        return dp[n]

    def generateParenthesis(self, n: int) -> List[str]:  # dp
        # 1 and 2 is base cases (set is used to deduplicate candidates)
        dp = {1: {'()'}, 2: {'(())', '()()'}}
        for i in range(3, n+1):  # pattern 1: outer parenthese + subproblem with length - 1
            dp[i] = {'(' + x + ')' for x in dp[i-1]}
            for j in range(1, i):  # pattern 2: dp[i] is formed by dp[j] + dp[i-j]
                dp[i] = dp[i].union(x + y for x in dp[j] for y in dp[i-j])
        return list(dp[n])


def test():
    numbers = [3, 1, 4]
    expectations = [
        ["((()))", "(()())", "(())()", "()(())", "()()()"],
        ["()"],
        ['(((())))', '((()()))', '((())())', '((()))()', '(()(()))', '(()()())', '(()())()', '(())(())', '(())()()',
         '()((()))', '()(()())', '()(())()', '()()(())', '()()()()'],
        ]
    for n, expected in zip(numbers, expectations):
        solution = Solution().generateParenthesis(n)
        assert set(solution) == set(expected)
