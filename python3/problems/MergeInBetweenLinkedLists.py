### Source : https://leetcode.com/problems/merge-in-between-linked-lists/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-20

##################################################################################################### 
#
# You are given two linked lists: list1 and list2 of sizes n and m respectively.
# 
# Remove list1's nodes from the ath node to the bth node, and put list2 in their place.
# 
# The blue edges and nodes in the following figure incidate the result:
# 
# Build the result list and return its head.
# 
# Example 1:
# 
# Input: list1 = [0,1,2,3,4,5], a = 3, b = 4, list2 = [1000000,1000001,1000002]
# Output: [0,1,2,1000000,1000001,1000002,5]
# Explanation: We remove the nodes 3 and 4 and put the entire list2 in their place. The blue edges 
# and nodes in the above figure indicate the result.
# 
# Example 2:
# 
# Input: list1 = [0,1,2,3,4,5,6], a = 2, b = 5, list2 = [1000000,1000001,1000002,1000003,1000004]
# Output: [0,1,1000000,1000001,1000002,1000003,1000004,6]
# Explanation: The blue edges and nodes in the above figure indicate the result.
# 
# Constraints:
# 
# 	3 <= list1.length <= 104
# 	1 <= a <= b < list1.length - 1
# 	1 <= list2.length <= 104
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def mergeInBetween(self, list1: ListNode, a: int, b: int, list2: ListNode):  # O(n + m) time and O(1) space

        prev_node = None
        node = list1
        for _ in range(a):
            prev_node = node
            node = node.next

        for _ in range(b - a):
            node = node.next
        next_node = node.next

        if prev_node:
            prev_node.next = list2
        else:
            list1 = list2

        node = list2
        while node.next:
            node = node.next
        node.next = next_node

        return list1


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([0, 1, 2, 3, 4, 5], 3, 4, [1000000, 1000001, 1000002]),
        ([0, 1, 2, 3, 4, 5, 6], 2, 5, [1000000, 1000001, 1000002, 1000003, 1000004]),
    ]
    expectations = [
        [0, 1, 2, 1000000, 1000001, 1000002, 5],
        [0, 1, 1000000, 1000001, 1000002, 1000003, 1000004, 6],
    ]
    for (nums1, a, b, nums2), expected in zip(arguments, expectations):
        list1, list2 = (SinglyLinkedList(nums).head for nums in (nums1, nums2))
        solution = Solution().mergeInBetween(list1, a, b, list2)
        assert solution == SinglyLinkedList(expected).head
