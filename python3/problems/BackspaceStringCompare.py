### Source : https://leetcode.com/problems/backspace-string-compare/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given two strings S and T, return if they are equal when both are typed into empty text editors. # 
# means a backspace character.
# 
# Example 1:
# 
# Input: S = "ab#c", T = "ad#c"
# Output: true
# Explanation: Both S and T become "ac".
# 
# Example 2:
# 
# Input: S = "ab##", T = "c#d#"
# Output: true
# Explanation: Both S and T become "".
# 
# Example 3:
# 
# Input: S = "a##c", T = "#a#c"
# Output: true
# Explanation: Both S and T become "c".
# 
# Example 4:
# 
# Input: S = "a#c", T = "b"
# Output: false
# Explanation: S becomes "c" while T becomes "b".
# 
# Note:
# 
# 	1 <= S.length <= 200
# 	1 <= T.length <= 200
# 	S and T only contain lowercase letters and '#' characters.
# 
# Follow up:
# 
# 	Can you solve it in O(N) time and O(1) space?
# 
#####################################################################################################


class Solution:
    def backspaceCompare(self, S: str, T: str) -> bool:  # O(m + n) time and O(m + n) space -> 24 ms

        def perform_back_space(s: str):
            stack = []
            for c in s:
                if c != '#':
                    stack.append(c)
                elif stack:
                    stack.pop()
            return stack

        return perform_back_space(S) == perform_back_space(T)

    def backspaceCompare(self, S: str, T: str) -> bool:  # O(n) time and O(1) space -> 48 ms

        def read_backwards(string: str) -> str:
            backspaces = 0
            for c in reversed(string):
                if c == '#':
                    backspaces += 1
                elif backspaces:
                    backspaces -= 1
                else:
                    yield c

        s_gen, t_gen = read_backwards(S), read_backwards(T)
        while all(((s := next(s_gen, None)), (t := next(t_gen, None)))):
            if not s == t:
                break
        return s == t

    def backspaceCompare(self, S: str, T: str) -> bool:  # O(n) time and O(1) space -> 60ms
        from functools import reduce

        def parse(s: str):
            return reduce(lambda parsed, c: parsed[:-1] if c == '#' else parsed + c, s, '')

        return parse(S) == parse(T)


def test():
    arguments = [
        ("ab#c", "ad#c"),
        ("ab##", "c#d#"),
        ("a##c", "#a#c"),
        ("a#c", "b"),
        ("a", "bac#"),
    ]
    expectations = [True, True, True, False, False]
    for (S, T), expected in zip(arguments, expectations):
        solution = Solution().backspaceCompare(S, T)
        assert solution == expected, solution
