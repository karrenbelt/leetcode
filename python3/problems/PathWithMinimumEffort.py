### Source : https://leetcode.com/problems/path-with-minimum-effort/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-13

##################################################################################################### 
#
# You are a hiker preparing for an upcoming hike. You are given heights, a 2D array of size rows x 
# columns, where heights[row][col] represents the height of cell (row, col). You are situated in the 
# top-left cell, (0, 0), and you hope to travel to the bottom-right cell, (rows-1, columns-1) (i.e., 
# 0-indexed). You can move up, down, left, or right, and you wish to find a route that requires the 
# minimum effort.
# 
# A route's effort is the maximum absolute difference in heights between two consecutive cells of the 
# route.
# 
# Return the minimum effort required to travel from the top-left cell to the bottom-right cell.
# 
# Example 1:
# 
# Input: heights = [[1,2,2],[3,8,2],[5,3,5]]
# Output: 2
# Explanation: The route of [1,3,5,3,5] has a maximum absolute difference of 2 in consecutive cells.
# This is better than the route of [1,2,2,2,5], where the maximum absolute difference is 3.
# 
# Example 2:
# 
# Input: heights = [[1,2,3],[3,8,4],[5,3,5]]
# Output: 1
# Explanation: The route of [1,2,3,4,5] has a maximum absolute difference of 1 in consecutive cells, 
# which is better than route [1,3,5,3,5].
# 
# Example 3:
# 
# Input: heights = [[1,2,1,1,1],[1,2,1,2,1],[1,2,1,2,1],[1,2,1,2,1],[1,1,1,2,1]]
# Output: 0
# Explanation: This route does not require any effort.
# 
# Constraints:
# 
# 	rows == heights.length
# 	columns == heights[i].length
# 	1 <= rows, columns <= 100
# 	1 <= heights[i][j] <= 106
#####################################################################################################

from typing import List


class Solution:
    def minimumEffortPath(self, heights: List[List[int]]) -> int:  # O(mn log(mn)) time and O(mn) space
        import heapq
        queue = [(0, 0, 0)]
        seen = set()
        while queue:
            d, i, j = heapq.heappop(queue)
            if (i, j) not in seen:  # check here else: TLE: 15 / 75 test cases passed.
                if i == len(heights) - 1 and j == len(heights[0]) - 1:
                    return d
                seen.add((i, j))
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < len(heights) and 0 <= y < len(heights[0]) and (x, y) not in seen:
                        heapq.heappush(queue, (max(d, abs(heights[i][j] - heights[x][y])), x, y))

    def minimumEffortPath(self, heights: List[List[int]]) -> int:  # O(m^2 * n^2), space: O(mn)
        # improved Bellman-Ford: Shortest Path Faster Algorithm
        # https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
        # https://en.wikipedia.org/wiki/Shortest_Path_Faster_Algorithm
        from collections import deque
        m, n = map(len, (heights, heights[0]))
        efforts = [[1 << 31 -1] * n for _ in range(m)]
        efforts[0][0] = 0
        queue = deque([(0, 0)])
        while queue:
            x, y = queue.popleft()
            for r, c in (x, y + 1), (x, y - 1), (x + 1, y), (x - 1, y):
                if m > r >= 0 <= c < n:
                    next_effort = max(efforts[x][y], abs(heights[r][c] - heights[x][y]))
                    if efforts[r][c] > next_effort:
                        efforts[r][c] = next_effort
                        queue.append((r, c))
        return efforts[-1][-1]

    def minimumEffortPath(self, heights: List[List[int]]) -> int:  #
        # Kruskal's Minimum Spanning Tree Algorithm
        # TODO: Quick Union, Quick Find, Weighted Union-Find, Weighted Union-Find with path compression
        import heapq

        class WeightedUnionFind:
            # weighted version of the Union-Find with path compression
            def __init__(self, n: int):
                self.ids = list(range(n))
                self.weights = [0] * n

            def union(self, p: int, q: int):
                parent_p = self.find(p)
                parent_q = self.find(q)
                if parent_p == parent_q:
                    return
                if self.weights[parent_p] > self.weights[parent_q]:
                    parent_p, parent_q = parent_q, parent_p
                self.ids[parent_p] = parent_q
                self.weights[parent_q] += self.weights[parent_p]

            def find(self, p: int) -> int:
                while p != self.ids[p]:
                    self.ids[p] = self.ids[self.ids[p]]
                    p = self.ids[p]
                return p

            def connected(self, p: int, q: int) -> bool:
                return self.find(p) == self.find(q)

        def collect_edges(grid: List[List[int]]) -> List[tuple]:
            edges = []
            for i, j in ((i, j) for i in range(len(grid)) for j in range(len(grid[0]))):
                from_id = i * len(grid[0]) + j
                if i:
                    diff = abs(grid[i][j] - grid[i - 1][j])
                    to_id = (i - 1) * len(grid[0]) + j
                    edges.append((diff, from_id, to_id))
                if j:
                    diff = abs(grid[i][j] - grid[i][j - 1])
                    to_id = i * len(grid[0]) + j - 1
                    edges.append((diff, from_id, to_id))
            return edges

        edges = collect_edges(heights)
        heapq.heapify(edges)
        uf = WeightedUnionFind(len(heights) * len(heights[0]))
        weight = source = 0
        target = len(heights) * len(heights[0]) - 1
        while not uf.connected(source, target):
            weight, root, child = heapq.heappop(edges)
            uf.union(root, child)
        return weight


def test():
    arguments = [
        [[1, 2, 2], [3, 8, 2], [5, 3, 5]],
        [[1, 2, 3], [3, 8, 4], [5, 3, 5]],
        [[1, 2, 1, 1, 1], [1, 2, 1, 2, 1], [1, 2, 1, 2, 1], [1, 2, 1, 2, 1], [1, 1, 1, 2, 1]],
    ]
    expectations = [2, 1, 0]
    for heights, expected in zip(arguments, expectations):
        solution = Solution().minimumEffortPath(heights)
        assert solution == expected
