### Source : https://leetcode.com/problems/sum-of-even-numbers-after-queries/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-06

##################################################################################################### 
#
# We have an array nums of integers, and an array queries of queries.
# 
# For the i-th query val = queries[i][0], index = queries[i][1], we add val to nums[index].  Then, 
# the answer to the i-th query is the sum of the even values of A.
# 
# (Here, the given index = queries[i][1] is a 0-based index, and each query permanently modifies the 
# array nums.)
# 
# Return the answer to all queries.  Your answer array should have answer[i] as the answer to the 
# i-th query.
# 
# Example 1:
# 
# Input: nums = [1,2,3,4], queries = [[1,0],[-3,1],[-4,0],[2,3]]
# Output: [8,6,2,4]
# Explanation: 
# At the beginning, the array is [1,2,3,4].
# After adding 1 to nums[0], the array is [2,2,3,4], and the sum of even values is 2 + 2 + 4 = 8.
# After adding -3 to nums[1], the array is [2,-1,3,4], and the sum of even values is 2 + 4 = 6.
# After adding -4 to nums[0], the array is [-2,-1,3,4], and the sum of even values is -2 + 4 = 2.
# After adding 2 to nums[3], the array is [-2,-1,3,6], and the sum of even values is -2 + 6 = 4.
# 
# Note:
# 
# 	1 <= nums.length <= 10000
# 	-10000 <= nums[i] <= 10000
# 	1 <= queries.length <= 10000
# 	-10000 <= queries[i][0] <= 10000
# 	0 <= queries[i][1] < nums.length
#####################################################################################################

from typing import List


class Solution:
    def sumEvenAfterQueries(self, nums: List[int], queries: List[List[int]]) -> List[int]:  # TLE
        even_values_sums = []
        for v, i in queries:
            nums[i] += v
            even_values_sums.append(sum(x for x in nums if not x % 2))
        return even_values_sums

    def sumEvenAfterQueries(self, nums: List[int], queries: List[List[int]]) -> List[int]:  # O(n) time
        even_values_sums = []
        current = sum(filter(lambda x: not x % 2, nums))
        for v, i in queries:
            current -= not nums[i] % 2 and nums[i]
            nums[i] += v
            current += not nums[i] % 2 and nums[i]
            even_values_sums.append(current)
        return even_values_sums


def test():
    arguments = [
        ([1, 2, 3, 4], [[1, 0], [-3, 1], [-4, 0], [2, 3]]),
    ]
    expectations = [
        [8, 6, 2, 4]
    ]
    for (nums, queries), expected in zip(arguments, expectations):
        solution = Solution().sumEvenAfterQueries(nums, queries)
        assert solution == expected
