### Source : https://leetcode.com/problems/find-largest-value-in-each-tree-row/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Given the root of a binary tree, return an array of the largest value in each row of the tree 
# (0-indexed).
# 
# Example 1:
# 
# Input: root = [1,3,2,5,3,null,9]
# Output: [1,3,9]
# 
# Example 2:
# 
# Input: root = [1,2,3]
# Output: [1,3]
# 
# Example 3:
# 
# Input: root = [1]
# Output: [1]
# 
# Example 4:
# 
# Input: root = [1,null,2]
# Output: [1,2]
# 
# Example 5:
# 
# Input: root = []
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the tree will be in the range [0, 104].
# 	-231 <= Node.val <= 231 - 1
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def largestValues(self, root: TreeNode) -> List[int]:

        def traverse(node: TreeNode, level: int):
            if not node:
                return
            if level == len(levels):
                levels.append([])
            levels[level].append(node.val)
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root, 0)
        return list(map(max, levels))

    def largestValues(self, root: TreeNode) -> List[int]:
        maxes = []
        row = [root]
        while any(row):
            maxes.append(max(node.val for node in row))
            row = [kid for node in row for kid in (node.left, node.right) if kid]
        return maxes


def test():
    from python3 import Codec
    arguments = [
        "[1,3,2,5,3,null,9]",
        "[1,2,3]",
        "[1]",
        "[1,null,2]",
        "[]",
        ]
    expectations = [
        [1, 3, 9],
        [1, 3],
        [1],
        [1, 2],
        [],
        ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().largestValues(root)
        assert solution == expected
