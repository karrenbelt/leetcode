### Source : https://leetcode.com/problems/implement-stack-using-queues/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Implement a last in first out (LIFO) stack using only two queues. The implemented stack should 
# support all the functions of a normal queue (push, top, pop, and empty).
# 
# Implement the yStack class:
# 
# 	void push(int x) Pushes element x to the top of the stack.
# 	int pop() Removes the element on the top of the stack and returns it.
# 	int top() Returns the element on the top of the stack.
# 	boolean empty() Returns true if the stack is empty, false otherwise.
# 
# Notes:
# 
# 	You must use only standard operations of a queue, which means only push to back, peek/pop 
# from front, size, and is empty operations are valid.
# 	Depending on your language, the queue may not be supported natively. You may simulate a 
# queue using a list or deque (double-ended queue), as long as you use only a queue's standard 
# operations.
# 
# Example 1:
# 
# Input
# ["yStack", "push", "push", "top", "pop", "empty"]
# [[], [1], [2], [], [], []]
# Output
# [null, null, null, 2, 2, false]
# 
# Explanation
# yStack myStack = new yStack();
# myStack.push(1);
# myStack.push(2);
# myStack.top(); // return 2
# myStack.pop(); // return 2
# myStack.empty(); // return False
# 
# Constraints:
# 
# 	1 <= x <= 9
# 	At most 100 calls will be made to push, pop, top, and empty.
# 	All the calls to pop and top are valid.
# 
# Follow-up: Can you implement the stack such that each operation is amortized O(1) time complexity? 
# In other words, performing n operations will take overall O(n) time even if one of those operations 
# may take longer. You can use more than two queues.
#####################################################################################################

from queue import Queue


class MyStack:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.q1 = Queue()
        self.q2 = Queue()

    def push(self, x: int) -> None:  # O(1)
        """
        Push element x onto stack.
        """
        self.q2.put(x)
        while not self.q1.empty():
            self.q2.put(self.q1.get())
        self.q1, self.q2 = self.q2, self.q1

    def pop(self) -> int:  # O(n)
        """
        Removes the element on top of the stack and returns that element.
        """
        if not self.q1.empty():
            return self.q1.get()

    def top(self) -> int:
        """
        Get the top element.
        """
        return self.q1.queue[0]

    def size(self) -> int:
        return self.q1._qsize() + self.q2._qsize()

    def empty(self) -> bool:
        """
        Returns whether the stack is empty.
        """
        return self.q1.empty() and self.q2.empty()


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()


def test():
    operations = ["MyStack", "push", "push", "top", "pop", "empty"]
    arguments = [[], [1], [2], [], [], []]
    expectations = [None, None, None, 2, 2, False]
    obj = MyStack()
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
