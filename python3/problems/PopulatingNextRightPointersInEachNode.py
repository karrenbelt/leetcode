### Source : https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# You are given a perfect binary tree where all leaves are on the same level, and every parent has 
# two children. The binary tree has the following definition:
# 
# struct Node {
#   int val;
#   Node *left;
#   Node *right;
#   Node *next;
# }
# 
# Populate each next pointer to point to its next right node. If there is no next right node, the 
# next pointer should be set to NULL.
# 
# Initially, all next pointers are set to NULL.
# 
# Follow up:
# 
# 	You may only use constant extra space.
# 	Recursive approach is fine, you may assume implicit stack space does not count as extra 
# space for this problem.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,6,7]
# Output: [1,#,2,3,#,4,5,6,7,#]
# Explanation: Given the above perfect binary tree (Figure A), your function should populate each 
# next pointer to point to its next right node, just like in Figure B. The serialized output is in 
# level order as connected by the next pointers, with '#' signifying the end of each level.
# 
# Constraints:
# 
# 	The number of nodes in the given tree is less than 4096.
# 	-1000 <= node.val <= 1000
#####################################################################################################

from python3 import BinaryTreeNode


class Node(BinaryTreeNode):
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        super().__init__(val, left, right)
        self.next = next


class Solution:
    def connect(self, root: 'Node') -> 'Node':  # O(n) time and O(w) space
        from collections import deque

        queue = deque([root])
        while root and queue:
            level = []
            for _ in range(len(queue)):
                level.append(queue.popleft())
                queue.extend(filter(None, (level[-1].left, level[-1].right)))
            for i in range(len(level) - 1):
                level[i].next = level[i + 1]
        return root

    def connect(self, root: 'Node') -> 'Node':  # O(n) time and O(h) space

        def dfs(node: 'Node'):
            if node:
                if node.left:
                    node.left.next = node.right
                    if node.next:
                        node.right.next = node.next.left
                self.connect(node.left)
                self.connect(node.right)

        return dfs(root) or root

    def connect(self, root: 'Node') -> 'Node':  # O(n) time and O(1) space
        node = root
        while node and node.left:
            next_node = node.left
            while node:
                node.left.next = node.right
                node.right.next = node.next and node.next.left
                node = node.next
            node = next_node
        return root


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,2,3,4,5,null,7]",
        ]
    expectations = [
        "[1,#,2,3,#,4,5,7,#]",
    ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        # solution = Solution().connect(root)
        # assert solution == Codec.deserialize(expected)
