### Source : https://leetcode.com/problems/course-schedule/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are 
# given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course 
# bi first if you want to take course ai.
# 
# 	For example, the pair [0, 1], indicates that to take course 0 you have to first take course 
# 1.
# 
# Return true if you can finish all courses. Otherwise, return false.
# 
# Example 1:
# 
# Input: numCourses = 2, prerequisites = [[1,0]]
# Output: true
# Explanation: There are a total of 2 courses to take. 
# To take course 1 you should have finished course 0. So it is possible.
# 
# Example 2:
# 
# Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
# Output: false
# Explanation: There are a total of 2 courses to take. 
# To take course 1 you should have finished course 0, and to take course 0 you should also have 
# finished course 1. So it is impossible.
# 
# Constraints:
# 
# 	1 <= numCourses <= 105
# 	0 <= prerequisites.length <= 5000
# 	prerequisites[i].length == 2
# 	0 <= ai, bi < numCourses
# 	All the pairs prerequisites[i] are unique.
#####################################################################################################

from typing import List


class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        # we build a graph, check for cycles
        graph = {}
        indegrees = dict.fromkeys(range(numCourses), 0)
        for a, b in prerequisites:
            graph.setdefault(a, set()).add(b)
            indegrees[b] += 1

        # find starting point
        seen = set()
        queue = [k for k, v in indegrees.items() if v == 0]
        while queue:
            new_queue = []
            for node in queue:
                seen.add(node)
                for neighbor in graph.get(node, []):
                    indegrees[neighbor] -= 1
                    if indegrees[neighbor] == 0:
                        new_queue.append(neighbor)
            queue = new_queue
        return len(seen) == numCourses


def test():
    arguments = [
        (2, [[1, 0]]),
        (2, [[1, 0], [0, 1]]),
        (3, [[0, 1], [1, 2], [2, 0]]),
        (20, [[0, 10], [3, 18], [5, 5], [6, 11], [11, 14], [13, 1], [15, 1], [17, 4]])
        ]
    expectations = [True, False, False, False]
    for (numCourses, prerequisites), expected in zip(arguments, expectations):
        solution = Solution().canFinish(numCourses, prerequisites)
        assert solution == expected
