### Source : https://leetcode.com/problems/minimum-ascii-delete-sum-for-two-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

##################################################################################################### 
#
# Given two strings s1 and s2, return the lowest ASCII sum of deleted characters to make two strings 
# equal.
# 
# Example 1:
# 
# Input: s1 = "sea", s2 = "eat"
# Output: 231
# Explanation: Deleting "s" from "sea" adds the ASCII value of "s" (115) to the sum.
# Deleting "t" from "eat" adds 116 to the sum.
# At the end, both strings are equal, and 115 + 116 = 231 is the minimum sum possible to achieve this.
# 
# Example 2:
# 
# Input: s1 = "delete", s2 = "leet"
# Output: 403
# Explanation: Deleting "dee" from "delete" to turn the string into "let",
# adds 100[d] + 101[e] + 101[e] to the sum.
# Deleting "e" from "leet" adds 101[e] to the sum.
# At the end, both strings are equal to "let", and the answer is 100+101+101+101 = 403.
# If instead we turned both strings into "lee" or "eet", we would get answers of 433 or 417, which 
# are higher.
# 
# Constraints:
# 
# 	1 <= s1.length, s2.length <= 1000
# 	s1 and s2 consist of lowercase English letters.
#####################################################################################################


class Solution:
    def minimumDeleteSum(self, s1: str, s2: str) -> int:  # TLE: 63 / 93 test cases passed.
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int, cost: int) -> int:
            if i == len(s1) or j == len(s2):
                return cost + (sum(map(ord, s2[j:])) if i == len(s1) else sum(map(ord, s1[i:])))
            if s1[i] == s2[j]:
                return dfs(i + 1, j + 1, cost)
            return min(dfs(i + 1, j, cost + ord(s1[i])), dfs(i, j + 1, cost + ord(s2[j])))

        return dfs(0, 0, 0)

    def minimumDeleteSum(self, s1: str, s2: str) -> int:  # O(mn) time and O(mn) space
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> int:
            if i == len(s1) or j == len(s2):
                return sum(map(ord, s2[j:])) if i == len(s1) else sum(map(ord, s1[i:]))
            if s1[i] == s2[j]:
                return dfs(i + 1, j + 1)
            return min(dfs(i + 1, j) + ord(s1[i]), dfs(i, j + 1) + ord(s2[j]))

        return dfs(0, 0)

    def minimumDeleteSum(self, s1: str, s2: str) -> int:  # O(mn) time and O(mn) space
        dp = [[0] * (len(s2) + 1) for _ in range(len(s1) + 1)]
        for i, c in enumerate(s1, 1):
            dp[i][0] = dp[i - 1][0] + ord(c)
        for j, c in enumerate(s2, 1):
            dp[0][j] = dp[0][j - 1] + ord(c)

        for i in range(len(s1)):
            for j in range(len(s2)):
                if s1[i] == s2[j]:
                    dp[i + 1][j + 1] = dp[i][j]
                else:
                    dp[i + 1][j + 1] = min(dp[i][j + 1] + ord(s1[i]), dp[i + 1][j] + ord(s2[j]))

        return dp[-1][-1]

    def minimumDeleteSum(self, s1: str, s2: str) -> int:  # O(mn) time and O(mn) space
        dp = [[0] * (len(s2) + 1) for _ in range(2)]
        for i in range(len(s1)):
            for j in range(len(s2)):
                if s1[i] == s2[j]:
                    dp[i + 1][j + 1] = dp[i][j] + ord(s1[i])
                else:
                    dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j])
        return sum(map(ord, s1 + s2)) - dp[len(s1)][len(s2)] * 2

    def minimumDeleteSum(self, s1: str, s2: str) -> int:  # O(mn) time and O(n) space
        dp = [[0] * (len(s2) + 1) for _ in range(len(s1) + 1)]
        for i in range(len(s1)):
            for j in range(len(s2)):
                if s1[i] == s2[j]:
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j] + ord(s1[i])
                else:
                    dp[(i + 1) % 2][j + 1] = max(dp[i % 2][j + 1], dp[(i + 1) % 2][j])
        return sum(map(ord, s1 + s2)) - dp[(len(s1) % 2)][len(s2)] * 2


def test():
    arguments = [
        ("sea", "eat"),
        ("delete", "leet"),
    ]
    expectations = [231, 403]
    for (s1, s2), expected in zip(arguments, expectations):
        solution = Solution().minimumDeleteSum(s1, s2)
        assert solution == expected, (solution, expected)
test()