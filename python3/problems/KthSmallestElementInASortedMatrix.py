### Source : https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-03

##################################################################################################### 
#
# Given an n x n matrix where each of the rows and columns are sorted in ascending order, return the 
# kth smallest element in the matrix.
# 
# Note that it is the kth smallest element in the sorted order, not the kth distinct element.
# 
# Example 1:
# 
# Input: matrix = [[1,5,9],[10,11,13],[12,13,15]], k = 8
# Output: 13
# Explanation: The elements in the matrix are [1,5,9,10,11,12,13,13,15], and the 8th smallest number 
# is 13
# 
# Example 2:
# 
# Input: matrix = [[-5]], k = 1
# Output: -5
# 
# Constraints:
# 
# 	n == matrix.length
# 	n == matrix[i].length
# 	1 <= n <= 300
# 	-109 <= matrix[i][j] <= 109
# 	All the rows and columns of matrix are guaranteed to be sorted in non-decreasing order.
# 	1 <= k <= n2
#####################################################################################################

from typing import List


class Solution:
    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:  # (n^3) time and O(n) space
        return sorted(sum(matrix, []))[k - 1]  # list concatenation is slow

    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:  # (n^3) time and O(n) space
        return sorted(element for row in matrix for element in row)[k - 1]

    def kthSmallest(self, matrix, k):  # binary search: O(n log n log n) time and O(1) space
        import bisect
        left, right = matrix[0][0], matrix[-1][-1]
        while left < right:
            mid = left + (right - left) // 2
            if sum(bisect.bisect_right(row, mid) for row in matrix) < k:
                left = mid + 1
            else:
                right = mid
        return left

    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:  # O(k log k) time and O(1) space
        import heapq
        kth_smallest, heap = None, []
        heapq.heappush(heap, (matrix[0][0], 0, 0))
        while k:
            kth_smallest, i, j = heapq.heappop(heap)
            if i == 0 and j < len(matrix[0]) - 1:
                heapq.heappush(heap, (matrix[i][j + 1], i, j + 1))
            if i < len(matrix) - 1:
                heapq.heappush(heap, (matrix[i + 1][j], i + 1, j))
            k -= 1
        return kth_smallest


def test():
    arguments = [
        ([[1, 5, 9], [10, 11, 13], [12, 13, 15]], 8),
        ([[-5]], 1),
    ]
    expectations = [13, -5]
    for (matrix, k), expected in zip(arguments, expectations):
        solution = Solution().kthSmallest(matrix, k)
        assert solution == expected, k
