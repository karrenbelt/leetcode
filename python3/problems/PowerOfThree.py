### Source : https://leetcode.com/problems/power-of-three/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-27

##################################################################################################### 
#
# Given an integer n, return true if it is a power of three. Otherwise, return false.
# 
# An integer n is a power of three, if there exists an integer x such that n == 3x.
# 
# Example 1:
# Input: n = 27
# Output: true
# Example 2:
# Input: n = 0
# Output: false
# Example 3:
# Input: n = 9
# Output: true
# Example 4:
# Input: n = 45
# Output: false
# 
# Constraints:
# 
# 	-231 <= n <= 231 - 1
# 
# Follow up: Could you solve it without loops/recursion?
#####################################################################################################


class Solution:
    def isPowerOfThree(self, n: int) -> bool:
        import math  # negative n would yield a float, whereas n is given to be int. Also domain error in log of course
        return n >= 1 and n == 3**round(math.log(n, 3))

    def isPowerOfThree(self, n: int) -> bool:  # log3(n) time and O(1) space
        if n % 2 == 0 or (n > 1 and n % 3 != 0):
            return False
        k = 1
        while k < n:
            k *= 3
        return k == n

    def isPowerOfThree(self, n: int) -> bool:  # log3(n) time and O(1) space
        if n > 1:
            while n % 3 == 0:
                n /= 3
        return n == 1


def test():
    arguments = [27, 0, 9, 45, 1]
    expectations = [True, False, True, False, True]
    for n, expected in zip(arguments, expectations):
        solution = Solution().isPowerOfThree(n)
        assert solution == expected
