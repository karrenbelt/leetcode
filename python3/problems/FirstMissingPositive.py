### Source : https://leetcode.com/problems/first-missing-positive/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given an unsorted integer array nums, find the smallest missing positive integer.
# 
# Example 1:
# Input: nums = [1,2,0]
# Output: 3
# Example 2:
# Input: nums = [3,4,-1,1]
# Output: 2
# Example 3:
# Input: nums = [7,8,9,11,12]
# Output: 1
# 
# Constraints:
# 
# 	0 <= nums.length <= 300
# 	-231 <= nums[i] <= 231 - 1
# 
# Follow up: Could you implement an algorithm that runs in O(n) time and uses constant extra space?
#####################################################################################################

from typing import List


class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        nums = set(nums)
        for i in range(1, len(nums) + 2):
            if i not in nums:
                return i

    def firstMissingPositive(self, nums: List[int]) -> int:  # cyclic sort, O(n) time and O(1) space
        for i in range(len(nums)):
            while nums[i] - 1 in range(len(nums)) and nums[i] != nums[nums[i] - 1]:
                nums[nums[i] - 1], nums[i] = nums[i], nums[nums[i] - 1]
        return next((i + 1 for i, num in enumerate(nums) if num != i + 1), len(nums) + 1)


def test():
    arguments = [
        [1, 2, 0],
        [3, 4, -1, 1],
        [7, 8, 9, 11, 12],
        ]
    expectations = [3, 2, 1]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().firstMissingPositive(nums)
        assert solution == expected
