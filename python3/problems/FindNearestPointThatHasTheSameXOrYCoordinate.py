### Source : https://leetcode.com/problems/find-nearest-point-that-has-the-same-x-or-y-coordinate/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# You are given two integers, x and y, which represent your current location on a Cartesian grid: (x, 
# y). You are also given an array points where each points[i] = [ai, bi] represents that a point 
# exists at (ai, bi). A point is valid if it shares the same x-coordinate or the same y-coordinate as 
# your location.
# 
# Return the index (0-indexed) of the valid point with the smallest anhattan distance from your 
# current location. If there are multiple, return the valid point with the smallest index. If there 
# are no valid points, return -1.
# 
# The anhattan distance between two points (x1, y1) and (x2, y2) is abs(x1 - x2) + abs(y1 - y2).
# 
# Example 1:
# 
# Input: x = 3, y = 4, points = [[1,2],[3,1],[2,4],[2,3],[4,4]]
# Output: 2
# Explanation: Of all the points, only [3,1], [2,4] and [4,4] are valid. Of the valid points, [2,4] 
# and [4,4] have the smallest anhattan distance from your current location, with a distance of 1. 
# [2,4] has the smallest index, so return 2.
# 
# Example 2:
# 
# Input: x = 3, y = 4, points = [[3,4]]
# Output: 0
# Explanation: The answer is allowed to be on the same location as your current location.
# 
# Example 3:
# 
# Input: x = 3, y = 4, points = [[2,3]]
# Output: -1
# Explanation: There are no valid points.
# 
# Constraints:
# 
# 	1 <= points.length <= 104
# 	points[i].length == 2
# 	1 <= x, y, ai, bi <= 104
#####################################################################################################

from typing import List


class Solution:
    def nearestValidPoint(self, x: int, y: int, points: List[List[int]]) -> int:  # O(n) time O(n) space
        candidates = [point for point in points if point[0] == x or point[1] == y]
        if candidates:
            return points.index(min(candidates, key=lambda point: abs(x - point[0]) + abs(y - point[1])))
        return -1

    def nearestValidPoint(self, x: int, y: int, points: List[List[int]]) -> int:  # O(n) time O(1) space
        index = -1
        min_distance = 10 ** 5
        for i, point in enumerate(points):
            if point[0] == x or point[1] == y:
                distance = abs(x - point[0]) + abs(y - point[1])
                if distance < min_distance:
                    min_distance = distance
                    index = i
        return index


def test():
    arguments = [
        (3, 4, [[1, 2], [3, 1], [2, 4], [2, 3], [4, 4]]),
        (3, 4, [[3, 4]]),
        (3, 4, [[2, 3]]),
        ]
    expectations = [2, 0, -1]
    for (x, y, points), expected in zip(arguments, expectations):
        solution = Solution().nearestValidPoint(x, y, points)
        assert solution == expected
