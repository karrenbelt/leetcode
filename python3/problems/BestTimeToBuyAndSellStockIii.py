### Source : https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-16

##################################################################################################### 
#
# You are given an array prices where prices[i] is the price of a given stock on the ith day.
# 
# Find the maximum profit you can achieve. You may complete at most two transactions.
# 
# Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock 
# before you buy again).
# 
# Example 1:
# 
# Input: prices = [3,3,5,0,0,3,1,4]
# Output: 6
# Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
# Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.
# 
# Example 2:
# 
# Input: prices = [1,2,3,4,5]
# Output: 4
# Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
# Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are engaging multiple 
# transactions at the same time. You must sell before buying again.
# 
# Example 3:
# 
# Input: prices = [7,6,4,3,1]
# Output: 0
# Explanation: In this case, no transaction is done, i.e. max profit = 0.
# 
# Example 4:
# 
# Input: prices = [1]
# Output: 0
# 
# Constraints:
# 
# 	1 <= prices.length <= 105
# 	0 <= prices[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:  # O(kn^2) time and O(kn) space
        dp = [[0] * len(prices) for _ in range(3)]
        for k in range(1, 3):
            for i in range(1, len(prices)):
                m = prices[0]
                for j in range(1, i):
                    m = min(m, prices[j] - dp[k - 1][j - 1])
                dp[k][i] = max(dp[k][i - 1], prices[i] - m)
        return dp[2][len(prices) - 1]

    def maxProfit(self, prices: List[int]) -> int:  # O(kn) time and O(kn) space
        # remove duplicate calculation of m
        dp = [[0] * len(prices) for _ in range(3)]
        for k in range(1, 3):
            m = prices[0]
            for i in range(1, len(prices)):
                m = min(m, prices[i] - dp[k-1][i-1])
                dp[k][i] = max(dp[k][i - 1], prices[i] - m)
        return dp[2][len(prices) - 1]

    def maxProfit(self, prices: List[int]) -> int:  # O(kn) time and O(kn) space
        # swap for loops, factor out m
        dp = [[0] * len(prices) for _ in range(3)]
        m = [prices[0]] * 3  # save for each transaction
        for i in range(1, len(prices)):
            for k in range(1, 3):
                m[k] = min(m[k], prices[i] - dp[k-1][i-1])
                dp[k][i] = max(dp[k][i - 1], prices[i] - m[k])
        return dp[2][len(prices) - 1]

    def maxProfit(self, prices: List[int]) -> int:  # O(kn) time and O(k) space
        # reduce the dimension of the dp array to reduce space complexity
        dp = [0] * 3
        m = [prices[0]] * 3
        for i in range(1, len(prices)):
            for k in range(1, 3):
                m[k] = min(m[k], prices[i] - dp[k-1])
                dp[k] = max(dp[k], prices[i] - m[k])
        return dp[2]

    def maxProfit(self, prices: List[int]) -> int:  # O(n) time and O(1) space
        # since k == 2, we can expand as follows
        buy1 = buy2 = float('inf')
        sell1 = sell2 = 0
        for i in range(len(prices)):
            buy1 = min(buy1, prices[i])
            sell1 = max(sell1, prices[i] - buy1)
            buy2 = min(buy2, prices[i] - sell1)
            sell2 = max(sell2, prices[i] - buy2)
        return sell2


def test():
    arguments = [
        [3, 3, 5, 0, 0, 3, 1, 4],
        [1, 2, 3, 4, 5],
        [7, 6, 4, 3, 1],
        [1],
        ]
    expectations = [6, 4, 0, 0]
    for prices, expected in zip(arguments, expectations):
        solution = Solution().maxProfit(prices)
        assert solution == expected

test()