### Source : https://leetcode.com/problems/reverse-vowels-of-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-27

##################################################################################################### 
#
# Write a function that takes a string as input and reverse only the vowels of a string.
# 
# Example 1:
# 
# Input: "hello"
# Output: "holle"
# 
# Example 2:
# 
# Input: "leetcode"
# Output: "leotcede"
# 
# Note:
# The vowels does not include the letter "y".
# 
#####################################################################################################

class Solution:
    def reverseVowels(self, s: str) -> str:
        vowels = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'}
        found = []
        s = list(s)
        for i in range(len(s)):
            if s[i] in vowels:
                found.append(i)
        while len(found) > 1:
            li = found.pop(0)
            ri = found.pop()
            s[li], s[ri] = s[ri], s[li]
        return ''.join(s)

    def reverseVowels(self, s: str) -> str:
        vowels = set('aeiou')
        found = []
        for i in range(len(s)):
            if s[i] in vowels:
                found.append(i)
        s = bytearray(s, encoding='ascii')
        while len(found) > 1:
            li = found.pop(0)
            ri = found.pop()
            s[li], s[ri] = s[ri], s[li]
        return s.decode('ascii')

    def reverseVowels(self, s: str) -> str:
        import re
        vowels = re.findall('(?i)[aeiou]', s)
        return re.sub('(?i)[aeiou]', lambda m: vowels.pop(), s)


def test():
    strings = [
        "hello",
        "leetcode",
        ]
    expectations = [
        "holle",
        "leotcede",
        ]
    for s, expected in zip(strings, expectations):
        solution = Solution().reverseVowels(s)
        assert solution == expected
