### Source : https://leetcode.com/problems/4sum/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-09

##################################################################################################### 
#
# Given an array nums of n integers and an integer target, are there elements a, b, c, and d in nums 
# such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of 
# target.
# 
# Notice that the solution set must not contain duplicate quadruplets.
# 
# Example 1:
# Input: nums = [1,0,-1,0,-2,2], target = 0
# Output: [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
# Example 2:
# Input: nums = [], target = 0
# Output: []
# 
# Constraints:
# 
# 	0 <= nums.length <= 200
# 	-109 <= nums[i] <= 109
# 	-109 <= target <= 109
#####################################################################################################

from typing import List


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:  # linear: O(n^4) -> TLE
        ans, seen = set(), set()
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                for k in range(j + 1, len(nums)):
                    for l in range(k + 1, len(nums)):
                        s = nums[i], nums[j], nums[k], nums[l]
                        if frozenset(s) not in seen and sum(s) == target:
                            seen.add(frozenset(s))
                            ans.add(s)
        return list(map(list, ans))

    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:  # linear: O(n^4) -> TLE
        import itertools
        ans, seen = set(), set()
        for combination in set(itertools.combinations(nums, 4)):
            if frozenset(combination) not in seen and sum(combination) == target:
                seen.add(frozenset(combination))
                ans.add(combination)
        return list(map(list, ans))

    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:  # double two-sum: O(n^2)
        d = dict()
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                d.setdefault(nums[i] + nums[j], []).append((i, j))

        result, seen = [], set()
        for two_sum, sum_indices in d.items():
            for k, l in d.get(target - two_sum, []):
                for i, j in sum_indices:
                    if len({i, j, k, l}) == 4:
                        four_sum = tuple(sorted([nums[i], nums[j], nums[k], nums[l]]))
                        if four_sum not in seen:
                            seen.add(four_sum)
                            result.append(list(four_sum))
        return result

    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:

        def find_k_sum(nums: List[int], target: int, k: int) -> List[List[int]]:
            k_sum = []
            if len(nums) == 0 or nums[0] * k > target or target > nums[-1] * k:  # speed up
                return k_sum
            if k == 2:
                return find_two_sum(nums, target)
            for i in range(len(nums)):
                if i == 0 or nums[i - 1] != nums[i]:
                    for array in (find_k_sum(nums[i + 1:], target - nums[i], k - 1)):
                        k_sum.append([nums[i]] + array)
            return k_sum

        def find_two_sum(nums: List[int], target: int) -> List[List[int]]:
            two_sum = []
            left, right = 0, len(nums) - 1
            while left < right:
                value = nums[left] + nums[right]  # second condition in if avoids duplication if sums equals target
                if value < target or (left > 0 and nums[left] == nums[left - 1]):
                    left += 1
                elif value > target or (right < len(nums) - 1 and nums[right] == nums[right + 1]):
                    right -= 1
                else:
                    two_sum.append([nums[left], nums[right]])
                    left += 1
                    right -= 1
            return two_sum

        nums.sort()
        return find_k_sum(nums, target, 4)

    # def fourSum(self, n: List[int], t: int) -> List[List[int]]:
    #     if not n:
    #         return []
    #     n.sort()
    #     L, N, S, M = len(n), {j: i for i, j in enumerate(n)}, set(), n[-1]
    #     for i in range(L - 3):
    #         a = n[i]
    #         if a + 3 * M < t:
    #             continue
    #         if 4 * a > t:
    #             break
    #         for j in range(i + 1, L - 2):
    #             b = n[j]
    #             if a + b + 2 * M < t:
    #                 continue
    #             if a + 3 * b > t:
    #                 break
    #             for k in range(j + 1, L - 1):
    #                 c = n[k]
    #                 d = t - (a + b + c)
    #                 if d > M:
    #                     continue
    #                 if d < c:
    #                     break
    #                 if d in N and N[d] > k:
    #                     S.add((a, b, c, d))
    #     return [list(s) for s in S]


def test():
    from collections import Counter
    arrays_of_numbers = [
        [1, 0, -1, 0, -2, 2],
        [],
    ]
    targets = [0, 0]
    expectations = [
        [[-2, -1, 1, 2], [-2, 0, 0, 2], [-1, 0, 0, 1]],
        [],
    ]
    for nums, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().fourSum(nums, target)
        assert Counter(map(lambda x: tuple(sorted(x)), solution)) == Counter(map(lambda x: tuple(sorted(x)), expected))
