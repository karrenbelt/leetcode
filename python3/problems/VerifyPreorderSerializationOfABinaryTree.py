### Source : https://leetcode.com/problems/verify-preorder-serialization-of-a-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-01

##################################################################################################### 
#
# One way to serialize a binary tree is to use preorder traversal. When we encounter a non-null node, 
# we record the node's value. If it is a null node, we record using a sentinel value such as '#'.
# 
# For example, the above binary tree can be serialized to the string "9,3,4,#,#,1,#,#,2,#,6,#,#", 
# where '#' represents a null node.
# 
# Given a string of comma-separated values preorder, return true if it is a correct preorder 
# traversal serialization of a binary tree.
# 
# It is guaranteed that each comma-separated value in the string must be either an integer or a 
# character '#' representing null pointer.
# 
# You may assume that the input format is always valid.
# 
# 	For example, it could never contain two consecutive commas, such as "1,,3".
# 
# Example 1:
# Input: preorder = "9,3,4,#,#,1,#,#,2,#,6,#,#"
# Output: true
# Example 2:
# Input: preorder = "1,#"
# Output: false
# Example 3:
# Input: preorder = "9,#,#,1"
# Output: false
# 
# Constraints:
# 
# 	1 <= preorder.length <= 104
# 	preoder consist of integers in the range [0, 100] and '#' separated by commas ','.
# 
# Follow up: Find an algorithm without reconstructing the tree.
#####################################################################################################


class Solution:
    def isValidSerialization(self, preorder: str) -> bool:  # O(n) time and O(n) space
        stack = []
        for value in preorder.split(','):
            if value == '#':
                while len(stack) >= 2 and stack[-1] == '#' and stack[-2].isdigit():
                    stack.pop() and stack.pop()
            stack.append(value)
        return stack == ['#']

    def isValidSerialization(self, preorder: str) -> bool:  # O(n) time and O(1) space
        count = 0
        nodes = preorder.split(',')
        for i, item in enumerate(nodes):
            count += 1 if item != "#" else -1
            if count == -1 and i < len(nodes) - 1:
                return False
        return count == -1

    def isValidSerialization(self, preorder: str) -> bool:  # O(n) time and O(1) space
        slots, prev = 1, ','
        for c in preorder:
            if c != ',' and prev == ',':
                slots -= 1
                if slots < 0:
                    return False
                slots += 2 if c.isdigit() else 0
            prev = c
        return not slots


def test():
    arguments = [
        "9,3,4,#,#,1,#,#,2,#,6,#,#",
        "1,#",
        "9,#,#,1",
        "9,#,92,#,#",
    ]
    expectations = [True, False, False, True]
    for preorder, expected in zip(arguments, expectations):
        solution = Solution().isValidSerialization(preorder)
        assert solution == expected


test()
