### Source : https://leetcode.com/problems/flip-string-to-monotone-increasing/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-10

##################################################################################################### 
#
# A binary string is monotone increasing if it consists of some number of 0's (possibly none), 
# followed by some number of 1's (also possibly none).
# 
# You are given a binary string s. You can flip s[i] changing it from 0 to 1 or from 1 to 0.
# 
# Return the minimum number of flips to make s monotone increasing.
# 
# Example 1:
# 
# Input: s = "00110"
# Output: 1
# Explanation: We flip the last digit to get 00111.
# 
# Example 2:
# 
# Input: s = "010110"
# Output: 2
# Explanation: We flip to get 011111, or alternatively 000111.
# 
# Example 3:
# 
# Input: s = "00011000"
# Output: 2
# Explanation: We flip to get 00000000.
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s[i] is either '0' or '1'.
#####################################################################################################


class Solution:
    def minFlipsMonoIncr(self, s: str) -> int:  # O(n) time and O(1) space
        # we start with the base case, a single element, which is always monotonic
        # then as we imagine appending values, we need to either flip the newly appended zeroes, or the preceding ones
        # we keep track of the minimum of the two: when the flip count exceeds the number of ones it's better to switch
        one = flip = 0
        for c in s:
            if c == '1':
                one += 1
            else:
                flip += 1
            flip = min(flip, one)
        return flip


def test():
    arguments = [
        "00110",
        "010110",
        "00011000",
    ]
    expectations = [1, 2, 2]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minFlipsMonoIncr(s)
        assert solution == expected
