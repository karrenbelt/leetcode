### Source : https://leetcode.com/problems/number-of-subarrays-with-bounded-maximum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-26

##################################################################################################### 
#
# We are given an array A of positive integers, and two positive integers L and R (L <= R).
# 
# Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array 
# element in that subarray is at least L and at most R.
# 
# Example :
# Input: 
# A = [2, 1, 4, 3]
# L = 2
# R = 3
# Output: 3
# Explanation: There are three subarrays that meet the requirements: [2], [2, 1], [3].
# 
# Note:
# 
# 	L, R  and A[i] will be an integer in the range [0, 109].
# 	The length of A will be in the range of [1, 50000].
# 
#####################################################################################################

from typing import List


class Solution:  # at least one number >= L, no number > R
    def numSubarrayBoundedMax(self, A: List[int], L: int, R: int) -> int:   # O(n) time and O(1) space

        def subarrays_lt(nums: List[int], maximum: int):
            current = total = 0
            for n in nums:
                if n <= maximum:
                    current += 1
                    total += current
                else:
                    current = 0
            return total

        return subarrays_lt(A, R) - subarrays_lt(A, L - 1)

    def numSubarrayBoundedMax(self, A: List[int], L: int, R: int) -> int:  # O(n) time and O(1) space
        ctr = last_in = last_out = 0
        for i, n in enumerate(A, 1):
            if n >= L:
                last_in = i
            if n > R:
                last_out = i
            ctr += last_in - last_out
        return ctr

    def numSubarrayBoundedMax(self, A: List[int], L: int, R: int) -> int:  # O(n) time and O(1) space
        res, low, high = 0, -1, -1
        for i, n in enumerate(A):
            if n > R:
                low = high = i
            elif n >= L:
                high = i
            res += high - low
        return res


def test():
    arguments = [
        ([2, 1, 4, 3], 2, 3),
    ]
    expectations = [3]
    for (A, L, R), expected in zip(arguments, expectations):
        solution = Solution().numSubarrayBoundedMax(A, L, R)
        assert solution == expected
