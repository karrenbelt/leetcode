### Source : https://leetcode.com/problems/sort-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-04

##################################################################################################### 
#
# Given an array of integers nums, sort the array in ascending order.
# 
# Example 1:
# Input: nums = [5,2,3,1]
# Output: [1,2,3,5]
# Example 2:
# Input: nums = [5,1,1,2,0,0]
# Output: [0,0,1,1,2,5]
# 
# Constraints:
# 
# 	1 <= nums.length <= 5 * 104
# 	-5 * 104 <= nums[i] <= 5 * 104
#####################################################################################################

from typing import List


class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:  # this is not the exercise, obviously
        return nums.sort() or nums

    def sortArray(self, nums: List[int]) -> List[int]:  # top-down

        def merge_sort(array):
            if len(array) <= 1:  # forgot this one first time
                return array
            pivot = len(array) // 2
            left_list = merge_sort(array[:pivot])
            right_list = merge_sort(array[pivot:])
            return merge(left_list, right_list)

        def merge(left_list, right_list):
            left = right = 0
            ans = []
            while left < len(left_list) and right < len(right_list):
                if left_list[left] < right_list[right]:
                    ans.append(left_list[left])
                    left += 1
                else:
                    ans.append(right_list[right])
                    right += 1
            ans.extend(left_list[left:])
            ans.extend(right_list[right:])
            return ans

        return merge_sort(nums)


def test():
    arguments = [
        [5, 2, 3, 1],
        [5, 1, 1, 2, 0, 0],
    ]
    expectations = [
        [1, 2, 3, 5],
        [0, 0, 1, 1, 2, 5],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().sortArray(nums)
        assert solution == expected
