### Source : https://leetcode.com/problems/minimum-distance-between-bst-nodes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Given the root of a Binary Search Tree (BST), return the minimum difference between the values of 
# any two different nodes in the tree.
# 
# Note: This question is the same as 530: 
# https://leetcode.com/problems/minimum-absolute-difference-in-bst/
# 
# Example 1:
# 
# Input: root = [4,2,6,1,3]
# Output: 1
# 
# Example 2:
# 
# Input: root = [1,0,48,null,null,12,49]
# Output: 1
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 100].
# 	0 <= Node.val <= 105
#####################################################################################################

# same question:
# 530: https://leetcode.com/problems/minimum-absolute-difference-in-bst/
# 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/

from typing import Generator, Iterator
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def minDiffInBST(self, root: TreeNode) -> int:  # O(n) time & O(h) space

        def traverse(node: TreeNode, low: int, high: int) -> int:
            if not node:
                return high - low
            left = traverse(node.left, low, node.val)
            right = traverse(node.right, node.val, high)
            return min(left, right)

        return traverse(root, -1 << 31, (1 << 31) - 1)

    def minDiffInBST(self, root: TreeNode) -> int:  # O(n) time & O(h) space
        minimum, prev = (1 << 31) - 1, -1 << 31
        stack, node = [], root
        while stack or node:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            minimum = min(minimum, node.val - prev)
            prev, node = node.val, node.right
        return minimum

    def minDiffInBST(self, root: TreeNode) -> int:  # O(n) time and O(1) space

        def pairwise(iterable: Iterator) -> Generator:
            last = next(iterable, None)
            for item in iterable:
                yield last, item
                last = item

        def traverse(node: TreeNode) -> Generator:
            if node:
                yield from traverse(node.left)
                yield node.val
                yield from traverse(node.right)

        return min(b - a for a, b in pairwise(traverse(root)))


def test():
    from python3 import Codec
    arguments = [
        "[4,2,6,1,3]",
        "[1,0,48,null,null,12,49]",
        ]
    expectations = [1, 1]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().minDiffInBST(root)
        assert solution == expected
