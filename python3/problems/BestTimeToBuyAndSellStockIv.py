### Source : https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-16

##################################################################################################### 
#
# You are given an integer array prices where prices[i] is the price of a given stock on the ith day, 
# and an integer k.
# 
# Find the maximum profit you can achieve. You may complete at most k transactions.
# 
# Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock 
# before you buy again).
# 
# Example 1:
# 
# Input: k = 2, prices = [2,4,1]
# Output: 2
# Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.
# 
# Example 2:
# 
# Input: k = 2, prices = [3,2,6,5,0,3]
# Output: 7
# Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4. Then buy on 
# day 5 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
# 
# Constraints:
# 
# 	0 <= k <= 100
# 	0 <= prices.length <= 1000
# 	0 <= prices[i] <= 1000
#####################################################################################################

from typing import List


class Solution:

    def maxProfit(self, k: int, prices: List[int]) -> int:  # O(kn) time and O(k) space
        buy = [float('inf')] * (k + 1)
        sell = [0] * (k + 1)
        for price in prices:
            for k in range(1, k+1):
                buy[k] = min(buy[k], price - sell[k-1])
                sell[k] = max(sell[k], price - buy[k])
        return sell[-1]

    def maxProfit(self, k, prices):  # bottom up dp: O(kn^2) time and O(kn) space
        if len(prices) < 2:
            return 0
        dp = [[0 for _ in range(k+1)] for _ in range(len(prices))]
        for k1 in range(1, k + 1):
            for i in range(1, len(prices)):
                dp[i][k1] = dp[i - 1][k1]
                for j in range(i):
                    tmp = prices[i] - prices[j]
                    tmp += dp[j][k1 - 1] if j > 0 else 0
                    dp[i][k1] = max(dp[i][k1], tmp)
        return dp[len(prices) - 1][k]

    def maxProfit(self, k: int, prices: List[int]) -> int:  # top-down dp with memoization: O(kn^2) time and O(kn) space

        def recursive(i, k):
            if i <= 0 or k <= 0:
                return 0
            if (i, k) in memo:
                return memo[(i, k)]
            max_profit = recursive(i - 1, k)
            for j in range(i):
                tmp = prices[i] - prices[j] + recursive(j - 1, k - 1)
                max_profit = max(max_profit, tmp)
            memo[(i, k)] = max_profit
            return memo[(i, k)]

        memo = {}
        return recursive(len(prices) - 1, k)

    def maxProfit(self, k: int, prices: List[int]) -> int:  # O(kn) time and O(kn) space
        if len(prices) < 2:
            return 0
        # if we cannot perform k transactions, this reduces to problem II
        # this is only here because of some exceptionally large test case
        if k >= len(prices) // 2:
            profit = 0
            for j in range(1, len(prices)):
                profit += max(prices[j] - prices[j - 1], 0)
            return profit
        # two dp arrays
        local_max = [[0] * len(prices) for _ in range(k + 1)]
        global_max = [[0] * len(prices) for _ in range(k + 1)]
        # global_max = local_max.copy()  # don't do this, nested list will be aliased
        for i in range(1, k + 1):
            for j in range(1, len(prices)):
                margin = prices[j] - prices[j - 1]
                local_max[i][j] = max(global_max[i - 1][j - 1] + max(margin, 0), local_max[i][j - 1] + margin)
                global_max[i][j] = max(global_max[i][j - 1], local_max[i][j])
        return global_max[k][-1]

    def maxProfit(self, k: int, prices: List[int]) -> int:  # O(kn) time and O(kn) space
        if len(prices) < 2:
            return 0
        # if we cannot perform k transactions, this reduces to problem II
        # this is only here because of some exceptionally large test case
        if k >= len(prices) // 2:
            profit = 0
            for i in range(1, len(prices)):
                profit += max(prices[i] - prices[i - 1], 0)
            return profit

        dp = [[0] * (k + 1) for _ in range(len(prices))]
        for j in range(1, k + 1):
            local_max = -prices[0]
            for i in range(1, len(prices)):
                dp[i][j] = max(dp[i - 1][j], prices[i] + local_max)
                local_max = max(local_max, dp[i - 1][j - 1] - prices[i])
        return dp[len(prices) - 1][k]


def test():
    arguments = [
        (2, [2, 4, 1]),
        (2, [3, 2, 6, 5, 0, 3]),
        (1, [1]),
        (5, [1, 2]),
        (2, [3, 3, 5, 0, 0, 3, 1, 4]),
        ]
    expectations = [2, 7, 0, 1, 6]
    for (k, prices), expected in zip(arguments, expectations):
        solution = Solution().maxProfit(k, prices)
        assert solution == expected

test()