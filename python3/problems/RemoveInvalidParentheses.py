### Source : https://leetcode.com/problems/remove-invalid-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-14

##################################################################################################### 
#
# Given a string s that contains parentheses and letters, remove the minimum number of invalid 
# parentheses to make the input string valid.
# 
# Return all the possible results. You may return the answer in any order.
# 
# Example 1:
# 
# Input: s = "()())()"
# Output: ["(())()","()()()"]
# 
# Example 2:
# 
# Input: s = "(a)())()"
# Output: ["(a())()","(a)()()"]
# 
# Example 3:
# 
# Input: s = ")("
# Output: [""]
# 
# Constraints:
# 
# 	1 <= s.length <= 25
# 	s consists of lowercase English letters and parentheses '(' and ')'.
# 	There will be at most 20 parentheses in s.
#####################################################################################################

from typing import List


class Solution:
    def removeInvalidParentheses(self, s: str) -> List[str]:  # O(2^n) time and O(2^n) space

        def count_violations(string: str) -> int:  # O(n) time and O(n) space
            stack, ctr = [], 0
            for c in filter(lambda x: not str.isalpha(x), string):
                if c == '(':
                    stack.append(c)
                elif stack:
                    stack.pop()
                else:
                    ctr += 1
            return len(stack) + ctr

        def backtracking(path: str, n: int) -> None:  # O(n^2) time and O(n^2) space
            if n == 0 and not count_violations(path):
                paths.append(path)
            else:
                for j in range(len(path)):
                    next_path = path[:j] + path[j + 1:]
                    if next_path not in seen and not path[j].isalpha():
                        seen.add(next_path)
                        backtracking(next_path, n - 1)

        s, paths, seen = s.lstrip(')').rstrip('('), [], set()
        return backtracking(s, count_violations(s)) or paths

    def removeInvalidParentheses(self, s: str) -> List[str]:  # O(n^2) time and O(n) space

        def is_valid(string: str) -> bool:  # O(n) time and O(1) space
            ctr = 0
            for c in filter(lambda x: not str.isalpha(x), string):
                ctr += 1 if c == '(' else -1
                if ctr < 0:
                    return False
            return ctr == 0

        level = {s}  # bfs: O(n^2) time and O(n) space
        while True:
            valid = list(filter(is_valid, level))
            if valid:
                return valid
            level = {path[:i] + path[i + 1:] for path in level for i in range(len(path))}


def test():
    from collections import Counter
    arguments = [
        "()())()",
        "(a)())()",
        ")(",
        "()))))))))))))))))))",
        "())b)(()((e)o())q()))(w))",  # approx. worst-case complexity
        "x(",
    ]
    expectations = [
        ["(())()", "()()()"],
        ["(a())()", "(a)()()"],
        [""],
        ["()"],
        ['(b((((e)o())q()))(w))', '(b(()((eo())q()))(w))', '(b(()((e)o()q()))(w))', '(b(()((e)o())q())(w))',
         '(b(()((e)o())q()))(w)', '(b)((((eo())q()))(w))', '(b)((((e)o()q()))(w))', '(b)((((e)o())q())(w))',
         '(b)((((e)o())q()))(w)', '(b)(()((eo()q()))(w))', '(b)(()((eo())q())(w))', '(b)(()((eo())q()))(w)',
         '(b)(()((e)o(q()))(w))', '(b)(()((e)o()q())(w))', '(b)(()((e)o()q()))(w)', '(b)(()((e)o())q()(w))',
         '(b)(()((e)o())q())(w)', '()b((((eo())q()))(w))', '()b((((e)o()q()))(w))', '()b((((e)o())q())(w))',
         '()b((((e)o())q()))(w)', '()b(()((eo()q()))(w))', '()b(()((eo())q())(w))', '()b(()((eo())q()))(w)',
         '()b(()((e)o(q()))(w))', '()b(()((e)o()q())(w))', '()b(()((e)o()q()))(w)', '()b(()((e)o())q()(w))',
         '()b(()((e)o())q())(w)'],
        ["x"],
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().removeInvalidParentheses(s)
        assert Counter(solution) == Counter(expected)
