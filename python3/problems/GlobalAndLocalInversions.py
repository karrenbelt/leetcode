### Source : https://leetcode.com/problems/global-and-local-inversions/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

#####################################################################################################
#
# We have some permutation A of [0, 1, ..., N - 1], where N is the length of A.
#
# The number of (global) inversions is the number of i < j with 0 <= i < j < N and A[i] > A[j].
#
# The number of local inversions is the number of i with 0 <= i < N and A[i] > A[i+1].
#
# Return true if and only if the number of global inversions is equal to the number of local
# inversions.
#
# Example 1:
#
# Input: A = [1,0,2]
# Output: true
# Explanation: There is 1 global inversion, and 1 local inversion.
#
# Example 2:
#
# Input: A = [1,2,0]
# Output: false
# Explanation: There are 2 global inversions, and 1 local inversion.
#
# Note:
#
# 	A will be a permutation of [0, 1, ..., A.length - 1].
# 	A will have length in range [1, 5000].
# 	The time limit for this problem has been reduced.
#
#####################################################################################################

from typing import List


class Solution:
    def isIdealPermutation(self, A: List[int]) -> bool:  # O(n) time and O(1) space
        # all local inversions are global inversion. If we find one non-local inversion we're done
        current_maximum = 0
        for i in range(len(A) - 2):
            current_maximum = max(current_maximum, A[i])
            if current_maximum > A[i + 2]:
                return False
        return True

    def isIdealPermutation(self, A: List[int]) -> bool:  # O(n) time and O(1) space
        # check whether compared to the enumeration: -1 <= difference <= 1
        return all(-1 <= (i - v) <= 1 for i, v in enumerate(A))

    def isIdealPermutation(self, A: List[int]) -> bool:  # O(n) time and O(1) space
        return not any(abs(i - v) >= 2 for i, v in enumerate(A))

    def isIdealPermutation(self, A: List[int]) -> bool:  # O(n) time and O(1) space
        # generalized, two pass
        i = 0
        while i < len(A) - 1:
            if A[i] > A[i + 1]:
                A[i], A[i + 1] = A[i + 1], A[i]
                i += 1
            i += 1
        return all(i == v for i, v in enumerate(A))


def test():
    arguments = [
        [1, 0, 2],  # 1 global, 1 local
        [1, 2, 0],  # 2 global, 1 local
        [0, 1, 2],  # 0 global, 0 local
        [3, 2, 1],  # 3 global, 2 local
        [2, 0, 1],  # 2 global, 1 local
        ]
    expectations = [True, False, True, False, False]
    for A, expected in zip(arguments, expectations):
        solution = Solution().isIdealPermutation(A)
        assert solution == expected
