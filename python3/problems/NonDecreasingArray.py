### Source : https://leetcode.com/problems/non-decreasing-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-21

##################################################################################################### 
#
# Given an array nums with n integers, your task is to check if it could become non-decreasing by 
# modifying at most one element.
# 
# We define an array is non-decreasing if nums[i] <= nums[i + 1] holds for every i (0-based) such 
# that (0 <= i <= n - 2).
# 
# Example 1:
# 
# Input: nums = [4,2,3]
# Output: true
# Explanation: You could modify the first 4 to 1 to get a non-decreasing array.
# 
# Example 2:
# 
# Input: nums = [4,2,1]
# Output: false
# Explanation: You can't get a non-decreasing array by modify at most one element.
# 
# Constraints:
# 
# 	n == nums.length
# 	1 <= n <= 104
# 	-105 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def checkPossibility(self, nums: List[int]) -> bool:  # O(n)
        changed = 0
        for i in range(len(nums) - 1):
            if nums[i] > nums[i + 1]:
                if changed > 0:
                    return False
                if i + 1 < len(nums) - 1 and i > 0:
                    if nums[i] > nums[i + 2] and nums[i - 1] > nums[i + 1]:
                        return False
                changed = 1
        return True

    def checkPossibility(self, nums: List[int]) -> bool:  # O(n)
        # set i-2 to i-1 to fix a violation, if not possible change i-1 to i
        modified = False
        for i in range(1, len(nums)):
            if nums[i] < nums[i-1]:  # if decreasing
                if modified:
                    return False
                elif i == 1:  # first iteration
                    nums[i-1] = nums[i]
                elif i > 1 and nums[i-2] <= nums[i]:  # if prev <= next
                    nums[i-1] = nums[i-2]  # then curr = prev
                else:
                    nums[i] = nums[i-1]  # next = curr
                modified = True
        return True


def test():
    arguments = [
        [4, 2, 3],
        [4, 2, 1],
        [5, 7, 1, 8],
        ]
    expectations = [True, False, True]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().checkPossibility(nums)
        assert solution == expected

