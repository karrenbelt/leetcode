### Source : https://leetcode.com/problems/max-value-of-equation/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-16

##################################################################################################### 
#
# You are given an array points containing the coordinates of points on a 2D plane, sorted by the 
# x-values, where points[i] = [xi, yi] such that xi < xj for all 1 <= i < j <= points.length. You are 
# also given an integer k.
# 
# Return the maximum value of the equation yi + yj + |xi - xj| where |xi - xj| <= k and 1 <= i < j <= 
# points.length.
# 
# It is guaranteed that there exists at least one pair of points that satisfy the constraint |xi - 
# xj| <= k.
# 
# Example 1:
# 
# Input: points = [[1,3],[2,0],[5,10],[6,-10]], k = 1
# Output: 4
# Explanation: The first two points satisfy the condition |xi - xj| <= 1 and if we calculate the 
# equation we get 3 + 0 + |1 - 2| = 4. Third and fourth points also satisfy the condition and give a 
# value of 10 + -10 + |5 - 6| = 1.
# No other pairs satisfy the condition, so we return the max of 4 and 1.
# 
# Example 2:
# 
# Input: points = [[0,0],[3,0],[9,2]], k = 3
# Output: 3
# Explanation: Only the first two points have an absolute difference of 3 or less in the x-values, 
# and give the value of 0 + 0 + |0 - 3| = 3.
# 
# Constraints:
# 
# 	2 <= points.length <= 105
# 	points[i].length == 2
# 	-108 <= xi, yi <= 108
# 	0 <= k <= 2 * 108
# 	xi < xj for all 1 <= i < j <= points.length
# 	xi form a strictly increasing sequence.
#####################################################################################################

from typing import List


class Solution:
    # since xi < xj -> yi + yj + |xi - xj| = (yi - xi) + (yj + xj)
    def findMaxValueOfEquation(self, points: List[List[int]], k: int) -> int:
        import heapq

        queue, maximum = [], -1 << 31
        for x, y in points:
            while queue and queue[0][1] < x - k:
                heapq.heappop(queue)
            if queue:
                maximum = max(maximum, -queue[0][0] + y + x)
            heapq.heappush(queue, (x - y, x))
        return maximum

    def findMaxValueOfEquation(self, points: List[List[int]], k: int) -> int:
        from collections import deque

        queue = deque()
        maximum = -1 << 31
        for x, y in points:
            while queue and queue[0][1] < x - k:
                queue.popleft()
            if queue:
                maximum = max(maximum, queue[0][0] + y + x)
            while queue and queue[-1][0] <= y - x:
                queue.pop()
            queue.append([y - x, x])
        return maximum


def test():
    strings = [
        ([[1, 3], [2, 0], [5, 10], [6, -10]], 1),
        ([[0, 0], [3, 0], [9, 2]], 3),
        ([[-17, 5], [-10, -8], [-5, -13], [-2, 7], [8, -14]], 4),
    ]
    expectations = [4, 3, -3]
    for (points, k), expected in zip(strings, expectations):
        solution = Solution().findMaxValueOfEquation(points, k)
        assert solution == expected
