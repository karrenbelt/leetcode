### Source : https://leetcode.com/problems/single-number-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an integer array nums, in which exactly two elements appear only once and all the other 
# elements appear exactly twice. Find the two elements that appear only once. You can return the 
# answer in any order.
# 
# Follow up: Your algorithm should run in linear runtime complexity. Could you implement it using 
# only constant space complexity?
# 
# Example 1:
# 
# Input: nums = [1,2,1,3,2,5]
# Output: [3,5]
# Explanation:  [5, 3] is also a valid answer.
# 
# Example 2:
# 
# Input: nums = [-1,0]
# Output: [-1,0]
# 
# Example 3:
# 
# Input: nums = [0,1]
# Output: [1,0]
# 
# Constraints:
# 
# 	2 <= nums.length <= 3 * 104
# 	-231 <= nums[i] <= 231 - 1
# 	Each integer in nums will appear twice, only two integers will appear once.
#####################################################################################################

from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        counts = {}
        for n in nums:
            counts[n] = counts.get(n, 0) + 1
        return [k for k, v in counts.items() if v == 1]

    def singleNumber(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        seen = set()
        for n in nums:
            if n in seen:
                seen.remove(n)
            else:
                seen.add(n)
        return list(seen)

    def singleNumber(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        a = b = xor_of_nums = 0
        for n in nums:
            xor_of_nums ^= n
        mask = 1
        while xor_of_nums & mask == 0:
            mask = mask << 1
        for n in nums:
            if n & mask:
                a ^= n
            else:
                b ^= n
        return [a, b]

    def singleNumber(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        from operator import xor
        from functools import reduce
        xor_of_nums = reduce(xor, nums)  # last non-zero bit = xor_of_nums & -xor_of_nums
        num1 = reduce(xor, filter(lambda n: n & xor_of_nums & -xor_of_nums, nums))
        return [num1, xor_of_nums ^ num1]


def test():
    arguments = [
        [1, 2, 1, 3, 2, 5],
        [-1, 0],
        [0, 1],
        ]
    expectations = [
        [5, 3],
        [-1, 0],
        [1, 0],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().singleNumber(nums)
        assert sorted(solution) == sorted(expected)
