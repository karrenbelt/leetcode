### Source : https://leetcode.com/problems/invert-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-01

##################################################################################################### 
#
# Invert a binary tree.
# 
# Example:
# 
# Input:
# 
#      4
#    /   \
#   2     7
#  / \   / \
# 1   3 6   9
# 
# Output:
# 
#      4
#    /   \
#   7     2
#  / \   / \
# 9   6 3   1
# 
# Trivia:
# This problem was inspired by this original tweet by ax Howell:
# 
# Google: 90% of our engineers use the software you wrote (Homebrew), but you can&rsquo;t invert a 
# binary tree on a whiteboard so f*** off.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:

        def invert(node):
            if not node:
                return
            node.left, node.right = node.right, node.left
            invert(node.left)
            invert(node.right)

        invert(root)
        return root

    def invertTree(self, root: TreeNode) -> TreeNode:

        def invert(node: TreeNode):
            if node:
                node.left, node.right = node.right, node.left
                invert(node.left) or invert(node.right)

        return invert(root) or root

    def invertTree(self, root: TreeNode) -> TreeNode:
        if root:
            root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
            return root

    def invertTree(self, root: TreeNode) -> TreeNode:
        stack = [root]
        while root and stack:
            node = stack.pop()
            node.left, node.right = node.right, node.left
            stack.extend(filter(None, [node.right, node.left]))
        return root


def test():
    from python3 import Codec
    serialized_trees = [
        "[4,2,7,1,3,6,9]",
        ]
    expectations = [
        "[4,7,2,9,6,3,1]"
        ]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().invertTree(root)
        assert solution == Codec.deserialize(expected)
