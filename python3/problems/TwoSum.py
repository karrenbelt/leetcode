### Source : https://leetcode.com/problems/two-sum/
### Author : karrenbelt
### Date   : 2019-03-29

##################################################################################################### 
#
# Given an array of integers, return indices of the two numbers such that they add up to a specific 
# target.
# 
# You may assume that each input would have exactly one solution, and you may not use the same 
# element twice.
# 
# Example:
# 
# Given nums = [2, 7, 11, 15], target = 9,
# 
# Because nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1].
# 
#####################################################################################################

from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:  # O(n^2) time, O(1) space
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]

    def twoSum(self, nums: List[int], target: int) -> List[int]:  # O(n^2) time, O(1) space
        return next([i, j] for i in range(len(nums)) for j in range(i + 1, len(nums)) if nums[i] + nums[j] == target)

    def twoSum(self, nums: List[int], target: int) -> List[int]:  # O(n) time, O(n) space
        d = {}
        for i, n in enumerate(nums):
            if target - n in d:
                return [d[target - n], i]
            d[n] = i


def test():
    numbers = [
        [2, 7, 11, 15],
        [3, 2, 4],
        [3, 3],
        ]
    targets = [9, 6, 6]
    expectations = [
        [0, 1],
        [1, 2],
        [0, 1]
        ]
    for nums, target, expected in zip(numbers, targets, expectations):
        solution = Solution().twoSum(nums, target)
        assert solution == expected
