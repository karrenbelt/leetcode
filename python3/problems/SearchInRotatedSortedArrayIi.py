### Source : https://leetcode.com/problems/search-in-rotated-sorted-array-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# You are given an integer array nums sorted in ascending order (not necessarily distinct values), 
# and an integer target.
# 
# Suppose that nums is rotated at some pivot unknown to you beforehand (i.e., [0,1,2,4,4,4,5,6,6,7] 
# might become [4,5,6,6,7,0,1,2,4,4]).
# 
# If target is found in the array return its index, otherwise, return -1.
# 
# Example 1:
# Input: nums = [2,5,6,0,0,1,2], target = 0
# Output: true
# Example 2:
# Input: nums = [2,5,6,0,0,1,2], target = 3
# Output: false
# 
# Constraints:
# 
# 	1 <= nums.length <= 5000
# 	-104 <= nums[i] <= 104
# 	nums is guaranteed to be rotated at some pivot.
# 	-104 <= target <= 104
# 
# Follow up: This problem is the same as Search in Rotated Sorted Array, where nums may contain 
# duplicates. Would this affect the run-time complexity? How and why?
#####################################################################################################

from typing import List


class Solution:

    def search(self, nums: List[int], target: int) -> bool:  # since worst case is O(n) anyway
        return target in nums

    def search(self, nums: List[int], target: int) -> bool:  # O(log n) if no duplicates, worst case O(n)
        left, right = 0, len(nums) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if nums[mid] == target:
                return True
            while left < mid and nums[left] == nums[mid]:  # cannot know which side is sorted
                left += 1
            if nums[left] <= nums[mid]:
                if nums[left] <= target < nums[mid]:
                    right = mid - 1
                else:
                    left = mid + 1
            else:
                if nums[mid] < target <= nums[right]:
                    left = mid + 1
                else:
                    right = mid - 1

        return False

    def search(self, nums: List[int], target: int) -> bool:

        def dfs(beg, end):
            if end - beg <= 1:
                return target in nums[beg: end + 1]  # O(n) part

            mid = (beg + end) // 2
            if nums[mid] > nums[end]:  # eg. 3,4,5,6,7,1,2
                if nums[end] < target <= nums[mid]:
                    return dfs(beg, mid)
                else:
                    return dfs(mid + 1, end)
            elif nums[mid] < nums[end]:  # eg. 6,7,1,2,3,4,5
                if nums[mid] < target <= nums[end]:
                    return dfs(mid + 1, end)
                else:
                    return dfs(beg, mid)
            else:
                return dfs(mid + 1, end) or dfs(beg, mid)

        return dfs(0, len(nums) - 1)


def test():
    nums = [2, 5, 6, 0, 0, 1, 2]
    targets = [0, 3]
    expectations = [True, False]
    for target, expected in zip(targets, expectations):
        solution = Solution().search(nums, target)
        assert solution == expected
