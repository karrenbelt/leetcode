### Source : https://leetcode.com/problems/max-consecutive-ones/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-26

##################################################################################################### 
#
# Given a binary array, find the maximum number of consecutive 1s in this array.
# 
# Example 1:
# 
# Input: [1,1,0,1,1,1]
# Output: 3
# Explanation: The first two digits or the last three digits are consecutive 1s.
#     The maximum number of consecutive 1s is 3.
# 
# Note:
# 
# The input array will only contain 0 and 1.
# The length of input array is a positive integer and will not exceed 10,000
# 
#####################################################################################################

from typing import List


class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        max_len = 0
        j = 0
        for i in range(len(nums)):
            if nums[i]:
                j += 1
                max_len = max(max_len, j)
            else:
                j = 0
        return max_len

    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        return max(map(lambda x: len(x), ''.join([str(num) for num in nums]).split('0')))

    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        return len(max(''.join(map(str, nums)).split('0'), key=len))


def test():
    arguments = [
        [1, 1, 0, 1, 1, 1],
        ]
    expectations = [3]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findMaxConsecutiveOnes(nums)
        assert solution == expected
