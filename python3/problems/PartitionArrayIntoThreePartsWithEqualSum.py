### Source : https://leetcode.com/problems/partition-array-into-three-parts-with-equal-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-23

##################################################################################################### 
#
# Given an array of integers arr, return true if we can partition the array into three non-empty 
# parts with equal sums.
# 
# Formally, we can partition the array if we can find indexes i + 1 < j with (arr[0] + arr[1] + ... + 
# arr[i] == arr[i + 1] + arr[i + 2] + ... + arr[j - 1] == arr[j] + arr[j + 1] + ... + arr[arr.length 
# - 1])
# 
# Example 1:
# 
# Input: arr = [0,2,1,-6,6,-7,9,1,2,0,1]
# Output: true
# Explanation: 0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1
# 
# Example 2:
# 
# Input: arr = [0,2,1,-6,6,7,9,-1,2,0,1]
# Output: false
# 
# Example 3:
# 
# Input: arr = [3,3,6,5,-2,2,5,1,-9,4]
# Output: true
# Explanation: 3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4
# 
# Constraints:
# 
# 	3 <= arr.length <= 5 * 104
# 	-104 <= arr[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def canThreePartsEqualSum(self, arr: List[int]) -> bool:  # wrong since numbers can be negative
        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in arr]
        return not prefix_sum[-1] % 3 and not {prefix_sum[-1] // 3 * i for i in range(1, 3)}.difference(prefix_sum)

    def canThreePartsEqualSum(self, arr: List[int]) -> bool:  # O(n) time and O(1) space
        part = ctr = 0
        target, remainder = divmod(sum(arr), 3)
        for n in arr:
            part += n
            if part == target:
                ctr += 1
                part = 0
        return not remainder and ctr >= 3


def test():
    arguments = [
        [0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1],
        [0, 2, 1, -6, 6, 7, 9, -1, 2, 0, 1],
        [3, 3, 6, 5, -2, 2, 5, 1, -9, 4],
        [0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1],
    ]
    expectations = [True, False, True, True]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().canThreePartsEqualSum(arr)
        assert solution == expected
