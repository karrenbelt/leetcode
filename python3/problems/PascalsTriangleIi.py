### Source : https://leetcode.com/problems/pascals-triangle-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given an integer rowIndex, return the rowIndexth (0-indexed) row of the Pascal's triangle.
# 
# In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:
# 
# Example 1:
# Input: rowIndex = 3
# Output: [1,3,3,1]
# Example 2:
# Input: rowIndex = 0
# Output: [1]
# Example 3:
# Input: rowIndex = 1
# Output: [1,1]
# 
# Constraints:
# 
# 	0 <= rowIndex <= 33
# 
# Follow up: Could you optimize your algorithm to use only O(rowIndex) extra space?
#####################################################################################################

from typing import List


class Solution:
    def getRow(self, rowIndex: int) -> List[int]:  # dp
        row = [1]
        for _ in range(rowIndex):
            row = [1] + [sum(row[j-1:j+1]) for j in range(1, len(row))] + [1]
        return row

    def getRow(self, rowIndex: int) -> List[int]:
        row = [1]
        for _ in range(rowIndex):
            row = [x + y for x, y in zip([0]+row, row+[0])]
        return row

    def getRow(self, rowIndex: int) -> List[int]:
        row = [0] * (rowIndex + 1)

        for i in range(rowIndex + 1):
            for j in range(i, -1, -1):
                if j == 0 or j == i:
                    row[j] = 1
                else:
                    row[j] = row[j - 1] + row[j]

        return row


def test():
    numbers = [3, 0, 1]
    expectations = [
        [1, 3, 3, 1],
        [1],
        [1, 1],
        ]
    for rowIndex, expected in zip(numbers, expectations):
        solution = Solution().getRow(rowIndex)
        assert solution == expected
