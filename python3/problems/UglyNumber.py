### Source : https://leetcode.com/problems/ugly-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-18

##################################################################################################### 
#
# Write a program to check whether a given number is an ugly number.
# 
# Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.
# 
# Example 1:
# 
# Input: 6
# Output: true
# Explanation: 6 = 2 &times; 3
# 
# Example 2:
# 
# Input: 8
# Output: true
# Explanation: 8 = 2 &times; 2 &times; 2
# 
# Example 3:
# 
# Input: 14
# Output: false 
# Explanation: 14 is not ugly since it includes another prime factor 7.
# 
# Note:
# 
# 	1 is typically treated as an ugly number.
# 	Input is within the 32-bit signed integer range: [&minus;231,  231 &minus; 1].
#####################################################################################################


class Solution:
    def isUgly(self, num: int) -> bool:  # TLE: 512 / 1012 passed

        def prime_factors(n):
            factors = set()
            d = 2
            while d * d <= n:
                while n > 1:
                    while n % d == 0:
                        print(d)
                        factors.add(d)
                        n = n / d
                    d += 1
            return factors

        if num in (1, 2, 3):
            return True
        if num < 1:
            return False
        factors = prime_factors(num)
        return not bool(factors.difference([2, 3, 5]))

    def isUgly(self, num: int) -> bool:
        if num in (1, 2, 3):
            return True
        if num < 1:
            return False
        for p in 2, 3, 5:
            while num % p == 0 < num:
                num /= p
        return num == 1

    def isUgly(self, num: int) -> bool:
        for p in 2, 3, 5:
            while num % p == 0 < num:
                num /= p
        return num == 1

    def isUgly(self, num: int) -> bool:
        if num > 0:
            for p in 2, 3, 5:
                while num % p == 0:
                    num /= p
        return num == 1


def test():
    numbers = [6, 8, 14, 1, 2147483648, -2147483648, 905391974]  # 905391974 TLE in first implementation
    expectations = [True, True, False, True, True, False]
    for num, expected in zip(numbers, expectations):
        solution = Solution().isUgly(num)
        assert solution == expected
