### Source : https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-26

##################################################################################################### 
#
# A decimal number is called deci-binary if each of its digits is either 0 or 1 without any leading 
# zeros. For example, 101 and 1100 are deci-binary, while 112 and 3001 are not.
# 
# Given a string n that represents a positive decimal integer, return the minimum number of positive 
# deci-binary numbers needed so that they sum up to n.
# 
# Example 1:
# 
# Input: n = "32"
# Output: 3
# Explanation: 10 + 11 + 11 = 32
# 
# Example 2:
# 
# Input: n = "82734"
# Output: 8
# 
# Example 3:
# 
# Input: n = "27346209830709182346"
# Output: 9
# 
# Constraints:
# 
# 	1 <= n.length <= 105
# 	n consists of only digits.
# 	n does not contain any leading zeros and represents a positive integer.
#####################################################################################################


class Solution:
    def minPartitions(self, n: str) -> int:  # 48 ms
        return int(max(n))

    def minPartitions(self, n: str) -> int:  # 40 ms
        return int(max(set(n)))


def test():
    arguments = ["32", "82734", "27346209830709182346"]
    expectations = [3, 8, 9]
    for n, expected in zip(arguments, expectations):
        solution = Solution().minPartitions(n)
        assert solution == expected
