### Source : https://leetcode.com/problems/magical-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# A magical string s consists of only '1' and '2' and obeys the following rules:
# 
# 	The string s is magical because concatenating the number of contiguous occurrences of 
# characters '1' and '2' generates the string s itself.
# 
# The first few elements of s is s = "1221121221221121122&hellip;&hellip;". If we group the 
# consecutive 1's and 2's in s, it will be "1 22 11 2 1 22 1 22 11 2 11 22 ......" and the 
# occurrences of 1's or 2's in each group are "1 2 2 1 1 2 1 2 2 1 2 2 ......". You can see that the 
# occurrence sequence is s itself.
# 
# Given an integer n, return the number of 1's in the first n number in the magical string s.
# 
# Example 1:
# 
# Input: n = 6
# Output: 3
# Explanation: The first 6 elements of magical string s is "12211" and it contains three 1's, so 
# return 3.
# 
# Example 2:
# 
# Input: n = 1
# Output: 1
# 
# Constraints:
# 
# 	1 <= n <= 105
#####################################################################################################


class Solution:
    def magicalString(self, n: int) -> int:  # O(n) time and O(n) space
        # 1 22 11 2 1 22 1 22 11 2 11 22
        # 1 2 2 1 1 2 1 2 2 1 2 2 1 1
        num, i = '122', 2
        while len(num) < n:
            num += ('2' if num[-1] == '1' else '1') if num[i] == '1' else ('22' if num[-1] == '1' else '11')
            i += 1
        return num[:n].count('1')

    def magicalString(self, n: int) -> int:  # O(n) time and O(n) space
        s, i = [1, 2, 2], 2
        while len(s) < n:
            i += s.extend([3 - s[-1]] * s[i]) or 1
        return s.count(1) - (len(s) > n and s[-1] == 1)


def test():
    arguments = [6, 1, 4]
    expectations = [3, 1, 2]
    for n, expected in zip(arguments, expectations):
        solution = Solution().magicalString(n)
        assert solution == expected
