### Source : https://leetcode.com/problems/two-sum-iv-input-is-a-bst/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-23

##################################################################################################### 
#
# Given the root of a Binary Search Tree and a target number k, return true if there exist two 
# elements in the BST such that their sum is equal to the given target.
# 
# Example 1:
# 
# Input: root = [5,3,6,2,4,null,7], k = 9
# Output: true
# 
# Example 2:
# 
# Input: root = [5,3,6,2,4,null,7], k = 28
# Output: false
# 
# Example 3:
# 
# Input: root = [2,1,3], k = 4
# Output: true
# 
# Example 4:
# 
# Input: root = [2,1,3], k = 1
# Output: false
# 
# Example 5:
# 
# Input: root = [2,1,3], k = 3
# Output: true
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-104 <= Node.val <= 104
# 	root is guaranteed to be a valid binary search tree.
# 	-105 <= k <= 105
#####################################################################################################

from typing import Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:

        def traverse(node: TreeNode):
            if not node:
                return
            traverse(node.left)
            counts[node.val] = counts.get(node.val, 0) + 1
            traverse(node.right)

        counts = {}
        traverse(root)
        return any(counts.get(k - n, 0) if k - n != n else counts.get(k - n, 0) > 1 for n in counts)

    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:

        def traverse(node: TreeNode):
            if not node:
                return False
            if k - node.val in seen:
                return True
            seen.add(node.val)
            return traverse(node.left) or traverse(node.right)

        seen = set()
        return traverse(root)

    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:
        queue, seen = [root], set()
        for node in queue:
            if k - node.val in seen:
                return True
            seen.add(node.val)
            if node.left:
                queue.append(node.left)
            if node.right:
                queue.append(node.right)
        return False

    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:

        def traverse(node: TreeNode):
            if node:
                yield from traverse(node.left)
                yield node.val
                yield from traverse(node.right)

        def reverse_traverse(node: TreeNode):
            if node:
                yield from reverse_traverse(node.right)
                yield node.val
                yield from reverse_traverse(node.left)

        left_traverse, right_traverse = traverse(root), reverse_traverse(root)
        left, right = - 1 << 31, 1 << 31
        while left < right:
            if left + right == k:
                return True
            elif left + right < k:
                left = next(left_traverse)
            else:
                right = next(right_traverse)
        return False


def test():
    from python3 import Codec
    arguments = [
        ("[5,3,6,2,4,null,7]", 9),
        ("[5,3,6,2,4,null,7]", 28),
        ("[2,1,3]", 4),
        ("[2,1,3]", 1),
        ("[2,1,3]", 3),
        ("[2,null,3]", 6),
        ("[2,1,3]", 4),
    ]
    expectations = [True, False, True, False, True, False]
    for (serialized_tree, k), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().findTarget(root, k)
        assert solution == expected
