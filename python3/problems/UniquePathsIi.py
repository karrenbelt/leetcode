### Source : https://leetcode.com/problems/unique-paths-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-12

##################################################################################################### 
#
# A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
# 
# The robot can only move either down or right at any point in time. The robot is trying to reach the 
# bottom-right corner of the grid (marked 'Finish' in the diagram below).
# 
# Now consider if some obstacles are added to the grids. How many unique paths would there be?
# 
# An obstacle and space is marked as 1 and 0 respectively in the grid.
# 
# Example 1:
# 
# Input: obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
# Output: 2
# Explanation: There is one obstacle in the middle of the 3x3 grid above.
# There are two ways to reach the bottom-right corner:
# 1. Right -> Right -> Down -> Down
# 2. Down -> Down -> Right -> Right
# 
# Example 2:
# 
# Input: obstacleGrid = [[0,1],[0,0]]
# Output: 1
# 
# Constraints:
# 
# 	m == obstacleGrid.length
# 	n == obstacleGrid[i].length
# 	1 <= m, n <= 100
# 	obstacleGrid[i][j] is 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        # important to note that we can only move right and down
        # hence the first row and column can be solved easily; 1 of no obstacle so far, else 0
        if obstacleGrid[0][0]:
            return 0
        dp = [[0] * len(obstacleGrid[0]) for _ in range(len(obstacleGrid))]
        # find first obstacles if they exist
        i = j = 0
        while i < len(obstacleGrid) and not obstacleGrid[i][0]:
            dp[i][0] = 1
            i += 1
        while j < len(obstacleGrid[0]) and not obstacleGrid[0][j]:
            dp[0][j] = 1
            j += 1
        for i in range(1, len(obstacleGrid)):
            for j in range(1, len(obstacleGrid[0])):
                if not obstacleGrid[i][j]:
                    dp[i][j] = dp[i][j - 1] + dp[i - 1][j]
        return dp[-1][-1]


def test():
    arguments = [
        [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
        [[0, 1], [0, 0]],
        [[0, 0], [1, 1], [0, 0]],
        ]
    expectations = [2, 1, 0]
    for obstacleGrid, expected in zip(arguments, expectations):
        solution = Solution().uniquePathsWithObstacles(obstacleGrid)
        assert solution == expected
