### Source : https://leetcode.com/problems/integer-replacement/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-27

##################################################################################################### 
#
# Given a positive integer n, you can apply one of the following operations:
# 
# 	If n is even, replace n with n / 2.
# 	If n is odd, replace n with either n + 1 or n - 1.
# 
# Return the minimum number of operations needed for n to become 1.
# 
# Example 1:
# 
# Input: n = 8
# Output: 3
# Explanation: 8 -> 4 -> 2 -> 1
# 
# Example 2:
# 
# Input: n = 7
# Output: 4
# Explanation: 7 -> 8 -> 4 -> 2 -> 1
# or 7 -> 6 -> 3 -> 2 -> 1
# 
# Example 3:
# 
# Input: n = 4
# Output: 2
# 
# Constraints:
# 
# 	1 <= n <= 231 - 1
#####################################################################################################


class Solution:
    def integerReplacement(self, n: int) -> int:  # O(log n) time and O(1) space
        ctr = 0
        while n > 1:
            if n & 1:  # try to make a multiple of 4, will always be faster. n == 3 is a corner case.
                n = n - 1 if n % 4 == 1 or n == 3 else n + 1
            else:
                n >>= 1
            ctr += 1
        return ctr


def test():
    arguments = [8, 7, 4, 65535]
    expectations = [3, 4, 2, 17]
    for n, expected in zip(arguments, expectations):
        solution = Solution().integerReplacement(n)
        assert solution == expected
