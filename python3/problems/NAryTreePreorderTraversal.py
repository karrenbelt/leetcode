### Source : https://leetcode.com/problems/n-ary-tree-preorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given an n-ary tree, return the preorder traversal of its nodes' values.
# 
# For example, given a 3-ary tree:
# 
# Return its preorder traversal as: [1,3,5,6,2,4].
# 
# Note:
# 
# Recursive solution is trivial, could you do it iteratively?
#####################################################################################################

from typing import List
from python3 import NAryTreeNode as Node


class Solution:
    def preorder(self, root: 'Node') -> List[int]:
        # Top Down:
        # 1. return specific value for null node
        # 2. update the answer if needed                              // answer <-- params
        # 3. for each child node root.children[k]:
        # 4.      ans[k] = top_down(root.children[k], new_params[k])  // new_params <-- root.val, params
        # 5. return the answer if needed

        def traverse(node):
            if not node:
                return
            ordered.append(node.val)
            for child in node.children:
                traverse(child)

        ordered = []
        traverse(root)
        return ordered

    def preorder(self, root: 'Node') -> List[int]:
        if not root:
            return []
        ordered, stack = [], [root]
        while stack:
            node = stack.pop()
            ordered.append(node.val)
            stack.extend(reversed(node.children))
        return ordered


def test():
    from python3 import NAryTreeCodec
    arguments = [
        "[1,null,3,2,4,null,5,6]",
        "[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]",
    ]
    expectations = [
        [1, 3, 5, 6, 2, 4],
        [1, 2, 3, 6, 7, 11, 14, 4, 8, 12, 5, 9, 13, 10],
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = NAryTreeCodec.deserialize(serialized_tree)
        solution = Solution().preorder(root)
        assert solution == expected
