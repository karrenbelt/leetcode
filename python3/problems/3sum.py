### Source : https://leetcode.com/problems/3sum/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-10

##################################################################################################### 
#
# Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find 
# all unique triplets in the array which gives the sum of zero.
# 
# Notice that the solution set must not contain duplicate triplets.
# 
# Example 1:
# Input: nums = [-1,0,1,2,-1,-4]
# Output: [[-1,-1,2],[-1,0,1]]
# Example 2:
# Input: nums = []
# Output: []
# Example 3:
# Input: nums = [0]
# Output: []
# 
# Constraints:
# 
# 	0 <= nums.length <= 3000
# 	-105 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:  # O(n^3), should yield TLE
        triplets = []
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                for k in range(j + 1, len(nums)):
                    subset = sorted([nums[i], nums[j], nums[k]])
                    if not sum(subset) and subset not in triplets:
                        triplets.append(subset)
        return triplets

    def threeSum(self, nums: List[int]) -> List[List[int]]:  # TLE -> 316 / 318 test cases passed.
        if len(nums) < 3:
            return []

        d = {}
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                d.setdefault(nums[i] + nums[j], []).append((i, j))

        triplets = set()
        for k, n in enumerate(nums):
            for i, j in d.get(-n, []):
                triplet = sorted([nums[i], nums[j], nums[k]])
                if i < j < k and not sum(triplet):
                    triplets.add(tuple(triplet))
        return list(map(list, triplets))

    def threeSum(self, nums: List[int]) -> List[List[int]]:  # works, but slow (no TLE)
        if len(nums) < 3:
            return []
        nums.sort()
        target, triplets = 0, set()
        last_index_of_value = {j: i for i, j in enumerate(nums)}  # prevent duplicates
        for i in range(len(nums) - 2):
            if nums[i] + 2 * nums[-1] < target:
                continue
            if 3 * nums[i] > target:
                break
            for j in range(i + 1, len(nums) - 1):
                c = - nums[i] - nums[j]
                if c > nums[-1]:
                    continue
                if c in last_index_of_value and last_index_of_value[c] > j:
                    triplets.add((nums[i], nums[j], c))
        return list(map(list, triplets))

    def threeSum(self, nums: List[int]) -> List[List[int]]:  # O(n^2)

        def k_sum(nums: List[int], target: int, k: int) -> List[List[int]]:
            res = []
            if len(nums) == 0 or nums[0] * k > target or target > nums[-1] * k:
                return res
            if k == 2:
                return two_sum(nums, target)
            for i in range(len(nums)):
                if i == 0 or nums[i - 1] != nums[i]:
                    for group in k_sum(nums[i + 1:], target - nums[i], k - 1):
                        res.append([nums[i]] + group)
            return res

        def two_sum(nums: List[int], target: int) -> List[List[int]]:
            pair_sum = []
            left, right = 0, len(nums) - 1
            while left < right:
                total = nums[left] + nums[right]  # second condition avoids duplication if sums equals target
                if total < target or (left > 0 and nums[left] == nums[left - 1]):
                    left += 1  # same as above to avoid duplication of results
                elif total > target or (right < len(nums) - 1 and nums[right] == nums[right + 1]):
                    right -= 1
                else:
                    pair_sum.append([nums[left], nums[right]])
                    left += 1
                    right -= 1
            return pair_sum

        nums.sort()
        return k_sum(nums, target=0, k=3)

    def threeSum(self, nums: List[int]) -> List[List[int]]:  # O(n^2)
        triplets = nums.sort() or []
        for i in range(len(nums) - 2):
            if i > 0 and nums[i] == nums[i - 1]:  # prevent duplicates
                continue
            left, right = i + 1, len(nums) - 1
            while left < right:
                s = nums[i] + nums[left] + nums[right]
                if s < 0:
                    left += 1
                elif s > 0:
                    right -= 1
                else:
                    triplets.append((nums[i], nums[left], nums[right]))
                    while left < right and nums[left] == nums[left + 1]:
                        left += 1
                    while left < right and nums[right] == nums[right - 1]:
                        right -= 1
                    left += 1
                    right -= 1
        return triplets

    def threeSum(self, nums: List[int]) -> List[List[int]]:  # O(n^2)
        # a + b = -c. 3 sum reduces to 2 sum problem.
        nums.sort()
        n, result = len(nums), []
        for i in range(n):
            if i > 0 and nums[i] == nums[i - 1]:  # skip duplicates 3 sum
                continue
            target, left, right = -nums[i], i + 1, n - 1
            while left < right:
                if nums[left] + nums[right] == target:
                    result.append([nums[i], nums[left], nums[right]])
                    left += 1
                    while left < right and nums[left] == nums[left - 1]:  # skip duplicates 2 sum
                        left += 1
                elif nums[left] + nums[right] < target:
                    left += 1
                else:
                    right -= 1
        return result


def test():
    arrays_of_numbers = [
        [-1, 0, 1, 2, -1, -4],
        [],
        [0],
        ]
    expectations = [
        [[-1, -1, 2], [-1, 0, 1]],
        [],
        [],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().threeSum(nums)
        assert sorted(map(sorted, solution)) == sorted(map(sorted, expected))
