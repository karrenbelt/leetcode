### Source : https://leetcode.com/problems/find-all-anagrams-in-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
# 
# Strings consists of lowercase English letters only and the length of both strings s and p will not 
# be larger than 20,100.
# 
# The order of output does not matter.
# 
# Example 1:
# 
# Input:
# s: "cbaebabacd" p: "abc"
# 
# Output:
# [0, 6]
# 
# Explanation:
# The substring with start index = 0 is "cba", which is an anagram of "abc".
# The substring with start index = 6 is "bac", which is an anagram of "abc".
# 
# Example 2:
# 
# Input:
# s: "abab" p: "ab"
# 
# Output:
# [0, 1, 2]
# 
# Explanation:
# The substring with start index = 0 is "ab", which is an anagram of "ab".
# The substring with start index = 1 is "ba", which is an anagram of "ab".
# The substring with start index = 2 is "ab", which is an anagram of "ab".
# 
#####################################################################################################

from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:  # TLE: 24/36 passed
        # we need all permutations of p, then find all occurrences in s

        def permutations(string: str):
            paths = ['']
            for c in string:
                tmp = []
                for path in paths:
                    for i in range(len(path) + 1):
                        tmp.append(path[:i] + c + path[i:])
                paths = tmp
            return paths

        if len(p) > len(s) or set(p).difference(s):
            return []

        indices = set()
        for perm in permutations(p):
            indices.update([i for i in range(len(s)) if s.startswith(perm, i)])
        return list(indices)

    def findAnagrams(self, s: str, p: str) -> List[int]:  # sliding window: O(n) time and O(n) space

        def count(string: str):
            counts = {c: 0 for c in set(s + p)}  # important to initialize all to zero for matching!
            for c in string:
                counts[c] = counts.get(c, 0) + 1
            return counts

        indices = []
        start, end = 0, len(p)
        s_counts, p_counts = map(count, (s[start:end], p))
        while end < len(s):
            if s_counts == p_counts:
                indices.append(start)
            s_counts[s[start]] -= 1
            s_counts[s[end]] = s_counts.get(s[end], 0) + 1
            start += 1
            end += 1
        if s_counts == p_counts:  # don't forget the last iteration
            indices.append(start)
        return indices


def test():
    arguments = [
        ("cbaebabacd", "abc"),
        ("abab", "ab"),
        ("baa", "aa"),
        ]
    expectations = [
        [0, 6],
        [0, 1, 2],
        [1],
        ]
    for (s, p), expected in zip(arguments, expectations):
        solution = Solution().findAnagrams(s, p)
        assert solution == expected
