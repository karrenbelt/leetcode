### Source : https://leetcode.com/problems/longest-increasing-path-in-a-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-10

##################################################################################################### 
#
# Given an m x n integers matrix, return the length of the longest increasing path in matrix.
# 
# From each cell, you can either move in four directions: left, right, up, or down. You may not move 
# diagonally or move outside the boundary (i.e., wrap-around is not allowed).
# 
# Example 1:
# 
# Input: matrix = [[9,9,4],[6,6,8],[2,1,1]]
# Output: 4
# Explanation: The longest increasing path is [1, 2, 6, 9].
# 
# Example 2:
# 
# Input: matrix = [[3,4,5],[3,2,6],[2,2,1]]
# Output: 4
# Explanation: The longest increasing path is [3, 4, 5, 6]. oving diagonally is not allowed.
# 
# Example 3:
# 
# Input: matrix = [[1]]
# Output: 1
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[i].length
# 	1 <= m, n <= 200
# 	0 <= matrix[i][j] <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:  # TLE: 137 / 138 test cases passed.
        # we can reverse it:
        # from any starting position, we find the longest decreasing path, then at the end of the path store the value
        # we cache the result for any position
        from functools import lru_cache

        @lru_cache(maxsize=None)
        # issue with this implementation is that length is larger if you manage to start on higher up:
        # solution: we can use a memo that only caches x and y
        def dfs(x: int, y: int, previous: int, length: int):
            nonlocal longest
            if not 0 <= x < len(matrix) or not 0 <= y < len(matrix[0]):
                return
            if matrix[x][y] < previous:
                longest = max(longest, length)
                for direction in directions:
                    nr, nc = x + direction[0], y + direction[1]
                    dfs(nr, nc, matrix[x][y], length + 1)

        longest = 0
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                dfs(i, j, 2**31, 0)
        return longest + 1

    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:  # O(m * n) time and O(m * n) space

        def dfs(i: int, j: int) -> int:
            if (i, j) not in memo:
                max_path = 0
                for direction in directions:
                    nr, nc = i + direction[0], j + direction[1]
                    if 0 <= nr < len(matrix) and 0 <= nc < len(matrix[0]) and matrix[nr][nc] > matrix[i][j]:
                        max_path = max(dfs(nr, nc), max_path)
                memo[(i, j)] = max_path + 1
            return memo[(i, j)]

        memo = {}
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        return max(dfs(x, y) for y in range(len(matrix[0])) for x in range(len(matrix)))

    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:  # O(m * n) time and O(m * n) space
        # build DAG, then perform topological sort
        from collections import deque

        graph = {}
        indegree = {}
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                for x, y in ((i + di, j + dj) for di, dj in directions):
                    if 0 <= x < len(matrix) and 0 <= y < len(matrix[0]) and matrix[i][j] < matrix[x][y]:
                        graph.setdefault((i, j), []).append((x, y))
                        indegree[x, y] = indegree.get((x, y), 0) + 1

        max_path = 0
        queue = deque((i, j) for i in range(len(matrix)) for j in range(len(matrix[0])) if (i, j) not in indegree)
        while queue:
            max_path += 1
            for _ in range(len(queue)):
                node = queue.popleft()
                for neighbor in graph.get(node, []):
                    indegree[neighbor] -= 1
                    if not indegree[neighbor]:
                        queue.append(neighbor)
        return max_path

    def longestIncreasingPath(self, matrix: List[List[int]]) -> int:  # O(m * n) time and O(m * n) space
        # build DAG, then perform topological sort
        graph = {}
        indegree = {}
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                for x, y in ((i + di, j + dj) for di, dj in directions):
                    if 0 <= x < len(matrix) and 0 <= y < len(matrix[0]) and matrix[i][j] < matrix[x][y]:
                        graph.setdefault((i, j), []).append((x, y))
                        indegree[x, y] = indegree.get((x, y), 0) + 1

        max_path = 0
        stack = [(i, j) for i in range(len(matrix)) for j in range(len(matrix[0])) if (i, j) not in indegree]
        while stack:
            max_path += 1
            nodes = []
            for neighbor in (neighbor for node in stack for neighbor in graph.get(node, [])):
                indegree[neighbor] -= 1
                if not indegree[neighbor]:
                    nodes.append(neighbor)
            stack = nodes
        return max_path


def test():
    arguments = [
        [[9, 9, 4], [6, 6, 8], [2, 1, 1]],
        [[3, 4, 5], [3, 2, 6], [2, 2, 1]],
        [[1]],
        ]
    expectations = [4, 4, 1]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().longestIncreasingPath(matrix)
        assert solution == expected
test()