### Source : https://leetcode.com/problems/last-stone-weight-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# We have a collection of rocks, each rock has a positive integer weight.
# 
# Each turn, we choose any two rocks and smash them together.  Suppose the stones have weights x and 
# y with x <= y.  The result of this smash is:
# 
# 	If x == y, both stones are totally destroyed;
# 	If x != y, the stone of weight x is totally destroyed, and the stone of weight y has new 
# weight y-x.
# 
# At the end, there is at most 1 stone left.  Return the smallest possible weight of this stone (the 
# weight is 0 if there are no stones left.)
# 
# Example 1:
# 
# Input: [2,7,4,1,8,1]
# Output: 1
# Explanation: 
# We can combine 2 and 4 to get 2 so the array converts to [2,7,1,8,1] then,
# we can combine 7 and 8 to get 1 so the array converts to [2,1,1,1] then,
# we can combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
# we can combine 1 and 1 to get 0 so the array converts to [1] then that's the optimal value.
# 
# Note:
# 
# 	1 <= stones.length <= 30
# 	1 <= stones[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def lastStoneWeightII(self, stones: List[int]) -> int:  # O(2^n) time
        # need to split the stones in two groups and minimize the difference of their sum
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def backtracking(a: int, b: int) -> int:
            if not stones:
                return abs(a - b)
            stone = stones.pop()
            minimum = min(backtracking(a + stone, b), backtracking(a, b + stone))
            stones.append(stone)
            return minimum

        return backtracking(0, 0)

    def lastStoneWeightII(self, stones: List[int]) -> int:
        # knapsack problem: divide numbers into two groups,
        # what is the minimum difference between the sum of two groups?
        dp = {0}
        for stone in stones:
            dp = {stone + x for x in dp} | {stone - x for x in dp}
        return min(map(abs, dp))


def test():
    arguments = [
        [2, 7, 4, 1, 8, 1],
        [31, 26, 33, 21, 40],
        [1, 2],
    ]
    expectations = [1, 5, 1]
    for stones, expected in zip(arguments, expectations):
        solution = Solution().lastStoneWeightII(stones)
        assert solution == expected
