### Source : https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-19

##################################################################################################### 
#
# Given a string s of '(' , ')' and lowercase English characters. 
# 
# Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that 
# the resulting parentheses string is valid and return any valid string.
# 
# Formally, a parentheses string is valid if and only if:
# 
# 	It is the empty string, contains only lowercase characters, or
# 	It can be written as AB (A concatenated with B), where A and B are valid strings, or
# 	It can be written as (A), where A is a valid string.
# 
# Example 1:
# 
# Input: s = "lee(t(c)o)de)"
# Output: "lee(t(c)o)de"
# Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
# 
# Example 2:
# 
# Input: s = "a)b(c)d"
# Output: "ab(c)d"
# 
# Example 3:
# 
# Input: s = "))(("
# Output: ""
# Explanation: An empty string is also valid.
# 
# Example 4:
# 
# Input: s = "(a(b(c)d)"
# Output: "a(b(c)d)"
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	s[i] is one of  '(' , ')' and lowercase English letters.
#####################################################################################################


class Solution:
    def minRemoveToMakeValid(self, s: str) -> str:  # O(n)
        remove = set()
        bracket_open = []
        for i in range(len(s)):
            c = s[i]
            if c == '(':
                bracket_open.append(i)
            elif c == ')':
                if bracket_open:
                    bracket_open.pop()
                else:
                    remove.add(i)
        remove.update(bracket_open)
        return ''.join([s[i] for i in range(len(s)) if i not in remove])


def test():
    arguments = [
        "lee(t(c)o)de)",
        "a)b(c)d",
        "))((",
        "(a(b(c)d)",
    ]
    expectations = [
        "lee(t(c)o)de",
        "ab(c)d",
        "",
        "a(b(c)d)",
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minRemoveToMakeValid(s)
        assert solution == expected
