### Source : https://leetcode.com/problems/kth-largest-element-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-23

##################################################################################################### 
#
# Given an integer array nums and an integer k, return the kth largest element in the array.
# 
# Note that it is the kth largest element in the sorted order, not the kth distinct element.
# 
# Example 1:
# Input: nums = [3,2,1,5,6,4], k = 2
# Output: 5
# Example 2:
# Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
# Output: 4
# 
# Constraints:
# 
# 	1 <= k <= nums.length <= 104
# 	-104 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        return sorted(nums)[-k]
    
    def findKthLargest(self, nums, k):  # quick-select: O(n)
        import random

        if not nums:
            return

        pivot = random.choice(nums)
        left = [x for x in nums if x > pivot]
        mid = [x for x in nums if x == pivot]
        right = [x for x in nums if x < pivot]

        if k <= len(left):
            return self.findKthLargest(left, k)
        elif k > len(left) + len(mid):
            return self.findKthLargest(right, k - len(left) - len(mid))
        return mid[0]


def test():
    arguments = [
        ([3, 2, 1, 5, 6, 4], 2),
        ([3, 2, 3, 1, 2, 4, 5, 5, 6], 4),
        ]
    expectations = [5, 4]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().findKthLargest(nums, k)
        assert solution == expected
test()