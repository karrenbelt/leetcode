### Source : https://leetcode.com/problems/power-of-four/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-01

##################################################################################################### 
#
# Given an integer n, return true if it is a power of four. Otherwise, return false.
# 
# An integer n is a power of four, if there exists an integer x such that n == 4x.
# 
# Example 1:
# Input: n = 16
# Output: true
# Example 2:
# Input: n = 5
# Output: false
# Example 3:
# Input: n = 1
# Output: true
# 
# Constraints:
# 
# 	-231 <= n <= 231 - 1
# 
# Follow up: Could you solve it without loops/recursion?
#####################################################################################################

class Solution:
    def isPowerOfFour(self, n: int) -> bool:
        return (n**(1/4)).is_integer()

    def isPowerOfFour(self, n: int) -> bool:
        while n > 1:
            n /= 4
        return n == 1

    def isPowerOfFour(self, n: int) -> bool:
        return n > 0 and bin(n).count('1') == 1 and bin(n).count('0') % 2

    def isPowerOfFour(self, n: int) -> bool:
        return 0 < n == 0b1010101010101010101010101010101 & n and n & (n - 1) == 0


def test():
    arguments = [16, 5, 1, 7, 64, 2]
    expectations = [True, False, True, False, True, False]
    for n, expected in zip(arguments, expectations):
        solution = Solution().isPowerOfFour(n)
        assert solution == expected, n
