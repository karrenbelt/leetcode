### Source : https://leetcode.com/problems/design-circular-queue/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Design your implementation of the circular queue. The circular queue is a linear data structure in 
# which the operations are performed based on FIFO (First In First Out) principle and the last 
# position is connected back to the first position to make a circle. It is also called "Ring Buffer".
# 
# One of the benefits of the circular queue is that we can make use of the spaces in front of the 
# queue. In a normal queue, once the queue becomes full, we cannot insert the next element even if 
# there is a space in front of the queue. But using the circular queue, we can use the space to store 
# new values.
# 
# Your implementation should support following operations:
# 
# 	yCircularQueue(k): Constructor, set the size of the queue to be k.
# 	Front: Get the front item from the queue. If the queue is empty, return -1.
# 	Rear: Get the last item from the queue. If the queue is empty, return -1.
# 	enQueue(value): Insert an element into the circular queue. Return true if the operation is 
# successful.
# 	deQueue(): Delete an element from the circular queue. Return true if the operation is 
# successful.
# 	isEmpty(): Checks whether the circular queue is empty or not.
# 	isFull(): Checks whether the circular queue is full or not.
# 
# Example:
# 
# yCircularQueue circularQueue = new yCircularQueue(3); // set the size to be 3
# circularQueue.enQueue(1);  // return true
# circularQueue.enQueue(2);  // return true
# circularQueue.enQueue(3);  // return true
# circularQueue.enQueue(4);  // return false, the queue is full
# circularQueue.Rear();  // return 3
# circularQueue.isFull();  // return true
# circularQueue.deQueue();  // return true
# circularQueue.enQueue(4);  // return true
# circularQueue.Rear();  // return 4
# 
# Note:
# 
# 	All values will be in the range of [0, 1000].
# 	The number of operations will be in the range of [1, 1000].
# 	Please do not use the built-in Queue library.
# 
#####################################################################################################

# utility function for cycling iterables
def cycle(iterable):
    while iterable:
        for element in iterable:
            yield element


class MyCircularQueue:

    def __init__(self, k: int):
        """Initialize your data structure here. Set the size of the queue to be k."""
        self.size = k
        self.queue = [None] * k
        self.front = 0
        self.rear = -1
        self.cnts = 0
        # self.head_cycler = cycle(range(1, k))
        # self.tail_cycler = cycle(range(k))

    def enQueue(self, value: int) -> bool:
        """Insert an element into the circular queue. Return true if the operation is successful."""
        if self.isFull():
            return False
        self.cnts += 1
        self.rear = (self.rear + 1) % self.size
        self.queue[self.rear] = value
        return True

    def deQueue(self) -> bool:
        """Delete an element from the circular queue. Return true if the operation is successful."""
        if self.isEmpty():
            return False
        self.cnts -= 1
        self.front = (self.front + 1) % self.size
        return True

    def Front(self) -> int:
        """Get the front item from the queue."""
        return self.queue[self.front] if not self.isEmpty() else -1

    def Rear(self) -> int:
        """Get the last item from the queue."""
        return self.queue[self.rear] if not self.isEmpty() else -1

    def isEmpty(self) -> bool:
        """Checks whether the circular queue is empty or not."""
        return self.cnts == 0

    def isFull(self) -> bool:
        """Checks whether the circular queue is full or not."""
        return self.cnts == self.size


# Your MyCircularQueue object will be instantiated and called as such:
# obj = MyCircularQueue(k)
# param_1 = obj.enQueue(value)
# param_2 = obj.deQueue()
# param_3 = obj.Front()
# param_4 = obj.Rear()
# param_5 = obj.isEmpty()
# param_6 = obj.isFull()


def test():
    operations = ["MyCircularQueue", "enQueue", "enQueue", "enQueue", "enQueue", "Rear",
               "isFull", "deQueue", "enQueue", "Rear"]
    arguments = [[3], [1], [2], [3], [4], [], [], [], [4], []]
    expectations = [None, True, True, True, False, 3, True, True, True, 4]
    obj = MyCircularQueue(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
