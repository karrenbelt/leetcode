### Source : https://leetcode.com/problems/cousins-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-18

##################################################################################################### 
#
# Given the root of a binary tree with unique values and the values of two different nodes of the 
# tree x and y, return true if the nodes corresponding to the values x and y in the tree are cousins, 
# or false otherwise.
# 
# Two nodes of a binary tree are cousins if they have the same depth with different parents.
# 
# Note that in a binary tree, the root node is at the depth 0, and children of each depth k node are 
# at the depth k + 1.
# 
# Example 1:
# 
# Input: root = [1,2,3,4], x = 4, y = 3
# Output: false
# 
# Example 2:
# 
# Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
# Output: true
# 
# Example 3:
# 
# Input: root = [1,2,3,null,4], x = 2, y = 3
# Output: false
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 100].
# 	1 <= Node.val <= 100
# 	Each node has a unique value.
# 	x != y
# 	x and y are exist in the tree.
#####################################################################################################

from typing import Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isCousins(self, root: Optional[TreeNode], x: int, y: int) -> bool:  # O(n) time
        level = [(root, None)]
        while level:
            next_level = []
            for node, parent in level:
                if node.val in (x, y):  # early return
                    other_value = x if node.val == y else y
                    other_parent = next((pair[1] for pair in level if pair[0].val == other_value), parent)
                    return parent != other_parent
                if node.left:
                    next_level.append((node.left, node))
                if node.right:
                    next_level.append((node.right, node))
            level = next_level

    def isCousins(self, root: Optional[TreeNode], x: int, y: int) -> bool:  # O(n) time

        def traverse(node: TreeNode, target: int, level: int = 0, parent=None):
            if node:
                if node.val == target:
                    return level, parent
                return traverse(node.left, target, level + 1, node) or traverse(node.right, target, level + 1, node)

        (x_level, x_parent), (y_level, y_parent) = traverse(root, x), traverse(root, y)
        return x_level == y_level and x_parent != y_parent


def test():
    from python3 import Codec
    arguments = [
        ("[1,2,3,4]", 4, 3),
        ("[1,2,3,null,4,null,5]", 5, 4),
        ("[1,2,3,null,4]", 2, 3),
    ]
    expectations = [False, True, False]
    for (serialized_tree, x, y), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().isCousins(root, x, y)
        assert solution == expected
