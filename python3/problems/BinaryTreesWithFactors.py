### Source : https://leetcode.com/problems/binary-trees-with-factors/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-13

##################################################################################################### 
#
# Given an array of unique integers, arr, where each integer arr[i] is strictly greater than 1.
# 
# We make a binary tree using these integers, and each number may be used for any number of times. 
# Each non-leaf node's value should be equal to the product of the values of its children.
# 
# Return the number of binary trees we can make. The answer may be too large so return the answer 
# modulo 109 + 7.
# 
# Example 1:
# 
# Input: arr = [2,4]
# Output: 3
# Explanation: We can make these trees: [2], [4], [4, 2, 2]
# 
# Example 2:
# 
# Input: arr = [2,4,5,10]
# Output: 7
# Explanation: We can make these trees: [2], [4], [5], [10], [4, 2, 2], [10, 2, 5], [10, 5, 2].
# 
# Constraints:
# 
# 	1 <= arr.length <= 1000
# 	2 <= arr[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def numFactoredBinaryTrees(self, arr: List[int]) -> int:  # dp: O(n^2) time
        # sort the array, all numbers in array can be roots, then we update as follows:
        # scan all roots for factors upto that root value, and update: dp[x] = dp[y] * dp[x/y]
        modulo_prime = 10**9 + 7
        arr = sorted(set(arr))
        dp = {}
        for i, n in enumerate(arr):
            dp[n] = 1
            for j in range(i):  # iterate smaller nodes than arr[i]
                y = arr[j]
                if y * (n//y) == n:
                    dp[n] += dp[y] * dp.get(n//y, 0)
        return divmod(sum(dp.values()), modulo_prime)[-1]

    def numFactoredBinaryTrees(self, arr: List[int]) -> int:  # dp: O(n^2) time, early stopping, fastest
        dp = {}
        arr.sort()
        for n in arr:
            dp[n] = 1
            sq = n ** 0.5
            dp[n] += dp.get(sq, 0) ** 2
            for sub in dp:
                if sub >= sq:
                    break
                elif n % sub == 0:
                    dp[n] += dp[sub] * dp.get(n // sub, 0) * 2
        return sum(dp.values()) % (10 ** 9 + 7)

    def numFactoredBinaryTrees(self, arr: List[int]) -> int:  # dp: O(n^2) time, 4 lines of code, slowest
        dp = arr.sort() or {}
        for n in arr:
            dp[n] = 1 + sum(dp[b] * dp.get(n / b, 0) for b in arr if b < n)
        return sum(dp.values()) % (10**9 + 7)


def test():
    arguments = [
        [2, 4],
        [2, 4, 5, 10],
        [15, 13, 22, 7, 11],
        [46, 144, 5040, 4488, 544, 380, 4410, 34, 11, 5, 3063808, 5550, 34496, 12, 540, 28, 18, 13, 2, 1056, 32710656,
         31, 91872, 23, 26, 240, 18720, 33, 49, 4, 38, 37, 1457, 3, 799, 557568, 32, 1400, 47, 10, 20774, 1296, 9, 21,
         92928, 8704, 29, 2162, 22, 1883700, 49588, 1078, 36, 44, 352, 546, 19, 523370496, 476, 24, 6000, 42, 30, 8,
         16262400, 61600, 41, 24150, 1968, 7056, 7, 35, 16, 87, 20, 2730, 11616, 10912, 690, 150, 25, 6, 14, 1689120,
         43, 3128, 27, 197472, 45, 15, 585, 21645, 39, 40, 2205, 17, 48, 136]
        ]
    expectations = [3, 7, 5, 509730797]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().numFactoredBinaryTrees(arr)
        assert solution == expected
