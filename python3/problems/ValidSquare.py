### Source : https://leetcode.com/problems/valid-square/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-18

##################################################################################################### 
#
# Given the coordinates of four points in 2D space p1, p2, p3 and p4, return true if the four points 
# construct a square.
# 
# The coordinate of a point pi is represented as [xi, yi]. The input is not given in any order.
# 
# A valid square has four equal sides with positive length and four equal angles (90-degree angles).
# 
# Example 1:
# 
# Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
# Output: true
# 
# Example 2:
# 
# Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,12]
# Output: false
# 
# Example 3:
# 
# Input: p1 = [1,0], p2 = [-1,0], p3 = [0,1], p4 = [0,-1]
# Output: true
# 
# Constraints:
# 
# 	p1.length == p2.length == p3.length == p4.length == 2
# 	-104 <= xi, yi <= 104
#####################################################################################################

from typing import List


class Solution:
    def validSquare(self, p1: List[int], p2: List[int], p3: List[int], p4: List[int]) -> bool:
        from itertools import combinations

        def distance(a: List[int], b: List[int]) -> int:
            return (a[0] - b[0])**2 + (a[1] - b[1])**2

        # p1, p2, p3, p4 = sorted((p1, p2, p3, p4))
        # return p1[0] == p2[0] and p3[0] == p4[0] and p1[1] == p3[1] and p2[1] == p4[1]  # only if not rotated
        # return p1[0] - p2[0] == p3[0] - p4[0] == p1[1] - p3[1] == p2[1] - p4[1]  # distance needs to be absolute
        # return abs(p1[0] - p2[0]) == abs(p3[0] - p4[0]) == abs(p1[1] - p3[1]) == abs(p2[1] - p4[1])  # no rhombuses
        distances = set(map(lambda pair_of_points: distance(*pair_of_points), combinations((p1, p2, p3, p4), 2)))
        return 0 not in distances and len(distances) == 2


def test():
    arguments = [
        ([0, 0], [1, 1], [1, 0], [0, 1]),
        ([0, 0], [1, 1], [1, 0], [0, 12]),
        ([1, 0], [-1, 0], [0, 1], [0, -1]),
        ([-2009, 2747], [-1566, 2436], [-2320, 2304], [-1877, 1993]),  # 199/253 before
        ([0, 0], [5, 0], [5, 4], [0, 4]),  # 249/253 before
    ]
    expectations = [True, False, True, True, False]
    for (p1, p2, p3, p4), expected in zip(arguments, expectations):
        solution = Solution().validSquare(p1, p2, p3, p4)
        print(solution)
        assert solution == expected
