### Source : https://leetcode.com/problems/number-of-matching-subsequences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-22

##################################################################################################### 
#
# Given a string s and an array of strings words, return the number of words[i] that is a subsequence 
# of s.
# 
# A subsequence of a string is a new string generated from the original string with some characters 
# (can be none) deleted without changing the relative order of the remaining characters.
# 
# 	For example, "ace" is a subsequence of "abcde".
# 
# Example 1:
# 
# Input: s = "abcde", words = ["a","bb","acd","ace"]
# Output: 3
# Explanation: There are three strings in words that are a subsequence of s: "a", "acd", "ace".
# 
# Example 2:
# 
# Input: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
# Output: 2
# 
# Constraints:
# 
# 	1 <= s.length <= 5 * 104
# 	1 <= words.length <= 5000
# 	1 <= words[i].length <= 50
# 	s and words[i] consist of only lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def numMatchingSubseq(self, s: str, words: List[str]) -> int:  # O(n^2) time: TLE -> 41 / 52 test cases passed.

        def is_subsequence(word: str) -> bool:
            it = iter(s)
            return all(any(c == ch for c in it) for ch in word)

        return sum(map(is_subsequence, words))

    def numMatchingSubseq(self, s: str, words: List[str]) -> int:  # O(n^2) time: 1500ms
        from collections import Counter

        def is_subsequence(word: str) -> bool:
            it = iter(s)
            return all(any(c == ch for c in it) for ch in word)

        return sum(count for w, count in Counter(words).items() if is_subsequence(w))

    def numMatchingSubseq(self, s: str, words: List[str]) -> int:  # TLE: 6 / 52 test cases passed.

        def build_trie():
            root = {}
            for word in words:
                node = root
                for c in word:
                    if c not in node:
                        node[c] = {}
                    node = node[c]
                node['.'] = node.get('.', 0) + 1
            return root

        def dfs(i: int, node: dict):
            count = node.pop('.', 0)
            return count if i == len(s) else count + dfs(i + 1, node) + dfs(i + 1, node.get(s[i], {}))

        return dfs(0, build_trie())

    def numMatchingSubseq(self, S: str, words: List[str]) -> int:  # 464 ms
        # build trie on the fly
        trie, count = {}, 0
        for w in words:
            trie.setdefault(w[0], []).append((1, w))

        for c in S:
            for i, w in trie.pop(c, []):
                if i == len(w):
                    count += 1 if i == len(w) else trie.setdefault(w[i], []).append((i + 1, w)) or 0
                else:
                    trie.setdefault(w[i], []).append((i + 1, w))
        return count

    def numMatchingSubseq(self, S: str, words: List[str]) -> int:  # 1044 ms
        trie, count = {}, 0
        for w in words:
            trie.setdefault(w[0], []).append((1, w))
        for items in map(lambda c: trie.pop(c, []), S):
            count += sum(1 if i == len(w) else trie.setdefault(w[i], []).append((i + 1, w)) or 0 for i, w in items)
        return count

    def numMatchingSubseq(self, S: str, words: List[str]) -> int:
        any((trie := {}) or (trie.setdefault(w[0], []).append((1, w)) for w in words))
        return sum(1 if i == len(w) else trie.setdefault(w[i], []).append((i + 1, w)) or 0 for items in
                   map(lambda c: trie.pop(c, []), S) for i, w in items)

    def numMatchingSubseq(self, S: str, words: List[str]) -> int:  # 620ms
        return (any((trie := {}) or (trie.setdefault(w[0], []).append((1, w)) for w in words)) or
                sum(1 if i == len(w) else trie.setdefault(w[i], []).append((i + 1, w)) or 0
                    for items in map(lambda c: trie.pop(c, []), S) for i, w in items))


def test():
    arguments = [
        ("abcde", ["a", "bb", "acd", "ace"]),
        ("dsahjpjauf", ["ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax"]),
        ("rwpddkvbnnuglnagtvamxkqtwhqgwbqgfbvgkwyuqkdwhzudsxvjubjgloeofnpjqlkdsqvruvabjrikfwronbrdyyjnakstqjac",
         ["wpddkvbnn", "lnagtva", "kvbnnuglnagtvamxkqtwhqgwbqgfbvgkwyuqkdwhzudsxvju", "rwpddkvbnnugln",
          "gloeofnpjqlkdsqvruvabjrikfwronbrdyyj", "vbgeinupkvgmgxeaaiuiyojmoqkahwvbpwugdainxciedbdkos",
          "mspuhbykmmumtveoighlcgpcapzczomshiblnvhjzqjlfkpina", "rgmliajkiknongrofpugfgajedxicdhxinzjakwnifvxwlokip",
          "fhepktaipapyrbylskxddypwmuuxyoivcewzrdwwlrlhqwzikq", "qatithxifaaiwyszlkgoljzkkweqkjjzvymedvclfxwcezqebx"])
    ]
    expectations = [3, 2, 5]
    for (s, words), expected in zip(arguments, expectations):
        solution = Solution().numMatchingSubseq(s, words)
        assert solution == expected, (solution, expected)


test()
