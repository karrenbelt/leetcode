### Source : https://leetcode.com/problems/reverse-string-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-27

##################################################################################################### 
#
# 
# Given a string and an integer k, you need to reverse the first k characters for every 2k characters 
# counting from the start of the string. If there are less than k characters left, reverse all of 
# them. If there are less than 2k but greater than or equal to k characters, then reverse the first k 
# characters and left the other as original.
# 
# Example:
# 
# Input: s = "abcdefg", k = 2
# Output: "bacdfeg"
# 
# Restrictions: 
# 
#  The string consists of lower English letters only.
#  Length of the given string and k will in the range [1, 10000]
#####################################################################################################

class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        s = list(s)
        for i in range(0, len(s), 2 * k):
            s[i:i + k] = reversed(s[i:i + k])
        return "".join(s)

    def reverseStr(self, s: str, k: int) -> str:
        return ''.join([s[i*k:(i+1)*k][::[-1, 1][i % 2]] for i in range(len(s)//k+1)])


s = 'abcdefg'
k = 2
# s = "abcdefg"  # expected = "dcbaefg"
# k = 4

sol = Solution().reverseStr(s, k)
print(sol)
