### Source : https://leetcode.com/problems/count-good-triplets/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-18

##################################################################################################### 
#
# Given an array of integers arr, and three integers a, b and c. You need to find the number of good 
# triplets.
# 
# A triplet (arr[i], arr[j], arr[k]) is good if the following conditions are true:
# 
# 	0 <= i < j < k < arr.length
# 	|arr[i] - arr[j]| <= a
# 	|arr[j] - arr[k]| <= b
# 	|arr[i] - arr[k]| <= c
# 
# Where |x| denotes the absolute value of x.
# 
# Return the number of good triplets.
# 
# Example 1:
# 
# Input: arr = [3,0,1,1,9,7], a = 7, b = 2, c = 3
# Output: 4
# Explanation: There are 4 good triplets: [(3,0,1), (3,0,1), (3,1,1), (0,1,1)].
# 
# Example 2:
# 
# Input: arr = [1,1,2,2,3], a = 0, b = 0, c = 1
# Output: 0
# Explanation: No triplet satisfies all conditions.
# 
# Constraints:
# 
# 	3 <= arr.length <= 100
# 	0 <= arr[i] <= 1000
# 	0 <= a, b, c <= 1000
#####################################################################################################

from typing import List


class Solution:
    def countGoodTriplets(self, arr: List[int], a: int, b: int, c: int) -> int:  # O(n^3) time
        ctr = 0
        for i in range(len(arr) - 2):
            for j in range(i + 1, len(arr) - 1):
                for k in range(j + 1, len(arr)):
                    ctr += abs(arr[i] - arr[j]) <= a and abs(arr[j] - arr[k]) <= b and abs(arr[i] - arr[k]) <= c
        return ctr

    def countGoodTriplets(self, arr: List[int], a: int, b: int, c: int) -> int:  # O(n^3) time
        from itertools import combinations

        def check(i, j, k) -> bool:
            return abs(i - j) <= a and abs(j - k) <= b and abs(i - k) <= c

        return sum(map(lambda triplet: check(*triplet), combinations(arr, 3)))

    def countGoodTriplets(self, arr: List[int], a: int, b: int, c: int) -> int:  # O(n^3) time
        from itertools import combinations
        return sum(abs(i - j) <= a and abs(j - k) <= b and abs(i - k) <= c for i, j, k in combinations(arr, 3))


def test():
    arguments = [
        ([3, 0, 1, 1, 9, 7], 7, 2, 3),
        ([1, 1, 2, 2, 3], 0, 0, 1),
    ]
    expectations = [4, 0]
    for (arr, a, b, c), expected in zip(arguments, expectations):
        solution = Solution().countGoodTriplets(arr, a, b, c)
        assert solution == expected
