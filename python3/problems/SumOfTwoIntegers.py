### Source : https://leetcode.com/problems/sum-of-two-integers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-06

##################################################################################################### 
#
# Given two integers a and b, return the sum of the two integers without using the operators + and -.
# 
# Example 1:
# Input: a = 1, b = 2
# Output: 3
# Example 2:
# Input: a = 2, b = 3
# Output: 5
# 
# Constraints:
# 
# 	-1000 <= a, b <= 1000
#####################################################################################################


class Solution:
    def getSum(self, a: int, b: int) -> int:  # O(1) time and O(1) space
        # adding numbers without carry we can use XOR. We can deal with the carry using AND and left shift
        mask = 0xffffffff
        while b & mask:
            carry = a & b
            a ^= b
            b = carry << 1
        return (a & mask) if b > mask else a  # for overflow conditions (e.g. 1 and -1)


def test():
    arguments = [
        (1, 2),
        (2, 3)
        ]
    expectations = [3, 5]
    for (a, b), expected in zip(arguments, expectations):
        solution = Solution().getSum(a, b)
        assert solution == expected
