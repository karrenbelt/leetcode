### Source : https://leetcode.com/problems/dungeon-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-07

##################################################################################################### 
#
# The demons had captured the princess and imprisoned her in the bottom-right corner of a dungeon. 
# The dungeon consists of m x n rooms laid out in a 2D grid. Our valiant knight was initially 
# positioned in the top-left room and must fight his way through dungeon to rescue the princess.
# 
# The knight has an initial health point represented by a positive integer. If at any point his 
# health point drops to 0 or below, he dies immediately.
# 
# Some of the rooms are guarded by demons (represented by negative integers), so the knight loses 
# health upon entering these rooms; other rooms are either empty (represented as 0) or contain magic 
# orbs that increase the knight's health (represented by positive integers).
# 
# To reach the princess as quickly as possible, the knight decides to move only rightward or downward 
# in each step.
# 
# Return the knight's minimum initial health so that he can rescue the princess.
# 
# Note that any room can contain threats or power-ups, even the first room the knight enters and the 
# bottom-right room where the princess is imprisoned.
# 
# Example 1:
# 
# Input: dungeon = [[-2,-3,3],[-5,-10,1],[10,30,-5]]
# Output: 7
# Explanation: The initial health of the knight must be at least 7 if he follows the optimal path: 
# RIGHT-> RIGHT -> DOWN -> DOWN.
# 
# Example 2:
# 
# Input: dungeon = [[0]]
# Output: 1
# 
# Constraints:
# 
# 	m == dungeon.length
# 	n == dungeon[i].length
# 	1 <= m, n <= 200
# 	-1000 <= dungeon[i][j] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def calculateMinimumHP(self, dungeon: List[List[int]]) -> int:   # top-down dp
        
        def traverse(i, j):
            print(i, j)
            n = len(dungeon)
            m = len(dungeon[0])
            if i == n or j == m:  # boundary
                return 200 * 200 * 1000
            if dp[i][j] != 200 * 200 * 1000:  # memoization
                return dp[i][j]
            if i == n - 1 and j == m - 1:
                return max(1 - dungeon[i][j], 1)
            down = traverse(i + 1, j)
            right = traverse(i, j + 1)
            required = min(down, right) - dungeon[i][j]
            dp[i][j] = max(1, required)
            return dp[i][j]

        dp = [[200 * 200 * 1000] * len(dungeon[0]) for _ in range(len(dungeon))]
        return traverse(0, 0)

    def calculateMinimumHP(self, dungeon: List[List[int]]) -> int:  # bottom-up dp: O(m * n) time
        # important clue is that we can only move right or down: this allows for dp.
        # we store the minimum health to reach each position in the arrays: 1 - mat[i][j]
        m, n = len(dungeon), len(dungeon[0])
        dp = [[200 * 200 * 1000] * (n + 1) for _ in range(m + 1)]
        dp[m][n - 1] = 1
        dp[m - 1][n] = 1
        # [[inf, inf, inf, inf]]
        # [[inf, inf, inf, inf]]
        # [[inf, inf, exit, 1 ]]
        # [[inf, inf,  1 , inf]]
        for i in range(m - 1, -1, -1):
            for j in range(n - 1, -1, -1):
                dp[i][j] = max(min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j], 1)
        return dp[0][0]


def test():
    arguments = [
        [[-2, -3, 3], [-5, -10, 1], [10, 30, -5]],
        [[0]],
        ]
    expectations = [7, 1]
    for dungeon, expected in zip(arguments, expectations):
        solution = Solution().calculateMinimumHP(dungeon)
        assert solution == expected
test()