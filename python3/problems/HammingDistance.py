### Source : https://leetcode.com/problems/hamming-distance/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-03

##################################################################################################### 
#
# The Hamming distance between two integers is the number of positions at which the corresponding 
# bits are different.
# 
# Given two integers x and y, calculate the Hamming distance.
# 
# Note:
# 0 &le; x, y < 231.
# 
# Example:
# 
# Input: x = 1, y = 4
# 
# Output: 2
# 
# Explanation:
# 1   (0 0 0 1)
# 4   (0 1 0 0)
#        &uarr;   &uarr;
# 
# The above arrows point to positions where the corresponding bits are different.
# 
#####################################################################################################


class Solution:
    def hammingDistance(self, x: int, y: int) -> int:  # O(32)
        return sum(map(int, bin(x ^ y)[2:]))

    def hammingDistance(self, x: int, y: int) -> int:
        return bin(x ^ y).count("1")

    def hammingDistance(self, x: int, y: int) -> int:  # no type conversion
        n = x ^ y
        ctr = 0
        while n:
            ctr += n & 1
            n >>= 1
        return ctr


def test():
    arguments = [(1, 4)]
    expectations = [2]
    for (x, y), expected in zip(arguments, expectations):
        solution = Solution().hammingDistance(x, y)
        assert solution == expected
