### Source : https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Serialization is the process of converting a data structure or object into a sequence of bits so 
# that it can be stored in a file or memory buffer, or transmitted across a network connection link 
# to be reconstructed later in the same or another computer environment.
# 
# Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your 
# serialization/deserialization algorithm should work. You just need to ensure that a binary tree can 
# be serialized to a string and this string can be deserialized to the original tree structure.
# 
# Clarification: The input/output format is the same as how LeetCode serializes a binary tree. You do 
# not necessarily need to follow this format, so please be creative and come up with different 
# approaches yourself.
# 
# Example 1:
# 
# Input: root = [1,2,3,null,null,4,5]
# Output: [1,2,3,null,null,4,5]
# 
# Example 2:
# 
# Input: root = []
# Output: []
# 
# Example 3:
# 
# Input: root = [1]
# Output: [1]
# 
# Example 4:
# 
# Input: root = [1,2]
# Output: [1,2]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 104].
# 	-1000 <= Node.val <= 1000
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Codec:

    def serialize(self, root):  # shortest
        return f'{root.val},{self.serialize(root.left)}{self.serialize(root.right)}' if root else ','

    def deserialize(self, data):
        def dfs():
            val = next(vals)
            if not val:
                return None
            node = TreeNode(int(val))
            node.left = dfs()
            node.right = dfs()
            return node

        vals = iter(data.split(','))
        return dfs()


# Your Codec object will be instantiated and called as such:
# ser = Codec()
# deser = Codec()
# ans = deser.deserialize(ser.serialize(root))


def test():
    from python3 import Codec as MyCodec
    serialized_trees = [
        "[1,2,3,null,null,4,5]",
        "[]",
        "[1]",
        "[1,2]",
        ]
    ser = Codec()
    deser = Codec()
    for serialized_tree in serialized_trees:
        root = MyCodec.deserialize(serialized_tree)
        ans = deser.deserialize(ser.serialize(root))
        assert ans is not root if ans else True  # None is None
        assert ans == root
