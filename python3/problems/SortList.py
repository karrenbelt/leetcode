### Source : https://leetcode.com/problems/sort-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given the head of a linked list, return the list after sorting it in ascending order.
# 
# Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?
# 
# Example 1:
# 
# Input: head = [4,2,1,3]
# Output: [1,2,3,4]
# 
# Example 2:
# 
# Input: head = [-1,5,3,4,0]
# Output: [-1,0,3,4,5]
# 
# Example 3:
# 
# Input: head = []
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 5 * 104].
# 	-105 <= Node.val <= 105
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def sortList(self, head: ListNode) -> ListNode:  # O(n log n) time and O(n) space
        if not head:
            return head

        nodes, node = [], head
        while node:
            nodes.append(node)
            node = node.next
        nodes.sort(key=lambda n: n.val)
        for i in range(len(nodes) - 1):
            nodes[i].next = nodes[i + 1]
        nodes[-1].next = None
        return nodes[0]

    def sortList(self, head: ListNode) -> ListNode:  # merge sort: O(n log n) time and O(log n) space

        def get_mid(node: ListNode):
            slow, fast = node, node
            while fast.next and fast.next.next:
                slow = slow.next
                fast = fast.next.next
            middle = slow.next
            slow.next = None
            return middle

        def merge(head1: ListNode, head2: ListNode):
            dummy = tail = ListNode(None)
            while head1 and head2:
                if head1.val < head2.val:
                    tail.next, tail, head1 = head1, head1, head1.next
                else:
                    tail.next, tail, head2 = head2, head2, head2.next
            tail.next = head1 or head2
            return dummy.next

        if not head or not head.next:
            return head
        mid = get_mid(head)
        left = self.sortList(head)
        right = self.sortList(mid)
        return merge(left, right)


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [4, 2, 1, 3],
        [-1, 5, 3, 4, 0],
        [],
        ]
    expectations = [
        [1, 2, 3, 4],
        [-1, 0, 3, 4, 5],
        [],
        ]
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().sortList(head)
        assert solution == SinglyLinkedList(expected).head
