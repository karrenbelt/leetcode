### Source : https://leetcode.com/problems/course-schedule-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-22

##################################################################################################### 
#
# There are a total of n courses you have to take labelled from 0 to n - 1.
# 
# Some courses may have prerequisites, for example, if prerequisites[i] = [ai, bi] this means you 
# must take the course bi before the course ai.
# 
# Given the total number of courses numCourses and a list of the prerequisite pairs, return the 
# ordering of courses you should take to finish all courses.
# 
# If there are many valid answers, return any of them. If it is impossible to finish all courses, 
# return an empty array.
# 
# Example 1:
# 
# Input: numCourses = 2, prerequisites = [[1,0]]
# Output: [0,1]
# Explanation: There are a total of 2 courses to take. To take course 1 you should have finished 
# course 0. So the correct course order is [0,1].
# 
# Example 2:
# 
# Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
# Output: [0,2,1,3]
# Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both 
# courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
# So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3].
# 
# Example 3:
# 
# Input: numCourses = 1, prerequisites = []
# Output: [0]
# 
# Constraints:
# 
# 	1 <= numCourses <= 2000
# 	0 <= prerequisites.length <= numCourses * (numCourses - 1)
# 	prerequisites[i].length == 2
# 	0 <= ai, bi < numCourses
# 	ai != bi
# 	All the pairs [ai, bi] are distinct.
#####################################################################################################

from typing import List


class Solution:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        graph = {}
        indegree = dict.fromkeys(range(numCourses), 0)
        for a, b in prerequisites:
            graph.setdefault(a, set()).add(b)
            indegree[b] += 1

        queue = [k for k, v in indegree.items() if v == 0]
        path = []
        seen = set()
        while queue:
            new_queue = []
            for node in queue:
                path.append(node)
                seen.add(node)
                for neighbor in graph.get(node, []):
                    indegree[neighbor] -= 1
                    if indegree[neighbor] == 0:
                        new_queue.append(neighbor)
            queue = new_queue
        return path[::-1] if len(seen) == numCourses else []


def test():
    arguments = [
        (2, [[1, 0]]),
        (4, [[1, 0], [2, 0], [3, 1], [3, 2]]),
        (1,  []),
        ]
    expectations = [
        [0, 1],
        [0, 2, 1, 3],
        [0],
        ]
    for (numCourses, prerequisits), expected in zip(arguments, expectations):
        solution = Solution().findOrder(numCourses, prerequisits)
        assert solution == expected
