### Source : https://leetcode.com/problems/recover-a-tree-from-preorder-traversal
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# We run a preorder depth first search on the root of a binary tree.
# 
# At each node in this traversal, we output D dashes (where D is the depth of this node), then we 
# output the value of this node.  (If the depth of a node is D, the depth of its immediate child is 
# D+1.  The depth of the root node is 0.)
# 
# If a node has only one child, that child is guaranteed to be the left child.
# 
# Given the output S of this traversal, recover the tree and return its root.
# 
# Example 1:
# 
# Input: "1-2--3--4-5--6--7"
# Output: [1,2,5,3,4,6,7]
# 
# Example 2:
# 
# Input: "1-2--3---4-5--6---7"
# Output: [1,2,5,3,null,6,null,4,null,7]
# 
# Example 3:
# 
# Input: "1-401--349---90--88"
# Output: [1,401,null,349,88,90]
# 
# Note:
# 
# 	The number of nodes in the original tree is between 1 and 1000.
# 	Each node will have a value between 1 and 109.
# 
#####################################################################################################

import collections
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def recoverFromPreorder(self, S: str) -> TreeNode:
        root = TreeNode('-')
        queue = collections.deque([root])
        depth = 0
        for s in S.split('-'):
            if s:
                for _ in range(len(queue) - depth - 1):
                    queue.pop()
                node = TreeNode(int(s))
                setattr(queue[-1], 'left' if queue[-1].left is None else 'right', node)
                queue.append(node)
                depth = 1
            else:
                depth += 1
        return root.left


def test():
    from python3 import Codec
    arguments = [
        "1-2--3--4-5--6--7",
        "1-2--3---4-5--6---7",
        "1-401--349---90--88",
    ]
    expectations = [
        "[1,2,5,3,4,6,7]",
        "[1,2,5,3,null,6,null,4,null,7]",
        "[1,401,null,349,88,90]",
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        solution = Solution().recoverFromPreorder(serialized_tree)
        assert solution == Codec.deserialize(expected)
