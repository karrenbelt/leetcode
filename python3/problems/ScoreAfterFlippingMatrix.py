### Source : https://leetcode.com/problems/score-after-flipping-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-08

##################################################################################################### 
#
# We have a two dimensional matrix grid where each value is 0 or 1.
# 
# A move consists of choosing any row or column, and toggling each value in that row or column: 
# changing all 0s to 1s, and all 1s to 0s.
# 
# After making any number of moves, every row of this matrix is interpreted as a binary number, and 
# the score of the matrix is the sum of these numbers.
# 
# Return the highest possible score.
# 
# Example 1:
# 
# Input: grid = [[0,0,1,1],[1,0,1,0],[1,1,0,0]]
# Output: 39
# Explanation:
# Toggled to [[1,1,1,1],[1,0,0,1],[1,1,1,1]].
# 0b1111 + 0b1001 + 0b1111 = 15 + 9 + 15 = 39
# 
# Note:
# 
# 	1 <= grid.length <= 20
# 	1 <= grid[0].length <= 20
# 	grid[i][j] is 0 or 1.
#####################################################################################################

from typing import List

class Solution:
    def matrixScore(self, grid: List[List[int]]) -> int:  # O(mn) time
        # maximize ones, prioritize left to right
        # 1. if left most is zero, we flip the row, since 1000 > 0111 in binary
        for i in range(len(grid)):
            if grid[i][0] == 0:
                grid[i] = [e ^ 1 for e in grid[i]]

        # 2. if column contains more zeroes than ones, we flip the column
        for j in range(len(grid[0])):
            if sum(grid[i][j] == 0 for i in range(len(grid))) > len(grid) // 2:
                for i in range(len(grid)):
                    grid[i][j] ^= 1

        # 3. convert to binary string, binary string to int, and sum
        return sum(int(''.join(map(str, row)), 2) for row in grid)

    def matrixScore(self, grid):
        grid[:] = [[e ^ grid[i][0] for e in grid[i]] for i in range(len(grid))]

        for j in range(len(grid[0])):
            if sum(grid[i][j] == 0 for i in range(len(grid))) > len(grid) // 2:
                for i in range(len(grid)):
                    grid[i][j] ^= 1

        # 3. convert to binary string, binary string to int, and sum
        return sum(int(''.join(map(str, row)), 2) for row in grid)


def test():
    arguments = [
        # flip third and fourth column, then flip top row
        [[0, 0, 1, 1],   # [[1, 1, 1, 1],
         [1, 0, 1, 0],   #  [1, 0, 0, 1],
         [1, 1, 0, 0]],  #  [1, 1, 1, 1]]
    ]
    expectations = [39]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().matrixScore(grid)
        assert solution == expected
test()