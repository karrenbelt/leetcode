### Source : https://leetcode.com/problems/interleaving-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-11

##################################################################################################### 
#
# Given strings s1, s2, and s3, find whether s3 is formed by an interleaving of s1 and s2.
# 
# An interleaving of two strings s and t is a configuration where they are divided into non-empty 
# substrings such that:
# 
# 	s = s1 + s2 + ... + sn
# 	t = t1 + t2 + ... + tm
# 	|n - m| <= 1
# 	The interleaving is s1 + t1 + s2 + t2 + s3 + t3 + ... or t1 + s1 + t2 + s2 + t3 + s3 + ...
# 
# Note: a + b is the concatenation of strings a and b.
# 
# Example 1:
# 
# Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
# Output: true
# 
# Example 2:
# 
# Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
# Output: false
# 
# Example 3:
# 
# Input: s1 = "", s2 = "", s3 = ""
# Output: true
# 
# Constraints:
# 
# 	0 <= s1.length, s2.length <= 100
# 	0 <= s3.length <= 200
# 	s1, s2, and s3 consist of lowercase English letters.
# 
# Follow up: Could you solve it using only O(s2.length) additional memory space?
#####################################################################################################


class Solution:
    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:  # dfs
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> bool:
            if i + j == len(s3):
                return True
            s1_match = i < len(s1) and s1[i] == s3[i + j]
            s2_match = j < len(s2) and s2[j] == s3[i + j]
            if s1_match and s2_match:
                return dfs(i + 1, j) or dfs(i, j + 1)
            elif s1_match:
                return dfs(i + 1, j)
            elif s2_match:
                return dfs(i, j + 1)
            return False

        return len(s1) + len(s2) == len(s3) and dfs(0, 0)

    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:  # dfs: TLE - not caching properly

        def dfs(i: int, j: int) -> bool:
            if valid[i][j] or i + j == len(s3):
                return True
            valid[i][j] = (i < len(s1) and s1[i] == s3[i + j] and dfs(i + 1, j) or
                           j < len(s2) and s2[j] == s3[i + j] and dfs(i, j + 1))
            return valid[i][j]

        valid = [[False] * (len(s2) + 1) for _ in range(len(s1) + 1)]
        return len(s1) + len(s2) == len(s3) and dfs(0, 0)

    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> bool:
            if i + j == len(s3):
                return True
            return (i < len(s1) and s1[i] == s3[i + j] and dfs(i + 1, j) or
                    j < len(s2) and s2[j] == s3[i + j] and dfs(i, j + 1))

        return len(s1) + len(s2) == len(s3) and dfs(0, 0)

    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:  # bfs
        if len(s3) != len(s1) + len(s2):
            return False
        indices = {(-1, -1)}
        for i in range(len(s3)):
            for (s1_i, s2_i) in indices.copy():
                indices.remove((s1_i, s2_i))
                if s1_i + 1 < len(s1) and s3[i] == s1[s1_i + 1]:
                    indices.add((s1_i + 1, s2_i))
                if s2_i + 1 < len(s2) and s3[i] == s2[s2_i + 1]:
                    indices.add((s1_i, s2_i + 1))
            if not indices:
                return False
        return True


def test():
    arguments = [
        ("aabcc", "dbbca", "aadbbcbcac"),
        ("aabcc", "dbbca", "aadbbbaccc"),
        ("", "", ""),
        ("abcd", "abcd", "abcabcdd"),
    ]
    expectations = [True, False, True, True]
    for (s1, s2, s3), expected in zip(arguments, expectations):
        solution = Solution().isInterleave(s1, s2, s3)
        assert solution == expected
