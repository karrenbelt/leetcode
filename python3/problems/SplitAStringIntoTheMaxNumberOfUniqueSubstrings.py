### Source : https://leetcode.com/problems/split-a-string-into-the-max-number-of-unique-substrings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-08

##################################################################################################### 
#
# Given a string s, return the maximum number of unique substrings that the given string can be split 
# into.
# 
# You can split string s into any list of non-empty substrings, where the concatenation of the 
# substrings forms the original string. However, you must split the substrings such that all of them 
# are unique.
# 
# A substring is a contiguous sequence of characters within a string.
# 
# Example 1:
# 
# Input: s = "ababccc"
# Output: 5
# Explanation: One way to split maximally is ['a', 'b', 'ab', 'c', 'cc']. Splitting like ['a', 'b', 
# 'a', 'b', 'c', 'cc'] is not valid as you have 'a' and 'b' multiple times.
# 
# Example 2:
# 
# Input: s = "aba"
# Output: 2
# Explanation: One way to split maximally is ['a', 'ba'].
# 
# Example 3:
# 
# Input: s = "aa"
# Output: 1
# Explanation: It is impossible to split the string any further.
# 
# Constraints:
# 
# 	1 <= s.length <= 16
# 
# 	s contains only lower case English letters.
# 
#####################################################################################################


class Solution:
    def maxUniqueSplit(self, s: str) -> int:  # O(n^3) time and O(n) space

        def backtracking(i: int, seen: set):
            if i == len(s):
                splits.append(seen)
            else:
                for j in range(i, len(s)):
                    if s[i:j + 1] not in seen:
                        backtracking(j + 1, seen | {s[i:j + 1]})

        splits = []
        backtracking(0, set())
        return max(map(len, splits))

    def maxUniqueSplit(self, s: str) -> int:

        def backtracking(i: int, seen) -> int:
            if i == len(s):
                return 0
            maximum = 0
            for j in range(i + 1, len(s) + 1):
                if (substring := s[i:j]) not in seen:
                    maximum = max(maximum, backtracking(j, seen | {substring}) + 1)
            return maximum

        return backtracking(0, set())


def test():
    arguments = ["ababccc", "aba", "aa", "abcdefghijklmnop"]
    expectations = [5, 2, 1]
    for s, expected in zip(arguments, expectations):
        solution = Solution().maxUniqueSplit(s)
        assert solution == expected, solution
