### Source : https://leetcode.com/problems/partition-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-11

##################################################################################################### 
#
# Given the head of a linked list and a value x, partition it such that all nodes less than x come 
# before nodes greater than or equal to x.
# 
# You should preserve the original relative order of the nodes in each of the two partitions.
# 
# Example 1:
# 
# Input: head = [1,4,3,2,5,2], x = 3
# Output: [1,2,2,4,3,5]
# 
# Example 2:
# 
# Input: head = [2,1], x = 2
# Output: [1,2]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 200].
# 	-100 <= Node.val <= 100
# 	-200 <= x <= 200
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def partition(self, head: ListNode, x: int) -> ListNode:  # O(n) time and O(1) space
        node = head
        lt_dummy = lt = ListNode(None)
        le_dummy = le = ListNode(None)
        while node:
            if node.val < x:
                lt.next = node
                lt = lt.next
            else:
                le.next = node
                le = le.next
            node = node.next
        le.next = None
        lt.next = le_dummy.next
        return lt_dummy.next


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([1, 4, 3, 2, 5, 2], 3),
        ([2, 1], 2),
        ]
    expectations = [
        [1, 2, 2, 4, 3, 5],
        [1, 2],
        ]
    for (nums, x), expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().partition(head, x)
        assert solution == SinglyLinkedList(expected).head
