### Source : https://leetcode.com/problems/unique-binary-search-trees-ii/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given an integer n, generate all structurally unique BST's (binary search trees) that store values 
# 1 ... n.
# 
# Example:
# 
# Input: 3
# Output:
# [
#   [1,null,3,2],
#   [3,2,null,1],
#   [3,1,null,null,2],
#   [2,1,3],
#   [1,null,2,null,3]
# ]
# Explanation:
# The above output corresponds to the 5 unique BST's shown below:
# 
#    1         3     3      2      1
#     \       /     /      / \      \
#      3     2     1      1   3      2
#     /     /       \                 \
#    2     1         2                 3
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    # n_solutions: (2n)! / ((n+1)! * n!)
    # (1, 2, 5, 14, 42, ...)
    def generateTrees(self, n: int) -> List[TreeNode]:
        if n < 1:
            return []

        def construct_trees(start, end):

            trees = []
            if start > end:
                trees.append(None)
                return trees

            for i in range(start, end + 1):
                left_subtree = construct_trees(start, i - 1)
                right_subtree = construct_trees(i + 1, end)
                for j in range(len(left_subtree)):
                    left = left_subtree[j]
                    for k in range(len(right_subtree)):
                        right = right_subtree[k]
                        node = TreeNode(i)  # making value i as root
                        node.left = left  # connect left subtree
                        node.right = right  # connect right subtree
                        trees.append(node)  # add this tree to list
            return trees

        return construct_trees(1, n)

    def generateTrees(self, n: int) -> List[TreeNode]:

        def build_trees(i: int, j: int):
            if i > j:
                return [None]
            if i == j:
                return [TreeNode(i)]
            trees = []
            for k in range(i, j + 1):
                left = build_trees(i, k - 1)
                right = build_trees(k + 1, j)
                for l in left:
                    for r in right:
                        trees.append(TreeNode(k, l, r))
            return trees

        return build_trees(1, n) if n else []


def test():
    from python3 import Codec
    numbers = [3, 1]
    expectations = [
        ["[1,null,2,null,3]", "[1,null,3,2]", "[2,1,3]", "[3,1,null,null,2]", "[3,2,null,1]"],
        ["[1]"],
        ]
    for n, expected in zip(numbers, expectations):
        solution = Solution().generateTrees(n)
        assert set(solution) == set(map(Codec.deserialize, expected))
