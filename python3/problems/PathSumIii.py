### Source : https://leetcode.com/problems/path-sum-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-17

##################################################################################################### 
#
# Given the root of a binary tree and an integer targetSum, return the number of paths where the sum 
# of the values along the path equals targetSum.
# 
# The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., 
# traveling only from parent nodes to child nodes).
# 
# Example 1:
# 
# Input: root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
# Output: 3
# Explanation: The paths that sum to 8 are shown.
# 
# Example 2:
# 
# Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
# Output: 3
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 1000].
# 	-109 <= Node.val <= 109
# 	-1000 <= targetSum <= 1000
#####################################################################################################

from typing import Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> int:
        from collections import Counter

        def dfs(node: TreeNode, running_sum: int):
            nonlocal ctr
            if node:
                running_sum += node.val
                ctr += prefix_sum.get(running_sum - targetSum, 0)
                prefix_sum[running_sum] = prefix_sum.get(running_sum, 0) + 1
                dfs(node.left, running_sum)
                dfs(node.right, running_sum)
                prefix_sum[running_sum] -= 1

        ctr, prefix_sum = 0, Counter({0: 1})
        return dfs(root, 0) or ctr

    def pathSum(self, root: TreeNode, targetSum: int) -> int:

        def backtrack(node, running_sum: int):
            if not node:
                return 0
            running_sum += node.val
            ctr = (targetSum == running_sum) + prefix_sum.get(running_sum - targetSum, 0)
            prefix_sum[running_sum] = prefix_sum.get(running_sum, 0) + 1
            ctr += backtrack(node.left, running_sum)
            ctr += backtrack(node.right, running_sum)
            prefix_sum[running_sum] -= 1
            return ctr

        prefix_sum = {}
        return backtrack(root, 0) if root else 0


def test():
    from python3 import Codec
    arguments = [
        ("[10,5,-3,3,2,null,11,3,-2,null,1]", 8),
        ("[5,4,8,11,null,13,4,7,2,null,null,5,1]", 22),
    ]
    expectations = [3, 3]
    for (serialized_tree, targetSum), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().pathSum(root, targetSum)
        assert solution == expected
