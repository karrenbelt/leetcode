### Source : https://leetcode.com/problems/find-three-consecutive-integers-that-sum-to-a-given-number/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# Given an integer num, return three consecutive integers (as a sorted array) that sum to num. If num 
# cannot be expressed as the sum of three consecutive integers, return an empty array.
# 
# Example 1:
# 
# Input: num = 33
# Output: [10,11,12]
# Explanation: 33 can be expressed as 10 + 11 + 12 = 33.
# 10, 11, 12 are 3 consecutive integers, so we return [10, 11, 12].
# 
# Example 2:
# 
# Input: num = 4
# Output: []
# Explanation: There is no way to express 4 as the sum of 3 consecutive integers.
# 
# Constraints:
# 
# 	0 <= num <= 1015
#####################################################################################################

from typing import List


class Solution:
    def sumOfThree(self, num: int) -> List[int]:  # O(1) time
        n, r = divmod(num, 3)
        return [n - 1, n, n + 1] if not r else []

    def sumOfThree(self, num: int) -> List[int]:  # O(1) time
        return [num//3 - 1, num//3, num//3 + 1] * (not num % 3)


def test():
    arguments = [33, 4, 30]
    expectations = [
        [10, 11, 12],
        [],
        [9, 10, 11],
        ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().sumOfThree(n)
        assert solution == expected

