### Source : https://leetcode.com/problems/binary-tree-preorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree, return the preorder traversal of its nodes' values.
# 
# Example:
# 
# Input: [1,null,2,3]
#    1
#     \
#      2
#     /
#    3
# 
# Output: [1,2,3]
# 
# Follow up: Recursive solution is trivial, could you do it iteratively?
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def preorderTraversal(self, root: TreeNode) -> List[int]:

        def traverse(node: TreeNode):
            if node:
                order.append(node.val)
                traverse(node.left)
                traverse(node.right)

        order = []
        traverse(root)
        return order

    def preorderTraversal(self, root: TreeNode) -> List[int]:
        order = []
        stack = [root]
        while stack:
            node = stack.pop()
            if node:
                order.append(node.val)
                stack.append(node.right)
                stack.append(node.left)
        return order

    def preorderTraversal(self, root: TreeNode) -> List[int]:  # Morris traversal: O(n) time and O(1) space
        order = []
        node = root
        while node:
            if not node.left:
                order.append(node.val)
                node = node.right
            else:
                pre = node.left
                while pre.right and pre.right is not node:
                    pre = pre.right
                if not pre.right:
                    order.append(node.val)
                    pre.right = node
                    node = node.left
                else:
                    pre.right = None
                    node = node.right
        return order


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,null,2,3]",
        "[]",
        "[1]",
    ]
    expectations = [
        [1, 2, 3],
        [],
        [1],
    ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().preorderTraversal(root)
        assert solution == expected
