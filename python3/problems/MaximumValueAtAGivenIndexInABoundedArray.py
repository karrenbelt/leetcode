### Source : https://leetcode.com/problems/maximum-value-at-a-given-index-in-a-bounded-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-01

##################################################################################################### 
#
# You are given three positive integers: n, index, and maxSum. You want to construct an array nums 
# (0-indexed) that satisfies the following conditions:
# 
# 	nums.length == n
# 	nums[i] is a positive integer where 0 <= i < n.
# 	abs(nums[i] - nums[i+1]) <= 1 where 0 <= i < n-1.
# 	The sum of all the elements of nums does not exceed maxSum.
# 	nums[index] is maximized.
# 
# Return nums[index] of the constructed array.
# 
# Note that abs(x) equals x if x >= 0, and -x otherwise.
# 
# Example 1:
# 
# Input: n = 4, index = 2,  maxSum = 6
# Output: 2
# Explanation: nums = [1,2,2,1] is one array that satisfies all the conditions.
# There are no arrays that satisfy all the conditions and have nums[2] == 3, so 2 is the maximum 
# nums[2].
# 
# Example 2:
# 
# Input: n = 6, index = 1,  maxSum = 10
# Output: 3
# 
# Constraints:
# 
# 	1 <= n <= maxSum <= 109
# 	0 <= index < n
#####################################################################################################


class Solution:
    def maxValue(self, n: int, index: int, maxSum: int) -> int:  # O(n) time
        # we need to imagine building a pyramid with the peak at index
        # first brick is 1 at index, n
        # next layer is 1 at neighboring indexes and index, etc, until array boundaries are met
        # we stop when either we cannot lay the complete next layer
        bricks = n
        left = right = index
        index_height = 1
        while bricks + (right - left + 1) <= maxSum:
            bricks += right - left + 1
            left = max(0, left - 1)
            right = min(n - 1, right + 1)
            index_height += 1
            # once whole range is covered, speed up (else TLE)
            if left == 0 and right == n - 1:
                steps = (maxSum - bricks) // n
                bricks += steps * n
                index_height += steps

        return index_height

    def maxValue(self, n: int, index: int, maxSum: int) -> int:  # O(n) time
        left = right = 0
        maxSum -= n
        index_height = 1
        while maxSum >= left + right + 1:
            maxSum -= left + right + 1
            index_height += 1
            left += 1 if index - left - 1 >= 0 else 0
            right += 1 if right + index + 1 < n else 0
            if left + right + 1 == n:
                index_height += maxSum // n
                break
        return index_height

    # def maxValue(self, n: int, index: int, maxSum: int) -> int:
    #     # https://leetcode.com/problems/maximum-value-at-a-given-index-in-a-bounded-array/discuss/1121605/
    #     # A-mechanical-way-to-implement-bug-free-binary-search-algorithms-python3
    #     def mysum(x, lgt):
    #         # lgt is the length of the part of array including x.
    #         if x > lgt:
    #             return lgt * (x + x - lgt + 1) // 2
    #         return x * (x + 1) // 2 + lgt - x
    #
    #     left, right = 1, maxSum
    #     while left < right:  # as always in inclusive boundary style
    #         mid = (left + right + 1) >> 1  # right_mid
    #         s = mysum(mid, index + 1) + mysum(mid - 1, n - index - 1)
    #         if s > maxSum:
    #             right = mid - 1  # mid-exclusive boundary
    #         else:
    #             left = mid  # mid-inclusive boundary is the first thing we need to consider.
    #
    #     return left


def test():
    arguments = [
        (4, 2, 6),
        (6, 1, 10),
        (3, 2, 18),
        (8, 7, 14),
        (4, 0, 4),
        (3, 0, 815094800),
        (9, 3, 16),
    ]
    expectations = [2, 3, 7, 4, 1, 271698267, 3]
    for (n, index, maxSum), expected in zip(arguments, expectations):
        solution = Solution().maxValue(n, index, maxSum)
        assert solution == expected
