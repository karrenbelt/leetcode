### Source : https://leetcode.com/problems/find-median-from-data-stream/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# The median is the middle value in an ordered integer list. If the size of the list is even, there 
# is no middle value and the median is the mean of the two middle values.
# 
# 	For example, for arr = [2,3,4], the median is 3.
# 	For example, for arr = [2,3], the median is (2 + 3) / 2 = 2.5.
# 
# Implement the MedianFinder class:
# 
# 	MedianFinder() initializes the MedianFinder object.
# 	void addNum(int num) adds the integer num from the data stream to the data structure.
# 	double findMedian() returns the median of all elements so far. Answers within 10-5 of the 
# actual answer will be accepted.
# 
# Example 1:
# 
# Input
# ["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
# [[], [1], [2], [], [3], []]
# Output
# [null, null, null, 1.5, null, 2.0]
# 
# Explanation
# MedianFinder medianFinder = new MedianFinder();
# medianFinder.addNum(1);    // arr = [1]
# medianFinder.addNum(2);    // arr = [1, 2]
# medianFinder.findMedian(); // return 1.5 (i.e., (1 + 2) / 2)
# medianFinder.addNum(3);    // arr[1, 2, 3]
# medianFinder.findMedian(); // return 2.0
# 
# Constraints:
# 
# 	-105 <= num <= 105
# 	There will be at least one element in the data structure before calling findMedian.
# 	At most 5 * 104 calls will be made to addNum and findMedian.
# 
# Follow up:
# 
# 	If all integer numbers from the stream are in the range [0, 100], how would you optimize 
# your solution?
# 	If 99% of all integer numbers from the stream are in the range [0, 100], how would you 
# optimize your solution?
#####################################################################################################


class MedianFinder:

    def __init__(self):
        self.nums = []

    def addNum(self, num: int) -> None:  # O(n log n)
        # bisect.insort is O(n) because of the insertion step.
        # Python's sort uses tim-sort which is fast when the list is partially sorted
        self.nums.append(num)
        self.nums.sort()

    def findMedian(self) -> float:  # O(1) time
        return (self.nums[len(self.nums) // 2] + self.nums[~len(self.nums) // 2]) / 2


class MedianFinder:
    def __init__(self):
        import heapq
        self.small = []  # the smaller half of the list, max heap (invert min-heap)
        self.large = []  # the larger half of the list, min heap
        self.push = heapq.heappush
        self.pushpop = heapq.heappushpop

    def addNum(self, num: int) -> None:  # O(log n) time
        if len(self.small) == len(self.large):
            self.push(self.large, -self.pushpop(self.small, -num))
        else:
            self.push(self.small, -self.pushpop(self.large, num))

    def findMedian(self) -> float:  # O(1) time
        return float(self.large[0] - self.small[0]) / 2 if len(self.small) == len(self.large) else float(self.large[0])


# TODO: we can also use a BST, however writing the BST insert method is complicated

# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()


def test():
    operations = ["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
    arguments = [[], [1], [2], [], [3], []]
    expectations = [None, None, None, 1.5, None, 2.0]
    obj = MedianFinder(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
