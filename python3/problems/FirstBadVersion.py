### Source : https://leetcode.com/problems/first-bad-version/
### Author : M.A.P. Karrenbelt
### Date   : 2020-05-01

##################################################################################################### 
#
# You are a product manager and currently leading a team to develop a new product. Unfortunately, the 
# latest version of your product fails the quality check. Since each version is developed based on 
# the previous version, all the versions after a bad version are also bad.
# 
# Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes 
# all the following ones to be bad.
# 
# You are given an API bool isBadVersion(version) which will return whether version is bad. Implement 
# a function to find the first bad version. You should minimize the number of calls to the API.
# 
# Example:
# 
# Given n = 5, and version = 4 is the first bad version.
# 
# call isBadVersion(3) -> false
# call isBadVersion(5) -> true
# call isBadVersion(4) -> true
# 
# Then 4 is the first bad version. 
# 
#####################################################################################################

# The isBadVersion API is already defined for you.
# @param version, an integer
# @return a bool


class Solution:
    def firstBadVersion(self, n: int) -> int:  # O(log n) time
        left, right = 0, n - 1
        while left <= right:
            mid = (left + right) // 2
            if isBadVersion(mid):
                right = mid - 1
            else:
                left = mid + 1
        return left

    def firstBadVersion(self, n: int) -> int:  # better and more natural solution
        left, right = 1, n
        while left < right:
            mid = left + right >> 1
            if not isBadVersion(mid):
                left = mid + 1
            else:
                right = mid
        return left


def test():
    numbers = [5, 1]
    expectations = [4, 1]
    for n, expected in zip(numbers, expectations):
        global isBadVersion
        isBadVersion = lambda x: x == expected
        solution = Solution().firstBadVersion(n)
        assert solution == expected, (solution, expected)
