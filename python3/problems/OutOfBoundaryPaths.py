### Source : https://leetcode.com/problems/out-of-boundary-paths/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-24

##################################################################################################### 
#
# There is an m x n grid with a ball. The ball is initially at the position [startRow, startColumn]. 
# You are allowed to move the ball to one of the four adjacent four cells in the grid (possibly out 
# of the grid crossing the grid boundary). You can apply at most maxove moves to the ball.
# 
# Given the five integers m, n, maxove, startRow, startColumn, return the number of paths to move 
# the ball out of the grid boundary. Since the answer can be very large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: m = 2, n = 2, maxove = 2, startRow = 0, startColumn = 0
# Output: 6
# 
# Example 2:
# 
# Input: m = 1, n = 3, maxove = 3, startRow = 0, startColumn = 1
# Output: 12
# 
# Constraints:
# 
# 	1 <= m, n <= 50
# 	0 <= maxove <= 50
# 	0 <= startRow <= m
# 	0 <= startColumn <= n
#####################################################################################################


class Solution:
    def findPaths(self, m: int, n: int, maxMove: int, startRow: int, startColumn: int) -> int:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def travel(i: int, j: int, moves: int) -> int:
            if moves:
                ctr = 0
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < m and 0 <= y < n:
                        ctr += travel(x, y, moves - 1)
                    else:
                        ctr += 1
                return ctr
            return 0

        return travel(startRow, startColumn, maxMove) % (10**9 + 7)

    def findPaths(self, m: int, n: int, maxMove: int, startRow: int, startColumn: int) -> int:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def travel(i: int, j: int, moves: int) -> int:
            return sum(travel(x, y, moves - 1) if 0 <= x < m and 0 <= y < n else 1
                       for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]) if moves else 0

        return travel(startRow, startColumn, maxMove) % (10**9 + 7)

    # def findPaths(self, m: int, n: int, maxMove: int, startRow: int, startColumn: int) -> int:  # TLE
    #     from collections import deque
    #     queue = deque([(startRow, startColumn, 0)])
    #     ctr = 0
    #     while queue:
    #         x, y, step = queue.popleft()
    #         if step > maxMove:
    #             break
    #         if 0 <= x < m and 0 <= y < n:
    #             queue.append((x + 1, y, step + 1))
    #             queue.append((x - 1, y, step + 1))
    #             queue.append((x, y + 1, step + 1))
    #             queue.append((x, y - 1, step + 1))
    #         else:
    #             ctr += 1
    #     return ctr % (10**9 + 7)

    def findPaths(self, m: int, n: int, maxMove: int, startRow: int, startColumn: int) -> int:
        dp = [[0] * n for _ in range(m)]
        dp[startRow][startColumn] = 1
        ctr = 0
        for time in range(maxMove):
            cur = dp
            dp = [[0] * n for _ in range(m)]
            for i in range(len(cur)):
                for j in range(len(cur[i])):
                    for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                        if 0 <= x < m and 0 <= y < n:
                            dp[x][y] += cur[i][j]
                        else:
                            ctr += cur[i][j]
        return ctr % (10**9 + 7)


def test():
    arguments = [
        (2, 2, 2, 0, 0),
        (1, 3, 3, 0, 1),
        (8, 7, 16, 1, 5),
    ]
    expectations = [6, 12, 102984580]
    for (m, n, maxMove, startRow, startColumn), expected in zip(arguments, expectations):
        solution = Solution().findPaths(m, n, maxMove, startRow, startColumn)
        assert solution == expected, (solution, expected)
