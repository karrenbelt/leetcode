### Source : https://leetcode.com/problems/stone-game-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-11

##################################################################################################### 
#
# Alice and Bob continue their games with piles of stones.  There are a number of piles arranged in a 
# row, and each pile has a positive integer number of stones piles[i].  The objective of the game is 
# to end with the most stones. 
# 
# Alice and Bob take turns, with Alice starting first.  Initially,  = 1.
# 
# On each player's turn, that player can take all the stones in the first X remaining piles, where 1 
# <= X <= 2.  Then, we set  = max(, X).
# 
# The game continues until all the stones have been taken.
# 
# Assuming Alice and Bob play optimally, return the maximum number of stones Alice can get.
# 
# Example 1:
# 
# Input: piles = [2,7,9,4,4]
# Output: 10
# Explanation:  If Alice takes one pile at the beginning, Bob takes two piles, then Alice takes 2 
# piles again. Alice can get 2 + 4 + 4 = 10 piles in total. If Alice takes two piles at the 
# beginning, then Bob can take all three piles left. In this case, Alice get 2 + 7 = 9 piles in 
# total. So we return 10 since it's larger. 
# 
# Example 2:
# 
# Input: piles = [1,2,3,4,5,100]
# Output: 104
# 
# Constraints:
# 
# 	1 <= piles.length <= 100
# 	1 <= piles[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def stoneGameII(self, piles: List[int]) -> int:  # O(n^3) time
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, m: int):
            maximum = 0
            total = sum(piles[i:])
            for j in range(1, min(2 * m + 1, len(piles) - i + 1)):
                optimum = dfs(i + j, max(m, j))
                maximum = max(maximum, total - optimum)
            return maximum

        return dfs(0, 1)


def test():
    arguments = [
        [2, 7, 9, 4, 4],
        [1, 2, 3, 4, 5, 100],
    ]
    expectations = [10, 104]
    for piles, expected in zip(arguments, expectations):
        solution = Solution().stoneGameII(piles)
        assert solution == expected, (piles)
test()