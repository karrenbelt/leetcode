### Source : https://leetcode.com/problems/surrounded-regions/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an m x n matrix board containing 'X' and 'O', capture all regions surrounded by 'X'.
# 
# A region is captured by flipping all 'O's into 'X's in that surrounded region.
# 
# Example 1:
# 
# Input: board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
# Output: [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
# Explanation: Surrounded regions should not be on the border, which means that any 'O' on the border 
# of the board are not flipped to 'X'. Any 'O' that is not on the border and it is not connected to 
# an 'O' on the border will be flipped to 'X'. Two cells are connected if they are adjacent cells 
# connected horizontally or vertically.
# 
# Example 2:
# 
# Input: board = [["X"]]
# Output: [["X"]]
# 
# Constraints:
# 
# 	m == board.length
# 	n == board[i].length
# 	1 <= m, n <= 200
# 	board[i][j] is 'X' or 'O'.
#####################################################################################################

from typing import List


class Solution:
    def solve(self, board: List[List[str]]) -> None:  # O(mn) time and O(mn) space
        """
        Do not return anything, modify board in-place instead.
        """
        # first we perform a dfs to find all those at or connected to the boarder '0's
        # the rest we set to 'X'
        def dfs(x: int, y: int):
            if 0 <= x < len(board) and 0 <= y < len(board[0]) and board[x][y] == 'O' and (x, y) not in dont_touch:
                dont_touch.add((x, y))
                any(dfs(nx, ny) for nx, ny in [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)])

        dont_touch = set()
        for i in range(len(board)):
            dfs(i, 0) or dfs(i, len(board[0]) - 1)
        for j in range(len(board[0])):
            dfs(0, j) or dfs(len(board) - 1, j)

        for i, j in ((i, j) for i in range(len(board)) for j in range(len(board[0])) if (i, j) not in dont_touch):
            board[i][j] = 'X'


def test():
    arguments = [
        [["X", "X", "X", "X"],
         ["X", "O", "O", "X"],
         ["X", "X", "O", "X"],
         ["X", "O", "X", "X"]],

        [["X"]],

        [["X", "O", "X", "O", "X", "O"],
         ["O", "X", "O", "X", "O", "X"],
         ["X", "O", "X", "O", "X", "O"],
         ["O", "X", "O", "X", "O", "X"]],
        ]
    expectations = [
        [["X", "X", "X", "X"],
         ["X", "X", "X", "X"],
         ["X", "X", "X", "X"],
         ["X", "O", "X", "X"]],

        [["X"]],

        [["X", "O", "X", "O", "X", "O"],
         ["O", "X", "X", "X", "X", "X"],
         ["X", "X", "X", "X", "X", "O"],
         ["O", "X", "O", "X", "O", "X"]],
        ]
    for board, expected in zip(arguments, expectations):
        Solution().solve(board)
        assert board == expected
