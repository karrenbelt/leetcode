### Source : https://leetcode.com/problems/maximum-product-of-two-elements-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-16

##################################################################################################### 
#
# Given the array of integers nums, you will choose two different indices i and j of that array. 
# Return the maximum value of (nums[i]-1)*(nums[j]-1).
# 
# Example 1:
# 
# Input: nums = [3,4,5,2]
# Output: 12 
# Explanation: If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum 
# value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12. 
# 
# Example 2:
# 
# Input: nums = [1,5,4,5]
# Output: 16
# Explanation: Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of 
# (5-1)*(5-1) = 16.
# 
# Example 3:
# 
# Input: nums = [3,7]
# Output: 12
# 
# Constraints:
# 
# 	2 <= nums.length <= 500
# 	1 <= nums[i] <= 103
#####################################################################################################

from typing import List


class Solution:
    def maxProduct(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        return nums.sort() or (nums.pop() - 1) * (nums.pop() - 1)

    def maxProduct(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        a = b = 0
        for n in nums:
            a, b = max(a, n), max(b, min(a, n))
        return (a - 1) * (b - 1)


def test():
    arguments = [
        [3, 4, 5, 2],
        [1, 5, 4, 5],
        [3, 7],
    ]
    expectations = [12, 16, 12]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maxProduct(nums)
        assert solution == expected
