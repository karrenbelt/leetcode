### Source : https://leetcode.com/problems/merge-two-2d-arrays-by-summing-values/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-03

##################################################################################################### 
#
# You are given two 2D integer arrays nums1 and nums2.
# 
# 	nums1[i] = [idi, vali] indicate that the number with the id idi has a value equal to vali.
# 	nums2[i] = [idi, vali] indicate that the number with the id idi has a value equal to vali.
# 
# Each array contains unique ids and is sorted in ascending order by id.
# 
# erge the two arrays into one array that is sorted in ascending order by id, respecting the 
# following conditions:
# 
# 	Only ids that appear in at least one of the two arrays should be included in the resulting 
# array.
# 	Each id should be included only once and its value should be the sum of the values of this 
# id in the two arrays. If the id does not exist in one of the two arrays then its value in that 
# array is considered to be 0.
# 
# Return the resulting array. The returned array must be sorted in ascending order by id.
# 
# Example 1:
# 
# Input: nums1 = [[1,2],[2,3],[4,5]], nums2 = [[1,4],[3,2],[4,1]]
# Output: [[1,6],[2,3],[3,2],[4,6]]
# Explanation: The resulting array contains the following:
# - id = 1, the value of this id is 2 + 4 = 6.
# - id = 2, the value of this id is 3.
# - id = 3, the value of this id is 2.
# - id = 4, the value of this id is 5 + 1 = 6.
# 
# Example 2:
# 
# Input: nums1 = [[2,4],[3,6],[5,5]], nums2 = [[1,3],[4,3]]
# Output: [[1,3],[2,4],[3,6],[4,3],[5,5]]
# Explanation: There are no common ids, so we just include each id with its value in the resulting 
# list.
# 
# Constraints:
# 
# 	1 <= nums1.length, nums2.length <= 200
# 	nums1[i].length == nums2[j].length == 2
# 	1 <= idi, vali <= 1000
# 	Both arrays contain unique ids.
# 	Both arrays are in strictly ascending order by id.
#####################################################################################################

from typing import List


class Solution:
    def mergeArrays(self, nums1: List[List[int]], nums2: List[List[int]]) -> List[List[int]]:
        from collections import Counter
        counts = Counter(dict(nums1)) + Counter(dict(nums2))
        return sorted(map(list, counts.items()))

    def mergeArrays(self, nums1: List[List[int]], nums2: List[List[int]]) -> List[List[int]]:
        merged = []
        i = j = 0
        while i < len(nums1) and j < len(nums2):
            if nums1[i][0] == nums2[j][0]:
                total = nums1[i][1] + nums2[j][1]
                merged.append([nums1[i][0], total])
                i, j = i + 1, j + 1
            elif nums1[i][0] < nums2[j][0]:
                merged.append(nums1[i])
                i += 1
            else:
                merged.append(nums2[j])
                j += 1
        merged.extend(nums1[i:] + nums2[j:])
        return merged

    def mergeArrays(self, nums1: List[List[int]], nums2: List[List[int]]) -> List[List[int]]:
        from enum import Enum, auto

        class Ordering(Enum):
            LESS = auto()
            EQUAL = auto()
            GREATER = auto()

        def compare(a, b) -> Ordering:
            if a < b:
                return Ordering.LESS
            elif a == b:
                return Ordering.EQUAL
            elif a > b:
                return Ordering.GREATER

        merged = []
        while nums1 and nums2:
            match compare(nums1[-1][0], nums2[-1][0]):
                case Ordering.EQUAL:
                    total = [nums1[-1][0], nums1.pop()[1] + nums2.pop()[1]]
                    merged.append(total)
                case Ordering.GREATER:
                    merged.append(nums1.pop())
                case Ordering.LESS:
                    merged.append(nums2.pop())

        merged.extend(reversed(nums1 or nums2))
        merged.reverse()
        return merged


def test():
    arguments = [
        ([[1, 2], [2, 3], [4, 5]], [[1, 4], [3, 2], [4, 1]]),
        ([[2, 4], [3, 6], [5, 5]], [[1, 3], [4, 3]]),
        (
            [[148, 597], [165, 623], [306, 359], [349, 566], [403, 646],
             [420, 381], [566, 543], [730, 209], [757, 875], [788, 208],
             [932, 695]],
            [[74, 669], [87, 399], [89, 165], [99, 749], [122, 401], [138, 16],
             [144, 714], [148, 206], [177, 948], [211, 653], [285, 775],
             [309, 289], [349, 396], [386, 831], [403, 318], [405, 119],
             [420, 153], [468, 433], [504, 101], [566, 128], [603, 688],
             [618, 628], [622, 586], [641, 46], [653, 922], [672, 772],
             [691, 823], [693, 900], [756, 878], [757, 952], [770, 795],
             [806, 118], [813, 88], [919, 501], [935, 253], [982, 385]],
        ),
    ]
    expectations = [
        [[1, 6], [2, 3], [3, 2], [4, 6]],
        [[1, 3], [2, 4], [3, 6], [4, 3], [5, 5]],
        [[74, 669], [87, 399], [89, 165], [99, 749], [122, 401], [138, 16],
         [144, 714], [148, 803], [165, 623], [177, 948], [211, 653], [285, 775],
         [306, 359], [309, 289], [349, 962], [386, 831], [403, 964], [405, 119],
         [420, 534], [468, 433], [504, 101], [566, 671], [603, 688], [618, 628],
         [622, 586], [641, 46], [653, 922], [672, 772], [691, 823], [693, 900],
         [730, 209], [756, 878], [757, 1827], [770, 795], [788, 208],
         [806, 118], [813, 88], [919, 501], [932, 695], [935, 253], [982, 385]],
    ]
    for (nums1, nums2), expected in zip(arguments, expectations):
        solution = Solution().mergeArrays(nums1, nums2)
        assert solution == expected
