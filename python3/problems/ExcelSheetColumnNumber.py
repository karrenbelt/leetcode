### Source : https://leetcode.com/problems/excel-sheet-column-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given a string columnTitle that represents the column title as appear in an Excel sheet, return its 
# corresponding column number.
# 
# For example:
# 
# A -> 1
# B -> 2
# C -> 3
# ...
# Z -> 26
# AA -> 27
# AB -> 28 
# ...
# 
# Example 1:
# 
# Input: columnTitle = "A"
# Output: 1
# 
# Example 2:
# 
# Input: columnTitle = "AB"
# Output: 28
# 
# Example 3:
# 
# Input: columnTitle = "ZY"
# Output: 701
# 
# Example 4:
# 
# Input: columnTitle = "FXSHRXW"
# Output: 2147483647
# 
# Constraints:
# 
# 	1 <= columnTitle.length <= 7
# 	columnTitle consists only of uppercase English letters.
# 	columnTitle is in the range ["A", "FXSHRXW"].
#####################################################################################################


class Solution:
    def titleToNumber(self, columnTitle: str) -> int:  # O(n) time and O(26) space
        mapping = {chr(i + 65): i + 1 for i in range(26)}
        n = 0
        for letter in columnTitle:
            n = n * 26 + mapping[letter]
        return n

    def titleToNumber(self, columnTitle: str) -> int:  # O(n) time and O(1) space
        if not columnTitle:
            return 0
        return 26 ** (len(columnTitle) - 1) * (ord(columnTitle[0]) - 64) + self.titleToNumber(columnTitle[1:])


def test():
    arguments = ["A", "AB", "ZY", "FXSHRXW"]
    expectations = [1, 28, 701, 2147483647]
    for columnTitle, expected in zip(arguments, expectations):
        solution = Solution().titleToNumber(columnTitle)
        assert solution == expected
test()