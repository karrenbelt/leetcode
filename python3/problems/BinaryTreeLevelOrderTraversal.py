### Source : https://leetcode.com/problems/binary-tree-level-order-traversal/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to 
# right, level by level).
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
# return its level order traversal as:
# 
# [
#   [3],
#   [9,20],
#   [15,7]
# ]
# 
#####################################################################################################
##################################################################################################### 
#
# Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to 
# right, level by level).
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
# return its level order traversal as:
# 
# [
#   [3],
#   [9,20],
#   [15,7]
# ]
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        from collections import deque

        levels = []
        if not root:
            return levels

        level = 0
        queue = deque([root, ])
        while queue:
            levels.append([])
            level_length = len(queue)
            for i in range(level_length):
                node = queue.popleft()
                levels[level].append(node.val)
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
            level += 1
        return levels

    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        from collections import deque

        if not root:
            return []

        levels = []
        queue = deque([root])
        while queue:
            level = []
            for i in range(len(queue)):
                node = queue.popleft()
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
                level.append(node.val)
            levels.append(level)
        return levels


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3, 9, 20, null, null, 15, 7]",
        "[1]",
        "[]",
        ]
    expectations = [
        [[3], [9, 20], [15, 7]],
        [[1]],
        [],
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().levelOrder(root)
        assert solution == expected

