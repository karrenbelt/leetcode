### Source : https://leetcode.com/problems/binary-search/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a sorted (in ascending order) integer array nums of n elements and a target value, write a 
# function to search target in nums. If target exists, then return its index, otherwise return -1.
# 
# Example 1:
# 
# Input: nums = [-1,0,3,5,9,12], target = 9
# Output: 4
# Explanation: 9 exists in nums and its index is 4
# 
# Example 2:
# 
# Input: nums = [-1,0,3,5,9,12], target = 2
# Output: -1
# Explanation: 2 does not exist in nums so return -1
# 
# Note:
# 
# 	You may assume that all elements in nums are unique.
# 	n will be in the range [1, 10000].
# 	The value of each element in nums will be in the range [-9999, 9999].
# 
#####################################################################################################
from typing import List


class Solution:
    # considerations:
    # 1. Include ALL possible answers when initialize left and high
    # 2. Don't overflow the mid calculation
    # 3. Shrink boundary using a logic that will exclude mid
    # 4. Avoid infinity loop by picking the correct mid and shrinking logic
    # 5. Always think of the case when there are 2 elements left
    #
    # questions:
    # - Do I use left or right mid?
    # - Do I use < or <= , > or >=?
    # - How much do I shrink the boundary? is it mid or mid - 1 or even mid + 1?
    def search(self, nums: List[int], target: int) -> int:  # O(log n) time and O(1) space
        left, right = 0, len(nums) - 1
        while left <= right:
            mid = left + (right - left) // 2
            if nums[mid] == target:
                return mid
            elif nums[mid] < target:
                left = mid + 1
            else:
                right = mid - 1
        return -1

    def search(self, nums: List[int], target: int) -> int:  # O(log n) time and O(1) space
        # this is an ideal template because of simplicity
        left, right = 0, len(nums) - 1   #
        while left < right:              # we will only end up with 1 element, since left == right
            mid = left + right >> 1      # no overflow and bit shift is fast
            if nums[mid] < target:       #
                left = mid + 1           # mid is excluded
            else:                        #
                right = mid              # mid is included
        return left if nums[left] == target else -1

    def search(self, nums: List[int], target: int) -> int:
        from bisect import bisect_left
        i = bisect_left(nums, target)
        return i if i < len(nums) and nums[i] == target else -1

    def search(self, nums: List[int], target: int) -> int:
        from bisect import bisect_right
        i = bisect_right(nums, target)
        return i - 1 if nums[i - 1] == target else -1  # == i * (nums[i - 1] == target) - 1


def test():
    arguments = [
        ([-1, 0, 3, 5, 9, 12], 9),
        ([-1, 0, 3, 5, 9, 12], 2),
        ([-1, 0, 3, 5, 9, 12], -1),
        ([-1, 0, 3, 5, 9, 12], 12),
        ([-1, 0, 3, 5, 9, 12], -2),
        ([-1, 0, 3, 5, 9, 12], 13),
        ]
    expectations = [4, -1, 0, 5, -1, -1]
    for (nums, target), expected in zip(arguments, expectations):
        solution = Solution().search(nums, target)
        assert solution == expected, (solution, expected)
