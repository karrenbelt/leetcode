### Source : https://leetcode.com/problems/find-k-pairs-with-smallest-sums/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-18

##################################################################################################### 
#
# You are given two integer arrays nums1 and nums2 sorted in ascending order and an integer k.
# 
# Define a pair (u, v) which consists of one element from the first array and one element from the 
# second array.
# 
# Return the k pairs (u1, v1), (u2, v2), ..., (uk, vk) with the smallest sums.
# 
# Example 1:
# 
# Input: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
# Output: [[1,2],[1,4],[1,6]]
# Explanation: The first 3 pairs are returned from the sequence: 
# [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]
# 
# Example 2:
# 
# Input: nums1 = [1,1,2], nums2 = [1,2,3], k = 2
# Output: [[1,1],[1,1]]
# Explanation: The first 2 pairs are returned from the sequence: 
# [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]
# 
# Example 3:
# 
# Input: nums1 = [1,2], nums2 = [3], k = 3
# Output: [[1,3],[2,3]]
# Explanation: All possible pairs are returned from the sequence: [1,3],[2,3]
# 
# Constraints:
# 
# 	1 <= nums1.length, nums2.length <= 104
# 	-109 <= nums1[i], nums2[i] <= 109
# 	nums1 and nums2 both are sorted in ascending order.
# 	1 <= k <= 1000
#####################################################################################################

from typing import List


class Solution:
    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int) -> List[List[int]]:
        import heapq
        import itertools
        return list(map(list, heapq.nsmallest(k, itertools.product(nums1, nums2), key=sum)))

    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int) -> List[List[int]]:
        import heapq
        smallest_pairs, seen = [], set()
        queue = [(nums1[0] + nums2[0], (0, 0))]
        while len(smallest_pairs) < k and queue:
            _, (i, j) = heapq.heappop(queue)
            smallest_pairs.append([nums1[i], nums2[j]])
            if j + 1 < len(nums2) and (i, j + 1) not in seen:
                heapq.heappush(queue, (nums1[i] + nums2[j + 1], (i, j + 1)))
                seen.add((i, j + 1))
            if i + 1 < len(nums1) and (i + 1, j) not in seen:
                heapq.heappush(queue, (nums1[i + 1] + nums2[j], (i + 1, j)))
                seen.add((i + 1, j))
        return smallest_pairs


def test():
    arguments = [
        ([1, 7, 11], [2, 4, 6], 3),
        ([1, 1, 2], [1, 2, 3], 2),
        ([1, 2], [3], 3),
        ]
    expectations = [
        [[1, 2], [1, 4], [1, 6]],
        [[1, 1], [1, 1]],
        [[1, 3], [2, 3]],
    ]
    for (nums1, nums2, k), expected in zip(arguments, expectations):
        solution = Solution().kSmallestPairs(nums1, nums2, k)
        assert solution == expected, solution
test()