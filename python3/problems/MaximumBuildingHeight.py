### Source : https://leetcode.com/problems/maximum-building-height/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-27

##################################################################################################### 
#
# You want to build n new buildings in a city. The new buildings will be built in a line and are 
# labeled from 1 to n.
# 
# However, there are city restrictions on the heights of the new buildings:
# 
# 	The height of each building must be a non-negative integer.
# 	The height of the first building must be 0.
# 	The height difference between any two adjacent buildings cannot exceed 1.
# 
# Additionally, there are city restrictions on the maximum height of specific buildings. These 
# restrictions are given as a 2D integer array restrictions where restrictions[i] = [idi, maxHeighti] 
# indicates that building idi must have a height less than or equal to maxHeighti.
# 
# It is guaranteed that each building will appear at most once in restrictions, and building 1 will 
# not be in restrictions.
# 
# Return the maximum possible height of the tallest building.
# 
# Example 1:
# 
# Input: n = 5, restrictions = [[2,1],[4,1]]
# Output: 2
# Explanation: The green area in the image indicates the maximum allowed height for each building.
# We can build the buildings with heights [0,1,2,1,2], and the tallest building has a height of 2.
# 
# Example 2:
# 
# Input: n = 6, restrictions = []
# Output: 5
# Explanation: The green area in the image indicates the maximum allowed height for each building.
# We can build the buildings with heights [0,1,2,3,4,5], and the tallest building has a height of 5.
# 
# Example 3:
# 
# Input: n = 10, restrictions = [[5,3],[2,5],[7,4],[10,3]]
# Output: 5
# Explanation: The green area in the image indicates the maximum allowed height for each building.
# We can build the buildings with heights [0,1,2,3,3,4,4,5,4,3], and the tallest building has a 
# height of 5.
# 
# Constraints:
# 
# 	2 <= n <= 109
# 	0 <= restrictions.length <= min(n - 1, 105)
# 	2 <= idi <= n
# 	idi is unique.
# 	0 <= maxHeighti <= 109
#####################################################################################################

from typing import List


class Solution:
    def maxBuilding(self, n: int, restrictions: List[List[int]]) -> int:  # O(n log n) time and O(n) space
        # there is at most one effective restriction per location
        # we pass the restrictions array twice to determine these
        # the we calculate the maximum height between subsequent restrictions
        limits = [[1, 0]] + sorted(restrictions) + [[n, n - 1]]
        for i in range(1, len(limits)):  # update left to right
            limits[i][1] = min(limits[i][1], limits[i - 1][1] + limits[i][0] - limits[i - 1][0])
        for i in range(len(limits) - 2, -1, -1):  # right to left
            limits[i][1] = min(limits[i][1], limits[i + 1][1] + limits[i + 1][0] - limits[i][0])
        maximum = 0
        for i in range(1, len(limits)):  # find the maximum height
            (left, h1), (right, h2) = limits[i - 1], limits[i]
            maximum = max(maximum, max(h1, h2), (right - left + h1 + h2) // 2)
        return maximum


def test():
    arguments = [
        (5, [[2, 1], [4, 1]]),
        (6, []),
        (10, [[5, 3], [2, 5], [7, 4], [10, 3]]),
    ]
    expectations = [2, 5, 5]
    for (n, restrictions), expected in zip(arguments, expectations):
        solution = Solution().maxBuilding(n, restrictions)
        assert solution == expected
