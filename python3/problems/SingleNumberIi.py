### Source : https://leetcode.com/problems/single-number-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an integer array nums where every element appears three times except for one, which appears 
# exactly once. Find the single element and return it.
# 
# Example 1:
# Input: nums = [2,2,3,2]
# Output: 3
# Example 2:
# Input: nums = [0,1,0,1,0,1,99]
# Output: 99
# 
# Constraints:
# 
# 	1 <= nums.length <= 3 * 104
# 	-231 <= nums[i] <= 231 - 1
# 	Each element in nums appears exactly three times except for one element which appears once.
# 
# Follow up: Your algorithm should have a linear runtime complexity. Could you implement it without 
# using extra memory?
#####################################################################################################

from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        counts = {}
        for n in nums:
            counts[n] = counts.get(n, 0) + 1
        return next(iter({k for k, v in counts.items() if v == 1}))

    def singleNumber(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        a = b = 0
        for n in nums:
            b = (b ^ n) & ~a
            a = (a ^ n) & ~b
        return b

    def singleNumber(self, nums: List[int]) -> int:  # O(n) time
        # generic solution to all numbers K times, one number only once
        def get_single_number(array: List[int], k: int):  # O(32n) time
            single_number = 0
            for bit in range(32):
                bit_count = 0
                for n in array:
                    if n & 1 << bit:
                        bit_count += 1
                if bit_count % k == 1:
                    single_number |= 1 << bit
            return single_number

        return get_single_number(nums, 3)


def test():
    arguments = [
        [2, 2, 3, 2],
        [0, 1, 0, 1, 0, 1, 99],
        ]
    expectations = [3, 99]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().singleNumber(nums)
        assert solution == expected
