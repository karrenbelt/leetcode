### Source : https://leetcode.com/problems/minimum-moves-to-equal-array-elements-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-19

##################################################################################################### 
#
# Given an integer array nums of size n, return the minimum number of moves required to make all 
# array elements equal.
# 
# In one move, you can increment or decrement an element of the array by 1.
# 
# Example 1:
# 
# Input: nums = [1,2,3]
# Output: 2
# Explanation:
# Only two moves are needed (remember each move increments or decrements one element):
# [1,2,3]  =>  [2,2,3]  =>  [2,2,2]
# 
# Example 2:
# 
# Input: nums = [1,10,2,9]
# Output: 16
# 
# Constraints:
# 
# 	n == nums.length
# 	1 <= nums.length <= 105
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def minMoves2(self, nums: List[int]) -> int:  # O(n^2) time -> TLE: 28 / 30 test cases passed.
        return min(sum(abs(i - j) for j in nums) for i in nums)

    def minMoves2(self, nums: List[int]) -> int:  # O(n log n) time and O(n) space (could do O(1) space)
        median = sorted(nums)[len(nums) // 2]
        return sum(abs(median - n) for n in nums)

    def minMoves2(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        nums.sort()
        return sum(nums[~i] - nums[i] for i in range(len(nums) // 2))


def test():
    arguments = [
        [1, 2, 3],
        [1, 10, 2, 9],
        [1, 0, 0, 8, 6],
    ]
    expectations = [2, 16, 14]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minMoves2(nums)
        assert solution == expected
