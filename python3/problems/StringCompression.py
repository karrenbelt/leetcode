### Source : https://leetcode.com/problems/string-compression/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-05

##################################################################################################### 
#
# Given an array of characters chars, compress it using the following algorithm:
# 
# Begin with an empty string s. For each group of consecutive repeating characters in chars:
# 
# 	If the group's length is 1, append the character to s.
# 	Otherwise, append the character followed by the group's length.
# 
# The compressed string s should not be returned separately, but instead be stored in the input 
# character array chars. Note that group lengths that are 10 or longer will be split into multiple 
# characters in chars.
# 
# After you are done modifying the input array, return the new length of the array.
# 
# Follow up:
# Could you solve it using only O(1) extra space?
# 
# Example 1:
# 
# Input: chars = ["a","a","b","b","c","c","c"]
# Output: Return 6, and the first 6 characters of the input array should be: ["a","2","b","2","c","3"]
# Explanation: The groups are "aa", "bb", and "ccc". This compresses to "a2b2c3".
# 
# Example 2:
# 
# Input: chars = ["a"]
# Output: Return 1, and the first character of the input array should be: ["a"]
# Explanation: The only group is "a", which remains uncompressed since it's a single character.
# 
# Example 3:
# 
# Input: chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
# Output: Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].
# Explanation: The groups are "a" and "bbbbbbbbbbbb". This compresses to "ab12".
# 
# Example 4:
# 
# Input: chars = ["a","a","a","b","b","a","a"]
# Output: Return 6, and the first 6 characters of the input array should be: 
# ["a","3","b","2","a","2"].
# Explanation: The groups are "aaa", "bb", and "aa". This compresses to "a3b2a2". Note that each 
# group is independent even if two groups have the same character.
# 
# Constraints:
# 
# 	1 <= chars.length <= 2000
# 	chars[i] is a lower-case English letter, upper-case English letter, digit, or symbol.
#####################################################################################################

from typing import List


class Solution:
    def compress(self, chars: List[str]) -> int:  # O(n) time and O(1) space
        i, count = 0, 1
        for j in range(1, len(chars) + 1):
            if j < len(chars) and chars[j] == chars[j - 1]:
                count += 1
            else:
                chars[i] = chars[j - 1]
                i += 1
                if count > 1:
                    for k in str(count):
                        chars[i] = k
                        i += 1
                count = 1
        chars[:] = chars[:i]
        return i

    def compress(self, chars: List[str]) -> int:  # O(n) time and O(1) space
        walker, runner = 0, 0
        while runner < len(chars):
            chars[walker] = chars[runner]
            count = 1
            while runner + 1 < len(chars) and chars[runner] == chars[runner + 1]:
                runner += 1
                count += 1
            if count > 1:
                for c in str(count):
                    chars[walker + 1] = c
                    walker += 1
                    runner += 1
            walker += 1
        return walker

    def compress(self, chars: List[str]) -> int:  # O(n^2) time and O(1) space
        count = 1
        for i in reversed(range(len(chars))):
            if i and chars[i] == chars[i - 1]:
                count += 1
                chars.pop(i)
            elif count > 1:
                for digit in reversed(str(count)):
                    chars.insert(i + 1, digit)
                count = 1
        return len(chars)

    def compress(self, chars: List[str]) -> int:  # O(n) time
        from itertools import groupby

        i = 0
        for char, itr in groupby(chars):
            i += 1
            chars[i] = char
            count = str(len(list(itr)))
            if count != "1":
                chars[i:i + len(count)] = count
                i += len(count)

        return i

    def compress(self, chars: List[str]) -> int:  # O(n) time and space
        from itertools import groupby

        s = ""
        for c, group in groupby(chars):
            count = len(list(group))
            s += c
            if count > 1:
                s += str(count)

        chars[:] = s
        return len(s)

    def compress(self, chars: List[str]) -> int:  # O(n) time
        from functools import reduce
        flips = [(chars[0], 0)] + [(chars[i], i) for i in range(1, len(chars)) if chars[i] != chars[i - 1]] + [(None, len(chars))]
        chunks = [(b[0], a[1] - b[1]) for (a, b) in zip(flips[1:], flips)]
        compressed = reduce(lambda a, b: (a + [b[0]] + (list(str(b[1])) if (b[1] > 1) else [])), chunks, [])
        chars[:len(compressed)] = compressed
        return len(compressed)

    def compress(self, chars: List[str]) -> int:  # O(n) time
        import re
        chars[:] = re.sub(r'(.)\1+', lambda m: m[1] + str(len(m[0])), ''.join(chars))
        return len(chars)


def test():
    arguments = [
        ["a", "a", "b", "b", "c", "c", "c"],
        ["a"],
        ["a", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"],
        ["a", "a", "a", "b", "b", "a", "a"],
        ]
    expectations = [6, 1, 4, 6]
    for chars, expected in zip(arguments, expectations):
        solution = Solution().compress(chars)
        assert solution == expected
