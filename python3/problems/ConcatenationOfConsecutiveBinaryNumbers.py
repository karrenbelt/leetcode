### Source : https://leetcode.com/problems/concatenation-of-consecutive-binary-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# Given an integer n, return the decimal value of the binary string formed by concatenating the 
# binary representations of 1 to n in order, modulo 109 + 7.
# 
# Example 1:
# 
# Input: n = 1
# Output: 1
# Explanation: "1" in binary corresponds to the decimal value 1. 
# 
# Example 2:
# 
# Input: n = 3
# Output: 27
# Explanation: In binary, 1, 2, and 3 corresponds to "1", "10", and "11".
# After concatenating them, we have "11011", which corresponds to the decimal value 27.
# 
# Example 3:
# 
# Input: n = 12
# Output: 505379714
# Explanation: The concatenation results in "1101110010111011110001001101010111100".
# The decimal value of that is 118505380540.
# After modulo 109 + 7, the result is 505379714.
# 
# Constraints:
# 
# 	1 <= n <= 105
#####################################################################################################


class Solution:
    def concatenatedBinary(self, n: int) -> int:  # O(n) time and O(n * n^2) space
        return int("".join(bin(i)[2:] for i in range(n + 1)), 2) % (10**9 + 7)

    def concatenatedBinary(self, n: int) -> int:
        return int("".join(format(i, "b") for i in range(n + 1)), 2) % (10**9 + 7)

    def concatenatedBinary(self, n: int) -> int:  # O(n) time and O(1) space
        s, prime = 0, 1_000_000_007
        for i in range(n + 1):
            s = (s << i.bit_length() | i) % prime
        return s

    def concatenatedBinary(self, n: int) -> int:  # O(n) time and O(1) space
        from functools import reduce
        return reduce(lambda s, i: (s << i.bit_length() | i) % 1000000007, range(n + 1))


def test():
    arguments = [1, 3, 12]
    expectations = [1, 27, 505379714]
    for n, expected in zip(arguments, expectations):
        solution = Solution().concatenatedBinary(n)
        assert solution == expected
