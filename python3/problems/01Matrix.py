### Source : https://leetcode.com/problems/01-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
# 
# The distance between two adjacent cells is 1.
# 
# Example 1: 
# 
# Input:
# [[0,0,0],
#  [0,1,0],
#  [0,0,0]]
# 
# Output:
# [[0,0,0],
#  [0,1,0],
#  [0,0,0]]
# 
# Example 2: 
# 
# Input:
# [[0,0,0],
#  [0,1,0],
#  [1,1,1]]
# 
# Output:
# [[0,0,0],
#  [0,1,0],
#  [1,2,1]]
# 
# Note:
# 
# 	The number of elements of the given matrix will not exceed 10,000.
# 	There are at least one 0 in the given matrix.
# 	The cells are adjacent in only four directions: up, down, left and right.
# 
#####################################################################################################

from typing import List


class Solution:
    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:  # TLE

        def dfs(i: int, j: int, seen: set):  # backtracking
            if 0 <= i < len(matrix) and 0 <= j < len(matrix[0]) and (i, j) not in seen:
                seen.add((i, j))
                distance = max(0, min(dfs(i + x, j + y, seen) + 1 for x, y in directions)) if matrix[i][j] else 0
                seen.remove((i, j))
                return distance
            return float("inf")

        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                matrix[i][j] = dfs(i, j, set())
                
        return matrix

    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:  # bfs
        from collections import deque

        queue = deque()
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j]:
                    matrix[i][j] = 1 << 31
                else:
                    queue.append((i, j))

        while queue:
            i, j = queue.popleft()
            for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                if 0 <= x < len(matrix) and 0 <= y < len(matrix[0]) and matrix[i][j] + 1 < matrix[x][y]:
                    matrix[x][y] = matrix[i][j] + 1
                    queue.append((x, y))

        return matrix

    def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:  # O(mn) time and O(1) space

        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j]:
                    left = matrix[i][j - 1] if j > 0 else 1 << 31
                    up = matrix[i - 1][j] if i > 0 else 1 << 31
                    matrix[i][j] = 1 + min(left, up)

        for i in reversed(range(len(matrix))):
            for j in reversed(range(len(matrix[0]))):
                if matrix[i][j]:
                    right = matrix[i][j + 1] if j < len(matrix[0]) - 1 else 1 << 31
                    down = matrix[i + 1][j] if i < len(matrix) - 1 else 1 << 31
                    matrix[i][j] = min(matrix[i][j], 1 + right, 1 + down)

        return matrix


def test():
    arguments = [
        [[0, 0, 0],
         [0, 1, 0],
         [0, 0, 0]],

        [[0, 0, 0],
         [0, 1, 0],
         [1, 1, 1]],
        ]
    expectations = [
        [[0, 0, 0],
         [0, 1, 0],
         [0, 0, 0]],

        [[0, 0, 0],
         [0, 1, 0],
         [1, 2, 1]],
        ]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().updateMatrix(matrix)
        assert solution == expected
