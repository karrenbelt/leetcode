### Source : https://leetcode.com/problems/flatten-a-multilevel-doubly-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-08

##################################################################################################### 
#
# You are given a doubly linked list which in addition to the next and previous pointers, it could 
# have a child pointer, which may or may not point to a separate doubly linked list. These child 
# lists may have one or more children of their own, and so on, to produce a multilevel data 
# structure, as shown in the example below.
# 
# Flatten the list so that all the nodes appear in a single-level, doubly linked list. You are given 
# the head of the first level of the list.
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
# Output: [1,2,3,7,8,11,12,9,10,4,5,6]
# Explanation:
# 
# The multilevel linked list in the input is as follows:
# 
# After flattening the multilevel linked list it becomes:
# 
# Example 2:
# 
# Input: head = [1,2,null,3]
# Output: [1,3,2]
# Explanation:
# 
# The input multilevel linked list is as follows:
# 
#   1---2---NULL
#   |
#   3---NULL
# 
# Example 3:
# 
# Input: head = []
# Output: []
# 
# How multilevel linked list is represented in test case:
# 
# We use the multilevel linked list from Example 1 above:
# 
#  1---2---3---4---5---6--NULL
#          |
#          7---8---9---10--NULL
#              |
#              11--12--NULL
# 
# The serialization of each level is as follows:
# 
# [1,2,3,4,5,6,null]
# [7,8,9,10,null]
# [11,12,null]
# 
# To serialize all levels together we will add nulls in each level to signify no node connects to the 
# upper node of the previous level. The serialization becomes:
# 
# [1,2,3,4,5,6,null]
# [null,null,7,8,9,10,null]
# [null,11,12,null]
# 
# erging the serialization of each level and removing trailing nulls we obtain:
# 
# [1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]
# 
# Constraints:
# 
# 	The number of Nodes will not exceed 1000.
# 	1 <= Node.val <= 105
#####################################################################################################

from python3 import DoublyLinkedListNode as Node

"""
# Definition for a Node.
class Node:
    def __init__(self, val, prev, next, child):
        self.val = val
        self.prev = prev
        self.next = next
        self.child = child
"""


class Solution:
    def flatten(self, head):
        node, stack = head, []
        while node:
            if node.child:
                if node.next:
                    stack.append(node.next)
                node.next, node.next.prev, node.child = node.child, node, None
            if not node.next and stack:
                next_node = stack.pop()
                next_node.prev, node.next = node, next_node
            node = node.next
        return head

    def flatten(self, head: 'Node') -> 'Node':  # two-pointers, O(n) time O(1) space
        node = head
        while node:
            if not node.child:
                node = node.next
                continue
            child_node = node.child
            while child_node.next:
                child_node = child_node.next
            child_node.next = node.next
            if node.next:
                node.next.prev = child_node
            node.next = node.child
            node.child.prev = node
            node.child = None
        return head

    def flatten(self, head: 'Node') -> 'Node':  # O(n) time and O(n) space

        def traverse(node: 'Node'):
            if node:
                yield node
                yield from traverse(node.child)
                yield from traverse(node.next)

        if head:
            nodes = list(traverse(head))  # purpose of generator is lost...
            for i in range(1, len(nodes)):
                nodes[i].prev, nodes[i - 1].next = nodes[i - 1], nodes[i]
                nodes[i].child = None
            head.child = None
        return head


def test():
    serialized_multilevel_doubly_linked_lists = [
        "[1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]",
        "[1,2,null,3]",
        "[]",
        ]
    expectations = [
        "[1,2,3,7,8,11,12,9,10,4,5,6]",
        "[1,3,2]",
        "[]"
        ]
    for serialized_linked_list, expected in zip(serialized_multilevel_doubly_linked_lists, expectations):
        # head = MLLLCodec.deserialize(serialized_linked_list)
        head = serialized_linked_list
        solution = Solution().flatten(head)
        assert solution == expected
