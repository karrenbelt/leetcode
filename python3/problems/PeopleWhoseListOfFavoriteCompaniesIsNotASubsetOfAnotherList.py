### Source : https://leetcode.com/problems/people-whose-list-of-favorite-companies-is-not-a-subset-of-another-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-20

##################################################################################################### 
#
# Given the array favoriteCompanies where favoriteCompanies[i] is the list of favorites companies for 
# the ith person (indexed from 0).
# 
# Return the indices of people whose list of favorite companies is not a subset of any other list of 
# favorites companies. You must return the indices in increasing order.
# 
# Example 1:
# 
# Input: favoriteCompanies = 
# [["leetcode","google","facebook"],["google","microsoft"],["google","facebook"],["google"],["amazon"]
# ]
# Output: [0,1,4] 
# Explanation: 
# Person with index=2 has favoriteCompanies[2]=["google","facebook"] which is a subset of 
# favoriteCompanies[0]=["leetcode","google","facebook"] corresponding to the person with index 0. 
# Person with index=3 has favoriteCompanies[3]=["google"] which is a subset of 
# favoriteCompanies[0]=["leetcode","google","facebook"] and 
# favoriteCompanies[1]=["google","microsoft"]. 
# Other lists of favorite companies are not a subset of another list, therefore, the answer is 
# [0,1,4].
# 
# Example 2:
# 
# Input: favoriteCompanies = 
# [["leetcode","google","facebook"],["leetcode","amazon"],["facebook","google"]]
# Output: [0,1] 
# Explanation: In this case favoriteCompanies[2]=["facebook","google"] is a subset of 
# favoriteCompanies[0]=["leetcode","google","facebook"], therefore, the answer is [0,1].
# 
# Example 3:
# 
# Input: favoriteCompanies = [["leetcode"],["google"],["facebook"],["amazon"]]
# Output: [0,1,2,3]
# 
# Constraints:
# 
# 	1 <= favoriteCompanies.length <= 100
# 	1 <= favoriteCompanies[i].length <= 500
# 	1 <= favoriteCompanies[i][j].length <= 20
# 	All strings in favoriteCompanies[i] are distinct.
# 	All lists of favorite companies are distinct, that is, If we sort alphabetically each list 
# then favoriteCompanies[i] != favoriteCompanies[j].
# 	All strings consist of lowercase English letters only.
#####################################################################################################

from typing import List


class Solution:
    def peopleIndexes(self, favoriteCompanies: List[List[str]]) -> List[int]:  # O(n^2) time: 484 ms
        sets = list(map(set, favoriteCompanies))
        return [i for i, s1 in enumerate(sets) if all(i == j or not s1.issubset(s2) for j, s2 in enumerate(sets))]

    def peopleIndexes(self, favoriteCompanies: List[List[str]]) -> List[int]:  # O(n^2) time: 448 ms
        indices, companies = [], sorted(enumerate(map(set, favoriteCompanies)), key=lambda x: len(x[1]))
        for i in range(len(companies) - 1, -1, -1):
            if not any(companies[i][1].issubset(companies[j][1]) for j in range(i + 1, len(companies))):
                indices.append(companies[i][0])
        return sorted(indices)


def test():
    arguments = [
        [["leetcode", "google", "facebook"], ["google", "microsoft"], ["google", "facebook"], ["google"], ["amazon"]],
        [["leetcode", "google", "facebook"], ["leetcode", "amazon"], ["facebook", "google"]],
        [["leetcode"], ["google"], ["facebook"], ["amazon"]],
    ]
    expectations = [
        [0, 1, 4],
        [0, 1],
        [0, 1, 2, 3],
    ]
    for favoriteCompanies, expected in zip(arguments, expectations):
        solution = Solution().peopleIndexes(favoriteCompanies)
        assert solution == expected
