### Source : https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-03

##################################################################################################### 
#
# Return the length of the shortest, non-empty, contiguous subarray of nums with sum at least k.
# 
# If there is no non-empty subarray with sum at least k, return -1.
# 
# Example 1:
# 
# Input: nums = [1], k = 1
# Output: 1
# 
# Example 2:
# 
# Input: nums = [1,2], k = 4
# Output: -1
# 
# Example 3:
# 
# Input: nums = [2,-1,2], k = 3
# Output: 3
# 
# Note:
# 
# 	1 <= nums.length <= 50000
# 	-105 <= nums[i] <= 105
# 	1 <= k <= 109
#####################################################################################################

from typing import List


class Solution:
    def shortestSubarray(self, nums: List[int], k: int) -> int:  # same as 209. MinimumSizeSubarraySum.py ?
        return


def test():
    arguments = [
        ([1], 1),
        ([1, 2], 4),
        ([2, -1, 2], 3),
    ]
    expectations = [1, -1, 3]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().shortestSubarray(nums, k)
        assert solution == expected
