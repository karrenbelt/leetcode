### Source : https://leetcode.com/problems/evaluate-the-bracket-pairs-of-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-20

##################################################################################################### 
#
# You are given a string s that contains some bracket pairs, with each pair containing a non-empty 
# key.
# 
# 	For example, in the string "(name)is(age)yearsold", there are two bracket pairs that 
# contain the keys "name" and "age".
# 
# You know the values of a wide range of keys. This is represented by a 2D string array knowledge 
# where each knowledge[i] = [keyi, valuei] indicates that key keyi has a value of valuei.
# 
# You are tasked to evaluate all of the bracket pairs. When you evaluate a bracket pair that contains 
# some key keyi, you will:
# 
# 	Replace keyi and the bracket pair with the key's corresponding valuei.
# 	If you do not know the value of the key, you will replace keyi and the bracket pair with a 
# question mark "?" (without the quotation marks).
# 
# Each key will appear at most once in your knowledge. There will not be any nested brackets in s.
# 
# Return the resulting string after evaluating all of the bracket pairs.
# 
# Example 1:
# 
# Input: s = "(name)is(age)yearsold", knowledge = [["name","bob"],["age","two"]]
# Output: "bobistwoyearsold"
# Explanation:
# The key "name" has a value of "bob", so replace "(name)" with "bob".
# The key "age" has a value of "two", so replace "(age)" with "two".
# 
# Example 2:
# 
# Input: s = "hi(name)", knowledge = [["a","b"]]
# Output: "hi?"
# Explanation: As you do not know the value of the key "name", replace "(name)" with "?".
# 
# Example 3:
# 
# Input: s = "(a)(a)(a)aaa", knowledge = [["a","yes"]]
# Output: "yesyesyesaaa"
# Explanation: The same key can appear multiple times.
# The key "a" has a value of "yes", so replace all occurrences of "(a)" with "yes".
# Notice that the "a"s not in a bracket pair are not evaluated.
# 
# Example 4:
# 
# Input: s = "(a)(b)", knowledge = [["a","b"],["b","a"]]
# Output: "ba"
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	0 <= knowledge.length <= 105
# 	knowledge[i].length == 2
# 	1 <= keyi.length, valuei.length <= 10
# 	s consists of lowercase English letters and round brackets '(' and ')'.
# 	Every open bracket '(' in s will have a corresponding close bracket ')'.
# 	The key in each bracket pair of s will be non-empty.
# 	There will not be any nested bracket pairs in s.
# 	keyi and valuei consist of lowercase English letters.
# 	Each keyi in knowledge is unique.
#####################################################################################################

from typing import List


class Solution:
    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # O(n^2) time
        mapping = {f"({key})": value for key, value in knowledge}
        i, keys = 0, []
        while (i := s.find('(', i)) != -1:
            j = s.find(')', i) + 1
            keys.append(s[i:j])
            i = j
        for key in keys:
            s = s.replace(key, mapping.get(key, "?"))
        return s

    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # code mutilation - works locally on python 3.9
        j, keys, mapping = 0, [], {f"({key})": value for key, value in knowledge}
        while (i := s.find('(', j)) != -1:
            j = s.find(')', i) + 1
            keys.append(s[i:j])
        return s if all(s := s.replace(key, mapping.get(key, "?")) for key in keys) else s

    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # O(n) time and O(n) space
        mapping = {key: value for key, value in knowledge}
        formatted, current, reading_key = [], '', False
        for c in s:
            if c == '(':
                reading_key = True
            elif c == ')':
                reading_key = False
                formatted.append(mapping.get(current, '?'))
                current = ''
            elif reading_key:
                current += c
            else:
                formatted.append(c)
        return "".join(formatted)

    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # TLE: 98 / 105 test cases passed.
        import re
        mapping = {f"({key})": value for key, value in knowledge}
        pattern = "|".join(map(re.escape, mapping.keys()))
        return re.sub(r"\([a-z]*\)", "?", re.sub(pattern, lambda match: mapping[match.group(0)], s) if pattern else s)

    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # O(n) time and O(n) space
        mapping, tokens = {f"({k})": v for k, v in knowledge}, s.replace('(', ' (').replace(')', ') ').split()
        return ''.join(token if token.isalpha() else mapping.get(token, "?") for token in tokens)

    def evaluate(self, s: str, knowledge: List[List[str]]) -> str:  # credit to: thhsu
        from collections import defaultdict
        return s.replace('(', '{d[').replace(')', ']}').format(d=defaultdict(lambda: '?', knowledge))

    def evaluate(self, s, knowledge):  # O(n + m) time and O(n + m) space
        mapping, tokens = dict(knowledge), s.split("(")
        return tokens[0] + ''.join(mapping.get(a, "?") + b for a, b in (token.split(")") for token in tokens[1:]))


def test():
    arguments = [
        ("(name)is(age)yearsold", [["name", "bob"], ["age", "two"]]),
        ("hi(name)", [["a", "b"]]),
        ("(a)(a)(a)aaa", [["a", "yes"]]),
        ("(a)(b)", [["a", "b"], ["b", "a"]]),
        ("c", []),
        ("(fy)(kj)(ege)r",
         [["uxhhkpvp", "h"], ["nriroroa", "m"], ["wvhiycvo", "z"], ["qsmfeing", "s"], ["hbcyqulf", "q"],
          ["xwgfjnrf", "b"], ["kfipazun", "s"], ["wnkrtxui", "u"], ["abwlsese", "e"], ["iimsmftc", "h"],
          ["pafqkquo", "v"], ["kj", "tzv"], ["fwwxotcd", "t"], ["yzgjjwjr", "l"]]),
    ]
    expectations = ["bobistwoyearsold", "hi?", "yesyesyesaaa", "ba", "c", "?tzv?r"]
    for (s, knowledge), expected in zip(arguments, expectations):
        solution = Solution().evaluate(s, knowledge)
        assert solution == expected, solution
