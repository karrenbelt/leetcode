### Source : https://leetcode.com/problems/check-if-word-equals-summation-of-two-words/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# The letter value of a letter is its position in the alphabet starting from 0 (i.e. 'a' -> 0, 'b' -> 
# 1, 'c' -> 2, etc.).
# 
# The numerical value of some string of lowercase English letters s is the concatenation of the 
# letter values of each letter in s, which is then converted into an integer.
# 
# 	For example, if s = "acb", we concatenate each letter's letter value, resulting in "021". 
# After converting it, we get 21.
# 
# You are given three strings firstWord, secondWord, and targetWord, each consisting of lowercase 
# English letters 'a' through 'j' inclusive.
# 
# Return true if the summation of the numerical values of firstWord and secondWord equals the 
# numerical value of targetWord, or false otherwise.
# 
# Example 1:
# 
# Input: firstWord = "acb", secondWord = "cba", targetWord = "cdb"
# Output: true
# Explanation:
# The numerical value of firstWord is "acb" -> "021" -> 21.
# The numerical value of secondWord is "cba" -> "210" -> 210.
# The numerical value of targetWord is "cdb" -> "231" -> 231.
# We return true because 21 + 210 == 231.
# 
# Example 2:
# 
# Input: firstWord = "aaa", secondWord = "a", targetWord = "aab"
# Output: false
# Explanation: 
# The numerical value of firstWord is "aaa" -> "000" -> 0.
# The numerical value of secondWord is "a" -> "0" -> 0.
# The numerical value of targetWord is "aab" -> "001" -> 1.
# We return false because 0 + 0 != 1.
# 
# Example 3:
# 
# Input: firstWord = "aaa", secondWord = "a", targetWord = "aaaa"
# Output: true
# Explanation: 
# The numerical value of firstWord is "aaa" -> "000" -> 0.
# The numerical value of secondWord is "a" -> "0" -> 0.
# The numerical value of targetWord is "aaaa" -> "0000" -> 0.
# We return true because 0 + 0 == 0.
# 
# Constraints:
# 
# 	1 <= firstWord.length, secondWord.length, targetWord.length <= 8
# 	firstWord, secondWord, and targetWord consist of lowercase English letters from 'a' to 'j' 
# inclusive.
#####################################################################################################


class Solution:
    def isSumEqual(self, firstWord: str, secondWord: str, targetWord: str) -> bool:

        def to_int(word: str) -> int:
            return int("".join(str(ord(c) - 97) for c in word))

        return to_int(firstWord) + to_int(secondWord) == to_int(targetWord)

    def isSumEqual(self, firstWord: str, secondWord: str, targetWord: str) -> bool:

        def to_int(word: str) -> int:
            return int("".join(map(mapping.get, word)))

        mapping = {chr(i + 97): str(i) for i in range(10)}
        return to_int(firstWord) + to_int(secondWord) == to_int(targetWord)

    def isSumEqual(self, firstWord: str, secondWord: str, targetWord: str) -> bool:
        words = firstWord, secondWord, targetWord
        table = str.maketrans({chr(i + 97): str(i) for i in range(10)})
        a, b, c = map(lambda s: int(s.translate(table)), words)
        return a + b == c

    def isSumEqual(self, firstWord: str, secondWord: str, targetWord: str) -> bool:
        words = firstWord, secondWord, targetWord
        a, b, c = (int(''.join(str(ord(c) - 97) for c in x)) for x in words)
        return a + b == c


def test():
    arguments = [
        ("acb", "cba", "cdb"),
        ("aaa", "a", "aab"),
        ("aaa", "a", "aaaa"),
    ]
    expectations = [True, False, True]
    for (firstWord, secondWord, targetWord), expected in zip(arguments, expectations):
        solution = Solution().isSumEqual(firstWord, secondWord, targetWord)
        assert solution == expected
