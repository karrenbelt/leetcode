### Source : https://leetcode.com/problems/leaf-similar-trees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-03

##################################################################################################### 
#
# Consider all the leaves of a binary tree, from left to right order, the values of those leaves form 
# a leaf value sequence.
# 
# For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).
# 
# Two binary trees are considered leaf-similar if their leaf value sequence is the same.
# 
# Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.
# 
# Example 1:
# 
# Input: root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 = 
# [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
# Output: true
# 
# Example 2:
# 
# Input: root1 = [1], root2 = [1]
# Output: true
# 
# Example 3:
# 
# Input: root1 = [1], root2 = [2]
# Output: false
# 
# Example 4:
# 
# Input: root1 = [1,2], root2 = [2,2]
# Output: true
# 
# Example 5:
# 
# Input: root1 = [1,2,3], root2 = [1,3,2]
# Output: false
# 
# Constraints:
# 
# 	The number of nodes in each tree will be in the range [1, 200].
# 	Both of the given trees will have values in the range [0, 200].
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:

        def traverse(node: TreeNode):
            if not node:
                return []
            if not node.left and not node.right:
                return [node.val]
            left = traverse(node.left)
            right = traverse(node.right)
            return left + right

        return traverse(root1) == traverse(root2)

    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:

        def dfs(node: TreeNode):
            return [] if not node else [node.val] if not (node.left or node.right) else dfs(node.left) + dfs(node.right)

        return dfs(root1) == dfs(root2)

    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
        from itertools import zip_longest

        def traverse(node: TreeNode):
            if not node:
                return
            if not node.left and not node.right:
                yield node.val
            yield from traverse(node.left)
            yield from traverse(node.right)

        return all(a == b for a, b in zip_longest(traverse(root1), traverse(root2)))



def test():
    from python3 import Codec
    arguments = [
        ("[3,5,1,6,2,9,8,null,null,7,4]", "[3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]"),
        ("[1]", "[1]"),
        ("[1]", "[2]"),
        ("[1,2]", "[2,2]"),
        ("[1,2,3]", "[1,3,2]"),
    ]
    expectations = [True, True, False, True, False]
    for serialized_trees, expected in zip(arguments, expectations):
        root1, root2 = map(Codec.deserialize, serialized_trees)
        solution = Solution().leafSimilar(root1, root2)
        assert solution == expected
