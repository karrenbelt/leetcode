### Source : https://leetcode.com/problems/permutations/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-22

##################################################################################################### 
#
# Given an array nums of distinct integers, return all the possible permutations. You can return the 
# answer in any order.
# 
# Example 1:
# Input: nums = [1,2,3]
# Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
# Example 2:
# Input: nums = [0,1]
# Output: [[0,1],[1,0]]
# Example 3:
# Input: nums = [1]
# Output: [[1]]
# 
# Constraints:
# 
# 	1 <= nums.length <= 6
# 	-10 <= nums[i] <= 10
# 	All the integers of nums are unique.
#####################################################################################################

from typing import List


class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:  # O(n * n!) time and O(n!) space

        def permute(remainder, path):
            if not remainder:
                ans.append(path)
            for i, n in enumerate(remainder):
                permute(remainder[:i] + remainder[i + 1:], path + [n])

        ans = []
        permute(nums, [])
        return ans

    def permute(self, nums: List[int]) -> List[List[int]]:
        paths = [[]]
        for n in nums:
            tmp = []
            for path in paths:
                for i in range(len(path)+1):
                    tmp.append(path[:i] + [n] + path[i:])
            paths = tmp
        return paths

    def permute(self, nums: List[int]) -> List[List[int]]:
        if len(nums) == 1:
            return [nums]
        result = []
        for i in range(len(nums)):
            for permutation in self.permute(nums[:i] + nums[i + 1:]):
                result.append([nums[i]] + permutation)
        return result

    def permute(self, nums: List[int]) -> List[List[int]]:

        def backtracking(remaining: list, path: list, paths: list):
            if not remaining:
                paths.append(path.copy())
            for i in range(len(remaining)):
                path.append(remaining[i])
                backtracking(remaining[:i] + remaining[i + 1:], path, paths)
                path.pop()
            return paths

        return backtracking(nums, [], [])

    def permute(self, nums: List[int]) -> List[List[int]]:

        def dfs(remaining: list, path: list, paths: list):
            if not remaining:
                paths.append(path)
            for i in range(len(remaining)):
                dfs(remaining[:i] + remaining[i + 1:], path + [remaining[i]], paths)
            return paths

        return dfs(nums, [], [])

    def permute(self, nums: List[int]) -> List[List[int]]:

        def dfs(remainder: list):
            if not remainder:
                yield []
            for i, n in enumerate(remainder):
                for p in dfs(remainder[:i] + remainder[i + 1:]):
                    yield [n] + p

        return list(dfs(nums))

    def permute(self, nums: List[int]) -> List[List[int]]:  # iterative dfs
        stack = [(nums, [])]
        paths = []
        while stack:
            nums, path = stack.pop()
            if not nums:
                paths.append(path)
            for i in range(len(nums)):
                stack.append((nums[:i] + nums[i + 1:], path + [nums[i]]))
        return paths

    def permute(self, nums: List[int]) -> List[List[int]]:  # iterative bfs
        from collections import deque
        queue = deque([(nums, [])])
        paths = []
        while queue:
            nums, path = queue.popleft()
            if not nums:
                paths.append(path)
            for i in range(len(nums)):
                queue.append((nums[:i] + nums[i + 1:], path + [nums[i]]))
        return paths

    def permute(self, nums: List[int]) -> List[List[int]]:
        paths = [[]]
        for i in range(len(nums)):
            paths = [path + [n] for path in paths for n in nums if n not in path]
        return paths

    def permute(self, nums: List[int]) -> List[List[int]]:
        return [[n] + p for i, n in enumerate(nums) for p in self.permute(nums[:i] + nums[i + 1:])] or [[]]

    def permute(self, nums: List[int]) -> List[List[int]]:
        import itertools
        return list(map(list, itertools.permutations(nums)))


def test():
    arrays_of_numbers = [
        [1, 2, 3],
        [0, 1],
        [1],
        ]
    expectations = [
        [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]],
        [[0, 1], [1, 0]],
        [[1]],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().permute(nums)
        assert solution == expected
