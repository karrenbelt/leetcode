### Source : https://leetcode.com/problems/check-completeness-of-a-binary-tree/
### Author : karrenbelt
### Date   : 2023-03-15

##################################################################################################### 
#
# Given the root of a binary tree, determine if it is a complete binary tree.
# 
# In a complete binary tree, every level, except possibly the last, is completely filled, and all 
# nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive 
# at the last level h.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,6]
# Output: true
# Explanation: Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and 
# all nodes in the last level ({4, 5, 6}) are as far left as possible.
# 
# Example 2:
# 
# Input: root = [1,2,3,4,5,null,7]
# Output: false
# Explanation: The node with value 7 isn't as far left as possible.
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 100].
# 	1 <= Node.val <= 1000
#####################################################################################################


class Solution:
    def isCompleteTree(self, root: Optional[TreeNode]) -> bool:
        # consecutive nulls means the tree is incomplete
        
        from collections import deque

        has_null, queue = False, deque([root] if root else [])
        
        while queue:
            node = queue.popleft()
            if not node:
                has_null = True
                continue
            if has_null:
                return False
            queue.extend([node.left, node.right])
            

        return True 


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,5,6]",
        "[1,2,3,4,5,null,7]",
        "[1,2,3,5,null,7,8]",
        ]
    expectations = [True, False, False]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().sumNumbers(root)
        assert solution == expected