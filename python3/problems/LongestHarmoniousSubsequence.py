### Source : https://leetcode.com/problems/longest-harmonious-subsequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-04

##################################################################################################### 
#
# We define a harmonious array as an array where the difference between its maximum value and its 
# minimum value is exactly 1.
# 
# Given an integer array nums, return the length of its longest harmonious subsequence among all its 
# possible subsequences.
# 
# A subsequence of array is a sequence that can be derived from the array by deleting some or no 
# elements without changing the order of the remaining elements.
# 
# Example 1:
# 
# Input: nums = [1,3,2,2,5,2,3,7]
# Output: 5
# Explanation: The longest harmonious subsequence is [3,2,2,2,3].
# 
# Example 2:
# 
# Input: nums = [1,2,3,4]
# Output: 2
# 
# Example 3:
# 
# Input: nums = [1,1,1,1]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def findLHS(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        # since we can delete we can use a counter, or we can sort the array

        def count(array: List[int]):
            d = {}
            for n in array:
                d[n] = d[n] + 1 if n in d else 1
            return d

        counts = count(nums)
        longest = 0
        for num, c in counts.items():  # note, the num - 1 is not needed since we iterate all keys of the dict
            longest = max(longest, c + max(counts.get(num + 1, -c), counts.get(num - 1, -c)))
        return longest

    def findLHS(self, nums: List[int]) -> int:  # O(n) time and O(n) space, single loop
        counts = {}
        longest = 0
        for n in nums:
            counts[n] = counts[n] + 1 if n in counts else 1
            longest = max(longest, counts[n] + max(counts.get(n + 1, -counts[n]), counts.get(n - 1, -counts[n])))
        return longest


def test():
    arguments = [
        [1, 3, 2, 2, 5, 2, 3, 7],
        [1, 2, 3, 4],
        [1, 1, 1, 1],
        ]
    expectations = [5, 2, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findLHS(nums)
        assert solution == expected
