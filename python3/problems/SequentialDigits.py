### Source : https://leetcode.com/problems/sequential-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-05

##################################################################################################### 
#
# An integer has sequential digits if and only if each digit in the number is one more than the 
# previous digit.
# 
# Return a sorted list of all the integers in the range [low, high] inclusive that have sequential 
# digits.
# 
# Example 1:
# Input: low = 100, high = 300
# Output: [123,234]
# Example 2:
# Input: low = 1000, high = 13000
# Output: [1234,2345,3456,4567,5678,6789,12345]
# 
# Constraints:
#
# 	10 <= low <= high <= 109
#####################################################################################################

from typing import List


class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:

        def dfs(path: List[int]):
            if min_len <= len(path) <= max_len:
                n = int(''.join(map(str, path)))
                if low <= n <= high:
                    paths.append(n)
            if len(path) <= max_len and path[-1] + 1 < 10:
                dfs(path + [path[-1] + 1])

        paths, min_len, max_len = [], len(str(low)), len(str(high))
        return any(dfs([i]) for i in range(1, 10)) or sorted(paths)

    def sequentialDigits(self, low: int, high: int) -> List[int]:

        def dfs(path: List[int]) -> List[int]:
            paths = []
            if min_len <= len(path) <= max_len and (n := int(''.join(map(str, path)))) and low <= n <= high:
                paths += [n]
            if len(path) <= max_len and path[-1] + 1 < 10:
                paths += dfs(path + [path[-1] + 1])
            return paths

        min_len, max_len = len(str(low)), len(str(high))
        return sorted([x for y in (dfs([i]) for i in range(1, 10)) for x in y])

    def sequentialDigits(self, low: int, high: int) -> List[int]:

        def generate(digit: int) -> int:
            n = digit
            while n <= high and digit < 10:
                if n >= low:
                    yield n
                digit += 1
                n = n * 10 + digit

        return sorted(n for digit in range(1, 10) for n in generate(digit))

    def sequentialDigits(self, low: int, high: int) -> List[int]:  # no sorting
        digits = "123456789"
        sequence = []
        for length in range(len(str(low)), len(str(high)) + 1):
            for i in range(len(digits) - length + 1):
                val = int(digits[i:i + length])
                if low <= val <= high:
                    sequence.append(val)
        return sequence


def test():
    arguments = [
        (100, 300),
        (1000, 13000),
        (1, 10**9)
    ]
    expectations = [
        [123, 234],
        [1234, 2345, 3456, 4567, 5678, 6789, 12345],
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 23, 34, 45, 56, 67, 78, 89, 123, 234, 345, 456, 567, 678, 789, 1234, 2345, 3456,
         4567, 5678, 6789, 12345, 23456, 34567, 45678, 56789, 123456, 234567, 345678, 456789, 1234567, 2345678, 3456789,
         12345678, 23456789, 123456789],
    ]
    for (low, high), expected in zip(arguments, expectations):
        solution = Solution().sequentialDigits(low, high)
        assert solution == expected
