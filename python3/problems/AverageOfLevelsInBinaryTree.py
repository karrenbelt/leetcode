### Source : https://leetcode.com/problems/average-of-levels-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given a non-empty binary tree, return the average value of the nodes on each level in the form of 
# an array.
# 
# Example 1:
# 
# Input:
#     3
#    / \
#   9  20
#     /  \
#    15   7
# Output: [3, 14.5, 11]
# Explanation:
# The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return 
# [3, 14.5, 11].
# 
# Note:
# 
# The range of node's value is in the range of 32-bit signed integer.
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:

        def traverse(node: TreeNode, level):
            if not node:
                return
            if len(levels) < level + 1:
                levels.append([])
            levels[level].append(node.val)
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root, 0)
        return [sum(level) / len(level) for level in levels]

    def averageOfLevels(self, root: TreeNode) -> List[float]:  # relatively slow
        levels = []
        level = [root]
        while level:
            values, children = zip(*[(node.val, [node.left, node.right]) for node in level])
            levels.append(sum(values) / len(values))
            level = list(filter(None, sum(children, [])))
        return levels

    def averageOfLevels(self, root: TreeNode) -> List[float]:  # fast
        levels = []
        level = [root]
        while level:
            levels.append(sum(node.val for node in level) / len(level))
            level = [child for node in level for child in (node.left, node.right) if child]
        return levels


def test():
    from python3 import Codec
    serialized_trees = [
        "[3,9,20,null,null,15,7]",
        ]
    expectations = [
        [3, 14.5, 11],
        ]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().averageOfLevels(root)
        assert solution == expected
