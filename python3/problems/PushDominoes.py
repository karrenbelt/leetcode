### Source : https://leetcode.com/problems/push-dominoes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-21

##################################################################################################### 
#
# There are n dominoes in a line, and we place each domino vertically upright. In the beginning, we 
# simultaneously push some of the dominoes either to the left or to the right.
# 
# After each second, each domino that is falling to the left pushes the adjacent domino on the left. 
# Similarly, the dominoes falling to the right push their adjacent dominoes standing on the right.
# 
# When a vertical domino has dominoes falling on it from both sides, it stays still due to the 
# balance of the forces.
# 
# For the purposes of this question, we will consider that a falling domino expends no additional 
# force to a falling or already fallen domino.
# 
# You are given a string dominoes representing the initial state where:
# 
# 	dominoes[i] = 'L', if the ith domino has been pushed to the left,
# 	dominoes[i] = 'R', if the ith domino has been pushed to the right, and
# 	dominoes[i] = '.', if the ith domino has not been pushed.
# 
# Return a string representing the final state.
# 
# Example 1:
# 
# Input: dominoes = "RR.L"
# Output: "RR.L"
# Explanation: The first domino expends no additional force on the second domino.
# 
# Example 2:
# 
# Input: dominoes = ".L.R...LR..L.."
# Output: "LL.RR.LLRRLL.."
# 
# Constraints:
# 
# 	n == dominoes.length
# 	1 <= n <= 105
# 	dominoes[i] is either 'L', 'R', or '.'.
#####################################################################################################


class Solution:
    def pushDominoes(self, dominoes: str) -> str:  # 1036 ms
        # by simulation
        dominoes = list(dominoes)
        dots = {i for i, domino in enumerate(dominoes) if domino == '.'}
        while True:
            new_states = {}
            for i in list(dots):
                left = dominoes[i - 1] if i - 1 >= 0 else '.'
                right = dominoes[i + 1] if i + 1 < len(dominoes) else '.'
                if left == 'R' and right != 'L':
                    new_states[i] = 'R'
                if left != 'R' and right == 'L':
                    new_states[i] = 'L'
            if not new_states:
                break
            for i, state in new_states.items():
                dominoes[i] = state
                dots.remove(i)
        return ''.join(dominoes)

    def pushDominoes(self, dominoes: str) -> str:  # O(n) time: 144 ms
        while True:
            next_state = dominoes.replace('R.L', '#')
            next_state = next_state.replace('.L', 'LL').replace('R.', 'RR')
            if next_state == dominoes:
                break
            dominoes = next_state
        return dominoes.replace('#', 'R.L')

    def pushDominoes(self, dominoes: str) -> str:  # O(n) time: 760 ms
        # which ever is closest, L on the right or R on the left, you become
        solution = [(len(dominoes), '.')] * len(dominoes)
        right_dist = left_dist = len(dominoes)
        for i, val in enumerate(dominoes):
            right_dist = 0 if dominoes[i] == 'R' else len(dominoes) if dominoes[i] == 'L' else right_dist + 1
            solution[i] = (len(dominoes), '.') if right_dist == solution[i][0] else min((right_dist, 'R'), solution[i])
            left_dist = 0 if dominoes[~i] == 'L' else len(dominoes) if dominoes[~i] == 'R' else left_dist + 1
            solution[~i] = (len(dominoes), '.') if left_dist == solution[~i][0] else min((left_dist, 'L'), solution[~i])
        return ''.join(x[1] for x in solution)

    def pushDominoes(self, dominoes: str) -> str:  # O(n) time: 336 ms
        left, right = 0, 0
        solution = list(dominoes)
        while right < len(dominoes):
            if dominoes[right] in "LR" or right == len(dominoes) - 1:
                reach = right - left + 1
                if solution[right] == "L" and solution[left] == "R":
                    solution[left:right + 1] = 'R' * (reach // 2) + '.' * (reach % 2) + 'L' * (reach // 2)
                elif solution[right] == 'L':
                    solution[left:right + 1] = "L" * reach
                elif solution[left] == "R":
                    solution[left:right + 1] = "R" * reach
                left = right
            right += 1
        return "".join(solution)


def test():
    arguments = [
        "RR.L",
        ".L.R...LR..L..",
        ".L.R.",
    ]
    expectations = [
        "RR.L",
        "LL.RR.LLRRLL..",
        "LL.RR",
    ]
    for dominoes, expected in zip(arguments, expectations):
        solution = Solution().pushDominoes(dominoes)
        assert solution == expected, (solution, expected)
