### Source : https://leetcode.com/problems/powx-n/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-22

##################################################################################################### 
#
# Implement pow(x, n), which calculates x raised to the power n (i.e. xn).
# 
# Example 1:
# 
# Input: x = 2.00000, n = 10
# Output: 1024.00000
# 
# Example 2:
# 
# Input: x = 2.10000, n = 3
# Output: 9.26100
# 
# Example 3:
# 
# Input: x = 2.00000, n = -2
# Output: 0.25000
# Explanation: 2-2 = 1/22 = 1/4 = 0.25
# 
# Constraints:
# 
# 	-100.0 < x < 100.0
# 	-231 <= n <= 231-1
# 	-104 <= xn <= 104
#####################################################################################################


class Solution:
    def myPow(self, x: float, n: int) -> float:  # O(log n)
        base = 1
        for _ in range(abs(n)):
            base *= x
        return base if n > 0 else 1 / base

    def myPow(self, x: float, n: int) -> float:  # O(log n)
        if n <= 1:
            return 1 if n == 0 else x if n == 1 else 1 / self.myPow(x, -n)
        half = self.myPow(x, n // 2)
        return half * half * x if n % 2 else half * half

    def myPow(self, x: float, n: int) -> float:  # O(log n)

        def fast(x, n):
            if n == 0:
                return 1
            half = fast(x, n // 2)
            if not n % 2:
                return half * half
            return x * half * half

        if n < 0:
            x = 1 / x
            n = -n

        return fast(x, n)

    def myPow(self, x: float, n: int) -> float:  # O(log n)
        if n == 0:
            return 1
        elif n < 0:
            return 1 / self.myPow(x, abs(n))
        elif n % 2 != 0:
            return x * self.myPow(x, n - 1)
        return self.myPow(x * x, n // 2)

    def myPow(self, x: float, n: int) -> float:  # O(log n)
        p = 1
        while n:
            if n < 0:
                x = 1 / x
                n = -n
            elif n % 2:
                p *= x
                n -= 1
            else:
                x *= x
                n //= 2
        return p

    def myPow(self, x: float, n: int) -> float:  # O(log n)
        m = abs(n)
        p = 1.0
        while m:
            if m & 1:
                p *= x
            x *= x
            m >>= 1
        return p if n >= 0 else 1 / p


def test():
    numbers = [
        (2.0, 10),
        (2.1, 3),
        (2.0, -2),
        ]
    expectations = [1024.0, 9.261, 0.25]
    for (x, n), expected in zip(numbers, expectations):
        solution = Solution().myPow(x, n)
        assert round(solution, 10) == expected
