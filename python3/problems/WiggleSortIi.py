### Source : https://leetcode.com/problems/wiggle-sort-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given an integer array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....
# 
# You may assume the input array always has a valid answer.
# 
# Example 1:
# 
# Input: nums = [1,5,1,1,6,4]
# Output: [1,6,1,5,1,4]
# Explanation: [1,4,1,5,1,6] is also accepted.
# 
# Example 2:
# 
# Input: nums = [1,3,2,2,3,1]
# Output: [2,3,1,3,1,2]
# 
# Constraints:
# 
# 	1 <= nums.length <= 5 * 104
# 	0 <= nums[i] <= 5000
# 	It is guaranteed that there will be an answer for the given input nums.
# 
# Follow Up: Can you do it in O(n) time and/or in-place with O(1) extra space?
#####################################################################################################

from typing import List


class Solution:
    # IMPORTANT: nums[0] < nums[1] > nums[2], and NOT nums[0] > nums[1] < nums[2]
    def wiggleSort(self, nums: List[int]) -> None:  # O(n log n) time and O(1) space
        """
        Do not return anything, modify nums in-place instead.
        """
        nums.sort()
        half = len(nums) // 2 + len(nums) % 2
        nums[::2], nums[1::2] = nums[:half][::-1], nums[half:][::-1]

    def wiggleSort(self, nums: List[int]) -> None:  # O(n log n) time and O(1) space
        for i, num in enumerate(sorted(nums)[::-1]):
            nums[(1 + 2 * i) % (len(nums) | 1)] = num


def test():
    arguments = [
        [1, 5, 1, 1, 6, 4],
        [1, 3, 2, 2, 3, 1],
        [1, 1, 2, 2, 3],
        [1, 1, 2, 1, 2, 2, 1],
        ]
    expectations = [
        ([1, 6, 1, 5, 1, 4], [1, 4, 1, 5, 1, 6]),  # is also accepted.
        ([2, 3, 1, 3, 1, 2], [1, 2,     1, 3, 2, 3]),
        ([2, 3, 1, 2, 1], ),
        ([1, 2, 1, 2, 1, 2, 1], ),
        ]
    for nums, expected in zip(arguments, expectations):
        Solution().wiggleSort(nums)
        print(nums)
        assert tuple(nums) in set(map(tuple, expected))
