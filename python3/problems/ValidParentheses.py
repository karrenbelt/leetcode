### Source : https://leetcode.com/problems/valid-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the 
# input string is valid.
# 
# An input string is valid if:
# 
# 	Open brackets must be closed by the same type of brackets.
# 	Open brackets must be closed in the correct order.
# 
# Note that an empty string is also considered valid.
# 
# Example 1:
# 
# Input: "()"
# Output: true
# 
# Example 2:
# 
# Input: "()[]{}"
# Output: true
# 
# Example 3:
# 
# Input: "(]"
# Output: false
# 
# Example 4:
# 
# Input: "([)]"
# Output: false
# 
# Example 5:
# 
# Input: "{[]}"
# Output: true
# 
#####################################################################################################


class Solution:
    def isValid(self, s: str) -> bool:  # O(n) time
        stack = []
        for i in s:
            if i == '(':
                stack.append(')')
            elif i == '{':
                stack.append('}')
            elif i == '[':
                stack.append(']')
            elif len(stack) == 0 or stack.pop() != i:
                return False
        return len(stack) == 0

    def isValid(self, s: str) -> bool:  # O(n) time
        stack, brackets = [], {'(': ')', '{': '}', '[': ']'}
        for c in s:
            if c in brackets:
                stack.append(brackets[c])
            elif not stack or stack.pop() != c:
                return False
        return not stack


def test():
    strings = ["()", "()[]{}", "(]", "([)]", "{[]}"]
    expectations = [True, True, False, False, True]
    for s, expected in zip(strings, expectations):
        solution = Solution().isValid(s)
        assert solution == expected
