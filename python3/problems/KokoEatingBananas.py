### Source : https://leetcode.com/problems/koko-eating-bananas/
### Author : karrenbelt
### Date   : 2023-03-09

##################################################################################################### 
#
# Koko loves to eat bananas. There are n piles of bananas, the ith pile has piles[i] bananas. The 
# guards have gone and will come back in h hours.
# 
# Koko can decide her bananas-per-hour eating speed of k. Each hour, she chooses some pile of bananas 
# and eats k bananas from that pile. If the pile has less than k bananas, she eats all of them 
# instead and will not eat any more bananas during this hour.
# 
# Koko likes to eat slowly but still wants to finish eating all the bananas before the guards return.
# 
# Return the minimum integer k such that she can eat all the bananas within h hours.
# 
# Example 1:
# 
# Input: piles = [3,6,7,11], h = 8
# Output: 4
# 
# Example 2:
# 
# Input: piles = [30,11,23,4,20], h = 5
# Output: 30
# 
# Example 3:
# 
# Input: piles = [30,11,23,4,20], h = 6
# Output: 23
# 
# Constraints:
# 
#   1 <= piles.length <= 104
#   piles.length <= h <= 109
#   1 <= piles[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def minEatingSpeed(self, piles: List[int], h: int) -> int:
        import math

        def binary_search(left: int, right: int, fn: callable) -> int:
            while left < right:
                mid = left + right >> 1
                left, right = (mid + 1, right) if fn(mid) else (left, mid)
            return left

        def condition(mid: int) -> bool:
            return not sum(math.ceil(pile / mid) for pile in piles) <= h

        return binary_search(1, max(piles), condition)


def test():
    numbers = [
        ([3, 6, 7, 11], 8),
        ([30, 11, 23, 4, 20], 5),
        ([30, 11, 23, 4, 20], 6),
    ]
    expectations = [4, 30, 23]
    for (piles, h), expected in zip(numbers, expectations):
        solution = Solution().minEatingSpeed(piles, h)
        assert solution == expected
