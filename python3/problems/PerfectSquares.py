### Source : https://leetcode.com/problems/perfect-squares/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given an integer n, return the least number of perfect square numbers that sum to n.
# 
# A perfect square is an integer that is the square of an integer; in other words, it is the product 
# of some integer with itself. For example, 1, 4, 9, and 16 are perfect squares while 3 and 11 are 
# not.
# 
# Example 1:
# 
# Input: n = 12
# Output: 3
# Explanation: 12 = 4 + 4 + 4.
# 
# Example 2:
# 
# Input: n = 13
# Output: 2
# Explanation: 13 = 4 + 9.
# 
# Constraints:
# 
# 	1 <= n <= 104
#####################################################################################################


class Solution:
    def numSquares(self, n: int) -> int:
        nums = [0]
        while len(nums) <= n:
            nums.append(min(nums[-i * i] for i in range(1, int(len(nums)**0.5 + 1))) + 1)
        return nums[n]

    def numSquares(self, n: int) -> int:
        # Lagrange's four-square theorem:
        # every natural number can be written as: n = a^2 + b^2 + c^2 + d^2
        nums, i = set(), 1
        while i ** 2 <= n:
            nums.add(i ** 2)
            i += 1

        if n in nums:
            return 1

        for b in nums:
            if n - b in nums:
                return 2

        for b in nums:
            for c in nums:
                if n - b - c in nums:
                    return 3

        return 4

    def numSquares(self, n: int) -> int:
        squares = {i**2 for i in range(int(n ** 0.5) + 1)}
        return next((3 - (i == 0) - (j == 0) for i in squares for j in squares if n - i - j in squares), 4)


def test():
    numbers = [12, 13]
    expectations = [3, 2]
    for n, expected in zip(numbers, expectations):
        solution = Solution().numSquares(n)
        assert solution == expected

