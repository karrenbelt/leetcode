### Source : https://leetcode.com/problems/time-based-key-value-store/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-23

##################################################################################################### 
#
# Design a time-based key-value data structure that can store multiple values for the same key at 
# different time stamps and retrieve the key's value at a certain timestamp.
# 
# Implement the Timeap class:
# 
# 	Timeap() Initializes the object of the data structure.
# 	void set(String key, String value, int timestamp) Stores the key key with the value value 
# at the given time timestamp.
# 	String get(String key, int timestamp) Returns a value such that set was called previously, 
# with timestamp_prev <= timestamp. If there are multiple such values, it returns the value 
# associated with the largest timestamp_prev. If there are no values, it returns "".
# 
# Example 1:
# 
# Input
# ["Timeap", "set", "get", "get", "set", "get", "get"]
# [[], ["foo", "bar", 1], ["foo", 1], ["foo", 3], ["foo", "bar2", 4], ["foo", 4], ["foo", 5]]
# Output
# [null, null, "bar", "bar", null, "bar2", "bar2"]
# 
# Explanation
# Timeap timeap = new Timeap();
# timeap.set("foo", "bar", 1);  // store the key "foo" and value "bar" along with timestamp = 1.
# timeap.get("foo", 1);         // return "bar"
# timeap.get("foo", 3);         // return "bar", since there is no value corresponding to foo at 
# timestamp 3 and timestamp 2, then the only value is at timestamp 1 is "bar".
# timeap.set("foo", "bar2", 4); // store the key "foo" and value "ba2r" along with timestamp = 4.
# timeap.get("foo", 4);         // return "bar2"
# timeap.get("foo", 5);         // return "bar2"
# 
# Constraints:
# 
# 	1 <= key.length, value.length <= 100
# 	key and value consist of lowercase English letters and digits.
# 	1 <= timestamp <= 107
# 	All the timestamps timestamp of set are strictly increasing.
# 	At most 2 * 105 calls will be made to set and get.
#####################################################################################################


class TimeMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.hashmap = {}

    def set(self, key: str, value: str, timestamp: int) -> None:  # O(1)
        self.hashmap.setdefault(key, []).append((timestamp, value))

    def get(self, key: str, timestamp: int) -> str:  # O(1) look-up and O(n log n) search in the values
        import bisect
        values = self.hashmap.get(key, [(0, "")])
        i = bisect.bisect_right(values, (timestamp, chr(ord('z') + 1)))  # cannot use an empty string!
        return values[i - 1][1] if i else ""


# Your TimeMap object will be instantiated and called as such:
# obj = TimeMap()
# obj.set(key,value,timestamp)
# param_2 = obj.get(key,timestamp)


def test():
    operations = ["TimeMap", "set", "get", "get", "set", "get", "get"]
    arguments = [[], ["foo", "bar", 1], ["foo", 1], ["foo", 3], ["foo", "bar2", 4], ["foo", 4], ["foo", 5]]
    expectations = [None, None, "bar", "bar", None, "bar2", "bar2"]

    operations = ["TimeMap", "set", "set", "get", "get", "get", "get", "get"]
    arguments = [[], ["love", "high", 10], ["love", "low", 20], ["love", 5], ["love", 10], ["love", 15], ["love", 20],
                 ["love", 25]]
    expectations = [None, None, None, "", "high", "high", "low", "low"]

    self = obj = TimeMap(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
    assert obj.get('non-existing ', 1) == ""


test()
