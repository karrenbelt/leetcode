### Source : https://leetcode.com/problems/coin-change/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-11

##################################################################################################### 
#
# You are given coins of different denominations and a total amount of money amount. Write a function 
# to compute the fewest number of coins that you need to make up that amount. If that amount of money 
# cannot be made up by any combination of the coins, return -1.
# 
# You may assume that you have an infinite number of each kind of coin.
# 
# Example 1:
# 
# Input: coins = [1,2,5], amount = 11
# Output: 3
# Explanation: 11 = 5 + 5 + 1
# 
# Example 2:
# 
# Input: coins = [2], amount = 3
# Output: -1
# 
# Example 3:
# 
# Input: coins = [1], amount = 0
# Output: 0
# 
# Example 4:
# 
# Input: coins = [1], amount = 1
# Output: 1
# 
# Example 5:
# 
# Input: coins = [1], amount = 2
# Output: 2
# 
# Constraints:
# 
# 	1 <= coins.length <= 12
# 	1 <= coins[i] <= 231 - 1
# 	0 <= amount <= 104
#####################################################################################################

from typing import List


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:  # O(len(coins) * amount)
        dp = [amount + 1] * (amount + 1)
        dp[0] = 0
        for coin in coins:
            for i in range(1, amount + 1):
                if i >= coin:
                    dp[i] = min(dp[i], dp[i-coin] + 1)
        return -1 if dp[amount] == amount + 1 else dp[amount]

    def coinChange(self, coins: List[int], amount: int) -> int:
        if not amount or not coins:
            return 0
        queue = [[0, 0]]
        visited = {0}
        for node, step in queue:
            for coin in coins:
                if node + coin in visited:
                    continue
                if node + coin == amount:
                    return step + 1
                elif node + coin < amount:
                    queue.append([node + coin, step + 1])
                    visited.add(node + coin)
        return -1


def test():
    arguments = [
        ([1, 2, 5], 11),
        ([2], 3),
        ([1], 0),
        ([1], 1),
        ([1], 2),
        ([1, 9, 10], 18),
        ]
    expectations = [3, -1, 0, 1, 2, 2]
    for (coins, amount), expected in zip(arguments, expectations):
        solution = Solution().coinChange(coins, amount)
        assert solution == expected
