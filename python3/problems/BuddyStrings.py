### Source : https://leetcode.com/problems/buddy-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# Given two strings A and B of lowercase letters, return true if you can swap two letters in A so the 
# result is equal to B, otherwise, return false.
# 
# Swapping letters is defined as taking two indices i and j (0-indexed) such that i != j and swapping 
# the characters at A[i] and A[j]. For example, swapping at indices 0 and 2 in "abcd" results in 
# "cbad".
# 
# Example 1:
# 
# Input: A = "ab", B = "ba"
# Output: true
# Explanation: You can swap A[0] = 'a' and A[1] = 'b' to get "ba", which is equal to B.
# 
# Example 2:
# 
# Input: A = "ab", B = "ab"
# Output: false
# Explanation: The only letters you can swap are A[0] = 'a' and A[1] = 'b', which results in "ba" != 
# B.
# 
# Example 3:
# 
# Input: A = "aa", B = "aa"
# Output: true
# Explanation: You can swap A[0] = 'a' and A[1] = 'a' to get "aa", which is equal to B.
# 
# Example 4:
# 
# Input: A = "aaaaaaabc", B = "aaaaaaacb"
# Output: true
# 
# Example 5:
# 
# Input: A = "", B = "aa"
# Output: false
# 
# Constraints:
# 
# 	0 <= A.length <= 20000
# 	0 <= B.length <= 20000
# 	A and B consist of lowercase letters.
#####################################################################################################


class Solution:
    def buddyStrings(self, A: str, B: str) -> bool:
        if len(A) != len(B):
            return False

        if A == B and len(A) != len(set(A)):
            return True

        idx = []
        for i in range(len(A)):
            if A[i] != B[i]:
                idx.append(i)
                if len(idx) > 2:
                    return False

        if len(idx) != 2:
            return False

        return A == B[:idx[0]] + B[idx[1]] + B[idx[0]+1: idx[1]] + B[idx[0]] + B[idx[1]+1:]

    def buddyStrings(self, A: str, B: str) -> bool:
        if len(A) != len(B):
            return False
        if A == B and len(set(A)) < len(A):
            return True
        dif = [(a, b) for a, b in zip(A, B) if a != b]
        return len(dif) == 2 and dif[0] == dif[1][::-1]


def test():
    arguments = [
        ("ab", "ba"),
        ("ab", "ab"),
        ("aa", "aa"),
        ("aaaaaaabc", "aaaaaaacb"),
    ]
    expectations = [True, False, True, True]
    for (A, B), expected in zip(arguments, expectations):
        solution = Solution().buddyStrings(A, B)
        assert solution == expected
