### Source : https://leetcode.com/problems/product-of-array-except-self/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-15

##################################################################################################### 
#
# Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal 
# to the product of all the elements of nums except nums[i].
# 
# Example:
# 
# Input:  [1,2,3,4]
# Output: [24,12,8,6]
# 
# Constraint: It's guaranteed that the product of the elements of any prefix or suffix of the array 
# (including the whole array) fits in a 32 bit integer.
# 
# Note: Please solve it without division and in O(n).
# 
# Follow up:
# Could you solve it with constant space complexity? (The output array does not count as extra space 
# for the purpose of space complexity analysis.)
#####################################################################################################

from typing import List


class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        # the best we can do is iterate twice, left to right and right to left
        # then on the second iteration start at n-1 to not multiple by self
        n = len(nums)
        output = [0] * n
        p = 1
        for i in range(n):
            output[i] = p
            p *= nums[i]

        p = 1
        for i in range(n - 1, -1, -1):
            output[i] *= p
            p *= nums[i]

        return output

    def productExceptSelf(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        products = [0] * len(nums)

        prefix = 1
        for i, n in enumerate(nums):
            products[i] = prefix
            prefix *= n

        prefix = 1
        for i, n in enumerate(reversed(nums)):
            products[~i] *= prefix
            prefix *= n

        return products

    def productExceptSelf(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        products = [1] * len(nums)
        left = right = 1
        for i in range(len(nums)):
            products[i] *= left
            products[~i] *= right
            left *= nums[i]
            right *= nums[~i]
        return products


def test():
    arrays_of_numbers = [
        [1, 2, 3, 4],
        [-1, 1, 0, -3, 3],
        ]
    expectations = [
        [24, 12, 8, 6],
        [0, 0, 9, 0, 0],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().productExceptSelf(nums)
        assert solution == expected
