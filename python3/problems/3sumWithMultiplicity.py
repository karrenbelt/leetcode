### Source : https://leetcode.com/problems/3sum-with-multiplicity/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-23

##################################################################################################### 
#
# Given an integer array arr, and an integer target, return the number of tuples i, j, k such that i 
# < j < k and arr[i] + arr[j] + arr[k] == target.
# 
# As the answer can be very large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
# Output: 20
# Explanation: 
# Enumerating by the values (arr[i], arr[j], arr[k]):
# (1, 2, 5) occurs 8 times;
# (1, 3, 4) occurs 8 times;
# (2, 2, 4) occurs 2 times;
# (2, 3, 3) occurs 2 times.
# 
# Example 2:
# 
# Input: arr = [1,1,2,2,2,2], target = 5
# Output: 12
# Explanation: 
# arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
# We choose one 1 from [1,1] in 2 ways,
# and two 2s from [2,2,2,2] in 6 ways.
# 
# Constraints:
# 
# 	3 <= arr.length <= 3000
# 	0 <= arr[i] <= 100
# 	0 <= target <= 300
#####################################################################################################

from typing import List


class Solution:
    def threeSumMulti(self, arr: List[int], target: int) -> int:  # O(n^2) time and O(n) space
        ans = 0
        counts = {}
        for i in range(2, len(arr)):
            for j in range(i - 1):
                two_sum = arr[j] + arr[i - 1]
                counts[two_sum] = counts.get(two_sum, 0) + 1
            ans += counts.get(target - arr[i], 0)
        return ans % (10**9 + 7)

    def threeSumMulti(self, arr: List[int], target: int) -> int:  # O(n^2) time and O(n^2) space, >10x faster
        single = {}
        double = {}
        ans = 0
        for n in arr:
            ans += double.get(target - n, 0)
            for k, v in single.items():
                double[k + n] = double.get(k + n, 0) + v
            single[n] = single.get(n, 0) + 1
        return ans % (10**9 + 7)


def test():
    arguments = [
        ([1, 1, 2, 2, 3, 3, 4, 4, 5, 5], 8),
        ([1, 1, 2, 2, 2, 2], 5)
        ]
    expectations = [20, 12]
    for (arr, target), expected in zip(arguments, expectations):
        solution = Solution().threeSumMulti(arr, target)
        assert solution == expected
