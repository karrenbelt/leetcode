### Source : https://leetcode.com/problems/minimum-number-of-frogs-croaking/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-02

##################################################################################################### 
#
# Given the string croakOfFrogs, which represents a combination of the string "croak" from different 
# frogs, that is, multiple frogs can croak at the same time, so multiple "croak&rdquo; are mixed. 
# Return the minimum number of different frogs to finish all the croak in the given string.
# 
# A valid "croak" means a frog is printing 5 letters &lsquo;c&rsquo;, &rsquo;r&rsquo;, 
# &rsquo;o&rsquo;, &rsquo;a&rsquo;, &rsquo;k&rsquo; sequentially. The frogs have to print all five 
# letters to finish a croak. If the given string is not a combination of valid "croak" return -1.
# 
# Example 1:
# 
# Input: croakOfFrogs = "croakcroak"
# Output: 1 
# Explanation: One frog yelling "croak" twice.
# 
# Example 2:
# 
# Input: croakOfFrogs = "crcoakroak"
# Output: 2 
# Explanation: The minimum number of frogs is two. 
# The first frog could yell "crcoakroak".
# The second frog could yell later "crcoakroak".
# 
# Example 3:
# 
# Input: croakOfFrogs = "croakcrook"
# Output: -1
# Explanation: The given string is an invalid combination of "croak" from different frogs.
# 
# Example 4:
# 
# Input: croakOfFrogs = "croakcroa"
# Output: -1
# 
# Constraints:
# 
# 	1 <= croakOfFrogs.length <= 105
# 	All characters in the string are: 'c', 'r', 'o', 'a' or 'k'.
#####################################################################################################


class Solution:
    def minNumberOfFrogs(self, croakOfFrogs: str) -> int:  # 636 ms
        mapping = dict(zip('croak', range(5)))
        croak = [0] * 5
        active = minimum = 0
        for c in croakOfFrogs:
            active += 1 if c == 'c' else -1 if c == 'k' else 0
            croak[mapping[c]] += 1
            minimum = max(minimum, active)
            if any(a < b for a, b in zip(croak, croak[1:])):
                return -1
        return minimum if not active and len(set(croak)) == 1 else -1

    def minNumberOfFrogs(self, croakOfFrogs: str) -> int:  # 136 ms
        minimum = c = r = o = a = k = 0
        for ch in croakOfFrogs:
            if ch == 'c':
                c += 1
                minimum = max(minimum, c - k)
            elif ch == 'r':
                r += 1
            elif ch == 'o':
                o += 1
            elif ch == 'a':
                a += 1
            else:
                k += 1
            if not c >= r >= o >= a >= k:
                return -1
        return minimum if c == k else -1


def test():
    arguments = [
        "croakcroak",
        "crcoakroak",
        "croakcrook",
        "croakcroa",
    ]
    expectations = [1, 2, -1, -1]
    for croakOfFrogs, expected in zip(arguments, expectations):
        solution = Solution().minNumberOfFrogs(croakOfFrogs)
        assert solution == expected
