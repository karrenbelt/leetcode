### Source : https://leetcode.com/problems/add-two-numbers-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-14

##################################################################################################### 
#
# You are given two non-empty linked lists representing two non-negative integers. The most 
# significant digit comes first and each of their nodes contains a single digit. Add the two numbers 
# and return the sum as a linked list.
# 
# You may assume the two numbers do not contain any leading zero, except the number 0 itself.
# 
# Example 1:
# 
# Input: l1 = [7,2,4,3], l2 = [5,6,4]
# Output: [7,8,0,7]
# 
# Example 2:
# 
# Input: l1 = [2,4,3], l2 = [5,6,4]
# Output: [8,0,7]
# 
# Example 3:
# 
# Input: l1 = [0], l2 = [0]
# Output: [0]
# 
# Constraints:
# 
# 	The number of nodes in each linked list is in the range [1, 100].
# 	0 <= Node.val <= 9
# 	It is guaranteed that the list represents a number that does not have leading zeros.
# 
# Follow up: Could you solve it without reversing the input lists?
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(m + n) time and O(m + n) space

        def to_digit(head: ListNode) -> int:
            stack, node = [], head
            while node:
                stack.append(node.val)
                node = node.next
            return int(''.join(map(str, stack)))

        def to_node(digits: int) -> ListNode:
            summed = list(map(int, reversed(str(digits))))
            head = node = ListNode(summed.pop())
            while summed:
                node.next = ListNode(summed.pop())
                node = node.next
            return head

        return to_node(to_digit(l1) + to_digit(l2))


def test():
    from python3 import SinglyLinkedList
    arguments = [
        ([7, 2, 4, 3], [5, 6, 4]),
        ([2, 4, 3], [5, 6, 4]),
        ([0], [0]),
        ]
    expectations = [
        [7, 8, 0, 7],
        [8, 0, 7],
        [0],
    ]
    for (nums1, nums2), expected in zip(arguments, expectations):
        l1, l2 = SinglyLinkedList(nums1).head, SinglyLinkedList(nums2).head
        solution = Solution().addTwoNumbers(l1, l2)
        assert solution == SinglyLinkedList(expected).head
