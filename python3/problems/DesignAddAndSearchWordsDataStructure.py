### Source : https://leetcode.com/problems/design-add-and-search-words-data-structure/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-03

##################################################################################################### 
#
# Design a data structure that supports adding new words and finding if a string matches any 
# previously added string.
# 
# Implement the WordDictionary class:
# 
# 	WordDictionary() Initializes the object.
# 	void addWord(word) Adds word to the data structure, it can be matched later.
# 	bool search(word) Returns true if there is any string in the data structure that matches 
# word or false otherwise. word may contain dots '.' where dots can be matched with any letter.
# 
# Example:
# 
# Input
# ["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
# [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
# Output
# [null,null,null,null,false,true,true,true]
# 
# Explanation
# WordDictionary wordDictionary = new WordDictionary();
# wordDictionary.addWord("bad");
# wordDictionary.addWord("dad");
# wordDictionary.addWord("mad");
# wordDictionary.search("pad"); // return False
# wordDictionary.search("bad"); // return True
# wordDictionary.search(".ad"); // return True
# wordDictionary.search("b.."); // return True
# 
# Constraints:
# 
# 	1 <= word.length <= 500
# 	word in addWord consists lower-case English letters.
# 	word in search consist of  '.' or lower-case English letters.
# 	At most 50000 calls will be made to addWord and search.
#####################################################################################################


class WordDictionary:

    def __init__(self):
        self.trie = {}

    def addWord(self, word: str) -> None:
        node = self.trie
        for c in word:
            if c not in node:
                node[c] = dict()
            node = node[c]
        node['.'] = None  # not super clear since this is wild card in search now, but w/e

    def search(self, word: str) -> bool:

        def dfs(node, word):
            nonlocal is_a_word
            if node is None:
                return
            if not word:
                if '.' in node:
                    is_a_word = True
            elif word[0] == ".":
                any(dfs(n, word[1:]) for n in node.values())
            else:
                dfs(node.get(word[0]), word[1:])

        is_a_word = False
        dfs(self.trie, word)
        return is_a_word

# Your WordDictionary object will be instantiated and called as such:
# obj = WordDictionary()
# obj.addWord(word)
# param_2 = obj.search(word)


def test():
    null = None
    operations = ["WordDictionary", "addWord", "addWord", "addWord", "search", "search", "search", "search"]
    words = [[], ["bad"], ["dad"], ["mad"], ["pad"], ["bad"], [".ad"], ["b.."]]
    expectations = [null, null, null, null, False, True, True, True]
    dictionary = WordDictionary()
    for operation, word, expected in zip(operations[1:], words[1:], expectations[1:]):
        print(word, expected)
        assert getattr(dictionary, operation)(word) == expected
