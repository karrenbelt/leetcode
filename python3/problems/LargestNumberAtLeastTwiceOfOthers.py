### Source : https://leetcode.com/problems/largest-number-at-least-twice-of-others/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# You are given an integer array nums where the largest integer is unique.
# 
# Determine whether the largest element in the array is at least twice as much as every other number 
# in the array. If it is, return the index of the largest element, or return -1 otherwise.
# 
# Example 1:
# 
# Input: nums = [3,6,1,0]
# Output: 1
# Explanation: 6 is the largest integer.
# For every other number in the array x, 6 is at least twice as big as x.
# The index of value 6 is 1, so we return 1.
# 
# Example 2:
# 
# Input: nums = [1,2,3,4]
# Output: -1
# Explanation: 4 is less than twice the value of 3, so we return -1.
# 
# Example 3:
# 
# Input: nums = [1]
# Output: 0
# Explanation: 1 is trivially at least twice the value as any other number because there are no other 
# numbers.
# 
# Constraints:
# 
# 	1 <= nums.length <= 50
# 	0 <= nums[i] <= 100
# 	The largest element in nums is unique.
#####################################################################################################

from typing import List


class Solution:
    def dominantIndex(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        # need to find the two largest numbers and compare them
        if not nums:
            return -1
        if len(nums) == 1:
            return 0
        a, b = sorted(set(nums))[-2:]
        return nums.index(b) if b >= a*2 else -1

    def dominantIndex(self, nums: List[int]) -> int:  # O(n log t) time and O(1) space
        import heapq
        if len(nums) < 2:
            return len(nums) - 1
        b, a = heapq.nlargest(2, nums)
        return nums.index(b) if b >= a*2 else -1

    def dominantIndex(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        a = b = -1
        for n in nums:
            if n >= b:
                a, b = b, n
            elif n >= a:
                a = n
        return len(nums) - 1 if len(nums) < 2 else nums.index(b) if a*2 <= b else -1


def test():
    arguments = [
        [3, 6, 1, 0],
        [1, 2, 3, 4],
        [1],
        [0, 0, 3, 2],
    ]
    expectations = [1, -1, 0, -1]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().dominantIndex(nums)
        assert solution == expected
test()