### Source : https://leetcode.com/problems/multiply-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given two non-negative integers num1 and num2 represented as strings, return the product of num1 
# and num2, also represented as a string.
# 
# Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.
# 
# Example 1:
# Input: num1 = "2", num2 = "3"
# Output: "6"
# Example 2:
# Input: num1 = "123", num2 = "456"
# Output: "56088"
# 
# Constraints:
# 
# 	1 <= num1.length, num2.length <= 200
# 	num1 and num2 consist of digits only.
# 	Both num1 and num2 do not contain any leading zero, except the number 0 itself.
#####################################################################################################


class Solution:
    def multiply(self, num1: str, num2: str) -> str:  # O(n + m) time and O(n + m) space
        if num1 == "0" or num2 == "0":
            return "0"
        answer = [0] * (len(num1) + len(num2))
        for i, n1 in enumerate(reversed(num1)):
            for j, n2 in enumerate(reversed(num2)):
                answer[i + j] += (ord(n1) - ord('0')) * (ord(n2) - ord('0'))
                answer[i + j + 1] += answer[i + j] // 10
                answer[i + j] %= 10
        while len(answer) > 1 and answer[-1] == 0:
            answer.pop()
        return ''.join(str(v) for v in answer)[::-1]


def test():
    arguments = [
        ("2", "3"),
        ("123", "456"),
        ]
    expectations = ["6", "56088"]
    for (num1, num2), expected in zip(arguments, expectations):
        solution = Solution().multiply(num1, num2)
        assert solution == expected
