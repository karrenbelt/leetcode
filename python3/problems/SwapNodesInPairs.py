### Source : https://leetcode.com/problems/swap-nodes-in-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-21

##################################################################################################### 
#
# Given a linked list, swap every two adjacent nodes and return its head.
# 
# Example 1:
# 
# Input: head = [1,2,3,4]
# Output: [2,1,4,3]
# 
# Example 2:
# 
# Input: head = []
# Output: []
# 
# Example 3:
# 
# Input: head = [1]
# Output: [1]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 100].
# 	0 <= Node.val <= 100
# 
# Follow up: Can you solve the problem without modifying the values in the list's nodes? (i.e., Only 
# nodes themselves may be changed.)
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def swapPairs(self, head: ListNode) -> ListNode:  # O(n) time and O(1) space
        if head and head.next:
            head.val, head.next.val = head.next.val, head.val
            self.swapPairs(head.next.next)
        return head

    def swapPairs(self, head):
        dummy = pre = ListNode(None)
        pre.next = head
        while pre.next and pre.next.next:
            a = pre.next
            b = a.next
            pre.next, a.next, b.next = b, b.next, a
            pre = a
        return dummy.next


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        [1, 2, 3, 4],
        [],
        [1]
        ]
    expectations = [
        [2, 1, 4, 3],
        [],
        [1]
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        head = singly_linked_list_from_array(nums).head
        solution = Solution().swapPairs(head)
        assert solution == singly_linked_list_from_array(expected).head
