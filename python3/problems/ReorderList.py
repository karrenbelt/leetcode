### Source : https://leetcode.com/problems/reorder-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-03

##################################################################################################### 
#
# Given a singly linked list L: L0&rarr;L1&rarr;&hellip;&rarr;Ln-1&rarr;Ln,
# reorder it to: L0&rarr;Ln&rarr;L1&rarr;Ln-1&rarr;L2&rarr;Ln-2&rarr;&hellip;
# 
# You may not modify the values in the list's nodes, only nodes itself may be changed.
# 
# Example 1:
# 
# Given 1->2->3->4, reorder it to 1->4->2->3.
# 
# Example 2:
# 
# Given 1->2->3->4->5, reorder it to 1->5->2->4->3.
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def reorderList(self, head: ListNode) -> None:  # O(n) time, O(n) space
        """
        Do not return anything, modify head in-place instead.
        """
        from collections import deque

        if not head:
            return

        node = head
        nodes = deque()
        while node.next:
            nodes.append(node.next)
            node = node.next

        i = 0
        node = head
        while nodes:
            if i % 2:
                node.next = nodes.popleft()
            else:
                node.next = nodes.pop()
            node = node.next
            i += 1
        node.next = None
        return head

    def reorderList(self, head: ListNode) -> None:  # O(n) time O(1) space
        if not head:
            return

        # find mid point
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        # reverse second half
        prev = None
        curr = slow
        while curr:
            curr.next, prev, curr = prev, curr, curr.next

        # merge sorted lists
        first, second = head, prev
        while second.next:
            first.next, first = second, first.next
            second.next, second = first, second.next


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [1, 2, 3, 4],
        [1, 2, 3, 4, 5],
        ]
    expectations = [
        [1, 4, 2, 3],
        [1, 5, 2, 4, 3],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        ll = SinglyLinkedList(nums)
        Solution().reorderList(ll.head)
        assert ll == SinglyLinkedList(expected)
        # llist = SinglyLinkedList()
        # llist._head = solution

