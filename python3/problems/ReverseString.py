### Source : https://leetcode.com/problems/reverse-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Write a function that reverses a string. The input string is given as an array of characters char[].
# 
# Do not allocate extra space for another array, you must do this by modifying the input array 
# in-place with O(1) extra memory.
# 
# You may assume all the characters consist of printable ascii characters.
# 
# Example 1:
# 
# Input: ["h","e","l","l","o"]
# Output: ["o","l","l","e","h"]
# 
# Example 2:
# 
# Input: ["H","a","n","n","a","h"]
# Output: ["h","a","n","n","a","H"]
# 
#####################################################################################################

from typing import List


class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        for i in range(len(s) // 2):
            s[i], s[-(i + 1)] = s[-(i + 1)], s[i]

    def reverseString(self, s: List[str]) -> None:
        for i in range(len(s) >> 1):
            s[i], s[~i] = s[~i], s[i]

    def reverseString(self, s: List[str]) -> None:
        s[:] = s[::-1]

    def reverseString(self, s: List[str]) -> None:
        s.reverse()  # not the exercise of course


def test():
    string_arrays = [
        ["h", "e", "l", "l", "o"],
        ["H", "a", "n", "n", "a", "h"],
        ]
    expectations = [
        ["o", "l", "l", "e", "h"],
        ["h", "a", "n", "n", "a", "H"],
        ]
    for s, expected in zip(string_arrays, expectations):
        Solution().reverseString(s)
        assert s == expected
