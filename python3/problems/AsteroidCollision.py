### Source : https://leetcode.com/problems/asteroid-collision/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-02

##################################################################################################### 
#
# We are given an array asteroids of integers representing asteroids in a row.
# 
# For each asteroid, the absolute value represents its size, and the sign represents its direction 
# (positive meaning right, negative meaning left). Each asteroid moves at the same speed.
# 
# Find out the state of the asteroids after all collisions. If two asteroids meet, the smaller one 
# will explode. If both are the same size, both will explode. Two asteroids moving in the same 
# direction will never meet.
# 
# Example 1:
# 
# Input: asteroids = [5,10,-5]
# Output: [5,10]
# Explanation: The 10 and -5 collide resulting in 10. The 5 and 10 never collide.
# 
# Example 2:
# 
# Input: asteroids = [8,-8]
# Output: []
# Explanation: The 8 and -8 collide exploding each other.
# 
# Example 3:
# 
# Input: asteroids = [10,2,-5]
# Output: [10]
# Explanation: The 2 and -5 collide resulting in -5. The 10 and -5 collide resulting in 10.
# 
# Example 4:
# 
# Input: asteroids = [-2,-1,1,2]
# Output: [-2,-1,1,2]
# Explanation: The -2 and -1 are moving left, while the 1 and 2 are moving right. Asteroids moving 
# the same direction never meet, so no asteroids will meet each other.
# 
# Constraints:
# 
# 	2 <= asteroids.length <= 104
# 	-1000 <= asteroids[i] <= 1000
# 	asteroids[i] != 0
#####################################################################################################

from typing import List


class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:  # O(n) time and O(n) space
        final_state = []
        for asteroid in asteroids:
            if asteroid > 0:
                final_state.append(asteroid)
            else:
                while final_state and 0 < final_state[-1] < abs(asteroid):
                    final_state.pop()
                if not final_state or final_state[-1] < 0:
                    final_state.append(asteroid)
                elif final_state[-1] == -asteroid:
                    final_state.pop()
        return final_state


def test():
    arguments = [
        [5, 10, -5],
        [8, -8],
        [10, 2, -5],
        [-2, -1, 1, 2],
        [-2, -2, -2, 1],
        [-2, -2, 1, -2],
    ]
    expectations = [
        [5, 10],
        [],
        [10],
        [-2, -1, 1, 2],
        [-2, -2, -2, 1],
        [-2, -2, -2]
    ]
    for asteroids, expected in zip(arguments, expectations):
        solution = Solution().asteroidCollision(asteroids)
        assert solution == expected
