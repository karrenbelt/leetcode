### Source : https://leetcode.com/problems/find-target-indices-after-sorting-array/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-23

##################################################################################################### 
#
# You are given a 0-indexed integer array nums and a target element target.
# 
# A target index is an index i such that nums[i] == target.
# 
# Return a list of the target indices of nums after sorting nums in non-decreasing order. If there 
# are no target indices, return an empty list. The returned list must be sorted in increasing order.
# 
# Example 1:
# 
# Input: nums = [1,2,5,2,3], target = 2
# Output: [1,2]
# Explanation: After sorting, nums is [1,2,2,3,5].
# The indices where nums[i] == 2 are 1 and 2.
# 
# Example 2:
# 
# Input: nums = [1,2,5,2,3], target = 3
# Output: [3]
# Explanation: After sorting, nums is [1,2,2,3,5].
# The index where nums[i] == 3 is 3.
# 
# Example 3:
# 
# Input: nums = [1,2,5,2,3], target = 5
# Output: [4]
# Explanation: After sorting, nums is [1,2,2,3,5].
# The index where nums[i] == 5 is 4.
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	1 <= nums[i], target <= 100
#####################################################################################################

from typing import List


class Solution:
    def targetIndices(self, nums: List[int], target: int) -> List[int]:  # O(n log n)
        return [i for i, n in enumerate(sorted(nums)) if n == target]

    def targetIndices(self, nums: List[int], target: int) -> List[int]:  # O(n)
        lower = higher = 0
        for n in nums:
            lower += n < target
            higher += n > target
        return list(range(lower, len(nums) - higher))

    def targetIndices(self, nums: List[int], target: int) -> List[int]:  # O(n)
        low, eq = map(sum, (zip(*((n < target, n == target) for n in nums))))
        return list(range(low, low + eq))


def test():
    arguments = [
        ([1, 2, 5, 2, 3], 2),
        ([1, 2, 5, 2, 3], 3),
        ([1, 2, 5, 2, 3], 5),
    ]
    expectations = [
        [1, 2],
        [3],
        [4],
    ]
    for (nums, target), expected in zip(arguments, expectations):
        solution = Solution().targetIndices(nums, target)
        assert solution == expected
