### Source : https://leetcode.com/problems/unique-binary-search-trees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Given an integer n, return the number of structurally unique BST's (binary search trees) which has 
# exactly n nodes of unique values from 1 to n.
# 
# Example 1:
# 
# Input: n = 3
# Output: 5
# 
# Example 2:
# 
# Input: n = 1
# Output: 1
# 
# Constraints:
# 
# 	1 <= n <= 19
#####################################################################################################


class Solution:
    # (1, 2, 5, 14, 42, ...)
    # (2n)! / ((n+1)! * n!)
    def numTrees(self, n: int) -> int:  # naive O(n^2)  --> TLE

        def naive_recursion(n):
            return sum([naive_recursion(i - 1) * naive_recursion(n - i) for i in range(1, n + 1)]) if n else 1

        return naive_recursion(n)

    def numTrees(self, n: int) -> int:  # naive O(n^2)
        from functools import lru_cache

        @lru_cache()
        def naive_recursion(n: int):
            return sum(naive_recursion(i - 1) * naive_recursion(n - i) for i in range(1, n + 1)) if n else 1

        return naive_recursion(n)

    def numTrees(self, n: int) -> int:  # dp: O(n ^ 2) time and O(n) space
        dp = [0] * (n + 1)
        dp[0] = dp[1] = 1
        for i in range(2, len(dp)):
            for j in range(1, i + 1):
                dp[i] += dp[j - 1] * dp[i - j]
        return dp[-1]

    def numTrees(self, n: int) -> int:  # Catalan numbers: O(n) time and O(1) space
        from operator import mul
        from functools import reduce, lru_cache

        @lru_cache()
        def factorial(n: int) -> int:
            return reduce(mul, range(n, 1, -1)) if n > 1 else n

        return factorial(2 * n) // (factorial(n + 1) * factorial(n))

    def numTrees(self, n: int) -> int:  # Catalan numbers: O(n) time and O(1) space
        from math import factorial
        return factorial(2 * n) // (factorial(n + 1) * factorial(n))

    def numTrees(self, n: int) -> int:  # Catalan numbers: O(n) time and O(1) space
        from functools import reduce
        from operator import mul
        return int(reduce(mul, map(lambda i: (4 * i + 2) / (i + 2), range(n)), 1))


def test():
    numbers = [3, 1, 7]
    expectations = [5, 1, 429]
    for n, expected in zip(numbers, expectations):
        solution = Solution().numTrees(n)
        assert solution == expected
