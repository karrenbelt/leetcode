### Source : https://leetcode.com/problems/group-anagrams/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an array of strings, group anagrams together.
# 
# Example:
# 
# Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
# Output:
# [
#   ["ate","eat","tea"],
#   ["nat","tan"],
#   ["bat"]
# ]
# 
# Note:
# 
# 	All inputs will be in lowercase.
# 	The order of your output does not matter.
# 
#####################################################################################################

import operator
import functools
from typing import List
from collections import Mapping


class FrozenDict(Mapping):
    def __init__(self, *args, **kwargs):
        self._d = dict(*args, **kwargs)
        self._hash = functools.reduce(operator.xor, map(hash, self._d.items()), 0)  # O(n) time

    def __iter__(self):
        return iter(self._d)

    def __len__(self):
        return len(self._d)

    def __getitem__(self, key):
        return self._d[key]

    def __hash__(self):
        return self._hash


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:  # O(n * (k log k)) time
        d = dict()
        for word in strs:
            letters = ''.join(sorted(word))
            if letters in d:
                d[letters].append(word)
            else:
                d[letters] = [word]
        return list(d.values())

    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:  # O(n * (k log k)) time: 94 ms 17.2 MB
        d = dict()
        for word in strs:
            d.setdefault(''.join(sorted(word)), []).append(word)
        return list(d.values())

    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:  # O(n * k) time: 172 ms 21.8 MB
        from collections import Counter
        d = dict()
        for word in strs:
            d.setdefault(FrozenDict(Counter(word).items()), []).append(word)
        return list(d.values())

    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:  # O(n * k) time: 144 ms 27.9 MB
        from collections import Counter
        d = dict()
        for word in strs:
            d.setdefault(frozenset(Counter(word).items()), []).append(word)
        return list(d.values())

    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:  # O(n log n * k log k) time
        from itertools import groupby
        return list(sorted(g) for _, g in groupby(sorted(strs, key=sorted), sorted))


def test():
    strings = [
        ["eat", "tea", "tan", "ate", "nat", "bat"],
        [""],
        ["a"],
        ]
    expectations = [
        [["bat"], ["nat", "tan"], ["ate", "eat", "tea"]],
        [[""]],
        [["a"]],
        ]
    for strs, expected in zip(strings, expectations):
        solution = Solution().groupAnagrams(strs)
        assert sorted(map(sorted, solution)) == sorted(map(sorted, expected))
