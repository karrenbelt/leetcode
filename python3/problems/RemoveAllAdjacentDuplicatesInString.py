### Source : https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

#####################################################################################################
#
# Given a string S of lowercase letters, a duplicate removal consists of choosing two adjacent and
# equal letters, and removing them.
#
# We repeatedly make duplicate removals on S until we no longer can.
#
# Return the final string after all such duplicate removals have been made.  It is guaranteed the
# answer is unique.
#
# Example 1:
#
# Input: "abbaca"
# Output: "ca"
# Explanation:
# For example, in "abbaca" we could remove "bb" since the letters are adjacent and equal, and this is
# the only possible move.  The result of this move is that the string is "aaca", of which only "aa"
# is possible, so the final string is "ca".
#
# Note:
#
# 	1 <= S.length <= 20000
# 	S consists only of English lowercase letters.
#####################################################################################################


class Solution:
    def removeDuplicates(self, S: str) -> str:  # O(n) time and O(n) space
        stack = []
        for c in S:
            if stack and stack[-1] == c:
                stack.pop()
            else:
                stack.append(c)
        return ''.join(stack)

    def removeDuplicates(self, s: str) -> str:  # O(n) time and O(n) space
        stack = []
        for c in s:
            if stack and stack[-1] == c:
                while stack and stack[-1] == c:
                    stack.pop()
            else:
                stack.append(c)
        return ''.join(stack)

    def removeDuplicates(self, s: str) -> str:  # O(n^2) time and O(1) space
        for c in s:
            s = s.replace(c * 2, "") if c * 2 in s else s
        return s

    def removeDuplicates(self, s: str) -> str:  # O(n^2) time and O(n) space : 2136 ms!
        return [s := s.replace(c * 2, "") if c * 2 in s else s for c in s][-1]

    def removeDuplicates(self, s: str) -> str:  # O(n) time and O(n) space
        return (stk := []) or any(not stk.pop() if stk and stk[-1] == c else stk.append(c) for c in s) or "".join(stk)


def test():
    arguments = [
        "abbaca",
        "azxxzy",
    ]
    expectations = ["ca", "ay"]
    for s, expected in zip(arguments, expectations):
        solution = Solution().removeDuplicates(s)
        assert solution == expected
