### Source : https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

#####################################################################################################
#
# Return the lexicographically smallest subsequence of s that contains all the distinct characters of
# s exactly once.
#
# Note: This question is the same as 316: https://leetcode.com/problems/remove-duplicate-letters/
#
# Example 1:
#
# Input: s = "bcabc"
# Output: "abc"
#
# Example 2:
#
# Input: s = "cbacdcbc"
# Output: "acdb"
#
# Constraints:
#
# 	1 <= s.length <= 1000
# 	s consists of lowercase English letters.
#####################################################################################################


class Solution:
    # same as leetcode question 316
    def smallestSubsequence(self, s: str) -> str:
        for c in sorted(set(s)):
            suffix = s[s.index(c):]
            if set(suffix) == set(s):
                return c + self.smallestSubsequence(suffix.replace(c, ""))
        return ""


def test():
    arguments = ["bcabc", "cbacdcbc"]
    expectations = ["abc", "acdb"]
    for s, expected in zip(arguments, expectations):
        solution = Solution().smallestSubsequence(s)
        assert solution == expected
