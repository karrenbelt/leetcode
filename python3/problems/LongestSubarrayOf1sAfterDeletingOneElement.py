### Source : https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-08

##################################################################################################### 
#
# Given a binary array nums, you should delete one element from it.
# 
# Return the size of the longest non-empty subarray containing only 1's in the resulting array.
# 
# Return 0 if there is no such subarray.
# 
# Example 1:
# 
# Input: nums = [1,1,0,1]
# Output: 3
# Explanation: After deleting the number in position 2, [1,1,1] contains 3 numbers with value of 1's.
# 
# Example 2:
# 
# Input: nums = [0,1,1,1,0,1,1,0,1]
# Output: 5
# Explanation: After deleting the number in position 4, [0,1,1,1,1,1,0,1] longest subarray with value 
# of 1's is [1,1,1,1,1].
# 
# Example 3:
# 
# Input: nums = [1,1,1]
# Output: 2
# Explanation: You must delete one element.
# 
# Example 4:
# 
# Input: nums = [1,1,0,0,1,1,1,0,1]
# Output: 4
# 
# Example 5:
# 
# Input: nums = [0,0,0]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	nums[i] is either 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def longestSubarray(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        zero = previous = current = longest = 0
        for n in nums:
            if n == 1:
                current += 1
            else:
                zero = 1
                if current == 0:
                    previous = 0
                longest = max(longest, previous + current)
                previous = current
                current = 0
        return max(longest, previous + current) if zero else current - 1 if current else 0

    def longestSubarray(self, nums: List[int]) -> int:  # counters: O(n) time and O(1) space
        zero = longest = 0
        counts = [0, 0]
        for n in nums:
            if n == 1:
                counts[0] += 1
            else:
                zero = 1
                counts.reverse()
                longest = max(longest, sum(counts))
                counts[0] = 0
        return max(longest, sum(counts)) if zero else len(nums) - 1

    def longestSubarray(self, nums: List[int]) -> int:  # sliding window: O(n) time and O(1) space
        left = length = longest = 0
        for right, num in enumerate(nums):
            length += num
            if length < right - left:
                length -= nums[left]
                left += 1
            longest = max(longest, right - left)
        return longest


def test():
    arguments = [
        [1, 1, 0, 1],
        [0, 1, 1, 1, 0, 1, 1, 0, 1],
        [1, 1, 1],
        [1, 1, 0, 0, 1, 1, 1, 0, 1],
        [0, 0, 0],
        [0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1]
        ]
    expectations = [3, 5, 2, 4, 0, 6]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().longestSubarray(nums)
        assert solution == expected
