### Source : https://leetcode.com/problems/rectangle-area-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-22

##################################################################################################### 
#
# We are given a list of (axis-aligned) rectangles. Each rectangle[i] = [xi1, yi1, xi2, yi2] , where 
# (xi1, yi1) are the coordinates of the bottom-left corner, and (xi2, yi2) are the coordinates of the 
# top-right corner of the ith rectangle.
# 
# Find the total area covered by all rectangles in the plane. Since the answer may be too large, 
# return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: rectangles = [[0,0,2,2],[1,0,2,3],[1,0,3,1]]
# Output: 6
# Explanation: As illustrated in the picture.
# 
# Example 2:
# 
# Input: rectangles = [[0,0,1000000000,1000000000]]
# Output: 49
# Explanation: The answer is 1018 modulo (109 + 7), which is (109)2 = (-7)2 = 49.
# 
# Constraints:
# 
# 	1 <= rectangles.length <= 200
# 	rectanges[i].length = 4
# 	0 <= rectangles[i][j] <= 109
# 	The total area covered by all rectangles will never exceed 263 - 1 and thus will fit in a 
# 64-bit signed integer.
#####################################################################################################

from typing import List


class Solution:
    def rectangleArea(self, rectangles: List[List[int]]) -> int:
        # 1. start with a rectangle that subsumes all other rectangles
        # 2. for each rectangle and the input rectangle, divvy up the rectangle in 0 to 4 new rectangles
        def intersecting_area(r1, c1, r2, c2, x1, y1, x2, y2) -> int:
            return max(0, min(r2, x2) - max(r1, x1)) * max(0, min(c2, y2) - max(c1, y1))

        def cut_rectangle(i, r1, c1, r2, c2) -> int:
            if i >= len(rectangles) or r1 >= r2 or c1 >= c2:
                return 0

            x1, y1, x2, y2 = rectangles[i]
            if x1 >= r2 or y1 >= c2 or x2 <= r1 or y2 <= c1:
                return cut_rectangle(i + 1, r1, c1, r2, c2)

            s1 = cut_rectangle(i + 1, r1, c1, min(x1, r2), c2) if x1 > r1 else 0
            s2 = cut_rectangle(i + 1, max(x2, r1), c1, r2, c2) if x2 < r2 else 0
            s3 = cut_rectangle(i + 1, max(x1, r1), c1, min(x2, r2), y1) if y1 > c1 else 0
            s4 = cut_rectangle(i + 1, max(x1, r1), y2, min(x2, r2), c2) if y2 < c2 else 0
            return s1 + s2 + s3 + s4 + intersecting_area(r1, c1, r2, c2, x1, y1, x2, y2)

        trp = list(zip(*rectangles))
        return cut_rectangle(0, min(trp[0]), min(trp[1]), max(trp[2]), max(trp[3])) % (10 ** 9 + 7)


def test():
    arguments = [
        [[0, 0, 2, 2], [1, 0, 2, 3], [1, 0, 3, 1]],
        [[0, 0, 1000000000, 1000000000]],
        [[93516, 44895, 94753, 69358], [13141, 52454, 59740, 71232], [22877, 11159, 85255, 61703],
         [11917, 8218, 84490, 36637], [75914, 29447, 83941, 64384], [22490, 71433, 64258, 74059],
         [18433, 51177, 87595, 98688], [70854, 80720, 91838, 92304], [46522, 49839, 48550, 94096],
         [95435, 37993, 99139, 49382], [10618, 696, 33239, 45957], [18854, 2818, 57522, 78807],
         [61229, 36593, 76550, 41271], [99381, 90692, 99820, 95125]]
    ]
    expectation = [6, 49, 971243962]
    for rectangles, expected in zip(arguments, expectation):
        solution = Solution().rectangleArea(rectangles)
        assert solution == expected
test()