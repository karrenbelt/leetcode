### Source : https://leetcode.com/problems/expressive-words/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-21

##################################################################################################### 
#
# Sometimes people repeat letters to represent extra feeling, such as "hello" -> "heeellooo", "hi" -> 
# "hiiii".  In these strings like "heeellooo", we have groups of adjacent letters that are all the 
# same:  "h", "eee", "ll", "ooo".
# 
# For some given string s, a query word is stretchy if it can be made to be equal to s by any number 
# of applications of the following extension operation: choose a group consisting of characters c, 
# and add some number of characters c to the group so that the size of the group is 3 or more.
# 
# For example, starting with "hello", we could do an extension on the group "o" to get "hellooo", but 
# we cannot get "helloo" since the group "oo" has size less than 3.  Also, we could do another 
# extension like "ll" -> "lllll" to get "helllllooo".  If s = "helllllooo", then the query word 
# "hello" would be stretchy because of these two extension operations: query = "hello" -> "hellooo" 
# -> "helllllooo" = s.
# 
# Given a list of query words, return the number of words that are stretchy. 
# 
# Example:
# Input: 
# s = "heeellooo"
# words = ["hello", "hi", "helo"]
# Output: 1
# Explanation: 
# We can extend "e" and "o" in the word "hello" to get "heeellooo".
# We can't extend "helo" to get "heeellooo" because the group "ll" is not size 3 or more.
# 
# Constraints:
# 
# 	0 <= len(s) <= 100.
# 	0 <= len(words) <= 100.
# 	0 <= len(words[i]) <= 100.
# 	s and all words in words consist only of lowercase letters
#####################################################################################################

from typing import List


class Solution:
    def expressiveWords(self, S: str, words: List[str]) -> int:

        def is_expressive(word: str) -> bool:
            i = j = 0
            while i < len(S) and j < len(word):
                if S[i] != word[j]:
                    return False
                x, y = i, j
                while i < len(S) and S[x] == S[i]:
                    i += 1
                while j < len(word) and word[y] == word[j]:
                    j += 1
                if i - x != j - y and i - x < max(3, j - y):   # had the second condition wrong: (i - x) - (j - y) < 2
                    return False
            return i == len(S) and j == len(word)

        return sum(is_expressive(w) for w in words)

    def expressiveWords(self, S: str, words: List[str]) -> int:
        import itertools

        def check(word: str) -> bool:
            pair_groups = itertools.zip_longest(*map(itertools.groupby, (S, word)),  fillvalue=(None, []))
            for (s_key, s_groups), (w_key, w_groups) in pair_groups:
                ls, lw = len(list(s_groups)), len(list(w_groups))
                if s_key != w_key or (ls != lw and ls < max(3, lw)):
                    return False
            return True

        return sum(check(word) for word in words)


def test():
    arguments = [
        ("heeellooo", ["hello", "hi", "helo"]),
        ("abcd", ["abc"]),
        ("dddiiiinnssssssoooo",
        ["dinnssoo", "ddinso", "ddiinnso", "ddiinnssoo", "ddiinso", "dinsoo", "ddiinsso", "dinssoo", "dinso"])
    ]
    expectations = [1, 0, 3]
    for (S, words), expected in zip(arguments, expectations):
        solution = Solution().expressiveWords(S, words)
        assert solution == expected, solution
