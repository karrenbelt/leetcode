### Source : https://leetcode.com/problems/smallest-number-in-infinite-set/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-19

##################################################################################################### 
#
# You have a set which contains all positive integers [1, 2, 3, 4, 5, ...].
# 
# Implement the SmallestInfiniteSet class:
# 
# 	SmallestInfiniteSet() Initializes the SmallestInfiniteSet object to contain all positive 
# integers.
# 	int popSmallest() Removes and returns the smallest integer contained in the infinite set.
# 	void addBack(int num) Adds a positive integer num back into the infinite set, if it is not 
# already in the infinite set.
# 
# Example 1:
# 
# Input
# ["SmallestInfiniteSet", "addBack", "popSmallest", "popSmallest", "popSmallest", "addBack", 
# "popSmallest", "popSmallest", "popSmallest"]
# [[], [2], [], [], [], [1], [], [], []]
# Output
# [null, null, 1, 2, 3, null, 1, 4, 5]
# 
# Explanation
# SmallestInfiniteSet smallestInfiniteSet = new SmallestInfiniteSet();
# smallestInfiniteSet.addBack(2);    // 2 is already in the set, so no change is made.
# smallestInfiniteSet.popSmallest(); // return 1, since 1 is the smallest number, and remove it from 
# the set.
# smallestInfiniteSet.popSmallest(); // return 2, and remove it from the set.
# smallestInfiniteSet.popSmallest(); // return 3, and remove it from the set.
# smallestInfiniteSet.addBack(1);    // 1 is added back to the set.
# smallestInfiniteSet.popSmallest(); // return 1, since 1 was added back to the set and
#                                    // is the smallest number, and remove it from the set.
# smallestInfiniteSet.popSmallest(); // return 4, and remove it from the set.
# smallestInfiniteSet.popSmallest(); // return 5, and remove it from the set.
# 
# Constraints:
# 
# 	1 <= num <= 1000
# 	At most 1000 calls will be made in total to popSmallest and addBack.
#####################################################################################################

import heapq


class SmallestInfiniteSet:

    def __init__(self):  # O(1) time
        self.smallest = 1
        self.heap = []
        self.hashset = set()

    def popSmallest(self) -> int:  # O(log n)
        if self.hashset:
            smallest = heapq.heappop(self.heap)
            self.hashset.remove(smallest)
        else:
            smallest = self.smallest
            self.smallest += 1
        return smallest

    def addBack(self, num: int) -> None:  # O(log n)
        if num < self.smallest and num not in self.hashset:
            self.hashset.add(num)
            heapq.heappush(self.heap, num)


class SmallestInfiniteSet:

    def __init__(self):  # O(1000) time
        from sortedcontainers import SortedSet
        self.sorted_set = SortedSet(reversed(range(1, 1001)), key=lambda x: -x)

    def popSmallest(self) -> int:  # O(log n) time
        return self.sorted_set.pop()

    def addBack(self, num: int) -> None:  # O(log n) time
        self.sorted_set.add(num)


class SmallestInfiniteSet:

    def __init__(self):  # O(1) time
        from sortedcontainers import SortedSet
        self.smallest = 1
        self.sorted_set = SortedSet()

    def popSmallest(self) -> int:  # O(log n) time
        if not self.sorted_set:
            self.smallest += 1
            return self.smallest - 1
        return self.sorted_set.pop(0)

    def addBack(self, num: int) -> None:  # O(log n) time
        if num < self.smallest:
            self.sorted_set.add(num)


# Your SmallestInfiniteSet object will be instantiated and called as such:
# obj = SmallestInfiniteSet()
# param_1 = obj.popSmallest()
# obj.addBack(num)


def test():
    operations = [
        ["SmallestInfiniteSet", "addBack", "popSmallest", "popSmallest",
         "popSmallest", "addBack", "popSmallest", "popSmallest", "popSmallest"],
        ["SmallestInfiniteSet", "popSmallest", "addBack", "popSmallest",
         "popSmallest", "popSmallest", "addBack", "addBack", "popSmallest",
         "popSmallest"],
    ][1]
    arguments = [
        [[], [2], [], [], [], [1], [], [], []],
        [[], [], [1], [], [], [], [2], [3], [], []],
    ][1]
    expectations = [
        [None, None, 1, 2, 3, None, 1, 4, 5],
        [None, 1, None, 1, 2, 3, None, None, 2, 3],
    ][1]
    # for ops, arguments,
    obj = SmallestInfiniteSet(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected, (solution, expected)
