### Source : https://leetcode.com/problems/reformat-the-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-07

##################################################################################################### 
#
# Given alphanumeric string s. (Alphanumeric string is a string consisting of lowercase English 
# letters and digits).
# 
# You have to find a permutation of the string where no letter is followed by another letter and no 
# digit is followed by another digit. That is, no two adjacent characters have the same type.
# 
# Return the reformatted string or return an empty string if it is impossible to reformat the string.
# 
# Example 1:
# 
# Input: s = "a0b1c2"
# Output: "0a1b2c"
# Explanation: No two adjacent characters have the same type in "0a1b2c". "a0b1c2", "0a1b2c", 
# "0c2a1b" are also valid permutations.
# 
# Example 2:
# 
# Input: s = "leetcode"
# Output: ""
# Explanation: "leetcode" has only characters so we cannot separate them by digits.
# 
# Example 3:
# 
# Input: s = "1229857369"
# Output: ""
# Explanation: "1229857369" has only digits so we cannot separate them by characters.
# 
# Example 4:
# 
# Input: s = "covid2019"
# Output: "c2o0v1i9d"
# 
# Example 5:
# 
# Input: s = "ab123"
# Output: "1a2b3"
# 
# Constraints:
# 
# 	1 <= s.length <= 500
# 	s consists of only lowercase English letters and/or digits.
#####################################################################################################


class Solution:
    def reformat(self, s: str) -> str:  # O(n) time and O(n) space
        short, long = sorted(map(lambda func: list(filter(func, s)), (str.isalpha, str.isdigit)), key=len)
        zipped = "".join(c for t in zip(long, short) for c in t) if len(long) - len(short) <= 1 else ""
        return zipped + long.pop() if len(long) - len(short) == 1 else zipped

    def reformat(self, s: str) -> str:  # O(n) time and O(n) space
        zipped, (short, long) = [""] * len(s), (sorted([[*filter(str.isalpha, s)], [*filter(str.isdigit, s)]], key=len))
        zipped[::2], zipped[1::2] = long, short
        return "" if len(long) - len(short) > 1 else "".join(zipped)

    def reformat(self, s: str) -> str:  # O(n) time and O(n) space
        (numbers := []) or (letters := []) or any((letters if c.isalpha() else numbers).append(c) for c in s)
        zipped = [''.join((letters.pop(), numbers.pop())) for _ in range(min(len(letters), len(numbers)))]
        return "" if abs(len(letters) - len(numbers)) > 1 else "".join(numbers + zipped + letters)

    def reformat(self, s: str) -> str:  # O(n) time and O(n) space
        from itertools import zip_longest  # basically the same, but don't need to import here so "2-line"...
        small, big = sorted(map(lambda func: list(filter(func, s)), (str.isalpha, str.isdigit)), key=len)
        return "".join("".join(t) for t in zip_longest(big, small, fillvalue="")) if len(big) - len(small) <= 1 else ""


def test():
    arguments = [
        "a0b1c2",
        "leetcode",
        "1229857369",
        "covid2019",
        "ab123",
    ]
    expectations = [
        {"0a1b2c", "a0b1c2", "0a1b2c", "0c2a1b"},
        {""},
        {""},
        {"c2o0v1i9d"},
        {"1a2b3"},
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().reformat(s)
        assert solution in expected, (solution, expected)
