### Source : https://leetcode.com/problems/linked-list-cycle-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
# 
# There is a cycle in a linked list if there is some node in the list that can be reached again by 
# continuously following the next pointer. Internally, pos is used to denote the index of the node 
# that tail's next pointer is connected to. Note that pos is not passed as a parameter.
# 
# Notice that you should not modify the linked list.
# 
# Example 1:
# 
# Input: head = [3,2,0,-4], pos = 1
# Output: tail connects to node index 1
# Explanation: There is a cycle in the linked list, where tail connects to the second node.
# 
# Example 2:
# 
# Input: head = [1,2], pos = 0
# Output: tail connects to node index 0
# Explanation: There is a cycle in the linked list, where tail connects to the first node.
# 
# Example 3:
# 
# Input: head = [1], pos = -1
# Output: no cycle
# Explanation: There is no cycle in the linked list.
# 
# Constraints:
# 
# 	The number of the nodes in the list is in the range [0, 104].
# 	-105 <= Node.val <= 105
# 	pos is -1 or a valid index in the linked-list.
# 
# Follow up: Can you solve it using O(1) (i.e. constant) memory?
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        if not head:
            return
        node = head
        seen = set()
        while node:
            if node in seen:
                return node
            seen.add(node)
            node = node.next

    def detectCycle(self, head: Optional[ListNode]) -> Optional[ListNode]:
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            if slow == fast:
                break
        else:
            return None
            
        slow = head
        while slow != fast:
            slow = slow.next
            fast = fast.next
        return slow


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [3, 2, 0, -4],
        [1, 2],
        [1],
        ]
    positions = [1, 0, -1]
    expectations = [1, 0, -1]
    for nums, pos, expected in zip(arrays_of_numbers, positions, expectations):
        ll = SinglyLinkedList(nums)
        if pos >= 0:
            ll.tail.next = ll[pos]
        solution = Solution().detectCycle(ll.head)
        assert solution == ll[pos] if pos >= 0 else None
