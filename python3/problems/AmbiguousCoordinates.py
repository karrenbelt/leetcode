### Source : https://leetcode.com/problems/ambiguous-coordinates/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-13

##################################################################################################### 
#
# We had some 2-dimensional coordinates, like "(1, 3)" or "(2, 0.5)".  Then, we removed all commas, 
# decimal points, and spaces, and ended up with the string s.  Return a list of strings representing 
# all possibilities for what our original coordinates could have been.
# 
# Our original representation never had extraneous zeroes, so we never started with numbers like 
# "00", "0.0", "0.00", "1.0", "001", "00.01", or any other number that can be represented with less 
# digits.  Also, a decimal point within a number never occurs without at least one digit occuring 
# before it, so we never started with numbers like ".1".
# 
# The final answer list can be returned in any order.  Also note that all coordinates in the final 
# answer have exactly one space between them (occurring after the comma.)
# 
# Example 1:
# Input: s = "(123)"
# Output: ["(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"]
# 
# Example 2:
# Input: s = "(00011)"
# Output:  ["(0.001, 1)", "(0, 0.011)"]
# Explanation: 
# 0.0, 00, 0001 or 00.01 are not allowed.
# 
# Example 3:
# Input: s = "(0123)"
# Output: ["(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)", "(0.1, 2.3)", "(0.12, 3)"]
# 
# Example 4:
# Input: s = "(100)"
# Output: [(10, 0)]
# Explanation: 
# 1.0 is not allowed.
# 
# Note: 
# 
# 	4 <= s.length <= 12.
# 	s[0] = "(", s[s.length - 1] = ")", and the other elements in s are digits.
# 
#####################################################################################################

from typing import List


class Solution:
    def ambiguousCoordinates(self, s: str) -> List[str]:  # O(n^3) time

        def generate(coord: str) -> str:
            if len(coord) == 1 or coord[0] != '0':  # no decimal
                yield coord
            if len(coord) > 1 and coord[-1] != '0':
                if coord[0] == '0':  # single decimal possibility
                    yield f"{coord[0]}.{coord[1:]}"
                else:  # decimal at every other position
                    yield from (f"{coord[:i]}.{coord[i:]}" for i in range(len(coord) - 1, 0, -1))

        return [f"({x}, {y})"
                for part1, part2 in ((s[1:i], s[i:-1]) for i in range(2, len(s) - 1))
                for x in generate(part1) for y in generate(part2)]


def test():
    from collections import Counter
    arguments = [
        "(123)",
        "(00011)",
        "(0123)",
        "(100)",
    ]
    expectations = [
        ["(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"],
        ["(0.001, 1)", "(0, 0.011)"],
        ["(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)", "(0.1, 2.3)", "(0.12, 3)"],
        ["(10, 0)"]
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().ambiguousCoordinates(s)
        assert Counter(solution) == Counter(expected)
