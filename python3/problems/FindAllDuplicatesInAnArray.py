### Source : https://leetcode.com/problems/find-all-duplicates-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Given an array of integers, 1 &le; a[i] &le; n (n = size of array), some elements appear twice and 
# others appear once.
# 
# Find all the elements that appear twice in this array.
# 
# Could you do it without extra space and in O(n) runtime?
# 
# Example:
# 
# Input:
# [4,3,2,7,8,2,3,1]
# 
# Output:
# [2,3]
#####################################################################################################

from typing import List


class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:  # O(n log n) time and O(1) space
        nums.sort()
        i = 1
        duplicates = []
        while i < len(nums):
            if nums[i] != i + nums[0]:
                duplicates.append(nums[i])
                i += 1
            i += 1
        return duplicates

    def findDuplicates(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        seen = set()
        duplicates = []
        for n in nums:
            if n in seen:
                duplicates.append(n)
            seen.add(n)
        return duplicates

    def findDuplicates(self, nums: List[int]) -> List[int]:  # O(n) time and O(1) space
        # using the array as a hash to store those seen before
        duplicates = []
        for n in nums:
            if nums[abs(n) - 1] > 0:
                nums[abs(n) - 1] *= -1
            else:
                duplicates.append(abs(n))
        return duplicates

    def findDuplicates(self, nums: List[int]) -> List[int]:  # O(n)
        from collections import Counter
        counts = Counter(nums)
        return [k for k, v in counts.items() if v > 1]


def test():
    arguments = [
        [4, 3, 2, 7, 8, 2, 3, 1],
        ]
    expectations = [
        [2, 3]
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findDuplicates(nums)
        assert solution == expected
