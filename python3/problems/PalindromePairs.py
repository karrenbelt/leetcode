### Source : https://leetcode.com/problems/palindrome-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-10

##################################################################################################### 
#
# Given a list of unique words, return all the pairs of the distinct indices (i, j) in the given 
# list, so that the concatenation of the two words words[i] + words[j] is a palindrome.
# 
# Example 1:
# 
# Input: words = ["abcd","dcba","lls","s","sssll"]
# Output: [[0,1],[1,0],[3,2],[2,4]]
# Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
# 
# Example 2:
# 
# Input: words = ["bat","tab","cat"]
# Output: [[0,1],[1,0]]
# Explanation: The palindromes are ["battab","tabbat"]
# 
# Example 3:
# 
# Input: words = ["a",""]
# Output: [[0,1],[1,0]]
# 
# Constraints:
# 
# 	1 <= words.length <= 5000
# 	0 <= words[i].length <= 300
# 	words[i] consists of lower-case English letters.
#####################################################################################################

from typing import List


class Solution:
    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # O(n^2 * k) time and O(1) space -> TLE
        palindrome_pairs = []
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                for x, y in [(i, j), (j, i)]:
                    new = words[x] + words[y]
                    if len(new) == 1:
                        palindrome_pairs.append([x, y])
                    elif new[:len(new) // 2] == new[-(len(new) // 2):][::-1]:
                        palindrome_pairs.append([x, y])
        return palindrome_pairs

    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # TLE
        palindrome_pairs = []
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                for x, y in [(i, j), (j, i)]:
                    new = words[x] + words[y]
                    for k in range(len(new)):
                        if new[k] != new[-k - 1]:
                            break
                    else:
                        palindrome_pairs.append([x, y])
        return palindrome_pairs

    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # O(n * m^2) time and O(n * m) space
        # something something trie but I'm too tired

        # build
        trie = {}
        for i, word in enumerate(words):
            node = trie
            for c in reversed(word):
                if c not in node:
                    node[c] = {}
                node = node[c]
            node.setdefault('.', set()).add(i)

        # search
        palindrome_pairs = []
        for i, word in enumerate(words):
            node = trie
            for c in word:
                if c not in node:
                    continue
                node = node[c]
            for j in node.get('.', set()).difference({i}):
                palindrome_pairs.append([i, j])

        return palindrome_pairs

    def palindromePairs(self, words: List[str]) -> List[List[int]]:  # O(n * m^2) time and O(n * m) space
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def is_palindrome(check):
            return check == check[::-1]

        palindrome_pairs = []
        words = {word: i for i, word in enumerate(words)}
        for word in words:
            for k in range(len(word) + 1):
                prefix, suffix = word[:k], word[k:]
                if is_palindrome(prefix):
                    back = suffix[::-1]
                    if back in words and words[back] != words[word]:
                        palindrome_pairs.append([words[back], words[word]])
                if k != len(word) and is_palindrome(suffix):  # k!= m is added to avoid considering A == B[::-1]  twice
                    back = prefix[::-1]
                    if back in words and words[back] != words[word]:
                        palindrome_pairs.append([words[word], words[back]])
        return palindrome_pairs


def test():
    from collections import Counter
    arguments = [
        ["abcd", "dcba", "lls", "s", "sssll"],
        ["bat", "tab", "cat"],
        ["a", ""],
    ]
    expectations = [
        [[0, 1], [1, 0], [3, 2], [2, 4]],
        [[0, 1], [1, 0]],
        [[0, 1], [1, 0]],
    ]
    for words, expected in zip(arguments, expectations):
        solution = Solution().palindromePairs(words)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected)), (expected, solution)
test()