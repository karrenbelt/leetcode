### Source : https://leetcode.com/problems/remove-outermost-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-03

##################################################################################################### 
#
# A valid parentheses string is either empty (""), "(" + A + ")", or A + B, where A and B are valid 
# parentheses strings, and + represents string concatenation.  For example, "", "()", "(())()", and 
# "(()(()))" are all valid parentheses strings.
# 
# A valid parentheses string s is primitive if it is nonempty, and there does not exist a way to 
# split it into s = A+B, with A and B nonempty valid parentheses strings.
# 
# Given a valid parentheses string s, consider its primitive decomposition: s = P_1 + P_2 + ... + 
# P_k, where P_i are primitive valid parentheses strings.
# 
# Return s after removing the outermost parentheses of every primitive string in the primitive 
# decomposition of S.
# 
# Example 1:
# 
# Input: s = "(()())(())"
# Output: "()()()"
# Explanation: 
# The input string is "(()())(())", with primitive decomposition "(()())" + "(())".
# After removing outer parentheses of each part, this is "()()" + "()" = "()()()".
# 
# Example 2:
# 
# Input: s = "(()())(())(()(()))"
# Output: "()()()()(())"
# Explanation: 
# The input string is "(()())(())(()(()))", with primitive decomposition "(()())" + "(())" + 
# "(()(()))".
# After removing outer parentheses of each part, this is "()()" + "()" + "()(())" = "()()()()(())".
# 
# Example 3:
# 
# Input: s = "()()"
# Output: ""
# Explanation: 
# The input string is "()()", with primitive decomposition "()" + "()".
# After removing outer parentheses of each part, this is "" + "" = "".
# 
# Note:
# 
# 	s.length <= 10000
# 	s[i] is "(" or ")"
# 	s is a valid parentheses string
# 
#####################################################################################################


class Solution:
    def removeOuterParentheses(self, s: str) -> str:  # O(n) time and O(n) space
        solution = []
        i = opened = 0
        for j, parenthesis in enumerate(s):
            opened += 1 if parenthesis == '(' else -1
            if not opened:
                solution.append(s[i + 1: j])
                i = j + 1
        return ''.join(solution)

    def removeOuterParentheses(self, S: str) -> str:  # O(n) time and O(n) space
        ans, level = "", 0
        for c in S:
            ans += c if level and c == '(' else c if level != 1 and c == ')' else ''
            level += 1 if c == '(' else -1
        return ans

    def removeOuterParentheses(self, s: str) -> str:  # O(n) time and O(n) space
        return ''.join(filter(lambda c, opened=[0]: opened.append(opened[-1] + ' ('.find(c)) or all(opened[-2:]), s))

    def removeOuterParentheses(self, s: str) -> str:  # O(n) time and O(n) space
        return (o := 0) or ''.join(c for c in s if min(o, (o := o + ' ('.find(c))))


def test():
    arguments = ["(()())(())", "(()())(())(()(()))", "()()"]
    expectations = ["()()()", "()()()()(())", ""]
    for s, expected in zip(arguments, expectations):
        solution = Solution().removeOuterParentheses(s)
        assert solution == expected, solution
