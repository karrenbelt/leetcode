### Source : https://leetcode.com/problems/compare-strings-by-frequency-of-the-smallest-character/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-19

##################################################################################################### 
#
# Let the function f(s) be the frequency of the lexicographically smallest character in a non-empty 
# string s. For example, if s = "dcce" then f(s) = 2 because the lexicographically smallest character 
# is 'c', which has a frequency of 2.
# 
# You are given an array of strings words and another array of query strings queries. For each query 
# queries[i], count the number of words in words such that f(queries[i]) < f(W) for each W in words.
# 
# Return an integer array answer, where each answer[i] is the answer to the ith query.
# 
# Example 1:
# 
# Input: queries = ["cbd"], words = ["zaaaz"]
# Output: [1]
# Explanation: On the first query we have f("cbd") = 1, f("zaaaz") = 3 so f("cbd") < f("zaaaz").
# 
# Example 2:
# 
# Input: queries = ["bbb","cc"], words = ["a","aa","aaa","aaaa"]
# Output: [1,2]
# Explanation: On the first query only f("bbb") < f("aaaa"). On the second query both f("aaa") and 
# f("aaaa") are both > f("cc").
# 
# Constraints:
# 
# 	1 <= queries.length <= 2000
# 	1 <= words.length <= 2000
# 	1 <= queries[i].length, words[i].length <= 10
# 	queries[i][j], words[i][j] consist of lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:  # O(n^2) time: 952 ms
        # we can sort and use binary search
        def f(s: str) -> int:  # frequency of the lexicographically smallest character
            return s.count(min(s))

        words[:] = [f(w) for w in words]
        for i, query in enumerate(map(f, queries)):
            queries[i] = len(list(filter(lambda w: query < w, words)))
        return queries

    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:  # O(n log n) time: 64 ms
        import bisect

        def f(s: str) -> int:  # frequency of the lexicographically smallest character
            return s.count(min(s))

        words[:] = sorted(f(w) for w in words)
        for i, query in enumerate(map(f, queries)):
            queries[i] = len(words) - bisect.bisect_right(words, query)
        return queries

    def numSmallerByFrequency(self, queries: List[str], words: List[str]) -> List[int]:  # O(n log n) time: 60 ms
        import bisect
        words[:] = sorted(w.count(min(w)) for w in words)
        return [len(words) - bisect.bisect_right(words, q) for i, q in enumerate(q.count(min(q)) for q in queries)]


def test():
    arguments = [
        (["cbd"], ["zaaaz"]),
        (["bbb", "cc"], ["a", "aa", "aaa", "aaaa"]),
    ]
    expectations = [[1], [1, 2]]
    for (queries, words), expected in zip(arguments, expectations):
        solution = Solution().numSmallerByFrequency(queries, words)
        assert solution == expected
