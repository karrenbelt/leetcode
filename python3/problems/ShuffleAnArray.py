### Source : https://leetcode.com/problems/shuffle-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-09

##################################################################################################### 
#
# Given an integer array nums, design an algorithm to randomly shuffle the array.
# 
# Implement the Solution class:
# 
# 	Solution(int[] nums) Initializes the object with the integer array nums.
# 	int[] reset() Resets the array to its original configuration and returns it.
# 	int[] shuffle() Returns a random shuffling of the array.
# 
# Example 1:
# 
# Input
# ["Solution", "shuffle", "reset", "shuffle"]
# [[[1, 2, 3]], [], [], []]
# Output
# [null, [3, 1, 2], [1, 2, 3], [1, 3, 2]]
# 
# Explanation
# Solution solution = new Solution([1, 2, 3]);
# solution.shuffle();    // Shuffle the array [1,2,3] and return its result. Any permutation of 
# [1,2,3] must be equally likely to be returned. Example: return [3, 1, 2]
# solution.reset();      // Resets the array back to its original configuration [1,2,3]. Return [1, 
# 2, 3]
# solution.shuffle();    // Returns the random shuffling of array [1,2,3]. Example: return [1, 3, 2]
# 
# Constraints:
# 
# 	1 <= nums.length <= 200
# 	-106 <= nums[i] <= 106
# 	All the elements of nums are unique.
# 	At most 5 * 104 calls will be made to reset and shuffle.
#####################################################################################################

from typing import List


class Solution:

    def __init__(self, nums: List[int]):
        self.nums = nums

    def reset(self) -> List[int]:
        """
        Resets the array to its original configuration and return it.
        """
        return self.nums

    def shuffle(self) -> List[int]:
        """
        Returns a random shuffling of the array.
        """
        import random
        return random.sample(self.nums, len(self.nums))


class Solution2:

    def __init__(self, nums):
        self.nums = nums

    def reset(self):
        return self.nums

    def shuffle(self):
        import random
        return sorted(self.nums, key=lambda x: random.random())

# Your Solution object will be instantiated and called as such:
# obj = Solution(nums)
# param_1 = obj.reset()
# param_2 = obj.shuffle()


def test():
    import random
    random.seed(312)
    operations = ["Solution", "shuffle", "reset", "shuffle"]
    arguments = [[[1, 2, 3]], [], [], []]
    expectations = [None, [3, 1, 2], [1, 2, 3], [1, 3, 2]]
    obj = Solution(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
