### Source : https://leetcode.com/problems/removing-minimum-and-maximum-from-array/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-21

##################################################################################################### 
#
# You are given a 0-indexed array of distinct integers nums.
# 
# There is an element in nums that has the lowest value and an element that has the highest value. We 
# call them the minimum and maximum respectively. Your goal is to remove both these elements from the 
# array.
# 
# A deletion is defined as either removing an element from the front of the array or removing an 
# element from the back of the array.
# 
# Return the minimum number of deletions it would take to remove both the minimum and maximum element 
# from the array.
# 
# Example 1:
# 
# Input: nums = [2,10,7,5,4,1,8,6]
# Output: 5
# Explanation: 
# The minimum element in the array is nums[5], which is 1.
# The maximum element in the array is nums[1], which is 10.
# We can remove both the minimum and maximum by removing 2 elements from the front and 3 elements 
# from the back.
# This results in 2 + 3 = 5 deletions, which is the minimum number possible.
# 
# Example 2:
# 
# Input: nums = [0,-4,19,1,8,-2,-3,5]
# Output: 3
# Explanation: 
# The minimum element in the array is nums[1], which is -4.
# The maximum element in the array is nums[2], which is 19.
# We can remove both the minimum and maximum by removing 3 elements from the front.
# This results in only 3 deletions, which is the minimum number possible.
# 
# Example 3:
# 
# Input: nums = [101]
# Output: 1
# Explanation:  
# There is only one element in the array, which makes it both the minimum and maximum element.
# We can remove it with 1 deletion.
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-105 <= nums[i] <= 105
# 	The integers in nums are distinct.
#####################################################################################################

from typing import List


class Solution:
    def minimumDeletions(self, nums: List[int]) -> int:  # O(n)

        def min_max_i(array: List[int]) -> tuple[int, int]:
            min_i = max_i = 0
            min_n, max_n = array[min_i], array[max_i]
            for i, num in enumerate(array):
                if num < min_n:
                    min_i, min_n = i, num
                if num > max_n:
                    max_i, max_n = i, num
            return min_i, max_i

        a, b = sorted(min_max_i(nums))
        return min(len(nums) - a, b + 1, a + len(nums) - b + 1)

    def minimumDeletions(self, nums: List[int]) -> int:  # O(n)
        a, b = sorted((nums.index(min(nums)), nums.index(max(nums))))
        return min(len(nums) - a, b + 1, a + len(nums) - b + 1)


def test():
    arguments = [
        [2, 10, 7, 5, 4, 1, 8, 6],
        [0, -4, 19, 1, 8, -2, -3, 5],
        [101]
    ]
    expectations = [5, 3, 1]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minimumDeletions(nums)
        assert solution == expected
