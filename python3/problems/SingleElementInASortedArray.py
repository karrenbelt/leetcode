### Source : https://leetcode.com/problems/single-element-in-a-sorted-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-06

##################################################################################################### 
#
# You are given a sorted array consisting of only integers where every element appears exactly twice, 
# except for one element which appears exactly once. Find this single element that appears only once.
# 
# Follow up: Your solution should run in O(log n) time and O(1) space.
# 
# Example 1:
# Input: nums = [1,1,2,3,3,4,4,8,8]
# Output: 2
# Example 2:
# Input: nums = [3,3,7,7,10,11,11]
# Output: 10
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	0 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        # no need to use set instead of list - list would have at most 1 element so constant look-up
        numbers = []
        for x in nums:
            if x in numbers:
                numbers.remove(x)
            else:
                numbers.append(x)
        return numbers.pop()

    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        return sum(set(nums)) * 2 - sum(nums)

    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        i = 0
        while i < len(nums) - 1:
            if nums[i] != nums[i + 1]:
                return nums[i]
            i += 2
        return nums[-1]

    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        single = 0
        for n in nums:
            single ^= n
        return single

    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(log n) time and O(1) space
        left, right = 0, len(nums) - 1
        while left < right:
            mid = left + (right - left) // 2
            mid -= mid % 2
            if nums[mid] == nums[mid + 1]:
                left = mid + 2
            else:
                right = mid
        return nums[left]

    def singleNonDuplicate(self, nums: List[int]) -> int:  # O(log n) time and O(1) space
        left, right = 0, len(nums) - 1
        while left < right:
            mid = left + (right - left >> 1)
            if nums[mid] == nums[mid ^ 1]:
                left = mid + 1
            else:
                right = mid
        return nums[left]


def test():
    arguments = [
        [1, 1, 2, 3, 3, 4, 4, 8, 8],
        [3, 3, 7, 7, 10, 11, 11],
        [1],
        [1, 1, 2],
        [1, 2, 2],
        ]
    expectations = [2, 10, 1, 2, 1]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().singleNonDuplicate(nums)
        assert solution == expected, (nums, expected, solution)
