### Source : https://leetcode.com/problems/linked-list-cycle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-03

##################################################################################################### 
#
# Given head, the head of a linked list, determine if the linked list has a cycle in it.
# 
# There is a cycle in a linked list if there is some node in the list that can be reached again by 
# continuously following the next pointer. Internally, pos is used to denote the index of the node 
# that tail's next pointer is connected to. Note that pos is not passed as a parameter.
# 
# Return true if there is a cycle in the linked list. Otherwise, return false.
# 
# Example 1:
# 
# Input: head = [3,2,0,-4], pos = 1
# Output: true
# Explanation: There is a cycle in the linked list, where the tail connects to the 1st node 
# (0-indexed).
# 
# Example 2:
# 
# Input: head = [1,2], pos = 0
# Output: true
# Explanation: There is a cycle in the linked list, where the tail connects to the 0th node.
# 
# Example 3:
# 
# Input: head = [1], pos = -1
# Output: false
# Explanation: There is no cycle in the linked list.
# 
# Constraints:
# 
# 	The number of the nodes in the list is in the range [0, 104].
# 	-105 <= Node.val <= 105
# 	pos is -1 or a valid index in the linked-list.
# 
# Follow up: Can you solve it using O(1) (i.e. constant) memory?
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def hasCycle(self, head: ListNode) -> bool:  # O(n) time and O(n) space
        # using an additional data structure
        seen = set()
        node = head
        while node:
            if node in seen:
                return True
            seen.add(node)
            node = node.next
        return False

    def hasCycle(self, head: ListNode) -> bool:  # O(n) time and O(1) space
        # using two pointers
        if not head:
            return False

        slow, fast = head, head.next
        while slow and fast and fast.next:
            if slow == fast:  # fast caught up with slow
                return True
            slow, fast = slow.next, fast.next.next
        return False


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [3, 2, 0, -4],
        [1, 2],
        [1],
        ]
    positions = [1, 0, -1]
    expectations = [True, True, False]
    for nums, pos, expected in zip(arrays_of_numbers, positions, expectations):
        ll = SinglyLinkedList(nums)
        if pos >= 0:
            ll.tail.next = ll[pos]
        solution = Solution().hasCycle(ll.head)
        assert solution == expected
