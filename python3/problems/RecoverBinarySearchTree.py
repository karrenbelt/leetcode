### Source : https://leetcode.com/problems/recover-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-09

##################################################################################################### 
#
# You are given the root of a binary search tree (BST), where exactly two nodes of the tree were 
# swapped by mistake. Recover the tree without changing its structure.
# 
# Follow up: A solution using O(n) space is pretty straight forward. Could you devise a constant 
# space solution?
# 
# Example 1:
# 
# Input: root = [1,3,null,null,2]
# Output: [3,1,null,null,2]
# Explanation: 3 cannot be a left child of 1 because 3 > 1. Swapping 1 and 3 makes the BST valid.
# 
# Example 2:
# 
# Input: root = [3,1,4,null,null,2]
# Output: [2,1,4,null,null,3]
# Explanation: 2 cannot be in the right subtree of 3 because 2 < 3. Swapping 2 and 3 makes the BST 
# valid.
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 1000].
# 	-231 <= Node.val <= 231 - 1
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def recoverTree(self, root: TreeNode) -> None:  # O(n) time and O(n) space, double pass and sort

        def traverse(node: TreeNode):
            if not node:
                return
            traverse(node.right)
            values.append(node.val)
            traverse(node.left)

        def replace_values(node):
            if not node:
                return
            replace_values(node.right)
            node.val = values.pop()
            replace_values(node.left)

        values = []
        traverse(root)
        values.sort()
        replace_values(root)

    def recoverTree(self, root: TreeNode) -> None:  # O(n) time and O(n) space, single pass
        node, prev = root, TreeNode(float('-inf'))
        nodes, stack = [], []
        while node or stack:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            if node.val < prev.val:
                nodes.append((prev, node))
            prev, node = node, node.right
        nodes[0][0].val, nodes[-1][-1].val = nodes[-1][-1].val, nodes[0][0].val

    def recoverTree(self, root: TreeNode) -> None:  # Morris traversal: O(n) time and O(1) space
        node, prev, nodes = root, TreeNode(float('-inf')), []
        while node:
            if node.left:
                temp = node.left
                while temp.right and temp.right != node:
                    temp = temp.right
                if not temp.right:  # no temporary connection back up, set it, then continue travelling left
                    temp.right, node = node, node.left
                    continue
                temp.right = None  # break the temporary connection
            if node.val < prev.val:
                nodes.append((prev, node))
            prev, node = node, node.right
        nodes[0][0].val, nodes[-1][-1].val = nodes[-1][-1].val, nodes[0][0].val


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,3,null,null,2]",
        "[3,1,4,null,null,2]",
        ]
    expectations = [
        "[3,1,null,null,2]",
        "[2,1,4,null,null,3]"
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        Solution().recoverTree(root)
        assert root == Codec.deserialize(expected)
