### Source : https://leetcode.com/problems/find-a-peak-element-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-20

##################################################################################################### 
#
# A peak element in a 2D grid is an element that is strictly greater than all of its adjacent 
# neighbors to the left, right, top, and bottom.
# 
# Given a 0-indexed m x n matrix mat where no two adjacent cells are equal, find any peak element 
# mat[i][j] and return the length 2 array [i,j].
# 
# You may assume that the entire matrix is surrounded by an outer perimeter with the value -1 in each 
# cell.
# 
# You must write an algorithm that runs in O(m log(n)) or O(n log(m)) time.
# 
# Example 1:
# 
# Input: mat = [[1,4],[3,2]]
# Output: [0,1]
# Explanation: Both 3 and 4 are peak elements so [1,0] and [0,1] are both acceptable answers.
# 
# Example 2:
# 
# Input: mat = [[10,20,15],[21,30,14],[7,16,32]]
# Output: [1,1]
# Explanation: Both 30 and 32 are peak elements so [1,1] and [2,2] are both acceptable answers.
# 
# Constraints:
# 
# 	m == mat.length
# 	n == mat[i].length
# 	1 <= m, n <= 500
# 	1 <= mat[i][j] <= 105
# 	No two adjacent cells are equal.
#####################################################################################################

from typing import List


class Solution:
    def findPeakGrid(self, mat: List[List[int]]) -> List[int]:  # O(n log n)
        top_row, bottom_row = 0, len(mat) - 1
        while bottom_row > top_row:
            mid = top_row + (bottom_row - top_row) // 2
            if max(mat[mid]) > max(mat[mid + 1]):
                bottom_row = mid
            else:
                top_row = mid + 1
        return [bottom_row, mat[bottom_row].index(max(mat[bottom_row]))]


def test():
    arguments = [
        [[1, 4], [3, 2]],
        [[10, 20, 15], [21, 30, 14], [7, 16, 32]],
    ]
    expectations = [
        [[0, 1], [1, 0]],
        [[1, 1], [2, 2]],
    ]
    for mat, expected in zip(arguments, expectations):
        solution = Solution().findPeakGrid(mat)
        assert tuple(solution) in set(map(tuple, expected))
