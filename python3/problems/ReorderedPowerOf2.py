### Source : https://leetcode.com/problems/reordered-power-of-2/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-21

##################################################################################################### 
#
# Starting with a positive integer N, we reorder the digits in any order (including the original 
# order) such that the leading digit is not zero.
# 
# Return true if and only if we can do this in a way such that the resulting number is a power of 2.
# 
# Example 1:
# 
# Input: 1
# Output: true
# 
# Example 2:
# 
# Input: 10
# Output: false
# 
# Example 3:
# 
# Input: 16
# Output: true
# 
# Example 4:
# 
# Input: 24
# Output: false
# 
# Example 5:
# 
# Input: 46
# Output: true
# 
# Note:
# 
# 	1 <= N <= 109
# 
#####################################################################################################


class Solution:
    def reorderedPowerOf2(self, N: int) -> bool:  # O(32) time and O(32) space
        from collections import Counter
        powers = []
        n = 1
        while n < 10 ** 9:
            powers.append(Counter(str(n)))
            n <<= 1
        return Counter(str(N)) in powers

    def reorderedPowerOf2(self, N: int) -> bool:  # O(32) time and O(1) space
        from collections import Counter
        target = Counter(str(N))
        n = 1
        while n < 10 ** 9:
            if Counter(str(n)) == target:
                return True
            n <<= 1
        return False

    def reorderedPowerOf2(self, N: int) -> bool:  # O(n log n) time
        n = sorted(str(N))  # max len if str(N) == 10, hence n log n is approx. O(23)
        for i in range(32):
            m = sorted(str(1 << i))
            if m == n:
                return True
        return False

    def reorderedPowerOf2(self, n: int) -> bool:
        return sorted(str(n)) in [sorted(str(1 << i)) for i in range(30)]


def test():
    arguments = [1, 10, 16, 24, 46, 1521]
    expectations = [True, False, True, False, True, False]
    for N, expected in zip(arguments, expectations):
        solution = Solution().reorderedPowerOf2(N)
        assert solution == expected
