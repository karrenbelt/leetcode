### Source : https://leetcode.com/problems/set-mismatch/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-02

##################################################################################################### 
#
# You have a set of integers s, which originally contains all the numbers from 1 to n. Unfortunately, 
# due to some error, one of the numbers in s got duplicated to another number in the set, which 
# results in repetition of one number and loss of another number.
# 
# You are given an integer array nums representing the data status of this set after the error.
# 
# Find the number that occurs twice and the number that is missing and return them in the form of an 
# array.
# 
# Example 1:
# Input: nums = [1,2,2,4]
# Output: [2,3]
# Example 2:
# Input: nums = [1,1]
# Output: [1,2]
# 
# Constraints:
# 
# 	2 <= nums.length <= 104
# 	1 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def findErrorNums(self, nums: List[int]) -> List[int]:
        from functools import reduce
        complete = set(range(1, len(nums) + 1))
        missing = next(iter(complete.difference(nums)))
        other = reduce(lambda x, y: x ^ y, list(complete) + nums) ^ missing
        return [other, missing]

    def findErrorNums(self, nums: List[int]) -> List[int]:
        temp = sum(set(nums))
        return [sum(nums) - temp, (len(nums) + 1) * len(nums) // 2 - temp]


def test():
    arguments = [
        [1, 2, 2, 4],
        [1, 1],
        ]
    expectations = [
        [2, 3],
        [1, 2],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findErrorNums(nums)
        assert set(solution) == set(expected)
