### Source : https://leetcode.com/problems/most-frequent-subtree-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-27

##################################################################################################### 
#
# Given the root of a binary tree, return the most frequent subtree sum. If there is a tie, return 
# all the values with the highest frequency in any order.
# 
# The subtree sum of a node is defined as the sum of all the node values formed by the subtree rooted 
# at that node (including the node itself).
# 
# Example 1:
# 
# Input: root = [5,2,-3]
# Output: [2,-3,4]
# 
# Example 2:
# 
# Input: root = [5,2,-5]
# Output: [2]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-105 <= Node.val <= 105
#####################################################################################################

from typing import List, Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def findFrequentTreeSum(self, root: Optional[TreeNode]) -> List[int]:
        from collections import Counter

        def traverse(node: TreeNode, running_sum: int):
            if not node:
                return 0
            left = traverse(node.left, running_sum + node.val)
            right = traverse(node.right, running_sum + node.val)
            subtree_sum_counts[node.val + left + right] += 1
            return node.val + left + right

        subtree_sum_counts = Counter()
        traverse(root, root.val)
        maximum_count = max(subtree_sum_counts.values())
        return [k for k, v in subtree_sum_counts.items() if v == maximum_count]


def test():
    from python3 import Codec
    arguments = [
        "[5,2,-3]",
        "[5,2,-5]",
    ]
    expectations = [
        [2, -3, 4],
        [2],
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().findFrequentTreeSum(root)
        assert solution == expected
