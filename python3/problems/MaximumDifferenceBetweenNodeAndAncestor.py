### Source : https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-02

##################################################################################################### 
#
# Given the root of a binary tree, find the maximum value V for which there exist different nodes A 
# and B where V = |A.val - B.val| and A is an ancestor of B.
# 
# A node A is an ancestor of B if either: any child of A is equal to B, or any child of A is an 
# ancestor of B.
# 
# Example 1:
# 
# Input: root = [8,3,10,1,6,null,14,null,null,4,7,13]
# Output: 7
# Explanation: We have various ancestor-node differences, some of which are given below :
# |8 - 3| = 5
# |3 - 7| = 4
# |8 - 1| = 7
# |10 - 13| = 3
# Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.
# 
# Example 2:
# 
# Input: root = [1,null,2,null,0,3]
# Output: 3
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 5000].
# 	0 <= Node.val <= 105
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def maxAncestorDiff(self, root: TreeNode) -> int:  # O(n) time and O(n) space: 36ms and 20.6MB

        def traverse(node: TreeNode, low: int, high: int):
            if not node:
                return high - low
            low, high = min(low, node.val), max(high, node.val)
            left = traverse(node.left, low, high)
            right = traverse(node.right, low, high)
            return left if left > right else right

        return traverse(root, root.val, root.val)

    def maxAncestorDiff(self, root: TreeNode) -> int:  # O(n) time and O(n) space: 32ms and 15.1MB
        queue, maximum_difference = [(root, root.val, root.val)], 0
        while queue:
            node, low, high = queue.pop()
            low, high = min(low, node.val), max(high, node.val)
            maximum_difference = max(maximum_difference, high - low)
            if node.left:
                queue.append((node.left, low, high))
            if node.right:
                queue.append((node.right, low, high))
        return maximum_difference


def test():
    from python3 import Codec
    arguments = [
        "[8,3,10,1,6,null,14,null,null,4,7,13]",
        "[1,null,2,null,0,3]",
    ]
    expectations = [7, 3]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().maxAncestorDiff(root)
        assert solution == expected
