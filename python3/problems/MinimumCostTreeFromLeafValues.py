### Source : https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-20

#####################################################################################################
#
# Given an array arr of positive integers, consider all binary trees such that:
#
# 	Each node has either 0 or 2 children;
# 	The values of arr correspond to the values of each leaf in an in-order traversal of the
# tree.
# 	The value of each non-leaf node is equal to the product of the largest leaf value in its
# left and right subtree, respectively.
#
# Among all possible binary trees considered, return the smallest possible sum of the values of each
# non-leaf node. It is guaranteed this sum fits into a 32-bit integer.
#
# A node is a leaf if and only if it has zero children.
#
# Example 1:
#
# Input: arr = [6,2,4]
# Output: 32
# Explanation: There are two possible trees shown.
# The first has a non-leaf node sum 36, and the second has non-leaf node sum 32.
#
# Example 2:
#
# Input: arr = [4,11]
# Output: 44
#
# Constraints:
#
# 	2 <= arr.length <= 40
# 	1 <= arr[i] <= 15
# 	It is guaranteed that the answer fits into a 32-bit signed integer (i.e., it is less than
# 231).
#####################################################################################################

from typing import List


class Solution:
    def mctFromLeafValues(self, arr: List[int]) -> int:  # O(n^3)
        from functools import lru_cache

        @lru_cache()
        def dfs(left: int, right: int) -> int:
            if left == right:
                return 0

            minimum = 1 << 31
            for i in range(left, right):
                value = max(arr[left:i + 1]) * max(arr[i + 1:right + 1])
                minimum = min(minimum, value + dfs(left, i) + dfs(i + 1, right))
            return minimum

        return dfs(0, len(arr) - 1)

    def mctFromLeafValues(self, arr: List[int]) -> int:  # O(n^3)

        dp = [[1 << 31] * len(arr) for _ in range(len(arr))]
        for i in range(len(arr)):
            dp[i][i] = 0

        for delta in range(1, len(arr)):
            for left in range(len(arr) - delta):
                right = left + delta
                for i in range(left, right):
                    value = max(arr[left:i + 1]) * max(arr[i + 1:right + 1])
                    value += dp[left][i] + dp[i + 1][right]
                    dp[left][right] = min(dp[left][right], value)

        return dp[0][-1]

    def mctFromLeafValues(self, arr: List[int]) -> int:  # O(n^2)
        minimum = 0
        while len(arr) > 1:
            i = arr.index(min(arr))
            minimum += arr.pop(i) * min(arr[max(0, i - 1): i + 1])
        return minimum

    def mctFromLeafValues(self, arr: List[int]) -> int:  # O(n log n)

        def backtrack(left: int, right: int) -> int:
            nonlocal minimum

            if left > right:  # avoid needless and excessive recursion
                return 0

            if left == right:
                return arr[left]

            if left + 1 == right:
                minimum += arr[left] * arr[right]
                return max(arr[left], arr[right])

            i = max(range(left, right + 1), key=lambda x: arr[x])
            left, right = backtrack(left, i - 1), backtrack(i + 1, right)
            minimum += arr[i] * left * bool(left)
            minimum += arr[i] * right * bool(right)
            return arr[i]

        minimum = 0
        backtrack(0, len(arr) - 1)
        return minimum

    def mctFromLeafValues(self, arr: List[int]) -> int:  # O(n)
        minimum, stack = 0, [1 << 31]
        for n in arr:
            while stack[-1] <= n:
                minimum += stack.pop() * min(stack[-1], n)
            stack.append(n)
        while len(stack) > 2:
            minimum += stack.pop() * stack[-1]
        return minimum


def test():
    strings = [
        [6, 2, 4],
        [4, 11],
        [6, 2, 4, 1, 7],
        [7, 12, 8, 10],
        [1, 2, 4, 1, 3, 7],
        [2, 8, 4, 1, 3],
        [2, 7, 3, 5],
        [7, 12, 8, 10],
        [15, 13, 5, 3, 15],
    ]
    expectations = [32, 44, 78, 284, 53, 63, 64, 284, 500]
    for arr, expected in zip(strings, expectations):
        solution = Solution().mctFromLeafValues(arr)
        assert solution == expected, (solution, expected)
