### Source : https://leetcode.com/problems/degree-of-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-10

##################################################################################################### 
#
# Given a non-empty array of non-negative integers nums, the degree of this array is defined as the 
# maximum frequency of any one of its elements.
# 
# Your task is to find the smallest possible length of a (contiguous) subarray of nums, that has the 
# same degree as nums.
# 
# Example 1:
# 
# Input: nums = [1,2,2,3,1]
# Output: 2
# Explanation: 
# The input array has a degree of 2 because both elements 1 and 2 appear twice.
# Of the subarrays that have the same degree:
# [1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2, 3], [2, 2]
# The shortest length is 2. So return 2.
# 
# Example 2:
# 
# Input: nums = [1,2,2,3,1,4,2]
# Output: 6
# Explanation: 
# The degree is 3 because the element 2 is repeated 3 times.
# So [2,2,3,1,4,2] is the shortest subarray, therefore returning 6.
# 
# Constraints:
# 
# 	nums.length will be between 1 and 50,000.
# 	nums[i] will be an integer between 0 and 49,999.
#####################################################################################################

from typing import List


class Solution:
    def findShortestSubArray(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        d = {}
        for i, n in enumerate(nums):
            d[n] = (d[n][0] + 1, d[n][1], i + 1) if n in d else (1, i, i + 1)
        shortest = max(d.items(), key=lambda x: (x[1][0], -(x[1][2] - x[1][1])))
        return shortest[1][-1] - shortest[1][-2]

    def findShortestSubArray(self, nums: List[int]) -> int:  # O(n) time and O(n) space - single pass
        first, count, res, degree = {}, {}, 0, 0
        for i, n in enumerate(nums):
            first.setdefault(n, i)
            count[n] = count.get(n, 0) + 1
            if count[n] > degree:
                degree = count[n]
                res = i - first[n] + 1
            elif count[n] == degree:
                res = min(res, i - first[n] + 1)
        return res


def test():
    arguments = [
        [1, 2, 2, 3, 1],
        [1, 2, 2, 3, 1, 4, 2],
        ]
    expectations = [2, 6]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findShortestSubArray(nums)
        assert solution == expected
