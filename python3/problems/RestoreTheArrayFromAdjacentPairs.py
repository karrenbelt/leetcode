### Source : https://leetcode.com/problems/restore-the-array-from-adjacent-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

#####################################################################################################
#
# There is an integer array nums that consists of n unique elements, but you have forgotten it.
# However, you do remember every pair of adjacent elements in nums.
#
# You are given a 2D integer array adjacentPairs of size n - 1 where each adjacentPairs[i] = [ui, vi]
# indicates that the elements ui and vi are adjacent in nums.
#
# It is guaranteed that every adjacent pair of elements nums[i] and nums[i+1] will exist in
# adjacentPairs, either as [nums[i], nums[i+1]] or [nums[i+1], nums[i]]. The pairs can appear in any
# order.
#
# Return the original array nums. If there are multiple solutions, return any of them.
#
# Example 1:
#
# Input: adjacentPairs = [[2,1],[3,4],[3,2]]
# Output: [1,2,3,4]
# Explanation: This array has all its adjacent pairs in adjacentPairs.
# Notice that adjacentPairs[i] may not be in left-to-right order.
#
# Example 2:
#
# Input: adjacentPairs = [[4,-2],[1,4],[-3,1]]
# Output: [-2,4,1,-3]
# Explanation: There can be negative numbers.
# Another solution is [-3,1,4,-2], which would also be accepted.
#
# Example 3:
#
# Input: adjacentPairs = [[100000,-100000]]
# Output: [100000,-100000]
#
# Constraints:
#
# 	nums.length == n
# 	adjacentPairs.length == n - 1
# 	adjacentPairs[i].length == 2
# 	2 <= n <= 105
# 	-105 <= nums[i], ui, vi <= 105
# 	There exists some nums that has adjacentPairs as its pairs.
#####################################################################################################

from typing import List


class ListNode:

    def __init__(self, value, prev=None, next=None):
        self.value = value
        self.prev = prev
        self.next = next

    def __repr__(self):
        return f"{self.__class__.__name__}: {self.value}"


class DoubleLinkedNode:
    def __init__(self, val):
        self.val = val
        self.pre = None
        self.next = None


class Solution:
    def restoreArray(self, adjacentPairs: List[List[int]]) -> List[int]:  # greedy O(n^2) -> TLE
        from collections import deque
        answer = deque(adjacentPairs[0])
        unseen = set(range(1, len(adjacentPairs)))
        while len(unseen):
            for i in list(unseen):
                if adjacentPairs[i][0] == answer[0]:
                    answer.appendleft(adjacentPairs[i][1])
                    unseen.remove(i)
                elif adjacentPairs[i][1] == answer[0]:
                    answer.appendleft(adjacentPairs[i][0])
                    unseen.remove(i)
                elif adjacentPairs[i][0] == answer[-1]:
                    answer.append(adjacentPairs[i][1])
                    unseen.remove(i)
                elif adjacentPairs[i][1] == answer[-1]:
                    answer.append(adjacentPairs[i][0])
                    unseen.remove(i)
        return list(answer)

    def restoreArray(self, adjacentPairs: List[List[int]]) -> List[int]:  # O(n) -> beats 99%

        adjacency = {}
        for i, j in adjacentPairs:
            adjacency.setdefault(i, []).append(j)
            adjacency.setdefault(j, []).append(i)

        start, end = [k for k, v in adjacency.items() if len(v) == 1]
        next_value = adjacency.pop(start)[0]
        answer = [start, next_value]
        while next_value != end:
            pair = adjacency.pop(next_value)
            next_value = pair[0] if pair[0] != answer[-2] else pair[-1]
            answer.append(next_value)
        return answer

    def restoreArray(self, adjacentPairs: List[List[int]]) -> List[int]:  # O(n) time

        def dfs(u: int):
            array.append(u)
            seen.add(u)
            for v in adjacency[u]:
                if v not in seen:
                    dfs(v)

        adjacency = {}
        for i, j in adjacentPairs:
            adjacency.setdefault(i, []).append(j)
            adjacency.setdefault(j, []).append(i)

        start, end = (k for k, v in adjacency.items() if len(v) == 1)
        array, seen = [], set()
        dfs(start)
        return array


def test():
    arguments = [
        [[2, 1], [3, 4], [3, 2]],
        [[4, -2], [1, 4], [-3, 1]],
        [[100000, -100000]],
    ]
    expectations = [
        [1, 2, 3, 4],
        [-2, 4, 1, -3],
        [100000, -100000],
    ]
    for adjacentPairs, expected in zip(arguments, expectations):
        solution = Solution().restoreArray(adjacentPairs)
        assert solution == expected
