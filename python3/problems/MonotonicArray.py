### Source : https://leetcode.com/problems/monotonic-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-19

##################################################################################################### 
#
# An array is monotonic if it is either monotone increasing or monotone decreasing.
# 
# An array A is monotone increasing if for all i <= j, A[i] <= A[j].  An array A is monotone 
# decreasing if for all i <= j, A[i] >= A[j].
# 
# Return true if and only if the given array A is monotonic.
# 
# Example 1:
# 
# Input: [1,2,2,3]
# Output: true
# 
# Example 2:
# 
# Input: [6,5,4,4]
# Output: true
# 
# Example 3:
# 
# Input: [1,3,2]
# Output: false
# 
# Example 4:
# 
# Input: [1,2,4,5]
# Output: true
# 
# Example 5:
# 
# Input: [1,1,1]
# Output: true
# 
# Note:
# 
# 	1 <= A.length <= 50000
# 	-100000 <= A[i] <= 100000
# 
#####################################################################################################

from typing import List


class Solution:
    def isMonotonic(self, A: List[int]) -> bool:  # O(n) time and O(1) space
        return len({a > b for a, b in zip(A, A[1:]) if a != b}) == 1 if len(set(A)) > 1 else True


def test():
    arguments = [
        [1, 2, 2, 3],
        [6, 5, 4, 4],
        [1, 3, 2],
        [1, 2, 4, 5],
        [1, 1, 1],
        ]
    expectations = [True, True, False, True, True]
    for A, expected in zip(arguments, expectations):
        solution = Solution().isMonotonic(A)
        assert solution == expected
