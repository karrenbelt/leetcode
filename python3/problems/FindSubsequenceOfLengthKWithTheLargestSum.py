### Source : https://leetcode.com/problems/find-subsequence-of-length-k-with-the-largest-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-12

##################################################################################################### 
#
# You are given an integer array nums and an integer k. You want to find a subsequence of nums of 
# length k that has the largest sum.
# 
# Return any such subsequence as an integer array of length k.
# 
# A subsequence is an array that can be derived from another array by deleting some or no elements 
# without changing the order of the remaining elements.
# 
# Example 1:
# 
# Input: nums = [2,1,3,3], k = 2
# Output: [3,3]
# Explanation:
# The subsequence has the largest sum of 3 + 3 = 6.
# 
# Example 2:
# 
# Input: nums = [-1,-2,3,4], k = 3
# Output: [-1,3,4]
# Explanation: 
# The subsequence has the largest sum of -1 + 3 + 4 = 6.
# 
# Example 3:
# 
# Input: nums = [3,4,3,3], k = 2
# Output: [3,4]
# Explanation:
# The subsequence has the largest sum of 3 + 4 = 7. 
# Another possible subsequence is [4, 3].
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	-105 <= nums[i] <= 105
# 	1 <= k <= nums.length
#####################################################################################################

from typing import List


class Solution:
    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:  # O(n^3)
        for i in range(len(nums) - k):
            nums.remove(min(nums))
        return nums

    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:  # O(k * n)
        import heapq
        n_smallest = heapq.nsmallest(len(nums) - k, nums)  # O(k log n)
        while n_smallest:  # O(k * n)
            nums.remove(n_smallest.pop())
        return nums

    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:  # O(k log n)
        import heapq
        from collections import Counter

        sub_seq, n_smallest = [], Counter(heapq.nsmallest(len(nums) - k, nums))  # O(k log n)
        for n in nums:  # O(n)
            if n_smallest[n]:
                n_smallest[n] -= 1
            else:
                sub_seq.append(n)
        return sub_seq

    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:  # O(k * log n)
        import heapq
        heap = []
        for i, n in enumerate(nums):  # O(k log n)
            heapq.heappush(heap, (n, i))  # O(k)
            if len(heap) > k:
                heapq.heappop(heap)
        heap.sort(key=lambda x: x[1])  # O(k log k)
        return [i[0] for i in heap]

    def maxSubsequence(self, nums: List[int], k: int) -> List[int]:  # O(n log n)
        nums_idx = sorted((n, i) for i, n in enumerate(nums))
        return [n_i[0] for n_i in sorted(nums_idx[-k:], key=lambda n_i: n_i[1])]


def test():
    arguments = [
        ([2, 1, 3, 3], 2),
        ([-1, -2, 3, 4], 3),
        ([3, 4, 3, 3], 2),
    ]
    expectations = [
        [3, 3],
        [-1, 3, 4],
        [3, 4],  # [4, 3] is also valid
    ]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().maxSubsequence(nums, k)
        assert solution == expected
