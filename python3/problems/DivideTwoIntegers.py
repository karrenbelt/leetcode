### Source : https://leetcode.com/problems/divide-two-integers/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-21

##################################################################################################### 
#
# Given two integers dividend and divisor, divide two integers without using multiplication, 
# division, and mod operator.
# 
# Return the quotient after dividing dividend by divisor.
# 
# The integer division should truncate toward zero, which means losing its fractional part. For 
# example, truncate(8.345) = 8 and truncate(-2.7335) = -2.
# 
# Note:
# 
# 	Assume we are dealing with an environment that could only store integers within the 32-bit 
# signed integer range: [&minus;231,  231 &minus; 1]. For this problem, assume that your function 
# returns 231 &minus; 1 when the division result overflows.
# 
# Example 1:
# 
# Input: dividend = 10, divisor = 3
# Output: 3
# Explanation: 10/3 = truncate(3.33333..) = 3.
# 
# Example 2:
# 
# Input: dividend = 7, divisor = -3
# Output: -2
# Explanation: 7/-3 = truncate(-2.33333..) = -2.
# 
# Example 3:
# 
# Input: dividend = 0, divisor = 1
# Output: 0
# 
# Example 4:
# 
# Input: dividend = 1, divisor = 1
# Output: 1
# 
# Constraints:
# 
# 	-231 <= dividend, divisor <= 231 - 1
# 	divisor != 0
#####################################################################################################


class Solution:
    def divide(self, dividend: int, divisor: int) -> int:  # O(dividend), not very efficient
        if dividend == -2147483648 and divisor == -1:  # DERP
            return 2147483647

        quotient = 0
        sign = 1 if (dividend < 0) == (divisor < 0) else -1
        dividend = abs(dividend)
        divisor = abs(divisor)
        while dividend >= divisor:
            quotient += 1
            dividend -= divisor

        return sign * quotient

    def divide(self, dividend: int, divisor: int) -> int:  # bit manipulation
        if dividend == -2147483648 and divisor == -1:  # DERP
            return 2147483647

        quotient = 0
        sign = -1 if ((dividend < 0) ^ (divisor < 0)) else 1
        dividend = abs(dividend)
        divisor = abs(divisor)

        temp = 0
        for i in range(31, -1, -1):  # 32-bit, start with highest but and shift
            if temp + (divisor << i) <= dividend:
                temp += divisor << i
                quotient |= 1 << i

        return sign * quotient


def test():
    numbers = [
        (10, 3),
        (7, -3),
        (0, 1),
        (1, 1),
        ]
    expectations = [3, -2, 0, 1]
    for (dividend, divisor), expected in zip(numbers, expectations):
        solution = Solution().divide(dividend, divisor)
        assert solution == expected
