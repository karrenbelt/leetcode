### Source : https://leetcode.com/problems/search-in-rotated-sorted-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
# 
# (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
# 
# You are given a target value to search. If found in the array return its index, otherwise return -1.
# 
# You may assume no duplicate exists in the array.
# 
# Your algorithm's runtime complexity must be in the order of O(log n).
# 
# Example 1:
# 
# Input: nums = [4,5,6,7,0,1,2], target = 0
# Output: 4
# 
# Example 2:
# 
# Input: nums = [4,5,6,7,0,1,2], target = 3
# Output: -1
#####################################################################################################
from typing import List


class Solution:

    def search(self, nums: List[int], target: int) -> int:
        # 1. find the pivot point
        # 2. divide the array in two subarrays
        # 3. call binary search

        def find_pivot(arr, low, high):
            if high < low:
                return -1
            if high == low:
                return low
            mid = low + (high - low) // 2
            if mid < high and arr[mid] > arr[mid + 1]:
                return mid
            if mid > low and arr[mid] < arr[mid - 1]:
                return mid - 1
            if arr[low] >= arr[mid]:
                return find_pivot(arr, low, mid - 1)
            return find_pivot(arr, mid + 1, high)

        def binary_search(arr, low, high, key):
            if high < low:
                return -1
            mid = low + (high - low) // 2
            if key == arr[mid]:
                return mid
            if key > arr[mid]:
                return binary_search(arr, (mid + 1), high, key)
            return binary_search(arr, low, (mid - 1), key)

        pivot = find_pivot(nums, 0, len(nums) - 1)
        if pivot == -1:  # if we didn't find a pivot, then the array is not rotated at all
            return binary_search(nums, 0, len(nums) - 1, target)
        elif nums[pivot] == target:
            return pivot
        elif nums[0] <= target:
            return binary_search(nums, 0, pivot - 1, target)
        return binary_search(nums, pivot + 1, len(nums) - 1, target)

    def search(self, nums: 'List[int]', target: int) -> int:
        # nums = [5, 6, 7, 8, 9, 0, 1, 2, 3, 4]
        # if target = 7, treat as: [5, 6, 7, 8, 9, inf, inf, inf, inf, inf]
        # if target = 2, treat as:  [-inf, -inf, -inf, -inf, -inf, 0, 1, 2, 3, 4]
        low, high = 0, len(nums)
        while low < high:
            mid = low + (high - low) // 2
            if target < nums[0] < nums[mid]:  # treat as if -inf
                low = mid + 1
            elif target >= nums[0] > nums[mid]:  # +inf
                high = mid
            elif nums[mid] < target:
                low = mid + 1
            elif nums[mid] > target:
                high = mid
            else:
                return mid
        return -1


def test():
    arrays_of_numbers = [
        [4, 5, 6, 7, 0, 1, 2],
        [4, 5, 6, 7, 0, 1, 2],
        [1],
        ]
    targets = [0, 3, 0]
    expectations = [4, -1, -1]
    for nums, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().search(nums, target)
        assert solution == expected
