### Source : https://leetcode.com/problems/guess-the-word/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-11

##################################################################################################### 
#
# This is an interactive problem.
# 
# You are given an array of unique strings wordlist where wordlist[i] is 6 letters long, and one word 
# in this list is chosen as secret.
# 
# You may call aster.guess(word) to guess a word. The guessed word should have type string and must 
# be from the original list with 6 lowercase letters.
# 
# This function returns an integer type, representing the number of exact matches (value and 
# position) of your guess to the secret word. Also, if your guess is not in the given wordlist, it 
# will return -1 instead.
# 
# For each test case, you have exactly 10 guesses to guess the word. At the end of any number of 
# calls, if you have made 10 or fewer calls to aster.guess and at least one of these guesses was 
# secret, then you pass the test case.
# 
# Example 1:
# 
# Input: secret = "acckzz", wordlist = ["acckzz","ccbazz","eiowzz","abcczz"], numguesses = 10
# Output: You guessed the secret word correctly.
# Explanation:
# master.guess("aaaaaa") returns -1, because "aaaaaa" is not in wordlist.
# master.guess("acckzz") returns 6, because "acckzz" is secret and has all 6 matches.
# master.guess("ccbazz") returns 3, because "ccbazz" has 3 matches.
# master.guess("eiowzz") returns 2, because "eiowzz" has 2 matches.
# master.guess("abcczz") returns 4, because "abcczz" has 4 matches.
# We made 5 calls to master.guess and one of them was the secret, so we pass the test case.
# 
# Example 2:
# 
# Input: secret = "hamada", wordlist = ["hamada","khaled"], numguesses = 10
# Output: You guessed the secret word correctly.
# 
# Constraints:
# 
# 	1 <= wordlist.length <= 100
# 	wordlist[i].length == 6
# 	wordlist[i] consist of lowercase English letters.
# 	All the strings of wordlist are unique.
# 	secret exists in wordlist.
# 	numguesses == 10
#####################################################################################################


# """
# This is Master's API interface.
# You should not implement it, or speculate about its implementation
# """
# class Master:
#     def guess(self, word: str) -> int:

from typing import List


class Master:

    def __init__(self, secret: str):
        self.secret = secret

    def guess(self, word: str):
        return sum(a == b for a, b in zip(self.secret, word))


class Solution:
    def findSecretWord(self, wordlist: List[str], master: 'Master') -> None:  # minimax
        from collections import Counter

        def match(w1: str, w2: str) -> int:
            return sum(a == b for a, b in zip(w1, w2))

        n = attempts = 0
        while n < 6:
            options = list(map(Counter, zip(*wordlist)))
            guess = max(wordlist, key=lambda word: sum(options[i][c] for i, c in enumerate(word)))
            n = master.guess(guess)
            wordlist = [w for w in wordlist if match(w, guess) == n]
            attempts += n < 6
        print('You guessed the secret word correctly.') if attempts <= 10 else print(f'Waddup dawg? {attempts}')


class Solution:
    def findSecretWord(self, wordlist: List[str], master: 'Master') -> None:  # O(n) time and O(n^2) space

        def generate_keys(w: str) -> str:
            yield from ("*" * i + c + "*" * (5 - i) for i, c in enumerate(w))

        def guess(words: List[str], attempt: int):
            word_buckets = {}
            for word in words:
                for key in generate_keys(word):
                    word_buckets.setdefault(key, []).append(word)

            word = next(word for word in words if word not in guessed)
            n = guessed.add(word) or master.guess(word)
            if n < 6:
                keys = list(generate_keys(word))
                candidates = (word_buckets[k] for k in keys)
                new_words = set().union(*candidates) if n else set(words).difference(*candidates)
                guess(list(new_words), attempt + 1)
            else:
                print('You guessed the secret word correctly.') if attempt <= 10 else print(f'Waddup dawg? {attempt}')

        guessed = set()
        guess(wordlist, 0)


def test():
    arguments = [
        (["acckzz", "ccbazz", "eiowzz", "abcczz"], Master("acckzz")),
        (["hamada", "khaled"], Master("hamada")),
        (["aazyxw", "aayxwv", "aaxwvu", "aawvut", "aavuts", "aautsr", "aatsrq", "aasrqp", "aarqpo", "aaqpon", "aaponm",
          "aaonml", "aanmlk", "aamlkj", "aalkji", "aakjih", "aajihg", "aaihgf", "aahgfe", "aagfed", "aafedc", "ccwwww",
          "ccssss", "ccoooo", "cckkkk", "ccgggg", "cccccc", "ccyyyy", "ccuuuu", "ccqqqq", "ccmmmm", "ddwwww", "ddssss",
          "ddoooo", "ddkkkk", "ddgggg", "ddcccc", "ddyyyy", "dduuuu", "ddqqqq", "ddmmmm", "eewwww", "eessss", "eeoooo",
          "eekkkk", "eegggg", "eecccc", "eeyyyy", "eeuuuu", "eeqqqq", "eemmmm", "ffwwww", "ffssss", "ffoooo", "ffkkkk",
          "ffgggg", "ffcccc", "ffyyyy", "ffuuuu", "ffqqqq", "ffmmmm", "ggwwww", "ggssss", "ggoooo", "ggkkkk", "gggggg",
          "ggcccc", "ggyyyy", "gguuuu", "ggqqqq", "ggmmmm", "hhwwww", "hhssss", "hhoooo", "hhkkkk", "hhgggg", "hhcccc",
          "hhyyyy", "hhuuuu", "hhqqqq", "hhmmmm", "iiwwww", "iissss", "iioooo", "iikkkk", "iigggg", "iicccc", "iiyyyy",
          "iiuuuu", "iiqqqq", "iimmmm", "jjwwww", "jjssss", "jjoooo", "jjkkkk", "jjgggg", "jjcccc", "jjyyyy", "jjuuuu",
          "jjqqqq"], Master("aaponm")),
    ]
    for (wordlist, master) in arguments:
        Solution().findSecretWord(wordlist, master)
