### Source : https://leetcode.com/problems/equal-sum-arrays-with-minimum-number-of-operations/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-28

#####################################################################################################
#
# You are given two arrays of integers nums1 and nums2, possibly of different lengths. The values in
# the arrays are between 1 and 6, inclusive.
#
# In one operation, you can change any integer's value in any of the arrays to any value between 1
# and 6, inclusive.
#
# Return the minimum number of operations required to make the sum of values in nums1 equal to the
# sum of values in nums2. Return -1-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K if it is not possible to make the sum of the two
# arrays equal.
#
# Example 1:
#
# Input: nums1 = [1,2,3,4,5,6], nums2 = [1,1,2,2,2,2]
# Output: 3
# Explanation: You can make the sums of nums1 and nums2 equal with 3 operations. All indices are
# 0-indexed.
# - Change nums2[0] to 6. nums1 = [1,2,3,4,5,6], nums2 = [6,1,2,2,2,2].
# - Change nums1[5] to 1. nums1 = [1,2,3,4,5,1], nums2 = [6,1,2,2,2,2].
# - Change nums1[2] to 2. nums1 = [1,2,2,4,5,1], nums2 = [6,1,2,2,2,2].
#
# Example 2:
#
# Input: nums1 = [1,1,1,1,1,1,1], nums2 = [6]
# Output: -1
# Explanation: There is no way to decrease the sum of nums1 or to increase the sum of nums2 to make
# them equal.
#
# Example 3:
#
# Input: nums1 = [6,6], nums2 = [1]
# Output: 3
# Explanation: You can make the sums of nums1 and nums2 equal with 3 operations. All indices are
# 0-indexed.
# - Change nums1[0] to 2. nums1 = [2,6], nums2 = [1].
# - Change nums1[1] to 2. nums1 = [2,2], nums2 = [1].
# - Change nums2[0] to 4. nums1 = [2,2], nums2 = [4].
#
# Constraints:
#
# 	1 <= nums1.length, nums2.length <= 105
# 	1 <= nums1[i], nums2[i] <= 6
#####################################################################################################

from typing import List


class Solution:
    def minOperations(self, nums1: List[int], nums2: List[int]) -> int:  # O(n log n) time
        import heapq

        if len(nums1) > 6 * len(nums2) or len(nums2) > 6 * len(nums1):
            return -1

        smaller, larger = sorted([nums1, nums2], key=sum)
        if len(smaller) * 6 < len(larger):
            return -1

        moves, difference = 0, sum(larger) - sum(smaller)
        if not difference:
            return moves

        heap = [n - 6 for n in smaller if n - 6] + [1 - n for n in larger if 1 - n]
        heapq.heapify(heap)
        while difference > 0 and (moves := moves + 1):
            n = -heapq.heappop(heap)
            difference -= n
        return moves

    def minOperations(self, nums1: List[int], nums2: List[int]) -> int:
        from collections import Counter

        smaller, larger = sorted([nums1, nums2], key=sum)
        if len(smaller) * 6 < len(larger):
            return -1

        m1, m2 = map(Counter, (larger, smaller))
        diff = sum(nums1) - sum(nums2)
        if diff == 0:
            return 0

        moves = 0
        for i in range(6):
            ctr = m1[6 - i] + m2[i + 1]
            delta = 6 - (i + 1)
            if delta <= 0:
                continue

            need = diff // delta
            rem = diff % delta
            if need < ctr:
                moves += need + bool(rem)
                break

            elif need == ctr:
                moves += need
                diff = rem
            else:
                moves += ctr
                diff -= delta * ctr

        return moves

    def minOperations(self, nums1: List[int], nums2: List[int]) -> int:
        import math
        from collections import Counter

        nums = sorted([nums1, nums2], key=sum)
        funcs = len, sum, Counter
        (l1, l2), (s1, s2), (d1, d2) = (map(f, nums) for f in funcs)

        if s1 == s2:
            return 0

        if l2 > l1 * 6:
            return -1

        ctr, i, diff = 0, 6, s2 - s1
        while i > 0 and diff > 0:
            ts = d1[7 - i] + d2[i]
            v = i - 1
            cl = math.ceil(diff / v)
            if ts >= cl:
                ctr += cl
                break
            diff -= v * ts
            ctr += ts
            i -= 1

        return ctr


def test():
    arguments = [
        ([1, 2, 3, 4, 5, 6], [1, 1, 2, 2, 2, 2]),
        ([1, 1, 1, 1, 1, 1, 1], [6]),
        ([6, 6], [1]),
    ]
    expectations = [3, -1, 3]
    for (nums1, nums2), expected in zip(arguments, expectations):
        solution = Solution().minOperations(nums1, nums2)
        assert solution == expected
test()