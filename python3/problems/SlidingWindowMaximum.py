### Source : https://leetcode.com/problems/sliding-window-maximum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# You are given an array of integers nums, there is a sliding window of size k which is moving from 
# the very left of the array to the very right. You can only see the k numbers in the window. Each 
# time the sliding window moves right by one position.
# 
# Return the max sliding window.
# 
# Example 1:
# 
# Input: nums = [1,3,-1,-3,5,3,6,7], k = 3
# Output: [3,3,5,5,6,7]
# Explanation: 
# Window position                ax
# ---------------               -----
# [1  3  -1] -3  5  3  6  7       3
#  1 [3  -1  -3] 5  3  6  7       3
#  1  3 [-1  -3  5] 3  6  7       5
#  1  3  -1 [-3  5  3] 6  7       5
#  1  3  -1  -3 [5  3  6] 7       6
#  1  3  -1  -3  5 [3  6  7]      7
# 
# Example 2:
# 
# Input: nums = [1], k = 1
# Output: [1]
# 
# Example 3:
# 
# Input: nums = [1,-1], k = 1
# Output: [1,-1]
# 
# Example 4:
# 
# Input: nums = [9,11], k = 2
# Output: [11]
# 
# Example 5:
# 
# Input: nums = [4,-2], k = 2
# Output: [4]
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-104 <= nums[i] <= 104
# 	1 <= k <= nums.length
#####################################################################################################

from typing import List


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        maxima = []
        for i in range(len(nums) - k + 1):
            maxima.append(max(nums[i: i + k]))
        return maxima

    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:  # O(n^2) -> TLE: 49 / 61 test cases passed.
        return [max(nums[i: i + k]) for i in range(len(nums) - k + 1)]

    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:  # O(n) time
        from collections import deque
        larger = deque()  # good candidates
        maxima = []
        for i, n in enumerate(nums):
            while larger and nums[larger[-1]] < n:  # ensure right-most is smallest
                larger.pop()                        # 1. pop indexes of smaller elements
            larger.append(i)                        # 2. append the current index
            if i - larger[0] >= k:                  # ensure left-most in-bound
                larger.popleft()                    # 3. pop index if it falls outside the window
            if i >= k - 1:
                maxima.append(nums[larger[0]])      # 4. if window size == k, append the current maximum
        return maxima


def test():
    arguments = [
        ([1, 3, -1, -3, 5, 3, 6, 7], 3),
        ([1], 1),
        ([1, -1], 1),
        ([9, 11], 2),
        ([4, -2], 2),
        ]
    expectations = [
        [3, 3, 5, 5, 6, 7],
        [1],
        [1, -1],
        [11],
        [4],
        ]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().maxSlidingWindow(nums, k)
        assert solution == expected
