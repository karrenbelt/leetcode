### Source : https://leetcode.com/problems/broken-calculator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-22

##################################################################################################### 
#
# On a broken calculator that has a number showing on its display, we can perform two operations:
# 
# 	Double: ultiply the number on the display by 2, or;
# 	Decrement: Subtract 1 from the number on the display.
# 
# Initially, the calculator is displaying the number X.
# 
# Return the minimum number of operations needed to display the number Y.
# 
# Example 1:
# 
# Input: X = 2, Y = 3
# Output: 2
# Explanation: Use double operation and then decrement operation {2 -> 4 -> 3}.
# 
# Example 2:
# 
# Input: X = 5, Y = 8
# Output: 2
# Explanation: Use decrement and then double {5 -> 4 -> 8}.
# 
# Example 3:
# 
# Input: X = 3, Y = 10
# Output: 3
# Explanation:  Use double, decrement and double {3 -> 6 -> 5 -> 10}.
# 
# Example 4:
# 
# Input: X = 1024, Y = 1
# Output: 1023
# Explanation: Use decrement operations 1023 times.
# 
# Note:
# 
# 	1 <= X <= 109
# 	1 <= Y <= 109
#####################################################################################################


class Solution:
    def brokenCalc(self, X: int, Y: int) -> int:
        return X - Y if X >= Y else 1 + Y % 2 + self.brokenCalc(X, (Y + 1) // 2)

    def brokenCalc(self, X: int, Y: int) -> int:
        multiple, ctr = 1, 0
        while X * multiple < Y:
            multiple <<= 1
            ctr += 1
        diff = X * multiple - Y
        while diff:
            ctr += diff // multiple
            diff -= diff // multiple * multiple
            multiple >>= 1
        return ctr

    def brokenCalc(self, X: int, Y: int) -> int:  # O(n) time
        ctr = 0
        while Y > X:
            ctr += 1
            Y = Y + 1 if Y % 2 else Y >> 1
        return ctr + X - Y

    def brokenCalc(self, X: int, Y: int) -> int:  # O(log log n) time. Who the fuck comes up with this...

        d = (Y - 1) // X
        mul = bool(d)
        if d & 0xffff0000: d >>= 16; mul += 16
        if d & 0xff00: d >>= 8; mul += 8
        if d & 0xf0: d >>= 4; mul += 4
        if d & 0xc: d >>= 2; mul += 2
        if d & 2: d >>= 1; mul += 1

        dec = (X << mul) - Y & (1 << mul) - 1
        dec = (dec >> 1 & 0x55555555) + (dec & 0x55555555)
        dec = (dec >> 2 & 0x33333333) + (dec & 0x33333333)
        dec = (dec >> 4 & 0x0f0f0f0f) + (dec & 0x0f0f0f0f)
        dec = (dec >> 8 & 0x00ff00ff) + (dec & 0x00ff00ff)
        dec = (dec >> 16 & 0x0000ffff) + (dec & 0x0000ffff)

        return mul + dec + ((X << mul) - Y >> mul)


def test():
    numbers = [
        (2, 3),
        (5, 8),
        (3, 10),
        (1024, 1),
    ]
    expectations = [2, 2, 3, 1023]
    for (X, Y), expected in zip(numbers, expectations):
        solution = Solution().brokenCalc(X, Y)
        assert solution == expected
