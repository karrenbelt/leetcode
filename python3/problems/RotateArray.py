### Source : https://leetcode.com/problems/rotate-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given an array, rotate the array to the right by k steps, where k is non-negative.
# 
# Follow up:
# 
# 	Try to come up as many solutions as you can, there are at least 3 different ways to solve 
# this problem.
# 	Could you do it in-place with O(1) extra space?
# 
# Example 1:
# 
# Input: nums = [1,2,3,4,5,6,7], k = 3
# Output: [5,6,7,1,2,3,4]
# Explanation:
# rotate 1 steps to the right: [7,1,2,3,4,5,6]
# rotate 2 steps to the right: [6,7,1,2,3,4,5]
# rotate 3 steps to the right: [5,6,7,1,2,3,4]
# 
# Example 2:
# 
# Input: nums = [-1,-100,3,99], k = 2
# Output: [3,99,-1,-100]
# Explanation: 
# rotate 1 steps to the right: [99,-1,-100,3]
# rotate 2 steps to the right: [3,99,-1,-100]
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	-231 <= nums[i] <= 231 - 1
# 	0 <= k <= 105
#####################################################################################################

from typing import List


class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        n = k % len(nums)
        nums[:] = nums[-n:] + nums[:-n]

    def rotate(self, nums: List[int], k: int) -> None:
        from collections import deque
        queue = deque(nums)
        nums[:] = queue.rotate(k) or queue

    def rotate(self, nums: List[int], k: int) -> None:  # O(n) time and O(1) space

        def reverse(left, right):
            while left < right:
                nums[left], nums[right] = nums[right], nums[left]
                left, right = left + 1, right - 1

        if k <= 0:
            return
        k, end = k % len(nums), len(nums) - 1
        reverse(0, end - k)
        reverse(end - k + 1, end)
        reverse(0, end)

    def rotate(self, nums: List[int], k: int) -> None:  # O(n) time and O(1) space
        n, k, j = len(nums), k % len(nums), 0
        while n > 0 and k % n != 0:
            for i in range(k):
                nums[j + i], nums[len(nums) - k + i] = nums[len(nums) - k + i], nums[j + i]
            n, j = n - k, j + k
            k %= n


def test():
    arrays_of_numbers = [
        [1, 2, 3, 4, 5, 6, 7],
        [-1, -100, 3, 99],
        ]
    rotations = [3, 2]
    expectations = [
        [5, 6, 7, 1, 2, 3, 4],
        [3, 99, -1, -100]
        ]
    for nums, k, expected in zip(arrays_of_numbers, rotations, expectations):
        Solution().rotate(nums, k)
        assert nums == expected

