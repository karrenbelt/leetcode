### Source : https://leetcode.com/problems/n-ary-tree-level-order-traversal/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given an n-ary tree, return the level order traversal of its nodes' values.
# 
# Nary-Tree input serialization is represented in their level order traversal, each group of children 
# is separated by the null value (See examples).
# 
# Example 1:
# 
# Input: root = [1,null,3,2,4,null,5,6]
# Output: [[1],[3,2,4],[5,6]]
# 
# Example 2:
# 
# Input: root = 
# [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
# Output: [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
# 
# Constraints:
# 
# 	The height of the n-ary tree is less than or equal to 1000
# 	The total number of nodes is between [0, 104]
#####################################################################################################

from typing import List
from python3 import NAryTreeNode as Node


class Solution:

    def levelOrder(self, root: 'Node') -> List[List[int]]:
        queue, ret = [root], []
        while any(queue):
            ret.append([node.val for node in queue])
            queue = [child for node in queue for child in node.children if child]
        return ret

    def levelOrder(self, root: 'Node') -> List[List[int]]:
        if not root:
            return []

        def bfs(nodes):
            if not nodes:
                return
            result.append([node.val for node in nodes])
            bfs([child for node in nodes for child in node.children])

        result = [[root.val]]
        bfs(root.children)
        return result


def test():
    from python3 import NAryTreeCodec
    serialized_trees = [
        "[1,null,3,2,4,null,5,6]",
        "[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]",
        ]
    expectations = [
        [[1], [3, 2, 4], [5, 6]],
        [[1], [2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13], [14]]
        ]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = NAryTreeCodec.deserialize(serialized_tree)
        solution = Solution().levelOrder(root)
        assert solution == expected
