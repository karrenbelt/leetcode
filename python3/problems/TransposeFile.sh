### Source : https://leetcode.com/problems/transpose-file/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-22

##################################################################################################### 
#
# Given a text file file.txt, transpose its content.
# 
# You may assume that each row has the same number of columns, and each field is separated by the ' ' 
# character.
# 
# Example:
# 
# If file.txt has the following content:
# 
# name age
# alice 21
# ryan 30
# 
# Output the following:
# 
# name alice ryan
# age 21 30
#####################################################################################################


#!/bin/bash
python3 -c 'print("\n".join(map(" ".join, zip(*(l.split() for l in open("file.txt"))))))'

