### Source : https://leetcode.com/problems/rectangle-area/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-23

##################################################################################################### 
#
# Given the coordinates of two rectilinear rectangles in a 2D plane, return the total area covered by 
# the two rectangles.
# 
# The first rectangle is defined by its bottom-left corner (A, B) and its top-right corner (C, D).
# 
# The second rectangle is defined by its bottom-left corner (E, F) and its top-right corner (G, H).
# 
# Example 1:
# 
# Input: A = -3, B = 0, C = 3, D = 4, E = 0, F = -1, G = 9, H = 2
# Output: 45
# 
# Example 2:
# 
# Input: A = -2, B = -2, C = 2, D = 2, E = -2, F = -2, G = 2, H = 2
# Output: 16
# 
# Constraints:
# 
# 	-104 <= A, B, C, D, E, F, G, H <= 104
#####################################################################################################

class RectangularArea:

    __slots__ = ['x1', 'y1', 'x2', 'y2']

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    @property
    def width(self) -> int:
        return abs(self.x2 - self.x1)

    @property
    def height(self) -> int:
        return abs(self.y2 - self.y1)

    @property
    def area(self) -> int:
        return self.width * self.height

    def __int__(self) -> int:
        return self.area

    def __and__(self, other: 'RectangularArea') -> int:
        """overlap between two areas"""
        if not isinstance(other, type(self)):
            return NotImplemented
        return max(min(self.x2, other.x2) - max(self.x1, other.x1), 0) \
               * max(min(self.y2, other.y2) - max(self.y1, other.y1), 0)

    def __or__(self, other) -> int:
        """area covered by both rectangles"""
        return self + other

    def __xor__(self, other: 'RectangularArea') -> int:
        """non-overlapping areas"""
        if not isinstance(other, type(self)):
            return NotImplemented
        return self + other - (self & other)

    def __add__(self, other: 'RectangularArea') -> int:
        """area covered by both rectangles"""
        return self.area + other.area - (self & other)

    def __sub__(self, other: 'RectangularArea') -> int:
        """area self minus overlap of the other"""
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.area - (self & other)

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.area}>"  # object at {hex(id(self))}


class Solution:
    def computeArea(self, A: int, B: int, C: int, D: int, E: int, F: int, G: int, H: int) -> int:
        area_rectangle1 = (D - B) * (C - A)
        area_rectangle2 = (H - F) * (G - E)
        area_overlap = max(min(C, G) - max(A, E), 0) * max(min(D, H) - max(B, F), 0)
        return area_rectangle1 + area_rectangle2 - area_overlap

    def computeArea(self, A: int, B: int, C: int, D: int, E: int, F: int, G: int, H: int) -> int:
        return (D - B) * (C - A) + (H - F) * (G - E) - max(min(C, G) - max(A, E), 0) * max((min(D, H) - max(B, F)), 0)

    def computeArea(self, A: int, B: int, C: int, D: int, E: int, F: int, G: int, H: int) -> int:
        # area of the two rectangles minus that of their overlap.
        # In my opinion this exercise with 8 variables is about writing readable code
        return RectangularArea(A, B, C, D) + RectangularArea(E, F, G, H)


def test():
    arguments = [
        (-3, 0, 3, 4, 0, -1, 9, 2),
        (-2, -2, 2, 2, -2, -2, 2, 2),
        (-2, -2, 2, 2, 3, 3, 4, 4),
    ]
    expectations = [45, 16, 17]
    for (A, B, C, D, E, F, G, H), expected in zip(arguments, expectations):
        solution = Solution().computeArea(A, B, C, D, E, F, G, H)
        assert solution == expected, (expected, solution)
