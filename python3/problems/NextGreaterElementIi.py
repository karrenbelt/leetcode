### Source : https://leetcode.com/problems/next-greater-element-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-27

##################################################################################################### 
#
# Given a circular integer array nums (i.e., the next element of nums[nums.length - 1] is nums[0]), 
# return the next greater number for every element in nums.
# 
# The next greater number of a number x is the first greater number to its traversing-order next in 
# the array, which means you could search circularly to find its next greater number. If it doesn't 
# exist, return -1 for this number.
# 
# Example 1:
# 
# Input: nums = [1,2,1]
# Output: [2,-1,2]
# Explanation: The first 1's next greater number is 2; 
# The number 2 can't find next greater number. 
# The second 1's next greater number needs to search circularly, which is also 2.
# 
# Example 2:
# 
# Input: nums = [1,2,3,4,3]
# Output: [2,3,4,-1,4]
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def nextGreaterElements(self, nums: List[int]) -> List[int]:  # O(n^2) time
        n = len(nums)
        next_greater = [-1] * n
        for i in range(n):
            for j in range(i, i + n):
                if nums[j % n] > nums[i]:
                    next_greater[i] = nums[j % n]
                    break
        return next_greater

    def nextGreaterElements(self, nums: List[int]) -> List[int]:  # O(n^2) time
        n = len(nums)
        return [next((nums[j % n] for j in range(i, i + n) if nums[j % n] > nums[i]), -1) for i in range(n)]

    def nextGreaterElements(self, nums: List[int]) -> List[int]:  # O(n) time and O(n) space
        # use stack for lookup. All indices into a monotonic stack
        n = len(nums)
        stack, next_greater = [], [-1] * n
        for i in range(len(nums) * 2):
            while stack and nums[stack[-1]] < nums[i % n]:
                next_greater[stack.pop()] = nums[i % n]
            stack.append(i % n)
        return next_greater


def test():
    arguments = [
        [1, 2, 1],
        [1, 2, 3, 4, 3],
    ]
    expectations = [
        [2, -1, 2],
        [2, 3, 4, -1, 4],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().nextGreaterElements(nums)
        assert solution == expected
