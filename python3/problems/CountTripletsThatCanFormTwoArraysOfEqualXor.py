### Source : https://leetcode.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-10

##################################################################################################### 
#
# Given an array of integers arr.
# 
# We want to select three indices i, j and k where (0 <= i < j <= k < arr.length).
# 
# Let's define a and b as follows:
# 
# 	a = arr[i]  arr[i + 1]  ...  arr[j - 1]
# 	b = arr[j]  arr[j + 1]  ...  arr[k]
# 
# Note that  denotes the bitwise-xor operation.
# 
# Return the number of triplets (i, j and k) Where a == b.
# 
# Example 1:
# 
# Input: arr = [2,3,1,6,7]
# Output: 4
# Explanation: The triplets are (0,1,2), (0,2,2), (2,3,4) and (2,4,4)
# 
# Example 2:
# 
# Input: arr = [1,1,1,1,1]
# Output: 10
# 
# Example 3:
# 
# Input: arr = [2,3]
# Output: 0
# 
# Example 4:
# 
# Input: arr = [1,3,5,7,9]
# Output: 3
# 
# Example 5:
# 
# Input: arr = [7,11,12,9,5,2,7,17,22]
# Output: 8
# 
# Constraints:
# 
# 	1 <= arr.length <= 300
# 	1 <= arr[i] <= 108
#####################################################################################################

from typing import List


class Solution:
    def countTriplets(self, arr: List[int]) -> int:  # O(n ^ 4) time -> TLE: 40 / 47 test cases passed.
        # smells like dp, we brute force first
        from functools import reduce
        ctr = 0
        for i in range(len(arr)):
            for j in range(i + 1, len(arr)):
                for k in range(j, len(arr)):
                    a = reduce(lambda x, y: x ^ y, arr[i:j])
                    b = reduce(lambda x, y: x ^ y, arr[j:k + 1])
                    if a == b:
                        ctr += 1
        return ctr

    def countTriplets(self, arr: List[int]) -> int:  # dp: O(n ^ 2) time and O(n) space
        # realization: need to only xor:  arr[x] ^ arr[x ... y - 1] ^ arr[y] for each consecutive iteration
        ctr = 0
        dp = [0] * (len(arr) + 1)
        for i in range(1, len(arr) + 1):
            p = dp[i] = dp[i - 1] ^ arr[i - 1]
            for j in range(i - 1):
                if p == 0:
                    ctr += i - j - 1
                p ^= arr[j]
        return ctr

    def countTriplets(self, arr: List[int]) -> int:  # dp: O(n ^ 2) time and O(1) space
        # we need to find how many pairs have a prefix value that is equal.
        # First,  if arr[i]^arr[i+1].......^arr[j-1] == arr[j]^arr[j+1].......^arr[k] ,
        #         then arr[i]^arr[i+1].......^arr[j-1] ^arr[j]^arr[j+1].......^arr[k] = 0
        # Second, if arr[i]^arr[i+1].......^arr[j-1] ^arr[j]^arr[j+1].......^arr[k] == 0,
        #         at any position you split the arr, arr[i]^arr[i+1].......^arr[q-1] == arr[q]^arr[q+1].......^arr[k]
        #         That's to say, if we have valid (i,k), the number of different split is k-i
        ctr = 0
        arr.insert(0, 0)
        for i in range(1, len(arr)):  # calculate prefix array
            arr[i] ^= arr[i - 1]
        for i in range(len(arr)):
            for k in range(i + 1, len(arr)):
                if arr[i] == arr[k]:
                    ctr += k - i - 1
        return ctr


def test():
    arguments = [
        [2, 3, 1, 6, 7],
        [1, 1, 1, 1, 1],
        [2, 3],
        [1, 3, 5, 7, 9],
        [7, 11, 12, 9, 5, 2, 7, 17, 22],
        list(range(100)),
        ]
    expectations = [4, 10, 0, 3, 8, 22725]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().countTriplets(arr)
        assert solution == expected
