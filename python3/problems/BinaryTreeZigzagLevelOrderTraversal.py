### Source : https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/
### Author : karrenbelt
### Date   : 2019-05-15

##################################################################################################### 
#
# Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left 
# to right, then right to left for the next level and alternate between).
# 
# For example:
# Given binary tree [3,9,20,null,null,15,7],
# 
#     3
#    / \
#   9  20
#     /  \
#    15   7
# 
# return its zigzag level order traversal as:
# 
# [
#   [3],
#   [20,9],
#   [15,7]
# ]
# 
#####################################################################################################

from typing import List, Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:

        def zigzag(node: Optional[TreeNode], level: int) -> None:
            if node is None:
                return
            elif level >= len(result_list):
                result_list.append([])
            if level % 2:
                result_list[level].append(node.val)
            else:
                result_list[level].insert(0, node.val)  # slow, use deque
            zigzag(node.right, level + 1)
            zigzag(node.left, level + 1)

        result_list = []
        zigzag(root, 0)
        return result_list

    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:

        def dfs(node: TreeNode, i: int) -> None:
            if node:
                levels.setdefault(i, []).append(node.val)
                dfs(node.left, i + 1) or dfs(node.right, i + 1)

        (levels := {}) or dfs(root, 0)  # insertion ordered
        return [v[::-1] if k % 2 else v for k, v in levels.items()]

    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        stack, result, i = [root] if root else [], [], 0
        while stack and (i := i + 1):
            result.append([n.val for n in stack[::(-1, 1)[i % 2]]])
            stack = [c for n in stack for c in (n.left, n.right) if c]
        return result


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3,9,20,null,null,15,7]",
        "[1]",
        "[]",
        ]
    expectations = [
        [[3], [20, 9], [15, 7]],
        [[1]],
        [],
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().zigzagLevelOrder(root)
        assert solution == expected
