### Source : https://leetcode.com/problems/create-maximum-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-10

##################################################################################################### 
#
# You are given two integer arrays nums1 and nums2 of lengths m and n respectively. nums1 and nums2 
# represent the digits of two numbers. You are also given an integer k.
# 
# Create the maximum number of length k <= m + n from digits of the two numbers. The relative order 
# of the digits from the same array must be preserved.
# 
# Return an array of the k digits representing the answer.
# 
# Example 1:
# 
# Input: nums1 = [3,4,6,5], nums2 = [9,1,2,5,8,3], k = 5
# Output: [9,8,6,5,3]
# 
# Example 2:
# 
# Input: nums1 = [6,7], nums2 = [6,0,4], k = 5
# Output: [6,7,6,0,4]
# 
# Example 3:
# 
# Input: nums1 = [3,9], nums2 = [8,9], k = 3
# Output: [9,8,9]
# 
# Constraints:
# 
# 	m == nums1.length
# 	n == nums2.length
# 	1 <= m, n <= 500
# 	0 <= nums1[i], nums2[i] <= 9
# 	1 <= k <= m + n
# 
# Follow up: Try to optimize your time and space complexity.
#####################################################################################################

from typing import List


class Solution:
    def maxNumber(self, nums1: List[int], nums2: List[int], k: int) -> List[int]:
        # iterate over selections from 0 up to k from nums1, take the remainder from nums2
        # select the maximum subsequence out of both individual arrays
        # merge the two selections

        def select_max(nums: List[int], sequence_length: int):  # O(n) time
            selected = []
            size = len(nums)
            for j in range(size):
                while selected and len(selected) + size - j > sequence_length and selected[-1] < nums[j]:
                    selected.pop()
                selected.append(nums[j])
            return selected[:sequence_length]

        def merge(selection1: List[int], selection2: List[int]):  # O(n^2) time due to pop
            merged = []
            while selection1 or selection2:
                merged += [max(selection1, selection2).pop(0)]
            return merged

        maximum = []
        for i in range(k + 1):  # O(k) time
            if i <= len(nums1) and k - i <= len(nums2):
                maximum = max(maximum, merge(select_max(nums1, i), select_max(nums2, k - i)))
        return maximum


def test():
    arguments = [
        ([3, 4, 6, 5], [9, 1, 2, 5, 8, 3], 5),
        ([6, 7], [6, 0, 4], 5),
        ([3, 9], [8, 9], 3),
    ]
    expectations = [
        [9, 8, 6, 5, 3],
        [6, 7, 6, 0, 4],
        [9, 8, 9],
    ]
    for (nums1, nums2, k), expected in zip(arguments, expectations):
        solution = Solution().maxNumber(nums1, nums2, k)
        assert solution == expected
test()