### Source : https://leetcode.com/problems/longest-valid-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-03

##################################################################################################### 
#
# Given a string containing just the characters '(' and ')', find the length of the longest valid 
# (well-formed) parentheses substring.
# 
# Example 1:
# 
# Input: s = "(()"
# Output: 2
# Explanation: The longest valid parentheses substring is "()".
# 
# Example 2:
# 
# Input: s = ")()())"
# Output: 4
# Explanation: The longest valid parentheses substring is "()()".
# 
# Example 3:
# 
# Input: s = ""
# Output: 0
# 
# Constraints:
# 
# 	0 <= s.length <= 3 * 104
# 	s[i] is '(', or ')'.
#####################################################################################################


class Solution:
    def longestValidParentheses(self, s: str) -> int:  # O(n^2) time: TLE
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def is_valid(sub_s: str) -> bool:
            stack = []
            for bracket in sub_s:
                if bracket == ')':
                    if stack:
                        stack.pop()
                    else:
                        return False
                else:
                    stack.append(bracket)
            return not stack

        longest = 0
        s = s.lstrip(')').rstrip('(')
        for i in range(len(s)):
            for j in range(i + 2, len(s) + 1, 2):
                if is_valid(s[i:j]):
                    longest = max(longest, j - i)
        return longest

    def longestValidParentheses(self, s):  # dp: O(n) time and O(n) space
        dp, stack = [0] * (len(s) + 1), []
        for i in range(len(s)):
            if s[i] == '(':
                stack.append(i)
            else:
                if stack:
                    p = stack.pop()
                    dp[i + 1] = dp[p] + i - p + 1
        return max(dp)

    def longestValidParentheses(self, s):  # stack: O(n) time and O(n) space
        stk, max_len = [(")", -1)], 0
        for i in range(len(s)):
            if s[i] == ")" and stk[-1][0] == "(":
                stk.pop()
                max_len = max(max_len, i - stk[-1][1])
            else:
                stk.append((s[i], i))
        return max_len

    def longestValidParentheses(self, s: str) -> int:  # O(n) time and O(1) space

        def search(string, target_bracket: str):
            ctr = length = longest = 0
            for bracket in string:
                ctr = ctr + 1 if bracket == target_bracket else ctr - 1
                if ctr < 0:
                    ctr = length = 0
                else:
                    length += 1
                    if ctr == 0:
                        longest = max(length, longest)
            return longest

        return max(search(s, '('), search(reversed(s), ')'))


def test():
    arguments = ["(()", ")()())", "", "(()(()"]
    expectations = [2, 4, 0, 2]
    for s, expected in zip(arguments, expectations):
        solution = Solution().longestValidParentheses(s)
        assert solution == expected
