### Source : https://leetcode.com/problems/iterator-for-combination/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-09

##################################################################################################### 
#
# Design the CombinationIterator class:
# 
# 	CombinationIterator(string characters, int combinationLength) Initializes the object with a 
# string characters of sorted distinct lowercase English letters and a number combinationLength as 
# arguments.
# 	next() Returns the next combination of length combinationLength in lexicographical order.
# 	hasNext() Returns true if and only if there exists a next combination.
# 
# Example 1:
# 
# Input
# ["CombinationIterator", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
# [["abc", 2], [], [], [], [], [], []]
# Output
# [null, "ab", true, "ac", true, "bc", false]
# 
# Explanation
# CombinationIterator itr = new CombinationIterator("abc", 2);
# itr.next();    // return "ab"
# itr.hasNext(); // return True
# itr.next();    // return "ac"
# itr.hasNext(); // return True
# itr.next();    // return "bc"
# itr.hasNext(); // return False
# 
# Constraints:
# 
# 	1 <= combinationLength <= characters.length <= 15
# 	All the characters of characters are unique.
# 	At most 104 calls will be made to next and hasNext.
# 	It's guaranteed that all calls of the function next are valid.
#####################################################################################################


class CombinationIterator:

    def __init__(self, characters: str, combinationLength: int):

        def combinations(cur, idx):
            if len(cur) == combinationLength:
                yield ''.join(cur)
                return
            for i in range(idx, len(characters)):
                cur.append(characters[i])
                yield from combinations(cur, i + 1)
                cur.pop()

        self.generator = combinations([], 0)
        self.has_next = None

    def next(self) -> str:
        if not self.has_next:
            self.hasNext()
        has_next, self.has_next = self.has_next, None
        return has_next

    def hasNext(self) -> bool:
        if self.has_next is None:
            try:
                self.has_next = next(self.generator)
            except StopIteration:
                self.has_next = None
        return bool(self.has_next)

#
# class CombinationIterator:
#
#     def __init__(self, chars: str, comblen: int):
#         self.generator = self.genNext(chars, [], comblen, 0)
#         self.last, self.end = None, chars[-comblen:]
#
#     def genNext(self, chars, path, coml, st):
#         if coml == 0:
#             yield ''.join(path)
#             return
#         for i in range(st, len(chars)):
#             yield from self.genNext(chars, path + [chars[i]], coml - 1, i + 1)
#
#     def next(self) -> str:
#         self.last = next(self.generator)
#         return self.last
#
#     def hasNext(self) -> bool:
#         return self.last != self.end

# Your CombinationIterator object will be instantiated and called as such:
# obj = CombinationIterator(characters, combinationLength)
# param_1 = obj.next()
# param_2 = obj.hasNext()


def test():
    operations = ["CombinationIterator", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
    arguments = [["abc", 2], [], [], [], [], [], []]
    expectations = [None, "ab", True, "ac", True, "bc", False]
    obj = CombinationIterator(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected

