### Source : https://leetcode.com/problems/all-nodes-distance-k-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-14

##################################################################################################### 
#
# We are given a binary tree (with root node root), a target node, and an integer value K.
# 
# Return a list of the values of all nodes that have a distance K from the target node.  The answer 
# can be returned in any order.
# 
# Example 1:
# 
# Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2
# 
# Output: [7,4,1]
# 
# Explanation: 
# The nodes that are a distance 2 from the target node (with value 5)
# have values 7, 4, and 1.
# 
# Note that the inputs "root" and "target" are actually TreeNodes.
# The descriptions of the inputs above are just serializations of these objects.
# 
# Note:
# 
# 	The given tree is non-empty.
# 	Each node in the tree has unique values 0 <= node.val <= 500.
# 	The target node is a node in the tree.
# 	0 <= K <= 1000.
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def distanceK(self, root: TreeNode, target: TreeNode, K: int) -> List[int]:  # 36 ms

        def make_graph(node: TreeNode):
            if not node:
                return
            if node.left:
                graph[node.left] = [node]
                graph[node].append(node.left)
                make_graph(node.left)
            if node.right:
                graph[node.right] = [node]
                graph[node].append(node.right)
                make_graph(node.right)

        def find_k_distance_neighbors(node: TreeNode, distance: int):
            if not node:
                return
            elif distance == K:
                k_distance_neighbors.append(node.val)
            for neighbor in graph[node]:
                if neighbor not in seen:
                    seen.add(neighbor)
                    find_k_distance_neighbors(neighbor, distance + 1)

        seen = {target}
        graph = {root: []}
        k_distance_neighbors = []
        make_graph(root)
        find_k_distance_neighbors(target, 0)
        return k_distance_neighbors

    def distanceK(self, root: TreeNode, target: TreeNode, K: int) -> List[int]:  # 36 ms
        from collections import deque

        queue = deque([root])
        graph = {root: []}
        while queue:
            node = queue.popleft()
            if node.left:
                graph[node.left] = [node]
                graph[node].append(node.left)
                queue.append(node.left)
            if node.right:
                graph[node.right] = [node]
                graph[node].append(node.right)
                queue.append(node.right)

        k_distance_neighbors = []
        queue = deque([{target}])
        seen = {target}
        distance = 0
        while queue:
            nodes = queue.popleft()
            if distance == K:
                k_distance_neighbors.extend([node.val for node in nodes])
                break
            neighbors = set(sum([graph[node] for node in nodes], []))
            unseen_neighbors = neighbors.difference(seen)
            seen.update(unseen_neighbors)
            queue.append(unseen_neighbors)
            distance += 1

        return k_distance_neighbors

    def distanceK(self, root: TreeNode, target: TreeNode, K: int) -> List[int]:  # 28 ms
        from collections import deque

        def add_parent(node: TreeNode, parent):
            if not node:
                return
            node.parent = parent
            add_parent(node.left, node)
            add_parent(node.right, node)

        add_parent(root, None)
        distance = 0
        seen = {target}
        queue = deque([seen])
        k_distance_neighbors = []
        while queue:
            nodes = queue.popleft()
            if distance == K:
                k_distance_neighbors.extend([node.val for node in nodes])
                break
            neighbors = set(sum([list(filter(None, [node.left, node.right, node.parent])) for node in nodes], []))
            unseen_neighbors = neighbors.difference(seen)
            seen.update(unseen_neighbors)
            queue.append(unseen_neighbors)
            distance += 1
        return k_distance_neighbors


def test():
    from python3 import Codec
    arguments = [
        ("[3,5,1,6,2,0,8,null,null,7,4]", 5, 2),  # 5 here denotes TreeNode(5) of the same identify as in the tree
    ]
    expectations = [
        [7, 4, 1],
    ]
    for (serialized_tree, target, K), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().distanceK(root, root.left, K)
        assert set(solution) == set(expected), solution
