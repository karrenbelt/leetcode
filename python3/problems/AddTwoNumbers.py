### Source : https://leetcode.com/problems/add-two-numbers/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-19

##################################################################################################### 
#
# You are given two non-empty linked lists representing two non-negative integers. The digits are 
# stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and 
# return the sum as a linked list.
# 
# You may assume the two numbers do not contain any leading zero, except the number 0 itself.
# 
# Example 1:
# 
# Input: l1 = [2,4,3], l2 = [5,6,4]
# Output: [7,0,8]
# Explanation: 342 + 465 = 807.
# 
# Example 2:
# 
# Input: l1 = [0], l2 = [0]
# Output: [0]
# 
# Example 3:
# 
# Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
# Output: [8,9,9,9,0,0,0,1]
# 
# Constraints:
# 
# 	The number of nodes in each linked list is in the range [1, 100].
# 	0 <= Node.val <= 9
# 	It is guaranteed that the list represents a number that does not have leading zeros.
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:  # O(n) time, O(n) space
        if not l1 or not l2:
            return l1 if l1 else l2
        carry, val = divmod(l1.val + l2.val, 10)
        head = ListNode(val)
        prev, node = head, head
        l1, l2 = l1.next, l2.next
        while l1 or l2:
            carry, val = divmod((l1.val if l1 else 0) + (l2.val if l2 else 0) + carry, 10)
            node = ListNode(val)
            prev.next, prev = node, node
            l1, l2 = l1.next if l1 else None, l2.next if l2 else None
        if carry > 0:
            node.next = ListNode(carry)
        return head

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        carry = 0
        dummy = node = ListNode(0)
        while l1 or l2 or carry:
            v1 = v2 = 0
            if l1:
                v1, l1 = l1.val, l1.next
            if l2:
                v2, l2 = l2.val, l2.next
            carry, val = divmod(v1 + v2 + carry, 10)
            node.next = ListNode(val)
            node = node.next
        return dummy.next

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        carry = 0
        dummy = node = ListNode(0)
        while l1 or l2:
            carry, val = divmod((l1.val if l1 else 0) + (l2.val if l2 else 0) + carry, 10)
            new = ListNode(value=val)
            node.next = node = new
            l1 = l1.next if l1 else None
            l2 = l2.next if l2 else None
        if carry:
            node.next = ListNode(value=carry)
        return dummy.next


def test():
    from python3 import SinglyLinkedList
    numbers = [
        ([2, 4, 3], [5, 6, 4]),
        ([0], [0]),
        ([9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9]),
        ]
    expectations = [
        [7, 0, 8],
        [0],
        [8, 9, 9, 9, 0, 0, 0, 1],
        ]
    for (nums1, nums2), expected in zip(numbers, expectations):
        l1, l2 = map(lambda nums: SinglyLinkedList(nums).head, (nums1, nums2))
        solution = Solution().addTwoNumbers(l1, l2)
        assert solution == SinglyLinkedList(expected).head
