### Source : https://leetcode.com/problems/h-index/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-12

##################################################################################################### 
#
# Given an array of integers citations where citations[i] is the number of citations a researcher 
# received for their ith paper, return compute the researcher's h-index.
# 
# According to the definition of h-index on Wikipedia: A scientist has an index h if h of their n 
# papers have at least h citations each, and the other n &minus; h papers have no more than h 
# citations each.
# 
# If there are several possible values for h, the maximum one is taken as the h-index.
# 
# Example 1:
# 
# Input: citations = [3,0,6,1,5]
# Output: 3
# Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had received 
# 3, 0, 6, 1, 5 citations respectively.
# Since the researcher has 3 papers with at least 3 citations each and the remaining two with no more 
# than 3 citations each, their h-index is 3.
# 
# Example 2:
# 
# Input: citations = [1,3,1]
# Output: 1
# 
# Constraints:
# 
# 	n == citations.length
# 	1 <= n <= 5000
# 	0 <= citations[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def hIndex(self, citations: List[int]) -> int:  # O(n log n) time and O(1) space
        citations.sort()
        for i, n in enumerate(citations):
            if n >= (len(citations) - i):
                return len(citations) - i
        return 0

    def hIndex(self, citations: List[int]) -> int:  # O(n log n) time and O(1) space
        return sum(i < j for i, j in enumerate(sorted(citations, reverse=True)))

    def hIndex(self, citations: List[int]) -> int:  # bucket sort: O(n) time and O(n) space
        n = len(citations)
        buckets = [0] * (n + 1)
        for c in citations:
            buckets[min(n, c)] += 1
        i = n
        s = buckets[n]
        while i > s:
            i -= 1
            s += buckets[i]
        return i


def test():
    arguments = [
        [3, 0, 6, 1, 5],
        [1, 3, 1],
        [1],
        ]
    expectations = [3, 1, 1]
    for citations, expected in zip(arguments, expectations):
        solution = Solution().hIndex(citations)
        assert solution == expected
