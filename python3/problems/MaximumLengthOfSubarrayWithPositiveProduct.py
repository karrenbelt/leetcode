### Source : https://leetcode.com/problems/maximum-length-of-subarray-with-positive-product/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-20

##################################################################################################### 
#
# Given an array of integers nums, find the maximum length of a subarray where the product of all its 
# elements is positive.
# 
# A subarray of an array is a consecutive sequence of zero or more values taken out of that array.
# 
# Return the maximum length of a subarray with positive product.
# 
# Example 1:
# 
# Input: nums = [1,-2,-3,4]
# Output: 4
# Explanation: The array nums already has a positive product of 24.
# 
# Example 2:
# 
# Input: nums = [0,1,-2,-3,-4]
# Output: 3
# Explanation: The longest subarray with positive product is [1,-2,-3] which has a product of 6.
# Notice that we cannot include 0 in the subarray since that'll make the product 0 which is not 
# positive.
# 
# Example 3:
# 
# Input: nums = [-1,-2,-3,0,1]
# Output: 2
# Explanation: The longest subarray with positive product is [-1,-2] or [-2,-3].
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def getMaxLen(self, nums: List[int]) -> int:
        from itertools import groupby

        max_len = 0
        for g in (g for b, g in groupby(nums, key=lambda x: not x) if not b):
            positive = negative = 0
            for n in g:
                if n > 0:
                    positive, negative = positive + 1, negative + bool(negative)
                else:
                    positive, negative = negative + bool(negative), positive + 1
                max_len = max(max_len, positive)

        return max_len

    def getMaxLen(self, nums: List[int]) -> int:
        max_len = positive = negative = 0
        for n in nums:
            if n > 0:
                positive, negative = positive + 1, negative + bool(negative)
            elif n < 0:
                positive, negative = negative + bool(negative), positive + 1
            else:
                positive = negative = 0
            max_len = max(max_len, positive)
        return max_len


def test():
    strings = [
        [1, -2, -3, 4],
        [0, 1, -2, -3, -4],
        [-1, -2, -3, 0, 1],
    ]
    expectations = [4, 3, 2]
    for nums, expected in zip(strings, expectations):
        solution = Solution().getMaxLen(nums)
        assert solution == expected
