### Source : https://leetcode.com/problems/longest-substring-without-repeating-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-19

##################################################################################################### 
#
# Given a string s, find the length of the longest substring without repeating characters.
# 
# Example 1:
# 
# Input: s = "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.
# 
# Example 2:
# 
# Input: s = "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# 
# Example 3:
# 
# Input: s = "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
# Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
# 
# Example 4:
# 
# Input: s = ""
# Output: 0
# 
# Constraints:
# 
# 	0 <= s.length <= 5 * 104
# 	s consists of English letters, digits, symbols and spaces.
#####################################################################################################


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:  # O(n) time and O(m) space
        seen = {}
        current = longest = left = 0
        for right, c in enumerate(s):
            if seen.get(c, -1) >= left:
                left = seen[c] + 1
                current = right - seen[c]
            else:
                current += 1
                longest = max(longest, current)
            seen[c] = right
        return longest

    def lengthOfLongestSubstring(self, s: str) -> int:  # O(n) time and O(m) space
        seen = {}
        longest = left = 0
        for right, c in enumerate(s):
            if c in seen:
                left = max(left, seen[c] + 1)
            seen[c] = right
            longest = max(longest, right - left + 1)
        return longest

    def lengthOfLongestSubstring(self, s: str) -> int:  # O(n) time
        left = right = longest = 0
        while right < len(s):
            longest = max(longest, right - left)
            if s[right] in s[left:right]:
                left += 1
            else:
                right += 1
        return max(longest, right - left) if s else 0

    def lengthOfLongestSubstring(self, s: str) -> int:  # O(n) time and O(m) space
        seen, longest = '', 0
        for c in s:
            seen = seen[seen.index(c) + 1:] + c if c in seen else seen + c
            longest = max(longest, len(seen))
        return longest

    def lengthOfLongestSubstring(self, s: str) -> int:  # O(n) time and O(1) space
        last_index = [-1] * 256
        current = longest = 0
        for i, unicode in enumerate(map(ord, s)):
            current = max(current, last_index[unicode] + 1)
            longest = max(longest, i - current + 1)
            last_index[unicode] = i
        return longest


def test():
    strings = ["abcabcbb", "bbbbb", "pwwkew", "", "au"]
    expectations = [3, 1, 3, 0, 2]
    for string, expected in zip(strings, expectations):
        sol = Solution().lengthOfLongestSubstring(string)
        assert sol == expected
test()