### Source : https://leetcode.com/problems/ways-to-split-array-into-three-subarrays/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-07

##################################################################################################### 
#
# A split of an integer array is good if:
# 
# 	The array is split into three non-empty contiguous subarrays - named left, mid, right 
# respectively from left to right.
# 	The sum of the elements in left is less than or equal to the sum of the elements in mid, 
# and the sum of the elements in mid is less than or equal to the sum of the elements in right.
# 
# Given nums, an array of non-negative integers, return the number of good ways to split nums. As the 
# number may be too large, return it modulo 109 + 7.
# 
# Example 1:
# 
# Input: nums = [1,1,1]
# Output: 1
# Explanation: The only good way to split nums is [1] [1] [1].
# 
# Example 2:
# 
# Input: nums = [1,2,2,2,5,0]
# Output: 3
# Explanation: There are three good ways of splitting nums:
# [1] [2] [2,2,5,0]
# [1] [2,2] [2,5,0]
# [1,2] [2,2] [5,0]
# 
# Example 3:
# 
# Input: nums = [3,2,1]
# Output: 0
# Explanation: There is no good way to split nums.
# 
# Constraints:
# 
# 	3 <= nums.length <= 105
# 	0 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def waysToSplit(self, nums: List[int]) -> int:  # MLE -> 49 / 87 test cases passed.
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int, a: int, b: int, c: int):
            nonlocal ctr
            if a <= b <= c:
                ctr += 1
            if i < j - 1:
                dfs(i + 1, j, a + nums[i], b - nums[i], c)
            if j < len(nums) - 1:
                dfs(i, j + 1, a, b + nums[j], c - nums[j])

        ctr = 0
        dfs(1, 2, nums[0], nums[1], sum(nums[2:]))
        return ctr

    def waysToSplit(self, nums: List[int]) -> int:  # O(n) time
        # two-pointer
        # i = last element of the first array, j last element of the second array, k first element of the last array
        for i in range(len(nums) - 1):  # prefix sum
            nums[i + 1] += nums[i]

        ctr = j = k = 0
        for i in range(len(nums) - 2):
            while j <= i or (j < len(nums) - 1 and nums[j] < nums[i] * 2):
                j += 1
            while k < j or (k < len(nums) - 1 and nums[k] - nums[i] <= nums[-1] - nums[k]):
                k += 1
            ctr += k - j
        return ctr % (10**9 + 7)

    def waysToSplit(self, nums: List[int]) -> int:  # O(n log n) time and O(n) space
        # prefix sum + binary search
        import bisect
        prefix = [0]
        for n in nums:
            prefix.append(prefix[-1] + n)

        ctr = 0
        for i in range(1, len(nums)):
            j = bisect.bisect_left(prefix, 2 * prefix[i])
            k = bisect.bisect_right(prefix, (prefix[i] + prefix[-1]) // 2)
            ctr += max(0, min(len(nums), k) - max(i + 1, j))
        return ctr % 1_000_000_007


def test():
    arguments = [
        [1, 1, 1],
        [1, 2, 2, 2, 5, 0],
        [3, 2, 1],
    ]
    expectations = [1, 3, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().waysToSplit(nums)
        assert solution == expected, (expected, solution)
