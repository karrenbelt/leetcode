### Source : https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-20

##################################################################################################### 
#
# Return the root node of a binary search tree that matches the given preorder traversal.
# 
# (Recall that a binary search tree is a binary tree where for every node, any descendant of 
# node.left has a value < node.val, and any descendant of node.right has a value > node.val.  Also 
# recall that a preorder traversal displays the value of the node first, then traverses node.left, 
# then traverses node.right.)
# 
# Example 1:
# 
# Input: [8,5,1,7,10,12]
# Output: [8,5,10,1,7,null,12]
# 
# Note: 
# 
# 	1 <= preorder.length <= 100
# 	The values of preorder are distinct.
# 
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def bstFromPreorder(self, preorder: List[int]) -> TreeNode:  # O(n) time and O(n) space since copying
        if preorder:
            return
        root = TreeNode(preorder[0])

        i = 1
        while i < len(preorder) and preorder[i] < preorder[0]:
            i += 1

        root.left = self.bstFromPreorder(preorder[1:i])  # rather pass pointers, slicing copies the array
        root.right = self.bstFromPreorder(preorder[i:])
        return root

    def bstFromPreorder(self, preorder: List[int]) -> TreeNode:  # O(n) time and O(h) space

        def build(left: int, right: int) -> TreeNode:
            if left < right:
                i = left + 1
                while i < right and preorder[i] < preorder[left]:
                    i += 1
                node = TreeNode(preorder[left])
                node.left = build(left + 1, i)
                node.right = build(i, right)
                return node

        return build(0, len(preorder))

    def bstFromPreorder(self, preorder: List[int]) -> TreeNode:  # O(n) time and O(h) space

        def dfs(ceil: int) -> TreeNode:
            return TreeNode(preorder[-1], dfs(preorder.pop()), dfs(ceil)) if preorder and preorder[-1] <= ceil else None

        return preorder.reverse() or dfs(1 << 31)

    def bstFromPreorder(self, preorder: List[int]) -> TreeNode:
        root = TreeNode(preorder.pop(0))
        stack = [root]
        for n in preorder:
            node = TreeNode(n)
            if stack[-1].val > n:
                stack[-1].left = node
                stack.append(node)
            else:
                prev_node = stack.pop()
                while stack and stack[-1].val <= n:
                    prev_node = stack.pop()
                prev_node.right = node
                stack.append(node)
        return root


def test():
    from python3 import Codec
    arguments = [
        [8, 5, 1, 7, 10, 12],
        [1, 3],
    ]
    expectations = [
        "[8,5,10,1,7,null,12]",
        "[1,null,3]",
    ]
    for preorder, expected in zip(arguments, expectations):
        solution = Solution().bstFromPreorder(preorder)
        assert solution == Codec.deserialize(expected)
