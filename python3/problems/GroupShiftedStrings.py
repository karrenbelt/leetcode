### Source : https://leetcode.com/problems/group-shifted-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-07

##################################################################################################### 
#
# Given a string, we can "shift" each of its letter to its successive letter,
# for example: "abc" -> "bcd". We can keep "shifting" which forms the sequence:
#    "abc" -> "bcd" -> ... -> "xyz"
#
# Given a list of strings which contains only lowercase alphabets,
# group all strings that belong to the same shifting sequence.
#
# For example, given: ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"],
# Return:
#
# [
# ["abc","bcd","xyz"],
# ["az","ba"],
# ["acef"],
# ["a","z"]
# ]
#
#
#  Note: For the return value, each inner list's elements must follow the lexicographic order.
#
#####################################################################################################

from typing import List


class Solution:
    def groupStrings(self, strings: List[str]) -> List[List[str]]:  # O(n*m) time and O(n) spoce

        def compute_distance(a: str, b: str) -> int:
            val, val2 = ord(a) - ord("a"), ord(b) - ord("a")
            return val2 - val if val < val2 else (26 - val) + val2

        shift_groups = dict()
        for s in strings:
            shift = []
            for i in range(1, len(s)):
                distance = compute_distance(s[i - 1], s[i])
                shift.append(distance)
            shift_groups.setdefault(tuple(shift), []).append(s)

        return list(shift_groups.values())

    def groupStrings(self, strings: List[str]) -> List[List[str]]:

        def shift_letter(letter: str, shift: int):
            return chr((ord(letter) - shift) % 26 + ord('a'))

        def get_hash(string: str):
            shift = ord(string[0])
            return ''.join(shift_letter(letter, shift) for letter in string)

        shift_groups = dict()
        for s in strings:
            key = get_hash(s)
            shift_groups.setdefault(key, []).append(s)

        return list(shift_groups.values())


def test():
    arguments = [
        ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"],
    ]
    expectations = [
        [
            ["abc", "bcd", "xyz"],
            ["az", "ba"],
            ["acef"],
            ["a", "z"]
        ]
    ]
    for strings, expected in zip(arguments, expectations):
        solution = Solution().groupStrings(strings)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
