### Source : https://leetcode.com/problems/longest-word-in-dictionary-through-deleting/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-22

##################################################################################################### 
#
# 
# Given a string and a string dictionary, find the longest string in the dictionary that can be 
# formed by deleting some characters of the given string. If there are more than one possible 
# results, return the longest word with the smallest lexicographical order. If there is no possible 
# result, return the empty string.
# 
# Example 1:
# 
# Input:
# s = "abpcplea", d = ["ale","apple","monkey","plea"]
# 
# Output: 
# "apple"
# 
# Example 2:
# 
# Input:
# s = "abpcplea", d = ["a","b","c"]
# 
# Output: 
# "a"
# 
# Note:
# 
# All the strings in the input will only contain lower-case letters.
# The size of the dictionary won't exceed 1,000.
# The length of all the strings in the input won't exceed 1,000.
# 
#####################################################################################################

from typing import List


class TrieNode(dict):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indices = []


class Solution:
    def findLongestWord(self, s: str, d: List[str]) -> str:  # TLE: 18/31

        trie = TrieNode()
        for i, word in enumerate(d):
            node = trie
            for c in word:
                if c not in node:
                    node[c] = TrieNode()
                node = node[c]
            node.indices.append(i)

        def dfs(index, node, level):
            if index <= len(s):
                found.setdefault(level, set()).update(node.indices)

            for i in range(index, len(s)):
                if s[i] in node:
                    dfs(i + 1, node[s[i]], level + 1)

        found = {}
        dfs(0, trie, 0)
        indices = max(found.items())[-1]
        return sorted([d[i] for i in indices])[0] if indices else ""

    def findLongestWord(self, s: str, d: List[str]) -> str:

        def check(s: str, w: str):
            i = 0
            for c in s:
                if c == w[i]:
                    i += 1
                    if i == len(w):
                        return True
            return False

        longest_word = ''
        for word in d:
            if check(s, word):
                if len(word) > len(longest_word) or len(word) == len(longest_word) and word < longest_word:
                    longest_word = word
        return longest_word

    def findLongestWord(self, s: str, d: List[str]) -> str:
        for word in sorted(d, key=lambda w: (-len(w), w)):
            it = iter(s)
            if all(c in it for c in word):
                return word
        return ""

    def findLongestWord(self, s: str, d: List[str]) -> str:
        import heapq
        heap = [(-len(word), word) for word in d]
        heapq.heapify(heap)
        while heap:
            word = heapq.heappop(heap)[1]
            it = iter(s)
            if all(c in it for c in word):
                return word
        return ""


def test():
    strings = ["abpcplea", "abpcplea", "bab", "apple"]
    dictionaries = [
        ["ale", "apple", "monkey", "plea"],
        ["a", "b", "c"],
        ["ba", "ab", "a", "b"],
        ["zxc", "vbn"],
        ]
    expectations = ["apple", "a", "ab", ""]
    for s, d, expected in zip(strings, dictionaries, expectations):
        solution = Solution().findLongestWord(s, d)
        assert solution == expected
