### Source : https://leetcode.com/problems/score-of-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-24

##################################################################################################### 
#
# Given a balanced parentheses string S, compute the score of the string based on the following rule:
# 
# 	() has score 1
# 	AB has score A + B, where A and B are balanced parentheses strings.
# 	(A) has score 2 * A, where A is a balanced parentheses string.
# 
# Example 1:
# 
# Input: "()"
# Output: 1
# 
# Example 2:
# 
# Input: "(())"
# Output: 2
# 
# Example 3:
# 
# Input: "()()"
# Output: 2
# 
# Example 4:
# 
# Input: "(()(()))"
# Output: 6
# 
# Note:
# 
# 	S is a balanced parentheses string, containing only ( and ).
# 	2 <= S.length <= 50
# 
#####################################################################################################

class Solution:
    def scoreOfParentheses(self, S: str) -> int:  # O(n) time and O(n) space
        stack, score = [], 0
        for i in S:
            if i == '(':
                stack.append(score)
                score = 0
            else:
                score += stack.pop() + max(score, 1)
        return score

    def scoreOfParentheses(self, S: str) -> int:  # O(n) time and O(1) space
        ans, bal = 0, 0
        for i, s in enumerate(S):
            bal = bal + 1 if s == "(" else bal - 1
            if i > 0 and S[i - 1: i + 1] == "()":
                ans += 1 << bal
        return ans

    def scoreOfParentheses(self, S: str) -> int:  # O(n) time and O(1) space
        ans = bal = 0
        for i, s in enumerate(S):
            bal += 1 if s == "(" else -1
            ans += 1 << bal if i > 0 and S[i - 1: i + 1] == "()" else 0
        return ans

    def scoreOfParentheses(self, S: str) -> int:  # O(n) time and O(1) space
        return eval(S.replace(')(', ')+(').replace('()', '1').replace(')', ')*2'))


def test():
    strings = [
        "()",
        "(())",
        "()()",
        "(()(()))",
        ]
    expectations = [1, 2, 2, 6]
    for S, expected in zip(strings, expectations):
        solution = Solution().scoreOfParentheses(S)
        assert solution == expected, solution
