### Source : https://leetcode.com/problems/count-submatrices-with-all-ones/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-19

##################################################################################################### 
#
# Given a rows * columns matrix mat of ones and zeros, return how many submatrices have all ones.
# 
# Example 1:
# 
# Input: mat = [[1,0,1],
#               [1,1,0],
#               [1,1,0]]
# Output: 13
# Explanation:
# There are 6 rectangles of side 1x1.
# There are 2 rectangles of side 1x2.
# There are 3 rectangles of side 2x1.
# There is 1 rectangle of side 2x2. 
# There is 1 rectangle of side 3x1.
# Total number of rectangles = 6 + 2 + 3 + 1 + 1 = 13.
# 
# Example 2:
# 
# Input: mat = [[0,1,1,0],
#               [0,1,1,1],
#               [1,1,1,0]]
# Output: 24
# Explanation:
# There are 8 rectangles of side 1x1.
# There are 5 rectangles of side 1x2.
# There are 2 rectangles of side 1x3. 
# There are 4 rectangles of side 2x1.
# There are 2 rectangles of side 2x2. 
# There are 2 rectangles of side 3x1. 
# There is 1 rectangle of side 3x2. 
# Total number of rectangles = 8 + 5 + 2 + 4 + 2 + 2 + 1 = 24.
# 
# Example 3:
# 
# Input: mat = [[1,1,1,1,1,1]]
# Output: 21
# 
# Example 4:
# 
# Input: mat = [[1,0,1],[0,1,0],[1,0,1]]
# Output: 5
# 
# Constraints:
# 
# 	1 <= rows <= 150
# 	1 <= columns <= 150
# 	0 <= mat[i][j] <= 1
#####################################################################################################

from typing import List


class Solution:
    def numSubmat(self, mat: List[List[int]]) -> int:  # O(m * n^2) time and O(m * n) space

        dp = [[0] * len(mat[0]) for _ in range(len(mat))]
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                if i == 0 and mat[i][j]:
                    dp[i][j] = 1
                elif mat[i][j]:
                    dp[i][j] = dp[i - 1][j] + 1

        total = 0
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                for k in range(j + 1, len(mat[0]) + 1):
                    total += min(dp[i][j:k])
        return total

    def numSubmat(self, mat: List[List[int]]) -> int:  # wrong still

        for i in range(len(mat)):
            for j in range(len(mat[0]) - 2, -1, -1):
                if mat[i][j]:
                    mat[i][j] = mat[i][j + 1] + 1

        ctr = 0
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                if mat[i][j]:
                    for k in range(i, len(mat)):
                        ctr += min(mat[i][j], mat[k][j])

        return ctr

    # def numSubmat(self, mat: List[List[int]]) -> int:  # O(m * n) time and O(1) space
    #     # similar to problems 84, 85, 363 and 1074
    #     for i in range(1, len(mat)):
    #         for j in range(len(mat[0])):
    #             if mat[i][j]:
    #                 mat[i][j] += mat[i - 1][j]
    #
    #     ctr = 0
    #     for i in range(len(mat)):
    #         stack = [(0, -1)]
    #         for j in range(len(mat[0]) + 1):
    #             current = mat[i][j] if j < len(mat[0]) else 0  # padding 0 at the end of each row
    #
    #             while current < stack[-1][0]:
    #                 height, index = stack.pop()
    #                 previous = stack[-1][1]
    #                 ctr += height * (index - previous) * (j - index)  # number of sub-rectangles with height 1 to h
    #             stack.append((current, j))
    #
    #     return ctr


def test():
    arguments = [
        [[1, 0, 1],
         [1, 1, 0],
         [1, 1, 0]],

        [[0, 1, 1, 0],
         [0, 1, 1, 1],
         [1, 1, 1, 0]],

        [[1, 1, 1, 1, 1, 1]],

        [[1, 0, 1],
         [0, 1, 0],
         [1, 0, 1]],
    ]
    expectations = [13, 24, 21, 5]
    for mat, expected in zip(arguments, expectations):
        # 1/0
        solution = Solution().numSubmat(mat)
        assert solution == expected
test()