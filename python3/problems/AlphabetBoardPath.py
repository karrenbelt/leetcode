### Source : https://leetcode.com/problems/alphabet-board-path/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-18

##################################################################################################### 
#
# On an alphabet board, we start at position (0, 0), corresponding to character board[0][0].
# 
# Here, board = ["abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"], as shown in the diagram below.
# 
# We may make the following moves:
# 
# 	'U' moves our position up one row, if the position exists on the board;
# 	'D' moves our position down one row, if the position exists on the board;
# 	'L' moves our position left one column, if the position exists on the board;
# 	'R' moves our position right one column, if the position exists on the board;
# 	'!' adds the character board[r][c] at our current position (r, c) to the answer.
# 
# (Here, the only positions that exist on the board are positions with letters on them.)
# 
# Return a sequence of moves that makes our answer equal to target in the minimum number of moves.  
# You may return any path that does so.
# 
# Example 1:
# Input: target = "leet"
# Output: "DDR!UURRR!!DDD!"
# Example 2:
# Input: target = "code"
# Output: "RR!DDRR!UUL!R!"
# 
# Constraints:
# 
# 	1 <= target.length <= 100
# 	target consists only of English lowercase letters.
#####################################################################################################


class Solution:
    def alphabetBoardPath(self, target: str) -> str:  # O(n) time and O(1) space
        # 'z' is special, we need to go up and left first whenever we can to avoid stepping off of the board
        board = ["abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"]
        positions = {c: (i, j) for i, row in enumerate(board) for j, c in enumerate(row)}
        i, j = (0, 0)
        sequence = ""
        for c in target:
            x, y = positions[c]
            if y < j:
                sequence += 'L' * (j - y)
            if x < i:
                sequence += 'U' * (i - x)
            if x > i:
                sequence += 'D' * (x - i)
            if y > j:
                sequence += 'R' * (y - j)
            sequence += '!'
            i, j = x, y
        return sequence


def test():
    board = ["abcde", "fghij", "klmno", "pqrst", "uvwxy", "z"]
    positions = {(i, j): c for i, row in enumerate(board) for j, c in enumerate(row)}
    directions = {'U': (-1, 0), 'D': (1, 0), 'L': (0, -1), 'R': (0, 1)}

    def verify(target, solution):
        produced = ""
        i, j = 0, 0
        for c in solution:
            if c == '!':
                produced += positions[i, j]
            else:
                x, y = directions[c]
                i, j = i + x, j + y
                if (i, j) not in positions:
                    return False
        return target == produced

    arguments = [
        "leet",
        "code",
    ]
    expectations = [  # any sequence that works can be submitted
        "DDR!UURRR!!DDD!",
        "RR!DDRR!UUL!R!",
    ]
    for target, expected in zip(arguments, expectations):
        solution = Solution().alphabetBoardPath(target)
        assert verify(target, solution)
        # assert solution == expected
