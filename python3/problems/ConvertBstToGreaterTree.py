### Source : https://leetcode.com/problems/convert-bst-to-greater-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-09

##################################################################################################### 
#
# Given the root of a Binary Search Tree (BST), convert it to a Greater Tree such that every key of 
# the original BST is changed to the original key plus sum of all keys greater than the original key 
# in BST.
# 
# As a reminder, a binary search tree is a tree that satisfies these constraints:
# 
# 	The left subtree of a node contains only nodes with keys less than the node's key.
# 	The right subtree of a node contains only nodes with keys greater than the node's key.
# 	Both the left and right subtrees must also be binary search trees.
# 
# Note: This question is the same as 1038: 
# https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/
# 
# Example 1:
# 
# Input: root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
# Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
# 
# Example 2:
# 
# Input: root = [0,null,1]
# Output: [1,null,1]
# 
# Example 3:
# 
# Input: root = [1,0,2]
# Output: [3,3,2]
# 
# Example 4:
# 
# Input: root = [3,2,4,1]
# Output: [7,9,4,10]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 104].
# 	-104 <= Node.val <= 104
# 	All the values in the tree are unique.
# 	root is guaranteed to be a valid binary search tree.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    # this question is the same as: 1038. Binary Search Tree to Greater Sum Tree
    def convertBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode):
            if not node:
                return
            traverse(node.right)
            nodes.append(node)
            traverse(node.left)

        nodes = []
        traverse(root)
        cum_sum = 0
        for node in nodes:
            node.val += cum_sum
            cum_sum = node.val
        return root

    def convertBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode):
            nonlocal value
            if not node:
                return
            traverse(node.right)
            node.val += value
            value = node.val
            traverse(node.left)

        value = 0
        traverse(root)
        return root

    def convertBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode, value=0):
            if not node:
                return value
            value = traverse(node.right, value)
            node.val += value
            value = node.val
            value = traverse(node.left, value)
            return value

        traverse(root)
        return root


def test():
    from python3 import Codec
    arguments = [
        "[4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]",
        "[0,null,1]",
        "[1,0,2]",
        "[3,2,4,1]",
        ]
    expectations = [
        "[30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]",
        "[1,null,1]",
        "[3,3,2]",
        "[7,9,4,10]",
        ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().convertBST(root)
        assert solution == Codec.deserialize(expected)
