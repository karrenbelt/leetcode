### Source : https://leetcode.com/problems/russian-doll-envelopes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-31

##################################################################################################### 
#
# You are given a 2D array of integers envelopes where envelopes[i] = [wi, hi] represents the width 
# and the height of an envelope.
# 
# One envelope can fit into another if and only if both the width and height of one envelope are 
# greater than the other envelope's width and height.
# 
# Return the maximum number of envelopes you can Russian doll (i.e., put one inside the other).
# 
# Note: You cannot rotate an envelope.
# 
# Example 1:
# 
# Input: envelopes = [[5,4],[6,4],[6,7],[2,3]]
# Output: 3
# Explanation: The maximum number of envelopes you can Russian doll is 3 ([2,3] => [5,4] => [6,7]).
# 
# Example 2:
# 
# Input: envelopes = [[1,1],[1,1],[1,1]]
# Output: 1
# 
# Constraints:
# 
# 	1 <= envelopes.length <= 5000
# 	envelopes[i].length == 2
# 	1 <= wi, hi <= 104
#####################################################################################################

from typing import List


class Solution:
    def maxEnvelopes(self, envelopes: List[List[int]]) -> int:  # O(2^n) time: TLE, 80/84
        # first thing that comes to mind is sorting and backtracking

        def backtrack(i: int, prev_i: int):
            if i == len(envelopes):
                return 0
            x = 0
            if prev_i == -1 or (envelopes[i][0] > envelopes[prev_i][0] and envelopes[i][1] > envelopes[prev_i][1]):
                if i in memo:
                    x = memo[i]
                else:
                    x = 1 + backtrack(i + 1, i)
                    memo[i] = x
            skipped = backtrack(i + 1, prev_i)
            return max(skipped, x)

        memo = {}
        envelopes = sorted(set(map(tuple, envelopes)))
        return backtrack(0, -1)

    def maxEnvelopes(self, envelopes: List[List[int]]) -> int:  # O(n log n)

        def binary_search(nums: List[int], target: int):
            left = 0
            right = len(nums) - 1
            while left <= right:
                mid = left + (right - left) // 2
                if nums[mid] == target:
                    return mid
                elif nums[mid] < target:
                    left = mid + 1
                else:
                    right = mid - 1
            return left

        def longest_subsequence(nums):  # leetcode question 300, Longest Increasing Subsequence
            res = []
            for num in nums:
                pos = binary_search(res, num)
                if pos >= len(res):
                    res.append(num)
                else:
                    res[pos] = num
            return len(res)

        # sort the envelopes by width because they need to be inorder before consider the height or versa
        envelopes.sort(key=lambda c: (c[0], -c[1]))
        return longest_subsequence(map(lambda x: x[1], envelopes))


def test():
    arguments = [
        [[5, 4], [6, 4], [6, 7], [2, 3]],
        [[1, 1], [1, 1], [1, 1]],
        [[10, 8], [1, 12], [6, 15], [2, 18]],
        ]
    expectations = [3, 1, 2]
    for envelopes, expected in zip(arguments, expectations):
        solution = Solution().maxEnvelopes(envelopes)
        assert solution == expected
