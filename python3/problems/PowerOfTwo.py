### Source : https://leetcode.com/problems/power-of-two/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# Given an integer n, return true if it is a power of two. Otherwise, return false.
# 
# An integer n is a power of two, if there exists an integer x such that n == 2x.
# 
# Example 1:
# 
# Input: n = 1
# Output: true
# Explanation: 20 = 1
# 
# Example 2:
# 
# Input: n = 16
# Output: true
# Explanation: 24 = 16
# 
# Example 3:
# 
# Input: n = 3
# Output: false
# 
# Example 4:
# 
# Input: n = 4
# Output: true
# 
# Example 5:
# 
# Input: n = 5
# Output: false
# 
# Constraints:
# 
# 	-231 <= n <= 231 - 1
#####################################################################################################


class Solution:
    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        binary = bin(n)[2:]
        return binary[0] == '1' and all(binary[i] == '0' for i in range(1, len(binary)))

    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        return n != 0 and all(bin(n)[2:][i] == '0' for i in range(1, n.bit_length()))

    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        return n > 0 and bin(n).count('1') == 1

    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        while n > 1:
            if n & 1:
                return False
            n >>= 1
        return n > 0

    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        return n > 0 and not (n & n - 1)  # remove left most bit using bitwise and

    def isPowerOfTwo(self, n: int) -> bool:  # O(log n) time and O(1) space
        return n != 0 and n == n & -n


def test():
    numbers = [1, 16, 3, 4, 5, 15, 0, -16]
    expectations = [True, True, False, True, False, False, False, False]
    for n, expected in zip(numbers, expectations):
        solution = Solution().isPowerOfTwo(n)
        assert solution == expected
