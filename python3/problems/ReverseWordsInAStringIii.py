### Source : https://leetcode.com/problems/reverse-words-in-a-string-iii/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a string, you need to reverse the order of characters in each word within a sentence while 
# still preserving whitespace and initial word order.
# 
# Example 1:
# 
# Input: "Let's take LeetCode contest"
# Output: "s'teL ekat edoCteeL tsetnoc"
# 
# Note:
# In the string, each word is separated by single space and there will not be any extra space in the 
# string.
#####################################################################################################

class Solution:
    def reverseWords(self, s: str) -> str:
        return ' '.join([x[::-1] for x in s.split(' ')])

    def reverseWords(self, s: str) -> str:
        return ' '.join(x[::-1] for x in s.split())


def test():
    arguments = [
        "Let's take LeetCode contest",
        "God Ding",
    ]
    expectations = [
        "s'teL ekat edoCteeL tsetnoc",
        "doG gniD",
    ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().reverseWords(s)
        assert solution == expected
