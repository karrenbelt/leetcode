### Source : https://leetcode.com/problems/number-of-longest-increasing-subsequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-21

##################################################################################################### 
#
# Given an integer array nums, return the number of longest increasing subsequences.
# 
# Notice that the sequence has to be strictly increasing.
# 
# Example 1:
# 
# Input: nums = [1,3,5,4,7]
# Output: 2
# Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].
# 
# Example 2:
# 
# Input: nums = [2,2,2,2,2]
# Output: 5
# Explanation: The length of longest continuous increasing subsequence is 1, and there are 5 
# subsequences' length is 1, so output 5.
# 
# Constraints:
# 
# 	1 <= nums.length <= 2000
# 	-106 <= nums[i] <= 106
#####################################################################################################

from typing import List


class Solution:  # check problem 300 as a prior: LongestIncreasingSubsequence.py
    def findNumberOfLIS(self, nums: List[int]) -> int:  # O(n^2) time and O(n) space
        # need to store the length of the longest subsequence and the number of longest sequences ending at this index
        dp, longest = [[1, 1] for _ in range(len(nums))], 1
        for i, n in enumerate(nums):
            length, ctr = 1, 0
            for j in range(i):
                if nums[j] < n:
                    length = max(length, dp[j][0] + 1)
            for j in range(i):
                if dp[j][0] == length - 1 and nums[j] < n:
                    ctr += dp[j][1]
            dp[i] = [length, max(ctr, dp[i][1])]
            longest = max(longest, length)
        return sum(item[1] for item in dp if item[0] == longest)


def test():
    arguments = [
        [1, 3, 5, 4, 7],
        [2, 2, 2, 2, 2],
    ]
    expectations = [2, 5]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findNumberOfLIS(nums)
        assert solution == expected
test()