### Source : https://leetcode.com/problems/remove-linked-list-elements/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-03

##################################################################################################### 
#
# Remove all elements from a linked list of integers that have value val.
# 
# Example:
# 
# Input:  1->2->6->3->4->5->6, val = 6
# Output: 1->2->3->4->5
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:  # O(n) time and O(1) space
        while head and head.val == val:
            head = head.next
        node = prev = head
        while node:
            if node.val == val:
                prev.next = node.next
            else:
                prev = node
            node = node.next
        return head

    def removeElements(self, head: ListNode, val: int) -> ListNode:  # O(n) time and O(1) space
        dummy = prev = node = ListNode(None, next=head)
        while node:
            if node.val == val:
                prev.next = node.next
            else:
                prev = node
            node = node.next
        return dummy.next


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [1, 2, 6, 3, 4, 5, 6],
        ]
    values = [6]
    expectations = [
        [1, 2, 3, 4, 5],
        ]
    for nums, val, expected in zip(arrays_of_numbers, values, expectations):
        llist = SinglyLinkedList(nums)
        solution = Solution().removeElements(llist.head, val)
        assert llist == SinglyLinkedList(expected)
