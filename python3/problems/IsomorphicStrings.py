### Source : https://leetcode.com/problems/isomorphic-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-03

##################################################################################################### 
#
# Given two strings s and t, determine if they are isomorphic.
# 
# Two strings s and t are isomorphic if the characters in s can be replaced to get t.
# 
# All occurrences of a character must be replaced with another character while preserving the order 
# of characters. No two characters may map to the same character, but a character may map to itself.
# 
# Example 1:
# Input: s = "egg", t = "add"
# Output: true
# Example 2:
# Input: s = "foo", t = "bar"
# Output: false
# Example 3:
# Input: s = "paper", t = "title"
# Output: true
# 
# Constraints:
# 
# 	1 <= s.length <= 5 * 104
# 	t.length == s.length
# 	s and t consist of any valid ascii character.
#####################################################################################################


class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        return len(set(s)) == len(set(t)) == len(set(zip(s, t)))

    def isIsomorphic(self, s: str, t: str) -> bool:
        return all(map({}.setdefault, a, b) == list(b) for a, b in ((s, t), (t, s)))

    def isIsomorphic(self, s: str, t: str) -> bool:
        return s.translate(s.maketrans(s, t)) == t and t.translate(t.maketrans(t, s)) == s

    def isIsomorphic(self, *s: str) -> bool:
        from itertools import count
        return all(map(lambda x: x[0] == x[1], zip(*(map({}.setdefault, t, count()) for t in s))))


def test():
    pairs_of_strings = [
        ("egg", "add"),
        ("foo", "bar"),
        ("paper", "title"),
        ("badc", "baba"),
        ]
    expectations = [True, False, True, False]
    for (s, t), expected in zip(pairs_of_strings, expectations):
        solution = Solution().isIsomorphic(s, t)
        assert solution == expected, (s, t)
test()