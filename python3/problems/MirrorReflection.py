### Source : https://leetcode.com/problems/mirror-reflection/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-21

##################################################################################################### 
#
# There is a special square room with mirrors on each of the four walls.  Except for the southwest 
# corner, there are receptors on each of the remaining corners, numbered 0, 1, and 2.
# 
# The square room has walls of length p, and a laser ray from the southwest corner first meets the 
# east wall at a distance q from the 0th receptor.
# 
# Return the number of the receptor that the ray meets first.  (It is guaranteed that the ray will 
# meet a receptor eventually.)
# 
# Example 1:
# 
# Input: p = 2, q = 1
# Output: 2
# Explanation: The ray meets receptor 2 the first time it gets reflected back to the left wall.
# 
# Note:
# 
# 	1 <= p <= 1000
# 	0 <= q <= p
#####################################################################################################


class Solution:
    def mirrorReflection(self, p: int, q: int) -> int:  # O(log n) time and O(1) space
        # when the laser reaches a corner either p or q needs to be odd
        while not p % 2 and not q % 2:
            p //= 2
            q //= 2
        return 2 if not p % 2 else 0 if not q % 2 else 1


def test():
    arguments = [
        (2, 1),
        (1, 1),
        (3, 2),
        (5, 2),
        (4, 5),
    ]
    expectations = [2, 1, 0, 0, 2]
    for (p, q), expected in zip(arguments, expectations):
        solution = Solution().mirrorReflection(p, q)
        assert solution == expected
