### Source : https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given a binary tree
# 
# struct Node {
#   int val;
#   Node *left;
#   Node *right;
#   Node *next;
# }
# 
# Populate each next pointer to point to its next right node. If there is no next right node, the 
# next pointer should be set to NULL.
# 
# Initially, all next pointers are set to NULL.
# 
# Follow up:
# 
# 	You may only use constant extra space.
# 	Recursive approach is fine, you may assume implicit stack space does not count as extra 
# space for this problem.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,null,7]
# Output: [1,#,2,3,#,4,5,7,#]
# Explanation: Given the above binary tree (Figure A), your function should populate each next 
# pointer to point to its next right node, just like in Figure B. The serialized output is in level 
# order as connected by the next pointers, with '#' signifying the end of each level.
# 
# Constraints:
# 
# 	The number of nodes in the given tree is less than 6000.
# 	-100 <= node.val <= 100
#####################################################################################################

from python3 import BinaryTreeNode as Node


class Solution:
    # def connect(self, root: 'Node') -> 'Node':
    #     # level order travesal
    #     if not root:
    #         return root
    #     levels = []
    #     queue = deque([root])
    #     while queue:
    #         level = []
    #         for i in range(len(queue)):
    #             node = queue.popleft()
    #             if node.left:
    #                 queue.append(node.left)
    #             if node.right:
    #                 queue.append(node.right)
    #             level.append(node)
    #         # now we set their neighors
    #         for i in range(len(level)-1):
    #             level[i].next = level[i+1]
    #     return root
    def connect(self, root: 'Node') -> 'Node':
        from collections import deque

        if not root:
            return root

        queue = deque([root])
        while queue:
            level_size = len(queue)  # must not change on iteration
            for i in range(level_size):
                curr = queue.popleft()
                if i < level_size - 1:
                    curr.next = queue[0]
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
        return root


def test():
    from python3 import Codec
