### Source : https://leetcode.com/problems/smallest-good-base/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-02

##################################################################################################### 
#
# Given an integer n represented as a string, return the smallest good base of n.
# 
# We call k >= 2 a good base of n, if all digits of n base k are 1's.
# 
# Example 1:
# 
# Input: n = "13"
# Output: "3"
# Explanation: 13 base 3 is 111.
# 
# Example 2:
# 
# Input: n = "4681"
# Output: "8"
# Explanation: 4681 base 8 is 11111.
# 
# Example 3:
# 
# Input: n = "1000000000000000000"
# Output: "999999999999999999"
# Explanation: 1000000000000000000 base 999999999999999999 is 11.
# 
# Constraints:
# 
# 	n is an integer in the range [3, 1018].
# 	n does not contain any leading zeros.
#####################################################################################################


class Solution:
    def smallestGoodBase(self, n: str) -> str:  # O(log n) time
        n = int(n)  # in python integers can be arbitrarily large
        max_len = len(bin(n)) - 2  # the longest possible representation
        for m in range(max_len, 1, -1):
            left, right = 2, n  # base 2 is the minimum
            while left <= right:
                mid = left + (right - left) // 2
                num = (pow(mid, m) - 1) // (mid - 1)
                if num == n:
                    return str(mid)
                elif num < n:
                    left = mid + 1
                else:
                    right = mid - 1
        # return str(n - 1)  # never reached. Would reach this is we narrow the binary search range from n to n - 1

    def smallestGoodBase(self, n: str) -> str:
        import math
        n = int(n)
        m_max = int(math.log(n, 2))
        for m in range(m_max, 1, -1):
            k = int(n ** (1 / m))
            if (k ** (m + 1) - 1) == n * (k - 1):
                return str(k)
        return str(n - 1)


def test():
    arguments = ["13", "4681", "1000000000000000000"]
    expectations = ["3", "8", "999999999999999999"]
    for n, expected in zip(arguments, expectations):
        solution = Solution().smallestGoodBase(n)
        assert solution == expected
