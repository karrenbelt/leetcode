### Source : https://leetcode.com/problems/check-whether-two-strings-are-almost-equivalent/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-21

##################################################################################################### 
#
# Two strings word1 and word2 are considered almost equivalent if the differences between the 
# frequencies of each letter from 'a' to 'z' between word1 and word2 is at most 3.
# 
# Given two strings word1 and word2, each of length n, return true if word1 and word2 are almost 
# equivalent, or false otherwise.
# 
# The frequency of a letter x is the number of times it occurs in the string.
# 
# Example 1:
# 
# Input: word1 = "aaaa", word2 = "bccb"
# Output: false
# Explanation: There are 4 'a's in "aaaa" but 0 'a's in "bccb".
# The difference is 4, which is more than the allowed 3.
# 
# Example 2:
# 
# Input: word1 = "abcdeef", word2 = "abaaacc"
# Output: true
# Explanation: The differences between the frequencies of each letter in word1 and word2 are at most 
# 3:
# - 'a' appears 1 time in word1 and 4 times in word2. The difference is 3.
# - 'b' appears 1 time in word1 and 1 time in word2. The difference is 0.
# - 'c' appears 1 time in word1 and 2 times in word2. The difference is 1.
# - 'd' appears 1 time in word1 and 0 times in word2. The difference is 1.
# - 'e' appears 2 times in word1 and 0 times in word2. The difference is 2.
# - 'f' appears 1 time in word1 and 0 times in word2. The difference is 1.
# 
# Example 3:
# 
# Input: word1 = "cccddabba", word2 = "babababab"
# Output: true
# Explanation: The differences between the frequencies of each letter in word1 and word2 are at most 
# 3:
# - 'a' appears 2 times in word1 and 4 times in word2. The difference is 2.
# - 'b' appears 2 times in word1 and 5 times in word2. The difference is 3.
# - 'c' appears 3 times in word1 and 0 times in word2. The difference is 3.
# - 'd' appears 2 times in word1 and 0 times in word2. The difference is 2.
# 
# Constraints:
# 
# 	n == word1.length == word2.length
# 	1 <= n <= 100
# 	word1 and word2 consist only of lowercase English letters.
#####################################################################################################


class Solution:
    def checkAlmostEquivalent(self, word1: str, word2: str) -> bool:  # O(n)
        from collections import Counter
        ctr1, ctr2 = map(Counter, (word1, word2))
        return all(abs(ctr1[c] - ctr2[c]) < 4 for c in set(word1 + word2))

    def checkAlmostEquivalent(self, word1: str, word2: str) -> bool:  # O(n)
        ctr = [0] * 26
        for c1, c2 in zip(word1, word2):
            ctr[ord(c1) - ord("a")] += 1
            ctr[ord(c2) - ord("a")] -= 1
        return max(map(abs, ctr)) < 4


def test():
    arguments = [
        ("aaaa", "bccb"),
        ("abcdeef", "abaaacc"),
        ("cccddabba", "babababab"),
        ("aaaa", "aaaa"),
        ("zzzyyy", "iiiiii"),
    ]
    expectations = [False, True, True, True]
    for (word1, word2), expected in zip(arguments, expectations):
        solution = Solution().checkAlmostEquivalent(word1, word2)
        assert solution == expected
