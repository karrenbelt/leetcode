### Source : https://leetcode.com/problems/complex-number-multiplication/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-24

##################################################################################################### 
#
# A complex number can be represented as a string on the form "real+imaginaryi" where:
# 
# 	real is the real part and is an integer in the range [-100, 100].
# 	imaginary is the imaginary part and is an integer in the range [-100, 100].
# 	i2 == -1.
# 
# Given two complex numbers num1 and num2 as strings, return a string of the complex number that 
# represents their multiplications.
# 
# Example 1:
# 
# Input: num1 = "1+1i", num2 = "1+1i"
# Output: "0+2i"
# Explanation: (1 + i) * (1 + i) = 1 + i2 + 2 * i = 2i, and you need convert it to the form of 0+2i.
# 
# Example 2:
# 
# Input: num1 = "1+-1i", num2 = "1+-1i"
# Output: "0+-2i"
# Explanation: (1 - i) * (1 - i) = 1 + i2 - 2 * i = -2i, and you need convert it to the form of 0+-2i.
# 
# Constraints:
# 
# 	num1 and num2 are valid complex numbers.
#####################################################################################################


class Solution:
    def complexNumberMultiply(self, num1: str, num2: str) -> str:
        n = complex.__mul__(*(complex(num.replace('i', 'j').replace('+-', '-')) for num in (num1, num2)))
        return f"{int(n.real)}+{int(n.imag)}i"

    def complexNumberMultiply(self, num1: str, num2: str) -> str:
        (a, ai), (b, bi) = (map(int, num[:-1].split('+')) for num in (num1, num2))
        return f"{int(a * b - ai * bi)}+{int(a * bi + ai * b)}i"


def test():
    arguments = [
        ("1+1i", "1+1i"),
        ("1+-1i", "1+-1i"),
    ]
    expectations = [
        "0+2i",
        "0+-2i",
    ]
    for (num1, num2), expected in zip(arguments, expectations):
        solution = Solution().complexNumberMultiply(num1, num2)
        assert solution == expected, (solution, expected)
