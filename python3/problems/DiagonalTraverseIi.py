### Source : https://leetcode.com/problems/diagonal-traverse-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Given a list of lists of integers, nums, return all elements of nums in diagonal order as shown in 
# the below images.
# 
# Example 1:
# 
# Input: nums = [[1,2,3],[4,5,6],[7,8,9]]
# Output: [1,4,2,7,5,3,8,6,9]
# 
# Example 2:
# 
# Input: nums = [[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]]
# Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16]
# 
# Example 3:
# 
# Input: nums = [[1,2,3],[4],[5,6,7],[8],[9,10,11]]
# Output: [1,4,2,5,3,8,6,9,7,10,11]
# 
# Example 4:
# 
# Input: nums = [[1,2,3,4,5,6]]
# Output: [1,2,3,4,5,6]
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	1 <= nums[i].length <= 105
# 	1 <= nums[i][j] <= 109
# 	There at most 105 elements in nums.
#####################################################################################################

from typing import List


class Solution:
    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:  # O(n) time and O(n) space
        diagonal = {}  # take advantage of the fact that a dict is sorted by default in python3
        for i in range(len(nums)):
            for j in range(len(nums[i])):
                diagonal.setdefault(i + j, []).append(nums[i][j])
        return [v for values in diagonal.values() for v in reversed(values)]

    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:  # O(n) time and O(n) space
        from collections import deque
        diagonal_order, queue = [], deque([(0, 0)])
        while queue:
            i, j = queue.popleft()
            diagonal_order.append(nums[i][j])
            if j == 0 and i + 1 < len(nums):  # we only add the number at the bottom if we are at column 0
                queue.append((i + 1, j))
            if j + 1 < len(nums[i]):   # add the number on the right
                queue.append((i, j + 1))
        return diagonal_order

    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:  # O(n log n) time and O(n) space
        return [nums[i - j][j] for i, j in sorted((i + j, j) for i in range(len(nums)) for j in range(len(nums[i])))]

    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:  # O(n log n) time and O(n) space
        return [n for *_, n in sorted((i + j, j, n) for i, row in enumerate(nums) for j, n in enumerate(row))]


def test():
    arguments = [
        [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
        [[1, 2, 3, 4, 5], [6, 7], [8], [9, 10, 11], [12, 13, 14, 15, 16]],
        [[1, 2, 3], [4], [5, 6, 7], [8], [9, 10, 11]],
        [[1, 2, 3, 4, 5, 6]],
    ]
    expectations = [
        [1, 4, 2, 7, 5, 3, 8, 6, 9],
        [1, 6, 2, 8, 7, 3, 9, 4, 12, 10, 5, 13, 11, 14, 15, 16],
        [1, 4, 2, 5, 3, 8, 6, 9, 7, 10, 11],
        [1, 2, 3, 4, 5, 6],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findDiagonalOrder(nums)
        assert solution == expected


test()
