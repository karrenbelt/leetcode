### Source : https://leetcode.com/problems/odd-even-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given a singly linked list, group all odd nodes together followed by the even nodes. Please note 
# here we are talking about the node number and not the value in the nodes.
# 
# You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time 
# complexity.
# 
# Example 1:
# 
# Input: 1->2->3->4->5->NULL
# Output: 1->3->5->2->4->NULL
# 
# Example 2:
# 
# Input: 2->1->3->5->6->4->7->NULL
# Output: 2->3->6->7->1->5->4->NULL
# 
# Constraints:
# 
# 	The relative order inside both the even and odd groups should remain as it was in the input.
# 	The first node is considered odd, the second node even and so on ...
# 	The length of the linked list is between [0, 104].
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        if not head:
            return
        node = head
        tail = []
        while node and node.next and node.next.next:
            tail.append(node.next)
            node.next = node.next.next
            node = node.next
        tail.append(node.next)
        for even_node in tail:
            node.next = even_node
            node = even_node
        return head

    def oddEvenList(self, head: ListNode) -> ListNode:  # two-pointer, O(1) space
        if not head:
            return head
        odd, even, even_head = head, head.next, head.next
        while even and even.next:
            odd.next, odd = even.next, even.next
            even.next, even = odd.next, odd.next
        odd.next = even_head
        return head


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [1, 2, 3, 4, 5],
        [2, 1, 3, 5, 6, 4, 7],
        ]
    expectations = [
        [1, 3, 5, 2, 4],
        [2, 3, 6, 7, 1, 5, 4],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        ll = SinglyLinkedList(nums)
        solution = Solution().oddEvenList(ll.head)
        assert solution == SinglyLinkedList(expected).head
