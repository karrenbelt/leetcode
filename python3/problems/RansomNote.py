### Source : https://leetcode.com/problems/ransom-note/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-15

##################################################################################################### 
#
# Given two stings ransomNote and magazine, return true if ransomNote can be constructed from 
# magazine and false otherwise.
# 
# Each letter in magazine can only be used once in ransomNote.
# 
# Example 1:
# Input: ransomNote = "a", magazine = "b"
# Output: false
# Example 2:
# Input: ransomNote = "aa", magazine = "ab"
# Output: false
# Example 3:
# Input: ransomNote = "aa", magazine = "aab"
# Output: true
# 
# Constraints:
# 
# 	1 <= ransomNote.length, magazine.length <= 105
# 	ransomNote and magazine consist of lowercase English letters.
#####################################################################################################


class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:  # O(m + n) time
        from collections import Counter
        return not Counter(ransomNote) - Counter(magazine)

    def canConstruct(self, ransomNote: str, magazine: str) -> bool:  # O(m + n) time
        counts = dict.fromkeys(magazine, 0)
        for c in magazine:
            counts[c] += 1
        for c in ransomNote:
            if not counts.get(c, 0):
                return False
            counts[c] -= 1
        return True

    def canConstruct(self, ransomNote: str, magazine: str) -> bool:  # O(n^2 + m * n) time
        return all(magazine.count(c) >= ransomNote.count(c) for c in set(ransomNote))


def test():
    arguments = [
        ("a", "b"),
        ("aa", "ab"),
        ("aa", "aab"),
    ]
    expectations = [False, False, True]
    for (ransomNote, magazine), expected in zip(arguments, expectations):
        solution = Solution().canConstruct(ransomNote, magazine)
        assert solution == expected
