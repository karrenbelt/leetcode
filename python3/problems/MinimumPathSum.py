### Source : https://leetcode.com/problems/minimum-path-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-18

##################################################################################################### 
#
# Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right 
# which minimizes the sum of all numbers along its path.
# 
# Note: You can only move either down or right at any point in time.
# 
# Example:
# 
# Input:
# [
#   [1,3,1],
#   [1,5,1],
#   [4,2,1]
# ]
# Output: 7
# Explanation: Because the path 1→3→1→1→1 minimizes the sum.
# 
#####################################################################################################

from typing import List


class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        min_path = 0
        if not grid or not grid[0]:
            return min_path

        m, n = len(grid), len(grid[0])
        min_path_sums = [[0] * n for _ in range(m)]
        min_path_sums[0][0] = grid[0][0]
        for i in range(1, n):
            min_path_sums[0][i] = min_path_sums[0][i - 1] + grid[0][i]
        for j in range(1, m):
            min_path_sums[j][0] = min_path_sums[j - 1][0] + grid[j][0]

        for i in range(1, m):
            for j in range(1, n):
                if min_path_sums[i - 1][j] <= min_path_sums[i][j - 1]:
                    min_path_sums[i][j] = min_path_sums[i - 1][j] + grid[i][j]
                else:
                    min_path_sums[i][j] = min_path_sums[i][j - 1] + grid[i][j]

        return min_path_sums[-1][-1]


def test():
    grids = [
        [[1, 3, 1], [1, 5, 1], [4, 2, 1]],
        [[1, 2, 3], [4, 5, 6]],
        [[1, 2, 5], [3, 2, 1]],
    ]
    expectations = [7, 12, 6]
    for grid, expected in zip(grids, expectations):
        solution = Solution().minPathSum(grid)
        assert solution == expected
