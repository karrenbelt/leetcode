### Source : https://leetcode.com/problems/smallest-string-with-swaps/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-06

##################################################################################################### 
#
# You are given a string s, and an array of pairs of indices in the string pairs where pairs[i] = [a, 
# b] indicates 2 indices(0-indexed) of the string.
# 
# You can swap the characters at any pair of indices in the given pairs any number of times.
# 
# Return the lexicographically smallest string that s can be changed to after using the swaps.
# 
# Example 1:
# 
# Input: s = "dcab", pairs = [[0,3],[1,2]]
# Output: "bacd"
# Explaination: 
# Swap s[0] and s[3], s = "bcad"
# Swap s[1] and s[2], s = "bacd"
# 
# Example 2:
# 
# Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
# Output: "abcd"
# Explaination: 
# Swap s[0] and s[3], s = "bcad"
# Swap s[0] and s[2], s = "acbd"
# Swap s[1] and s[2], s = "abcd"
# 
# Example 3:
# 
# Input: s = "cba", pairs = [[0,1],[1,2]]
# Output: "abc"
# Explaination: 
# Swap s[0] and s[1], s = "bca"
# Swap s[1] and s[2], s = "bac"
# Swap s[0] and s[1], s = "abc"
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	0 <= pairs.length <= 105
# 	0 <= pairs[i][0], pairs[i][1] < s.length
# 	s only contains lower case English letters.
#####################################################################################################

from typing import List


class Solution:
    def smallestStringWithSwaps(self, s: str, pairs: List[List[int]]) -> str:  # O(mn + n log n) time and O(n) space
        # union find
        # sort letters for each group
        def union(x: int, y: int):
            parents[find(x)] = find(y)

        def find(x: int):
            if x != parents[x]:
                parents[x] = find(parents[x])
            return parents[x]

        parents, mapping = list(range(len(s))), {}
        for a, b in pairs:
            union(a, b)
        for i, c in enumerate(s):
            mapping.setdefault(find(i), []).append(c)
        for letters in mapping.values():
            letters.sort(reverse=True)
        return "".join(mapping[find(i)].pop() for i in range(len(s)))


def test():
    arguments = [
        ("dcab", [[0, 3], [1, 2]]),
        ("dcab", [[0, 3], [1, 2], [0, 2]]),
        ("cba", [[0, 1], [1, 2]])
    ]
    expectations = [
        "bacd",
        "abcd",
        "abc",
    ]
    for (s, pairs), expected in zip(arguments, expectations):
        solution = Solution().smallestStringWithSwaps(s, pairs)
        assert solution == expected
