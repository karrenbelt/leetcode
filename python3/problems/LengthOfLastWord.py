### Source : https://leetcode.com/problems/length-of-last-word/
### Author : karrenbelt
### Date   : 2019-05-15

##################################################################################################### 
#
# Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the 
# length of last word in the string.
# 
# If the last word does not exist, return 0.
# 
# Note: A word is defined as a character sequence consists of non-space characters only.
# 
# Example:
# 
# Input: "Hello World"
# Output: 5
# 
#####################################################################################################

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        return len(s.strip().rsplit(' ', 1)[-1])

    def lengthOfLastWord(self, s: str) -> int:
        return len(s.rstrip().rpartition(' ')[-1])


def test():
    strings = []
    expectations = [5, 0]
    for s, expected in zip(strings, expectations):
        solution = Solution().lengthOfLastWord(s)
        assert solution == expected

