### Source : https://leetcode.com/problems/number-of-steps-to-reduce-a-number-to-zero/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-18

##################################################################################################### 
#
# Given a non-negative integer num, return the number of steps to reduce it to zero. If the current 
# number is even, you have to divide it by 2, otherwise, you have to subtract 1 from it.
# 
# Example 1:
# 
# Input: num = 14
# Output: 6
# Explanation: 
# Step 1) 14 is even; divide by 2 and obtain 7. 
# Step 2) 7 is odd; subtract 1 and obtain 6.
# Step 3) 6 is even; divide by 2 and obtain 3. 
# Step 4) 3 is odd; subtract 1 and obtain 2. 
# Step 5) 2 is even; divide by 2 and obtain 1. 
# Step 6) 1 is odd; subtract 1 and obtain 0.
# 
# Example 2:
# 
# Input: num = 8
# Output: 4
# Explanation: 
# Step 1) 8 is even; divide by 2 and obtain 4. 
# Step 2) 4 is even; divide by 2 and obtain 2. 
# Step 3) 2 is even; divide by 2 and obtain 1. 
# Step 4) 1 is odd; subtract 1 and obtain 0.
# 
# Example 3:
# 
# Input: num = 123
# Output: 12
# 
# Constraints:
# 
# 	0 <= num <= 106
#####################################################################################################


class Solution:
    def numberOfSteps(self, num: int) -> int:  # arithmetic
        ctr = 0
        while num:
            num = num - 1 if num % 2 else num // 2
            ctr += 1
        return ctr

    def numberOfSteps(self, num: int) -> int:  # bit-manipulation
        ctr = 0
        while num:
            num = num ^ 1 if num & 1 else num >> 1
            ctr += 1
        return ctr

    def numberOfSteps(self, num: int) -> int:  # log(num) time and log(num) space
        digits = f'{num:b}'
        return digits.count('1') - 1 + len(digits)


def test():
    arguments = [14, 8, 123]
    expectations = [6, 4, 12]
    for num, expected in zip(arguments, expectations):
        solution = Solution().numberOfSteps(num)
        assert solution == expected
