### Source : https://leetcode.com/problems/top-k-frequent-words/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-15

##################################################################################################### 
#
# Given a non-empty list of words, return the k most frequent elements.
# Your answer should be sorted by frequency from highest to lowest. If two words have the same 
# frequency, then the word with the lower alphabetical order comes first.
# 
# Example 1:
# 
# Input: ["i", "love", "leetcode", "i", "love", "coding"], k = 2
# Output: ["i", "love"]
# Explanation: "i" and "love" are the two most frequent words.
#     Note that "i" comes before "love" due to a lower alphabetical order.
# 
# Example 2:
# 
# Input: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
# Output: ["the", "is", "sunny", "day"]
# Explanation: "the", "is", "sunny" and "day" are the four most frequent words,
#     with the number of occurrence being 4, 3, 2 and 1 respectively.
# 
# Note:
# 
# You may assume k is always valid, 1 &le; k &le; number of unique elements.
# Input words contain only lowercase letters.
# 
# Follow up:
# 
# Try to solve it in O(n log k) time and O(n) extra space.
# 
#####################################################################################################

from typing import List


class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:  # O(n log n)

        def count(items):
            d = {}
            for item in items:
                d[item] = d.get(item, 0) + 1
            return d

        return list(map(lambda x: x[0], sorted(count(words).items(), key=lambda x: (-x[1],  x[0]))[:k]))

    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        import heapq

        def count(items):
            d = {}
            for item in items:
                d[item] = d.get(item, 0) + 1
            return d

        heap = [(-value, key) for key, value in count(words).items()]
        heapq.heapify(heap)
        return [heapq.heappop(heap)[1] for _ in range(k)]

    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        from collections import Counter
        import heapq
        return [word for word, _ in (heapq.nsmallest(k, Counter(words).items(), key=lambda item: (-item[1], item[0])))]


def test():
    arguments = [
        (["i", "love", "leetcode", "i", "love", "coding"], 2),
        (["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], 4),
        (["d", "c", "b", "a"], 2),
        (["a", "aa", "aaa"], 1),
        ]
    expectations = [
        ["i", "love"],
        ["the", "is", "sunny", "day"],
        ['a', 'b'],
        ['a'],
        ]
    for (words, k), expected in zip(arguments, expectations):
        solution = Solution().topKFrequent(words, k)
        assert solution == expected
