### Source : https://leetcode.com/problems/bitwise-and-of-numbers-range/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-23

##################################################################################################### 
#
# Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers in this 
# range, inclusive.
# 
# Example 1:
# 
# Input: [5,7]
# Output: 4
# 
# Example 2:
# 
# Input: [0,1]
# Output: 0
#####################################################################################################


class Solution:
    def rangeBitwiseAnd(self, m: int, n: int) -> int:  # O(1) time and O(1) space
        # observations:
        # 1. whenever any bit flips it will be 0 in the resulting answer
        # 2. whenever any bit flips all less significant bits will also be zero
        # hence, only the most significant bits that m and n have in common will be preserved, the rest will be zero
        # we thus traverse from the most significant to the least significant bit
        # strategy:
        # 1. we right shift both m and n until they are equal -> gives us the most significant bits in common
        # 2. we left shift either m or n by the number of right shifts required in the previous step
        i = 0
        while m != n:
            m >>= 1
            n >>= 1
            i += 1
        return n << i  # == m << i

    def rangeBitwiseAnd(self, m: int, n: int) -> int:  # O(1) time and O(1) space
        while m < n:
            n &= n - 1
        return n

    def rangeBitwiseAnd(self, m: int, n: int) -> int:  # O(1) time and O(1) space
        return m & n & ((1 << 32) - pow(2, (n - m).bit_length()))

    def rangeBitwiseAnd(self, m: int, n: int) -> int:  # O(1) time and O(1) space
        return self.rangeBitwiseAnd(m >> 1, n >> 1) << 1 if m != n else m


def test():
    pairs_of_numbers = [
        [5, 7],
        [0, 1],
    ]
    expectations = [4, 0]
    for (m, n), expected in zip(pairs_of_numbers, expectations):
        solution = Solution().rangeBitwiseAnd(m, n)
        assert solution == expected
