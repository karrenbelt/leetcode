### Source : https://leetcode.com/problems/projection-area-of-3d-shapes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-08

##################################################################################################### 
#
# You are given an n x n grid where we place some 1 x 1 x 1 cubes that are axis-aligned with the x, 
# y, and z axes.
# 
# Each value v = grid[i][j] represents a tower of v cubes placed on top of the cell (i, j).
# 
# We view the projection of these cubes onto the xy, yz, and zx planes.
# 
# A projection is like a shadow, that maps our 3-dimensional figure to a 2-dimensional plane. We are 
# viewing the "shadow" when looking at the cubes from the top, the front, and the side.
# 
# Return the total area of all three projections.
# 
# Example 1:
# 
# Input: grid = [[1,2],[3,4]]
# Output: 17
# Explanation: Here are the three projections ("shadows") of the shape made with each axis-aligned 
# plane.
# 
# Example 2:
# 
# Input: grid = [[2]]
# Output: 5
# 
# Example 3:
# 
# Input: grid = [[1,0],[0,2]]
# Output: 8
# 
# Example 4:
# 
# Input: grid = [[1,1,1],[1,0,1],[1,1,1]]
# Output: 14
# 
# Example 5:
# 
# Input: grid = [[2,2,2],[2,1,2],[2,2,2]]
# Output: 21
# 
# Constraints:
# 
# 	n == grid.length
# 	n == grid[i].length
# 	1 <= n <= 50
# 	0 <= grid[i][j] <= 50
#####################################################################################################

from typing import List


class Solution:
    def projectionArea(self, grid: List[List[int]]) -> int:  # O(n) time and O(sqrt(n)) space. Also for non n x n grid
        xy = zx = 0
        yz = [0] * len(grid[0])
        for i in range(len(grid)):
            zx += max(grid[i])
            for j in range(len(grid[0])):
                xy += 1 if grid[i][j] else 0
                yz[j] = max(yz[j], grid[i][j])
        return sum((xy, *yz, zx))

    def projectionArea(self, grid: List[List[int]]) -> int:  # O(n) time and O(1) space
        area = 0
        for i in range(len(grid)):
            x = y = 0
            for j in range(len(grid[0])):
                x = max(x, grid[i][j])
                y = max(y, grid[j][i])
                area += 1 if grid[i][j] else 0
            area += x + y
        return area


def test():
    arguments = [
        [[1, 2], [3, 4]],
        [[2]],
        [[1, 0], [0, 2]],
        [[1, 1, 1], [1, 0, 1], [1, 1, 1]],
        [[2, 2, 2], [2, 1, 2], [2, 2, 2]],
        ]
    expectations = [17, 5, 8, 14, 21]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().projectionArea(grid)
        assert solution == expected
