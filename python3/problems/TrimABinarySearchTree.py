### Source : https://leetcode.com/problems/trim-a-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# Given the root of a binary search tree and the lowest and highest boundaries as low and high, trim 
# the tree so that all its elements lies in [low, high]. Trimming the tree should not change the 
# relative structure of the elements that will remain in the tree (i.e., any node's descendant should 
# remain a descendant). It can be proven that there is a unique answer.
# 
# Return the root of the trimmed binary search tree. Note that the root may change depending on the 
# given bounds.
# 
# Example 1:
# 
# Input: root = [1,0,2], low = 1, high = 2
# Output: [1,null,2]
# 
# Example 2:
# 
# Input: root = [3,0,4,null,2,null,null,1], low = 1, high = 3
# Output: [3,2,null,1]
# 
# Example 3:
# 
# Input: root = [1], low = 1, high = 2
# Output: [1]
# 
# Example 4:
# 
# Input: root = [1,null,2], low = 1, high = 3
# Output: [1,null,2]
# 
# Example 5:
# 
# Input: root = [1,null,2], low = 2, high = 4
# Output: [2]
# 
# Constraints:
# 
# 	The number of nodes in the tree in the range [1, 104].
# 	0 <= Node.val <= 104
# 	The value of each node in the tree is unique.
# 	root is guaranteed to be a valid binary search tree.
# 	0 <= low <= high <= 104
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def trimBST(self, root: TreeNode, low: int, high: int) -> TreeNode:

        def trim(node: TreeNode):
            if not node:
                return
            if node.left and node.left.val < low:
                new_left = node.left.right
                while new_left and new_left.val < low:
                    new_left = new_left.right
                node.left = new_left
            if node.right and node.right.val > high:
                new_right = node.right.left
                while new_right and new_right.val > high:
                    new_right = new_right.left
                node.right = new_right
            trim(node.left)
            trim(node.right)

        # if needed, we re-root the tree
        def find_new_root(node: TreeNode):
            if not node:
                return
            elif low <= node.val <= high:
                return node
            return find_new_root(node.left if node.val > high else node.right)

        new_root = find_new_root(root)
        trim(new_root)
        return new_root

    def trimBST(self, root: TreeNode, low: int, high: int) -> TreeNode:

        def trim(node: TreeNode):
            if not node:
                return
            elif node.val < low:
                return trim(node.right)
            elif node.val > high:
                return trim(node.left)
            else:
                node.left = trim(node.left)
                node.right = trim(node.right)
                return node

        return trim(root)


def test():
    from python3 import Codec
    arguments = [
        ("[1,0,2]", 1, 2),
        ("[3,0,4,null,2,null,null,1]", 1, 3),
        ("[1]", 1, 2),
        ("[1,null,2]", 1, 3),
        ("[1,null,2]", 2, 4),
        ]
    expectations = [
        "[1,null,2]",
        "[3,2,null,1]",
        "[1]",
        "[1,null,2]",
        "[2]"
        ]
    for (serialized_tree, low, high), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().trimBST(root, low, high)
        assert solution == Codec.deserialize(expected)
