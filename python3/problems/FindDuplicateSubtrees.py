### Source : https://leetcode.com/problems/find-duplicate-subtrees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given the root of a binary tree, return all duplicate subtrees.
# 
# For each kind of duplicate subtrees, you only need to return the root node of any one of them.
# 
# Two trees are duplicate if they have the same structure with the same node values.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,null,2,4,null,null,4]
# Output: [[2,4],[4]]
# 
# Example 2:
# 
# Input: root = [2,1,1]
# Output: [[1]]
# 
# Example 3:
# 
# Input: root = [2,2,2,3,null,3,null]
# Output: [[2,3],[3]]
# 
# Constraints:
# 
# 	The number of the nodes in the tree will be in the range [1, 104]
# 	-200 <= Node.val <= 200
#####################################################################################################

from typing import List, Optional
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def findDuplicateSubtrees(self, root: TreeNode) -> List[TreeNode]:  # works, but slow

        def preorder(node: TreeNode):
            if not node:
                return ''
            s = f'{node.val},{preorder(node.left)}{preorder(node.right)}'
            if s in seen:
                duplicated[s] = node
            seen.add(s)

        duplicated = {}
        seen = set()
        preorder(root)
        return list(duplicated.values())

    def findDuplicateSubtrees(self, root: TreeNode) -> List[TreeNode]:

        def dfs(node: TreeNode):
            if not node:
                return ""
            serialized = f"{node.val},{dfs(node.left)},{dfs(node.right)},"
            count[serialized] = count.get(serialized, 0) + 1
            if count[serialized] == 2:
                duplicated.append(node)
            return serialized

        count = {}
        duplicated = []
        dfs(root)
        return duplicated

    def findDuplicateSubtrees(self, root: Optional[TreeNode]) -> List[Optional[TreeNode]]:

        def dfs(node: Optional[TreeNode]) -> str:
            if not node:
                return ""
            s = f"{node.val},{dfs(node.left)},{dfs(node.right)}"
            duplicates.setdefault(s, []).append(node)
            return s

        (duplicates := {}) or dfs(root)
        return [v.pop() for k, v in duplicates.items() if len(v) > 1]

    def findDuplicateSubtrees(self, root: Optional[TreeNode]) -> List[Optional[TreeNode]]:
        from collections import defaultdict

        def getid(node: Optional[TreeNode]):
            if node:
                id = tree_ids[node.val, getid(node.left), getid(node.right)]
                trees.setdefault(id, []).append(node)
                return id

        trees, tree_ids = {}, defaultdict()
        tree_ids.default_factory = tree_ids.__len__
        getid(root)
        return [roots.pop() for roots in trees.values() if len(roots) > 1]


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,null,2,4,null,null,4]",
        "[2,1,1]",
        "[2,2,2,3,null,3,null]",
        ]
    expectations = [
        ["[2, 4]", "[4]"],
        ["[1]"],
        ["[2, 3]", "[3]"],
        ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().findDuplicateSubtrees(root)
        assert set(solution) == set(map(Codec.deserialize, expected))
