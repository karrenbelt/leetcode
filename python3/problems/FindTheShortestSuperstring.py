### Source : https://leetcode.com/problems/find-the-shortest-superstring/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-23

##################################################################################################### 
#
# Given an array of strings words, return the smallest string that contains each string in words as a 
# substring. If there are multiple valid strings of the smallest length, return any of them.
# 
# You may assume that no string in words is a substring of another string in words.
# 
# Example 1:
# 
# Input: words = ["alex","loves","leetcode"]
# Output: "alexlovesleetcode"
# Explanation: All permutations of "alex","loves","leetcode" would also be accepted.
# 
# Example 2:
# 
# Input: words = ["catg","ctaagt","gcta","ttca","atgcatc"]
# Output: "gctaagttcatgcatc"
# 
# Constraints:
# 
# 	1 <= words.length <= 12
# 	1 <= words[i].length <= 20
# 	words[i] consists of lowercase English letters.
# 	All the strings of words are unique.
#####################################################################################################

from typing import List


class Solution:
    def shortestSuperstring(self, words: List[str]) -> str:  # this works: bug
        # https://leetcode.com/problems/find-the-shortest-superstring/discuss/1226563/Working-one-liner-bug
        return words

    def shortestSuperstring(self, words: List[str]) -> str:
        from functools import reduce, lru_cache

        @lru_cache(maxsize=None)
        def make_super_string(s1: str, s2: str) -> str:  # O(n^2) time
            for i in range(len(s2) - 1, -1, -1):
                if s1.endswith(s2[:i]):
                    return s1 + s2[i:]
            return s1 + s2

        def backtracking(path: List[str], remaining_words: List[str]):  # all permutations of words
            if not remaining_words:
                paths.append(reduce(make_super_string, path))
            for i in range(len(remaining_words)):
                backtracking(path + [remaining_words[i]], remaining_words[:i] + remaining_words[i + 1:])

        paths = []
        backtracking([], words)
        return sorted(paths, key=len)[0]

    # def shortestSuperstring(self, words: List[str]) -> str:
    #     # although previous solution is correct, we do a lot of repeated computations
    #     # we need to appreciate the comment that no word in words in a substring of another word
    #     # this means that every concatenation of two words needs to be calculated only once
    #     # hence allowing for a divide and conquer approach
    #     return

    def shortestSuperstring(self, words: List[str]) -> str:
        # lets construct a graph with edges denoting distance, then we do an A-star search
        # we denote the distance as the minimal distance on a string we need to travel before overlapping with next
        from collections import deque
        import heapq

        def distance(s1: str, s2: str) -> int:
            for i in range(1, len(s1)):
                if s2.startswith(s1[i:]):
                    return len(s1) - i
            return 0

        def pathtoStr(path):
            res = words[path[0]]
            for i in range(1, len(path)):
                res += words[path[i]][graph[path[i - 1]][path[i]]:]
            return res

        graph = [[0] * len(words) for _ in range(len(words))]
        for i in range(len(words)):
            for j in range(i + 1, len(words)):
                graph[i][j] = distance(words[i], words[j])
                graph[j][i] = distance(words[j], words[i])

        d = [[0] * len(words) for _ in range(1 << len(words))]
        queue = deque((i, 1 << i, [i], 0) for i in range(len(words)))
        length = -1  # record the maximum s_len
        path = []  # record the path corresponding to maximum s_len
        while queue:
            node, mask, path, dis = queue.popleft()
            if dis < d[mask][node]:
                continue
            if mask == (1 << len(words)) - 1 and dis > length:
                path, length = path, dis
                continue
            for i in range(len(words)):
                new_mask = mask | (1 << i)
                # case1: make sure that each node is only traversed once
                # case2: only if we can get larger save length, we consider it.
                if new_mask != mask and d[mask][node] + graph[node][i] >= d[new_mask][i]:
                    d[new_mask][i] = d[mask][node] + graph[node][i]
                    queue.append((i, new_mask, path + [i], d[new_mask][i]))

        # queue = []
        # for i in range(len(words)):
        #     distance = tuple(1 if j == i else 0 for j in range(len(words)))
        #     heapq.heappush(queue, (len(words[i]), distance, [i]))

        return pathtoStr(path)


def test():
    arguments = [
        ["alex", "loves", "leetcode"],
        ["catg", "ctaagt", "gcta", "ttca", "atgcatc"],
        # [''.join(chr(random.randint(97, 122)) for _ in range(20)) for _ in range(12)]
    ]
    expectations = [
        "alexlovesleetcode",
        "gctaagttcatgcatc",
    ]
    for words, expected in zip(arguments, expectations):
        solution = Solution().shortestSuperstring(words)
        assert len(solution) == len(expected)  # why not return length instead... so much easier to verify


test()
