### Source : https://leetcode.com/problems/serialize-and-deserialize-bst/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-06

##################################################################################################### 
#
# Serialization is converting a data structure or object into a sequence of bits so that it can be 
# stored in a file or memory buffer, or transmitted across a network connection link to be 
# reconstructed later in the same or another computer environment.
# 
# Design an algorithm to serialize and deserialize a binary search tree. There is no restriction on 
# how your serialization/deserialization algorithm should work. You need to ensure that a binary 
# search tree can be serialized to a string, and this string can be deserialized to the original tree 
# structure.
# 
# The encoded string should be as compact as possible.
# 
# Example 1:
# Input: root = [2,1,3]
# Output: [2,1,3]
# Example 2:
# Input: root = []
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 104].
# 	0 <= Node.val <= 104
# 	The input tree is guaranteed to be a binary search tree.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Codec:
    # this is the same as SerializeDeserializeBinaryTree - leetcode problem 297
    def serialize(self, root):
        """Encodes a tree to a single string.
        """
        return f'{root.val},{self.serialize(root.left)}{self.serialize(root.right)}' if root else ','

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        """

        def dfs():
            val = next(vals)
            if not val:
                return None
            node = TreeNode(int(val))
            node.left = dfs()
            node.right = dfs()
            return node

        vals = iter(data.split(','))
        return dfs()


def test():
    from python3 import Codec as MyCodec
    arguments = [
        "[1,2,3,null,null,4,5]",
        "[]",
        "[1]",
        "[1,2]",
        ]
    coded = Codec()
    for serialized_tree in arguments:
        root = MyCodec.deserialize(serialized_tree)
        reconstructed = coded.deserialize(coded.serialize(root))
        assert reconstructed is not root if reconstructed is not None else True  # None is None
        assert reconstructed == root
