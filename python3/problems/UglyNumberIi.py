### Source : https://leetcode.com/problems/ugly-number-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-29

##################################################################################################### 
#
# An ugly number is a positive integer whose prime factors are limited to 2, 3, and 5.
# 
# Given an integer n, return the nth ugly number.
# 
# Example 1:
# 
# Input: n = 10
# Output: 12
# Explanation: [1, 2, 3, 4, 5, 6, 8, 9, 10, 12] is the sequence of the first 10 ugly numbers.
# 
# Example 2:
# 
# Input: n = 1
# Output: 1
# Explanation: 1 has no prime factors, therefore all of its prime factors are limited to 2, 3, and 5.
# 
# Constraints:
# 
# 	1 <= n <= 1690
#####################################################################################################


class Solution:
    def nthUglyNumber(self, n: int) -> int:  # must be TLE
        # really dumb approach but it was my first approach as a novice.
        def prime_factors(number: int):
            factors = []
            while number % 2 == 0:
                factors.append(2)
                number = number // 2
            for i in range(3, int(number ** 0.5) + 1, 2):
                while number % i == 0:
                    factors.append(i)
                    number = number // i
            if number > 1:
                factors.append(number)
            return factors

        def is_ugly(number: int):
            return not set(prime_factors(number)) - {2, 3, 5}

        ugly_numbers = []
        i = 1
        while len(ugly_numbers) < n:
            if is_ugly(i):
                ugly_numbers.append(i)
            i += 1
        return ugly_numbers[-1]

    def nthUglyNumber(self, n: int) -> int:  # O(n) time and (n) space: 350 ms
        factors = (2, 3, 5)
        k = len(factors)
        starts, ugly_numbers = [0] * k, [1]
        for i in range(n - 1):
            candidates = [factors[i] * ugly_numbers[starts[i]] for i in range(k)]
            new_num = min(candidates)
            ugly_numbers.append(new_num)
            starts = [starts[i] + (candidates[i] == new_num) for i in range(k)]
        return ugly_numbers[-1]

    def nthUglyNumber(self, n: int) -> int:  # O(n) time and (n) space: 164 ms
        i = j = k = 0
        ugly_numbers = [1]
        while len(ugly_numbers) < n:
            ugly_numbers.append(min(ugly_numbers[i] * 2, ugly_numbers[j] * 3, ugly_numbers[k] * 5))
            if ugly_numbers[-1] == ugly_numbers[i] * 2:
                i += 1
            if ugly_numbers[-1] == ugly_numbers[j] * 3:
                j += 1
            if ugly_numbers[-1] == ugly_numbers[k] * 5:
                k += 1
        return ugly_numbers[-1]

    ugly = sorted(2**a * 3**b * 5**c for a in range(32) for b in range(20) for c in range(14))

    def nthUglyNumber(self, n):  # precompute above and O(1) look-up here
        return self.ugly[n-1]


def test():
    arguments = [10, 1, 100, 1600]
    expectations = [12, 1, 1536, 1399680000]
    for n, expected in zip(arguments, expectations):
        solution = Solution().nthUglyNumber(n)
        assert solution == expected
