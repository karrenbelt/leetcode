### Source : https://leetcode.com/problems/fizz-buzz/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-26

##################################################################################################### 
#
# Given an integer n, return a string array answer (1-indexed) where:
# 
# 	answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
# 	answer[i] == "Fizz" if i is divisible by 3.
# 	answer[i] == "Buzz" if i is divisible by 5.
# 	answer[i] == i if non of the above conditions are true.
# 
# Example 1:
# Input: n = 3
# Output: ["1","2","Fizz"]
# Example 2:
# Input: n = 5
# Output: ["1","2","Fizz","4","Buzz"]
# Example 3:
# Input: n = 15
# Output: ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"]
# 
# Constraints:
# 
# 	1 <= n <= 104
#####################################################################################################

from typing import List


class Solution:
    sequence = ["FizzBuzz" if i % 3 == i % 5 == 0 else "Fizz" if i % 3 == 0 else "Buzz" if i % 5 == 0 else str(i)
                for i in range(1, 10_000 + 1)]

    def fizzBuzz(self, n: int) -> List[str]:
        return self.sequence[:n]

    def fizzBuzz(self, n: int) -> List[str]:
        return ['Fizz' * (not i % 3) + 'Buzz' * (not i % 5) or str(i) for i in range(1, n + 1)]


def test():
    arguments = [3, 5, 15]
    expectations = [
        ["1", "2", "Fizz"],
        ["1", "2", "Fizz", "4", "Buzz"],
        ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"]
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().fizzBuzz(nums)
        assert solution == expected
