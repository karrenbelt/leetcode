### Source : https://leetcode.com/problems/heaters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Winter is coming! During the contest, your first job is to design a standard heater with a fixed 
# warm radius to warm all the houses.
# 
# Every house can be warmed, as long as the house is within the heater's warm radius range. 
# 
# Given the positions of houses and heaters on a horizontal line, return the minimum radius standard 
# of heaters so that those heaters could cover all houses.
# 
# Notice that all the heaters follow your radius standard, and the warm radius will the same.
# 
# Example 1:
# 
# Input: houses = [1,2,3], heaters = [2]
# Output: 1
# Explanation: The only heater was placed in the position 2, and if we use the radius 1 standard, 
# then all the houses can be warmed.
# 
# Example 2:
# 
# Input: houses = [1,2,3,4], heaters = [1,4]
# Output: 1
# Explanation: The two heater was placed in the position 1 and 4. We need to use radius 1 standard, 
# then all the houses can be warmed.
# 
# Example 3:
# 
# Input: houses = [1,5], heaters = [2]
# Output: 3
# 
# Constraints:
# 
# 	1 <= houses.length, heaters.length <= 3 * 104
# 	1 <= houses[i], heaters[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def findRadius(self, houses: List[int], heaters: List[int]) -> int:  # O(n log n)
        houses.sort()
        heaters.sort()
        heaters = [float('-inf')] + heaters + [float('inf')]
        i = radius = 0
        for house in houses:
            while house > heaters[i + 1]:
                i += 1
            radius = max(radius, min(house - heaters[i], heaters[i + 1] - house))
        return radius


def test():
    arguments = [
        ([1, 2, 3], [2]),
        ([1, 2, 3, 4], [1, 4]),
        ([1, 5], [2]),
        ]
    expectations = [1, 1, 3]
    for (houses, heaters), expected in zip(arguments, expectations):
        solution = Solution().findRadius(houses, heaters)
        assert solution == expected
