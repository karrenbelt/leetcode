### Source : https://leetcode.com/problems/maximum-xor-with-an-element-from-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-01

##################################################################################################### 
#
# You are given an array nums consisting of non-negative integers. You are also given a queries 
# array, where queries[i] = [xi, mi].
# 
# The answer to the ith query is the maximum bitwise XOR value of xi and any element of nums that 
# does not exceed mi. In other words, the answer is max(nums[j] XOR xi) for all j such that nums[j] 
# <= mi. If all elements in nums are larger than mi, then the answer is -1.
# 
# Return an integer array answer where answer.length == queries.length and answer[i] is the answer to 
# the ith query.
# 
# Example 1:
# 
# Input: nums = [0,1,2,3,4], queries = [[3,1],[1,3],[5,6]]
# Output: [3,3,7]
# Explanation:
# 1) 0 and 1 are the only two integers not greater than 1. 0 XOR 3 = 3 and 1 XOR 3 = 2. The larger of 
# the two is 3.
# 2) 1 XOR 2 = 3.
# 3) 5 XOR 2 = 7.
# 
# Example 2:
# 
# Input: nums = [5,2,4,6,6,3], queries = [[12,4],[8,1],[6,3]]
# Output: [15,-1,5]
# 
# Constraints:
# 
# 	1 <= nums.length, queries.length <= 105
# 	queries[i].length == 2
# 	0 <= nums[j], xi, mi <= 109
#####################################################################################################

from typing import List


class Solution:
    def maximizeXor(self, nums: List[int], queries: List[List[int]]) -> List[int]:  # TLE: 54 / 67 test cases passed.
        maximized = []
        nums[:] = sorted(set(nums))
        for x, m in queries:
            maximum = -1
            for n in nums:
                if n > m:
                    break
                maximum = max(maximum, x ^ n)
            maximized.append(maximum)
        return maximized

    def maximizeXor(self, nums: List[int], queries: List[List[int]]) -> List[int]:  # O(n log n) time and O(n) space
        # The best number has the opposite bit at every possible positions
        # we search from the most significant to the least significant bit
        def bits(n: int):
            return map(int, f'{n:030b}')

        nums.sort(reverse=True)
        queries = sorted((m, x, i) for i, (x, m) in enumerate(queries))
        maximized = [-1] * len(queries)
        trie = {}
        for m, x, i in queries:
            while nums and nums[-1] <= m:
                node = trie
                for c in bits(nums[-1]):
                    node = node.setdefault(c, {})
                node['value'] = nums.pop()
            if trie:
                node = trie
                for c in bits(x):
                    node = node.get(1 - c) or node.get(c)
                maximized[i] = x ^ node['value']
        return maximized

    def maximizeXor(self, nums: List[int], queries: List[List[int]]) -> List[int]:  # O(n log(n) + q log(n)) time
        import bisect
        nums.sort()
        maximized = []
        for x, m in queries:
            start, stop = 0, bisect.bisect_right(nums, m)
            num = 0
            for i in range(30)[::-1]:
                cut = bisect.bisect_left(nums, num + 2**i, start, stop)
                if cut > start and x & 1 << i:
                    stop = cut
                elif cut < stop:
                    start = cut
                    num += 1 << i
            maximized.append(num ^ x if start < stop else -1)
        return maximized

    def maximizeXor(self, nums: List[int], queries: List[List[int]]) -> List[int]:
        import bisect
        nums.sort()
        maximized = []
        for x, m in queries:
            start, stop = 0, bisect.bisect_right(nums, m)
            num = 0
            bit = 1 << m.bit_length()
            while bit:
                cut = bisect.bisect_left(nums, num + bit, start, stop)
                if cut != stop:
                    if cut != start and x & bit:
                        stop = cut
                    else:
                        start = cut
                        num += bit
                bit >>= 1
            maximized.append(num ^ x if start < stop else -1)
        return maximized


def test():
    arguments = [
        ([0, 1, 2, 3, 4], [[3, 1], [1, 3], [5, 6]]),
        ([5, 2, 4, 6, 6, 3], [[12, 4], [8, 1], [6, 3]]),
    ]
    expectations = [
        [3, 3, 7],
        [15, -1, 5],
    ]
    for (nums, queries), expected in zip(arguments, expectations):
        solution = Solution().maximizeXor(nums, queries)
        assert solution == expected
