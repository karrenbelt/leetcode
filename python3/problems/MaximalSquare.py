### Source : https://leetcode.com/problems/maximal-square/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-27

##################################################################################################### 
#
# Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and 
# return its area.
# 
# Example:
# 
# Input: 
# 
# 1 0 1 0 0
# 1 0 1 1 1
# 1 1 1 1 1
# 1 0 0 1 0
# 
# Output: 4
#####################################################################################################
from typing import List


class Solution:
    def maximalSquare(self, matrix):
        dp = [[0 for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
        max_area = 0
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if i == 0 or j == 0:
                    dp[i][j] = int(matrix[i][j])
                elif int(matrix[i][j]) == 1:
                    dp[i][j] = min(dp[i - 1][j - 1], dp[i][j - 1], dp[i - 1][j]) + 1
                max_area = max(max_area, dp[i][j])
        return max_area * max_area


def test():
    matrices = [
        [
            [1, 0, 1, 0, 0, ],
            [1, 0, 1, 1, 1, ],
            [1, 1, 1, 1, 1, ],
            [1, 0, 0, 1, 0, ],
        ],
        [
            [0, 1],
            [1, 0],
        ],
    ]
    expectations = [4, 1]
    for matrix, expected in zip(matrices, expectations):
        solution = Solution().maximalSquare(matrix)
        assert solution == expected
