### Source : https://leetcode.com/problems/open-the-lock/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# You have a lock in front of you with 4 circular wheels. Each wheel has 10 slots: '0', '1', '2', 
# '3', '4', '5', '6', '7', '8', '9'. The wheels can rotate freely and wrap around: for example we can 
# turn '9' to be '0', or '0' to be '9'. Each move consists of turning one wheel one slot.
# 
# The lock initially starts at '0000', a string representing the state of the 4 wheels.
# 
# You are given a list of deadends dead ends, meaning if the lock displays any of these codes, the 
# wheels of the lock will stop turning and you will be unable to open it.
# 
# Given a target representing the value of the wheels that will unlock the lock, return the minimum 
# total number of turns required to open the lock, or -1 if it is impossible.
# 
# Example 1:
# 
# Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
# Output: 6
# Explanation:
# A sequence of valid moves would be "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> 
# "0202".
# Note that a sequence like "0000" -> "0001" -> "0002" -> "0102" -> "0202" would be invalid,
# because the wheels of the lock become stuck after the display becomes the dead end "0102".
# 
# Example 2:
# 
# Input: deadends = ["8888"], target = "0009"
# Output: 1
# Explanation:
# We can turn the last wheel in reverse to move from "0000" -> "0009".
# 
# Example 3:
# 
# Input: deadends = ["8887","8889","8878","8898","8788","8988","7888","9888"], target = "8888"
# Output: -1
# Explanation:
# We can't reach the target without getting stuck.
# 
# Example 4:
# 
# Input: deadends = ["0000"], target = "8888"
# Output: -1
# 
# Constraints:
# 
# 	1 <= deadends.length <= 500
# 	deadends[i].length == 4
# 	target.length == 4
# 	target will not be in the list deadends.
# 	target and deadends[i] consist of digits only.
#####################################################################################################

from typing import List


class Solution:
    def openLock(self, deadends: List[str], target: str) -> int:
        from collections import deque

        step = -1
        root = "0000"
        queue = deque([root])
        visited = {root}
        dead_ends = set(deadends)

        if root in dead_ends:
            return step

        while queue:
            step += 1
            for i in range(len(queue)):
                node = queue.popleft()
                if node == target:
                    return step

                # start rotating
                for j in range(len(node)):  # all positions
                    for k in (-1, 1):  # rotate one up / down
                        new_digit = str((int(node[j]) + k) % 10)
                        next_node = node[:j] + new_digit + node[j + 1:]
                        if next_node not in dead_ends and next_node not in visited:
                            queue.append(next_node)
                            visited.add(next_node)
        return -1

    def openLock(self, deadends: List[str], target: str) -> int:

        step = -1
        root = "0000"
        queue = [root]
        visited = set(deadends)

        if root in visited:
            return step

        visited.add(root)
        while queue:
            step += 1
            next_level = []
            for i in range(len(queue)):
                node = queue.pop()
                if node == target:
                    return step
                for neighbor in (f'{node[:j]}{(int(node[j])+k)%10}{node[j+1:]}' for j in range(4) for k in (-1, 1)):
                    if neighbor not in visited:
                        next_level.append(neighbor)
                        visited.add(neighbor)
            queue = next_level
        return -1

    def openLock(self, deadends: List[str], target: str) -> int:

        def next_states(state: str) -> str:
            for i in range(len(state)):
                yield state[:i] + str((int(state[i]) + 1) % 10) + state[i + 1:]
                yield state[:i] + str(9 if state[i] == "0" else int(state[i]) - 1) + state[i + 1:]

        queue = [("0000", 0)]
        seen = set(deadends)
        if "0000" in seen:
            return -1

        while queue:
            new_queue = []
            for node, n_moves in queue:
                if node == target:
                    return n_moves
                for next_node in next_states(node):
                    if next_node not in seen:
                        seen.add(next_node)
                        new_queue.append((next_node, n_moves + 1))
            queue = new_queue
        return -1


def test():
    arguments = [
        (["0201", "0101", "0102", "1212", "2002"], "0202"),
        (["8888"], "0009"),
        (["8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"], "8888"),
        (["0000"], "0000"),
        ]
    expectations = [6, 1, -1, -1]
    for (deadends, target), expected in zip(arguments, expectations):
        solution = Solution().openLock(deadends, target)
        assert solution == expected
