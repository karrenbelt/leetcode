### Source : https://leetcode.com/problems/same-tree
### Author : karrenbelt
### Date   : 2019-05-15

##################################################################################################### 
#
# Given two binary trees, write a function to check if they are the same or not.
# 
# Two binary trees are considered the same if they are structurally identical and the nodes have the 
# same value.
# 
# Example 1:
# 
# Input:     1         1
#           / \       / \
#          2   3     2   3
# 
#         [1,2,3],   [1,2,3]
# 
# Output: true
# 
# Example 2:
# 
# Input:     1         1
#           /           \
#          2             2
# 
#         [1,2],     [1,null,2]
# 
# Output: false
# 
# Example 3:
# 
# Input:     1         1
#           / \       / \
#          2   1     1   2
# 
#         [1,2,1],   [1,1,2]
# 
# Output: false
# 
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode

class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:  # recursive
        if not p and not q:
            return True
        if not q or not p:
            return False
        if p.val != q.val:
            return False
        return self.isSameTree(p.right, q.right) and self.isSameTree(p.left, q.left)

    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:  # recursive one-liner
        return p and q and p.val == q.val and all(map(self.isSameTree, (p.left, p.right), (q.left, q.right))) or p is q

    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:  # iterative using queue
        from collections import deque

        def check(p, q):
            if not p and not q:
                return True
            if not q or not p:
                return False
            return p.val == q.val

        deq = deque([(p, q), ])
        while deq:
            p, q = deq.popleft()
            if not check(p, q):
                return False
            if p:
                deq.append((p.left, q.left))
                deq.append((p.right, q.right))
        return True

    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:  # iterative using stack

        def are_equal(p, q):
            if not p and not q:
                return True
            if not p or not q:
                return False
            return p.val == q.val

        stack = [(p, q), ]
        while stack:
            p, q = stack.pop()
            if not are_equal(p, q):
                return False
            elif p and q:
                stack.append((p.left, q.left))
                stack.append((p.right, q.right))
        return True


def test():
    from python3 import Codec
    deserialized_trees = [
        ("[1, 2, 3]", "[1, 2, 3]"),
        ("[1, 2]", "[1, null, 2]"),
        ("[1,2,1]", "[1,1,2]"),
        ]
    expectations = [True, False, False]
    for (nums1, nums2), expected in zip(deserialized_trees, expectations):
        p, q = map(Codec.deserialize, (nums1, nums2))
        solution = Solution().isSameTree(p, q)
        assert solution == expected
