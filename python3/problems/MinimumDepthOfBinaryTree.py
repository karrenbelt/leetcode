### Source : https://leetcode.com/problems/minimum-depth-of-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-13

##################################################################################################### 
#
# Given a binary tree, find its minimum depth.
# 
# The minimum depth is the number of nodes along the shortest path from the root node down to the 
# nearest leaf node.
# 
# Note: A leaf is a node with no children.
# 
# Example 1:
# 
# Input: root = [3,9,20,null,null,15,7]
# Output: 2
# 
# Example 2:
# 
# Input: root = [2,null,3,null,4,null,5,null,6]
# Output: 5
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 105].
# 	-1000 <= Node.val <= 1000
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def minDepth(self, root: TreeNode) -> int:  # bfs, early stop as soon as level does not have children
        from collections import deque

        if not root:
            return 0

        queue = deque([(root, 1)])
        while queue:
            node, depth = queue.popleft()
            if not node.left and not node.right:
                return depth
            if node.left:
                queue.append((node.left, depth + 1))
            if node.right:
                queue.append((node.right, depth + 1))

    def minDepth(self, root: TreeNode) -> int:
        if not root:
            return 0
        d = sorted(map(self.minDepth, (root.left, root.right)))
        return 1 + (min(d) or max(d))

    def minDepth(self, root: TreeNode):

        def dfs(node):
            if not node:
                return float("inf")
            if not node.left and not node.right:
                return 1
            return min(dfs(node.left), dfs(node.right)) + 1

        minimal_depth = dfs(root)
        return minimal_depth if minimal_depth != float("inf") else 0


def test():
    from python3 import Codec
    deserialized_trees = [
        "[3,9,20,null,null,15,7]",
        "[2,null,3,null,4,null,5,null,6]",
        ]
    expectations = [2, 5]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        solution = Solution().minDepth(root)
        assert solution == expected
