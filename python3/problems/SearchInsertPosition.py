### Source : https://leetcode.com/problems/search-insert-position/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# Given a sorted array of distinct integers and a target value, return the index if the target is 
# found. If not, return the index where it would be if it were inserted in order.
# 
# Example 1:
# Input: nums = [1,3,5,6], target = 5
# Output: 2
# Example 2:
# Input: nums = [1,3,5,6], target = 2
# Output: 1
# Example 3:
# Input: nums = [1,3,5,6], target = 7
# Output: 4
# Example 4:
# Input: nums = [1,3,5,6], target = 0
# Output: 0
# Example 5:
# Input: nums = [1], target = 0
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-104 <= nums[i] <= 104
# 	nums contains distinct values sorted in ascending order.
# 	-104 <= target <= 104
#####################################################################################################

from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:  # binary search: O(log n) time
        left, right = 0, len(nums) - 1
        while left <= right:
            mid = (left + right) // 2  # left + (right - left) // 2 is better since no overflow
            if nums[mid] >= target:
                right = mid - 1
            else:
                left = mid + 1
        return left

    def searchInsert(self, nums: List[int], target: int) -> int:  # O(log n) time
        # clearer solution. Note no len(nums) - 1, as we might need to insert at the end of the array
        left, right = 0, len(nums)
        while left < right:
            mid = left + right >> 1
            if nums[mid] < target:
                left = mid + 1
            else:
                right = mid
        return left

    def searchInsert(self, nums: List[int], target: int) -> int:
        import bisect
        return bisect.bisect_left(nums, target)


def test():
    arrays_of_numbers = [
        [1, 3, 5, 6],
        [1, 3, 5, 6],
        [1, 3, 5, 6],
        [1, 3, 5, 6],
        [1],
        ]
    targets = [5, 2, 7, 0, 0]
    expectations = [2, 1, 4, 0, 0]
    for nums, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().searchInsert(nums, target)
        assert solution == expected, (solution, expected)
