### Source : https://leetcode.com/problems/summary-ranges/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-09

##################################################################################################### 
#
# You are given a sorted unique integer array nums.
# 
# Return the smallest sorted list of ranges that cover all the numbers in the array exactly. That is, 
# each element of nums is covered by exactly one of the ranges, and there is no integer x such that x 
# is in one of the ranges but not in nums.
# 
# Each range [a,b] in the list should be output as:
# 
# 	"a->b" if a != b
# 	"a" if a == b
# 
# Example 1:
# 
# Input: nums = [0,1,2,4,5,7]
# Output: ["0->2","4->5","7"]
# Explanation: The ranges are:
# [0,2] --> "0->2"
# [4,5] --> "4->5"
# [7,7] --> "7"
# 
# Example 2:
# 
# Input: nums = [0,2,3,4,6,8,9]
# Output: ["0","2->4","6","8->9"]
# Explanation: The ranges are:
# [0,0] --> "0"
# [2,4] --> "2->4"
# [6,6] --> "6"
# [8,9] --> "8->9"
# 
# Example 3:
# 
# Input: nums = []
# Output: []
# 
# Example 4:
# 
# Input: nums = [-1]
# Output: ["-1"]
# 
# Example 5:
# 
# Input: nums = [0]
# Output: ["0"]
# 
# Constraints:
# 
# 	0 <= nums.length <= 20
# 	-231 <= nums[i] <= 231 - 1
# 	All the values of nums are unique.
# 	nums is sorted in ascending order.
#####################################################################################################

from typing import List


class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:  # O(n) time and O(n) space
        i, parts = 0, []
        for j in range(1, len(nums) + 1):
            if j == len(nums) or nums[j] != nums[j - 1] + 1:
                parts.append([nums[i], nums[j - 1]])
                i = j
        return [f"{n1}" if n1 == n2 else f"{n1}->{n2}" for n1, n2 in parts]
    

def test():
    arguments = [
        [0, 1, 2, 4, 5, 7],
        [0, 2, 3, 4, 6, 8, 9],
        [],
        [-1],
        [0],
        ]
    expectations = [
        ["0->2", "4->5", "7"],
        ["0", "2->4", "6", "8->9"],
        [],
        ["-1"],
        ["0"],
        ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().summaryRanges(nums)
        assert solution == expected
