### Source : https://leetcode.com/problems/n-queens/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two 
# queens attack each other.
# 
# Given an integer n, return all distinct solutions to the n-queens puzzle.
# 
# Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' 
# both indicate a queen and an empty space, respectively.
# 
# Example 1:
# 
# Input: n = 4
# Output: [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
# Explanation: There exist two distinct solutions to the 4-queens puzzle as shown above
# 
# Example 2:
# 
# Input: n = 1
# Output: [["Q"]]
# 
# Constraints:
# 
# 	1 <= n <= 9
#####################################################################################################

from typing import List


class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:

        def build_board(positions: List):
            board = [['.'] * n for _ in range(n)]
            for x, y in positions:
                board[x][y] = 'Q'
            return (''.join(row) for row in board)

        def backtrack(i: int, path: List):
            if len(path) == n:
                solutions.append(build_board(path))
                return
            for j in range(n):
                if j not in cols and i - j not in diag and i + j not in off_diag:
                    cols.add(j)
                    diag.add(i - j)
                    off_diag.add(i + j)
                    backtrack(i + 1, path + [(i, j)])
                    off_diag.remove(i + j)
                    diag.remove(i - j)
                    cols.remove(j)

        cols, diag, off_diag = (set() for _ in range(3))
        solutions = []
        backtrack(0, [])
        return list(map(list, solutions))


def test():
    arguments = [4, 1]
    expectations = [
        [[".Q..", "...Q", "Q...", "..Q."], ["..Q.", "Q...", "...Q", ".Q.."]],
        [["Q"]],
        ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().solveNQueens(n)
        assert solution == expected

