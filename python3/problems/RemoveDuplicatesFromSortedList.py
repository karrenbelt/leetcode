### Source : https://leetcode.com/problems/remove-duplicates-from-sorted-list/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-26

##################################################################################################### 
#
# Given a sorted linked list, delete all duplicates such that each element appear only once.
# 
# Example 1:
# 
# Input: 1->1->2
# Output: 1->2
# 
# Example 2:
# 
# Input: 1->1->2->3->3
# Output: 1->2->3
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if not head:
            return head
        node = head
        while node and node.next:
            if node.val == node.next.val:
                node.next = node.next.next
                continue
            node = node.next
        return head

    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if not head:
            return head
        node = head
        while node.next:
            if node.val == node.next.val:
                node.next = node.next.next
            else:
                node = node.next
        return head

    def deleteDuplicates(self, head: ListNode) -> ListNode:
        node = head
        while node:
            while node.next and node.next.val == node.val:
                node.next = node.next.next
            node = node.next
        return head


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        [1, 1, 2],
        [1, 1, 2, 3, 3],
        ]
    expectations = [
        [1, 2],
        [1, 2, 3],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        head = singly_linked_list_from_array(nums).head
        solution = Solution().deleteDuplicates(head)
        assert solution == singly_linked_list_from_array(expected).head

