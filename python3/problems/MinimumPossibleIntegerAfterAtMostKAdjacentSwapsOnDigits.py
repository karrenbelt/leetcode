### Source : https://leetcode.com/problems/minimum-possible-integer-after-at-most-k-adjacent-swaps-on-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-12

#####################################################################################################
#
# Given a string num representing the digits of a very large integer and an integer k.
#
# You are allowed to swap any two adjacent digits of the integer at most k times.
#
# Return the minimum integer you can obtain also as a string.
#
# Example 1:
#
# Input: num = "4321", k = 4
# Output: "1342"
# Explanation: The steps to obtain the minimum integer from 4321 with 4 adjacent swaps are shown.
#
# Example 2:
#
# Input: num = "100", k = 1
# Output: "010"
# Explanation: It's ok for the output to have leading zeros, but the input is guaranteed not to have
# any leading zeros.
#
# Example 3:
#
# Input: num = "36789", k = 1000
# Output: "36789"
# Explanation: We can keep the number without any swaps.
#
# Example 4:
#
# Input: num = "22", k = 22
# Output: "22"
#
# Example 5:
#
# Input: num = "9438957234785635408", k = 23
# Output: "0345989723478563548"
#
# Constraints:
#
# 	1 <= num.length <= 30000
# 	num contains digits only and doesn't have leading zeros.
# 	1 <= k <= 109
#####################################################################################################



class Solution:
    def minInteger(self, num: str, k: int) -> str:  # O(n^2) time: TLE -> 47 / 51 test cases passed.
        # first we move as many lowest digits up front as possible, then we perform backtracking on the remaining k
        i = 0
        while k > 0 and i < len(num):  # keep swapping until we run out of moves or num is completely sorted
            j, minimum = min(enumerate(num[i: min(i + k + 1, len(num))], start=i), key=lambda x: x[1])
            num, k, i = num[:i] + minimum + num[i:j] + num[j + 1:], k + i - j, i + 1
        return num

    def minInteger(self, num: str, k: int) -> str:  # O(n log n) time
        import bisect

        digits = {}
        for i, c in enumerate(num):
            digits.setdefault(c, []).append(i)
        any(v.reverse() for v in digits.values())

        ret, moved = '', []
        for _ in range(len(num)):
            for c in map(str, range(10)):
                if digits.get(c, []):  # we pop the list, we need to check for entries, not just c in digits
                    idx = digits[c][-1]
                    real_idx = idx - bisect.bisect(moved, idx)
                    if real_idx <= k:
                        k -= real_idx
                        ret += c
                        bisect.insort(moved, digits[c].pop())
                        break
        return ret


def test():
    arguments = [
        ("4321", 4),
        ("100", 1),
        ("36789", 1000),
        ("22", 22),
        ("9438957234785635408", 23),
    ]
    expectations = [
        "1342",
        "010",
        "36789",
        "22",
        "0345989723478563548",
    ]
    for (num, k), expected in zip(arguments, expectations):
        solution = Solution().minInteger(num, k)
        assert solution == expected
