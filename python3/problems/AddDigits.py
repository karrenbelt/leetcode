### Source : https://leetcode.com/problems/add-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.
# 
# Example:
# 
# Input: 38
# Output: 2 
# Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2. 
#              Since 2 has only one digit, return it.
# 
# Follow up:
# Could you do it without any loop/recursion in O(1) runtime?
#####################################################################################################


class Solution:

    def addDigits(self, num: int) -> int:
        # str - list - int conversion with recursion
        str_num = str(num)
        if len(str_num) == 1:
            return num
        return self.addDigits(sum([int(i) for i in list(str_num)]))

    def addDigits(self, num: int) -> int:
        # floor divide and module
        num = num // 10 + num % 10
        if num < 10:
            return num
        return self.addDigits(num)

    def addDigits(self, num: int) -> int:
        # optimized, one-liner with no loop and no recursion; O(1) time
        return 9 if num % 9 == 0 and num != 0 else num % 9


def test():
    numbers = [38, ]
    expectations = [2, ]
    for num, expected in zip(numbers, expectations):
        solution = Solution().addDigits(num)
        assert solution == expected

