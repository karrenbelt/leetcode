### Source : https://leetcode.com/problems/most-common-word/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

#####################################################################################################
#
# Given a paragraph and a list of banned words, return the most frequent word that is not in the list
# of banned words.  It is guaranteed there is at least one word that isn't banned, and that the
# answer is unique.
#
# Words in the list of banned words are given in lowercase, and free of punctuation.  Words in the
# paragraph are not case sensitive.  The answer is in lowercase.
#
# Example:
#
# Input:
# paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
# banned = ["hit"]
# Output: "ball"
# Explanation:
# "hit" occurs 3 times, but it is a banned word.
# "ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the
# paragraph.
# Note that words in the paragraph are not case sensitive,
# that punctuation is ignored (even if adjacent to words, such as "ball,"),
# and that "hit" isn't the answer even though it occurs more because it is banned.
#
# Note:
#
# 	1 <= paragraph.length <= 1000.
# 	0 <= banned.length <= 100.
# 	1 <= banned[i].length <= 10.
# 	The answer is unique, and written in lowercase (even if its occurrences in paragraph may
# have uppercase symbols, and even if it is a proper noun.)
# 	paragraph only consists of letters, spaces, or the punctuation symbols !?',;.
# 	There are no hyphens or hyphenated words.
# 	Words only consist of letters, never apostrophes or other punctuation symbols.
#
#####################################################################################################

from typing import List


class Solution:
    def mostCommonWord(self, paragraph: str, banned: List[str]) -> str:  # O(n + m)
        import string
        words = paragraph.lower().translate(str.maketrans(string.punctuation, ' ' * 32)).split()
        non_banned_words = [w for w in words if w not in set(banned)]
        return max(non_banned_words, key=non_banned_words.count)

    def mostCommonWord(self, paragraph: str, banned: List[str]) -> str:  # O(n + m)
        import re
        words = [w for w in re.split(r'\W+', paragraph.lower()) if w not in set(banned)]
        return max(words, key=words.count)


def test():
    arguments = [
        ("Bob hit a ball, the hit BALL flew far after it was hit.", ["hit"]),
        ("a.", []),
    ]
    expectations = ["ball", "a"]
    for (paragraph, banned), expected in zip(arguments, expectations):
        solution = Solution().mostCommonWord(paragraph, banned)
        assert solution == expected
