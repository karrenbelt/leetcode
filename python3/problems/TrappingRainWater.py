### Source : https://leetcode.com/problems/trapping-rain-water/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given n non-negative integers representing an elevation map where the width of each bar is 1, 
# compute how much water it is able to trap after raining.
# 
# The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of 
# rain water (blue section) are being trapped. Thanks Marcos for contributing this image!
# 
# Example:
# 
# Input: [0,1,0,2,1,0,1,3,2,1,2,1]
# Output: 6
#####################################################################################################
from typing import List


class Solution:
    def trap(self, height: List[int]) -> int:  # brute force: O(n^2)
        # water is trapped between two peaks; the highest water level is that of the lowest peak of the two
        if len(height) < 3:
            return 0

        volume = 0
        for i in range(1, len(height) - 1):
            l_max = max(height[:i])
            r_max = max(height[i + 1:])
            diff = min(l_max, r_max)
            if diff - height[i] > 0:
                volume = volume + (diff - height[i])
        return volume

    def trap(self, height: List[int]) -> int:  # dp: O(n) time complexity, O(n) space

        if len(height) < 3:
            return 0

        volume = 0
        l_max = [0] * len(height)
        r_max = [0] * len(height)

        # find maximum height from left end
        l_max[0] = height[0]
        for i in range(1, len(height)-1):
            l_max[i] = max(l_max[i-1], height[i])

        # find maximum height from right end
        r_max[len(height) - 1] = height[-1]
        for i in range(2, len(height)):
            r_max[-i] = max(height[-i], r_max[-(i - 1)])

        for i in range(1, len(height) - 1):
            volume += min(l_max[i], r_max[i]) - height[i]
        return volume

    def trap(self, height: List[int]) -> int:  # dp: O(n) time complexity, O(n) space
        height = [0] + height + [0]
        l_max = [height[0]] + [0] * (len(height) - 1)
        r_max = [0] * (len(height) - 1) + [height[-1]]
        for i in range(1, len(height) - 1):
            l_max[i] = max(l_max[i - 1], height[i])
            r_max[~i] = max(r_max[~(i - 1)], height[~i])
        return sum(min(left, right) - h for left, right, h in zip(l_max, r_max, height))

    def trap(self, height: List[int]) -> int:  # stack; O(n) time O(n) space

        if len(height) < 3:
            return 0

        stack, volume = [], 0
        for i in range(len(height)):
            while stack and height[stack[-1]] < height[i]:
                idx = stack.pop()
                if stack:
                    left_idx = stack[-1]
                    volume += (min(height[i], height[left_idx]) - height[idx]) * (i - left_idx - 1)
            stack.append(i)
        return volume

    def trap(self, height: List[int]) -> int:  # two pointer approach; O(n) time O(1) space
        # we are dealing with monotonic functions (lower envelope -> left_max increasing and right_max non-increasing)
        if len(height) < 3:
            return 0

        volume = 0
        i, j = 0, len(height) - 1
        l_max, r_max = height[i], height[j]
        while i < j:
            l_max = max(l_max, height[i])
            r_max = max(r_max, height[j])
            if l_max <= r_max:
                volume += (l_max - height[i])
                i += 1
            else:
                volume += (r_max - height[j])
                j -= 1
        return volume


def test():
    arrays_of_numbers = [
        [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1],
        [2, 0, 2],
        [5, 4, 1, 2],
        [0, 2, 0, 1, 0, 2],
        [4, 3, 3, 9, 3, 0, 9, 2, 8, 3],
        [],
    ]
    expectations = [6, 2, 1, 5, 23, 0]
    for height, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().trap(height)
        assert solution == expected
