### Source : https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the 
# BST.
# 
# According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between two 
# nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node 
# to be a descendant of itself).&rdquo;
# 
# Example 1:
# 
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
# Output: 6
# Explanation: The LCA of nodes 2 and 8 is 6.
# 
# Example 2:
# 
# Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
# Output: 2
# Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant of itself according to 
# the LCA definition.
# 
# Example 3:
# 
# Input: root = [2,1], p = 2, q = 1
# Output: 2
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [2, 105].
# 	-109 <= Node.val <= 109
# 	All Node.val are unique.
# 	p != q
# 	p and q will exist in the BST.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        # store the path if the first find, see where the other deviates
        while root:
            next_p = root if root.val == p.val else root.left if p.val < root.val else root.right
            next_q = root if root.val == q.val else root.left if q.val < root.val else root.right
            if next_p is not next_q:
                break
            root = next_p
        return root

    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        while root:
            if max(p.val, q.val) < root.val:
                root = root.left
            elif min(p.val, q.val) > root.val:
                root = root.right
            else:
                return root

    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        while (root.val - p.val) * (root.val - q.val) > 0:
            root = (root.right, root.left)[root.val > max(p.val, q.val)]
        return root

    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        if root:
            if max(p.val, q.val) < root.val:
                return self.lowestCommonAncestor(root.left, p, q)
            elif min(p.val, q.val) > root.val:
                return self.lowestCommonAncestor(root.right, p, q)
            return root

    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        node = root.left if p.val < root.val > q.val else root.right if p.val > root.val < q.val else None
        return self.lowestCommonAncestor(node, p, q) if node else root


def test():
    from python3 import Codec
    serialized_trees = [
        ("[6,2,8,0,4,7,9,null,null,3,5]", 2, 8),
        ("[6,2,8,0,4,7,9,null,null,3,5]", 2, 4),
        ("[2,1]", 2, 1),
        ]
    expectations = [6, 2, 2, ]
    for (serialized_tree, p_val, q_val), expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        p, q = [node for node in root if node.val == p_val or node.val == q_val]
        solution = Solution().lowestCommonAncestor(root, p, q)
        assert solution.val == expected
