### Source : https://leetcode.com/problems/maximum-difference-by-remapping-a-digit/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-05

##################################################################################################### 
#
# You are given an integer num. You know that Danny ittal will sneakily remap one of the 10 possible 
# digits (0 to 9) to another digit.
# 
# Return the difference between the maximum and minimum values Danny can make by remapping exactly 
# one digit in num.
# 
# Notes:
# 
# 	When Danny remaps a digit d1 to another digit d2, Danny replaces all occurrences of d1 in 
# num with d2.
# 	Danny can remap a digit to itself, in which case num does not change.
# 	Danny can remap different digits for obtaining minimum and maximum values respectively.
# 	The resulting number after remapping can contain leading zeroes.
# 	We mentioned "Danny ittal" to congratulate him on being in the top 10 in Weekly Contest 
# 326.
# 
# Example 1:
# 
# Input: num = 11891
# Output: 99009
# Explanation: 
# To achieve the maximum value, Danny can remap the digit 1 to the digit 9 to yield 99899.
# To achieve the minimum value, Danny can remap the digit 1 to the digit 0, yielding 890.
# The difference between these two numbers is 99009.
# 
# Example 2:
# 
# Input: num = 90
# Output: 99
# Explanation:
# The maximum value that can be returned by the function is 99 (if 0 is replaced by 9) and the 
# minimum value that can be returned by the function is 0 (if 9 is replaced by 0).
# Thus, we return 99.
# 
# Constraints:
# 
# 	1 <= num <= 108
#####################################################################################################

from typing import Generator


class Solution:
    def minMaxDifference(self, num: int) -> int:  # O(log n) time & space
        s = str(num)
        c = next((c for c in s if c != "9"), "0")
        return int(s.replace(c, "9")) - int(s.replace(s[0], "0"))

    def minMaxDifference(self, num: int) -> int:  # O(log n) time & O(1) space

        def get_digits(x: int) -> Generator:
            if x:
                x, r = divmod(x, 10)
                yield from get_digits(x)
                yield r

        minimum = maximum = 0
        first_digit, first_not_nine = 0, 9
        for n in get_digits(num):
            if not first_digit:
                first_digit = n
            if n != 9 and first_not_nine == 9:
                first_not_nine = n
            minimum = minimum * 10 + n * (first_digit != n)
            maximum = maximum * 10 + (n, 9)[first_not_nine == n]

        return maximum - minimum


def test():
    arguments = [11891, 90]
    expectations = [99009, 99]
    for num, expected in zip(arguments, expectations):
        solution = Solution().minMaxDifference(num)
        assert solution == expected
