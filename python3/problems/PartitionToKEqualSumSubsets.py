### Source : https://leetcode.com/problems/partition-to-k-equal-sum-subsets/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# Given an array of integers nums and a positive integer k, find whether it's possible to divide this 
# array into k non-empty subsets whose sums are all equal.
# 
# Example 1:
# 
# Input: nums = [4, 3, 2, 3, 5, 2, 1], k = 4
# Output: True
# Explanation: It's possible to divide it into 4 subsets (5), (1, 4), (2,3), (2,3) with equal sums.
# 
# Note:
# 
# 	1 <= k <= len(nums) <= 16.
# 	0 < nums[i] < 10000.
# 
#####################################################################################################

from typing import List


class Solution:
    # def canPartitionKSubsets(self, nums: List[int], k: int) -> bool:
    #     nums.sort()
    #     ctr = {i: 0 for i in range(k)}
    #     while nums:
    #         ctr[min(ctr.items(), key=lambda x: x[1])[0]] += nums.pop()
    #     return len(set(ctr.values())) == 1

    def canPartitionKSubsets(self, nums: List[int], k: int) -> bool:
        if len(nums) < k:
            return False
        ASum = sum(nums)
        nums.sort(reverse=True)
        if ASum % k != 0:
            return False
        target = [ASum / k] * k

        def dfs(pos):
            if pos == len(nums):
                return True
            for i in range(k):
                if target[i] >= nums[pos]:
                    target[i] -= nums[pos]
                    if dfs(pos + 1):
                        return True
                    target[i] += nums[pos]
            return False
        return dfs(0)

    def canPartitionKSubsets(self, nums: List[int], k: int) -> bool:

        def dfs(idx: int):
            if idx == len(nums):
                return True  # len(set(buck)) == 1
            for i in range(k):
                buck[i] += nums[idx]
                if buck[i] <= k_sum and dfs(idx + 1):
                    return True
                buck[i] -= nums[idx]
                if buck[i] == 0:
                    break
            return False

        nums.sort(reverse=True)
        buck, k_sum = [0] * k, sum(nums) // k
        return dfs(0)


def test():
    arrays_of_numbers = [
        [4, 3, 2, 3, 5, 2, 1],
        [2, 2, 10, 5, 2, 7, 2, 2, 13],
        ]
    groups = [4, 3]
    expectations = [True, True]
    for nums, k, expected in zip(arrays_of_numbers, groups, expectations):
        solution = Solution().canPartitionKSubsets(nums, k)
        assert solution == expected
