### Source : https://leetcode.com/problems/combination-sum-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-15

##################################################################################################### 
#
# Given a collection of candidate numbers (candidates) and a target number (target), find all unique 
# combinations in candidates where the candidate numbers sum to target.
# 
# Each number in candidates may only be used once in the combination.
# 
# Note: The solution set must not contain duplicate combinations.
# 
# Example 1:
# 
# Input: candidates = [10,1,2,7,6,1,5], target = 8
# Output: 
# [
# [1,1,6],
# [1,2,5],
# [1,7],
# [2,6]
# ]
# 
# Example 2:
# 
# Input: candidates = [2,5,2,1,2], target = 5
# Output: 
# [
# [1,2,2],
# [5]
# ]
# 
# Constraints:
# 
# 	1 <= candidates.length <= 100
# 	1 <= candidates[i] <= 50
# 	1 <= target <= 30
#####################################################################################################

from typing import List


class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:

        def backtrack(target, index, path):
            if target < 0:
                return
            elif target == 0:
                answer.append(path)
                return
            for i in range(index, len(candidates)):
                if i > index and candidates[i] == candidates[i - 1]:
                    continue
                backtrack(target - candidates[i], i + 1, path + [candidates[i]])

        candidates.sort()
        answer = []
        backtrack(target, 0, [])
        return answer

    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:  # dp using tabulation
        candidates.sort()
        dp = [set() for _ in range(target+1)]
        dp[0].add(())
        for num in candidates:
            for i in range(target, num - 1, -1):
                for prev in dp[i-num]:
                    dp[i].add(prev + (num, ))
        return list(map(list, dp[-1]))


def test():
    arrays_of_numbers = [
        [10, 1, 2, 7, 6, 1, 5],
        [2, 5, 2, 1, 2],
    ]
    targets = [8, 5]
    expectations = [
        [
            [1, 1, 6],
            [1, 2, 5],
            [1, 7],
            [2, 6]
        ],
        [
            [1, 2, 2],
            [5]
        ]
    ]
    for candidates, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().combinationSum2(candidates, target)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
