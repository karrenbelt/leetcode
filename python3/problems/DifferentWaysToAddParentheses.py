### Source : https://leetcode.com/problems/different-ways-to-add-parentheses/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-26

##################################################################################################### 
#
# Given a string expression of numbers and operators, return all possible results from computing all 
# the different possible ways to group numbers and operators. You may return the answer in any order.
# 
# Example 1:
# 
# Input: expression = "2-1-1"
# Output: [0,2]
# Explanation:
# ((2-1)-1) = 0 
# (2-(1-1)) = 2
# 
# Example 2:
# 
# Input: expression = "2*3-4*5"
# Output: [-34,-14,-10,-10,10]
# Explanation:
# (2*(3-(4*5))) = -34 
# ((2*3)-(4*5)) = -14 
# ((2*(3-4))*5) = -10 
# (2*((3-4)*5)) = -10 
# (((2*3)-4)*5) = 10
# 
# Constraints:
# 
# 	1 <= expression.length <= 20
# 	expression consists of digits and the operator '+', '-', and '*'.
#####################################################################################################

from typing import List


class Solution:
    def diffWaysToCompute(self, expression: str) -> List[int]:

        def split(s: str) -> List[str]:  # O(n) time
            stack = []
            for i in range(len(s)):
                if stack and stack[-1].isnumeric() and s[i].isnumeric():
                    stack[-1] += s[i]
                else:
                    stack.append(s[i])
            return stack

        def backtrack(parts: List[str]):  # O(2^n) time
            if len(parts) == 1:
                solutions[parts[0]] = eval(parts[0])
            for j in range(1, len(parts), 2):
                backtrack(parts[:j - 1] + ['(' + ''.join(parts[j - 1: j + 2]) + ')'] + parts[j + 2:])

        solutions = {}  # backtracking gives duplicated solutions
        backtrack(split(expression))
        return list(solutions.values())

    def diffWaysToCompute(self, expression: str) -> List[int]:

        operators = {'+': int.__add__, '-': int.__sub__, '*': int.__mul__}

        def tokenize(infix: str):  # O(n)
            tokens, n = [], ''
            for c in infix:
                if c.isdigit():
                    n += c
                else:
                    tokens.extend([int(n), operators[c]])
                    n = ''
            return tokens + [int(n)]

        def dfs(tokens):  # O(2^n) time
            result = []  # for i in range(1, len(tokens), 2):
            for i, operator in filter(lambda x: callable(x[1]), enumerate(tokens)):
                for left in dfs(tokens[:i]):
                    for right in dfs(tokens[i + 1:]):
                        result.append(operator(left, right))
            return result or [tokens[0]]

        return dfs(tokenize(expression))

    def diffWaysToCompute(self, expression: str) -> List[int]:
        import re
        o = {'+': int.__add__, '-': int.__sub__, '*': int.__mul__}
        f = lambda t: [t[i](l, r) for i in range(1, len(t), 2) for l in f(t[:i]) for r in f(t[i + 1:])] or [t[0]]
        return f([o[v] if i % 2 else int(v) for i, v in enumerate(re.split(r'(\+|\*|\-)', expression))])

    # def diffWaysToCompute(self, expression: str) -> List[int]:
    #     return [a + b if c == '+' else a - b if c == '-' else a * b
    #             for i, c in enumerate(expression) if c in '+-*'
    #             for a in self.diffWaysToCompute(expression[:i])
    #             for b in self.diffWaysToCompute(expression[i+1:])] or [int(expression)]


def test():
    from collections import Counter
    arguments = [
        "2-1-1",
        "2*3-4*5",
        "23-1*2",
        ]
    expectations = [
        [0, 2],
        [-34, -14, -10, -10, 10],
        [44, 21],
        ]
    for expression, expected in zip(arguments, expectations):
        solution = Solution().diffWaysToCompute(expression)
        assert Counter(solution) == Counter(expected)
test()