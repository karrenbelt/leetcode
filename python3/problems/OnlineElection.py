### Source : https://leetcode.com/problems/online-election/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# In an election, the i-th vote was cast for persons[i] at time times[i].
# 
# Now, we would like to implement the following query function: TopVotedCandidate.q(int t) will 
# return the number of the person that was leading the election at time t.  
# 
# Votes cast at time t will count towards our query.  In the case of a tie, the most recent vote 
# (among tied candidates) wins.
# 
# Example 1:
# 
# Input: ["TopVotedCandidate","q","q","q","q","q","q"], 
# [[[0,1,1,0,0,1,0],[0,5,10,15,20,25,30]],[3],[12],[25],[15],[24],[8]]
# Output: [null,0,1,1,0,0,1]
# Explanation: 
# At time 3, the votes are [0], and 0 is leading.
# At time 12, the votes are [0,1,1], and 1 is leading.
# At time 25, the votes are [0,1,1,0,0,1], and 1 is leading (as ties go to the most recent vote.)
# This continues for 3 more queries at time 15, 24, and 8.
# 
# Note:
# 
# 	1 <= persons.length = times.length <= 5000
# 	0 <= persons[i] <= persons.length
# 	times is a strictly increasing array with all elements in [0, 109].
# 	TopVotedCandidate.q is called at most 10000 times per test case.
# 	TopVotedCandidate.q(int t) is always called with t >= times[0].
# 
#####################################################################################################

from typing import List


class TopVotedCandidate:

    def __init__(self, persons: List[int], times: List[int]):
        self.lead = []
        self.times = times

        lead = -1
        count = {k: 0 for k in set(persons)}
        count[lead] = 0

        for p, t in zip(persons, times):
            count[p] += 1
            if count[lead] <= count[p]:
                lead = p
            self.lead.append(lead)

    def q(self, t: int) -> int:

        left, right = 0, len(self.lead) - 1
        while left + 1 < right:
            mid = (left + right) // 2
            if self.times[mid] == t:
                return self.lead[mid]
            elif self.times[mid] > t:
                right = mid
            else:
                left = mid

        if self.times[right] <= t:
            return self.lead[right]
        return self.lead[left]


class TopVotedCandidate:
    def __init__(self, persons: List[int], times: List[int]):
        self.leads, self.times, count = [], times, {}
        lead = -1
        for p in persons:
            count[p] = count.get(p, 0) + 1
            lead = p if count[p] >= count.get(lead, 0) else lead
            self.leads.append(lead)

    def q(self, t: int) -> int:
        import bisect
        return self.leads[bisect.bisect_right(self.times, t) - 1]

# Your TopVotedCandidate object will be instantiated and called as such:
# obj = TopVotedCandidate(persons, times)
# param_1 = obj.q(t)


def test():
    operations = ["TopVotedCandidate", "q", "q", "q", "q", "q", "q"]
    arguments = [[[0, 1, 1, 0, 0, 1, 0], [0, 5, 10, 15, 20, 25, 30]], [3], [12], [25], [15], [24], [8]]
    expectations = [None, 0, 1, 1, 0, 0, 1]
    obj = TopVotedCandidate(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
