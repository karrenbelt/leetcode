### Source : https://leetcode.com/problems/maximum-number-of-balloons/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-13

##################################################################################################### 
#
# Given a string text, you want to use the characters of text to form as many instances of the word 
# "balloon" as possible.
# 
# You can use each character in text at most once. Return the maximum number of instances that can be 
# formed.
# 
# Example 1:
# 
# Input: text = "nlaebolko"
# Output: 1
# 
# Example 2:
# 
# Input: text = "loonbalxballpoon"
# Output: 2
# 
# Example 3:
# 
# Input: text = "leetcode"
# Output: 0
# 
# Constraints:
# 
# 	1 <= text.length <= 104
# 	text consists of lower case English letters only.
#####################################################################################################


class Solution:
    def maxNumberOfBalloons(self, text: str) -> int:
        from collections import Counter
        balloon = Counter('balloon')
        chars = Counter(text)
        ctr = 0
        while not balloon - chars:
            chars -= balloon
            ctr += 1
        return ctr

    def maxNumberOfBalloons(self, text: str) -> int:
        from collections import Counter
        counts = Counter(text)
        return min(counts['b'], counts['a'], counts['l'] // 2, counts['o'] // 2, counts['n'])

    def maxNumberOfBalloons(self, t: str) -> int:
        return min(t.count(c) // 'balloon'.count(c) for c in 'balon')


def test():
    arguments = [
        "nlaebolko",
        "loonbalxballpoon",
        "leetcode",
    ]
    expectations = [1, 2, 0]
    for text, expected in zip(arguments, expectations):
        solution = Solution().maxNumberOfBalloons(text)
        assert solution == expected
