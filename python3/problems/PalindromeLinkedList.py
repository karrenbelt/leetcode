### Source : https://leetcode.com/problems/palindrome-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-05

##################################################################################################### 
#
# Given the head of a singly linked list, return true if it is a palindrome.
# 
# Example 1:
# 
# Input: head = [1,2,2,1]
# Output: true
# 
# Example 2:
# 
# Input: head = [1,2]
# Output: false
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [1, 105].
# 	0 <= Node.val <= 9
# 
# Follow up: Could you do it in O(n) time and O(1) space?
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def isPalindrome(self, head: ListNode) -> bool:  # simplest, not O(1) space
        sequence = []
        node = head
        while node:
            sequence.append(node.val)
            node = node.next
        for i in range(len(sequence) // 2):
            if sequence[i] != sequence[-i - 1]:
                return False
        return True

    def isPalindrome(self, head: ListNode) -> bool:  # O(1) space: find middle, reverse half, compare
        slow = fast = head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
        prev = None
        while slow:
            slow_next, slow.next = slow.next, prev
            prev, slow = slow, slow_next
        while prev:
            if prev.val != head.val:
                return False
            head, prev = head.next, prev.next
        return True


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [1, 2, 2, 1],
        [1, 2],
        ]
    expectations = [True, False]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().isPalindrome(SinglyLinkedList(nums).head)
        assert solution == expected
