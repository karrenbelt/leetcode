### Source : https://leetcode.com/problems/palindromic-substrings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-28

##################################################################################################### 
#
# Given a string, your task is to count how many palindromic substrings in this string.
# 
# The substrings with different start indexes or end indexes are counted as different substrings even 
# they consist of same characters.
# 
# Example 1:
# 
# Input: "abc"
# Output: 3
# Explanation: Three palindromic strings: "a", "b", "c".
# 
# Example 2:
# 
# Input: "aaa"
# Output: 6
# Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
# 
# Note:
# 
# 	The input string length won't exceed 1000.
# 
#####################################################################################################


class Solution:
    def countSubstrings(self, s: str) -> int:  # O(n^2) time and O(1) space
        ans = 0
        for center in range(2 * len(s) - 1):
            left = center // 2
            right = left + center % 2
            while left >= 0 and right < len(s) and s[left] == s[right]:
                ans += 1
                left -= 1
                right += 1
        return ans

    def countSubstrings(self, s):  # O(n^3) time
        return sum(s[i:j] == s[i:j][::-1] for j in range(len(s) + 1) for i in range(j))

    def countSubstrings(self, s: str) -> int:  # O(n) time and O(n) space

        def manachers(S):
            A = '@#' + '#'.join(S) + '#$'
            Z = [0] * len(A)
            center = right = 0
            for i in range(1, len(A) - 1):
                if i < right:
                    Z[i] = min(right - i, Z[2 * center - i])
                while A[i + Z[i] + 1] == A[i - Z[i] - 1]:
                    Z[i] += 1
                if i + Z[i] > right:
                    center, right = i, i + Z[i]
            return Z

        return sum((v + 1) // 2 for v in manachers(s))

    def countSubstrings(self, s: str) -> int:  # two pointer: O(n) time
        count = 0
        i = 0
        while i < len(s):
            right = left = i
            count += 1
            delta = 2
            # Move right pointer to the last duplicate of value, s[i]
            while right < len(s) - 1 and s[right] == s[right + 1]:
                right += 1
                count += delta
                delta += 1
            # Set the next starting point
            i = right + 1
            # Expand outwards to find longer palindrome
            while right < len(s) - 1 and left > 0 and s[right + 1] == s[left - 1]:
                right += 1
                left -= 1
                count += 1
        return count


def test():
    arguments = ["abc", "aaa", "aabaa"]
    expectations = [3, 6, 9]
    for s, expected in zip(arguments, expectations):
        solution = Solution().countSubstrings(s)
        assert solution == expected
