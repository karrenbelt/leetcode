### Source : https://leetcode.com/problems/valid-anagram/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-11

##################################################################################################### 
#
# Given two strings s and t , write a function to determine if t is an anagram of s.
# 
# Example 1:
# 
# Input: s = "anagram", t = "nagaram"
# Output: true
# 
# Example 2:
# 
# Input: s = "rat", t = "car"
# Output: false
# 
# Note:
# You may assume the string contains only lowercase alphabets.
# 
# Follow up:
# What if the inputs contain unicode characters? How would you adapt your solution to such case?
#####################################################################################################


class Solution:
    def isAnagram(self, s: str, t: str) -> bool:

        def count(arr):
            d = {}
            for elem in arr:
                d[elem] = d[elem] + 1 if elem in d else 1
            return d

        if len(s) != len(t) or set(s) != set(t):
            return False
        return count(s) == count(t)

    def isAnagram(self, s: str, t: str) -> bool:
        return all(s.count(c) == t.count(c) for c in set(s).union(t))

    def isAnagram(self, s: str, t: str) -> bool:
        from collections import Counter
        return Counter(s) == Counter(t)


def test():
    string_pairs = [
        ("anagram", "nagaram"),
        ("rat", "car"),
        ]
    expectations = [True, False]
    for (s, t), expected in zip(string_pairs, expectations):
        solution = Solution().isAnagram(s, t)
        assert solution == expected
