### Source : https://leetcode.com/problems/number-of-steps-to-reduce-a-number-in-binary-representation-to-one/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given a number s in their binary representation. Return the number of steps to reduce it to 1 under 
# the following rules:
# 
# 	If the current number is even, you have to divide it by 2.
# 
# 	If the current number is odd, you have to add 1 to it.
# 
# It's guaranteed that you can always reach to one for all testcases.
# 
# Example 1:
# 
# Input: s = "1101"
# Output: 6
# Explanation: "1101" corressponds to number 13 in their decimal representation.
# Step 1) 13 is odd, add 1 and obtain 14. 
# Step 2) 14 is even, divide by 2 and obtain 7.
# Step 3) 7 is odd, add 1 and obtain 8.
# Step 4) 8 is even, divide by 2 and obtain 4.  
# Step 5) 4 is even, divide by 2 and obtain 2. 
# Step 6) 2 is even, divide by 2 and obtain 1.  
# 
# Example 2:
# 
# Input: s = "10"
# Output: 1
# Explanation: "10" corressponds to number 2 in their decimal representation.
# Step 1) 2 is even, divide by 2 and obtain 1.  
# 
# Example 3:
# 
# Input: s = "1"
# Output: 0
# 
# Constraints:
# 
# 	1 <= s.length <= 500
# 	s consists of characters '0' or '1'
# 	s[0] == '1'
#####################################################################################################


class Solution:
    def numSteps(self, s: str) -> int:
        from collections import deque
        queue = deque(s)
        ctr = 0
        while len(queue) > 1:
            if queue[-1] == '0':  # even, right shift: '110' -> '11'
                queue.pop()
            else:
                for i in range(len(queue) - 1, -1, -1):
                    if queue[i] == '0':
                        queue[i] = '1'
                        queue[-1] = '0'
                        break
                else:  # if we didn't find a zero, e.g. '111' -> '1000'
                    queue = deque('1' + '0' * len(queue))
            ctr += 1
        return ctr

    def numSteps(self, s: str) -> int:  # O(s) time and O(1) space
        ctr = 0
        n = int(s, 2)
        while n != 1:
            if n % 2 == 0:
                n >>= 1
            else:
                n += 1
            ctr += 1
        return ctr


def test():
    arguments = ["1101", "10", "1"]
    expectations = [6, 1, 0]
    for s, expected in zip(arguments, expectations):
        solution = Solution().numSteps(s)
        assert solution == expected
