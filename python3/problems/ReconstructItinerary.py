### Source : https://leetcode.com/problems/reconstruct-itinerary/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-14

##################################################################################################### 
#
# You are given a list of airline tickets where tickets[i] = [fromi, toi] represent the departure and 
# the arrival airports of one flight. Reconstruct the itinerary in order and return it.
# 
# All of the tickets belong to a man who departs from "JFK", thus, the itinerary must begin with 
# "JFK". If there are multiple valid itineraries, you should return the itinerary that has the 
# smallest lexical order when read as a single string.
# 
# 	For example, the itinerary ["JFK", "LGA"] has a smaller lexical order than ["JFK", "LGB"].
# 
# You may assume all tickets form at least one valid itinerary. You must use all the tickets once and 
# only once.
# 
# Example 1:
# 
# Input: tickets = [["UC","LHR"],["JFK","UC"],["SFO","SJC"],["LHR","SFO"]]
# Output: ["JFK","UC","LHR","SFO","SJC"]
# 
# Example 2:
# 
# Input: tickets = [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
# Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
# Explanation: Another possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"] but it is 
# larger in lexical order.
# 
# Constraints:
# 
# 	1 <= tickets.length <= 300
# 	tickets[i].length == 2
# 	fromi.length == 3
# 	toi.length == 3
# 	fromi and toi consist of uppercase English letters.
# 	fromi != toi
#####################################################################################################

from typing import List


class Solution:
    def findItinerary(self, tickets: List[List[str]]) -> List[str]:
        # all tickets (edges) need to be visited, not all nodes. We use topological sorting
        graph = {}
        for departure, arrival in sorted(tickets, reverse=True):
            graph.setdefault(departure, list()).append(arrival)

        path, stack = [], ["JFK"]
        while stack:
            while graph.get(stack[-1], []):
                stack.append(graph[stack[-1]].pop())
            path.append(stack.pop())
        return path[::-1]

    def findItinerary(self, tickets: List[List[str]]) -> List[str]:

        def dfs(airport: str):
            while graph.get(airport, []):
                dfs(graph[airport].pop())
            path.append(airport)

        graph, path = {}, []
        for departure, arrival in sorted(tickets, reverse=True):
            graph.setdefault(departure, list()).append(arrival)

        return dfs("JFK") or path[::-1]


def test():
    arguments = [
        [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]],
        [["JFK", "SFO"], ["JFK", "ATL"], ["SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
    ]
    expectations = [
        ["JFK", "MUC", "LHR", "SFO", "SJC"],
        ["JFK", "ATL", "JFK", "SFO", "ATL", "SFO"],
    ]
    for tickets, expected in zip(arguments, expectations):
        solution = Solution().findItinerary(tickets)
        assert solution == expected, (expected, solution)
