### Source : https://leetcode.com/problems/find-k-closest-elements/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# Given a sorted integer array arr, two integers k and x, return the k closest integers to x in the 
# array. The result should also be sorted in ascending order.
# 
# An integer a is closer to x than an integer b if:
# 
# 	|a - x| < |b - x|, or
# 	|a - x| == |b - x| and a < b
# 
# Example 1:
# Input: arr = [1,2,3,4,5], k = 4, x = 3
# Output: [1,2,3,4]
# Example 2:
# Input: arr = [1,2,3,4,5], k = 4, x = -1
# Output: [1,2,3,4]
# 
# Constraints:
# 
# 	1 <= k <= arr.length
# 	1 <= arr.length <= 104
# 	arr is sorted in ascending order.
# 	-104 <= arr[i], x <= 104
#####################################################################################################

from typing import List


class Solution:

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(n log(n) + k log(k)), O(k + n)
        return sorted(sorted(arr, key=lambda n: abs(x - n))[:k])

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # sliding window
        k_closest = arr[:k]
        differences = [abs(number - x) for number in arr]  # O(n)
        target_sum = sum(differences[:k])  # O(k)
        i = 0
        while i + k < len(arr):  # O(n - k)
            prev_sum = target_sum
            target_sum -= differences[i]
            target_sum += differences[i + k]
            if target_sum < prev_sum:
                k_closest = arr[i + 1: i + k + 1]
            i += 1
        return k_closest

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(log(n) + k), O(k)

        left, right = 0, len(arr) - 1
        while left <= right:
            mid = (left + right) // 2
            if arr[mid] < x:
                left = mid + 1
            else:
                right = mid - 1

        right = left
        while right - left < k:
            if left == 0:
                return arr[:k]
            if right == len(arr):
                return arr[-k:]
            if x - arr[left - 1] <= arr[right] - x:
                left -= 1
            else:
                right += 1

        return arr[left:right]

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        import bisect
        idx = max(0, bisect.bisect(arr, x) - k)
        while idx + k < len(arr) and abs(x - arr[idx + k]) < abs(x - arr[idx]):
            idx += 1
        return arr[idx:idx + k]

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:  # O(log n) time and O(k) space
        left, right = 0, len(arr) - k
        while left < right:
            mid = left + (right - left) // 2
            if x - arr[mid] > arr[mid + k] - x:
                left = mid + 1
            else:
                right = mid
        return arr[left:left + k]


def test():
    arguments = [
        ([1, 2, 3, 4, 5], 4, 3),
        ([1, 2, 3, 4, 5], 4, -1),
        ]
    expectations = [
        [1, 2, 3, 4],
        [1, 2, 3, 4],
        ]
    for (arr, k, x), expected in zip(arguments, expectations):
        solution = Solution().findClosestElements(arr, k, x)
        assert solution == expected
