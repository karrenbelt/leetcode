### Source : https://leetcode.com/problems/median-of-two-sorted-arrays/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-14

##################################################################################################### 
#
# Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two 
# sorted arrays.
# 
# Follow up: The overall run time complexity should be O(log (m+n)).
# 
# Example 1:
# 
# Input: nums1 = [1,3], nums2 = [2]
# Output: 2.00000
# Explanation: merged array = [1,2,3] and median is 2.
# 
# Example 2:
# 
# Input: nums1 = [1,2], nums2 = [3,4]
# Output: 2.50000
# Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
# 
# Example 3:
# 
# Input: nums1 = [0,0], nums2 = [0,0]
# Output: 0.00000
# 
# Example 4:
# 
# Input: nums1 = [], nums2 = [1]
# Output: 1.00000
# 
# Example 5:
# 
# Input: nums1 = [2], nums2 = []
# Output: 2.00000
# 
# Constraints:
# 
# 	nums1.length == m
# 	nums2.length == n
# 	0 <= m <= 1000
# 	0 <= n <= 1000
# 	1 <= m + n <= 2000
# 	-106 <= nums1[i], nums2[i] <= 106
#####################################################################################################

from typing import List


class Solution:

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(m+n log m+n)
        nums1.extend(nums2)
        nums1.sort()
        return (nums1[len(nums1)//2] + nums1[~len(nums1)//2]) / 2

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(m+n)
        n1, n2 = len(nums1), len(nums2)
        n = n1 + n2
        end = n // 2
        i = i1 = i2 = current = previous = 0
        # implicitly build half of the sorted merge list, but only save last values
        while i <= end:
            previous = current
            if i1 == n1:  # first list is exhausted ==> choose from second list
                current = nums2[i2]
                i2 += 1
            elif i2 == n2:  # second list is exhausted ==> choose from first list
                current = nums1[i1]
                i1 += 1
            elif nums1[i1] < nums2[i2]:
                current = nums1[i1]
                i1 += 1
            else:
                current = nums2[i2]
                i2 += 1
            i += 1

        if n % 2:
            return current
        return (previous + current) / 2

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:  # O(log m+n)

        def kth(a, b, k):
            if len(a) > len(b):
                a, b = b, a
            if not a:
                return b[k]
            if k == len(a) + len(b) - 1:
                return max(a[-1], b[-1])
            i = len(a) // 2
            j = k - i
            if a[i] > b[j]:
                return kth(a[:i], b[j:], i)
            return kth(a[i:], b[:j], j)

        length = len(nums1) + len(nums2)
        if length % 2 == 1:
            return kth(nums1, nums2, length // 2)
        return (kth(nums1, nums2, length // 2) + kth(nums1, nums2, length // 2 - 1)) / 2

    def findMedianSortedArrays(self, nums1, nums2):  # log(m + n)
        a, b = sorted((nums1, nums2), key=len)
        m, n = len(a), len(b)
        after = (m + n - 1) // 2
        left, right = 0, m
        while left < right:
            mid = left + (right - left) // 2
            if after - mid - 1 < 0 or a[mid] >= b[after - mid - 1]:
                right = mid
            else:
                left = mid + 1
        mid = left
        nextfew = sorted(a[mid:mid + 2] + b[after - mid:after - mid + 2])
        return (nextfew[0] + nextfew[1 - (m + n) % 2]) / 2.0


def test():
    numbers = [
        ([1, 3], [2]),
        ([1, 2], [3, 4]),
        ([0, 0], [0, 0]),
        ([], [1]),
        ([2], []),
        ]
    expectations = [2.0, 2.5, 0.0, 1.0, 2.0]
    for (nums1, nums2), expected in zip(numbers, expectations):
        solution = Solution().findMedianSortedArrays(nums1, nums2)
        assert solution == expected
