### Source : https://leetcode.com/problems/redundant-connection/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-25

##################################################################################################### 
#
# In this problem, a tree is an undirected graph that is connected and has no cycles.
# 
# You are given a graph that started as a tree with n nodes labeled from 1 to n, with one additional 
# edge added. The added edge has two different vertices chosen from 1 to n, and was not an edge that 
# already existed. The graph is represented as an array edges of length n where edges[i] = [ai, bi] 
# indicates that there is an edge between nodes ai and bi in the graph.
# 
# Return an edge that can be removed so that the resulting graph is a tree of n nodes. If there are 
# multiple answers, return the answer that occurs last in the input.
# 
# Example 1:
# 
# Input: edges = [[1,2],[1,3],[2,3]]
# Output: [2,3]
# 
# Example 2:
# 
# Input: edges = [[1,2],[2,3],[3,4],[1,4],[1,5]]
# Output: [1,4]
# 
# Constraints:
# 
# 	n == edges.length
# 	3 <= n <= 1000
# 	edges[i].length == 2
# 	1 <= ai < bi <= edges.length
# 	ai != bi
# 	There are no repeated edges.
# 	The given graph is connected.
#####################################################################################################

from typing import List


class Solution:
    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:
        # it's a tree with one additional edge, which hence creates a cycle
        # if we can already reach v from u before adding the edge, we found the additional edge added
        def dfs(x: int, y: int, seen: set) -> bool:
            return x not in seen and (x == y or any(dfs(node, y, seen | {x}) for node in graph[x]))

        graph = {}
        for u, v in edges:
            if u in graph and v in graph and dfs(u, v, set()):
                return [u, v]
            graph.setdefault(u, set()).add(v)
            graph.setdefault(v, set()).add(u)

    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:

        def dfs(node: int) -> int:  # union find
            if tree[node] != node:
                tree[node] = dfs(tree[node])
            return tree[node]

        tree = list(range(1001))
        for u, v in edges:
            if dfs(u) == dfs(v):
                return [u, v]
            tree[dfs(u)] = dfs(v)

    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:
        tree = ''.join(map(chr, range(1001)))
        for u, v in edges:
            if tree[u] == tree[v]:
                return [u, v]
            tree = tree.replace(tree[u], tree[v])


def test():
    arguments = [
        [[1, 2], [1, 3], [2, 3]],
        [[1, 2], [2, 3], [3, 4], [1, 4], [1, 5]],
    ]
    expectations = [
        [2, 3],
        [1, 4],
    ]
    for edges, expected in zip(arguments, expectations):
        solution = Solution().findRedundantConnection(edges)
        assert solution == expected, solution
