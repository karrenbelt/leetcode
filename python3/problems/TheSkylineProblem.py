### Source : https://leetcode.com/problems/the-skyline-problem/
### Author : M.A.P. Karrenbelt
### Date   : 2021-01-06

##################################################################################################### 
#
# A city's skyline is the outer contour of the silhouette formed by all the buildings in that city 
# when viewed from a distance. Given the locations and heights of all the buildings, return the 
# skyline formed by these buildings collectively.
# 
# The geometric information of each building is given in the array buildings where buildings[i] = 
# [lefti, righti, heighti]:
# 
# 	lefti is the x coordinate of the left edge of the ith building.
# 	righti is the x coordinate of the right edge of the ith building.
# 	heighti is the height of the ith building.
# 
# You may assume all buildings are perfect rectangles grounded on an absolutely flat surface at 
# height 0.
# 
# The skyline should be represented as a list of "key points" sorted by their x-coordinate in the 
# form [[x1,y1],[x2,y2],...]. Each key point is the left endpoint of some horizontal segment in the 
# skyline except the last point in the list, which always has a y-coordinate 0 and is used to mark 
# the skyline's termination where the rightmost building ends. Any ground between the leftmost and 
# rightmost buildings should be part of the skyline's contour.
# 
# Note: There must be no consecutive horizontal lines of equal height in the output skyline. For 
# instance, [...,[2 3],[4 5],[7 5],[11 5],[12 7],...] is not acceptable; the three lines of height 5 
# should be merged into one in the final output as such: [...,[2 3],[4 5],[12 7],...]
# 
# Example 1:
# 
# Input: buildings = [[2,9,10],[3,7,15],[5,12,12],[15,20,10],[19,24,8]]
# Output: [[2,10],[3,15],[7,12],[12,0],[15,10],[20,8],[24,0]]
# Explanation:
# Figure A shows the buildings of the input.
# Figure B shows the skyline formed by those buildings. The red points in figure B represent the key 
# points in the output list.
# 
# Example 2:
# 
# Input: buildings = [[0,2,3],[2,5,3]]
# Output: [[0,3],[5,0]]
# 
# Constraints:
# 
# 	1 <= buildings.length <= 104
# 	0 <= lefti < righti <= 231 - 1
# 	1 <= heighti <= 231 - 1
# 	buildings is sorted by lefti in non-decreasing order.
#####################################################################################################

from typing import List


class Solution:

    def getSkyline(self, buildings: List[List[int]]) -> List[List[int]]:  # divide and conquer
        if not buildings:
            return []
        if len(buildings) == 1:
            return [[buildings[0][0], buildings[0][2]], [buildings[0][1], 0]]
        mid = len(buildings) // 2
        left = self.getSkyline(buildings[:mid])
        right = self.getSkyline(buildings[mid:])
        return self.merge(left, right)

    def merge(self, left, right):
        h1, h2, ans = 0, 0, []
        while left and right:
            if left[0][0] < right[0][0]:
                pos, h1 = left[0]
                left = left[1:]
            elif left[0][0] > right[0][0]:
                pos, h2 = right[0]
                right = right[1:]
            else:
                pos, h1 = left[0]
                h2 = right[0][1]
                left, right = left[1:], right[1:]
            h = max(h1, h2)
            if not ans or h != ans[-1][1]:
                ans.append([pos, h])
        if left:
            ans += left
        if right:
            ans += right
        return ans

    def getSkyline(self, buildings: List[List[int]]) -> List[List[int]]:  # O(n log n)
        import heapq
        ans = [[-1, 0]]
        heap = []  # store overlapping buildings - ordered by negative height (min heap)
        corners = sorted(set(sum([[b[0], b[1]] for b in buildings], [])))
        i = 0
        for c in corners:
            while heap and heap[0][1] <= c:  # if we passed it, or it is the current corner, we remove it
                heapq.heappop(heap)
            while i < len(buildings) and buildings[i][0] <= c:  # add those that start before the current corner
                heapq.heappush(heap, (-buildings[i][2], buildings[i][1]))
                i += 1
            h = -heap[0][0] if heap else 0
            if ans[-1][1] != h:  # only add if it's not the same height sa before
                ans.append([c, h])
        return ans[1:]

    def getSkyline(self, buildings):
        import heapq
        events = sorted([(L, -H, R) for L, R, H in buildings] + list(set((R, 0, None) for L, R, H in buildings)))
        res, hp = [[0, 0]], [(0, float("inf"))]
        for x, negH, R in events:
            while x >= hp[0][1]:
                heapq.heappop(hp)
            if negH:
                heapq.heappush(hp, (negH, R))
            if res[-1][1] + hp[0][0]:
                res += [x, -hp[0][0]],
        return res[1:]


def test():
    matrices = [
        [[2, 9, 10], [3, 7, 15], [5, 12, 12], [15, 20, 10], [19, 24, 8]],
        [[0, 2, 3], [2, 5, 3]],
        ]
    expectations = [
        [[2, 10], [3, 15], [7, 12], [12, 0], [15, 10], [20, 8], [24, 0]],
        [[0, 3], [5, 0]],
        ]
    for buildings, expected in zip(matrices, expectations):
        solution = Solution().getSkyline(buildings)
        assert solution == expected
