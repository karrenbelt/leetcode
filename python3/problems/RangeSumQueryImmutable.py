### Source : https://leetcode.com/problems/range-sum-query-immutable/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given an integer array nums, find the sum of the elements between indices left and right inclusive, 
# where (left <= right).
# 
# Implement the NumArray class:
# 
# 	NumArray(int[] nums) initializes the object with the integer array nums.
# 	int sumRange(int left, int right) returns the sum of the elements of the nums array in the 
# range [left, right] inclusive (i.e., sum(nums[left], nums[left + 1], ... , nums[right])).
# 
# Example 1:
# 
# Input
# ["NumArray", "sumRange", "sumRange", "sumRange"]
# [[[-2, 0, 3, -5, 2, -1]], [0, 2], [2, 5], [0, 5]]
# Output
# [null, 1, -1, -3]
# 
# Explanation
# NumArray numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
# numArray.sumRange(0, 2); // return 1 ((-2) + 0 + 3)
# numArray.sumRange(2, 5); // return -1 (3 + (-5) + 2 + (-1)) 
# numArray.sumRange(0, 5); // return -3 ((-2) + 0 + 3 + (-5) + 2 + (-1))
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-105 <= nums[i] <= 105
# 	0 <= left <= right < nums.length
# 	At most 104 calls will be made to sumRange.
#####################################################################################################

from typing import List


class NumArray:
    def __init__(self, nums: List[int]):
        self.nums = nums

    def sumRange(self, left: int, right: int) -> int:  # O(n) time
        return sum(self.nums[left: right + 1])


class NumArray:
    def __init__(self, nums: List[int]):
        import math
        self.sqrt_n = math.ceil(len(nums) ** 0.5)
        self.matrix = []
        self.row_sums = []
        for i in range(0, len(nums), self.sqrt_n):
            row = nums[i: i + self.sqrt_n]
            self.matrix.append(row)
            self.row_sums.append(sum(row))

    def sumRange(self, left: int, right: int) -> int:  # O(sqrt(n)) time
        s = 0
        left_row, left_i = divmod(left, self.sqrt_n)
        right_row, right_i = divmod(right, self.sqrt_n)
        right_i += 1
        if right_row == left_row:
            s += sum(self.matrix[left_row][left_i: right_i])
        else:
            s += sum(self.matrix[left_row][left_i:])
            s += sum(self.row_sums[left_row + 1: right_row])
            s += sum(self.matrix[right_row][:right_i])
        return s


class NumArray:
    def __init__(self, nums: List[int]):
        self.prefix = [0]
        for n in nums:
            self.prefix.append(self.prefix[-1] + n)

    def sumRange(self, left: int, right: int) -> int:  # O(1) time
        return self.prefix[right + 1] - self.prefix[left]

# Your NumArray object will be instantiated and called as such:
# obj = NumArray(nums)
# param_1 = obj.sumRange(left,right)


def test():
    null = None
    operations = ["NumArray", "sumRange", "sumRange", "sumRange"]
    arguments = [[-2, 0, 3, -5, 2, -1], [0, 2], [2, 5], [0, 5]]
    expectations = [null, 1, -1, -3]
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        obj = NumArray(arguments[0])
        obj.sumRange(*args)
