### Source : https://leetcode.com/problems/move-zeroes/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an array nums, write a function to move all 0's to the end of it while maintaining the 
# relative order of the non-zero elements.
# 
# Example:
# 
# Input: [0,1,0,3,12]
# Output: [1,3,12,0,0]
# 
# Note:
# 
# 	You must do this in-place without making a copy of the array.
# 	inimize the total number of operations.
#####################################################################################################

from typing import List


class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        right = 0
        left = len(nums) - 1
        while right <= left:
            if nums[right] == 0:
                nums.append(nums.pop(right))
                left -= 1
            else:
                right += 1


def test():
    arrays_of_numbers = [
        [0, 1, 0, 3, 12],
    ]
    expectations = [
        [1, 3, 12, 0, 0],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        Solution().moveZeroes(nums)
        assert nums == expected
