### Source : https://leetcode.com/problems/max-area-of-island/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-04

##################################################################################################### 
#
# Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) 
# connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are 
# surrounded by water.
# 
# Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area 
# is 0.)
# 
# Example 1:
# 
# [[0,0,1,0,0,0,0,1,0,0,0,0,0],
#  [0,0,0,0,0,0,0,1,1,1,0,0,0],
#  [0,1,1,0,1,0,0,0,0,0,0,0,0],
#  [0,1,0,0,1,1,0,0,1,0,1,0,0],
#  [0,1,0,0,1,1,0,0,1,1,1,0,0],
#  [0,0,0,0,0,0,0,0,0,0,1,0,0],
#  [0,0,0,0,0,0,0,1,1,1,0,0,0],
#  [0,0,0,0,0,0,0,1,1,0,0,0,0]]
# 
# Given the above grid, return 6. Note the answer is not 11, because the island must be connected 
# 4-directionally.
# 
# Example 2:
# 
# [[0,0,0,0,0,0,0,0]]
# Given the above grid, return 0.
# 
# Note: The length of each dimension in the given grid does not exceed 50.
#####################################################################################################

from typing import List


class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:  # O(n * m) time and O(1) space

        def dfs(i: int, j: int) -> int:
            if not -1 < i < len(grid) or not -1 < j < len(grid[0]) or grid[i][j] == 0:
                return 0
            grid[i][j] = 0
            return sum(dfs(i + y, j + x) for y, x in directions) + 1

        max_area = 0
        directions = [(-1, 0), (0, -1), (1, 0), (0, 1)]
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 1:
                    max_area = max(max_area, dfs(i, j))
        return max_area

    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:

        def dfs(x: int, y: int):
            if not 0 <= x < len(grid) or not 0 <= y < len(grid[0]) or grid[x][y] == 0:
                return 0
            grid[x][y] = 0
            return 1 + sum(dfs(x + a, y + b) for a, b in [(0, 1), (1, 0), (0, -1), (-1, 0)])

        return max(dfs(i, j) for i in range(len(grid)) for j in range(len(grid[0])))

    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:  # bfs
        maximum = 0
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j]:
                    queue, area = {(i, j)}, 0
                    while queue:
                        r, c = queue.pop()
                        grid[r][c] = 0
                        area += 1
                        for a, b in directions:
                            x, y = r + a, c + b
                            if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and grid[x][y]:
                                queue.add((x, y))
                    maximum = max(maximum, area)
        return maximum

    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        maximum = 0
        for node in ((i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j]):
            queue = [node]
            for i, j in queue:
                grid[i][j] = 0
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and grid[x][y]:
                        grid[x][y] = 0
                        queue.append((x, y))
            maximum = max(maximum, len(queue))
        return maximum

    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:

        def area(z):
            return grid.pop(z, 0) and 1 + sum(area(z + 1j ** k) for k in range(4))

        grid = {i + j * 1j: val for i, row in enumerate(grid) for j, val in enumerate(row)}
        return max(map(area, set(grid)))


def test():
    arguments = [
        [[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
         [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
         [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]],

        [[0, 0, 0, 0, 0, 0, 0, 0]],

        [[1, 1, 0, 0, 0], [1, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 1]],
        ]
    expectations = [6, 0, 4]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().maxAreaOfIsland(grid)
        assert solution == expected, (solution, expected)
