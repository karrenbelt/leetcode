### Source : https://leetcode.com/problems/set-matrix-zeroes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# Given an m x n matrix. If an element is 0, set its entire row and column to 0. Do it in-place.
# 
# Follow up:
# 
# 	A straight forward solution using O(mn) space is probably a bad idea.
# 	A simple improvement uses O(m + n) space, but still not the best solution.
# 	Could you devise a constant space solution?
# 
# Example 1:
# 
# Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
# Output: [[1,0,1],[0,0,0],[1,0,1]]
# 
# Example 2:
# 
# Input: matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
# Output: [[0,0,0,0],[0,4,5,0],[0,3,1,0]]
# 
# Constraints:
# 
# 	m == matrix.length
# 	n == matrix[0].length
# 	1 <= m, n <= 200
# 	-231 <= matrix[i][j] <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:  # O(n * m) time and O(n + m) space
        """
        Do not return anything, modify matrix in-place instead.
        """
        cols, rows = set(), set()
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == 0:
                    rows.add(i)
                    cols.add(j)
        for i in rows:
            for j in range(len(matrix[0])):
                matrix[i][j] = 0
        for j in cols:
            for i in range(len(matrix)):
                matrix[i][j] = 0

    def setZeroes(self, matrix: List[List[int]]) -> None:  # O(n * m) time and O(1) space
        # store information on the first element of the row / column
        # since row[0] and col[0] overlap, we introduce 1 extra variable
        col0 = 1
        for i in range(0, len(matrix)):
            if matrix[i][0] == 0:
                col0 = 0
            for j in range(1, len(matrix[0])):
                if matrix[i][j] == 0:
                    matrix[i][0] = matrix[0][j] = 0

        for i in range(len(matrix)-1, -1, -1):
            for j in range(1, len(matrix[0])):
                if matrix[i][0] == 0 or matrix[0][j] == 0:
                    matrix[i][j] = 0
            if col0 == 0:
                matrix[i][0] = 0


def test():
    arguments = [
        [[1, 1, 1], [1, 0, 1], [1, 1, 1]],
        [[0, 1, 2, 0], [3, 4, 5, 2], [1, 3, 1, 5]],
        [[1, 2, 3, 4], [5, 0, 7, 8], [0, 10, 11, 12], [13, 14, 15, 0]],
        ]
    expectations = [
        [[1, 0, 1], [0, 0, 0], [1, 0, 1]],
        [[0, 0, 0, 0], [0, 4, 5, 0], [0, 3, 1, 0]],
        [[0, 0, 3, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
        ]
    for matrix, expected in zip(arguments, expectations):
        Solution().setZeroes(matrix)
        assert matrix == expected
