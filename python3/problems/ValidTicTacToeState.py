### Source : https://leetcode.com/problems/valid-tic-tac-toe-state/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# Given a Tic-Tac-Toe board as a string array board, return true if and only if it is possible to 
# reach this board position during the course of a valid tic-tac-toe game.
# 
# The board is a 3 x 3 array that consists of characters ' ', 'X', and 'O'. The ' ' character 
# represents an empty square.
# 
# Here are the rules of Tic-Tac-Toe:
# 
# 	Players take turns placing characters into empty squares ' '.
# 	The first player always places 'X' characters, while the second player always places 'O' 
# characters.
# 	'X' and 'O' characters are always placed into empty squares, never filled ones.
# 	The game ends when there are three of the same (non-empty) character filling any row, 
# column, or diagonal.
# 	The game also ends if all squares are non-empty.
# 	No more moves can be played if the game is over.
# 
# Example 1:
# 
# Input: board = ["O  ","   ","   "]
# Output: false
# Explanation: The first player always plays "X".
# 
# Example 2:
# 
# Input: board = ["XOX"," X ","   "]
# Output: false
# Explanation: Players take turns making moves.
# 
# Example 3:
# 
# Input: board = ["XXX","   ","OOO"]
# Output: false
# 
# Example 4:
# 
# Input: board = ["XOX","O O","XOX"]
# Output: true
# 
# Constraints:
# 
# 	board.length == 3
# 	board[i].length == 3
# 	board[i][j] is either 'X', 'O', or ' '.
#####################################################################################################

from typing import List


class Solution:
    def validTicTacToe(self, board: List[str]) -> bool:

        def win(s: str) -> bool:
            if any(all(c == s for c in row) for row in board):
                return True
            if any(all(row[i] == s for row in board) for i in range(len(board))):
                return True
            if all(row[i] == s for i, row in enumerate(board)) or all(row[~i] == s for i, row in enumerate(board)):
                return True
            return False

        x, o = (sum(row.count(c) for row in board) for c in 'XO')
        if x - o not in (0, 1):
            return False
        if x > 2:
            if x == o and win('X') or x != o and win('O'):
                return False
        return True


def test():
    arguments = [
        ["O  ", "   ", "   "],
        ["XOX", " X ", "   "],
        ["XXX", "   ", "OOO"],
        ["XOX", "O O", "XOX"],
        ["XOX", "OOX", "XO "],
    ]
    expectations = [False, False, False, True, True]
    for board, expected in zip(arguments, expectations):
        solution = Solution().validTicTacToe(board)
        assert solution == expected
