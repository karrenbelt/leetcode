### Source : https://leetcode.com/problems/shuffle-the-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-08

##################################################################################################### 
#
# Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].
# 
# Return the array in the form [x1,y1,x2,y2,...,xn,yn].
# 
# Example 1:
# 
# Input: nums = [2,5,1,3,4,7], n = 3
# Output: [2,3,5,4,1,7] 
# Explanation: Since x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 then the answer is [2,3,5,4,1,7].
# 
# Example 2:
# 
# Input: nums = [1,2,3,4,4,3,2,1], n = 4
# Output: [1,4,2,3,3,2,4,1]
# 
# Example 3:
# 
# Input: nums = [1,1,2,2], n = 2
# Output: [1,2,1,2]
# 
# Constraints:
# 
# 	1 <= n <= 500
# 	nums.length == 2n
# 	1 <= nums[i] <= 103
#####################################################################################################

from typing import List


class Solution:
    def shuffle(self, nums: List[int], n: int) -> List[int]:
        result = []
        for i in range(n):
            result.append(nums[i])
            result.append(nums[n + i])
        return result

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 92 ms
        return [item for items in zip(nums[:n], nums[n:]) for item in items]

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 60 ms
        return list(sum(zip(nums[:n], nums[n:]), tuple()))

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 76 ms
        return [item for i in range(n) for item in (nums[i], nums[i + n])]

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 112 ms
        return (arr := []) or any(any((arr.append(nums[i]), arr.append(nums[n + i]))) for i in range(n)) or arr

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 72 ms
        from itertools import chain
        return list(chain(*zip(nums[:n], nums[n:])))

    def shuffle(self, nums: List[int], n: int) -> List[int]:  # O(n) time and O(n) space: 104 ms
        from itertools import chain
        return list(chain.from_iterable(zip(nums[:n], nums[n:])))

    def shuffle(self, nums: List[int], n: int) -> List[int]:
        return [x for i in range(n) for x in nums[i::n]]

def test():
    arguments = [
        ([2, 5, 1, 3, 4, 7], 3),
        ([1, 2, 3, 4, 4, 3, 2, 1], 4),
        ([1, 1, 2, 2], 2),
    ]
    expectations = [
        [2, 3, 5, 4, 1, 7],
        [1, 4, 2, 3, 3, 2, 4, 1],
        [1, 2, 1, 2],
    ]
    for (nums, n), expected in zip(arguments, expectations):
        solution = Solution().shuffle(nums, n)
        assert solution == expected
