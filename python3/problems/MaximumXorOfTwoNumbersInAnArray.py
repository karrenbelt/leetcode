### Source : https://leetcode.com/problems/maximum-xor-of-two-numbers-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-02

##################################################################################################### 
#
# Given an integer array nums, return the maximum result of nums[i] XOR nums[j], where 0 &le; i &le; 
# j < n.
# 
# Follow up: Could you do this in O(n) runtime?
# 
# Example 1:
# 
# Input: nums = [3,10,5,25,2,8]
# Output: 28
# Explanation: The maximum result is 5 XOR 25 = 28.
# 
# Example 2:
# 
# Input: nums = [0]
# Output: 0
# 
# Example 3:
# 
# Input: nums = [2,4]
# Output: 6
# 
# Example 4:
# 
# Input: nums = [8,10,2]
# Output: 10
# 
# Example 5:
# 
# Input: nums = [14,70,53,83,49,91,36,80,92,51,66,70]
# Output: 127
# 
# Constraints:
# 
# 	1 <= nums.length <= 2 * 104
# 	0 <= nums[i] <= 231 - 1
#####################################################################################################

from typing import List


class Solution:
    def findMaximumXOR(self, nums: List[int]) -> int:  # time: O(n^2) --> TLE
        value = 0
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                xor = nums[i] ^ nums[j]
                if xor > value:
                    value = xor
        return value

    def findMaximumXOR(self, nums: List[int]) -> int:  # still TLE
        nums = sorted(set(nums), reverse=True)
        value = 0
        longest_bits = nums[0].bit_length()
        for i in range(len(nums)):
            if nums[i].bit_length() < longest_bits:
                break
            for j in range(i, len(nums)):
                value = max(nums[i] ^ nums[j], value)

        return value

    def findMaximumXOR(self, nums: List[int]) -> int:  # O(n)
        ans, mask = 0, 0
        for i in range(31, -1, -1):
            mask |= 1 << i
            found = {num & mask for num in nums}
            start = ans | 1 << i
            if any(start ^ pref in found for pref in found):
                ans = start
        return ans

    def findMaximumXOR(self, nums: List[int]) -> int:  # O(n) with optimization
        max_xor = mask = 0
        for i in reversed(range(max(nums).bit_length())):
            mask |= 1 << i
            next_max_xor = max_xor | 1 << i
            prefixes = {num & mask for num in nums}
            candidates = [num for num in nums if (num & mask) ^ next_max_xor in prefixes]
            if candidates:
                nums = candidates
                max_xor = next_max_xor
        return max_xor


def test():
    arrays_of_numbers = [
        [3, 10, 5, 25, 2, 8],
        [0],
        [2, 4],
        [8, 10, 2],
        [14, 70, 53, 83, 49, 91, 36, 80, 92, 51, 66, 70],
        ]
    expectations = [28, 0, 6, 10, 127]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().findMaximumXOR(nums)
        assert solution == expected
