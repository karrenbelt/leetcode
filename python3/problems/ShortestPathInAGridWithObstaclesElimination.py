### Source : https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-25

##################################################################################################### 
#
# You are given an m x n integer matrix grid where each cell is either 0 (empty) or 1 (obstacle). You 
# can move up, down, left, or right from and to an empty cell in one step.
# 
# Return the minimum number of steps to walk from the upper left corner (0, 0) to the lower right 
# corner (m - 1, n - 1) given that you can eliminate at most k obstacles. If it is not possible to 
# find such walk return -1.
# 
# Example 1:
# 
# Input: 
# grid = 
# [[0,0,0],
#  [1,1,0],
#  [0,0,0],
#  [0,1,1],
#  [0,0,0]], 
# k = 1
# Output: 6
# Explanation: 
# The shortest path without eliminating any obstacle is 10. 
# The shortest path with one obstacle elimination at position (3,2) is 6. Such path is (0,0) -> (0,1) 
# -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).
# 
# Example 2:
# 
# Input: 
# grid = 
# [[0,1,1],
#  [1,1,1],
#  [1,0,0]], 
# k = 1
# Output: -1
# Explanation: 
# We need to eliminate at least two obstacles to find such a walk.
# 
# Constraints:
# 
# 	m == grid.length
# 	n == grid[i].length
# 	1 <= m, n <= 40
# 	1 <= k <= m * n
# 	grid[i][j] == 0 or 1
# 	grid[0][0] == grid[m - 1][n - 1] == 0
#####################################################################################################

from typing import List


class Solution:
    def shortestPath(self, grid: List[List[int]], k: int) -> int:
        queue = [(0, 0, k)]
        seen = set()
        steps = 0
        while queue:
            new_queue = []
            for x, y, z in queue:
                if x == len(grid) - 1 and y == len(grid[0]) - 1:
                    return steps
                for i, j in [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)]:
                    if 0 <= i < len(grid) and 0 <= j < len(grid[0]) \
                            and z - grid[i][j] >= 0 and (i, j, z - grid[i][j]) not in seen:
                        new_queue.append((i, j, z - grid[i][j]))
                        seen.add((i, j, z - grid[i][j]))
            queue = new_queue
            steps += 1
        return -1


def test():
    arguments = [
        ([[0, 0, 0],
          [1, 1, 0],
          [0, 0, 0],
          [0, 1, 1],
          [0, 0, 0]], 1),

        ([[0, 1, 1],
          [1, 1, 1],
          [1, 0, 0]], 1),

        ([[0, 0], [1, 0], [1, 0], [1, 0], [1, 0], [1, 0], [0, 0],
          [0, 1], [0, 1], [0, 1], [0, 0], [1, 0], [1, 0], [0, 0]],4)
    ]
    expectations = [6, -1, 14]
    for (grid, k), expected in zip(arguments, expectations):
        solution = Solution().shortestPath(grid, k)
        assert solution == expected
