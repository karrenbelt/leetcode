### Source : https://leetcode.com/problems/all-elements-in-two-binary-search-trees/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-10

##################################################################################################### 
#
# Given two binary search trees root1 and root2.
# 
# Return a list containing all the integers from both trees sorted in ascending order.
# 
# Example 1:
# 
# Input: root1 = [2,1,4], root2 = [1,0,3]
# Output: [0,1,1,2,3,4]
# 
# Example 2:
# 
# Input: root1 = [0,-10,10], root2 = [5,1,7,0,2]
# Output: [-10,0,0,1,2,5,7,10]
# 
# Example 3:
# 
# Input: root1 = [], root2 = [5,1,7,0,2]
# Output: [0,1,2,5,7]
# 
# Example 4:
# 
# Input: root1 = [0,-10,10], root2 = []
# Output: [-10,0,10]
# 
# Example 5:
# 
# Input: root1 = [1,null,8], root2 = [8,1]
# Output: [1,1,8,8]
# 
# Constraints:
# 
# 	Each tree has at most 5000 nodes.
# 	Each node's value is between [-105, 105].
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def getAllElements(self, root1: TreeNode, root2: TreeNode) -> List[int]:  # O(n+m log n+m) time and O(n+m) space
        # simplest: traverse twice in-order, concatenate and sort. Optimize after

        def traverse(node: TreeNode, values: List) -> List[int]:
            if not node:
                return values
            traverse(node.left, values)
            values.append(node.val)
            traverse(node.right, values)
            return values

        return sorted(traverse(root1, []) + traverse(root2, []))  # python sort is fast enough (quick + merge sort)

    def getAllElements(self, root1: TreeNode, root2: TreeNode) -> List[int]:

        def traverse(node: TreeNode, values: List) -> List[int]:
            if not node:
                return values
            traverse(node.left, values)
            values.append(node.val)
            traverse(node.right, values)
            return values

        def merge_sorted_lists(list1: List[int], list2: List[int]):
            i = j = k = 0
            answer = [0] * (len(list1) + len(list2))
            while i < len(list1) and j < len(list2):
                if list1[i] < list2[j]:
                    answer[k] = list1[i]
                    i += 1
                else:
                    answer[k] = list2[j]
                    j += 1
                k += 1
            answer[k:] = list1[i:] or list2[j:]
            return answer

        return merge_sorted_lists(traverse(root1, []), traverse(root2, []))


def test():
    from python3 import Codec
    arguments = [
        ("[2,1,4]", "[1,0,3]"),
        ("[0,-10,10]", "[5,1,7,0,2]"),
        ("[]", "[5,1,7,0,2]"),
        ("[0,-10,10]", "[]"),
        ("[1,null,8]", "[8,1]"),
    ]
    expectations = [
        [0, 1, 1, 2, 3, 4],
        [-10, 0, 0, 1, 2, 5, 7, 10],
        [0, 1, 2, 5, 7],
        [-10, 0, 10],
        [1, 1, 8, 8],
    ]
    for serialized_trees, expected in zip(arguments, expectations):
        root1, root2 = map(Codec.deserialize, serialized_trees)
        solution = Solution().getAllElements(root1, root2)
        assert solution == expected
