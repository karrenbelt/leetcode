### Source : https://leetcode.com/problems/the-k-strongest-values-in-an-array/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-03

##################################################################################################### 
#
# Given an array of integers arr and an integer k.
# 
# A value arr[i] is said to be stronger than a value arr[j] if |arr[i] - m| > |arr[j] - m| where m is 
# the median of the array.
# If |arr[i] - m| == |arr[j] - m|, then arr[i] is said to be stronger than arr[j] if arr[i] > arr[j].
# 
# Return a list of the strongest k values in the array. return the answer in any arbitrary order.
# 
# edian is the middle value in an ordered integer list. ore formally, if the length of the list is 
# n, the median is the element in position ((n - 1) / 2) in the sorted list (0-indexed).
# 
# 	For arr = [6, -3, 7, 2, 11], n = 5 and the median is obtained by sorting the array arr = 
# [-3, 2, 6, 7, 11] and the median is arr[m] where m = ((5 - 1) / 2) = 2. The median is 6.
# 	For arr = [-7, 22, 17,&thinsp;3], n = 4 and the median is obtained by sorting the array arr 
# = [-7, 3, 17, 22] and the median is arr[m] where m = ((4 - 1) / 2) = 1. The median is 3.
# 
# Example 1:
# 
# Input: arr = [1,2,3,4,5], k = 2
# Output: [5,1]
# Explanation: edian is 3, the elements of the array sorted by the strongest are [5,1,4,2,3]. The 
# strongest 2 elements are [5, 1]. [1, 5] is also accepted answer.
# Please note that although |5 - 3| == |1 - 3| but 5 is stronger than 1 because 5 > 1.
# 
# Example 2:
# 
# Input: arr = [1,1,3,5,5], k = 2
# Output: [5,5]
# Explanation: edian is 3, the elements of the array sorted by the strongest are [5,5,1,1,3]. The 
# strongest 2 elements are [5, 5].
# 
# Example 3:
# 
# Input: arr = [6,7,11,7,6,8], k = 5
# Output: [11,8,6,6,7]
# Explanation: edian is 7, the elements of the array sorted by the strongest are [11,8,6,6,7,7].
# Any permutation of [11,8,6,6,7] is accepted.
# 
# Example 4:
# 
# Input: arr = [6,-3,7,2,11], k = 3
# Output: [-3,11,2]
# 
# Example 5:
# 
# Input: arr = [-7,22,17,3], k = 2
# Output: [22,17]
# 
# Constraints:
# 
# 	1 <= arr.length <= 105
# 	-105 <= arr[i] <= 105
# 	1 <= k <= arr.length
#####################################################################################################

from typing import List


class Solution:
    def getStrongest(self, arr: List[int], k: int) -> List[int]:  # O(n log n) time and O(n) space
        median = sorted(arr)[(len(arr) - 1) // 2]  # there exists an algorithm to find the median in O(n) time
        return sorted(arr, key=lambda n: (abs(n - median), n > median), reverse=True)[:k]

    def getStrongest(self, arr: List[int], k: int) -> List[int]:  # O(n log n) time and O(1) space
        median = arr.sort() or arr[(len(arr) - 1) // 2]
        return arr.sort(key=lambda n: (abs(n - median), n), reverse=True) or arr[:k]

    def getStrongest(self, arr: List[int], k: int) -> List[int]:  # O(n log n) time
        return arr.sort() or sorted(arr, key=lambda n: (abs(n - arr[(len(arr) - 1) // 2]), n))[-k:]


def test():
    from collections import Counter
    arguments = [
        ([1, 2, 3, 4, 5], 2),
        ([1, 1, 3, 5, 5], 2),
        ([6, 7, 11, 7, 6, 8], 5),
        ([6, -3, 7, 2, 11], 3),
        ([-7, 22, 17, 3], 2),
    ]
    expectations = [
        [5, 1],
        [5, 5],
        [11, 8, 6, 6, 7],
        [-3, 11, 2],
        [22, 17],
    ]
    for (arr, k), expected in zip(arguments, expectations):
        solution = Solution().getStrongest(arr, k)
        assert Counter(solution) == Counter(expected)
