### Source : https://leetcode.com/problems/longest-common-prefix/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Write a function to find the longest common prefix string amongst an array of strings.
# 
# If there is no common prefix, return an empty string "".
# 
# Example 1:
# 
# Input: ["flower","flow","flight"]
# Output: "fl"
# 
# Example 2:
# 
# Input: ["dog","racecar","car"]
# Output: ""
# Explanation: There is no common prefix among the input strings.
# 
# Note:
# 
# All given inputs are in lowercase letters a-z.
#####################################################################################################

from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:  # O(n^2) time
        # such a horrible solution
        if not strs:
            return ""
        lengths = [len(s) for s in strs]
        min_len = min(lengths)
        shortest = strs[lengths.index(min_len)]
        i = min_len
        while i > 0:
            prefix = shortest[:i]
            if all([s.startswith(prefix) for s in strs]):
                return prefix
            i -= 1
        return ""

    def longestCommonPrefix(self, strs: List[str]) -> str:  # O(n + m) time
        i, a, z = 0, min(strs), max(strs)
        while i < min(len(a), len(z)) and a[i] == z[i]:
            i += 1
        return a[:i]

    def longestCommonPrefix(self, strs: List[str]) -> str:  # O(n + m) time

        def minmax(sequence):
            it = iter(sequence)
            minimum = maximum = next(it)
            for elem in it:
                minimum, maximum = min(minimum, elem), max(maximum, elem)
            return minimum, maximum

        prefix, i, (a, z) = "", 0, minmax(strs)
        for c in zip(a, z):
            if a != z:
                break
            prefix += c
        return prefix

    def longestCommonPrefix(self, strs: List[str]) -> str:  # O(n * m) time
        prefix = ''
        for chars in zip(*strs):
            if len(set(chars)) > 1:
                break
            prefix += chars[0]
        return prefix

    def longestCommonPrefix(self, strs: List[str]) -> str:  # O(n * m) time
        prefix = ''
        for chars in map(set, zip(*strs)):
            if len(chars) > 1:
                break
            prefix += chars.pop()
        return prefix

    def longestCommonPrefix(self, strs: List[str]) -> str:
        import os
        return os.path.commonprefix(strs)

    def longestCommonPrefix(self, strs: List[str]) -> str:
        from itertools import takewhile, groupby

        def all_equal(iterable):  # more_itertools
            g = groupby(iterable)
            return next(g, True) and not next(g, False)

        return ''.join(x[0] for x in takewhile(all_equal, zip(*strs)))


def test():
    arrays_of_strings = [
        ["flower", "flow", "flight"],
        ["dog", "racecar", "car"],
        ]
    expectations = ["fl", ""]
    for strs, expected in zip(arrays_of_strings, expectations):
        solution = Solution().longestCommonPrefix(strs)
        assert solution == expected
