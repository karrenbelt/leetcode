### Source : https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-14

##################################################################################################### 
#
# Given the root of a binary tree, flatten the tree into a "linked list":
# 
# 	The "linked list" should use the same TreeNode class where the right child pointer points 
# to the next node in the list and the left child pointer is always null.
# 	The "linked list" should be in the same order as a pre-order traversal of the binary tree.
# 
# Example 1:
# 
# Input: root = [1,2,5,3,4,null,6]
# Output: [1,null,2,null,3,null,4,null,5,null,6]
# 
# Example 2:
# 
# Input: root = []
# Output: []
# 
# Example 3:
# 
# Input: root = [0]
# Output: [0]
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 2000].
# 	-100 <= Node.val <= 100
# 
# Follow up: Can you flatten the tree in-place (with O(1) extra space)?
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def flatten(self, root: TreeNode) -> None:
        def traverse(node: TreeNode):
            if not node:
                return
            nodes.append(node)
            traverse(node.left)
            traverse(node.right)

        if not root:
            return

        nodes = []
        traverse(root)
        for i in range(len(nodes) - 1):
            nodes[i].left = None
            nodes[i].right = nodes[i + 1]
        nodes[-1].left = None
        nodes[-1].right = None

    def flatten(self, root: TreeNode) -> None:
        node = root
        while node:
            if node.left:
                prev = node.left
                while prev.right:
                    prev = prev.right
                prev.right = node.right
                node.right = node.left
                node.left = None
            node = node.right

    def flatten(self, root: TreeNode) -> None:

        def traverse(node: TreeNode):
            nonlocal prev
            if not node:
                return
            traverse(node.right)
            traverse(node.left)
            node.right = prev
            node.left = None
            prev = node

        prev = None
        traverse(root)


def test():
    from python3 import Codec
    deserialized_trees = [
        "[1,2,5,3,4,null,6]",
        "[]",
        "[0]",
        ]
    expectations = [
        "[1,null,2,null,3,null,4,null,5,null,6]",
        "[]",
        "[0]",
        ]
    for deserialized_tree, expected in zip(deserialized_trees, expectations):
        root = Codec.deserialize(deserialized_tree)
        Solution().flatten(root)
        assert root == Codec.deserialize(expected)
