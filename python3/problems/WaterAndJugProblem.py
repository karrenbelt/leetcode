### Source : https://leetcode.com/problems/water-and-jug-problem/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-06

##################################################################################################### 
#
# You are given two jugs with capacities jug1Capacity and jug2Capacity liters. There is an infinite 
# amount of water supply available. Determine whether it is possible to measure exactly 
# targetCapacity liters using these two jugs.
# 
# If targetCapacity liters of water are measurable, you must have targetCapacity liters of water 
# contained within one or both buckets by the end.
# 
# Operations allowed:
# 
# 	Fill any of the jugs with water.
# 	Empty any of the jugs.
# 	Pour water from one jug into another till the other jug is completely full, or the first 
# jug itself is empty.
# 
# Example 1:
# 
# Input: jug1Capacity = 3, jug2Capacity = 5, targetCapacity = 4
# Output: true
# Explanation: The famous Die Hard example 
# 
# Example 2:
# 
# Input: jug1Capacity = 2, jug2Capacity = 6, targetCapacity = 5
# Output: false
# 
# Example 3:
# 
# Input: jug1Capacity = 1, jug2Capacity = 2, targetCapacity = 3
# Output: true
# 
# Constraints:
# 
# 	1 <= jug1Capacity, jug2Capacity, targetCapacity <= 106
#####################################################################################################


class Solution:
    def canMeasureWater(self, jug1Capacity: int, jug2Capacity: int, targetCapacity: int) -> bool:
        smaller, larger = (jug1Capacity, jug2Capacity) if jug1Capacity < jug2Capacity else (jug2Capacity, jug1Capacity)
        if targetCapacity > smaller + larger:
            return False

        queue = {(0, 0)}
        seen = set()
        while queue:
            new_queue = set()
            for x, y in queue:
                if x + y == targetCapacity:
                    return True
                seen.add((x, y))
                new_queue.update({
                    (smaller, y), (x, larger), (0, y), (x, 0),
                    (min(smaller, y + x), 0 if y < smaller - x else y - (smaller - x)),  # pour large into small
                    (0 if x + y < larger else x - (larger - y), min(y + x, larger)),     # pour small into large
                    })
            queue = new_queue - seen
        return False

    def canMeasureWater(self, jug1Capacity: int, jug2Capacity: int, targetCapacity: int) -> bool:
        # we find the greatest common denominator: Bezout's Lemma
        smaller, larger = (jug1Capacity, jug2Capacity) if jug1Capacity < jug2Capacity else (jug2Capacity, jug1Capacity)
        x, y = smaller, larger
        while y:
            x, y = y, x % y
        return bool(not targetCapacity or (x and targetCapacity <= smaller+larger and not targetCapacity % x))


def test():
    arguments = [
        (3, 5, 4),
        (2, 6, 5),
        (1, 2, 3),
        (4, 6, 8),
        ]
    expectations = [True, False, True, True]
    for (jug1Capacity, jug2Capacity, targetCapacity), expected in zip(arguments, expectations):
        solution = Solution().canMeasureWater(jug1Capacity, jug2Capacity, targetCapacity)
        assert solution == expected
