### Source : https://leetcode.com/problems/number-of-submatrices-that-sum-to-target/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-17

##################################################################################################### 
#
# Given a matrix and a target, return the number of non-empty submatrices that sum to target.
# 
# A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x <= x2 and y1 <= y <= 
# y2.
# 
# Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if they have some 
# coordinate that is different: for example, if x1 != x1'.
# 
# Example 1:
# 
# Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
# Output: 4
# Explanation: The four 1x1 submatrices that only contain 0.
# 
# Example 2:
# 
# Input: matrix = [[1,-1],[-1,1]], target = 0
# Output: 5
# Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the 2x2 submatrix.
# 
# Example 3:
# 
# Input: matrix = [[904]], target = 0
# Output: 0
# 
# Constraints:
# 
# 	1 <= matrix.length <= 100
# 	1 <= matrix[0].length <= 100
# 	-1000 <= matrix[i] <= 1000
# 	-108 <= target <= 108
#####################################################################################################

from typing import List


class Solution:
    def numSubmatrixSumTarget(self, matrix: List[List[int]], target: int) -> int:
        ## TODO: don't use backtracking, use subarray sum approach with prefix sum (dp) instead

        def is_valid(x1, x2, y1, y2):
            return 0 <= x1 < len(matrix) and 0 <= x2 < len(matrix) \
                   and 0 <= y1 < len(matrix[0]) and 0 <= y2 < len(matrix[0]) \
                   and x1 <= x2 and y1 <= y2

        def backtracking(x1, x2, y1, y2):
            if not is_valid(x1, x2, y1, y2):
                return False
            if (x1, x2, y1, y2) not in memo:
                count = 0
                # print()
                for i in range(x1, x2 + 1):
                    for j in range(y1, y2 + 1):
                        # print(i, j)
                        count += matrix[i][j]
                memo[x1, x2, y1, y2] = count == target
                for dx1, dx2, dy1, dy2 in [(-1, 0, 0, 0), (0, 1, 0, 0), (0, 0, -1, 0), (0, 0, 0, 1),
                                           # (-1, 0, -1, 0), (0, 1, 0, 1),
                                           (-1, 0, 0, 1), (0, 1, -1, 0),
                                           ]:
                    memo[x1 + dx1, x2 + dx2, y1 + dy1, y2 + dy2] = backtracking(x1 + dx1, x2 + dx2, y1 + dy1, y2 + dy2)
            return memo[x1, x2, y1, y2]

        memo = {}
        backtracking(0, 0, 0, 0)
        return sum(memo.values())


def test():
    arguments = [
        ([[0, 1, 0], [1, 1, 1], [0, 1, 0]], 0),
        ([[1, -1], [-1, 1]], 0),
        ([[907]], 0),
        ]
    expectations = [4, 5, 0]
    for (matrix, target), expected in zip(arguments, expectations):
        solution = Solution().numSubmatrixSumTarget(matrix, target)
        assert solution == expected
test()