### Source : https://leetcode.com/problems/sum-of-left-leaves/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-01

##################################################################################################### 
#
# Given the root of a binary tree, return the sum of all left leaves.
# 
# Example 1:
# 
# Input: root = [3,9,20,null,null,15,7]
# Output: 24
# Explanation: There are two left leaves in the binary tree, with values 9 and 15 respectively.
# 
# Example 2:
# 
# Input: root = [1]
# Output: 0
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 1000].
# 	-1000 <= Node.val <= 1000
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def sumOfLeftLeaves(self, root: TreeNode) -> int:

        def traverse(node: TreeNode) -> int:
            if not node:
                return 0
            if node.left and node.left.left is node.left.right:
                return node.left.val + traverse(node.right)
            return traverse(node.left) + traverse(node.right)

        return traverse(root)

    def sumOfLeftLeaves(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, left: bool = False) -> int:
            if not node:
                return 0
            if left and node.left is node.right:  # small trick
                return node.val
            return traverse(node.left, True) + traverse(node.right)

        return traverse(root)


def test():
    from python3 import Codec
    arguments = [
        "[3,9,20,null,null,15,7]",
        "[1]",
        "[1,2,3,4,5]",
    ]
    expecations = [24, 0, 4]
    for serialized_tree, expected in zip(arguments, expecations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().sumOfLeftLeaves(root)
        assert solution == expected
