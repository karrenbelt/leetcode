### Source : https://leetcode.com/problems/stone-game-vii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-11

##################################################################################################### 
#
# Alice and Bob take turns playing a game, with Alice starting first.
# 
# There are n stones arranged in a row. On each player's turn, they can remove either the leftmost 
# stone or the rightmost stone from the row and receive points equal to the sum of the remaining 
# stones' values in the row. The winner is the one with the higher score when there are no stones 
# left to remove.
# 
# Bob found that he will always lose this game (poor Bob, he always loses), so he decided to minimize 
# the score's difference. Alice's goal is to maximize the difference in the score.
# 
# Given an array of integers stones where stones[i] represents the value of the ith stone from the 
# left, return the difference in Alice and Bob's score if they both play optimally.
# 
# Example 1:
# 
# Input: stones = [5,3,1,4,2]
# Output: 6
# Explanation: 
# - Alice removes 2 and gets 5 + 3 + 1 + 4 = 13 points. Alice = 13, Bob = 0, stones = [5,3,1,4].
# - Bob removes 5 and gets 3 + 1 + 4 = 8 points. Alice = 13, Bob = 8, stones = [3,1,4].
# - Alice removes 3 and gets 1 + 4 = 5 points. Alice = 18, Bob = 8, stones = [1,4].
# - Bob removes 1 and gets 4 points. Alice = 18, Bob = 12, stones = [4].
# - Alice removes 4 and gets 0 points. Alice = 18, Bob = 12, stones = [].
# The score difference is 18 - 12 = 6.
# 
# Example 2:
# 
# Input: stones = [7,90,5,1,100,10,10,2]
# Output: 122
# 
# Constraints:
# 
# 	n == stones.length
# 	2 <= n <= 1000
# 	1 <= stones[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def stoneGameVII(self, stones: List[int]) -> int:

        def accumulate(nums: List[int]):
            it = iter(nums)
            total = next(it)
            yield total
            for n in it:
                total += n
                yield total

        dp = [[0] * len(stones) for _ in range(len(stones))]
        p_sum = [0] + list(accumulate(stones))
        for i in range(len(stones) - 2, -1, -1):
            for j in range(i + 1, len(stones)):
                dp[i][j] = max(p_sum[j + 1] - p_sum[i + 1] - dp[i + 1][j], p_sum[j] - p_sum[i] - dp[i][j - 1])
        return dp[0][len(stones) - 1]


def test():
    arguments = [
        [5, 3, 1, 4, 2],
        [7, 90, 5, 1, 100, 10, 10, 2],
    ]
    expectations = [6, 122]
    for stones, expected in zip(arguments, expectations):
        solution = Solution().stoneGameVII(stones)
        assert solution == expected, solution
test()