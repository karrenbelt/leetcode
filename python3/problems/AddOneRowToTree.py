### Source : https://leetcode.com/problems/add-one-row-to-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-09

##################################################################################################### 
#
# Given the root of a binary tree, then value v and depth d, you need to add a row of nodes with 
# value v at the given depth d. The root node is at depth 1. 
# 
# The adding rule is: given a positive integer depth d, for each NOT null tree nodes N in depth d-1, 
# create two tree nodes with value v as N's left subtree root and right subtree root. And N's 
# original left subtree should be the left subtree of the new left subtree root, its original right 
# subtree should be the right subtree of the new right subtree root. If depth d is 1 that means there 
# is no depth d-1 at all, then create a tree node with value v as the new root of the whole original 
# tree, and the original tree is the new root's left subtree.
# 
# Example 1:
# 
# Input: 
# A binary tree as following:
#        4
#      /   \
#     2     6
#    / \   / 
#   3   1 5   
# 
# v = 1
# 
# d = 2
# 
# Output: 
#        4
#       / \
#      1   1
#     /     \
#    2       6
#   / \     / 
#  3   1   5   
# 
# Example 2:
# 
# Input: 
# A binary tree as following:
#       4
#      /   
#     2    
#    / \   
#   3   1    
# 
# v = 1
# 
# d = 3
# 
# Output: 
#       4
#      /   
#     2
#    / \    
#   1   1
#  /     \  
# 3       1
# 
# Note:
# 
# The given d is in range [1, maximum depth of the given tree + 1].
# The given binary tree has at least one tree node.
# 
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def addOneRow(self, root: TreeNode, v: int, d: int) -> TreeNode:

        def traverse(node: TreeNode, distance):
            if distance == 1:
                node.left = TreeNode(v, left=node.left)
                node.right = TreeNode(v, right=node.right)
            if node.left:
                node.left = traverse(node.left, distance - 1)
            if node.right:
                node.right = traverse(node.right, distance - 1)
            return node

        if d == 1:
            return TreeNode(v, left=root)
        return traverse(root, d - 1)

    def addOneRow(self, root, v, d):
        dummy = TreeNode(None, left=root)
        row = [dummy]
        for _ in range(d - 1):
            row = [child for node in row for child in (node.left, node.right) if child]
        for node in row:
            node.left = TreeNode(v, left=node.left)
            node.right = TreeNode(v, right=node.right)
        return dummy.left


def test():
    from python3 import Codec
    arguments = [
        ("[4,2,6,3,1,5]", 1, 2),
        ("[4,2,null,3,1]", 1, 3),
        ("[1,2,3,4]", 5, 4),
        ]
    expectations = [
        "[4,1,1,2,null,null,6,3,1,5]",
        "[4,2,null,1,1,3,null,null,1]",
        "[1,2,3,4,null,null,null,5,5]",
        ]
    for (serialized_tree, v, d), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().addOneRow(root, v, d)
        assert solution == Codec.deserialize(expected)
