### Source : https://leetcode.com/problems/pacific-atlantic-water-flow/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-25

##################################################################################################### 
#
# Given an m x n matrix of non-negative integers representing the height of each unit cell in a 
# continent, the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic 
# ocean" touches the right and bottom edges.
# 
# Water can only flow in four directions (up, down, left, or right) from a cell to another one with 
# height equal or lower.
# 
# Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.
# 
# Note:
# 
# 	The order of returned grid coordinates does not matter.
# 	Both m and n are less than 150.
# 
# Example:
# 
# Given the following 5x5 matrix:
# 
#   Pacific ~   ~   ~   ~   ~ 
#        ~  1   2   2   3  (5) *
#        ~  3   2   3  (4) (4) *
#        ~  2   4  (5)  3   1  *
#        ~ (6) (7)  1   4   5  *
#        ~ (5)  1   1   2   4  *
#           *   *   *   *   * Atlantic
# 
# Return:
# 
# [[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]] (positions with parentheses in above 
# matrix).
# 
#####################################################################################################

from typing import List


class Solution:
    def pacificAtlantic(self, matrix: List[List[int]]) -> List[List[int]]:  # dfs: O(n) time and O(n) space
        if not matrix:
            return []

        def dfs(i: int, j: int, visited: set):
            visited.add((i, j))
            for x, y in ((i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)):
                if 0 <= x < m and 0 <= y < n and matrix[x][y] >= matrix[i][j] and (x, y) not in visited:
                    dfs(x, y, visited)

        m, n = len(matrix), len(matrix[0])
        a_visited = set()
        p_visited = set()
        for i in range(m):
            dfs(i, 0, p_visited)
            dfs(i, n - 1, a_visited)
        for j in range(n):
            dfs(0, j, p_visited)
            dfs(m - 1, j, a_visited)
        return list(p_visited & a_visited)

    def pacificAtlantic(self, matrix: List[List[int]]) -> List[List[int]]:  # bfs: O(n) time and O(n) space
        import collections

        def bfs(ocean: List):
            queue = collections.deque(ocean)
            visited = set(ocean)
            while queue:
                r, c = queue.popleft()
                for i, j in directions:
                    nr, nc = r + i, c + j
                    if 0 <= nr < m and 0 <= nc < n and (nr, nc) not in visited and matrix[nr][nc] >= matrix[r][c]:
                        queue.append((nr, nc))
                        visited.add((nr, nc))
            return visited

        if not matrix or not matrix[0]:
            return []

        m, n = len(matrix), len(matrix[0])
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        pacific = [(0, j) for j in range(n)] + [(i, 0) for i in range(1, m)]
        atlantic = [(i, n - 1) for i in range(m)] + [(m - 1, j) for j in range(n - 1)]
        return list(map(list, bfs(pacific) & bfs(atlantic)))


def test():
    arguments = [
        [
            [1, 2, 2, 3, 5],
            [3, 2, 3, 4, 4],
            [2, 4, 5, 3, 1],
            [6, 7, 1, 4, 5],
            [5, 1, 1, 2, 4],
        ]
        ]
    expectations = [
        [[0, 4], [1, 3], [1, 4], [2, 2], [3, 0], [3, 1], [4, 0]]
        ]
    for matrix, expected in zip(arguments, expectations):
        solution = Solution().pacificAtlantic(matrix)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
