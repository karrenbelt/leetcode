### Source : https://leetcode.com/problems/monotone-increasing-digits/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-02

##################################################################################################### 
#
# An integer has monotone increasing digits if and only if each pair of adjacent digits x and y 
# satisfy x <= y.
# 
# Given an integer n, return the largest number that is less than or equal to n with monotone 
# increasing digits.
# 
# Example 1:
# 
# Input: n = 10
# Output: 9
# 
# Example 2:
# 
# Input: n = 1234
# Output: 1234
# 
# Example 3:
# 
# Input: n = 332
# Output: 299
# 
# Constraints:
# 
# 	0 <= n <= 109
#####################################################################################################


class Solution:
    def monotoneIncreasingDigits(self, n: int) -> int:
        digits = list(map(int, str(n)))
        idx = len(digits)
        for i in range(len(digits) - 1, 0, -1):
            if digits[i] < digits[i - 1]:
                digits[i - 1] -= 1
                idx = i
        digits[idx:] = '9' * (len(digits) - idx)
        return int(''.join(map(str, digits)))


def test():
    arguments = [10, 1234, 332, 120, 12321]
    expectations = [9, 1234, 299, 119, 12299]
    for n, expected in zip(arguments, expectations):
        solution = Solution().monotoneIncreasingDigits(n)
        assert solution == expected, solution
