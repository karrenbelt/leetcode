### Source : https://leetcode.com/problems/sort-the-matrix-diagonally/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# Given a m * n matrix mat of integers, sort it diagonally in ascending order from the top-left to 
# the bottom-right then return the sorted array.
# 
# Example 1:
# 
# Input: mat = [[3,3,1,1],[2,2,1,2],[1,1,1,2]]
# Output: [[1,1,1,1],[1,2,2,2],[1,2,3,3]]
# 
# Constraints:
# 
# 	m == mat.length
# 	n == mat[i].length
# 	1 <= m, n <= 100
# 	1 <= mat[i][j] <= 100
#####################################################################################################

from typing import List


class Solution:
    def diagonalSort(self, mat: List[List[int]]) -> List[List[int]]:  # O(m * n log(min(m, n)) time, O(min(m, n)) space

        def sort(i: int, j: int):
            ij = list(zip(range(i, len(mat)), range(j, len(mat[0]))))
            values = iter(sorted(mat[i][j] for i, j in ij))
            for i, j in ij:
                mat[i][j] = next(values)

        any(sort(i, 0) for i in range(len(mat)))
        any(sort(0, j) for j in range(len(mat[0])))
        return mat

    def diagonalSort(self, mat: List[List[int]]) -> List[List[int]]:
        # m, n = , len(mat[0])

        def sort(i: int, j: int):
            rj = list(zip(mat[i:], range(j, len(mat[0]))))  # mat[i:] copies references only, not content
            values = iter(sorted(r[j] for r, j in rj))
            for r, j in rj:
                r[j] = next(values)

        any(sort(i, 0) for i in range(len(mat)))
        any(sort(0, j) for j in range(len(mat[0])))
        return mat

    def diagonalSort(self, mat: List[List[int]]) -> List[List[int]]:  # O(m * n log(min(m, n)) time, O(m * n) space
        for i in range(len(mat)):  # now in lower rows we only iterate the first cell, cutting time a bit
            for j in range(1 if i else len(mat[0])):
                rj = list(zip(mat[i:], range(j, len(mat[0]))))
                values = iter(sorted(r[j] for r, j in rj))
                for r, j in rj:
                    r[j] = next(values)
        return mat

    def diagonalSort(self, mat: List[List[int]]) -> List[List[int]]:  # time: O(m*n log(min(m, n)), space O(m * n)
        diagonals = {}
        for i, j, value in ((i, j, value) for i, row in enumerate(mat) for j, value in enumerate(row)):
            diagonals.setdefault(i - j, []).append(value)
        any(v.sort(reverse=True) for v in diagonals.values())
        for i, j in ((i, j) for i in range(len(mat)) for j in range(len(mat[0]))):
            mat[i][j] = diagonals[i - j].pop()
        return mat


def test():
    arguments = [
        [[3, 3, 1, 1],
         [2, 2, 1, 2],
         [1, 1, 1, 2]],

        [[11, 25, 66, 1, 69, 7],
         [23, 55, 17, 45, 15, 52],
         [75, 31, 36, 44, 58, 8],
         [22, 27, 33, 25, 68, 4],
         [84, 28, 14, 11, 5, 50]],
    ]
    expectations = [
        [[1, 1, 1, 1],
         [1, 2, 2, 2],
         [1, 2, 3, 3]],

        [[5, 17, 4, 1, 52, 7],
         [11, 11, 25, 45, 8, 69],
         [14, 23, 25, 44, 58, 15],
         [22, 27, 31, 36, 50, 66],
         [84, 28, 75, 33, 55, 68]],
    ]
    for mat, expected in zip(arguments, expectations):
        solution = Solution().diagonalSort(mat)
        assert solution == expected
