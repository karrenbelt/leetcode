### Source : https://leetcode.com/problems/search-in-a-binary-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-28

##################################################################################################### 
#
# You are given the root of a binary search tree (BST) and an integer val.
# 
# Find the node in the BST that the node's value equals val and return the subtree rooted with that 
# node. If such a node does not exist, return null.
# 
# Example 1:
# 
# Input: root = [4,2,7,1,3], val = 2
# Output: [2,1,3]
# 
# Example 2:
# 
# Input: root = [4,2,7,1,3], val = 5
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 5000].
# 	1 <= Node.val <= 107
# 	root is a binary search tree.
# 	1 <= val <= 107
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def searchBST(self, root: TreeNode, val: int) -> TreeNode:  # O(n) time
        # regular traversal to check if a value exists in a binary tree
        def traverse(node: TreeNode):
            return None if not node else node if node.val == val else traverse(node.left) or traverse(node.right)

        return traverse(root)

    def searchBST(self, root: TreeNode, val: int) -> TreeNode:  # O(h) time

        def traverse(node: TreeNode):
            if node:
                return node if node.val == val else traverse(node.left) if node.val > val else traverse(node.right)

        return traverse(root)

    def searchBST(self, root: TreeNode, val: int) -> TreeNode:  # O(h) time
        while root:
            if root.val == val:
                return root
            root = root.left if root.val > val else root.right


def test():
    from python3 import Codec
    arguments = [
        ("[4,2,7,1,3]", 2),
        ("[4,2,7,1,3]", 5),
    ]
    expectations = ["[2,1,3]", "[]"]
    for (serialized_tree, val), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().searchBST(root, val)
        assert solution == Codec.deserialize(expected)
