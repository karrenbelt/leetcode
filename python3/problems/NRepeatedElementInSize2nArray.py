### Source : https://leetcode.com/problems/n-repeated-element-in-size-2n-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# In a array A of size 2N, there are N+1 unique elements, and exactly one of these elements is 
# repeated N times.
# 
# Return the element repeated N times.
# 
# Example 1:
# 
# Input: [1,2,3,3]
# Output: 3
# 
# Example 2:
# 
# Input: [2,1,2,5,3,2]
# Output: 2
# 
# Example 3:
# 
# Input: [5,1,5,2,5,3,5,4]
# Output: 5
# 
# Note:
# 
# 	4 <= A.length <= 10000
# 	0 <= A[i] < 10000
# 	A.length is even
# 
#####################################################################################################

from typing import List


class Solution:
    def repeatedNTimes(self, A: List[int]) -> int:  # O(n^2) time and O(1) space -> TLE
        for i in range(len(A)):
            for j in range(i + 1, len(A)):
                if A[i] == A[j]:
                    return A[i]

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n^2) time and O(1) space -> TLE
        return next((A[i] for i in range(len(A)) for j in range(i + 1, len(A)) if A[i] == A[j]), None)

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n^2) time and O(1) space -> TLE
        return max(A, key=A.count)

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n log n) time and O(1) space
        A.sort()
        return A[0] if A[0] == A[1] else A[len(A) // 2]

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n) time and O(n) space
        seen = set()
        for n in A:
            if n in seen:
                return n
            seen.add(n)

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n) time and O(n) space
        # I would add None as a default to next, but would exceed the character limit of the line...
        return (seen := set()) or next(filter(lambda x: x is not None, (n if n in seen else seen.add(n) for n in A)))

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n) time and O(n) space
        from collections import Counter
        return Counter(A).most_common(1).pop()[0]

    def repeatedNTimes(self, A: List[int]) -> int:  # O(n) time and O(n) space
        return (sum(A) - sum(set(A))) // (len(A) // 2 - 1)


def test():
    arguments = [
        [1, 2, 3, 3],
        [2, 1, 2, 5, 3, 2],
        [5, 1, 5, 2, 5, 3, 5, 4],
        [4, 1, 7, 0, 0, 9, 0, 0],
    ]
    expectations = [3, 2, 5, 0]
    for A, expected in zip(arguments, expectations):
        solution = Solution().repeatedNTimes(A)
        assert solution == expected, solution
test()