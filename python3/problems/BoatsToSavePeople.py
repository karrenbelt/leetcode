### Source : https://leetcode.com/problems/boats-to-save-people/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# You are given an array people where people[i] is the weight of the ith person, and an infinite 
# number of boats where each boat can carry a maximum weight of limit. Each boat carries at most two 
# people at the same time, provided the sum of the weight of those people is at most limit.
# 
# Return the minimum number of boats to carry every given person.
# 
# Example 1:
# 
# Input: people = [1,2], limit = 3
# Output: 1
# Explanation: 1 boat (1, 2)
# 
# Example 2:
# 
# Input: people = [3,2,2,1], limit = 3
# Output: 3
# Explanation: 3 boats (1, 2), (2) and (3)
# 
# Example 3:
# 
# Input: people = [3,5,3,4], limit = 5
# Output: 4
# Explanation: 4 boats (3), (3), (4), (5)
# 
# Constraints:
# 
# 	1 <= people.length <= 5 * 104
# 	1 <= people[i] <= limit <= 3 * 104
#####################################################################################################

from typing import List


class Solution:
    def numRescueBoats(self, people: List[int], limit: int) -> int:  # O(n log n) time and O(n) space
        from collections import deque

        ctr, queue = 0, deque(sorted(people))
        while queue:
            ctr += 1
            weight = queue.pop()
            if queue and queue[0] + weight <= limit:
                queue.popleft()
        return ctr

    def numRescueBoats(self, people: List[int], limit: int) -> int:  # O(n log n) time and O(1) space
        people.sort()
        ctr, left, right = 0, 0, len(people) - 1
        while left < right:
            if people[right] + people[left] <= limit:
                left += 1
            ctr += 1
            right -= 1
        return ctr + (left == right)

    def numRescueBoats(self, people: List[int], limit: int) -> int:  # one less reference
        people.sort(reverse=True)
        left, right = 0, len(people) - 1
        while left <= right:
            if people[left] + people[right] <= limit:
                right -= 1
            left += 1
        return left

    def numRescueBoats(self, people: List[int], limit: int) -> int:  # generalized to k-people
        from collections import deque
        k = 2
        deque = deque(sorted(people))
        ctr = 0
        while deque:
            weight, people = 0, 0
            while deque and deque[-1] + weight <= limit and people < k:
                weight += deque.pop()
                people += 1
            while deque and deque[0] + weight <= limit and people < k:
                weight += deque.popleft()
                people += 1
            ctr += 1
        return ctr


def test():
    arrays_of_numbers = [
        [1, 2],
        [3, 2, 2, 1],
        [3, 5, 3, 4],
        [5, 1, 4, 2],
        ]
    limits = [3, 3, 5, 6]
    expectations = [1, 3, 4, 2]
    for people, limit, expected in zip(arrays_of_numbers, limits, expectations):
        solution = Solution().numRescueBoats(people, limit)
        assert solution == expected
