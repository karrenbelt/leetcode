### Source : https://leetcode.com/problems/rotate-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given the head of a linked list, rotate the list to the right by k places.
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5], k = 2
# Output: [4,5,1,2,3]
# 
# Example 2:
# 
# Input: head = [0,1,2], k = 4
# Output: [2,0,1]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 500].
# 	-100 <= Node.val <= 100
# 	0 <= k <= 2 * 109
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def rotateRight(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(n) space
        if not head or not head.next or not k:
            return head
        ordered = []
        node = head
        while node:
            ordered.append(node)
            node = node.next
        k %= len(ordered)
        ordered[-1].next = head  # make circular
        ordered[-k - 1].next = None  # break circle
        return ordered[-k]

    def rotateRight(self, head: ListNode, k: int) -> ListNode:  # O(n) time and O(1) space
        if not head or not head.next:
            return head
        last, n = head, 1
        while last.next:
            last = last.next
            n += 1
        if k % n == 0:  # important!
            return head
        middle = head
        for i in range(n - k % n - 1):
            middle = middle.next
        new_head, last.next, middle.next = middle.next, head, None
        return new_head


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        [1, 2, 3, 4, 5],
        [0, 1, 2],
        [1, 2]
        ]
    target_rotations = [2, 4, 0]
    expectations = [
        [4, 5, 1, 2, 3],
        [2, 0, 1],
        [1, 2]
        ]
    for nums, k, expected in zip(arrays_of_numbers, target_rotations, expectations):
        head = singly_linked_list_from_array(nums).head
        solution = Solution().rotateRight(head, k)
        assert solution == singly_linked_list_from_array(expected).head
