### Source : https://leetcode.com/problems/custom-sort-string/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# S and T are strings composed of lowercase letters. In S, no letter occurs more than once.
# 
# S was sorted in some custom order previously. We want to permute the characters of T so that they 
# match the order that S was sorted. ore specifically, if x occurs before y in S, then x should 
# occur before y in the returned string.
# 
# Return any permutation of T (as a string) that satisfies this property.
# 
# Example :
# Input: 
# S = "cba"
# T = "abcd"
# Output: "cbad"
# Explanation: 
# "a", "b", "c" appear in S, so the order of "a", "b", "c" should be "c", "b", and "a". 
# Since "d" does not appear in S, it can be at any position in T. "dcba", "cdba", "cbda" are also 
# valid outputs.
# 
# Note:
# 
# 	S has length at most 26, and no character is repeated in S.
# 	T has length at most 200.
# 	S and T consist of lowercase letters only.
# 
#####################################################################################################


class Solution:
    def customSortString(self, order: str, string: str) -> str:  # O(m + n) time
        d = {c: 0 for c in set(string + order)}
        for c in string:
            d[c] += 1
        return ''.join(c * d.pop(c) for c in order) + ''.join(c * v for c, v in d.items())

    def customSortString(self, S: str, T: str) -> str:  # O(m + n) time
        from collections import Counter
        return ''.join(c * d.pop(c, 0) for c in S) + ''.join(c * v for c, v in d.items()) if (d := Counter(T)) else T

    def customSortString(self, S: str, T: str) -> str:  # O(m * log(m) + n)
        return "".join(sorted(T, key=lambda t: m.get(t, -1))) if (m := {c: i for i, c in enumerate(S)}) else T

    def customSortString(self, order: str, string: str) -> str:  # O(m * n log n)
        return "".join(sorted(string, key=order.find))

    def customSortString(self, order: str, string: str) -> str:  # O(m + n log n) time
        d = {k: i for i, k in enumerate(order)}
        return "".join(sorted(string, key=lambda k: d.get(k, len(string) + ord(k))))


def test():
    pairs_of_strings = [
        ("cba", "abcd"),
        ("cbafg", "abcd"),
        ]
    expectations = ["cbad", "cbad"]
    for (order, string), expected in zip(pairs_of_strings, expectations):
        solution = Solution().customSortString(order, string)
        assert solution == expected
