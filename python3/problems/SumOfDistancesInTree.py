### Source : https://leetcode.com/problems/sum-of-distances-in-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-04

##################################################################################################### 
#
# There is an undirected connected tree with n nodes labeled from 0 to n - 1 and n - 1 edges.
# 
# You are given the integer n and the array edges where edges[i] = [ai, bi] indicates that there is 
# an edge between nodes ai and bi in the tree.
# 
# Return an array answer of length n where answer[i] is the sum of the distances between the ith node 
# in the tree and all other nodes.
# 
# Example 1:
# 
# Input: n = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
# Output: [8,12,6,10,10,10]
# Explanation: The tree is shown above.
# We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
# equals 1 + 1 + 2 + 2 + 2 = 8.
# Hence, answer[0] = 8, and so on.
# 
# Example 2:
# 
# Input: n = 1, edges = []
# Output: [0]
# 
# Example 3:
# 
# Input: n = 2, edges = [[1,0]]
# Output: [1,1]
# 
# Constraints:
# 
# 	1 <= n <= 3 * 104
# 	edges.length == n - 1
# 	edges[i].length == 2
# 	0 <= ai, bi < n
# 	ai != bi
# 	The given input represents a valid tree.
#####################################################################################################

from typing import List


class Solution:
    def sumOfDistancesInTree(self, N: int, edges: List[List[int]]) -> List[int]:
        # brute force would be to look up the distances of all child nodes for each node,
        # this would take O(n) time for each node and result in an O(n^2) time solution
        # however since each intermediate node is a child of it's parent and hence we do duplicate work
        def pre_order(node: int, parent: int) -> None:
            for child in graph.get(node, set()):
                if child != parent:
                    pre_order(child, node)
                    population[node] += population[child]
                    distances[node] += distances[child] + population[child]

        def post_order(node: int, parent: int) -> None:
            for child in graph.get(node, set()):
                if child != parent:
                    distances[child] = distances[node] + N - 2 * population[child]
                    post_order(child, node)

        graph = {}
        for i, j in edges:
            graph.setdefault(i, set()).add(j)
            graph.setdefault(j, set()).add(i)

        population = [1] * N  # numbers of the i-th node and all of its children
        distances = [0] * N  # sum of distance from the i-th node to each of its children
        pre_order(0, -1)
        post_order(0, -1)
        return distances


def test():
    arguments = [
        (6, [[0, 1], [0, 2], [2, 3], [2, 4], [2, 5]]),
        (1, []),
        (2, [[1, 0]]),
    ]
    expectations = [
        [8, 12, 6, 10, 10, 10],
        [0],
        [1, 1],
    ]
    for (N, edges), expected in zip(arguments, expectations):
        solution = Solution().sumOfDistancesInTree(N, edges)
        assert solution == expected
