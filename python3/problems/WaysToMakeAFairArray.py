### Source : https://leetcode.com/problems/ways-to-make-a-fair-array/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-27

##################################################################################################### 
#
# You are given an integer array nums. You can choose exactly one index (0-indexed) and remove the 
# element. Notice that the index of the elements may change after the removal.
# 
# For example, if nums = [6,1,7,4,1]:
# 
# 	Choosing to remove index 1 results in nums = [6,7,4,1].
# 	Choosing to remove index 2 results in nums = [6,1,4,1].
# 	Choosing to remove index 4 results in nums = [6,1,7,4].
# 
# An array is fair if the sum of the odd-indexed values equals the sum of the even-indexed values.
# 
# Return the number of indices that you could choose such that after the removal, nums is fair. 
# 
# Example 1:
# 
# Input: nums = [2,1,6,4]
# Output: 1
# Explanation:
# Remove index 0: [1,6,4] -> Even sum: 1 + 4 = 5. Odd sum: 6. Not fair.
# Remove index 1: [2,6,4] -> Even sum: 2 + 4 = 6. Odd sum: 6. Fair.
# Remove index 2: [2,1,4] -> Even sum: 2 + 4 = 6. Odd sum: 1. Not fair.
# Remove index 3: [2,1,6] -> Even sum: 2 + 6 = 8. Odd sum: 1. Not fair.
# There is 1 index that you can remove to make nums fair.
# 
# Example 2:
# 
# Input: nums = [1,1,1]
# Output: 3
# Explanation: You can remove any index and the remaining array is fair.
# 
# Example 3:
# 
# Input: nums = [1,2,3]
# Output: 0
# Explanation: You cannot make a fair array after removing any index.
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	1 <= nums[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def waysToMakeFair(self, nums: List[int]) -> int:  # O(n^2) time
        ctr = 0
        for i in range(len(nums)):
            new = nums[:i] + nums[i + 1:]
            ctr += sum(new[::2]) == sum(new[1::2])
        return ctr

    def waysToMakeFair(self, nums: List[int]) -> int:  # O(n) time
        ctr, s1, s2 = 0, [0, 0], [sum(nums[0::2]), sum(nums[1::2])]
        for i, n in enumerate(nums):
            s2[i % 2] -= n
            ctr += s1[0] + s2[1] == s1[1] + s2[0]
            s1[i % 2] += n
        return ctr


def test():
    arguments = [
        [2, 1, 6, 4],
        [1, 1, 1],
        [1, 2, 3],
    ]
    expectations = [1, 3, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().waysToMakeFair(nums)
        assert solution == expected
