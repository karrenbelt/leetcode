### Source : https://leetcode.com/problems/palindrome-partitioning/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-23

#####################################################################################################
#
# Given a string s, partition s such that every substring of the partition is a palindrome. Return
# all possible palindrome partitioning of s.
#
# A palindrome string is a string that reads the same backward as forward.
#
# Example 1:
# Input: s = "aab"
# Output: [["a","a","b"],["aa","b"]]
# Example 2:
# Input: s = "a"
# Output: [["a"]]
#
# Constraints:
#
# 	1 <= s.length <= 16
# 	s contains only lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def partition(self, s: str) -> List[List[str]]:  # O(2^n) time and O(2^n) space

        def backtracking(i, path):
            if i == len(s):
                paths.append(path)
            else:
                for j in range(i + 1, len(s) + 1):
                    sub = s[i:j]
                    if sub == sub[::-1]:
                        backtracking(j, path + [sub])

        paths = []
        backtracking(0, [])
        return paths

    def partition(self, s: str) -> List[List[str]]:

        def is_palindrome(string: str):
            if string not in memo:
                memo[string] = string == string[::-1]
            return memo[string]

        def backtracking(i, path):
            if i == len(s):
                paths.append(path)
            else:
                for j in range(i + 1, len(s) + 1):
                    sub = s[i:j]
                    if is_palindrome(sub):
                        backtracking(j, path + [sub])

        memo = {}
        paths = []
        backtracking(0, [])
        return paths

    def partition(self, s: str) -> List[List[str]]:  # iterative with tmp
        paths = [[]]
        for c in s:
            tmp = []
            for path in paths:
                if path and path[-1] == c:
                    tmp.append(path[:-1] + [c*2])
                if len(path) > 1 and path[-2] == c:
                    tmp.append(path[:-2] + [c+path[-1] + c])
                tmp += [path + [c]]
            paths = tmp
        return paths

    def partition(self, s: str) -> List[List[str]]:  # without tmp
        paths = [[]]
        for c in s:
            for i in range(len(paths)):   # cannot enumerate without copying; changes size, can iterate len
                path = paths[i]
                if path and path[-1] == c:
                    paths.append(path[:-1] + [c * 2])
                if len(path) > 1 and path[-2] == c:
                    paths.append(path[:-2] + [c + path[-1] + c])
                paths[i].append(c)
        return paths

    def partition(self, s):  # slowest
        return [[s[:i]] + r for i in range(1, len(s)+1) if s[:i] == s[i-1::-1] for r in self.partition(s[i:])] or [[]]


def test():
    arguments = ["aab", "a"]
    expectations = [
        [["a", "a", "b"], ["aa", "b"]],
        [["a"]],
        ]
    for s, expected in zip(arguments, expectations):
        solution = Solution().partition(s)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
