### Source : https://leetcode.com/problems/island-perimeter/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-04

##################################################################################################### 
#
# You are given row x col grid representing a map where grid[i][j] = 1 represents land and grid[i][j] 
# = 0 represents water.
# 
# Grid cells are connected horizontally/vertically (not diagonally). The grid is completely 
# surrounded by water, and there is exactly one island (i.e., one or more connected land cells).
# 
# The island doesn't have "lakes", meaning the water inside isn't connected to the water around the 
# island. One cell is a square with side length 1. The grid is rectangular, width and height don't 
# exceed 100. Determine the perimeter of the island.
# 
# Example 1:
# 
# Input: grid = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
# Output: 16
# Explanation: The perimeter is the 16 yellow stripes in the image above.
# 
# Example 2:
# 
# Input: grid = [[1]]
# Output: 4
# 
# Example 3:
# 
# Input: grid = [[1,0]]
# Output: 4
# 
# Constraints:
# 
# 	row == grid.length
# 	col == grid[i].length
# 	1 <= row, col <= 100
# 	grid[i][j] is 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def islandPerimeter(self, grid: List[List[int]]) -> int:
        # only one island without lakes exists
        def dfs(i: int, j: int) -> int:
            if not 0 <= i < len(grid) or not 0 <= j < len(grid[0]) or not grid[i][j]:
                return 1
            seen.add((i, j))
            return sum(dfs(x, y) for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)] if (x, y) not in seen)

        seen = set()
        return dfs(*next((i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j]))

    def islandPerimeter(self, grid):
        # for each cell with land sum the surrounding units of water
        def dfs(i: int, j: int) -> int:
            return sum((j == 0 or grid[i][j - 1] == 0,
                        j == len(grid[0]) - 1 or grid[i][j + 1] == 0,
                        i == 0 or grid[i - 1][j] == 0,
                        i == len(grid) - 1 or grid[i + 1][j] == 0))

        return sum(dfs(i, j) for i, row in enumerate(grid) for j, cell in enumerate(row) if cell)


def test():
    arguments = [
        [[0, 1, 0, 0], [1, 1, 1, 0], [0, 1, 0, 0], [1, 1, 0, 0]],
        [[1]],
        [[1, 0]],
    ]
    expectations = [16, 4, 4]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().islandPerimeter(grid)
        assert solution == expected, (solution, expected)
