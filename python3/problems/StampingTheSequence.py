### Source : https://leetcode.com/problems/stamping-the-sequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-31

##################################################################################################### 
#
# You want to form a target string of lowercase letters.
# 
# At the beginning, your sequence is target.length '?' marks.  You also have a stamp of lowercase 
# letters.
# 
# On each turn, you may place the stamp over the sequence, and replace every letter in the sequence 
# with the corresponding letter from the stamp.  You can make up to 10 * target.length turns.
# 
# For example, if the initial sequence is "?????", and your stamp is "abc",  then you may make 
# "abc??", "?abc?", "??abc" in the first turn.  (Note that the stamp must be fully contained in the 
# boundaries of the sequence in order to stamp.)
# 
# If the sequence is possible to stamp, then return an array of the index of the left-most letter 
# being stamped at each turn.  If the sequence is not possible to stamp, return an empty array.
# 
# For example, if the sequence is "ababc", and the stamp is "abc", then we could return the answer 
# [0, 2], corresponding to the moves "?????" -> "abc??" -> "ababc".
# 
# Also, if the sequence is possible to stamp, it is guaranteed it is possible to stamp within 10 * 
# target.length moves.  Any answers specifying more than this number of moves will not be accepted.
# 
# Example 1:
# 
# Input: stamp = "abc", target = "ababc"
# Output: [0,2]
# ([1,0,2] would also be accepted as an answer, as well as some other answers.)
# 
# Example 2:
# 
# Input: stamp = "abca", target = "aabcaca"
# Output: [3,0,1]
# 
# Note:
# 
# 	1 <= stamp.length <= target.length <= 1000
# 	stamp and target only contain lowercase letters.
#####################################################################################################

from typing import List


class Solution:
    def movesToStamp(self, stamp: str, target: str) -> List[int]:  # O(n^2) time
        # reconstruct the reverse stamping order
        if stamp[0] != target[0] or stamp[-1] != target[-1]:
            return []
        if set(stamp) != set(target) or stamp not in target:
            return []

        def place_stamp(i: int) -> bool:
            changed = False
            for j in range(len(stamp)):
                if stamped[i + j] == '*':
                    continue
                if stamped[i + j] != stamp[j]:
                    return False
                changed = True
            if changed:
                stamped[i: i + len(stamp)] = ['*'] * len(stamp)
                stamp_order.append(i)
            return changed

        stamped = list(target)
        stamp_order = []

        changed = True
        while changed:
            changed = False
            for i in range(len(target) - len(stamp) + 1):
                changed |= place_stamp(i)

        not_found = set(stamped).intersection(target)
        stamp_order.reverse()
        return [] if not_found else stamp_order

    def movesToStamp(self, stamp: str, target: str) -> List[int]:

        def dfs(i, j, path):
            if (i, j) in memo:
                return memo[i, j]
            elif j == len(target):
                memo[i, j] = path if i == len(stamp) else []
            elif i == len(stamp):
                for k in range(len(stamp)):
                    candidate = dfs(k, j, [j - k] + path)  # prepend the stamp
                    memo[i, j] = candidate
                    if candidate:
                        break
            elif stamp[i] == target[j]:
                # we try to match the next character of the stamp, otherwise need to start a new one
                candidate = dfs(i + 1, j + 1, path)  # see if we can match more of this stamp
                memo[i, j] = candidate if candidate else dfs(0, j + 1, path + [j + 1])  # start a new one
            else:
                memo[i, j] = []
            return memo[i, j]

        memo = {}
        return dfs(0, 0, [0])


def test():
    arguments = [
        ("abc", "ababc"),
        ("abca", "aabcaca"),
        ]
    expectations = [
        {(0, 2), (1, 0, 2)},  # more solutions
        {(3, 0, 1), (0, 3, 1)},
        ]
    for (stamp, target), expected in zip(arguments, expectations):
        solution = Solution().movesToStamp(stamp, target)
        assert tuple(solution) in expected
