### Source : https://leetcode.com/problems/design-authentication-manager/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# There is an authentication system that works with authentication tokens. For each session, the user 
# will receive a new authentication token that will expire timeToLive seconds after the currentTime. 
# If the token is renewed, the expiry time will be extended to expire timeToLive seconds after the 
# (potentially different) currentTime.
# 
# Implement the Authenticationanager class:
# 
# 	Authenticationanager(int timeToLive) constructs the Authenticationanager and sets the 
# timeToLive.
# 	generate(string tokenId, int currentTime) generates a new token with the given tokenId at 
# the given currentTime in seconds.
# 	renew(string tokenId, int currentTime) renews the unexpired token with the given tokenId at 
# the given currentTime in seconds. If there are no unexpired tokens with the given tokenId, the 
# request is ignored, and nothing happens.
# 	countUnexpiredTokens(int currentTime) returns the number of unexpired tokens at the given 
# currentTime.
# 
# Note that if a token expires at time t, and another action happens on time t (renew or 
# countUnexpiredTokens), the expiration takes place before the other actions.
# 
# Example 1:
# 
# Input
# ["Authenticationanager", "renew", "generate", "countUnexpiredTokens", "generate", "renew", 
# "renew", "countUnexpiredTokens"]
# [[5], ["aaa", 1], ["aaa", 2], [6], ["bbb", 7], ["aaa", 8], ["bbb", 10], [15]]
# Output
# [null, null, null, 1, null, null, null, 0]
# 
# Explanation
# Authenticationanager authenticationanager = new Authenticationanager(5); // Constructs the 
# Authenticationanager with timeToLive = 5 seconds.
# authenticationanager.renew("aaa", 1); // No token exists with tokenId "aaa" at time 1, so nothing 
# happens.
# authenticationanager.generate("aaa", 2); // Generates a new token with tokenId "aaa" at time 2.
# authenticationanager.countUnexpiredTokens(6); // The token with tokenId "aaa" is the only 
# unexpired one at time 6, so return 1.
# authenticationanager.generate("bbb", 7); // Generates a new token with tokenId "bbb" at time 7.
# authenticationanager.renew("aaa", 8); // The token with tokenId "aaa" expired at time 7, and 8 >= 
# 7, so at time 8 the renew request is ignored, and nothing happens.
# authenticationanager.renew("bbb", 10); // The token with tokenId "bbb" is unexpired at time 10, so 
# the renew request is fulfilled and now the token will expire at time 15.
# authenticationanager.countUnexpiredTokens(15); // The token with tokenId "bbb" expires at time 15, 
# and the token with tokenId "aaa" expired at time 7, so currently no token is unexpired, so return 0.
# 
# Constraints:
# 
# 	1 <= timeToLive <= 108
# 	1 <= currentTime <= 108
# 	1 <= tokenId.length <= 5
# 	tokenId consists only of lowercase letters.
# 	All calls to generate will contain unique values of tokenId.
# 	The values of currentTime across all the function calls will be strictly increasing.
# 	At most 2000 calls will be made to all functions combined.
#####################################################################################################


class AuthenticationManager:

    def __init__(self, timeToLive: int):
        from sortedcontainers import SortedList
        self.limit = timeToLive
        self.times = SortedList()
        self.expiry = dict()

    def generate(self, tokenId: str, currentTime: int) -> None:  # O(1)
        self.expiry[tokenId] = currentTime + self.limit
        self.times.add(self.expiry[tokenId])

    def renew(self, tokenId: str, currentTime: int) -> None:  # O(1)
        if tokenId in self.expiry and currentTime < self.expiry.get(tokenId, 0):
            time, new_time = self.expiry[tokenId], currentTime + self.limit
            self.expiry[tokenId] = new_time
            self.times.remove(time)
            self.times.add(new_time)

    def countUnexpiredTokens(self, currentTime: int) -> int:  # O(log n)
        index = self.times.bisect_right(currentTime)
        return len(self.times) - index


class AuthenticationManager:

    def __init__(self, timeToLive: int):
        self.tokens = dict()
        self.time = timeToLive

    def generate(self, tokenId: str, currentTime: int) -> None:
        self.tokens[tokenId] = currentTime

    def renew(self, tokenId: str, currentTime: int) -> None:
        expiry_date = currentTime - self.time
        if tokenId in self.tokens and self.tokens[tokenId] > expiry_date:
            self.tokens[tokenId] = currentTime

    def countUnexpiredTokens(self, currentTime: int) -> int:
        expiry_date = currentTime - self.time
        return sum(t > expiry_date for t in self.tokens.values())


class AuthenticationManager:

    def __init__(self, timeToLive: int):
        from collections import deque
        self.limit = timeToLive
        self.queue = deque()
        self.tokens = {}

    def update(self, currentTime: int):
        while self.queue and self.queue[0][0] <= currentTime:
            ti, to = self.queue.popleft()
            if self.tokens[to] == ti:
                self.tokens.pop(to)

    def generate(self, tokenId: str, currentTime: int) -> None:
        self.tokens[tokenId] = currentTime + self.limit
        self.queue.append((self.tokens[tokenId], tokenId))

    def renew(self, tokenId: str, currentTime: int) -> None:
        self.update(currentTime)
        if tokenId in self.tokens and self.tokens[tokenId] > currentTime:
            self.tokens.pop(tokenId)
            self.generate(tokenId, currentTime)

    def countUnexpiredTokens(self, currentTime: int) -> int:
        self.update(currentTime)
        return len(self.tokens)

# Your AuthenticationManager object will be instantiated and called as such:
# obj = AuthenticationManager(timeToLive)
# obj.generate(tokenId,currentTime)
# obj.renew(tokenId,currentTime)
# param_3 = obj.countUnexpiredTokens(currentTime)


def test():
    i = 2
    methods = [
        ["AuthenticationManager", "renew", "generate", "countUnexpiredTokens",
         "generate", "renew", "renew", "countUnexpiredTokens"],
        ["AuthenticationManager", "countUnexpiredTokens", "renew", "generate",
         "renew", "countUnexpiredTokens", "renew", "generate",
         "countUnexpiredTokens", "countUnexpiredTokens", "countUnexpiredTokens"],
        ["AuthenticationManager", "renew", "countUnexpiredTokens",
         "countUnexpiredTokens", "generate", "generate", "renew", "generate",
         "generate", "countUnexpiredTokens", "countUnexpiredTokens",
         "countUnexpiredTokens", "renew", "countUnexpiredTokens",
         "countUnexpiredTokens", "countUnexpiredTokens", "generate",
         "countUnexpiredTokens", "renew"],
    ][i]
    arguments = [
        [[5], ["aaa", 1], ["aaa", 2], [6], ["bbb", 7], ["aaa", 8], ["bbb", 10],
         [15]],
        [[28], [2], ["xokiw", 6], ["ofn", 7], ["dses", 15], [17], ["ofzu", 19],
         ["dses", 20], [23], [27], [30]],
        [[13], ["ajvy", 1], [3], [4], ["fuzxq", 5], ["izmry", 7], ["puv", 12],
         ["ybiqb", 13], ["gm", 14], [15], [18], [19], ["ybiqb", 21], [23], [25],
         [26], ["aqdm", 28], [29], ["puv", 30]],
    ][i]
    expectations = [
        [None, None, None, 1, None, None, None, 0],
        [None, 0, None, None, None, 1, None, None, 2, 2, 2],
        [None, None, 0, 0, None, None, None, None, None, 4, 3, 3, None, 2, 2, 2,
         None, 2, None],
    ][i]
    obj = AuthenticationManager(*arguments[0])
    for method, args, expected in zip(methods[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
