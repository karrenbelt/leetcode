### Source : https://leetcode.com/problems/longest-consecutive-sequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-12

##################################################################################################### 
#
# Given an unsorted array of integers nums, return the length of the longest consecutive elements 
# sequence.
# 
# Example 1:
# 
# Input: nums = [100,4,200,1,3,2]
# Output: 4
# Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
# 
# Example 2:
# 
# Input: nums = [0,3,7,2,5,8,4,6,0,1]
# Output: 9
# 
# Constraints:
# 
# 	0 <= nums.length <= 104
# 	-109 <= nums[i] <= 109
# 
# Follow up: Could you implement the O(n) solution?
#####################################################################################################

from typing import List


class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        nums.sort()
        max_len, curr = 0, int(len(nums) > 0)
        for i in range(1, len(nums)):
            if nums[i] - 1 == nums[i - 1]:
                curr += 1
                max_len = max(max_len, curr)
            elif not nums[i] == nums[i - 1]:
                curr = 1
        return max(max_len, curr)

    def longestConsecutive(self, nums: List[int]) -> int:
        # question was later changed and now the O(n) time requirement is a must, not a follow-up
        # this is just the first thing I came up with, without taking time to think it trough. Shrug. Sunday night
        # so ridiculous. But need to finish the daily challenge I suppose

        class ListNode:
            def __init__(self, value, next=None, prev=None):
                self.value = value
                self.next = next
                self.prev = prev

            def __repr__(self):
                return f"{self.__class__.__name__}: {self.value}"

        nodes = {}
        for n in set(nums):
            node = ListNode(n)
            if n - 1 in nodes:
                nodes[n - 1].next = node
                node.prev = nodes[n - 1]
            if n + 1 in nodes:
                nodes[n + 1].prev = node
                node.next = nodes[n + 1]
            nodes[n] = node

        longest = 0
        while list(nodes):
            length = 1
            forward = backward = nodes.pop(next(iter(nodes)))
            while forward.next:
                forward = forward.next
                nodes.pop(forward.value)
                length += 1
            while backward.prev:
                backward = backward.prev
                nodes.pop(backward.value)
                length += 1
            longest = max(longest, length)
        return longest

    def longestConsecutive(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        nums, longest = set(nums), 0
        while nums:
            forward = backward = nums.pop()
            while forward + 1 in nums:
                forward += not nums.remove(forward + 1)
            while backward - 1 in nums:
                backward -= not nums.remove(backward - 1)
            longest = max(longest, forward - backward + 1)
        return longest

    def longestConsecutive(self, A: List[int]) -> int:
        from itertools import dropwhile, count
        return (lambda S: max((next(dropwhile(lambda y: y in S, count(x+1))) - x for x in S if x-1 not in S), default=0))(set(A))


def test():
    arguments = [
        [100, 4, 200, 1, 3, 2],
        [0, 3, 7, 2, 5, 8, 4, 6, 0, 1],
        [1, 2, 0, 1],
        [9, 1, -3, 2, 4, 8, 3, -1, 6, -2, -4, 7],
        ]
    expectations = [4, 9, 3]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().longestConsecutive(nums)
        assert solution == expected, solution
