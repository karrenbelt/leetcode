### Source : https://leetcode.com/problems/max-points-on-a-line/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane, return the 
# maximum number of points that lie on the same straight line.
# 
# Example 1:
# 
# Input: points = [[1,1],[2,2],[3,3]]
# Output: 3
# 
# Example 2:
# 
# Input: points = [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
# Output: 4
# 
# Constraints:
# 
# 	1 <= points.length <= 300
# 	points[i].length == 2
# 	-104 <= xi, yi <= 104
# 	All the points are unique.
#####################################################################################################

from typing import List


class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:  # O(n^2) time
        coefficients = {}
        for i in range(len(points)):
            for j in range(i + 1, len(points)):
                (x1, y1), (x2, y2) = points[i], points[j]
                dx, dy = x1 - x2, y1 - y2
                if not dx:
                    coefficients.setdefault(x1, set()).add(j)
                else:
                    slope = dy / dx
                    intercept = y1 - dy / dx * x1
                    coefficients.setdefault((slope, intercept), set()).add(j)
        return max(map(len, coefficients.values())) + 1 if coefficients else len(points)


def test():
    arguments = [
        [[1, 1], [2, 2], [3, 3]],
        [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]],
        [[0, 0]],
        [[3, 3], [1, 4], [1, 1], [2, 1], [2, 2]],
        # list(zip(range(300), range(300))),
        ]
    expectations = [3, 4, 1, 3, 300]
    for points, expected in zip(arguments, expectations):
        solution = Solution().maxPoints(points)
        assert solution == expected
