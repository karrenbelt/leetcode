### Source : https://leetcode.com/problems/second-largest-digit-in-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-02

##################################################################################################### 
#
# Given an alphanumeric string s, return the second largest numerical digit that appears in s, or -1 
# if it does not exist.
# 
# An alphanumeric string is a string consisting of lowercase English letters and digits.
# 
# Example 1:
# 
# Input: s = "dfa12321afd"
# Output: 2
# Explanation: The digits that appear in s are [1, 2, 3]. The second largest digit is 2.
# 
# Example 2:
# 
# Input: s = "abc1111"
# Output: -1
# Explanation: The digits that appear in s are [1]. There is no second largest digit. 
# 
# Constraints:
# 
# 	1 <= s.length <= 500
# 	s consists of only lowercase English letters and/or digits.
#####################################################################################################

class Solution:
    def secondHighest(self, s: str) -> int:
        nums = sorted(map(int, set(filter(str.isdigit, s))))
        return nums[-2] if len(nums) > 1 else -1

    def secondHighest(self, s: str) -> int:
        return ([-1, -1] + sorted(map(int, filter(str.isdigit, set(s)))))[-2]

    def secondHighest(self, s: str) -> int:  # O(n log k) -> O(n) since k = 2
        import heapq
        largest = heapq.nlargest(2, map(int, filter(str.isdigit, set(s))))
        return largest.pop() if len(largest) > 1 else -1

    def secondHighest(self, s: str) -> int:  # O(n)
        max1 = max2 = -1
        for n in map(int, filter(str.isdigit, s)):
            if n > max1:
                max1, max2 = n, max1
            elif max1 > n > max2:
                max2 = n
        return max2

    def secondHighest(self, s: str) -> int:
        from sortedcontainers import SortedSet
        s = SortedSet(map(int, filter(str.isdigit, s)))
        return s.pop(-2) if len(s) > 1 else -1


def test():
    arguments = [
        "dfa12321afd",
        "abc1111",
        "ck077",
        "sjhtz8344",
    ]
    expectations = [2, -1, 0, 4]
    for s, expected in zip(arguments, expectations):
        solution = Solution().secondHighest(s)
        assert solution == expected
