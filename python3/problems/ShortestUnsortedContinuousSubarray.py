### Source : https://leetcode.com/problems/shortest-unsorted-continuous-subarray/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-25

##################################################################################################### 
#
# Given an integer array nums, you need to find one continuous subarray that if you only sort this 
# subarray in ascending order, then the whole array will be sorted in ascending order.
# 
# Return the shortest such subarray and output its length.
# 
# Example 1:
# 
# Input: nums = [2,6,4,8,10,9,15]
# Output: 5
# Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in 
# ascending order.
# 
# Example 2:
# 
# Input: nums = [1,2,3,4]
# Output: 0
# 
# Example 3:
# 
# Input: nums = [1]
# Output: 0
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-105 <= nums[i] <= 105
# 
# Follow up: Can you solve it in O(n) time complexity?
#####################################################################################################

from typing import List


class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:  # O(n log n)
        sorted_nums = sorted(nums)
        i = j = 0
        while i < len(nums) and nums[i] == sorted_nums[i]:
            i += 1
        if i != len(nums):
            while j < len(nums) and nums[-j-1] == sorted_nums[-j-1]:
                j += 1
        return len(nums) - j - i


def test():
    arrays_of_numbers = [
        [2, 6, 4, 8, 10, 9, 15],
        [1, 2, 3, 4],
        [1],
        ]
    expectations = [5, 0, 0]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().findUnsortedSubarray(nums)
        assert solution == expected
