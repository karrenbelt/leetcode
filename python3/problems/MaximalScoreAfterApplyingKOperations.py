### Source : https://leetcode.com/problems/maximal-score-after-applying-k-operations/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-05

##################################################################################################### 
#
# You are given a 0-indexed integer array nums and an integer k. You have a starting score of 0.
# 
# In one operation:
# 
# 	choose an index i such that 0 <= i < nums.length,
# 	increase your score by nums[i], and
# 	replace nums[i] with ceil(nums[i] / 3).
# 
# Return the maximum possible score you can attain after applying exactly k operations.
# 
# The ceiling function ceil(val) is the least integer greater than or equal to val.
# 
# Example 1:
# 
# Input: nums = [10,10,10,10,10], k = 5
# Output: 50
# Explanation: Apply the operation to each array element exactly once. The final score is 10 + 10 + 
# 10 + 10 + 10 = 50.
# 
# Example 2:
# 
# Input: nums = [1,10,3,3,3], k = 3
# Output: 17
# Explanation: You can do the following operations:
# Operation 1: Select i = 1, so nums becomes [1,4,3,3,3]. Your score increases by 10.
# Operation 2: Select i = 1, so nums becomes [1,2,3,3,3]. Your score increases by 4.
# Operation 3: Select i = 2, so nums becomes [1,1,1,3,3]. Your score increases by 3.
# The final score is 10 + 4 + 3 = 17.
# 
# Constraints:
# 
# 	1 <= nums.length, k <= 105
# 	1 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def maxKelements(self, nums: List[int], k: int) -> int:  # TLE
        from math import ceil
        maximum = 0
        while nums.sort() or k:
            maximum, k = maximum + nums[-1], k - 1
            nums.append(ceil(nums.pop() / 3))
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 7999 ms
        from math import ceil
        import bisect

        maximum = int(bool(nums.sort()))
        while k and (n := nums.pop()):
            maximum, k = maximum + n, k - 1
            bisect.insort(nums, ceil(n / 3))
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 6717 ms
        from math import ceil
        import bisect
        from collections import Counter

        maximum, counts = 0, sorted(Counter(nums).items())
        while k > 0:
            n, ctr = counts.pop()
            maximum, k = maximum + n * min(k, ctr), k - ctr
            bisect.insort_left(counts, (ceil(n / 3), ctr))
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 1975 ms
        from math import ceil
        from sortedcontainers import SortedList

        nums = SortedList(nums)
        maximum = nums.pop()
        nums.add(ceil(maximum / 3))
        while (k := k - 1) and (n := nums.pop()):
            maximum += n
            nums.add(ceil(n / 3))
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 1362 ms
        from math import ceil
        from collections import Counter
        from sortedcontainers import SortedDict
        counts, maximum = SortedDict(Counter(nums)), 0
        while k:
            n, ctr = counts.popitem()
            n_taken = min(ctr, k)
            maximum += n * n_taken
            counts[ceil(n / 3)] = counts.get(ceil(n / 3), 0) + n_taken
            k -= n_taken
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 1294 ms
        from math import floor
        from collections import Counter
        import heapq

        heap, maximum = list(Counter(map(int.__neg__, nums)).items()), 0
        heapq.heapify(heap)
        while k > 0:
            n, ctr = heapq.heappop(heap)
            maximum -= n * min(k, ctr)
            heapq.heappush(heap, (floor(n / 3), ctr))
            k -= ctr
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 979 ms
        from math import floor
        import heapq
        heap, maximum = [-n for n in nums], 0
        heapq.heapify(heap)
        for _ in range(k):
            n = heapq.heappop(heap)
            maximum -= n
            heapq.heappush(heap, floor(n / 3))
        return maximum

    def maxKelements(self, nums: List[int], k: int) -> int:  # 931 ms
        import heapq
        heap, maximum = [-n for n in nums], 0
        heapq.heapify(heap)
        for _ in range(k):
            maximum -= heap[0]
            heapq.heapreplace(heap, heap[0] // 3)
        return maximum


def test():
    arguments = [
        ([10, 10, 10, 10, 10], 5),
        ([1, 10, 3, 3, 3], 3),
    ]
    expectations = [50, 17]
    for (nums, k), expected in zip(arguments, expectations):
        solution = Solution().maxKelements(nums, k)
        assert solution == expected
