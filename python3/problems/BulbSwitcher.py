### Source : https://leetcode.com/problems/bulb-switcher/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-06

#####################################################################################################
#
# There are n bulbs that are initially off. You first turn on all the bulbs, then you turn off every
# second bulb.
#
# On the third round, you toggle every third bulb (turning on if it's off or turning off if it's on).
# For the ith round, you toggle every i bulb. For the nth round, you only toggle the last bulb.
#
# Return the number of bulbs that are on after n rounds.
#
# Example 1:
#
# Input: n = 3
# Output: 1
# Explanation: At first, the three bulbs are [off, off, off].
# After the first round, the three bulbs are [on, on, on].
# After the second round, the three bulbs are [on, off, on].
# After the third round, the three bulbs are [on, off, off].
# So you should return 1 because there is only one bulb is on.
#
# Example 2:
#
# Input: n = 0
# Output: 0
#
# Example 3:
#
# Input: n = 1
# Output: 1
#
# Constraints:
#
# 	0 <= n <= 109
#####################################################################################################


class Solution:
    def bulbSwitch(self, n: int) -> int:  # O(log n) time and O(1) space
        left, right = 1, n
        while left <= right:
            mid = left + (right - left) // 2
            if mid * mid > n:
                right = mid - 1
            else:
                left = mid + 1
        return right

    # def bulbSwitch(self, n: int) -> int:  # O(1) time and O(1) space
    #     # 0: x
    #     # 1: o
    #     # 2: o x
    #     # 3: o x x
    #     # 4: o x x o
    #     # 5: o x x o x
    #     # 6: o x x o x x
    #     # 7: o x x o x x x
    #     # 9: o x x o x x x x o
    #     # only all bulbs that are a square numbers end up being on
    #     return int(n ** 0.5)


def test():
    arguments = [3, 0, 1]
    expectations = [1, 0, 1]
    for n, expected in zip(arguments, expectations):
        solution = Solution().bulbSwitch(n)
        assert solution == int(n ** 0.5)
test()