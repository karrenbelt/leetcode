### Source : https://leetcode.com/problems/reverse-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-03

##################################################################################################### 
#
# Given the head of a singly linked list, reverse the list, and return the reversed list.
# 
# Example 1:
# 
# Input: head = [1,2,3,4,5]
# Output: [5,4,3,2,1]
# 
# Example 2:
# 
# Input: head = [1,2]
# Output: [2,1]
# 
# Example 3:
# 
# Input: head = []
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the list is the range [0, 5000].
# 	-5000 <= Node.val <= 5000
# 
# Follow up: A linked list can be reversed either iteratively or recursively. Could you implement 
# both?
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def reverseList(self, head: ListNode) -> ListNode:  # O(n) time and O(n) space

        def solve(node: ListNode, prev=None) -> ListNode:
            if not node:
                return
            temp = node.next
            node.next = prev
            prev = node
            if not temp:
                return node
            node = temp
            return solve(node, prev)

        return solve(head)

    def reverseList(self, head: ListNode) -> ListNode:  # O(n) time and O(n) space

        def solve(node: ListNode, prev=None) -> ListNode:
            if node:
                next_node, prev, node.next = node.next, node, prev
                return solve(next_node, prev) if next_node else node

        return solve(head)

    def reverseList(self, head: ListNode) -> ListNode:  # O(n) time and O(1) space
        prev, node = None, head
        while node:
            temp = node
            node = node.next
            temp.next = prev
            prev = temp
        return prev

    def reverseList(self, head: ListNode) -> ListNode:  # O(n) time and O(1) space
        prev, node = None, head
        while node:
            next_node, prev, node.next = node.next, node, prev
            node = next_node
        return prev


def test():
    from python3 import SinglyLinkedList
    arrays_of_numbers = [
        [1, 2, 3, 4, 5],
        [1, 2],
        [],
        ]
    expectations = [
        [5, 4, 3, 2, 1],
        [2, 1],
        [],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().reverseList(SinglyLinkedList(nums).head)
        assert solution == SinglyLinkedList(expected).head
