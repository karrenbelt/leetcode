### Source : https://leetcode.com/problems/peeking-iterator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-08

##################################################################################################### 
#
# Given an Iterator class interface with methods: next() and hasNext(), design and implement a 
# PeekingIterator that support the peek() operation -- it essentially peek() at the element that will 
# be returned by the next call to next().
# 
# Example:
# 
# Assume that the iterator is initialized to the beginning of the list: [1,2,3].
# 
# Call next() gets you 1, the first element in the list.
# Now you call peek() and it returns 2, the next element. Calling next() after that still return 2. 
# You call next() the final time and it returns 3, the last element. 
# Calling hasNext() after that should return false.
# 
# Follow up: How would you extend your design to be generic and work with all types, not just integer?
#####################################################################################################


# Below is the interface for Iterator, which is already defined for you.
class Iterator:
    def __init__(self, nums):
        """
        Initializes an iterator object to the beginning of a list.
        :type nums: List[int]
        """
        self.nums = nums[::-1]

    def hasNext(self) -> bool:
        """
        Returns true if the iteration has more elements.
        :rtype: bool
        """
        return bool(self.nums)

    def next(self) -> int:
        """
        Returns the next element in the iteration.
        :rtype: int
        """
        return self.nums.pop()


class PeekingIterator:
    def __init__(self, iterator):
        """
        Initialize your data structure here.
        :type iterator: Iterator
        """
        self.iterator = iterator
        self._next = iterator.next() if iterator.hasNext() else None

    def peek(self):
        """
        Returns the next element in the iteration without advancing the iterator.
        :rtype: int
        """
        return self._next

    def next(self):
        """
        :rtype: int
        """
        data = self._next
        self._next = self.iterator.next() if self.iterator.hasNext() else None
        return data

    def hasNext(self):
        """
        :rtype: bool
        """
        return bool(self._next)


# Your PeekingIterator object will be instantiated and called as such:
# iter = PeekingIterator(Iterator(nums))
# while iter.hasNext():
#     val = iter.peek()   # Get the next element but not advance the iterator.
#     iter.next()         # Should return the same value as [val].

def test():
    operations = ["PeekingIterator", "next", "peek", "next", "next", "hasNext"]
    arguments = [[[1, 2, 3]], [], [], [], [], []]
    expectations = [None, 1, 2, 2, 3, False]
    obj = PeekingIterator(Iterator(*arguments[0]))
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
