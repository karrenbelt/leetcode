### Source : https://leetcode.com/problems/subsets/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# Given an integer array nums, return all possible subsets (the power set).
# 
# The solution set must not contain duplicate subsets.
# 
# Example 1:
# 
# Input: nums = [1,2,3]
# Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
# 
# Example 2:
# 
# Input: nums = [0]
# Output: [[],[0]]
# 
# Constraints:
# 
# 	1 <= nums.length <= 10
# 	-10 <= nums[i] <= 10
#####################################################################################################

from typing import List


class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        res = [[]]
        for num in nums:
            res += [item + [num] for item in res]
        return res

    def subsets(self, nums: List[int]) -> List[List[int]]:

        def dfs(nums, path, ret):
            ret.append(path)
            for i in range(len(nums)):
                dfs(nums[i + 1:], path + [nums[i]], ret)
            return ret

        return dfs(nums, [], [])


def test():
    arrays_of_numbers = [
        [1, 2, 3],
        [0],
        ]
    expectations = [
        [[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]],
        [[], [0]],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().subsets(nums)
        assert set(map(frozenset, solution)) == set(map(frozenset, expected))
