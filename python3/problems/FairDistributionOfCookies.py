### Source : https://leetcode.com/problems/fair-distribution-of-cookies/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-03

##################################################################################################### 
#
# You are given an integer array cookies, where cookies[i] denotes the number of cookies in the ith 
# bag. You are also given an integer k that denotes the number of children to distribute all the bags 
# of cookies to. All the cookies in the same bag must go to the same child and cannot be split up.
# 
# The unfairness of a distribution is defined as the maximum total cookies obtained by a single child 
# in the distribution.
# 
# Return the minimum unfairness of all distributions.
# 
# Example 1:
# 
# Input: cookies = [8,15,10,20,8], k = 2
# Output: 31
# Explanation: One optimal distribution is [8,15,8] and [10,20]
# - The 1st child receives [8,15,8] which has a total of 8 + 15 + 8 = 31 cookies.
# - The 2nd child receives [10,20] which has a total of 10 + 20 = 30 cookies.
# The unfairness of the distribution is max(31,30) = 31.
# It can be shown that there is no distribution with an unfairness less than 31.
# 
# Example 2:
# 
# Input: cookies = [6,1,3,2,2,4,1,2], k = 3
# Output: 7
# Explanation: One optimal distribution is [6,1], [3,2,2], and [4,1,2]
# - The 1st child receives [6,1] which has a total of 6 + 1 = 7 cookies.
# - The 2nd child receives [3,2,2] which has a total of 3 + 2 + 2 = 7 cookies.
# - The 3rd child receives [4,1,2] which has a total of 4 + 1 + 2 = 7 cookies.
# The unfairness of the distribution is max(7,7,7) = 7.
# It can be shown that there is no distribution with an unfairness less than 7.
# 
# Constraints:
# 
# 	2 <= cookies.length <= 8
# 	1 <= cookies[i] <= 105
# 	2 <= k <= cookies.length
#####################################################################################################

from typing import List


class Solution:
    def distributeCookies(self, cookies: List[int], k: int) -> int:

        def binary_search(left: int, right: int, fn: callable) -> int:
            while left < right:
                mid = left + right >> 1
                left, right = (mid + 1, right) if fn(mid) else (left, mid)
            return left

        def condition(limit: int) -> bool:

            def dfs(i: int) -> bool:
                if i == len(cookies):
                    return True
                for j in range(k):
                    if bags[j] + cookies[i] <= limit:
                        bags[j] += cookies[i]
                        if dfs(i + 1):
                            return True
                        bags[j] -= cookies[i]
                return False

            bags = [0] * k
            return not dfs(0)

        return binary_search(max(cookies), sum(cookies), condition)

    def distributeCookies(self, cookies: List[int], k: int) -> int:
        from functools import lru_cache

        @lru_cache
        def dfs(mask: int, i: int) -> int:
            if mask == 0:
                return 0
            if i == 0:
                return 1 << 31
            ans = 1 << 31
            n = mask
            while mask:
                mask = n & (mask - 1)
                amt = sum(cookies[i] for i in range(len(cookies)) if (n ^ mask) & 1 << i)
                ans = min(ans, max(amt, dfs(mask, i - 1)))
            return ans

        return dfs((1 << len(cookies)) - 1, k)


def test():
    arguments = [
        ([8, 15, 10, 20, 8], 2),
        ([6, 1, 3, 2, 2, 4, 1, 2], 3),
    ]
    expectations = [31, 7]
    for (cookies, k), expected in zip(arguments, expectations):
        solution = Solution().distributeCookies(cookies, k)
        assert solution == expected
