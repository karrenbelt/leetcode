### Source : https://leetcode.com/problems/find-bottom-left-tree-value/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-13

##################################################################################################### 
#
# Given the root of a binary tree, return the leftmost value in the last row of the tree.
# 
# Example 1:
# 
# Input: root = [2,1,3]
# Output: 1
# 
# Example 2:
# 
# Input: root = [1,2,3,4,null,5,6,null,null,7]
# Output: 7
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 104].
# 	-231 <= Node.val <= 231 - 1
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def findBottomLeftValue(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, level: int):
            if not node:
                return -1, -1
            if node.left is node.right:
                return node.val, level
            left, left_level = traverse(node.left, level + 1)
            right, right_level = traverse(node.right, level + 1)
            return (left, left_level) if left_level >= right_level else (right, right_level)

        return traverse(root, 0)[0]

    def findBottomLeftValue(self, root: TreeNode) -> int:
        queue = [root]
        for node in queue:
            queue += filter(None, (node.right, node.left))
        return node.val


def test():
    from python3 import Codec
    arguments = [
        "[2,1,3]",
        "[1,2,3,4,null,5,6,null,null,7]",
    ]
    expectations = [1, 7]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().findBottomLeftValue(root)
        assert solution == expected, (solution, expected)
