### Source : https://leetcode.com/problems/reverse-words-in-a-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given an input string, reverse the string word by word.
# 
# Example 1:
# 
# Input: "the sky is blue"
# Output: "blue is sky the"
# 
# Example 2:
# 
# Input: "  hello world!  "
# Output: "world! hello"
# Explanation: Your reversed string should not contain leading or trailing spaces.
# 
# Example 3:
# 
# Input: "a good   example"
# Output: "example good a"
# Explanation: You need to reduce multiple spaces between two words to a single space in the reversed 
# string.
# 
# Note:
# 
# 	A word is defined as a sequence of non-space characters.
# 	Input string may contain leading or trailing spaces. However, your reversed string should 
# not contain leading or trailing spaces.
# 	You need to reduce multiple spaces between two words to a single space in the reversed 
# string.
# 
# Follow up:
# 
# For C programmers, try to solve it in-place in O(1) extra space.
#####################################################################################################


class Solution:
    def reverseWords(self, s: str) -> str:
        return ' '.join([w for w in s.strip().split()][::-1])

    def reverseWords(self, s: str) -> str:  # O(n) time and O(1) space
        return ' '.join(reversed(s.split()))


def test():
    strings = [
        "the sky is blue",
        "  hello world  ",
        "a good   example",
        "  Bob    Loves  Alice   ",
        "Alice does not even like bob",
        ]
    expectations = [
        "blue is sky the",
        "world hello",
        "example good a",
        "Alice Loves Bob",
        "bob like even not does Alice",
        ]
    for s, expected in zip(strings, expectations):
        solution = Solution().reverseWords(s)
        assert solution == expected
