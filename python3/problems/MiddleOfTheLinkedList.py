### Source : https://leetcode.com/problems/middle-of-the-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Given a non-empty, singly linked list with head node head, return a middle node of linked list.
# 
# If there are two middle nodes, return the second middle node.
# 
# Example 1:
# 
# Input: [1,2,3,4,5]
# Output: Node 3 from this list (Serialization: [3,4,5])
# The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).
# Note that we returned a ListNode object ans, such that:
# ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
# 
# Example 2:
# 
# Input: [1,2,3,4,5,6]
# Output: Node 4 from this list (Serialization: [4,5,6])
# Since the list has two middle nodes with values 3 and 4, we return the second one.
# 
# Note:
# 
# 	The number of nodes in the given list will be between 1 and 100.
# 
#####################################################################################################

from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def middleNode(self, head: ListNode) -> ListNode:  # O(n) time and O(n) space

        def ravel(node: ListNode):
            if node:
                values.append(node.val)
                return ravel(node.next)

        values = []
        li = ravel(head)
        idx = len(li) // 2
        node = head
        for i in range(idx):
            node = node.next
        return node

    def middleNode(self, head: ListNode) -> ListNode:  # O(n) time and O(1) space
        node, i = head, 0
        while node:
            node, i = node.next, i + 1
        node = head
        while i >> 1:
            node, i = node.next, i - 2
        return node

    def middleNode(self, head: ListNode) -> ListNode:  # O(n) time and O(1) space
        slow = fast = head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
        return slow


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [1, 2, 3, 4, 5],
        [1, 2, 3, 4, 5, 6],
    ]
    expectations = [3, 4]  # NOTE: need to have the node from the list with the same identity
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().middleNode(head)
        middle_node = list(head)[nums.index(expected)]
        assert solution == middle_node
