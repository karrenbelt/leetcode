### Source : https://leetcode.com/problems/third-maximum-number/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-06

##################################################################################################### 
#
# Given integer array nums, return the third maximum number in this array. If the third maximum does 
# not exist, return the maximum number.
# 
# Example 1:
# 
# Input: nums = [3,2,1]
# Output: 1
# Explanation: The third maximum is 1.
# 
# Example 2:
# 
# Input: nums = [1,2]
# Output: 2
# Explanation: The third maximum does not exist, so the maximum (2) is returned instead.
# 
# Example 3:
# 
# Input: nums = [2,2,3,1]
# Output: 1
# Explanation: Note that the third maximum here means the third maximum distinct number.
# Both numbers with value 2 are both considered as second maximum.
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	-231 <= nums[i] <= 231 - 1
# 
# Follow up: Can you find an O(n) solution?
#####################################################################################################

from typing import List


class Solution:
    def thirdMax(self, nums: List[int]) -> int:
        nums = sorted(set(nums))
        if len(nums) < 3:
            return max(nums)
        return nums[-3]

    def thirdMax(self, nums: List[int]) -> int:
        import bisect
        l = []
        for n in set(nums):
            bisect.insort(l, -n)
            if len(l) > 3:
                l.pop()
        return -l[2] if len(l) > 2 else -l[0]

    def thirdMax(self, nums: List[int]) -> int:
        nums = set(nums)
        for _ in range((2, 0)[len(nums) <= 2]):
            nums.remove(max(nums))
        return max(nums)


def test():
    arrays_of_numbers = [
        [3, 2, 1],
        [1, 2],
        [2, 2, 3, 1]
        ]
    expectations = [1, 2, 1]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().thirdMax(nums)
        assert solution == expected

