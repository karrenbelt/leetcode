### Source : https://leetcode.com/problems/zigzag-conversion/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-16

##################################################################################################### 
#
# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: 
# (you may want to display this pattern in a fixed font for better legibility)
# 
# P   A   H   N
# A P L S I I G
# Y   I   R
# 
# And then read line by line: "PAHNAPLSIIGYIR"
# 
# Write the code that will take a string and make this conversion given a number of rows:
# 
# string convert(string s, int numRows);
# 
# Example 1:
# 
# Input: s = "PAYPALISHIRING", numRows = 3
# Output: "PAHNAPLSIIGYIR"
# 
# Example 2:
# 
# Input: s = "PAYPALISHIRING", numRows = 4
# Output: "PINALSIGYAHRPI"
# Explanation:
# P     I    N
# A   L S  I G
# Y A   H R
# P     I
# 
# Example 3:
# 
# Input: s = "A", numRows = 1
# Output: "A"
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s consists of English letters (lower-case and upper-case), ',' and '.'.
# 	1 <= numRows <= 1000
#####################################################################################################


class Solution:
    def convert(self, s: str, numRows: int) -> str:
        # 2 -> 1. every 2 starting from 0;
        #      2. every 2 starting from 1;
        #
        # 3 -> 1. every 5 starting from 0;
        #      2. every 2 starting from 1;
        #      3. every 5 starting from 2;
        #
        # 4 -> 1. every 7 starting from 0;
        #      2. every alternating 5 and 3, starting from 1;
        #      3. every alternating 3 and 5, starting from 2;
        #      4. every 7 starting from 4;
        if numRows == 1:
            return s
        cycle = 2 * (numRows - 1)
        d = {i: i if i < numRows else cycle - i for i in range(cycle)}
        ans = ["" for _ in range(numRows)]
        for i, c in enumerate(s):
            ans[d[i % cycle]] += c
        return "".join(ans)

    def convert(self, s: str, numRows: int) -> str:  # O(n)
        if numRows == 1:
            return s
        ans = ["" for _ in range(numRows)]
        row = 0
        increase = -1
        for i, c in enumerate(s):
            if i % (numRows - 1) == 0:
                increase *= -1
            ans[row] += c
            row += increase
        return "".join(ans)

    def convert(self, s: str, numRows: int) -> str:  # O(n)
        step = (numRows == 1) - 1
        rows, idx = [''] * numRows, 0
        for c in s:
            rows[idx] += c
            if idx == 0 or idx == numRows-1:
                step *= -1
            idx += step
        return ''.join(rows)

    def convert(self, s: str, numRows: int) -> str:  # O(n)
        from itertools import chain, cycle
        idx = chain(range(numRows), reversed(range(1, numRows - 1)))
        matrix = [[] for _ in range(numRows)]
        for c, i in zip(s, cycle(idx)):
            matrix[i].append(c)
        return "".join(map("".join, matrix))


def test():
    strings = ["PAYPALISHIRING", "PAYPALISHIRING"]
    rows = [3, 4]
    expectations = ["PAHNAPLSIIGYIR", "PINALSIGYAHRPI"]
    for s, numRows, expected in zip(strings, rows, expectations):
        solution = Solution().convert(s, numRows)
        assert solution == expected
