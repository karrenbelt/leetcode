### Source : https://leetcode.com/problems/sort-colors/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-15

##################################################################################################### 
#
# Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects 
# of the same color are adjacent, with the colors in the order red, white, and blue.
# 
# We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.
# 
# Example 1:
# Input: nums = [2,0,2,1,1,0]
# Output: [0,0,1,1,2,2]
# Example 2:
# Input: nums = [2,0,1]
# Output: [0,1,2]
# Example 3:
# Input: nums = [0]
# Output: [0]
# Example 4:
# Input: nums = [1]
# Output: [1]
# 
# Constraints:
# 
# 	n == nums.length
# 	1 <= n <= 300
# 	nums[i] is 0, 1, or 2.
# 
# Follow up:
# 
# 	Could you solve this problem without using the library's sort function?
# 	Could you come up with a one-pass algorithm using only O(1) constant space?
#####################################################################################################

from typing import List


class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        nums.sort()

    def sortColors(self, nums: List[int]) -> None:  # O(n log n) time for best anv average and O(n^2) worst case time

        def qsort(lst, lo, hi):
            if lo < hi:
                p = partition(lst, lo, hi)
                qsort(lst, lo, p - 1)
                qsort(lst, p + 1, hi)

        def partition(lst, lo, hi):
            pivot = lst[hi]
            i = lo
            for j in range(lo, hi):
                if lst[j] < pivot:
                    lst[i], lst[j] = lst[j], lst[i]
                    i += 1
            lst[i], lst[hi] = lst[hi], lst[i]
            return i  # index of pivot value in the sorted array

        qsort(nums, 0, len(nums) - 1)
        return nums  # sorted in-place

    def sortColors(self, nums):  # O(n) time and O(1) space
        # https://en.wikipedia.org/wiki/Dutch_national_flag_problem
        red, white, blue = 0, 0, len(nums) - 1
        while white <= blue:
            if nums[white] == 0:
                nums[red], nums[white] = nums[white], nums[red]
                white += 1
                red += 1
            elif nums[white] == 1:
                white += 1
            else:
                nums[white], nums[blue] = nums[blue], nums[white]
                blue -= 1

    def sortColors(self, nums: List[int]) -> None:  # bucket sort
        buckets = [0 for _ in range(3)]
        for num in nums:
            buckets[num] += 1
        idx = 0
        for color, freq in enumerate(buckets):
            nums[idx:idx + freq] = [color] * freq
            idx += freq

    def sortColors(self, nums, k=3):  # generalized: O(n) time and O(1) space (constant extra space for padding)
        start = [0] * (k + 1)  # some padding
        for n in nums:
            for i in reversed(range(n, k)):
                nums[start[i + 1]] = i
                start[i + 1] += 1


def test():
    arguments = [
        [2, 0, 2, 1, 1, 0],
        [2, 0, 1],
        [0],
        [1],
        ]
    expectations = [
        [0, 0, 1, 1, 2, 2],
        [0, 1, 2],
        [0],
        [1],
        ]
    for nums, expected in zip(arguments, expectations):
        Solution().sortColors(nums)
        assert nums == expected
