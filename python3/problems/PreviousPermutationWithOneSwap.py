### Source : https://leetcode.com/problems/previous-permutation-with-one-swap/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-18

##################################################################################################### 
#
# Given an array of positive integers arr (not necessarily distinct), return the lexicographically 
# largest permutation that is smaller than arr, that can be made with exactly one swap (A swap 
# exchanges the positions of two numbers arr[i] and arr[j]). If it cannot be done, then return the 
# same array.
# 
# Example 1:
# 
# Input: arr = [3,2,1]
# Output: [3,1,2]
# Explanation: Swapping 2 and 1.
# 
# Example 2:
# 
# Input: arr = [1,1,5]
# Output: [1,1,5]
# Explanation: This is already the smallest permutation.
# 
# Example 3:
# 
# Input: arr = [1,9,4,6,7]
# Output: [1,7,4,6,9]
# Explanation: Swapping 9 and 7.
# 
# Example 4:
# 
# Input: arr = [3,1,1,3]
# Output: [1,3,1,3]
# Explanation: Swapping 1 and 3.
# 
# Constraints:
# 
# 	1 <= arr.length <= 104
# 	1 <= arr[i] <= 104
#####################################################################################################

from typing import List


class Solution:
    def prevPermOpt1(self, arr: List[int]) -> List[int]:  # O(n) time and O(1) space
        # find the first position on the right that is not sorted
        i = len(arr) - 2
        while i >= 0 and arr[i] <= arr[i + 1]:
            i -= 1
        # find the first largest number on the right that is less than arr[i]
        if i >= 0:
            j = i + 1
            for k in range(j + 1, len(arr)):
                if arr[j] < arr[k] < arr[i]:  # ensure you skip duplicate numbers
                    j = k
            # swap the two indices
            arr[j], arr[i] = arr[i], arr[j]
        return arr

    def prevPermOpt1(self, arr: List[int]) -> List[int]:  # O(n) time and O(1) space
        i = len(arr) - 2
        while i >= 0:
            if arr[i] > arr[i + 1]:
                pivot = j = i + 1
                while j < len(arr) and arr[i] > arr[j]:
                    if arr[j] > arr[pivot]:
                        pivot = j
                    j += 1
                arr[i], arr[pivot] = arr[pivot], arr[i]
                break
            i -= 1
        return arr


def test():
    arguments = [
        [3, 2, 1],
        [1, 1, 5],
        [1, 9, 4, 6, 7],
        [3, 1, 1, 3],
    ]
    expectations = [
        [3, 1, 2],
        [1, 1, 5],
        [1, 7, 4, 6, 9],
        [1, 3, 1, 3],
    ]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().prevPermOpt1(arr)
        assert solution == expected, solution
