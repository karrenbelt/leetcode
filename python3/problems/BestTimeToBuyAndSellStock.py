### Source : https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-03

##################################################################################################### 
#
# You are given an array prices where prices[i] is the price of a given stock on the ith day.
# 
# You want to maximize your profit by choosing a single day to buy one stock and choosing a different 
# day in the future to sell that stock.
# 
# Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, 
# return 0.
# 
# Example 1:
# 
# Input: prices = [7,1,5,3,6,4]
# Output: 5
# Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
# Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.
# 
# Example 2:
# 
# Input: prices = [7,6,4,3,1]
# Output: 0
# Explanation: In this case, no transactions are done and the max profit = 0.
# 
# Constraints:
# 
# 	1 <= prices.length <= 105
# 	0 <= prices[i] <= 104
#####################################################################################################

from typing import List


class Solution:

    def maxProfit(self, prices: List[int]) -> int:
        max_profit, min_price = 0, prices[0]
        for price in prices:
            if price < min_price:
                min_price = price
            if max_profit < price - min_price:
                max_profit = price - min_price
        return max_profit

    def maxProfit(self, prices: List[int]) -> int:
        profit, min_price = 0, prices[0]
        for price in prices:
            min_price = min(min_price, price)
            profit = max(profit, price - min_price)
        return profit

    def maxProfit(self, prices: List[int]) -> int:  # kadane's algorithm
        margins = [b - a for a, b in zip(prices, prices[1:])]
        for i in range(1, len(margins)):
            if margins[i - 1] > 0:
                margins[i] += margins[i - 1]
        return max(margins, default=0)

    def maxProfit(self, prices: List[int]) -> int:  # kadane's algorithm
        best = curr = 0
        for i in range(1, len(prices)):
            curr = max(0, curr + prices[i] - prices[i - 1])
            best = max(best, curr)
        return best


def test():
    arrays_of_numbers = [
        [7, 1, 5, 3, 6, 4],
        [7, 6, 4, 3, 1],
        ]
    expectations = [5, 0]
    for prices, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().maxProfit(prices)
        assert solution == expected
