### Source : https://leetcode.com/problems/combination-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-23

##################################################################################################### 
#
# Given an array of distinct integers candidates and a target integer target, return a list of all 
# unique combinations of candidates where the chosen numbers sum to target. You may return the 
# combinations in any order.
# 
# The same number may be chosen from candidates an unlimited number of times. Two combinations are 
# unique if the frequency of at least one of the chosen numbers is different.
# 
# It is guaranteed that the number of unique combinations that sum up to target is less than 150 
# combinations for the given input.
# 
# Example 1:
# 
# Input: candidates = [2,3,6,7], target = 7
# Output: [[2,2,3],[7]]
# Explanation:
# 2 and 3 are candidates, and 2 + 2 + 3 = 7. Note that 2 can be used multiple times.
# 7 is a candidate, and 7 = 7.
# These are the only two combinations.
# 
# Example 2:
# 
# Input: candidates = [2,3,5], target = 8
# Output: [[2,2,2,2],[2,3,3],[3,5]]
# 
# Example 3:
# 
# Input: candidates = [2], target = 1
# Output: []
# 
# Example 4:
# 
# Input: candidates = [1], target = 1
# Output: [[1]]
# 
# Example 5:
# 
# Input: candidates = [1], target = 2
# Output: [[1,1]]
# 
# Constraints:
# 
# 	1 <= candidates.length <= 30
# 	1 <= candidates[i] <= 200
# 	All elements of candidates are distinct.
# 	1 <= target <= 500
#####################################################################################################

from typing import List


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:

        def backtrack(target, index, path):
            if target < 0:
                return
            if target == 0:
                ans.append(path)
                return
            for i in range(index, len(candidates)):
                backtrack(target - candidates[i], i, path + [candidates[i]])

        ans = []
        backtrack(target, 0, [])
        return ans

    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:  # dp using tabulation
        candidates.sort()
        dp = [[[]]] + [[] for _ in range(target)]
        for i in range(1, target + 1):
            for number in candidates:
                if number > i:
                    break
                for L in dp[i - number]:
                    if not L or number >= L[-1]:
                        dp[i] += L + [number],
        return dp[target]


def test():
    arrays_of_numbers = [
        [2, 3, 6, 7],
        [2, 3, 5],
        [2],
        [1],
        [1],
        ]
    targets = [7, 8, 1, 1, 2]
    expectations = [
        [[2, 2, 3], [7]],
        [[2, 2, 2, 2], [2, 3, 3], [3, 5]],
        [],
        [[1]],
        [[1, 1]]
        ]
    for candidates, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().combinationSum(candidates, target)
        assert solution == expected
