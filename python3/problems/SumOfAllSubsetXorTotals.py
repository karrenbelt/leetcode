### Source : https://leetcode.com/problems/sum-of-all-subset-xor-totals/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-03

##################################################################################################### 
#
# The XOR total of an array is defined as the bitwise XOR of all its elements, or 0 if the array is 
# empty.
# 
# 	For example, the XOR total of the array [2,5,6] is 2 XOR 5 XOR 6 = 1.
# 
# Given an array nums, return the sum of all XOR totals for every subset of nums. 
# 
# Note: Subsets with the same elements should be counted multiple times.
# 
# An array a is a subset of an array b if a can be obtained from b by deleting some (possibly zero) 
# elements of b.
# 
# Example 1:
# 
# Input: nums = [1,3]
# Output: 6
# Explanation: The 4 subsets of [1,3] are:
# - The empty subset has an XOR total of 0.
# - [1] has an XOR total of 1.
# - [3] has an XOR total of 3.
# - [1,3] has an XOR total of 1 XOR 3 = 2.
# 0 + 1 + 3 + 2 = 6
# 
# Example 2:
# 
# Input: nums = [5,1,6]
# Output: 28
# Explanation: The 8 subsets of [5,1,6] are:
# - The empty subset has an XOR total of 0.
# - [5] has an XOR total of 5.
# - [1] has an XOR total of 1.
# - [6] has an XOR total of 6.
# - [5,1] has an XOR total of 5 XOR 1 = 4.
# - [5,6] has an XOR total of 5 XOR 6 = 3.
# - [1,6] has an XOR total of 1 XOR 6 = 7.
# - [5,1,6] has an XOR total of 5 XOR 1 XOR 6 = 2.
# 0 + 5 + 1 + 6 + 4 + 3 + 7 + 2 = 28
# 
# Example 3:
# 
# Input: nums = [3,4,5,6,7,8]
# Output: 480
# Explanation: The sum of all XOR totals for every subset is 480.
# 
# Constraints:
# 
# 	1 <= nums.length <= 12
# 	1 <= nums[i] <= 20
#####################################################################################################

from typing import List


class Solution:
    def subsetXORSum(self, nums: List[int]) -> int:

        def backtracking(i: int, xor_sum: int) -> int:
            if i == len(nums):
                return xor_sum
            return backtracking(i + 1, xor_sum) + backtracking(i + 1, xor_sum ^ nums[i])

        return backtracking(0, 0)

    def subsetXORSum(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        bits = 0
        for n in nums:
            bits |= n
        return bits * 2 ** (len(nums) - 1)

    def subsetXORSum(self, nums: List[int]) -> int:  # O(n) time and O(n) space  (doesn't work on leetcode)
        return (bits := 0) or list((bits := bits | n for n in nums))[-1] * 2 ** (len(nums) - 1)

    def subsetXORSum(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        from functools import reduce
        return reduce(lambda x, y: x | y, nums) * 2 ** (len(nums) - 1)


def test():
    arguments = [
        [1, 3],
        [5, 1, 6],
        [3, 4, 5, 6, 7, 8],
    ]
    expectations = [6, 28, 480]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().subsetXORSum(nums)
        assert solution == expected, (solution, expected)
