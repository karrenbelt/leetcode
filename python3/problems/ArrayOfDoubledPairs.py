### Source : https://leetcode.com/problems/array-of-doubled-pairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-11

##################################################################################################### 
#
# Given an array of integers arr of even length, return true if and only if it is possible to reorder 
# it such that arr[2 * i + 1] = 2 * arr[2 * i] for every 0 <= i < len(arr) / 2.
# 
# Example 1:
# 
# Input: arr = [3,1,3,6]
# Output: false
# 
# Example 2:
# 
# Input: arr = [2,1,2,6]
# Output: false
# 
# Example 3:
# 
# Input: arr = [4,-2,2,-4]
# Output: true
# Explanation: We can take two groups, [-2,-4] and [2,4] to form [-2,-4,2,4] or [2,4,-2,-4].
# 
# Example 4:
# 
# Input: arr = [1,2,4,16,8,4]
# Output: false
# 
# Constraints:
# 
# 	0 <= arr.length <= 3 * 104
# 	arr.length is even.
# 	-105 <= arr[i] <= 105
#####################################################################################################

from typing import List


class Solution:
    def canReorderDoubled(self, arr: List[int]) -> bool:  # O(n log n)
        from collections import Counter

        counts = Counter(arr)
        for n in sorted(counts, key=abs):
            if counts[n] > counts[2 * n]:
                return False
            counts[2 * n] -= counts[n]
        return True


def test():
    arguments = [
        [3, 1, 3, 6],
        [2, 1, 2, 6],
        [4, -2, 2, -4],
        [1, 2, 4, 16, 8, 4],
    ]
    expectations = [False, False, True, False]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().canReorderDoubled(arr)
        assert solution == expected
