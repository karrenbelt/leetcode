### Source : https://leetcode.com/problems/champagne-tower/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# We stack glasses in a pyramid, where the first row has 1 glass, the second row has 2 glasses, and 
# so on until the 100th row.  Each glass holds one cup of champagne.
# 
# Then, some champagne is poured into the first glass at the top.  When the topmost glass is full, 
# any excess liquid poured will fall equally to the glass immediately to the left and right of it.  
# When those glasses become full, any excess champagne will fall equally to the left and right of 
# those glasses, and so on.  (A glass at the bottom row has its excess champagne fall on the floor.)
# 
# For example, after one cup of champagne is poured, the top most glass is full.  After two cups of 
# champagne are poured, the two glasses on the second row are half full.  After three cups of 
# champagne are poured, those two cups become full - there are 3 full glasses total now.  After four 
# cups of champagne are poured, the third row has the middle glass half full, and the two outside 
# glasses are a quarter full, as pictured below.
# 
# Now after pouring some non-negative integer cups of champagne, return how full the jth glass in the 
# ith row is (both i and j are 0-indexed.)
# 
# Example 1:
# 
# Input: poured = 1, query_row = 1, query_glass = 1
# Output: 0.00000
# Explanation: We poured 1 cup of champange to the top glass of the tower (which is indexed as (0, 
# 0)). There will be no excess liquid so all the glasses under the top glass will remain empty.
# 
# Example 2:
# 
# Input: poured = 2, query_row = 1, query_glass = 1
# Output: 0.50000
# Explanation: We poured 2 cups of champange to the top glass of the tower (which is indexed as (0, 
# 0)). There is one cup of excess liquid. The glass indexed as (1, 0) and the glass indexed as (1, 1) 
# will share the excess liquid equally, and each will get half cup of champange.
# 
# Example 3:
# 
# Input: poured = 100000009, query_row = 33, query_glass = 17
# Output: 1.00000
# 
# Constraints:
# 
# 	0 <= poured <= 109
# 	0 <= query_glass <= query_row < 100
#####################################################################################################


class Solution:
    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        from collections import deque
        d = query_row - query_glass
        q = deque([(0, 0, float(poured))])
        while q:
            r, c, w = q.popleft()
            if r == query_row and c == query_glass:
                return min(1.0, w)
            if w > 1.0 and (r - d) <= c <= query_glass:
                w = (w - 1.0) / 2.0
                if q and q[-1][1] == c:
                    q[-1][2] += w
                else:
                    q.append((r + 1, c, w))
                q.append((r + 1, c + 1, w))
        return 0.0

    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:  # dfs with memo, actually dp

        def dfs(r, c):
            if (r, c) in d:
                return d[(r, c)]
            res = 0
            if r != c:
                res += max(0.0, dfs(r - 1, c) - 1.0) / 2.0
            if c:
                res += max(0.0, dfs(r - 1, c - 1) - 1.0) / 2.0
            d[(r, c)] = res
            return res

        d = {(0, 0): poured}
        return min(dfs(query_row, query_glass), 1.0)

    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:  # O(n^2) time
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dp(r, c):
            if not 0 <= c <= r:
                return 0
            elif r == 0 and c == 0:
                return poured
            return max(dp(r - 1, c - 1) - 1, 0) / 2 + max(dp(r - 1, c) - 1, 0) / 2

        return min(1.0, dp(query_row, query_glass))

    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:  # O(n^2) time and O(n^2) space
        dp = [[0] * n for n in range(1, query_row + 2)]
        dp[0][0] = poured
        for i in range(query_row):
            for j in range(len(dp[i])):
                tmp = (dp[i][j] - 1) / 2
                if tmp > 0:
                    dp[i + 1][j] += tmp
                    dp[i + 1][j + 1] += tmp
        return dp[query_row][query_glass] if dp[query_row][query_glass] <= 1 else 1

    def champagneTower(self, poured: int, query_row: int, query_glass: int) -> float:
        res = [poured] + [0] * query_row
        for row in range(1, query_row + 1):
            for i in range(row, -1, -1):
                res[i] = max(res[i] - 1, 0) / 2.0 + max(res[i - 1] - 1, 0) / 2.0
        return min(res[query_glass], 1)


def test():
    triplets_of_numbers = [
        (1, 1, 1),
        (2, 1, 1),
        (100000009, 33, 17),
        ]
    expectations = [0.0, 0.5, 1.0]
    for (poured, query_row, query_glass), expected in zip(triplets_of_numbers, expectations):
        solution = Solution().champagneTower(poured, query_row, query_glass)
        assert solution == expected
