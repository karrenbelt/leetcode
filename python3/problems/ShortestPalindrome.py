### Source : https://leetcode.com/problems/shortest-palindrome/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# You are given a string s. You can convert s to a palindrome by adding characters in front of it.
# 
# Return the shortest palindrome you can find by performing this transformation.
# 
# Example 1:
# Input: s = "aacecaaa"
# Output: "aaacecaaa"
# Example 2:
# Input: s = "abcd"
# Output: "dcbabcd"
# 
# Constraints:
# 
# 	0 <= s.length <= 5 * 104
# 	s consists of lowercase English letters only.
#####################################################################################################


class Solution:
    def shortestPalindrome(self, s: str) -> str:  # O(n^2) time
        for i in range(len(s), -1, -1):
            if s[:i][::-1] == s[:i]:
                return s[i:][::-1] + s

    def shortestPalindrome(self, s: str) -> str:  # O(n^2) time
        r = s[::-1]  # much better, almost 10 times faster; no repeated reversing operation
        for i in range(len(s) + 1):
            if s.startswith(r[i:]):
                return r[:i] + s


def test():
    arguments = ["aacecaaa", "abcd", "ababbbabbaba"]
    expectations = ["aaacecaaa", "dcbabcd", "ababbabbbababbbabbaba"]
    for s, expected in zip(arguments, expectations):
        solution = Solution().shortestPalindrome(s)
        assert solution == expected
