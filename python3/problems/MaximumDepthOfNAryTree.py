### Source : https://leetcode.com/problems/maximum-depth-of-n-ary-tree/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a n-ary tree, find its maximum depth.
# 
# The maximum depth is the number of nodes along the longest path from the root node down to the 
# farthest leaf node.
# 
# For example, given a 3-ary tree:
# 
# We should return its max depth, which is 3.
# 
# Note:
# 
# 	The depth of the tree is at most 1000.
# 	The total number of nodes is at most 5000.
# 
#####################################################################################################

from python3 import NAryTreeNode as Node


class Solution:
    def maxDepth(self, root: 'Node') -> int:  # iteration: O(n) time and (n) space
        return 0 if not root else 1 if not root.children else max(self.maxDepth(child) for child in root.children) + 1

    def maxDepth(self, root: 'Node') -> int:  # dfs: O(n) time and O(n) space
        max_depth = 0
        stack = [(root, 1)]
        while stack:
            node, depth = stack.pop()
            if node is not None:
                max_depth = max(max_depth, depth)
                stack.extend((child, depth + 1) for child in node.children)
        return max_depth if root else 0

    def maxDepth(self, root: 'Node') -> int:  # bfs: O(n) time and O(n) space
        from collections import deque

        depth = 1
        queue = deque([(root, depth)])
        while queue:
            node, depth = queue.popleft()
            if node and node.children:
                for child in node.children:
                    queue.append((child, depth + 1))
        return depth if root else 0


def test():
    from python3 import NAryTreeCodec
    arguments = [
        "[1,null,3,2,4,null,5,6]",
        "[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]",
        ]
    expectations = [3, 5]
    for serialized_tree, expected in zip(arguments, expectations):
        root = NAryTreeCodec.deserialize(serialized_tree)
        solution = Solution().maxDepth(root)
        assert solution == expected
