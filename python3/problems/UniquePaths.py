### Source : https://leetcode.com/problems/unique-paths/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-16

##################################################################################################### 
#
# A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
# 
# The robot can only move either down or right at any point in time. The robot is trying to reach the 
# bottom-right corner of the grid (marked 'Finish' in the diagram below).
# 
# How many possible unique paths are there?
# 
# Example 1:
# 
# Input: m = 3, n = 7
# Output: 28
# 
# Example 2:
# 
# Input: m = 3, n = 2
# Output: 3
# Explanation:
# From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
# 1. Right -> Down -> Down
# 2. Down -> Down -> Right
# 3. Down -> Right -> Down
# 
# Example 3:
# 
# Input: m = 7, n = 3
# Output: 28
# 
# Example 4:
# 
# Input: m = 3, n = 3
# Output: 6
# 
# Constraints:
# 
# 	1 <= m, n <= 100
# 	It's guaranteed that the answer will be less than or equal to 2 * 109.
#####################################################################################################


class Solution:
    # combinations, we need m-1 and n-1 steps in directions m and n, in any order.
    def uniquePaths(self, m: int, n: int) -> int:

        def factorial(x) -> int:
            if x in d:
                return d[x]
            d[x] = x * factorial(x - 1)
            return d[x]

        d = {0: 1, 1: 1, 2: 2}
        return factorial(m + n - 2) // (factorial(n - 1) * factorial(m - 1))

    def uniquePaths(self, m, n):  # O(mn) time and O(mn) space
        dp = [[1] * n for _ in range(m)]
        for i in range(1, m):
            for j in range(1, n):
                dp[i][j] = dp[i-1][j] + dp[i][j-1]
        return dp[-1][-1]

    def uniquePaths(self, m: int, n: int) -> int:  # O(n) space
        dp = [1] * n
        for _ in range(1, m):
            for j in range(1, n):
                dp[j] += dp[j - 1]
        return dp[-1] if m and n else 0

    def uniquePaths(self, m: int, n: int) -> int:  # inefficient since no memoization
        from functools import reduce
        return 1 if 1 in (n, m) else reduce(lambda x, y: x*y, range(n, n+m-1)) // reduce(lambda x, y: x*y, range(1, m))


def test():
    dimensions = [
        (3, 7),
        (3, 2),
        (7, 3),
        (3, 3),
        (4, 4),
        ]
    expectations = [28, 3, 28, 6, 20]
    for (m, n), expected in zip(dimensions, expectations):
        solution = Solution().uniquePaths(m, n)
        assert solution == expected
test()