### Source : https://leetcode.com/problems/count-complete-tree-nodes/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-24

##################################################################################################### 
#
# Given the root of a complete binary tree, return the number of the nodes in the tree.
# 
# According to Wikipedia, every level, except possibly the last, is completely filled in a complete 
# binary tree, and all nodes in the last level are as far left as possible. It can have between 1 and 
# 2h nodes inclusive at the last level h.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,6]
# Output: 6
# 
# Example 2:
# 
# Input: root = []
# Output: 0
# 
# Example 3:
# 
# Input: root = [1]
# Output: 1
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 5 * 104].
# 	0 <= Node.val <= 5 * 104
# 	The tree is guaranteed to be complete.
# 
# Follow up: Traversing the tree to count the number of nodes in the tree is an easy solution but 
# with O(n) complexity. Could you find a faster algorithm?
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def countNodes(self, root: TreeNode) -> int:  # O(n)

        def traverse(node: TreeNode):
            nonlocal ctr
            if not node:
                return
            ctr += 1
            traverse(node.left)
            traverse(node.right)

        ctr = 0
        traverse(root)
        return ctr

    def countNodes(self, root: TreeNode) -> int:  # O(n)
        return 1 + self.countNodes(root.left) + self.countNodes(root.right) if root else 0

    def countNodes(self, root: TreeNode) -> int:  # O(log n * log n)

        def depth(node: TreeNode, is_left: bool):
            return 0 if not node else 1 + depth(node.left, is_left) if is_left else 1 + depth(node.right, is_left)

        left, right = depth(root, True), depth(root, False)
        return 2 ** left - 1 if left == right else 1 + self.countNodes(root.left) + self.countNodes(root.right)


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,5,6]",
        "[]",
        "[1]",
        ]
    expectations = [6, 0, 1]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().countNodes(root)
        assert solution == expected
