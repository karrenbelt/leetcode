### Source : https://leetcode.com/problems/even-odd-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-10

##################################################################################################### 
#
# A binary tree is named Even-Odd if it meets the following conditions:
# 
# 	The root of the binary tree is at level index 0, its children are at level index 1, their 
# children are at level index 2, etc.
# 	For every even-indexed level, all nodes at the level have odd integer values in strictly 
# increasing order (from left to right).
# 	For every odd-indexed level, all nodes at the level have even integer values in strictly 
# decreasing order (from left to right).
# 
# Given the root of a binary tree, return true if the binary tree is Even-Odd, otherwise return false.
# 
# Example 1:
# 
# Input: root = [1,10,4,3,null,7,9,12,8,6,null,null,2]
# Output: true
# Explanation: The node values on each level are:
# Level 0: [1]
# Level 1: [10,4]
# Level 2: [3,7,9]
# Level 3: [12,8,6,2]
# Since levels 0 and 2 are all odd and increasing, and levels 1 and 3 are all even and decreasing, 
# the tree is Even-Odd.
# 
# Example 2:
# 
# Input: root = [5,4,2,3,3,7]
# Output: false
# Explanation: The node values on each level are:
# Level 0: [5]
# Level 1: [4,2]
# Level 2: [3,3,7]
# Node values in the level 2 must be in strictly increasing order, so the tree is not Even-Odd.
# 
# Example 3:
# 
# Input: root = [5,9,1,3,5,7]
# Output: false
# Explanation: Node values in the level 1 should be even integers.
# 
# Example 4:
# 
# Input: root = [1]
# Output: true
# 
# Example 5:
# 
# Input: root = [11,8,6,1,3,9,11,30,20,18,16,12,10,4,2,17]
# Output: true
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 105].
# 	1 <= Node.val <= 106
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def isEvenOddTree(self, root: TreeNode) -> bool:

        def traverse(node: TreeNode, level: int = 0):
            if node:
                if level > len(levels) - 1:
                    levels.append([])
                levels[level].append(node.val)
                traverse(node.left, level + 1)
                traverse(node.right, level + 1)

        def check_monotonicity(values, decreasing: int) -> bool:
            return all(a > b if decreasing else a < b for a, b in zip(values, values[1:]))

        def check_parity(values, even: int) -> bool:
            return all(value & 1 ^ 1 == even for value in values)

        levels = []
        traverse(root)
        return all(check_monotonicity(level, i & 1) and check_parity(level, i & 1) for i, level in enumerate(levels))

    def isEvenOddTree(self, root: TreeNode) -> bool:
        from collections import deque
        queue = deque([root])
        num = 0
        while queue:
            level, prev = [], None
            while queue:
                node = queue.popleft()
                if num and (node.val & 1 or prev and prev <= node.val):
                    return False
                elif not num and (not node.val & 1 or prev and prev >= node.val):
                    return False
                prev = node.val
                level.extend([node.left, node.right])
            queue.extend(filter(None, level))
            num ^= 1
        return True


def test():
    from python3 import Codec
    arguments = [
        "[1,10,4,3,null,7,9,12,8,6,null,null,2]",
        "[5,4,2,3,3,7]",
        "[5,9,1,3,5,7]",
        "[1]",
    ]
    expectations = [True, False, False, True]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().isEvenOddTree(root)
        assert solution == expected
