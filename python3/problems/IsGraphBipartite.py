### Source : https://leetcode.com/problems/is-graph-bipartite/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-14

##################################################################################################### 
#
# Given an undirected graph, return true if and only if it is bipartite.
# 
# Recall that a graph is bipartite if we can split its set of nodes into two independent subsets A 
# and B, such that every edge in the graph has one node in A and another node in B.
# 
# The graph is given in the following form: graph[i] is a list of indexes j for which the edge 
# between nodes i and j exists. Each node is an integer between 0 and graph.length - 1. There are no 
# self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.
# 
# Example 1:
# 
# Input: graph = [[1,3],[0,2],[1,3],[0,2]]
# Output: true
# Explanation: We can divide the vertices into two groups: {0, 2} and {1, 3}.
# 
# Example 2:
# 
# Input: graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
# Output: false
# Explanation: We cannot find a way to divide the set of nodes into two independent subsets.
# 
# Constraints:
# 
# 	1 <= graph.length <= 100
# 	0 <= graph[i].length < 100
# 	0 <= graph[i][j] <= graph.length - 1
# 	graph[i][j] != i
# 	All the values of graph[i] are unique.
# 	The graph is guaranteed to be undirected. 
#####################################################################################################

from typing import List


class Solution:
    def isBipartite(self, graph: List[List[int]]) -> bool:  # TLE

        a, b = {0}, set(graph[0])
        graph = set(enumerate(map(tuple, graph)))
        while graph:
            for node, neighbors in list(graph):
                if node in a:
                    if a.intersection(neighbors):
                        return False
                    b = b.union(neighbors)
                    graph.remove((node, neighbors))
                elif node in b:
                    if b.intersection(neighbors):
                        return False
                    a = a.union(neighbors)
                    graph.remove((node, neighbors))
        return True

    def isBipartite(self, graph: List[List[int]]) -> bool:
        from collections import deque

        def bfs(node: int) -> bool:
            queue = deque([node])
            color[node] = 1
            while queue:
                node = queue.popleft()
                for neighbor in graph[node]:
                    if neighbor not in color:
                        color[neighbor] = - color[node]
                        queue.append(neighbor)
                    elif color[neighbor] == color[node]:
                        return False
            return True

        color = {}
        return all(i in color or bfs(i) for i in range(len(graph)))

    def isBipartite(self, graph: List[List[int]]) -> bool:
        from collections import deque

        group_a, group_b = {0}, set()
        queue = deque(range(len(graph)))
        while queue:
            node = queue.popleft()
            root_set, node_set = (group_a, group_b) if node in group_a else (group_b, group_a)
            if root_set.intersection(graph[node]):
                return False
            new_nodes = set(graph[node]).difference(node_set)
            node_set.update(new_nodes)
            queue.extendleft(new_nodes)
        return True

    def isBipartite(self, graph: List[List[int]]) -> bool:

        def dfs(u: int, color: int) -> bool:
            if u in colors:
                return colors[u] == color
            colors[u] = color
            return all(dfs(v, 1 - color) for v in graph[u])

        colors = {}
        return all(dfs(u, 0) for u in range(len(graph)) if u not in colors)


def test():
    arguments = [
        [[1, 2, 3], [0, 2], [0, 1, 3], [0, 2]],
        [[1, 3], [0, 2], [1, 3], [0, 2]],
    ]
    expectations = [False, True]
    for graph, expected in zip(arguments, expectations):
        solution = Solution().isBipartite(graph)
        assert solution == expected
