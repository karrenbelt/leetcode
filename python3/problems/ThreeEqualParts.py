### Source : https://leetcode.com/problems/three-equal-parts/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-17

##################################################################################################### 
#
# You are given an array arr which consists of only zeros and ones, divide the array into three 
# non-empty parts such that all of these parts represent the same binary value.
# 
# If it is possible, return any [i, j] with i + 1 < j, such that:
# 
# 	arr[0], arr[1], ..., arr[i] is the first part,
# 	arr[i + 1], arr[i + 2], ..., arr[j - 1] is the second part, and
# 	arr[j], arr[j + 1], ..., arr[arr.length - 1] is the third part.
# 	All three parts have equal binary values.
# 
# If it is not possible, return [-1, -1].
# 
# Note that the entire part is used when considering what binary value it represents. For example, 
# [1,1,0] represents 6 in decimal, not 3. Also, leading zeros are allowed, so [0,1,1] and [1,1] 
# represent the same value.
# 
# Example 1:
# Input: arr = [1,0,1,0,1]
# Output: [0,3]
# Example 2:
# Input: arr = [1,1,0,1,1]
# Output: [-1,-1]
# Example 3:
# Input: arr = [1,1,0,0,1]
# Output: [0,2]
# 
# Constraints:
# 
# 	3 <= arr.length <= 3 * 104
# 	arr[i] is 0 or 1
#####################################################################################################

from typing import List
import functools


@functools.total_ordering
class BinarySegment:
    # just a fancied-up deque
    def __init__(self, arr: List[int]):
        from collections import deque
        self.array = deque()
        self.value = 0
        for byte in arr:
            self.append(byte)

    def append(self, byte: int):  # O(1) time
        self.array.append(byte)
        self.value = (self.value << 1) ^ byte

    def popleft(self) -> int:  # O(log n) time
        byte = self.array.popleft()
        if byte:
            mask = self.value  # clear most significant bit
            while mask & (mask - 1):
                mask &= mask - 1
            self.value &= ~mask
        return byte

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value

    def __len__(self):
        return len(self.array)

    def __repr__(self):
        # return f"{self.__class__.__name__}: {bin(self.value)[2:]}"
        return f"{self.__class__.__name__}: {list(self.array)}"


class Solution:
    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n^3) time, presumably TLE

        def to_int(nums: List[int]) -> int:
            return int(''.join(map(str, nums)), 2)

        for i in range(len(arr) - 2):
            for j in range(i + 1, len(arr) - 1):
                if to_int(arr[:i + 1]) == to_int(arr[i + 1: j + 1]) == to_int(arr[j + 1:]):
                    return [i, j + 1]
        return [-1, -1]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n^2) time -> TLE: 108 / 118 test cases passed.
        from collections import deque

        def to_int(nums: List[int]) -> int:
            return int(''.join(map(str, nums)), 2) if nums else 0

        if sum(arr) % 3:
            return [0, len(arr) - 1] if not sum(arr) else [-1, -1]

        i, j = 0, 1
        left, mid, right = map(deque, (arr[:i + 1], arr[i + 1: j + 1], arr[j + 1:]))
        while i < len(arr) - 2:
            left_val, mid_val, right_val = map(to_int, (left, mid, right))
            if left_val == mid_val == right_val:
                return [i, j + 1]
            if right_val < left_val:
                break
            if mid_val and left_val < mid_val:
                i += 1
                left.append(mid.popleft())
            if right_val and mid_val < right_val:
                j += 1
                mid.append(right.popleft())
        return [-1, -1]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # not O(n log n) time: expect TLE
        i, j = 0, 1
        left, mid, right = map(BinarySegment, (arr[:i + 1], arr[i + 1: j + 1], arr[j + 1:]))
        while i < len(arr) - 2:
            if left == mid == right:
                return [i, j + 1]
            if right < left:
                break
            while mid and left < mid:
                i += 1
                left.append(mid.popleft())
            while right and mid < right:
                j += 1
                mid.append(right.popleft())
        return [-1, -1]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n) time and O(n) space
        # prefix sum and binary search
        import bisect

        target = sum(arr)
        if not target or target % 3:
            return [-1, -1] if target % 3 else [0, len(arr) - 1]

        prefix_sum = (prefix := 0) or [prefix := prefix + n for n in arr]
        trailing = len(arr) - bisect.bisect_left(prefix_sum, prefix_sum[-1]) - 1
        i = bisect.bisect_left(prefix_sum, target // 3) + trailing
        j = 1 + bisect.bisect_left(prefix_sum, 2 * target // 3) + trailing
        start = bisect.bisect_right(prefix_sum, 0)
        return [i, j] if arr[start: i+1] == arr[j - (i + 1 - start): j] == arr[-(i + 1 - start):] else [-1, -1]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n) time and O(n) space
        # all parts need to have the same number of ones and trailing zeroes
        zero_indices = [i for i, n in enumerate(arr) if n == 1]
        if not zero_indices:
            return [0, len(arr) - 1]
        if len(zero_indices) % 3:
            return [-1, -1]
        index_indices = [(i * len(zero_indices) // 3, (i + 1) * len(zero_indices) // 3 - 1) for i in range(3)]
        parts = [(zero_indices[i], zero_indices[j]) for i, j in index_indices]
        trailing = len(arr) - parts[-1][-1] - 1
        if any(e1 + trailing >= s2 or arr[s1:e1] != arr[s2:e2] for (s1, e1), (s2, e2) in zip(parts, parts[1:])):
            return [-1, -1]
        return [part[1] + trailing + 1 * i for i, part in enumerate(parts[:-1])]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n) time and O(n) space
        ones = [i for i, d in enumerate(arr) if d == 1]
        if not ones or len(ones) % 3:
            return [-1, -1] if len(ones) % 3 else [0, 2]
        i, j, k = [ones[len(ones) // 3 * i] for i in range(3)]
        length = len(arr) - k
        return [i + length - 1, j + length] if arr[i:i + length] == arr[j:j + length] == arr[k:k + length] else [-1, -1]

    def threeEqualParts(self, arr: List[int]) -> List[int]:  # O(n) time and O(1) space
        ones = sum(arr)
        if not ones or ones % 3:
            return [-1, -1] if ones % 3 else [0, 2]

        ctr, starts = 0, []
        for i, n in enumerate(arr):
            if n == 1:
                if ctr % (ones // 3) == 0:
                    starts.append(i)
                ctr += 1

        i, j, k = starts
        while k < len(arr):
            if arr[i] == arr[j] == arr[k]:
                i += 1
                j += 1
                k += 1
            else:
                return [-1, -1]
        return [i - 1, j]


def test():
    def print_solution(i: int, j: int):
        print('no solution') if i == j else print(arr[:i + 1], arr[i + 1: j], arr[j:])

    def verify_solution(array, i, j):
        if i != j != -1:
            a, b, c = map(lambda nums: int(''.join(map(str, nums)), 2), [array[:i + 1], array[i + 1:j], array[j:]])
            assert a == b == c

    arguments = [
        [1, 0, 1, 0, 1],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 0, 1],
        [0, 1, 0, 0, 1, 0, 0, 1, 0],
        [0, 0, 0, 1, 1, 1],
        [0, 1, 0],  # [-1, -1]
        [0, 1, 0, 0, 0, 0, 1],  # [-1, -1]
        # [0, 0, 0, 0, 0],  # any is good
        [1, 1, 0, 1, 1, 1, 1, 0, 0],  # [-1, -1]
        [0, 0, 1, 1, 0, 1, 1, 1, 1],
    ]
    expectations = [
        [0, 3],
        [-1, -1],
        [0, 2],
        [2, 6],
        [3, 5],
        [-1, -1],
        [-1, -1],
        # [0, 2],  # any is good (but [0, 0] and [0, 1])
        [-1, -1],
        [3, 7],
    ]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().threeEqualParts(arr)
        verify_solution(arr, *solution)
        assert solution == expected, (solution, expected, arr)
