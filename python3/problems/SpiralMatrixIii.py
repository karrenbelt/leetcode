### Source : https://leetcode.com/problems/spiral-matrix-iii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-07

##################################################################################################### 
#
# On a 2 dimensional grid with R rows and C columns, we start at (r0, c0) facing east.
# 
# Here, the north-west corner of the grid is at the first row and column, and the south-east corner 
# of the grid is at the last row and column.
# 
# Now, we walk in a clockwise spiral shape to visit every position in this grid. 
# 
# Whenever we would move outside the boundary of the grid, we continue our walk outside the grid (but 
# may return to the grid boundary later.) 
# 
# Eventually, we reach all R * C spaces of the grid.
# 
# Return a list of coordinates representing the positions of the grid in the order they were visited.
# 
# Example 1:
# 
# Input: R = 1, C = 4, r0 = 0, c0 = 0
# Output: [[0,0],[0,1],[0,2],[0,3]]
# 
# Example 2:
# 
# Input: R = 5, C = 6, r0 = 1, c0 = 4
# Output: 
# [[1,4],[1,5],[2,5],[2,4],[2,3],[1,3],[0,3],[0,4],[0,5],[3,5],[3,4],[3,3],[3,2],[2,2],[1,2],[0,2],[4,
# 5],[4,4],[4,3],[4,2],[4,1],[3,1],[2,1],[1,1],[0,1],[4,0],[3,0],[2,0],[1,0],[0,0]]
# 
# Note:
# 
# 	1 <= R <= 100
# 	1 <= C <= 100
# 	0 <= r0 < R
# 	0 <= c0 < C
# 
#####################################################################################################

from typing import List


class Solution:
    def spiralMatrixIII(self, R: int, C: int, r0: int, c0: int) -> List[List[int]]:
        result = [[r0, c0]]
        row, col = r0, c0
        step, direction = 1, 1
        while len(result) < R * C:
            for _ in range(step):
                col = col + 1 * direction
                if 0 <= row < R and 0 <= col < C:
                    result.append([row, col])
            for _ in range(step):
                row = row + 1 * direction
                if 0 <= row < R and 0 <= col < C:
                    result.append([row, col])
            step += 1
            direction *= -1
        return result

    def spiralMatrixIII(self, R: int, C: int, r0: int, c0: int) -> List[List[int]]:
        import itertools
        direction = itertools.cycle([(0, 1), (1, 0), (0, -1), (-1, 0)])
        ans = [[r0, c0]]
        step = 0
        while len(ans) < R * C:
            di, dj = next(direction)
            if di == 0:
                step += 1
            for _ in range(step):
                r0 += di
                c0 += dj
                if 0 <= r0 < R and 0 <= c0 < C:
                    ans.append([r0, c0])
        return ans

    def spiralMatrixIII(self, R, C, r, c):
        import math

        def key(args):
            x, y = args
            x, y = x - r, y - c
            return max(abs(x), abs(y)), -((math.atan2(-1, 1) - math.atan2(x, y)) % (math.pi * 2))

        return sorted([[i, j] for i in range(R) for j in range(C)], key=key)

    def spiralMatrixIII(self, R, C, r0, c0):
        return sorted([[r, c] for r in range(R) for c in range(C)],
                      key=lambda x: (lambda r, c: (max(abs(r - r0), abs(c - c0)),
                                                   (0, r, -c) if r + c > r0 + c0 else (1, -r, c)))(*x))


def test():
    quadruplets_of_numbers = [
        (1, 4, 0, 0),
        (5, 6, 1, 4),
        ]
    expectations = [
        [[0, 0], [0, 1], [0, 2], [0, 3]],
        [[1, 4], [1, 5], [2, 5], [2, 4], [2, 3], [1, 3], [0, 3], [0, 4], [0, 5], [3, 5], [3, 4], [3, 3], [3, 2], [2, 2],
         [1, 2], [0, 2], [4, 5], [4, 4], [4, 3], [4, 2], [4, 1], [3, 1], [2, 1], [1, 1], [0, 1], [4, 0], [3, 0], [2, 0],
         [1, 0], [0, 0]],
        ]
    for (R, C, r0, c0), expected in zip(quadruplets_of_numbers, expectations):
        solution = Solution().spiralMatrixIII(R, C, r0, c0)
        assert solution == expected
