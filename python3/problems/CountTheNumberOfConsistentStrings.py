### Source : https://leetcode.com/problems/count-the-number-of-consistent-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# You are given a string allowed consisting of distinct characters and an array of strings words. A 
# string is consistent if all characters in the string appear in the string allowed.
# 
# Return the number of consistent strings in the array words.
# 
# Example 1:
# 
# Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
# Output: 2
# Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.
# 
# Example 2:
# 
# Input: allowed = "abc", words = ["a","b","c","ab","ac","bc","abc"]
# Output: 7
# Explanation: All strings are consistent.
# 
# Example 3:
# 
# Input: allowed = "cad", words = ["cc","acd","b","ba","bac","bad","ac","d"]
# Output: 4
# Explanation: Strings "cc", "acd", "ac", and "d" are consistent.
# 
# Constraints:
# 
# 	1 <= words.length <= 104
# 	1 <= allowed.length <= 26
# 	1 <= words[i].length <= 10
# 	The characters in allowed are distinct.
# 	words[i] and allowed contain only lowercase English letters.
#####################################################################################################

from typing import List


class Solution:
    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:  # 276 ms
        return sum(not set(word).difference(allowed) for word in words)

    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:  # 264 ms
        return sum(all(c in allowed for c in w) for w in words)

    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:  # 264 ms
        return sum(all(c in s for c in w) for w in words) if (s := allowed) else 0

    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:  # 280 ms
        return sum(all(c in s for c in w) for w in map(set, words)) if (s := allowed) else 0

    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:  # 268 ms
        return sum(set(allowed) >= w for w in map(set, words))


def test():
    arguments = [
        ("ab", ["ad", "bd", "aaab", "baa", "badab"]),
        ("abc", ["a", "b", "c", "ab", "ac", "bc", "abc"]),
        ("cad", ["cc", "acd", "b", "ba", "bac", "bad", "ac", "d"]),
    ]
    expectations = [2, 7, 4]
    for (allowed, words), expected in zip(arguments, expectations):
        solution = Solution().countConsistentStrings(allowed, words)
        assert solution == expected
