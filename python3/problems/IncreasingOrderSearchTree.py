### Source : https://leetcode.com/problems/increasing-order-search-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-16

##################################################################################################### 
#
# Given the root of a binary search tree, rearrange the tree in in-order so that the leftmost node in 
# the tree is now the root of the tree, and every node has no left child and only one right child.
# 
# Example 1:
# 
# Input: root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
# Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
# 
# Example 2:
# 
# Input: root = [5,1,7]
# Output: [1,null,5,null,7]
# 
# Constraints:
# 
# 	The number of nodes in the given tree will be in the range [1, 100].
# 	0 <= Node.val <= 1000
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def increasingBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode):
            if not node:
                return
            traverse(node.left)
            nodes.append(node)
            traverse(node.right)

        nodes = []
        traverse(root)
        for i in range(len(nodes) - 1):
            nodes[i].left = None
            nodes[i].right = nodes[i + 1]
        nodes[-1].right = None
        nodes[-1].left = None
        return nodes[0]

    def increasingBST(self, root: TreeNode) -> TreeNode:

        def traverse(node: TreeNode, tail=None):
            if not node:
                return tail
            res = traverse(node.left, node)
            node.left = None
            node.right = traverse(node.right, tail)
            return res

        return traverse(root)

    def increasingBST(self, root: TreeNode) -> TreeNode:  # O(n) time and O(log n) space - with dummy node
        new = dummy = TreeNode(0)
        stack = []
        node = root
        while stack or node:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            new.right = node
            new = new.right
            node = node.right
            new.left = None
        return dummy.right

    def increasingBST(self, node: TreeNode) -> TreeNode:  # Morris traversal: O(n) time O(1) space
        dummy = tail = TreeNode(0)
        while node:
            if node.left:
                predecessor = node.left
                while predecessor.right:
                    predecessor = predecessor.right
                predecessor.right = node
                left, node.left = node.left, None
                node = left
            else:
                tail.right = tail = node
                node = node.right
        return dummy.right


def test():
    from python3 import Codec
    arguments = [
        "[5,3,6,2,4,null,8,1,null,null,null,7,9]",
        "[5,1,7]",
    ]
    expectations = [
        "[1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]",
        "[1,null,5,null,7]",
    ]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().increasingBST(root)
        assert solution == Codec.deserialize(expected)

