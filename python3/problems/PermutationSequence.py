### Source : https://leetcode.com/problems/permutation-sequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-05

##################################################################################################### 
#
# The set [1, 2, 3, ..., n] contains a total of n! unique permutations.
# 
# By listing and labeling all of the permutations in order, we get the following sequence for n = 3:
# 
# 	"123"
# 	"132"
# 	"213"
# 	"231"
# 	"312"
# 	"321"
# 
# Given n and k, return the kth permutation sequence.
# 
# Example 1:
# Input: n = 3, k = 3
# Output: "213"
# Example 2:
# Input: n = 4, k = 9
# Output: "2314"
# Example 3:
# Input: n = 3, k = 1
# Output: "123"
# 
# Constraints:
# 
# 	1 <= n <= 9
# 	1 <= k <= n!
#####################################################################################################


class Solution:
    def getPermutation(self, n: int, k: int) -> str:  # TLE: 127 / 200 passed
        paths = [[]]
        for i in range(1, n + 1):
            tmp = []
            for path in paths:
                for j in range(len(path) + 1):
                    tmp.append(path[:j] + [i] + path[j:])
            paths = tmp
        paths.sort()
        return ''.join(map(str, paths[k - 1]))

    def getPermutation(self, n: int, k: int) -> str:
        # realize that when n = 4, you have {1, 2, 3}
        # all permutations == 1 + (permutations of 2, 3) & 2 + (permutations of 1, 3) & 3 + (permutations of 1, 2)
        # all of these subset of permutations have the same length,
        # and the index of the next digit can be found: index -= (index from previous) * (n-1)!
        # once the index is used we need to delete it from the list of candidates

        from functools import lru_cache

        @lru_cache(maxsize=None)
        def factorial(num: int):
            if num < 2:
                return 1
            elif num >= 2:
                return num * factorial(num - 1)

        numbers = list(range(1, n + 1))
        permutation = ''
        k -= 1  # important
        while n > 0:
            n -= 1
            index, k = divmod(k, factorial(n))
            permutation += str(numbers[index])
            del numbers[index]
        return permutation


def test():
    arguments = [
        (3, 3),
        (4, 9),
        (3, 1),
    ]
    expectations = ["213", "2314", "123"]
    for (n, k), expected in zip(arguments, expectations):
        solution = Solution().getPermutation(n, k)
        assert solution == expected
