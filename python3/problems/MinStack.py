### Source : https://leetcode.com/problems/min-stack/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-13

##################################################################################################### 
#
# Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
# 
# 	push(x) -- Push element x onto stack.
# 	pop() -- Removes the element on top of the stack.
# 	top() -- Get the top element.
# 	getin() -- Retrieve the minimum element in the stack.
# 
# Example:
# 
# inStack minStack = new inStack();
# minStack.push(-2);
# minStack.push(0);
# minStack.push(-3);
# minStack.getin();   --> Returns -3.
# minStack.pop();
# minStack.top();      --> Returns 0.
# minStack.getin();   --> Returns -2.
# 
#####################################################################################################


class MinStack:

    def __init__(self):
        self.stack = []

    def push(self, x: int) -> None:
        minimum = self.getMin()
        if minimum is None or x < minimum:
            minimum = x
        self.stack.append((x, minimum))

    def pop(self) -> None:
        self.stack.pop()[0]

    def top(self) -> int:
        return self.stack[-1][0]

    def getMin(self) -> int:
        if self.stack:
            return self.stack[-1][1]


# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()


def test():
    null = None
    operations = [
        (["MinStack","push","push","push","getMin","pop","top","getMin"],
         [[],[-2],[0],[-3],[],[],[],[]])
        ]
    expectations = [
        [null, null, null, null, -3, null, 0, -2],
        ]
    obj = MinStack()
    for (method, args), expected in zip(operations[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
