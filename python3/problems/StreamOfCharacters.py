### Source : https://leetcode.com/problems/stream-of-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-12-04

##################################################################################################### 
#
# Design an algorithm that accepts a stream of characters and checks if a suffix of these characters 
# is a string of a given array of strings words.
# 
# For example, if words = ["abc", "xyz"] and the stream added the four characters (one by one) 'a', 
# 'x', 'y', and 'z', your algorithm should detect that the suffix "xyz" of the characters "axyz" 
# matches "xyz" from words.
# 
# Implement the StreamChecker class:
# 
# 	StreamChecker(String[] words) Initializes the object with the strings array words.
# 	boolean query(char letter) Accepts a new character from the stream and returns true if any 
# non-empty suffix from the stream forms a word that is in words.
# 
# Example 1:
# 
# Input
# ["StreamChecker", "query", "query", "query", "query", "query", "query", "query", "query", "query", 
# "query", "query", "query"]
# [[["cd", "f", "kl"]], ["a"], ["b"], ["c"], ["d"], ["e"], ["f"], ["g"], ["h"], ["i"], ["j"], ["k"], 
# ["l"]]
# Output
# [null, false, false, false, true, false, true, false, false, false, false, false, true]
# 
# Explanation
# StreamChecker streamChecker = new StreamChecker(["cd", "f", "kl"]);
# streamChecker.query("a"); // return False
# streamChecker.query("b"); // return False
# streamChecker.query("c"); // return False
# streamChecker.query("d"); // return True, because 'cd' is in the wordlist
# streamChecker.query("e"); // return False
# streamChecker.query("f"); // return True, because 'f' is in the wordlist
# streamChecker.query("g"); // return False
# streamChecker.query("h"); // return False
# streamChecker.query("i"); // return False
# streamChecker.query("j"); // return False
# streamChecker.query("k"); // return False
# streamChecker.query("l"); // return True, because 'kl' is in the wordlist
# 
# Constraints:
# 
# 	1 <= words.length <= 2000
# 	1 <= words[i].length <= 2000
# 	words[i] consists of lowercase English letters.
# 	letter is a lowercase English letter.
# 	At most 4 * 104 calls will be made to query.
#####################################################################################################

from typing import List


class StreamChecker:  # 6000 ms
    def __init__(self, words: List[str]):  # O(mn) time and O(mn + w) space
        self.active_nodes = []  # O(w)  space
        self.trie = {}          # O(mn) space
        for word in words:      # O(m)  time
            node = self.trie
            for c in word:      # O(n)  time
                node = node.setdefault(c, {})
            node['.'] = {}

    def query(self, letter: str) -> bool:  # O(wq) time and O(w) space
        active_nodes = []  # O(w)
        if letter in self.trie:
            active_nodes.append(self.trie[letter])
        for node in self.active_nodes:
            if letter in node:  # O(1), one iteration of O(q)
                active_nodes.append(node[letter])
        self.active_nodes = active_nodes
        return any('.' in node for node in active_nodes)


class StreamChecker:  # 600 ms
    def __init__(self, words: List[str]):  # O(mn) time and O(mn + w) space
        self.suffix = ""
        self.maximum_length = max(map(len, words))
        self.trie = {}
        for word in words:
            node = self.trie
            for c in reversed(word):
                node = node.setdefault(c, {})
            node['.'] = {}

    def query(self, letter: str) -> bool:  # O(wq) time and O(w) space
        node = self.trie
        self.suffix = (letter + self.suffix)[:self.maximum_length]
        for c in self.suffix:
            if c in node:
                node = node[c]
                if '.' in node:
                    return True
            else:
                break
        return False

# Your StreamChecker object will be instantiated and called as such:
# obj = StreamChecker(words)
# param_1 = obj.query(letter)


def test():
    operations = ["StreamChecker", "query", "query", "query", "query", "query",
                  "query", "query", "query", "query", "query", "query", "query"]
    arguments = [[["cd", "f", "kl"]], ["a"], ["b"], ["c"], ["d"], ["e"],
                 ["f"], ["g"], ["h"], ["i"], ["j"], ["k"], ["l"]]
    expectations = [None, False, False, False, True, False, True, False, False, False, False, False, True]
    obj = StreamChecker(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
