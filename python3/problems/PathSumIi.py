### Source : https://leetcode.com/problems/path-sum-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-21

##################################################################################################### 
#
# Given the root of a binary tree and an integer targetSum, return all root-to-leaf paths where each 
# path's sum equals targetSum.
# 
# A leaf is a node with no children.
# 
# Example 1:
# 
# Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
# Output: [[5,4,11,2],[5,8,4,5]]
# 
# Example 2:
# 
# Input: root = [1,2,3], targetSum = 5
# Output: []
# 
# Example 3:
# 
# Input: root = [1,2], targetSum = 0
# Output: []
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [0, 5000].
# 	-1000 <= Node.val <= 1000
# 	-1000 <= targetSum <= 1000
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def pathSum(self, root: TreeNode, targetSum: int) -> List[List[int]]:

        def traverse(node: TreeNode, path: List[int]):
            if not node:
                return
            if not node.left and not node.right:
                path.append(node.val)
                if sum(path) == targetSum:
                    paths.append(path)
            traverse(node.left, path + [node.val])
            traverse(node.right, path + [node.val])

        paths = []
        traverse(root, [])
        return paths


def test():
    from python3 import Codec
    arguments = [
        ("[5,4,8,11,null,13,4,7,2,null,null,5,1]", 22),
        ("[1,2,3]", 5),
        ("[1,2]", 0),
        ]
    expectations = [
        [[5, 4, 11, 2], [5, 8, 4, 5]],
        [],
        [],
        ]
    for (serialized_tree, targetSum), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().pathSum(root, targetSum)
        assert set(map(tuple, solution)) == set(map(tuple, expected))
