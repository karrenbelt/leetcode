### Source : https://leetcode.com/problems/path-crossing/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-12

##################################################################################################### 
#
# Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each representing moving one unit north, 
# south, east, or west, respectively. You start at the origin (0, 0) on a 2D plane and walk on the 
# path specified by path.
# 
# Return true if the path crosses itself at any point, that is, if at any time you are on a location 
# you have previously visited. Return false otherwise.
# 
# Example 1:
# 
# Input: path = "NES"
# Output: false 
# Explanation: Notice that the path doesn't cross any point more than once.
# 
# Example 2:
# 
# Input: path = "NESWW"
# Output: true
# Explanation: Notice that the path visits the origin twice.
# 
# Constraints:
# 
# 	1 <= path.length <= 104
# 	path[i] is either 'N', 'S', 'E', or 'W'.
#####################################################################################################


class Solution:
    def isPathCrossing(self, path: str) -> bool:
        x = y = 0
        path, seen = list(path), {(x, y)}
        walk = {'N': (0, 1), 'S': (0, -1), 'W': (-1, 0), 'E': (1, 0)}
        while path:
            x, y = (sum(x) for x in zip((x, y), walk[path.pop()]))
            if (x, y) in seen:
                return True
            seen.add((x, y))
        return False

    def isPathCrossing(self, path: str) -> bool:
        seen = {(0, 0)}
        x = y = 0
        for c in path:
            step = (0, 1) if c == 'N' else (0, -1) if c == 'S' else (-1, 0) if c == 'W' else (1, 0)
            x, y = (sum(x) for x in zip((x, y), step))
            if (x, y) in seen:
                return True
            seen.add((x, y))
        return False


def test():
    arguments = [
        "NES",
        "NESWW",
        "NS",
    ]
    expectation = [False, True, True]
    for path, expected in zip(arguments, expectation):
        solution = Solution().isPathCrossing(path)
        assert solution == expected
