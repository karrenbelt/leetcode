### Source : https://leetcode.com/problems/add-binary/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Given two binary strings a and b, return their sum as a binary string.
# 
# Example 1:
# Input: a = "11", b = "1"
# Output: "100"
# Example 2:
# Input: a = "1010", b = "1011"
# Output: "10101"
# 
# Constraints:
# 
# 	1 <= a.length, b.length <= 104
# 	a and b consist only of '0' or '1' characters.
# 	Each string does not contain leading zeros except for the zero itself.
#####################################################################################################


class Solution:
    def addBinary(self, a: str, b: str) -> str:
        return bin(int(a, 2) + int(b, 2))[2:]

    def addBinary(self, a: str, b: str) -> str:
        max_len = max(len(a), len(b))
        a = a.zfill(max_len)
        b = b.zfill(max_len)
        result = ''
        carry_over = 0
        for i in range(max_len - 1, -1, -1):
            r = carry_over + int(a[i]) + int(b[i])
            result = ('1' if r % 2 == 1 else '0') + result
            carry_over = 0 if r < 2 else 1
        return '1' + result if carry_over else result


def test():
    strings = [
        ("11", "1"),
        ("1010", "1011"),
        ]
    expectations = ["100", "10101"]
    for (a, b), expected in zip(strings, expectations):
        solution = Solution().addBinary(a, b)
        assert solution == expected
