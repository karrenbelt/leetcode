### Source : https://leetcode.com/problems/binary-tree-inorder-traversal/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a binary tree, return the inorder traversal of its nodes' values.
# 
# Example:
# 
# Input: [1,null,2,3]
#    1
#     \
#      2
#     /
#    3
# 
# Output: [1,3,2]
# 
# Follow up: Recursive solution is trivial, could you do it iteratively?
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:

    def inorderTraversal(self, root: TreeNode) -> List[int]:

        def traverse(node: TreeNode):
            if not node:
                return
            traverse(node.left)
            order.append(node.val)
            traverse(node.right)

        order = []
        traverse(root)
        return order

    def inorderTraversal(self, root: TreeNode) -> List[int]:  # using a stack
        node = root
        order = []
        stack = []
        while node or stack:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            order.append(node.val)
            node = node.right
        return order

    def inorderTraversal(self, root: TreeNode) -> List[int]:  # one-liner
        def inorder(node):
            return inorder(node.left) + [node.val] + inorder(node.right) if node else []
        return inorder(root)


def test():
    from python3 import Codec
    serialized_trees = [
        "[1,null,2,3]",
        "[]",
        "[1]",
        "[1,2]",
        "[1,null,2]",
        ]
    expectations = [
        [1, 3, 2],
        [],
        [1],
        [2, 1],
        [1, 2]
        ]
    for serialized_tree, expected in zip(serialized_trees, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().inorderTraversal(root)
        assert solution == expected
