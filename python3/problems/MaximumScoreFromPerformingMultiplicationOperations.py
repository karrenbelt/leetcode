### Source : https://leetcode.com/problems/maximum-score-from-performing-multiplication-operations/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-19

##################################################################################################### 
#
# You are given two integer arrays nums and multipliers of size n and m respectively, where n >= m. 
# The arrays are 1-indexed.
# 
# You begin with a score of 0. You want to perform exactly m operations. On the ith operation 
# (1-indexed), you will:
# 
# 	Choose one integer x from either the start or the end of the array nums.
# 	Add multipliers[i] * x to your score.
# 	Remove x from the array nums.
# 
# Return the maximum score after performing m operations.
# 
# Example 1:
# 
# Input: nums = [1,2,3], multipliers = [3,2,1]
# Output: 14
# Explanation: An optimal solution is as follows:
# - Choose from the end, [1,2,3], adding 3 * 3 = 9 to the score.
# - Choose from the end, [1,2], adding 2 * 2 = 4 to the score.
# - Choose from the end, [1], adding 1 * 1 = 1 to the score.
# The total score is 9 + 4 + 1 = 14.
# 
# Example 2:
# 
# Input: nums = [-5,-3,-3,-2,7,1], multipliers = [-10,-5,3,4,6]
# Output: 102
# Explanation: An optimal solution is as follows:
# - Choose from the start, [-5,-3,-3,-2,7,1], adding -5 * -10 = 50 to the score.
# - Choose from the start, [-3,-3,-2,7,1], adding -3 * -5 = 15 to the score.
# - Choose from the start, [-3,-2,7,1], adding -3 * 3 = -9 to the score.
# - Choose from the end, [-2,7,1], adding 1 * 4 = 4 to the score.
# - Choose from the end, [-2,7], adding 7 * 6 = 42 to the score. 
# The total score is 50 + 15 - 9 + 4 + 42 = 102.
# 
# Constraints:
# 
# 	n == nums.length
# 	m == multipliers.length
# 	1 <= m <= 103
# 	m <= n <= 105 
# 	-1000 <= nums[i], multipliers[i] <= 1000
#####################################################################################################

from typing import List


class Solution:
    def maximumScore(self, nums: List[int], multipliers: List[int]) -> int:  # O(m^2) time and O(m^2) space
        from functools import lru_cache

        @lru_cache(maxsize=2000)  # otherwise MLE
        def dp(left: int, right: int, k: int) -> int:
            if k == len(multipliers):
                return 0
            pick_left = nums[left] * multipliers[k] + dp(left + 1, right, k + 1)
            pick_right = nums[right] * multipliers[k] + dp(left, right - 1, k + 1)
            return max(pick_left, pick_right)

        return dp(0, len(nums) - 1, 0)

    def maximumScore(self, nums: List[int], multipliers: List[int]) -> int:  # O(m^2) time and O(m^2) space
        n, m = len(nums), len(multipliers)
        dp = [[0] * m for _ in range(m + 1)]
        for i in reversed(range(m)):
            for j in range(i, m):
                k = i + m - j - 1
                dp[i][j] = max(nums[i] * multipliers[k] + dp[i + 1][j], nums[j - m + n] * multipliers[k] + dp[i][j - 1])
        return dp[0][-1]

    def maximumScore(self, nums: List[int], multipliers: List[int]) -> int:  # O(m^2) time and O(m) space
        lr = [0] * (len(multipliers) + 1)
        for l, m in enumerate(reversed(multipliers), start=len(nums) - len(multipliers)):
            lr = [max(m * nums[i] + lr[i + 1], m * nums[i + l] + lr[i]) for i, n in enumerate(lr[:-1])]
        return max(lr)


def test():
    arguments = [
        ([1, 2, 3], [3, 2, 1]),
        ([-5, -3, -3, -2, 7, 1], [-10, -5, 3, 4, 6]),
    ]
    expectations = [14, 102]
    for (nums, multipliers), expected in zip(arguments, expectations):
        solution = Solution().maximumScore(nums, multipliers)
        assert solution == expected, (solution, expected)
