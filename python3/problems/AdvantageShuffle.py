### Source : https://leetcode.com/problems/advantage-shuffle/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-24

##################################################################################################### 
#
# Given two arrays A and B of equal size, the advantage of A with respect to B is the number of 
# indices i for which A[i] > B[i].
# 
# Return any permutation of A that maximizes its advantage with respect to B.
# 
# Example 1:
# 
# Input: A = [2,7,11,15], B = [1,10,4,11]
# Output: [2,11,7,15]
# 
# Example 2:
# 
# Input: A = [12,24,8,32], B = [13,25,32,11]
# Output: [24,32,8,12]
# 
# Note:
# 
# 	1 <= A.length = B.length <= 10000
# 	0 <= A[i] <= 109
# 	0 <= B[i] <= 109
# 
#####################################################################################################

from typing import List


class Solution:
    def advantageCount(self, A: List[int], B: List[int]) -> List[int]:  # O(n^2) time and O(n) space
        # iterate B, pick closest bigger in A if present, otherwise smallest
        from collections import deque
        A_sorted = deque(sorted(A))
        B_sorted = sorted(B)

        ans = [0] * len(A)
        starting_index = {v: 0 for v in set(B)}
        while B_sorted:
            b = B_sorted.pop()
            if b < A_sorted[-1]:
                a = A_sorted.pop()
            else:
                a = A_sorted.popleft()
            b_start = starting_index[b]
            b_i = B.index(b, b_start)  # don't do this.
            ans[b_i] = a
            starting_index[b] = b_i + 1
        return ans

    def advantageCount(self, A: List[int], B: List[int]) -> List[int]:  # O(n log n) time and O(n) space
        from collections import deque
        ans = [0] * len(A)
        B_sorted = deque(sorted((b, i) for i, b in enumerate(B)))
        for a in sorted(A):
            if a > B_sorted[0][0]:
                v, i = B_sorted.popleft()
            else:
                v, i = B_sorted.pop()
            ans[i] = a
        return ans


def test():
    arguments = [
        ([2, 7, 11, 15], [1, 10, 4, 11]),
        ([12, 24, 8, 32], [13, 25, 32, 11]),
        ([1, 2, 2, 1, 2, 1], [1, 3, 3, 1, 3, 1]),  # custom test case
    ]
    expectations = [
        [2, 11, 7, 15],
        [24, 32, 8, 12],
        [2, 1, 1, 2, 1, 2],
    ]
    for (A, B), expected in zip(arguments, expectations):
        solution = Solution().advantageCount(A, B)
        assert solution == expected
