### Source : https://leetcode.com/problems/maximum-gap/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-20

##################################################################################################### 
#
# Given an integer array nums, return the maximum difference between two successive elements in its 
# sorted form. If the array contains less than two elements, return 0.
# 
# Example 1:
# 
# Input: nums = [3,6,9,1]
# Output: 3
# Explanation: The sorted form of the array is [1,3,6,9], either (3,6) or (6,9) has the maximum 
# difference 3.
# 
# Example 2:
# 
# Input: nums = [10]
# Output: 0
# Explanation: The array contains less than 2 elements, therefore return 0.
# 
# Constraints:
# 
# 	1 <= nums.length <= 104
# 	0 <= nums[i] <= 109
# 
# Follow up: Could you solve it in linear time/space?
#####################################################################################################

from typing import List


class Solution:
    def maximumGap(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return 0
        nums.sort()
        return max(b - a for a, b in zip(nums, nums[1:]))

    def maximumGap(self, nums: List[int]) -> int:  # bucket sort: O(n) time
        MAX_VAL = 2**31-1

        minimum, maximum = min(nums), max(nums)
        if len(nums) < 2 or minimum == maximum:
            return 0

        size = (maximum - minimum) // (len(nums) - 1) or 1
        bucket = [[MAX_VAL, -MAX_VAL] for _ in range((maximum - minimum) // size + 1)]
        for n in nums:
            b = bucket[(n - minimum) // size]
            b[0] = min(b[0], n)
            b[1] = max(b[1], n)
        bucket = [x for x in bucket if x[0] < MAX_VAL]
        return max(b[0] - a[1] for a, b in zip(bucket, bucket[1:]))


def test():
    arguments = [
        [3, 6, 9, 1],
        [10],
    ]
    expectations = [3, 0]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().maximumGap(nums)
        assert solution == expected
