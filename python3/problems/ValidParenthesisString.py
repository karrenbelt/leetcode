### Source : https://leetcode.com/problems/valid-parenthesis-string/
### Author : M.A.P. Karrenbelt
### Date   : 2020-04-16

##################################################################################################### 
#
# 
# Given a string containing only three types of characters: '(', ')' and '*', write a function to 
# check whether this string is valid. We define the validity of a string by these rules:
# 
# Any left parenthesis '(' must have a corresponding right parenthesis ')'.
# Any right parenthesis ')' must have a corresponding left parenthesis '('.
# Left parenthesis '(' must go before the corresponding right parenthesis ')'.
# '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty 
# string.
# An empty string is also valid.
# 
# Example 1:
# 
# Input: "()"
# Output: True
# 
# Example 2:
# 
# Input: "(*)"
# Output: True
# 
# Example 3:
# 
# Input: "(*))"
# Output: True
# 
# Note:
# 
# The string size will be in the range [1, 100].
# 
#####################################################################################################


class Solution:
    def checkValidString(self, s: str) -> bool:  # O(n) time and O(n) space

        stack = []
        wild_cards = []
        for i, c in enumerate(s):
            if c == '*':
                wild_cards.append((c, i))
            elif c == '(':
                stack.append((c, i))
            else:
                if stack:
                    stack.pop()  # close first parenthesis
                elif wild_cards:
                    wild_cards.pop(0)  # must have come first
                else:
                    return False  # invalid

        # leftover '('
        while stack and wild_cards:
            c, c_pos = stack.pop()  # pop right
            wc, wc_pos = wild_cards.pop()
            if wc_pos < c_pos:  # if the right most wildcard came before: invalid
                return False
        return not stack

    def checkValidString(self, s: str):  # O(n) time and O(1) space
        minimum_open = maximum_open = 0
        for i in s:
            maximum_open = maximum_open - 1 if i == ")" else maximum_open + 1
            minimum_open = minimum_open + 1 if i == '(' else max(minimum_open - 1, 0)
            if maximum_open < 0:
                return False
        return minimum_open == 0


def test():
    arguments = ['()', "(*))", '(*)', '((()*)(*)', '**(())', '*(*(*)*)*', '(**(']
    expectations = [True, True, True, True, True, True, False]
    for s, expected in zip(arguments, expectations):
        solution = Solution().checkValidString(s)
        assert solution == expected

