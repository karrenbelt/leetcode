### Source : https://leetcode.com/problems/remove-all-occurrences-of-a-substring/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-24

##################################################################################################### 
#
# Given two strings s and part, perform the following operation on s until all occurrences of the 
# substring part are removed:
# 
# 	Find the leftmost occurrence of the substring part and remove it from s.
# 
# Return s after removing all occurrences of part.
# 
# A substring is a contiguous sequence of characters in a string.
# 
# Example 1:
# 
# Input: s = "daabcbaabcbc", part = "abc"
# Output: "dab"
# Explanation: The following operations are done:
# - s = "daabcbaabcbc", remove "abc" starting at index 2, so s = "dabaabcbc".
# - s = "dabaabcbc", remove "abc" starting at index 4, so s = "dababc".
# - s = "dababc", remove "abc" starting at index 3, so s = "dab".
# Now s has no occurrences of "abc".
# 
# Example 2:
# 
# Input: s = "axxxxyyyyb", part = "xy"
# Output: "ab"
# Explanation: The following operations are done:
# - s = "axxxxyyyyb", remove "xy" starting at index 4 so s = "axxxyyyb".
# - s = "axxxyyyb", remove "xy" starting at index 3 so s = "axxyyb".
# - s = "axxyyb", remove "xy" starting at index 2 so s = "axyb".
# - s = "axyb", remove "xy" starting at index 1 so s = "ab".
# Now s has no occurrences of "xy".
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	1 <= part.length <= 1000
# 	s-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K-b-@-K and part consists of lowercase English letters.
#####################################################################################################


class Solution:
    def removeOccurrences(self, s: str, part: str) -> str:  # 32 ma
        while part in s:
            s = s.replace(part, '', 1)
        return s

    def removeOccurrences(self, s: str, part: str) -> str:  # 36 ms
        from functools import reduce
        return reduce(lambda a, b: (a + b).removesuffix(part), s, '')

    def removeOccurrences(self, s: str, part: str) -> str:  # 28 ms
        return self.removeOccurrences(s.replace(part, '', 1), part) if part in s else s


def test():
    arguments = [
        ("daabcbaabcbc", "abc"),
        ("axxxxyyyyb", "xy"),
    ]
    expectations = ["dab", "ab"]
    for (s, part), expected in zip(arguments, expectations):
        solution = Solution().removeOccurrences(s, part)
        assert solution == expected
