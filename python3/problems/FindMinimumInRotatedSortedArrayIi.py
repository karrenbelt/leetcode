### Source : https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
# 
# (i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).
# 
# Find the minimum element.
# 
# The array may contain duplicates.
# 
# Example 1:
# 
# Input: [1,3,5]
# Output: 1
# 
# Example 2:
# 
# Input: [2,2,2,0,1]
# Output: 0
# 
# Note:
# 
# 	This is a follow up problem to Find inimum in Rotated Sorted Array.
# 	Would allow duplicates affect the run-time complexity? How and why?
# 
#####################################################################################################

from typing import List


class Solution:
    def findMin(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        return min(nums)

    def findMin(self, nums: List[int]) -> int:
        left, right = 0, len(nums) - 1
        while left < right:
            mid = (left + right) // 2
            if nums[mid] < nums[right]:  # < instead of <=
                right = mid
            elif nums[mid] > nums[right]:
                left = mid + 1
            else:  # nums[mid] == nums[right]
                right -= 1
        return nums[left]

    def findMin(self, nums: List[int]) -> int:
        left, right = 0, len(nums) - 1
        while left < right:
            mid = left + (right - left) // 2
            if nums[mid] > nums[right]:
                left = mid + 1
            elif nums[mid] < nums[right]:
                right = mid
            else:
                right -= 1
        return nums[left]


def test():
    arrays_of_numbers = [
        [1, 3, 5],
        [2, 2, 2, 0, 1],
    ]
    expectations = [1, 0]
    for nums, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().findMin(nums)
        assert solution == expected
test()