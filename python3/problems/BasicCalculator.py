### Source : https://leetcode.com/problems/basic-calculator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-24

##################################################################################################### 
#
# Given a string s representing an expression, implement a basic calculator to evaluate it.
# 
# Example 1:
# 
# Input: s = "1 + 1"
# Output: 2
# 
# Example 2:
# 
# Input: s = " 2-1 + 2 "
# Output: 3
# 
# Example 3:
# 
# Input: s = "(1+(4+5+2)-3)+(6+8)"
# Output: 23
# 
# Constraints:
# 
# 	1 <= s.length <= 3 * 105
# 	s consists of digits, '+', '-', '(', ')', and ' '.
# 	s represents a valid expression.
#####################################################################################################


class Solution:
    def calculate(self, s: str) -> int:  # SyntaxError: too many nested parentheses: 37 / 40 test cases passed.
        return eval(s)

    def calculate(self, s: str) -> int:  # O(n) time and O(n) space
        result, num, sign, stack = 0, 0, 1, []
        for i, c in enumerate(s):
            if c.isdigit():
                num = num * 10 + int(c)
            elif c in '+-':
                result += num * sign
                sign = 1 if c == '+' else -1
                num = 0
            elif c == '(':
                stack.extend([result, sign])
                sign, result = 1, 0
            elif c == ')':
                result += sign * num
                result *= stack.pop()
                result += stack.pop()
                num = 0
        return result + num * sign


def test():
    arguments = [
        "1 + 1",
        " 2-1 + 2 ",
        "(1+(4+5+2)-3)+(6+8)",
        "- (3 + (4 + 5))",
        ]
    expectations = [2, 3, 23, -12]
    for s, expected in zip(arguments, expectations):
        solution = Solution().calculate(s)
        assert solution == expected
