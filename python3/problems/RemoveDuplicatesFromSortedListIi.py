### Source : https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-03

##################################################################################################### 
#
# Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only 
# distinct numbers from the original list. Return the linked list sorted as well.
# 
# Example 1:
# 
# Input: head = [1,2,3,3,4,4,5]
# Output: [1,2,5]
# 
# Example 2:
# 
# Input: head = [1,1,1,2,3]
# Output: [2,3]
# 
# Constraints:
# 
# 	The number of nodes in the list is in the range [0, 300].
# 	-100 <= Node.val <= 100
# 	The list is guaranteed to be sorted in ascending order.
#####################################################################################################

# Definition for singly-linked list.
from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        # 1 -> 1 -> 2 -> 3 -> 3
        dummy = ListNode(None)
        dummy.next = head
        prev, node = dummy, head
        while node:
            if node.next and node.val == node.next.val:
                # iterate to next, once no longer the same node is the last duplicate
                while node and node.next and node.val == node.next.val:
                    node = node.next
                prev.next = node.next  # propose next for prev, to be verified upon next iteration here
            else:  # cannot do while-else, else will execute when while breaks
                prev = prev.next  # if no duplicate, we keep the original, otherwise we verified the proposed
            node = node.next
        return dummy.next


def test():
    from python3 import singly_linked_list_from_array
    arrays_of_numbers = [
        [1, 2, 3, 3, 4, 4, 5],
        [1, 1, 1, 2, 3],
        [1, 1, 2, 3, 3],
        ]
    expectations = [
        [1, 2, 5],
        [2, 3],
        [2],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        head = singly_linked_list_from_array(nums).head
        solution = Solution().deleteDuplicates(head)
        assert solution == singly_linked_list_from_array(expected).head
