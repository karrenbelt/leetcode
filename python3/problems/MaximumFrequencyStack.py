### Source : https://leetcode.com/problems/maximum-frequency-stack/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-28

##################################################################################################### 
#
# Design a stack-like data structure to push elements to the stack and pop the most frequent element 
# from the stack.
# 
# Implement the FreqStack class:
# 
# 	FreqStack() constructs an empty frequency stack.
# 	void push(int val) pushes an integer val onto the top of the stack.
# 	int pop() removes and returns the most frequent element in the stack.
# 
# 		If there is a tie for the most frequent element, the element closest to the stack's 
# top is removed and returned.
# 
# Example 1:
# 
# Input
# ["FreqStack", "push", "push", "push", "push", "push", "push", "pop", "pop", "pop", "pop"]
# [[], [5], [7], [5], [7], [4], [5], [], [], [], []]
# Output
# [null, null, null, null, null, null, null, 5, 7, 5, 4]
# 
# Explanation
# FreqStack freqStack = new FreqStack();
# freqStack.push(5); // The stack is [5]
# freqStack.push(7); // The stack is [5,7]
# freqStack.push(5); // The stack is [5,7,5]
# freqStack.push(7); // The stack is [5,7,5,7]
# freqStack.push(4); // The stack is [5,7,5,7,4]
# freqStack.push(5); // The stack is [5,7,5,7,4,5]
# freqStack.pop();   // return 5, as 5 is the most frequent. The stack becomes [5,7,5,7,4].
# freqStack.pop();   // return 7, as 5 and 7 is the most frequent, but 7 is closest to the top. The 
# stack becomes [5,7,5,4].
# freqStack.pop();   // return 5, as 5 is the most frequent. The stack becomes [5,7,4].
# freqStack.pop();   // return 4, as 4, 5 and 7 is the most frequent, but 4 is closest to the top. 
# The stack becomes [5,7].
# 
# Constraints:
# 
# 	0 <= val <= 109
# 	At most 2 * 104 calls will be made to push and pop.
# 	It is guaranteed that there will be at least one element in the stack before calling pop.
#####################################################################################################


class FreqStack:  # TLE -> 31 / 37 test cases passed.
    # first a naive implementation
    def __init__(self):
        self.nums = []
        self.counts = {}

    def push(self, x: int) -> None:
        self.nums.append(x)
        self.counts[x] = self.counts.get(x, 0) + 1

    def pop(self) -> int:
        maximum_frequency = max(self.counts.values())
        numbers = {k for k, v in self.counts.items() if v == maximum_frequency}
        i, n = next((~i, n) for i, n in enumerate(reversed(self.nums)) if n in numbers)
        self.nums.pop(i)
        self.counts[n] -= 1
        return n


class FreqStack:
    def __init__(self):
        self.heap = []
        self.counts = {}
        self.order_ctr = 0

    def push(self, x: int) -> None:
        import heapq
        self.counts[x] = self.counts.get(x, 0) + 1
        heapq.heappush(self.heap, (-self.counts[x], -self.order_ctr, x))
        self.order_ctr += 1

    def pop(self) -> int:
        import heapq
        *_, value = heapq.heappop(self.heap)
        self.counts[value] -= 1
        return value

# Your FreqStack object will be instantiated and called as such:
# obj = FreqStack()
# obj.push(x)
# param_2 = obj.pop()


def test():
    operations = ["FreqStack", "push", "push", "push", "push", "push", "push", "pop", "pop", "pop", "pop"]
    arguments = [[], [5], [7], [5], [7], [4], [5], [], [], [], []]
    expectations = [None, None, None, None, None, None, None, 5, 7, 5, 4]
    obj = FreqStack(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
