### Source : https://leetcode.com/problems/implement-magic-dictionary/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-13

##################################################################################################### 
#
# Design a data structure that is initialized with a list of different words. Provided a string, you 
# should determine if you can change exactly one character in this string to match any word in the 
# data structure.
# 
# Implement the agicDictionary class:
# 
# 	agicDictionary() Initializes the object.
# 	void buildDict(String[] dictionary) Sets the data structure with an array of distinct 
# strings dictionary.
# 	bool search(String searchWord) Returns true if you can change exactly one character in 
# searchWord to match any string in the data structure, otherwise returns false.
# 
# Example 1:
# 
# Input
# ["agicDictionary", "buildDict", "search", "search", "search", "search"]
# [[], [["hello", "leetcode"]], ["hello"], ["hhllo"], ["hell"], ["leetcoded"]]
# Output
# [null, null, false, true, false, false]
# 
# Explanation
# agicDictionary magicDictionary = new agicDictionary();
# magicDictionary.buildDict(["hello", "leetcode"]);
# magicDictionary.search("hello"); // return False
# magicDictionary.search("hhllo"); // We can change the second 'h' to 'e' to match "hello" so we 
# return True
# magicDictionary.search("hell"); // return False
# magicDictionary.search("leetcoded"); // return False
# 
# Constraints:
# 
# 	1 <= dictionary.length <= 100
# 	1 <= dictionary[i].length <= 100
# 	dictionary[i] consists of only lower-case English letters.
# 	All the strings in dictionary are distinct.
# 	1 <= searchWord.length <= 100
# 	searchWord consists of only lower-case English letters.
# 	buildDict will be called only once before search.
# 	At most 100 calls will be made to search.
#####################################################################################################

from typing import List


class MagicDictionary:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.d = {}

    def buildDict(self, dictionary: List[str]) -> None:
        for word in dictionary:
            self.d.setdefault(len(word), []).append(word)

    def search(self, searchWord: str) -> bool:
        candidates = self.d.get(len(searchWord), [])
        for word in candidates:
            mismatch = 0
            for c1, c2 in zip(word, searchWord):
                if mismatch > 1:
                    break
                mismatch += c1 != c2
            if mismatch == 1:
                return True
        return False


class MagicDictionary:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.d = {}
        self.words = set()

    def buildDict(self, dictionary: List[str]) -> None:
        self.words = set(dictionary)
        for word in dictionary:
            for i in range(len(word)):
                wild_card_word = word[:i] + '*' + word[i + 1:]
                self.d[wild_card_word] = self.d.get(wild_card_word, 0) + 1

    def search(self, searchWord: str) -> bool:
        for word in (searchWord[:i] + '*' + searchWord[i + 1:] for i in range(len(searchWord))):
            count = self.d.get(word, 0)
            if count > 1 or count == 1 and searchWord not in self.words:
                return True
        return False

# Your MagicDictionary object will be instantiated and called as such:
# obj = MagicDictionary()
# obj.buildDict(dictionary)
# param_2 = obj.search(searchWord)


def test():
    operations = ["MagicDictionary", "buildDict", "search", "search", "search", "search"]
    arguments = [[], [["hello", "leetcode"]], ["hello"], ["hhllo"], ["hell"], ["leetcoded"]]
    expectations = [None, None, False, True, False, False]

    operations = ["MagicDictionary", "buildDict", "search", "search", "search", "search"]
    arguments = [[], [["hello", "hallo", "leetcode"]], ["hello"], ["hhllo"], ["hell"], ["leetcoded"]]
    expectations = [None, None, True, True, False, False]
    obj = MagicDictionary()
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
