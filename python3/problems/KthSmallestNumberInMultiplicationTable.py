### Source : https://leetcode.com/problems/kth-smallest-number-in-multiplication-table/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-01

##################################################################################################### 
#
# Nearly everyone has used the ultiplication Table. The multiplication table of size m x n is an 
# integer matrix mat where mat[i][j] == i * j (1-indexed).
# 
# Given three integers m, n, and k, return the kth smallest element in the m x n multiplication table.
# 
# Example 1:
# 
# Input: m = 3, n = 3, k = 5
# Output: 3
# Explanation: The 5th smallest number is 3.
# 
# Example 2:
# 
# Input: m = 2, n = 3, k = 6
# Output: 6
# Explanation: The 6th smallest number is 6.
# 
# Constraints:
# 
# 	1 <= m, n <= 3 * 104
# 	1 <= k <= m * n
#####################################################################################################


class Solution:
    def findKthNumber(self, m: int, n: int, k: int) -> int:  # brute force O(n log n) time, should TLE
        return sorted(i * j for i in range(1, m + 1) for j in range(1, n + 1))[k - 1]

    def findKthNumber(self, m: int, n: int, k: int) -> int:  # O(log n) time

        def condition(value: int) -> bool:
            count = 0
            for i in range(1, m + 1):  # count row by row
                add = min(value // i, n)
                if add == 0:  # early exit
                    break
                count += add
            return count >= k

        left, right = 0, n * m
        while left < right:
            mid = left + (right - left) // 2
            if condition(mid):
                right = mid
            else:
                left = mid + 1
        return left

    def findKthNumber(self, m: int, n: int, k: int) -> int:  # O(log n) time

        def condition(value: int) -> bool:
            return sum(min(value // i, n) for i in range(1, m + 1)) >= k

        left, right = 0, m * n
        while left < right:
            mid = left + (right - left) // 2
            if condition(mid):
                right = mid
            else:
                left = mid + 1
        return left


def test():
    arguments = [
        (3, 3, 5),
        (2, 3, 6),
    ]
    expectations = [3, 6]
    for (m, n, k), expected in zip(arguments, expectations):
        solution = Solution().findKthNumber(m, n, k)
        assert solution == expected
