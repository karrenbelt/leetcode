### Source : https://leetcode.com/problems/split-two-strings-to-make-palindrome/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-02

##################################################################################################### 
#
# You are given two strings a and b of the same length. Choose an index and split both strings at the 
# same index, splitting a into two strings: aprefix and asuffix where a = aprefix + asuffix, and 
# splitting b into two strings: bprefix and bsuffix where b = bprefix + bsuffix. Check if aprefix + 
# bsuffix or bprefix + asuffix forms a palindrome.
# 
# When you split a string s into sprefix and ssuffix, either ssuffix or sprefix is allowed to be 
# empty. For example, if s = "abc", then "" + "abc", "a" + "bc", "ab" + "c" , and "abc" + "" are 
# valid splits.
# 
# Return true if it is possible to form a palindrome string, otherwise return false.
# 
# Notice that x + y denotes the concatenation of strings x and y.
# 
# Example 1:
# 
# Input: a = "x", b = "y"
# Output: true
# Explaination: If either a or b are palindromes the answer is true since you can split in the 
# following way:
# aprefix = "", asuffix = "x"
# bprefix = "", bsuffix = "y"
# Then, aprefix + bsuffix = "" + "y" = "y", which is a palindrome.
# 
# Example 2:
# 
# Input: a = "abdef", b = "fecab"
# Output: true
# 
# Example 3:
# 
# Input: a = "ulacfd", b = "jizalu"
# Output: true
# Explaination: Split them at index 3:
# aprefix = "ula", asuffix = "cfd"
# bprefix = "jiz", bsuffix = "alu"
# Then, aprefix + bsuffix = "ula" + "alu" = "ulaalu", which is a palindrome.
# 
# Example 4:
# 
# Input: a = "xbdef", b = "xecab"
# Output: false
# 
# Constraints:
# 
# 	1 <= a.length, b.length <= 105
# 	a.length == b.length
# 	a and b consist of lowercase English letters
#####################################################################################################


class Solution:
    def checkPalindromeFormation(self, a: str, b: str) -> bool:  # O(n^2) time -> TLE: 91 / 108 test cases passed.
        for i in range(len(a)):
            x, y = a[:i] + b[i:], b[:i] + a[i:]
            if x == x[::-1] or y == y[::-1]:
                return True
        return False

    def checkPalindromeFormation(self, a: str, b: str) -> bool:  # O(n) time and O(n) space: 72ms

        def can_form_palindrome(x: str, y: str) -> bool:
            left, right = 0, len(a) - 1
            while left < right and x[left] == y[right]:
                left, right = left + 1, right - 1
            s1, s2 = x[left:right + 1], y[left:right + 1]
            return s1 == s1[::-1] or s2 == s2[::-1]

        return can_form_palindrome(a, b) or can_form_palindrome(b, a)

    def checkPalindromeFormation(self, a: str, b: str) -> bool:  # O(n) time and O(n) space: 108 ms

        def is_palindrome(s: str) -> bool:
            return all(s[i] == s[~i] for i in range(len(s) // 2))

        def can_form_palindrome(x: str, y: str) -> bool:
            left, right = 0, len(a) - 1
            while left < right and x[left] == y[right]:
                left, right = left + 1, right - 1
            return is_palindrome(x[left:right + 1]) or is_palindrome(y[left:right + 1])

        return can_form_palindrome(a, b) or can_form_palindrome(b, a)

    def checkPalindromeFormation(self, a: str, b: str) -> bool:  # O(n) time and O(1) space: 148 ms

        def is_palindrome(s: str, left: int, right: int) -> bool:
            return all(s[i] == s[~i] for i in range(left, right))

        def can_form_palindrome(x: str, y: str) -> bool:
            left, right = 0, len(a) - 1
            while left < right and x[left] == y[right]:
                left, right = left + 1, right - 1
            return is_palindrome(x, left, right) or is_palindrome(y, left, right)

        return can_form_palindrome(a, b) or can_form_palindrome(b, a)


def test():
    arguments = [
        ("x", "y"),
        ("abdef", "fecab"),
        ("ulacfd", "jizalu"),
        ("xbdef", "xecab"),
        ("pvhmupgqeltozftlmfjjde", "yjgpzbezspnnpszebzmhvp"),  # pvhmzbezspnnpszebzmhvp
    ]
    expectations = [True, True, True, False, True]
    for (a, b), expected in zip(arguments, expectations):
        solution = Solution().checkPalindromeFormation(a, b)
        assert solution == expected
