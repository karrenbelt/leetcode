### Source : https://leetcode.com/problems/clumsy-factorial/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-09

##################################################################################################### 
#
# The factorial of a positive integer n is the product of all positive integers less than or equal to 
# n.
# 
# 	For example, factorial(10) = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1.
# 
# We make a clumsy factorial using the integers in decreasing order by swapping out the multiply 
# operations for a fixed rotation of operations with multiply '*', divide '/', add '+', and subtract 
# '-' in this order.
# 
# 	For example, clumsy(10) = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1.
# 
# However, these operations are still applied using the usual order of operations of arithmetic. We 
# do all multiplication and division steps before any addition or subtraction steps, and 
# multiplication and division steps are processed left to right.
# 
# Additionally, the division that we use is floor division such that 10 * 9 / 8 = 90 / 8 = 11.
# 
# Given an integer n, return the clumsy factorial of n.
# 
# Example 1:
# 
# Input: n = 4
# Output: 7
# Explanation: 7 = 4 * 3 / 2 + 1
# 
# Example 2:
# 
# Input: n = 10
# Output: 12
# Explanation: 12 = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1
# 
# Constraints:
# 
# 	1 <= n <= 104
#####################################################################################################


class Solution:
    def clumsy(self, n: int) -> int:

        def cycle(*items):
            while items:
                for element in items:
                    yield element

        operators = cycle('*', '//', '+', '-')
        s = ''
        while n > 1:
            s += f"{n} {next(operators)} "
            n -= 1
        return eval(s + str(n))

    def clumsy(self, n: int) -> int:
        from operator import mul, add, sub, truediv
        from itertools import cycle
        operators = cycle([mul, truediv, add, sub])
        stack, n = [n], n - 1
        while n:
            operator = next(operators)
            if operator == add:
                stack.append(n)
            elif operator == sub:
                stack.append(-n)
            else:
                stack.append(int(operator(stack.pop(), n)))
            n -= 1
        return sum(stack)


def test():
    arguments = [4, 10]
    expectations = [7, 12]
    for n, expected in zip(arguments, expectations):
        solution = Solution().clumsy(n)
        assert solution == expected, solution
