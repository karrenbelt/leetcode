### Source : https://leetcode.com/problems/jump-game-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-02

##################################################################################################### 
#
# Given an array of non-negative integers nums, you are initially positioned at the first index of 
# the array.
# 
# Each element in the array represents your maximum jump length at that position.
# 
# Your goal is to reach the last index in the minimum number of jumps.
# 
# You can assume that you can always reach the last index.
# 
# Example 1:
# 
# Input: nums = [2,3,1,1,4]
# Output: 2
# Explanation: The minimum number of jumps to reach the last index is 2. Jump 1 step from index 0 to 
# 1, then 3 steps to the last index.
# 
# Example 2:
# 
# Input: nums = [2,3,0,1,4]
# Output: 2
# 
# Constraints:
# 
# 	1 <= nums.length <= 3 * 104
# 	0 <= nums[i] <= 105
#####################################################################################################

from typing import List


class Solution:

    def jump(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return 0

        n_jumps = 1
        max_distance = current_distance = nums[0]
        for i in range(len(nums)):
            if max_distance >= len(nums) - 1:
                return n_jumps
            if i < max_distance:  # if i in range update current distance that can be covered
                if i + nums[i] > current_distance:
                    current_distance = i + nums[i]
            else:
                max_distance = max(current_distance, i + nums[i])
                n_jumps += 1
        return n_jumps

    def jump(self, nums: List[int]) -> int:  # O(n) time and O(n) space
        dp = [2**31 - 1] * len(nums)
        dp[0] = 0
        for i, n in enumerate(nums):
            for j in range(1, n + 1):
                if i + j < len(nums):
                    dp[i + j] = min(dp[i + j], dp[i] + 1)
        return dp[-1]

    def jump(self, nums: List[int]) -> int:  # recursive one-liner: TLE
        return 1 if len(nums) - 1 <= nums[0] else 1 + self.jump(nums[max([(i + nums[i], i) for i in range(nums[0] + 1)])[1]:])

    def jump(self, nums: List[int]) -> int:  # O(n) time and O(1) space
        l = r = j = 0
        while r < len(nums) - 1 and (j := j + 1):
            l, r = r, max(i + nums[i] for i in range(l, r + 1))
        return j


def test():
    arguments = [
        [2, 3, 1, 1, 4],
        [2, 3, 0, 1, 4]
        ]
    expectations = [2, 2]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().jump(nums)
        assert solution == expected
