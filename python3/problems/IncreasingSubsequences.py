### Source : https://leetcode.com/problems/increasing-subsequences/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-01

##################################################################################################### 
#
# Given an integer array nums, return all the different possible increasing subsequences of the given 
# array with at least two elements. You may return the answer in any order.
# 
# The given array may contain duplicates, and two equal integers should also be considered a special 
# case of increasing sequence.
# 
# Example 1:
# 
# Input: nums = [4,6,7,7]
# Output: [[4,6],[4,6,7],[4,6,7,7],[4,7],[4,7,7],[6,7],[6,7,7],[7,7]]
# 
# Example 2:
# 
# Input: nums = [4,4,3,2,1]
# Output: [[4,4]]
# 
# Constraints:
# 
# 	1 <= nums.length <= 15
# 	-100 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def findSubsequences(self, nums: List[int]) -> List[List[int]]:

        def backtracking(i, path):
            if len(path) >= 2:
                paths.add(tuple(path))
            for j in range(i, len(nums)):
                if not path or path[-1] <= nums[j]:
                    backtracking(j + 1, path + [nums[j]])

        paths = set()
        backtracking(0, [])
        return list(map(list, paths))

    def findSubsequences(self, nums: List[int]) -> List[List[int]]:
        paths = {()}
        for n in nums:
            paths |= {path + (n, ) for path in paths if not path or path[-1] <= n}
        return [list(path) for path in paths if len(path) >= 2]


def test():
    from collections import Counter
    arguments = [
        [4, 6, 7, 7],
        [4, 4, 3, 2, 1],
    ]
    expectations = [
        [[4, 6], [4, 6, 7], [4, 6, 7, 7], [4, 7], [4, 7, 7], [6, 7], [6, 7, 7], [7, 7]],
        [[4, 4]],
    ]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().findSubsequences(nums)
        assert Counter(map(tuple, solution)) == Counter(map(tuple, expected))
