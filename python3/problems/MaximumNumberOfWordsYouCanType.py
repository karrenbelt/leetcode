### Source : https://leetcode.com/problems/maximum-number-of-words-you-can-type/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-24

##################################################################################################### 
#
# There is a malfunctioning keyboard where some letter keys do not work. All other keys on the 
# keyboard work properly.
# 
# Given a string text of words separated by a single space (no leading or trailing spaces) and a 
# string brokenLetters of all distinct letter keys that are broken, return the number of words in 
# text you can fully type using this keyboard.
# 
# Example 1:
# 
# Input: text = "hello world", brokenLetters = "ad"
# Output: 1
# Explanation: We cannot type "world" because the 'd' key is broken.
# 
# Example 2:
# 
# Input: text = "leet code", brokenLetters = "lt"
# Output: 1
# Explanation: We cannot type "leet" because the 'l' and 't' keys are broken.
# 
# Example 3:
# 
# Input: text = "leet code", brokenLetters = "e"
# Output: 0
# Explanation: We cannot type either word because the 'e' key is broken.
# 
# Constraints:
# 
# 	1 <= text.length <= 104
# 	0 <= brokenLetters.length <= 26
# 	text consists of words separated by a single space without any leading or trailing spaces.
# 	Each word only consists of lowercase English letters.
# 	brokenLetters consists of distinct lowercase English letters.
#####################################################################################################


class Solution:
    def canBeTypedWords(self, text: str, brokenLetters: str) -> int:  # 28 ms, beats one-line set implementations
        return len(list(filter(lambda word: not any(c in brokenLetters for c in word), text.split())))

    def canBeTypedWords(self, text: str, brokenLetters: str) -> int:  # 32 ms, create a word set every iteration.
        return sum(not set(w) & broken for w in text.split()) if (broken := set(brokenLetters)) else len(text.split())

    def canBeTypedWords(self, text: str, brokenLetters: str) -> int:  # 36 ms, create two sets every iterations
        return sum(not set(word) & set(brokenLetters) for word in text.split())


def test():
    arguments = [
        ("hello world", "ad"),
        ("leet code", "lt"),
        ("leet code", "e"),
    ]
    expectations = [1, 1, 0]
    for (text, brokenLetters), expected in zip(arguments, expectations):
        solution = Solution().canBeTypedWords(text, brokenLetters)
        assert solution == expected
