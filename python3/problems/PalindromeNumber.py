### Source : https://leetcode.com/problems/palindrome-number/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same 
# backward as forward.
# 
# Example 1:
# 
# Input: 121
# Output: true
# 
# Example 2:
# 
# Input: -121
# Output: false
# Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it 
# is not a palindrome.
# 
# Example 3:
# 
# Input: 10
# Output: false
# Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
# 
# Follow up:
# 
# Could you solve it without converting the integer to a string?
#####################################################################################################


class Solution:
    def isPalindrome(self, x: int) -> bool:
        str_x = str(x)
        return str_x == str_x[::-1]

    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        p, res = x, 0
        while p:
            p, r = divmod(p, 10)
            res = res * 10 + r
        return res == x


def test():
    numbers = [121, -121, 10, -101]
    expectations = [True, False, False, False]
    for x, expected in zip(numbers, expectations):
        solution = Solution().isPalindrome(x)
        assert solution == expected
