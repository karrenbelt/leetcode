### Source : https://leetcode.com/problems/random-flip-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-27

##################################################################################################### 
#
# There is an m x n binary grid matrix with all the values set 0 initially. Design an algorithm to 
# randomly pick an index (i, j) where matrix[i][j] == 0 and flips it to 1. All the indices (i, j) 
# where matrix[i][j] == 0 should be equally likely to be returned.
# 
# Optimize your algorithm to minimize the number of calls made to the built-in random function of 
# your language and optimize the time and space complexity.
# 
# Implement the Solution class:
# 
# 	Solution(int m, int n) Initializes the object with the size of the binary matrix m and n.
# 	int[] flip() Returns a random index [i, j] of the matrix where matrix[i][j] == 0 and flips 
# it to 1.
# 	void reset() Resets all the values of the matrix to be 0.
# 
# Example 1:
# 
# Input
# ["Solution", "flip", "flip", "flip", "reset", "flip"]
# [[3, 1], [], [], [], [], []]
# Output
# [null, [1, 0], [2, 0], [0, 0], null, [2, 0]]
# 
# Explanation
# Solution solution = new Solution(3, 1);
# solution.flip();  // return [1, 0], [0,0], [1,0], and [2,0] should be equally likely to be returned.
# solution.flip();  // return [2, 0], Since [1,0] was returned, [2,0] and [0,0]
# solution.flip();  // return [0, 0], Based on the previously returned indices, only [0,0] can be 
# returned.
# solution.reset(); // All the values are reset to 0 and can be returned.
# solution.flip();  // return [2, 0], [0,0], [1,0], and [2,0] should be equally likely to be returned.
# 
# Constraints:
# 
# 	1 <= m, n <= 104
# 	There will be at least one free cell for each call to flip.
# 	At most 1000 calls will be made to flip and reset.
#####################################################################################################

from typing import List


class Solution:

    def __init__(self, n_rows: int, n_cols: int):
        self.nums = []
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.reset()

    def flip(self) -> List[int]:
        return list(self.nums.pop())

    def reset(self) -> None:
        import random  # since at most 1000 calls will be made
        samples = random.sample(range(0, self.n_rows * self.n_cols), min(self.n_rows * self.n_cols, 1000))
        self.nums = [divmod(sample, self.n_cols) for sample in samples]

# Your Solution object will be instantiated and called as such:
# obj = Solution(n_rows, n_cols)
# param_1 = obj.flip()
# obj.reset()


def test():
    import random
    random.seed(222)
    operations = ["Solution", "flip", "flip", "flip", "reset", "flip"]
    arguments = [[3, 1], [], [], [], [], []]
    expectations = [None, [1, 0], [2, 0], [0, 0], None, [2, 0]]
    obj = Solution(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        solution = getattr(obj, method)(*args)
        assert solution == expected
