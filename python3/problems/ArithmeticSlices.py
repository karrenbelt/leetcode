### Source : https://leetcode.com/problems/arithmetic-slices/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-19

##################################################################################################### 
#
# A sequence of numbers is called arithmetic if it consists of at least three elements and if the 
# difference between any two consecutive elements is the same.
# 
# For example, these are arithmetic sequences:
# 
# 1, 3, 5, 7, 9
# 7, 7, 7, 7
# 3, -1, -5, -9
# 
# The following sequence is not arithmetic.
# 
# 1, 1, 2, 5, 7
# 
# A zero-indexed array A consisting of N numbers is given. A slice of that array is any pair of 
# integers (P, Q) such that 0 <= P < Q < N.
# 
# A slice (P, Q) of the array A is called arithmetic if the sequence:
# A[P], A[P + 1], ..., A[Q - 1], A[Q] is arithmetic. In particular, this means that P + 1 < Q.
# 
# The function should return the number of arithmetic slices in the array A.
# 
# Example:
# 
# A = [1, 2, 3, 4]
# 
# return: 3, for 3 arithmetic slices in A: [1, 2, 3], [2, 3, 4] and [1, 2, 3, 4] itself.
#####################################################################################################

from typing import List


class Solution:

    def numberOfArithmeticSlices(self, A: List[int]) -> int:  # Gauss Sum
        arithmetic_slices_ctr = seq_ctr = 0
        for i in range(2, len(A)):
            if A[i] - A[i - 1] == A[i - 1] - A[i - 2]:
                seq_ctr += 1
                arithmetic_slices_ctr += seq_ctr
            else:
                seq_ctr = 0
        return arithmetic_slices_ctr

    def numberOfArithmeticSlices(self, A: List[int]) -> int:
        from functools import reduce
        func = lambda a, i: ((0, a[1]), (a[0] + 1, a[0] + a[1] + 1))[A[i] - A[i - 1] == A[i - 1] - A[i - 2]]
        return reduce(func, range(2, len(A)), (0, 0))[1]


def test():
    arrays_of_numbers = [
        [1, 2, 3, 4],
        [1],
        ]
    expectations = [3, 0]
    for A, expected in zip(arrays_of_numbers, expectations):
        solution = Solution().numberOfArithmeticSlices(A)
        assert solution == expected
