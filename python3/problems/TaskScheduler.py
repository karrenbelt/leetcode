### Source : https://leetcode.com/problems/task-scheduler/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-02

##################################################################################################### 
#
# Given a characters array tasks, representing the tasks a CPU needs to do, where each letter 
# represents a different task. Tasks could be done in any order. Each task is done in one unit of 
# time. For each unit of time, the CPU could complete either one task or just be idle.
# 
# However, there is a non-negative integer n that represents the cooldown period between two same 
# tasks (the same letter in the array), that is that there must be at least n units of time between 
# any two same tasks.
# 
# Return the least number of units of times that the CPU will take to finish all the given tasks.
# 
# Example 1:
# 
# Input: tasks = ["A","A","A","B","B","B"], n = 2
# Output: 8
# Explanation: 
# A -> B -> idle -> A -> B -> idle -> A -> B
# There is at least 2 units of time between any two same tasks.
# 
# Example 2:
# 
# Input: tasks = ["A","A","A","B","B","B"], n = 0
# Output: 6
# Explanation: On this case any permutation of size 6 would work since n = 0.
# ["A","A","A","B","B","B"]
# ["A","B","A","B","A","B"]
# ["B","B","B","A","A","A"]
# ...
# And so on.
# 
# Example 3:
# 
# Input: tasks = ["A","A","A","A","A","A","B","C","D","E","F","G"], n = 2
# Output: 16
# Explanation: 
# One possible solution is
# A -> B -> C -> A -> D -> E -> A -> F -> G -> A -> idle -> idle -> A -> idle -> idle -> A
# 
# Constraints:
# 
# 	1 <= task.length <= 104
# 	tasks[i] is upper-case English letter.
# 	The integer n is in the range [0, 100].
#####################################################################################################

from typing import List


class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:
        # e.g. ['A', 'A', 'A', 'B', 'B', 'B', 'C', 'C', 'D'] and say n = 3
        # here A and B each occur 3 times, thus 2 elements occur 3 times
        # we can use a greedy approach to place the most common elements first
        # e.g. ['A', 'B', _, _, 'A', 'B', _, _, 'A', 'B', _, _, ...]
        # then we will in the remainder of the positions in a similar fashion in the missing spots
        # e.g. ['A', 'B', 'C', 'D', 'A', 'B', 'C', _, 'A', 'B']
        # tasks = ['A', 'A', 'A', 'B', 'B', 'B', 'C', 'C', 'D']
        from collections import Counter
        counts = sorted(Counter(tasks).values(), reverse=True)
        idle_time = (counts[0] - 1) * n - sum(min(counts[0] - 1, counts[i]) for i in range(1, len(counts)))
        return len(tasks) + max(0, idle_time)

    def leastInterval(self, tasks: List[str], n: int) -> int:
        from collections import Counter
        counts = Counter(tasks)
        most_common, max_count = counts.most_common(1)[0]
        return max(len(tasks), sum(1 for i in counts if counts[i] == max_count) + (n + 1) * (max_count - 1))


def test():
    arguments = [
        (["A", "A", "A", "B", "B", "B"], 2),
        (["A", "A", "A", "B", "B", "B"], 0),
        (["A", "A", "A", "A", "A", "A", "B", "C", "D", "E", "F", "G"], 2),
        ]
    expectations = [8, 6, 16]
    for (tasks, n), expected in zip(arguments, expectations):
        solution = Solution().leastInterval(tasks, n)
        assert solution == expected, (tasks, n)
