### Source : https://leetcode.com/problems/build-an-array-with-stack-operations/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-09

##################################################################################################### 
#
# Given an array target and an integer n. In each iteration, you will read a number from  list = 
# {1,2,3..., n}.
# 
# Build the target array using the following operations:
# 
# 	Push: Read a new element from the beginning list, and push it in the array.
# 	Pop: delete the last element of the array.
# 	If the target array is already built, stop reading more elements.
# 
# Return the operations to build the target array. You are guaranteed that the answer is unique.
# 
# Example 1:
# 
# Input: target = [1,3], n = 3
# Output: ["Push","Push","Pop","Push"]
# Explanation: 
# Read number 1 and automatically push in the array -> [1]
# Read number 2 and automatically push in the array then Pop it -> [1]
# Read number 3 and automatically push in the array -> [1,3]
# 
# Example 2:
# 
# Input: target = [1,2,3], n = 3
# Output: ["Push","Push","Push"]
# 
# Example 3:
# 
# Input: target = [1,2], n = 4
# Output: ["Push","Push"]
# Explanation: You only need to read the first 2 numbers and stop.
# 
# Example 4:
# 
# Input: target = [2,3,4], n = 4
# Output: ["Push","Pop","Push","Push","Push"]
# 
# Constraints:
# 
# 	1 <= target.length <= 100
# 	1 <= target[i] <= n
# 	1 <= n <= 100
# 	target is strictly increasing.
#####################################################################################################

from typing import List


class Solution:
    def buildArray(self, target: List[int], n: int) -> List[str]:  # O(|target|) time
        operations, ctr = [], 1
        for i, n in enumerate(target):
            while target[i] != i + ctr:
                operations.extend(["Push", "Pop"])
                ctr += 1
            operations.append("Push")
        return operations

    def buildArray(self, target: List[int], n: int) -> List[str]:  # O(|target|) time
        curr, result = 1, []
        for num in target:
            result += ["Push"] + ["Pop", "Push"] * (num - curr)
            curr = num + 1
        return result


def test():
    arguments = [
        ([1, 3], 3),
        ([1, 2, 3], 3),
        ([1, 2], 4),
        ([2, 3, 4], 4),
    ]
    expectations = [
        ["Push", "Push", "Pop", "Push"],
        ["Push", "Push", "Push"],
        ["Push", "Push"],
        ["Push", "Pop", "Push", "Push", "Push"],
    ]
    for (target, n), expected in zip(arguments, expectations):
        solution = Solution().buildArray(target, n)
        assert solution == expected
