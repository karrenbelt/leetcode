### Source : https://leetcode.com/problems/perfect-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# A perfect number is a positive integer that is equal to the sum of its positive divisors, excluding 
# the number itself. A divisor of an integer x is an integer that can divide x evenly.
# 
# Given an integer n, return true if n is a perfect number, otherwise return false.
# 
# Example 1:
# 
# Input: num = 28
# Output: true
# Explanation: 28 = 1 + 2 + 4 + 7 + 14
# 1, 2, 4, 7, and 14 are all divisors of 28.
# 
# Example 2:
# 
# Input: num = 6
# Output: true
# 
# Example 3:
# 
# Input: num = 496
# Output: true
# 
# Example 4:
# 
# Input: num = 8128
# Output: true
# 
# Example 5:
# 
# Input: num = 2
# Output: false
# 
# Constraints:
# 
# 	1 <= num <= 108
#####################################################################################################


class Solution:
    # the trick to avoiding TLE is to realize that there are divisors of num on both side of sqrt(num)

    def checkPerfectNumber(self, num: int) -> bool:  # O(num) -> TLE
        return sum(i for i in range(1, num // 2 + 1) if num % i == 0) == num

    def checkPerfectNumber(self, num: int) -> bool:  # O(sqrt(num)) time
        if num < 2:
            return False

        sum_of_factor = 1
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                if i == num // i:  # square root, should only add once
                    sum_of_factor += i
                else:
                    sum_of_factor += (i + num // i)
        return sum_of_factor == num

    def checkPerfectNumber(self, num: int) -> bool:  # same as previous: # O(sqrt(num)) time
        if num < 2:
            return False
        return sum(i if i == num//i else i + num//i for i in range(2, int(num**0.5) + 1) if num % i == 0) + 1 == num

    def checkPerfectNumber(self, num: int) -> bool:  # O(sqrt(num)) time

        def prime_factorization(n):
            d = 2
            while d * d <= n:
                expo = 0
                while n % d == 0:
                    expo += 1
                    n //= d
                if expo:
                    yield d, expo
                d += 1
            if n > 1:
                yield n, 1

        ans = 1
        for prime, exponent in prime_factorization(abs(num)):
            ans *= sum(prime ** k for k in range(exponent + 1))
        return ans == 2 * num

    def checkPerfectNumber(self, num: int) -> bool:  # O(1) time and O(1) space
        primes = [2, 3, 5, 7, 13, 17]  # , 19, 31]
        return any((2 ** (prime - 1)) * ((2 ** prime) - 1) == num for prime in primes) if num > 2 else False

    def checkPerfectNumber(self, num: int) -> bool:  # O(1) time and O(1) space
        return num in {6, 28, 496, 8128, 33550336, 8589869056}


def test():
    arguments = [28, 6, 496, 8128, 2]
    expectations = [True, True, True, True, False]
    for num, expected in zip(arguments, expectations):
        solution = Solution().checkPerfectNumber(num)
        assert solution == expected
