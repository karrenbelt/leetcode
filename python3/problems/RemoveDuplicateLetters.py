### Source : https://leetcode.com/problems/remove-duplicate-letters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given a string s, remove duplicate letters so that every letter appears once and only once. You 
# must make sure your result is the smallest in lexicographical order among all possible results.
# 
# Note: This question is the same as 1081: 
# https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/
# 
# Example 1:
# 
# Input: s = "bcabc"
# Output: "abc"
# 
# Example 2:
# 
# Input: s = "cbacdcbc"
# Output: "acdb"
# 
# Constraints:
# 
# 	1 <= s.length <= 104
# 	s consists of lowercase English letters.
#####################################################################################################


class Solution:
    # same as leetcode question 1081
    def removeDuplicateLetters(self, s: str) -> str:  # O(n) time
        last = {c: i for i, c in enumerate(s)}
        stack = []
        for i, c in enumerate(s):
            if c in stack:
                continue
            while stack and stack[-1] > c and i < last[stack[-1]]:
                stack.pop()
            stack.append(c)
        return "".join(stack)

    def removeDuplicateLetters(self, s: str) -> str:
        for c in sorted(set(s)):
            suffix = s[s.index(c):]
            if set(suffix) == set(s):
                return c + self.removeDuplicateLetters(suffix.replace(c, ''))
        return ''

def test():
    arguments = ["bcabc", "cbacdcbc"]
    expectations = ["abc", "acdb"]
    for s, expected in zip(arguments, expectations):
        solution = Solution().removeDuplicateLetters(s)
        assert solution == expected
