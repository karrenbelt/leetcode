### Source : https://leetcode.com/problems/find-words-that-can-be-formed-by-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-07

#####################################################################################################
#
# You are given an array of strings words and a string chars.
#
# A string is good if it can be formed by characters from chars (each character can only be used
# once).
#
# Return the sum of lengths of all good strings in words.
#
# Example 1:
#
# Input: words = ["cat","bt","hat","tree"], chars = "atach"
# Output: 6
# Explanation:
# The strings that can be formed are "cat" and "hat" so the answer is 3 + 3 = 6.
#
# Example 2:
#
# Input: words = ["hello","world","leetcode"], chars = "welldonehoneyr"
# Output: 10
# Explanation:
# The strings that can be formed are "hello" and "world" so the answer is 5 + 5 = 10.
#
# Note:
#
# 	1 <= words.length <= 1000
# 	1 <= words[i].length, chars.length <= 100
# 	All strings contain lowercase English letters only.
#####################################################################################################

from typing import List


class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        from collections import Counter
        return sum(len(word) for word in words if not Counter(word) - available) if (available := Counter(chars)) else 0


def test():
    arguments = [
        (["cat", "bt", "hat", "tree"], "atach"),
        (["hello", "world", "leetcode"], "welldonehoneyr"),
    ]
    expectations = [6, 10]
    for (words, chars), expected in zip(arguments, expectations):
        solution = Solution().countCharacters(words, chars)
        assert solution == expected
