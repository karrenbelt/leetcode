### Source : https://leetcode.com/problems/stone-game/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-11

##################################################################################################### 
#
# Alex and Lee play a game with piles of stones.  There are an even number of piles arranged in a 
# row, and each pile has a positive integer number of stones piles[i].
# 
# The objective of the game is to end with the most stones.  The total number of stones is odd, so 
# there are no ties.
# 
# Alex and Lee take turns, with Alex starting first.  Each turn, a player takes the entire pile of 
# stones from either the beginning or the end of the row.  This continues until there are no more 
# piles left, at which point the person with the most stones wins.
# 
# Assuming Alex and Lee play optimally, return True if and only if Alex wins the game.
# 
# Example 1:
# 
# Input: piles = [5,3,4,5]
# Output: true
# Explanation: 
# Alex starts first, and can only take the first 5 or the last 5.
# Say he takes the first 5, so that the row becomes [3, 4, 5].
# If Lee takes 3, then the board is [4, 5], and Alex takes 5 to win with 10 points.
# If Lee takes the last 5, then the board is [3, 4], and Alex takes 4 to win with 9 points.
# This demonstrated that taking the first 5 was a winning move for Alex, so we return true.
# 
# Constraints:
# 
# 	2 <= piles.length <= 500
# 	piles.length is even.
# 	1 <= piles[i] <= 500
# 	sum(piles) is odd.
#####################################################################################################

from typing import List


class Solution:
    def stoneGame(self, piles: List[int]):

        def dfs(i: int, j: int, is_alex: bool, alex: int, lee: int) -> bool:
            key = (i, j, is_alex)
            if key in memo:
                return memo[key]
            elif i == j:  # even number of piles, so it's always Lee that gets the final pile
                memo[key] = alex > lee + piles[i]
                return memo[key]
            elif is_alex:
                left = dfs(i + 1, j, False, alex + piles[i], lee)
                right = dfs(i, j - 1, False, alex + piles[j], lee)
            else:
                left = dfs(i + 1, j, True, alex, lee + piles[i])
                right = dfs(i, j - 1, True, alex, lee + piles[j])
            memo[key] = left or right
            return memo[key]

        memo = {}
        return dfs(0, len(piles) - 1, True, 0, 0)

    def stoneGame(self, piles: List[int]) -> bool:
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, j: int) -> int:
            if i == j:
                return 0
            if i == j - 1 and j < len(piles):
                return piles[i]
            return max(piles[i] + min(dfs(i + 2, j), dfs(i + 1, j - 1)),
                       piles[j - 1] + min(dfs(i + 1, j - 1), dfs(i, j - 2)))

        alex = dfs(0, len(piles))
        return alex > sum(piles) - alex

    def stoneGame(self, piles: List[int]) -> bool:
        dp = [[0] * len(piles) for _ in range(len(piles))]
        for i in range(len(piles)):
            dp[i][i] = piles[i]
        for d in range(1, len(piles)):
            for i in range(len(piles) - d):
                dp[i][i + d] = max(piles[i] - dp[i + 1][i + d], piles[i + d] - dp[i][i + d - 1])
        return dp[0][-1] > 0

    def stoneGame(self, piles: List[int]) -> bool:
        dp = piles[:]
        for d in range(1, len(piles)):
            for i in range(len(piles) - d):
                dp[i] = max(piles[i] - dp[i + 1], piles[i + d] - dp[i])
        return dp[0] > 0

    def stoneGame(self, piles: List[int]) -> bool:  # O(1) time and O(1) space
        return True


def test():
    arguments = [
        [5, 3, 4, 5],
    ]
    expectations = [True]
    for piles, expected in zip(arguments, expectations):
        solution = Solution().stoneGame(piles)
        assert solution == expected
