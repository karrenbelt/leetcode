### Source : https://leetcode.com/problems/matchsticks-to-square/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-19

##################################################################################################### 
#
# You are given an integer array matchsticks where matchsticks[i] is the length of the ith 
# matchstick. You want to use all the matchsticks to make one square. You should not break any stick, 
# but you can link them up, and each matchstick must be used exactly one time.
# 
# Return true if you can make this square and false otherwise.
# 
# Example 1:
# 
# Input: matchsticks = [1,1,2,2,2]
# Output: true
# Explanation: You can form a square with length 2, one side of the square came two sticks with 
# length 1.
# 
# Example 2:
# 
# Input: matchsticks = [3,3,3,3,4]
# Output: false
# Explanation: You cannot find a way to form a square with all the matchsticks.
# 
# Constraints:
# 
# 	1 <= matchsticks.length <= 15
# 	0 <= matchsticks[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def makesquare(self, matchsticks: List[int]) -> bool:  # 1392 ms
        side_length, remainder = divmod(sum(matchsticks), 4)
        if len(matchsticks) < 3 or remainder:
            return False

        def backtracking(matches: List[int], i: int, path: List[int]) -> bool:
            if i == len(matches):
                return True
            for j in range(4):
                if matches[i] <= path[j]:
                    path[j] -= matches[i]
                    if backtracking(matches, i + 1, path):
                        return True
                    path[j] += matches[i]
            return False

        matchsticks.sort(reverse=True)  # otherwise TLE: 133 / 173 test cases passed.
        return backtracking(matchsticks, 0, [side_length] * 4)

    def makesquare(self, matchsticks: List[int]) -> bool:  # O(2^n) time: 588 ms (2620ms without memo)
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def backtracking(i: int, sides):
            if i == len(matchsticks):
                return not any(sides)
            return any(backtracking(i + 1, sides[:j] + (sides[j] - matchsticks[i],) + sides[j + 1:])
                       for j in range(4) if matchsticks[i] <= sides[j])  # if to trim the space

        side_length, remainder = divmod(sum(matchsticks), 4)
        matchsticks.sort(reverse=True)  # sort so we can abort the recursion faster when when surpassing side length
        return False if len(matchsticks) < 3 or remainder else backtracking(0, tuple([side_length] * 4))


def test():
    arguments = [
        [1, 1, 2, 2, 2],
        [3, 3, 3, 3, 4],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 30, 39, 40, 40],
        [10, 6, 5, 5, 5, 3, 3, 3, 2, 2, 2, 2],
    ]
    expectations = [True, False, True, True]
    for matchsticks, expected in zip(arguments, expectations):
        solution = Solution().makesquare(matchsticks)
        assert solution == expected


test()
