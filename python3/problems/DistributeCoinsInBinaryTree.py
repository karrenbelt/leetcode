### Source : https://leetcode.com/problems/distribute-coins-in-binary-tree/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-02

##################################################################################################### 
#
# You are given the root of a binary tree with n nodes where each node in the tree has node.val 
# coins. There are n coins in total throughout the whole tree.
# 
# In one move, we may choose two adjacent nodes and move one coin from one node to another. A move 
# may be from parent to child, or from child to parent.
# 
# Return the minimum number of moves required to make every node have exactly one coin.
# 
# Example 1:
# 
# Input: root = [3,0,0]
# Output: 2
# Explanation: From the root of the tree, we move one coin to its left child, and one coin to its 
# right child.
# 
# Example 2:
# 
# Input: root = [0,3,0]
# Output: 3
# Explanation: From the left child of the root, we move two coins to the root [taking two moves]. 
# Then, we move one coin from the root of the tree to the right child.
# 
# Example 3:
# 
# Input: root = [1,0,2]
# Output: 2
# 
# Example 4:
# 
# Input: root = [1,0,0,null,3]
# Output: 4
# 
# Constraints:
# 
# 	The number of nodes in the tree is n.
# 	1 <= n <= 100
# 	0 <= Node.val <= n
# 	The sum of all Node.val is n.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def distributeCoins(self, root: TreeNode) -> int:

        def traverse(node: TreeNode) -> int:
            nonlocal moves
            if not node:
                return 0
            left = traverse(node.left)
            right = traverse(node.right)
            moves += abs(left) + abs(right)
            return node.val + left + right - 1

        moves = 0
        traverse(root)
        return moves

    def distributeCoins(self, root: TreeNode) -> int:
        # post-order traversal so we visit the children of each node first
        # if the difference between node.val and 1 is the amount we need to move to / from the parent
        # this difference we propagate to the parent node
        def traverse(node: TreeNode):
            if not node:  # base case: leave requires no redistribution of coins
                return 0, 0
            left_moves, left_difference = traverse(node.left)
            right_moves, right_difference = traverse(node.right)
            difference = node.val + left_difference + right_difference - 1
            moves = left_moves + right_moves + abs(left_difference) + abs(right_difference)
            return moves, difference

        return traverse(root)[0]


def test():
    from python3 import Codec
    arguments = ["[3,0,0]", "[0,3,0]", "[1,0,2]", "[1,0,0,null,3]"]
    expectations = [2, 3, 2, 4]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().distributeCoins(root)
        assert solution == expected
