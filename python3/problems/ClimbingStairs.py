### Source : https://leetcode.com/problems/climbing-stairs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# You are climbing a staircase. It takes n steps to reach the top.
# 
# Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
# 
# Example 1:
# 
# Input: n = 2
# Output: 2
# Explanation: There are two ways to climb to the top.
# 1. 1 step + 1 step
# 2. 2 steps
# 
# Example 2:
# 
# Input: n = 3
# Output: 3
# Explanation: There are three ways to climb to the top.
# 1. 1 step + 1 step + 1 step
# 2. 1 step + 2 steps
# 3. 2 steps + 1 step
# 
# Constraints:
# 
# 	1 <= n <= 45
#####################################################################################################


class Solution:
    def climbStairs(self, n: int) -> int:  # O(n) time and O(n) space
        #
        def climb(x: int):
            if x in d:
                return d[x]
            d[x] = climb(x - 1) + climb(x - 2)
            return d[x]

        d = {0: 0, 1: 1, 2: 2}  # 3, 5, 8 -->  almost like fibonacci
        return climb(n)

    def climbStairs(self, n: int) -> int:  # O(n) time and O(n) space
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def climb(x: int):
            return x if x <= 2 else climb(x - 1) + climb(x - 2)

        return climb(n)

    def climbStairs(self, n: int) -> int:  # O(n) time and O(n) space
        memo = {0: 0, 1: 1, 2: 2}
        for i in range(3, n + 1):
            memo[i] = memo[i - 1] + memo[i - 2]
        return memo[n]

    def climbStairs(self, n: int) -> int:  # O(n) time and O(1) space
        a, b = 0, 1
        for _ in range(2, n + 1):
            a, b = b, a + b
        return a + b

    def climbStairs(self, n: int) -> int:  # O(n) time and O(1) space
        a, b = 0, 1  # , 2, 3, 5, 8
        for _ in range(n - 1):
            a, b = b, a + b
        return a + b

    def climbStairs(self, n: int) -> int:  # closed-form solution
        # https://en.wikipedia.org/wiki/Fibonacci_number#Closed-form_expression
        return int((5 ** .5 / 5) * (((1 + 5 ** .5) / 2) ** (n + 1) - ((1 - 5 ** .5) / 2) ** (n + 1)))


def test():
    numbers = [2, 3, 4, 5]
    expectations = [2, 3, 5, 8]
    for n, expected in zip(numbers, expectations):
        solution = Solution().climbStairs(n)
        assert solution == expected
