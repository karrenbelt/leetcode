### Source : https://leetcode.com/problems/rearrange-words-in-a-sentence/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-16

##################################################################################################### 
#
# Given a sentence text (A sentence is a string of space-separated words) in the following format:
# 
# 	First letter is in upper case.
# 	Each word in text are separated by a single space.
# 
# Your task is to rearrange the words in text such that all words are rearranged in an increasing 
# order of their lengths. If two words have the same length, arrange them in their original order.
# 
# Return the new text following the format shown above.
# 
# Example 1:
# 
# Input: text = "Leetcode is cool"
# Output: "Is cool leetcode"
# Explanation: There are 3 words, "Leetcode" of length 8, "is" of length 2 and "cool" of length 4.
# Output is ordered by length and the new first word starts with capital letter.
# 
# Example 2:
# 
# Input: text = "Keep calm and code on"
# Output: "On and keep calm code"
# Explanation: Output is ordered as follows:
# "On" 2 letters.
# "and" 3 letters.
# "keep" 4 letters in case of tie order by position in original text.
# "calm" 4 letters.
# "code" 4 letters.
# 
# Example 3:
# 
# Input: text = "To be or not to be"
# Output: "To be or to be not"
# 
# Constraints:
# 
# 	text begins with a capital letter and then contains lowercase letters and single space 
# between words.
# 	1 <= text.length <= 105
#####################################################################################################

class Solution:
    def arrangeWords(self, text: str) -> str:
        from itertools import count
        c = count()
        ordered = sorted(text.split(), key=lambda x: (len(x), next(c)))
        return " ".join(ordered).lower().capitalize()

    def arrangeWords(self, text: str) -> str:
        ordered = sorted(enumerate(text.split()), key=lambda x: (len(x[1]), x[0]))
        return " ".join(w for _, w in ordered).lower().capitalize()

    def arrangeWords(self, text: str) -> str:
        # 1. sort is stable, 2. capitalize takes care of original capital
        return " ".join(sorted(text.split(), key=len)).capitalize()


def test():
    strings = [
        "Leetcode is cool",
        "Keep calm and code on",
        "To be or not to be",
    ]
    expectations = [
        "Is cool leetcode",
        "On and keep calm code",
        "To be or to be not",
    ]
    for text, expected in zip(strings, expectations):
        solution = Solution().arrangeWords(text)
        assert solution == expected

