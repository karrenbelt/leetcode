### Source : https://leetcode.com/problems/merge-k-sorted-lists/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-03

##################################################################################################### 
#
# You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
# 
# erge all the linked-lists into one sorted linked-list and return it.
# 
# Example 1:
# 
# Input: lists = [[1,4,5],[1,3,4],[2,6]]
# Output: [1,1,2,3,4,4,5,6]
# Explanation: The linked-lists are:
# [
#   1->4->5,
#   1->3->4,
#   2->6
# ]
# merging them into one sorted list:
# 1->1->2->3->4->4->5->6
# 
# Example 2:
# 
# Input: lists = []
# Output: []
# 
# Example 3:
# 
# Input: lists = [[]]
# Output: []
# 
# Constraints:
# 
# 	k == lists.length
# 	0 <= k <= 104
# 	0 <= lists[i].length <= 500
# 	-104 <= lists[i][j] <= 104
# 	lists[i] is sorted in ascending order.
# 	The sum of lists[i].length won't exceed 104.
#####################################################################################################

from typing import List
from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:  # not even TLE

        def mergeTwoLists(l1: ListNode, l2: ListNode) -> ListNode:
            pointer = result = ListNode(None)
            while l1 and l2:
                if l1.val <= l2.val:
                    tmp, l1 = l1, l1.next
                    tmp.next, pointer.next = None, tmp
                else:
                    tmp, l2 = l2, l2.next
                    tmp.next, pointer.next = None, tmp
                pointer = pointer.next
            pointer.next = l1 or l2
            return result.next

        merged = lists[0]
        for i in range(1, len(lists)):
            merged = mergeTwoLists(merged, lists[i])
        return merged

    def mergeKLists(self, lists: List[ListNode]) -> ListNode:  # divide and conquer

        def merge(l1: ListNode, l2: ListNode) -> ListNode:
            pointer = result = ListNode(None)
            while l1 and l2:
                if l1.val <= l2.val:
                    tmp, l1 = l1, l1.next
                    tmp.next, pointer.next = None, tmp
                else:
                    tmp, l2 = l2, l2.next
                    tmp.next, pointer.next = None, tmp
                pointer = pointer.next
            pointer.next = l1 or l2
            return result.next

        if not lists:
            return
        if len(lists) == 1:
            return lists[0]
        mid = len(lists) // 2
        l, r = self.mergeKLists(lists[:mid]), self.mergeKLists(lists[mid:])
        return merge(l, r)

    # def mergeKLists(self, lists: List[ListNode]) -> ListNode:
    #
    #     values = []
    #     for node in lists:
    #         while node:
    #             values.append(node.val)
    #             node = node.next
    #     if not values:
    #         return
    #     values.sort()
    #     node = merged = ListNode(values[0])
    #     for i in range(1, len(values)):
    #         node.next = ListNode(values[i])
    #         node = node.next
    #     return merged
    #
    # def mergeKLists(self, lists: List[ListNode]) -> ListNode:
    #     values = []
    #     for node in lists:
    #         while node:
    #             values.append(node)
    #             node = node.next
    #     if not values:
    #         return
    #     values.sort(key=lambda node: node.val, reverse=True)
    #     node = merged = values.pop()
    #     while values:
    #         node.next = values.pop()
    #         node = node.next
    #     node.next = None
    #     return merged


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [[1, 4, 5], [1, 3, 4], [2, 6]],
        ]
    expectations = [
        [1, 1, 2, 3, 4, 4, 5, 6],
        ]
    for arrays, expected in zip(arguments, expectations):
        lists = list(map(lambda nums: SinglyLinkedList(nums).head, arrays))
        solution = Solution().mergeKLists(lists)
        assert solution == SinglyLinkedList(expected).head
