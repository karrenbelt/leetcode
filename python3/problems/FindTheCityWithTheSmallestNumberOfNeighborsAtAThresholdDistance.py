### Source : https://leetcode.com/problems/find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-17

##################################################################################################### 
#
# There are n cities numbered from 0 to n-1. Given the array edges where edges[i] = [fromi, toi, 
# weighti] represents a bidirectional and weighted edge between cities fromi and toi, and given the 
# integer distanceThreshold.
# 
# Return the city with the smallest number of cities that are reachable through some path and whose 
# distance is at most distanceThreshold, If there are multiple such cities, return the city with the 
# greatest number.
# 
# Notice that the distance of a path connecting cities i and j is equal to the sum of the edges' 
# weights along that path.
# 
# Example 1:
# 
# Input: n = 4, edges = [[0,1,3],[1,2,1],[1,3,4],[2,3,1]], distanceThreshold = 4
# Output: 3
# Explanation: The figure above describes the graph. 
# The neighboring cities at a distanceThreshold = 4 for each city are:
# City 0 -> [City 1, City 2] 
# City 1 -> [City 0, City 2, City 3] 
# City 2 -> [City 0, City 1, City 3] 
# City 3 -> [City 1, City 2] 
# Cities 0 and 3 have 2 neighboring cities at a distanceThreshold = 4, but we have to return city 3 
# since it has the greatest number.
# 
# Example 2:
# 
# Input: n = 5, edges = [[0,1,2],[0,4,8],[1,2,3],[1,4,2],[2,3,1],[3,4,1]], distanceThreshold = 2
# Output: 0
# Explanation: The figure above describes the graph. 
# The neighboring cities at a distanceThreshold = 2 for each city are:
# City 0 -> [City 1] 
# City 1 -> [City 0, City 4] 
# City 2 -> [City 3, City 4] 
# City 3 -> [City 2, City 4]
# City 4 -> [City 1, City 2, City 3] 
# The city 0 has 1 neighboring city at a distanceThreshold = 2.
# 
# Constraints:
# 
# 	2 <= n <= 100
# 	1 <= edges.length <= n * (n - 1) / 2
# 	edges[i].length == 3
# 	0 <= fromi < toi < n
# 	1 <= weighti, distanceThreshold <= 104
# 	All pairs (fromi, toi) are distinct.
#####################################################################################################

from typing import List


class Solution:
    def findTheCity(self, n: int, edges: List[List[int]], distanceThreshold: int) -> int:
        import heapq

        graph, cost = {}, [[(1 << 31) - 1] * n for _ in range(n)]
        for u, v, w in edges:
            graph.setdefault(u, []).append(v)
            graph.setdefault(v, []).append(u)
            cost[u][v] = cost[v][u] = w

        def dijkstra(city: int) -> int:

            distances = [(1 << 31) - 1] * n
            distances[city], heap = 0, [(0, city)]

            while heap:
                distance_travelled, u = heapq.heappop(heap)
                if distance_travelled > distances[u]:
                    continue
                for v in graph.get(u, []):
                    this_cost = distance_travelled + cost[u][v]
                    if this_cost < distances[v]:
                        distances[v] = this_cost
                        heapq.heappush(heap, (this_cost, v))

            return sum(d <= distanceThreshold for d in distances) - 1

        city_neighbors = dict(zip(map(dijkstra, range(n)), range(n)))
        return city_neighbors[min(city_neighbors)]

    def findTheCity(self, n: int, edges: List[List[int]], distanceThreshold: int) -> int:  # O(n^3) time & O(n^2) space
        # Floyd–Warshall algorithm
        from itertools import product
        r = range(n)
        matrix = [[(1 << 31) - 1] * n for _ in r]
        for u, v, w in edges:
            matrix[u][v] = matrix[v][u] = w
        for u in r:
            matrix[u][u] = 0
        for k, u, v in product(r, r, r):
            matrix[u][v] = min(matrix[u][v], matrix[u][k] + matrix[k][v])
        res = {sum(d <= distanceThreshold for d in matrix[i]): i for i in r}
        return res[min(res)]


def test():
    strings = [
        (4, [[0, 1, 3], [1, 2, 1], [1, 3, 4], [2, 3, 1]], 4),
        (5, [[0, 1, 2], [0, 4, 8], [1, 2, 3], [1, 4, 2], [2, 3, 1], [3, 4, 1]], 2),
        (6, [[0, 3, 7], [2, 4, 1], [0, 1, 5], [2, 3, 10], [1, 3, 6], [1, 2, 1]], 417),
        (4, [[0, 2, 7732], [1, 3, 7714], [0, 1, 4106], [0, 3, 7084]], 8439),
    ]
    expectations = [3, 0, 5, 2]
    for (n, edges, distanceThreshold), expected in zip(strings, expectations):
        solution = Solution().findTheCity(n, edges, distanceThreshold)
        print(solution)
        assert solution == expected
