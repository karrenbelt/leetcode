### Source : https://leetcode.com/problems/excel-sheet-column-title/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-26

##################################################################################################### 
#
# Given a positive integer, return its corresponding column title as appear in an Excel sheet.
# 
# For example:
# 
#     1 -> A
#     2 -> B
#     3 -> C
#     ...
#     26 -> Z
#     27 -> AA
#     28 -> AB 
#     ...
# 
# Example 1:
# 
# Input: 1
# Output: "A"
# 
# Example 2:
# 
# Input: 28
# Output: "AB"
# 
# Example 3:
# 
# Input: 701
# Output: "ZY"
#####################################################################################################


class Solution:
    def convertToTitle(self, n: int) -> str:
        base = 26
        letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        digits = []
        while n:
            digits.append(letters[(n-1) % base])
            n //= base
        return digits[::-1]

    def convertToTitle(self, n: int) -> str:
        base = 26
        letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        result = []
        while n:
            result.append(letters[(n-1) % base])
            n = (n-1) // base
        return ''.join(result[::-1])

    def convertToTitle(self, n: int) -> str:
        res = ''
        base = ord('A')  # 65
        while n:
            n, r = divmod(n - 1, 26)  # remainder
            res = '{}{}'.format(chr(base + r), res)
        return res

    def convertToTitle(self, n: int) -> str:
        r = ''
        while n:
            n, i = divmod(n - 1, 26)
            r = chr(65 + i) + r
        return r


def test():
    numbers = [1, 28, 701]
    expectations = ["A", "AB", "ZY"]
    for n, expected in zip(numbers, expectations):
        solution = Solution().convertToTitle(n)
        assert solution == expected
