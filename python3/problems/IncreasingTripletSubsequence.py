### Source : https://leetcode.com/problems/increasing-triplet-subsequence/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-15

##################################################################################################### 
#
# Given an integer array nums, return true if there exists a triple of indices (i, j, k) such that i 
# < j < k and nums[i] < nums[j] < nums[k]. If no such indices exists, return false.
# 
# Example 1:
# 
# Input: nums = [1,2,3,4,5]
# Output: true
# Explanation: Any triplet where i < j < k is valid.
# 
# Example 2:
# 
# Input: nums = [5,4,3,2,1]
# Output: false
# Explanation: No triplet exists.
# 
# Example 3:
# 
# Input: nums = [2,1,5,0,4,6]
# Output: true
# Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4] == 4 < nums[5] == 6.
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-231 <= nums[i] <= 231 - 1
# 
# Follow up: Could you implement a solution that runs in O(n) time complexity and O(1) space 
# complexity?
#####################################################################################################

from typing import List


class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:  # brute force: O(n^3)
        return any(nums[i] < nums[j] < nums[k]
                   for i in range(len(nums)) for j in range(i + 1, len(nums)) for k in range(j + 1, len(nums)))

    def increasingTriplet(self, nums: List[int]) -> bool:  # O(n log n) time

        def binary_search(array, left, right, target):
            while left < right:
                mid = left + (right - left) // 2
                if array[mid] >= target:
                    right = mid
                else:
                    left = mid + 1
            return left

        dp = [nums[0]] + [0] * len(nums)
        j = 1
        for i in range(1, len(nums)):
            if nums[i] < dp[0]:
                dp[0] = nums[i]
            elif nums[i] > dp[j - 1]:
                dp[j] = nums[i]
                j += 1
            else:
                dp[binary_search(dp, -1, j - 1, nums[i])] = nums[i]

        return j > 2

    def increasingTriplet(self, nums: List[int]) -> bool:  # O(n) time and O(1) space
        first = second = float('inf')
        for n in nums:
            if n <= first:
                first = n
            elif n <= second:
                second = n
            else:
                return True
        return False

    def increasingTriplet(self, nums: List[int]) -> bool:  # generalized: O(n log n) time and O(k) space
        import bisect
        k = 3
        inc = [float('inf')] * (k - 1)
        for n in nums:
            i = bisect.bisect_left(inc, n)
            if i == k - 1:
                return True
            inc[i] = n
        return False


def test():
    arguments = [
        [1, 2, 3, 4, 5],
        [5, 4, 3, 2, 1],
        [2, 1, 5, 0, 4, 6],
        [1, 2, 0, 1, 3],
        [1, 2, 0, 1, 2],
    ]
    expectations = [True, False, True, True, True]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().increasingTriplet(nums)
        assert solution == expected
