### Source : https://leetcode.com/problems/convert-to-base-2/
### Author : M.A.P. Karrenbelt
### Date   : 2021-11-10

##################################################################################################### 
#
# Given an integer n, return a binary string representing its representation in base -2.
# 
# Note that the returned string should not have leading zeros unless the string is "0".
# 
# Example 1:
# 
# Input: n = 2
# Output: "110"
# Explantion: (-2)2 + (-2)1 = 2
# 
# Example 2:
# 
# Input: n = 3
# Output: "111"
# Explantion: (-2)2 + (-2)1 + (-2)0 = 3
# 
# Example 3:
# 
# Input: n = 4
# Output: "100"
# Explantion: (-2)2 = 4
# 
# Constraints:
# 
# 	0 <= n <= 109
#####################################################################################################


class Solution:
    def baseNeg2(self, n: int) -> str:  # O(log n) time
        binary = ''
        while n:
            binary += str(n & 1)
            n = -(n >> 1)
        return binary[::-1] if binary else "0"

    def baseNeg2(self, n: int) -> str:  # O(log n) time
        return str(n) if n in {0, 1} else self.baseNeg2(-(n >> 1)) + str(n & 1)


def test():
    arguments = [2, 3, 4, 0]
    expectations = ["110", "111", "100", "0"]
    for n, expected in zip(arguments, expectations):
        solution = Solution().baseNeg2(n)
        assert solution == expected
