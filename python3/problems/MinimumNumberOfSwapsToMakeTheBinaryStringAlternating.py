### Source : https://leetcode.com/problems/minimum-number-of-swaps-to-make-the-binary-string-alternating/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

##################################################################################################### 
#
# Given a binary string s, return the minimum number of character swaps to make it alternating, or -1 
# if it is impossible.
# 
# The string is called alternating if no two adjacent characters are equal. For example, the strings 
# "010" and "1010" are alternating, while the string "0100" is not.
# 
# Any two characters may be swapped, even if they are not adjacent.
# 
# Example 1:
# 
# Input: s = "111000"
# Output: 1
# Explanation: Swap positions 1 and 4: "111000" -> "101010"
# The string is now alternating.
# 
# Example 2:
# 
# Input: s = "010"
# Output: 0
# Explanation: The string is already alternating, no swaps are needed.
# 
# Example 3:
# 
# Input: s = "1110"
# Output: -1
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s[i] is either '0' or '1'.
#####################################################################################################


class Solution:
    def minSwaps(self, s: str) -> int:  # O(n) time and O(1) space, single pass
        # we count zeros and one, starting from each byte (two zero-one byte counters to find minimum in single pass)
        t, n, m = 0, [0, 0], [0, 0]
        for byte in map(int, s):
            if byte ^ t:
                n[byte] += 1
            else:
                m[byte] += 1
            t = 1 ^ t
        minimum = min(n[0] if n[0] == n[1] else 1_000, m[0] if m[0] == m[1] else 1_000)
        return minimum if minimum < 1000 else -1


def test():
    arguments = [
        "111000",
        "010",
        "1110",
        "11110001100",  # "0101 010 1010" -> 3 flipped
        "1",
    ]
    expectations = [1, 0, -1, 3, 0]
    for s, expected in zip(arguments, expectations):
        solution = Solution().minSwaps(s)
        assert solution == expected
