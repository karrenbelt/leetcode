### Source : https://leetcode.com/problems/longest-substring-with-at-least-k-repeating-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-26

##################################################################################################### 
#
# Given a string s and an integer k, return the length of the longest substring of s such that the 
# frequency of each character in this substring is greater than or equal to k.
# 
# Example 1:
# 
# Input: s = "aaabb", k = 3
# Output: 3
# Explanation: The longest substring is "aaa", as 'a' is repeated 3 times.
# 
# Example 2:
# 
# Input: s = "ababbc", k = 2
# Output: 5
# Explanation: The longest substring is "ababb", as 'a' is repeated 2 times and 'b' is repeated 3 
# times.
# 
# Constraints:
# 
# 	1 <= s.length <= 104
# 	s consists of only lowercase English letters.
# 	1 <= k <= 105
#####################################################################################################


class Solution:
    def longestSubstring(self, s: str, k: int) -> int:  # O(mn)
        from collections import Counter

        longest = 0
        for i in range(1, len(set(s)) + 1):
            counter = Counter()
            left = right = 0
            while right < len(s):
                if len(counter) > i:
                    counter[s[left]] -= 1
                    if not counter[s[left]]:
                        del counter[s[left]]
                    left += 1
                else:
                    counter[s[right]] += 1
                    right += 1
                    if min(counter.values()) >= k:
                        longest = max(longest, right - left)
        return longest

    def longestSubstring(self, s, k):
        from collections import Counter
        counts = Counter(s)
        for c, v in counts.items():
            if v < k:
                return max((self.longestSubstring(sub, k) for sub in s.split(c) if len(sub) >= k), default=0)
        return len(s)

    def longestSubstring(self, s: str, k: int) -> int:
        for c in set(s):
            if s.count(c) < k:
                return max(self.longestSubstring(t, k) for t in s.split(c))
        return len(s)

    def longestSubstring(self, s: str, k: int) -> int:
        stack, longest = [s], 0
        while stack:
            s = stack.pop()
            for c in set(s):
                if s.count(c) < k:
                    stack.extend(s.split(c))
                    break
            else:
                longest = max(longest, len(s))
        return longest


def test():
    arguments = [
        ("aaabb", 3),
        ("ababbc", 2),
    ]
    expectations = [3, 5]
    for (s, k), expected in zip(arguments, expectations):
        solution = Solution().longestSubstring(s, k)
        assert solution == expected
