### Source : https://leetcode.com/problems/special-positions-in-a-binary-matrix/
### Author : M.A.P. Karrenbelt
### Date   : 2023-02-13

##################################################################################################### 
#
# Given an m x n binary matrix mat, return the number of special positions in mat.
# 
# A position (i, j) is called special if mat[i][j] == 1 and all other elements in row i and column j 
# are 0 (rows and columns are 0-indexed).
# 
# Example 1:
# 
# Input: mat = [[1,0,0],[0,0,1],[1,0,0]]
# Output: 1
# Explanation: (1, 2) is a special position because mat[1][2] == 1 and all other elements in row 1 
# and column 2 are 0.
# 
# Example 2:
# 
# Input: mat = [[1,0,0],[0,1,0],[0,0,1]]
# Output: 3
# Explanation: (0, 0), (1, 1) and (2, 2) are special positions.
# 
# Constraints:
# 
# 	m == mat.length
# 	n == mat[i].length
# 	1 <= m, n <= 100
# 	mat[i][j] is either 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def numSpecial(self, mat: List[List[int]]) -> int:
        ctr = 0
        for i, row in enumerate(mat):
            for j, col in enumerate(zip(*mat)):
                ctr += mat[i][j] == sum(row) == sum(col) == 1
        return ctr

    def numSpecial(self, mat: List[List[int]]) -> int:  # O(m * n) time
        from itertools import product, chain
        zipper = zip(chain(*mat), product(mat, zip(*mat)))
        return sum(n == sum(r) == sum(c) == 1 for n, (r, c) in zipper)


def test():
    arguments = [
        [
            [1, 0, 0],
            [0, 0, 1],
            [1, 0, 0],
        ],
        [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
        ],
        [
            [0, 0, 1, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 1, 0, 0],
         ],
        [
            [0, 1],
        ],
        [
            [0],
            [1],
        ],
    ]
    expectations = [1, 3, 2, 1, 1]
    for mat, expected in zip(arguments, expectations):
        solution = Solution().numSpecial(mat)
        assert solution == expected
