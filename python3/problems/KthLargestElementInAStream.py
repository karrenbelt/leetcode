### Source : https://leetcode.com/problems/kth-largest-element-in-a-stream/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-30

##################################################################################################### 
#
# Design a class to find the kth largest element in a stream. Note that it is the kth largest element 
# in the sorted order, not the kth distinct element.
# 
# Implement KthLargest class:
# 
# 	KthLargest(int k, int[] nums) Initializes the object with the integer k and the stream of 
# integers nums.
# 	int add(int val) Returns the element representing the kth largest element in the stream.
# 
# Example 1:
# 
# Input
# ["KthLargest", "add", "add", "add", "add", "add"]
# [[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
# Output
# [null, 4, 5, 5, 8, 8]
# 
# Explanation
# KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
# kthLargest.add(3);   // return 4
# kthLargest.add(5);   // return 5
# kthLargest.add(10);  // return 5
# kthLargest.add(9);   // return 8
# kthLargest.add(4);   // return 8
# 
# Constraints:
# 
# 	1 <= k <= 104
# 	0 <= nums.length <= 104
# 	-104 <= nums[i] <= 104
# 	-104 <= val <= 104
# 	At most 104 calls will be made to add.
# 	It is guaranteed that there will be at least k elements in the array when you search for 
# the kth element.
#####################################################################################################

import heapq
from typing import List
from python3 import BinaryTreeNode as TreeNode


class Node(TreeNode):

    def __init__(self, val: int, left=None, right=None):
        super().__init__(val, left, right)
        self.count = 1
        self.left_subtree_count = 0
        self.right_subtree_count = 0


class KthLargest:
    # NOTE: a heap would be good for this, so would binary search by, but we are doing BST here
    def __init__(self, k: int, nums: List[int]):
        # It is guaranteed that there will be at least k elements in the array when you search for the kth element.
        # hence -> there will be nums, since 1 <= k <= 10^4
        self.k = k
        self.root = None
        for num in nums:
            self._insert(num)

    def _insert(self, val):

        def insert(node: Node):
            if not node:
                return Node(val)
            if val == node.val:
                node.count += 1
            elif val < node.val:
                if node.left:
                    insert(node.left)
                else:
                    node.left = Node(val)
                node.left_subtree_count += 1
            else:
                if node.right:
                    insert(node.right)
                else:
                    node.right = Node(val)
                node.right_subtree_count += 1

        if not self.root:
            self.root = Node(val)
        else:
            insert(self.root)

    @property
    def _kth_largest(self):

        def kth_largest(node: Node, k: int):
            if not node:
                return 'error'
                # raise ValueError(f'kth-largest {self.k} does not exist')

            if node.count + node.right_subtree_count >= k > node.right_subtree_count:
                return node.val
            elif k <= node.right_subtree_count:
                return kth_largest(node.right, k)
            else:
                return kth_largest(node.right, k - node.count - node.right_subtree_count)

        return kth_largest(self.root, self.k)

    def add(self, val: int) -> int:
        self._insert(val)
        return self._kth_largest


class KthLargest(object):

    def __init__(self, k, nums):
        self.pool = nums
        self.k = k
        heapq.heapify(self.pool)
        while len(self.pool) > k:
            heapq.heappop(self.pool)

    def add(self, val):
        if len(self.pool) < self.k:
            heapq.heappush(self.pool, val)
        # elif val > self.pool[0]:
        #     heapq.heapreplace(self.pool, val)
        else:  # faster than heapq.heapreplace
            heapq.heappushpop(self.pool, val)
        return self.pool[0]

# Your KthLargest object will be instantiated and called as such:
# obj = KthLargest(k, nums)
# param_1 = obj.add(val)


def test():
    operations = ["KthLargest", "add", "add", "add", "add", "add"]
    arguments = [[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
    expectations = [None, 4, 5, 5, 8, 8]
    obj = KthLargest(*arguments[0])
    for method, args, expected in zip(operations[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
