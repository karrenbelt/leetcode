### Source : https://leetcode.com/problems/maximum-number-of-achievable-transfer-requests/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-18

##################################################################################################### 
#
# We have n buildings numbered from 0 to n - 1. Each building has a number of employees. It's 
# transfer season, and some employees want to change the building they reside in.
# 
# You are given an array requests where requests[i] = [fromi, toi] represents an employee's request 
# to transfer from building fromi to building toi.
# 
# All buildings are full, so a list of requests is achievable only if for each building, the net 
# change in employee transfers is zero. This means the number of employees leaving is equal to the 
# number of employees moving in. For example if n = 3 and two employees are leaving building 0, one 
# is leaving building 1, and one is leaving building 2, there should be two employees moving to 
# building 0, one employee moving to building 1, and one employee moving to building 2.
# 
# Return the maximum number of achievable requests.
# 
# Example 1:
# 
# Input: n = 5, requests = [[0,1],[1,0],[0,1],[1,2],[2,0],[3,4]]
# Output: 5
# Explantion: Let's see the requests:
# From building 0 we have employees x and y and both want to move to building 1.
# From building 1 we have employees a and b and they want to move to buildings 2 and 0 respectively.
# From building 2 we have employee z and they want to move to building 0.
# From building 3 we have employee c and they want to move to building 4.
# From building 4 we don't have any requests.
# We can achieve the requests of users x and b by swapping their places.
# We can achieve the requests of users y, a and z by swapping the places in the 3 buildings.
# 
# Example 2:
# 
# Input: n = 3, requests = [[0,0],[1,2],[2,1]]
# Output: 3
# Explantion: Let's see the requests:
# From building 0 we have employee x and they want to stay in the same building 0.
# From building 1 we have employee y and they want to move to building 2.
# From building 2 we have employee z and they want to move to building 1.
# We can achieve all the requests. 
# 
# Example 3:
# 
# Input: n = 4, requests = [[0,3],[3,1],[1,2],[2,0]]
# Output: 4
# 
# Constraints:
# 
# 	1 <= n <= 20
# 	1 <= requests.length <= 16
# 	requests[i].length == 2
# 	0 <= fromi, toi < n
#####################################################################################################

from typing import List, Tuple


class Solution:
    def maximumRequests(self, n: int, requests: List[List[int]]) -> int:  # O((N + R) * 2^R) time
        # we iterate through all combinations and check. TLE: 114 / 117 test cases passed.
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def backtracking(moves: int, balance: Tuple[int], open_requests: Tuple[Tuple[int]]):
            nonlocal maximum
            if all(b == 0 for b in balance):
                maximum = max(maximum, moves)
            for i, (from_i, to_i) in enumerate(open_requests):
                new_balance = list(balance)
                new_balance[from_i] -= 1
                new_balance[to_i] += 1
                backtracking(moves + 1, tuple(new_balance), open_requests[:i] + open_requests[i + 1:])

        maximum = 0
        backtracking(0, tuple([0] * n), tuple(map(tuple, requests)))
        return maximum

    def maximumRequests(self, n: int, requests: List[List[int]]) -> int:  # O((N + R) * 2^R) time
        from functools import lru_cache

        @lru_cache(maxsize=None)
        def dfs(i: int, balance: Tuple[int]) -> int:
            if i == len(requests):
                return 0 if all(b == 0 for b in balance) else float('-inf')
            new_balance = list(balance)
            new_balance[requests[i][0]] -= 1
            new_balance[requests[i][1]] += 1
            return max(1 + dfs(i + 1, tuple(new_balance)), dfs(i + 1, balance))

        return dfs(0, tuple([0] * n))


def test():
    arguments = [
        (5, [[0, 1], [1, 0], [0, 1], [1, 2], [2, 0], [3, 4]]),
        (3, [[0, 0], [1, 2], [2, 1]]),
        (4, [[0, 3], [3, 1], [1, 2], [2, 0]]),
        (2, [[1, 1], [1, 0], [0, 1], [0, 0], [0, 0], [0, 1], [0, 1], [1, 0], [1, 0], [1, 1], [0, 0], [1, 0]]),
        (3, [[1, 2], [1, 2], [2, 2], [0, 2], [2, 1], [1, 1], [1, 2]]),
        (3, [[1, 2], [0, 0], [0, 2], [0, 1], [0, 0], [0, 2], [1, 0], [0, 1], [2, 0]])
    ]
    expectations = [5, 3, 4, 11, 4, 7]
    for (n, requests), expected in zip(arguments, expectations):
        solution = Solution().maximumRequests(n, requests)
        assert solution == expected
