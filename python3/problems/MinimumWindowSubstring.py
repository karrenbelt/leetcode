### Source : https://leetcode.com/problems/minimum-window-substring/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-12

##################################################################################################### 
#
# Given two strings s and t, return the minimum window in s which will contain all the characters in 
# t. If there is no such window in s that covers all characters in t, return the empty string "".
# 
# Note that If there is such a window, it is guaranteed that there will always be only one unique 
# minimum window in s.
# 
# Example 1:
# Input: s = "ADOBECODEBANC", t = "ABC"
# Output: "BANC"
# Example 2:
# Input: s = "a", t = "a"
# Output: "a"
# 
# Constraints:
# 
# 	1 <= s.length, t.length <= 105
# 	s and t consist of English letters.
# 
# Follow up: Could you find an algorithm that runs in O(n) time?
#####################################################################################################


class Solution:
    def minWindow(self, s: str, t: str) -> str:
        from collections import Counter
        if len(t) > len(s):
            return ""

        min_window = ""
        i = start = 0
        target_count = Counter(t)
        window_count = Counter(s[:len(t)])
        for i in range(len(t), len(s)):
            # if we find a window that contains the target, we shrink from the left
            while not target_count - window_count:
                # if we don't have a window yet or it is smaller than our current
                if not min_window or len(min_window) > i - start:
                    min_window = s[start:i]
                window_count[s[start]] -= 1
                if not window_count[s[start]]:
                    del window_count[s[start]]
                start += 1
            # we expand to the right
            window_count[s[i]] = window_count.get(s[i], 0) + 1

        # we need to deal with the final iteration
        while not target_count - window_count:
            if not min_window or len(min_window) > i - start:
                min_window = s[start:]  # NOTE: up to end of string here
            window_count[s[start]] -= 1
            if not window_count[s[start]]:
                del window_count[s[start]]
            start += 1

        return min_window

    def minWindow(self, s: str, t: str) -> str:
        from collections import Counter
        window, needed = Counter(s[:len(t)]), Counter(t)
        i, j, x, y = 0, len(t), 0, 0
        while j < len(s):
            if not needed - window:  # we matched the requirement
                if x == y == 0 or j - i < y - x:
                    x, y = i, j
                window[s[i]] -= 1
                i += 1
            else:
                window[s[j]] += 1
                j += 1
        while not needed - window:  # need to shrink the left is possible
            window[s[i]] -= 1
            if x == y == 0 or j - i < y - x:
                x, y = i, j
            i += 1
        return s[x:y]

    def minWindow(self, s: str, t: str) -> str:
        from collections import Counter  # returns a default value of 0 for non-existing keys
        need, missing = Counter(t), len(t)
        i = start = end = 0
        for j, c in enumerate(s, start=1):
            missing -= need[c] > 0  # if we find one that we need still
            need[c] -= 1
            if not missing:
                while need[s[i]] < 0:  # trim the left side if we found more than we need
                    need[s[i]] += 1
                    i += 1
                if not end or j - i <= end - start:  # if we find a (new) window
                    start, end = i, j
                need[s[i]] += 1
                i += 1  # update next window starting position
                missing += 1
        return s[start: end]


def test():
    arguments = [
        ("ADOBECODEBANC", "ABC"),
        ("a", "a"),
        ("", "asdf"),
        ("abecdxabcd", "acdb"),
    ]
    expectations = ["BANC", "a", "", "abcd"]
    for (s, t), expected in zip(arguments, expectations):
        solution = Solution().minWindow(s, t)
        assert solution == expected
