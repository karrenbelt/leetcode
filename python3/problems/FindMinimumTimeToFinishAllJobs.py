### Source : https://leetcode.com/problems/find-minimum-time-to-finish-all-jobs/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-06

##################################################################################################### 
#
# You are given an integer array jobs, where jobs[i] is the amount of time it takes to complete the 
# ith job.
# 
# There are k workers that you can assign jobs to. Each job should be assigned to exactly one worker. 
# The working time of a worker is the sum of the time it takes to complete all jobs assigned to them. 
# Your goal is to devise an optimal assignment such that the maximum working time of any worker is 
# minimized.
# 
# Return the minimum possible maximum working time of any assignment. 
# 
# Example 1:
# 
# Input: jobs = [3,2,3], k = 3
# Output: 3
# Explanation: By assigning each person one job, the maximum time is 3.
# 
# Example 2:
# 
# Input: jobs = [1,2,4,7,8], k = 2
# Output: 11
# Explanation: Assign the jobs the following way:
# Worker 1: 1, 2, 8 (working time = 1 + 2 + 8 = 11)
# Worker 2: 4, 7 (working time = 4 + 7 = 11)
# The maximum working time is 11.
# 
# Constraints:
# 
# 	1 <= k <= jobs.length <= 12
# 	1 <= jobs[i] <= 107
#####################################################################################################

from typing import List


class Solution:
    # def minimumTimeRequired(self, jobs: List[int], k: int) -> int:  # O(n log n) time: WRONG
    #     import heapq
    #     queue = sorted(jobs)
    #     heap = [0] * k
    #     while queue:
    #         worker = heapq.heappop(heap)
    #         worker += queue.pop()
    #         heapq.heappush(heap, worker)
    #     return max(heap)

    def minimumTimeRequired(self, jobs: List[int], k: int) -> int:  # TLE: 16 / 60 test cases passed.

        def backtracking(i: int) -> int:
            if i == len(jobs):
                return max(workers)
            max_time_required = []
            for j in range(k):
                workers[j] += jobs[i]
                max_time_required.append(backtracking(i + 1))
                workers[j] -= jobs[i]
            return min(max_time_required)

        jobs.sort()
        workers = [0] * k
        return backtracking(0)

    def minimumTimeRequired(self, jobs: List[int], k: int) -> int:
        # early branch cutting
        def backtracking(i: int):
            nonlocal min_time_required
            if i == len(jobs):
                min_time_required = min(min_time_required, max(workers))
            else:
                seen = set()
                for j in range(k):
                    if workers[j] in seen or workers[j] + jobs[i] >= min_time_required:
                        continue
                    seen.add(workers[j])
                    workers[j] += jobs[i]
                    backtracking(i + 1)
                    workers[j] -= jobs[i]

        workers, min_time_required = [0] * k, 1 << 31 - 1
        return jobs.sort(reverse=True) or backtracking(0) or min_time_required


def test():
    arguments = [
        ([3, 2, 3], 3),
        ([1, 2, 4, 7, 8], 2),
        ([5, 5, 4, 4, 4], 2),
    ]
    expectations = [3, 11]
    for (jobs, k), expected in zip(arguments, expectations):
        solution = Solution().minimumTimeRequired(jobs, k)
        assert solution == expected
