### Source : https://leetcode.com/problems/1-bit-and-2-bit-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

##################################################################################################### 
#
# We have two special characters:
# 
# 	The first character can be represented by one bit 0.
# 	The second character can be represented by two bits (10 or 11).
# 
# Given a binary array bits that ends with 0, return true if the last character must be a one-bit 
# character.
# 
# Example 1:
# 
# Input: bits = [1,0,0]
# Output: true
# Explanation: The only way to decode it is two-bit character and one-bit character.
# So the last character is one-bit character.
# 
# Example 2:
# 
# Input: bits = [1,1,1,0]
# Output: false
# Explanation: The only way to decode it is two-bit character and two-bit character.
# So the last character is not one-bit character.
# 
# Constraints:
# 
# 	1 <= bits.length <= 1000
# 	bits[i] is either 0 or 1.
#####################################################################################################

from typing import List


class Solution:
    def isOneBitCharacter(self, bits: List[int]) -> bool:  # 77 / 93 test cases passed.
        from itertools import groupby
        return len(bits) == 1 or bits[-2] == 0 or not [len(list(x)) for _, x in groupby(bits)][-2] & 1


def test():
    arguments = [
        [1, 0, 0],
        [1, 1, 1, 0],
        [1, 0, 1, 0],
        [0, 0, 1, 0],
        [1, 1, 0, 0],
        [0],
        [1, 1, 0],
        [1, 1, 1, 0],
        [1, 1, 1, 1, 0],
    ]
    expectations = [True, False, False, False, True, True, True, False, True]
    for bits, expected in zip(arguments, expectations):
        solution = Solution().isOneBitCharacter(bits)
        assert solution == expected
