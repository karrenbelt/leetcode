### Source : https://leetcode.com/problems/widest-vertical-area-between-two-points-containing-no-points/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-25

##################################################################################################### 
#
# Given n points on a 2D plane where points[i] = [xi, yi], Return the widest vertical area between 
# two points such that no points are inside the area.
# 
# A vertical area is an area of fixed-width extending infinitely along the y-axis (i.e., infinite 
# height). The widest vertical area is the one with the maximum width.
# 
# Note that points on the edge of a vertical area are not considered included in the area.
# 
# Example 1:
# -b-@-K
# 
# Input: points = [[8,7],[9,9],[7,4],[9,7]]
# Output: 1
# Explanation: Both the red and the blue area are optimal.
# 
# Example 2:
# 
# Input: points = [[3,1],[9,0],[1,0],[1,4],[5,3],[8,8]]
# Output: 3
# 
# Constraints:
# 
# 	n == points.length
# 	2 <= n <= 105
# 	points[i].length == 2
# 	0 <= xi, yi <= 109
#####################################################################################################

from typing import List


class Solution:
    def maxWidthOfVerticalArea(self, points: List[List[int]]) -> int:  # O(n log n) time
        x_values = sorted(set([x[0] for x in points]))
        gap = 0
        for i in range(len(x_values) - 1):
            diff = x_values[i + 1] - x_values[i]
            gap = max(gap, diff)
        return gap

    def maxWidthOfVerticalArea(self, points: List[List[int]]) -> int:  # O(n log n) time
        x_values = sorted(set([x[0] for x in points]))
        return max(x2 - x1 for x1, x2 in zip(x_values, x_values[1:])) if len(x_values) > 1 else 0

    def maxWidthOfVerticalArea(self, points: List[List[int]]) -> int:  # O(n log n) time
        return points.sort() or max(x2 - x1 for (x1, _), (x2, _) in zip(points, points[1:]))


def test():
    arguments = [
        [[8, 7], [9, 9], [7, 4], [9, 7]],
        [[3, 1], [9, 0], [1, 0], [1, 4], [5, 3], [8, 8]],
        [[1, 1], [1, 2], [1, 3]],
    ]
    expectations = [1, 3, 0]
    for points, expected in zip(arguments, expectations):
        solution = Solution().maxWidthOfVerticalArea(points)
        assert solution == expected
