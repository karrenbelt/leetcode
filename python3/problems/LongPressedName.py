### Source : https://leetcode.com/problems/long-pressed-name/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-01

##################################################################################################### 
#
# Your friend is typing his name into a keyboard. Sometimes, when typing a character c, the key might 
# get long pressed, and the character will be typed 1 or more times.
# 
# You examine the typed characters of the keyboard. Return True if it is possible that it was your 
# friends name, with some characters (possibly none) being long pressed.
# 
# Example 1:
# 
# Input: name = "alex", typed = "aaleex"
# Output: true
# Explanation: 'a' and 'e' in 'alex' were long pressed.
# 
# Example 2:
# 
# Input: name = "saeed", typed = "ssaaedd"
# Output: false
# Explanation: 'e' must have been pressed twice, but it wasn't in the typed output.
# 
# Example 3:
# 
# Input: name = "leelee", typed = "lleeelee"
# Output: true
# 
# Example 4:
# 
# Input: name = "laiden", typed = "laiden"
# Output: true
# Explanation: It's not necessary to long press any character.
# 
# Constraints:
# 
# 	1 <= name.length <= 1000
# 	1 <= typed.length <= 1000
# 	name and typed contain only lowercase English letters.
#####################################################################################################


class Solution:
    def isLongPressedName(self, name: str, typed: str) -> bool:  # O(n) time and O(1) space
        i = j = 0
        while j < len(typed):
            if i < len(name) and name[i] == typed[j]:
                i += 1
            elif j == 0 or typed[j] != typed[j - 1]:
                return False
            j += 1
        return len(name) == i


def test():
    arguments = [
        ("alex", "aaleex"),
        ("saeed", "ssaaedd"),
        ("leelee", "lleeelee"),
        ("laiden", "laiden"),
    ]
    expectations = [True, False, True, True]
    for (name, typed), expected in zip(arguments, expectations):
        solution = Solution().isLongPressedName(name, typed)
        assert solution == expected
