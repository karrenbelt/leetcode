### Source : https://leetcode.com/problems/next-permutation/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-21

##################################################################################################### 
#
# Implement next permutation, which rearranges numbers into the lexicographically next greater 
# permutation of numbers.
# 
# If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e., 
# sorted in ascending order).
# 
# The replacement must be in place and use only constant extra memory.
# 
# Example 1:
# Input: nums = [1,2,3]
# Output: [1,3,2]
# Example 2:
# Input: nums = [3,2,1]
# Output: [1,2,3]
# Example 3:
# Input: nums = [1,1,5]
# Output: [1,5,1]
# Example 4:
# Input: nums = [1]
# Output: [1]
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 100
#####################################################################################################
##################################################################################################### 
#
# Implement next permutation, which rearranges numbers into the lexicographically next greater 
# permutation of numbers.
# 
# If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e., 
# sorted in ascending order).
# 
# The replacement must be in place and use only constant extra memory.
# 
# Example 1:
# Input: nums = [1,2,3]
# Output: [1,3,2]
# Example 2:
# Input: nums = [3,2,1]
# Output: [1,2,3]
# Example 3:
# Input: nums = [1,1,5]
# Output: [1,5,1]
# Example 4:
# Input: nums = [1]
# Output: [1]
# 
# Constraints:
# 
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 100
#####################################################################################################
#####################################################################################################
#
# Implement next permutation, which rearranges numbers into the lexicographically next greater
# permutation of numbers.
#
# If such an arrangement is not possible, it must rearrange it as the lowest possible order (i.e.,
# sorted in ascending order).
#
# The replacement must be in place and use only constant extra memory.
#
# Example 1:
# Input: nums = [1,2,3]
# Output: [1,3,2]
# Example 2:
# Input: nums = [3,2,1]
# Output: [1,2,3]
# Example 3:
# Input: nums = [1,1,5]
# Output: [1,5,1]
# Example 4:
# Input: nums = [1]
# Output: [1]
#
# Constraints:
#
# 	1 <= nums.length <= 100
# 	0 <= nums[i] <= 100
#####################################################################################################

from typing import List


class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        for i in range(len(nums) - 1, 0, -1):
            if nums[i] > nums[i - 1]:
                j = i
                while j < len(nums) and nums[j] > nums[i - 1]:
                    j += 1
                nums[j - 1], nums[i - 1] = nums[i - 1], nums[j - 1]
                nums[i:] = sorted(nums[i:])
                return
        nums.reverse()

    def nextPermutation(self, nums: List[int]) -> None:
        # find pivot, swap i and j, sort the remainder of the list
        i = j = len(nums) - 1
        while i > 0:
            if nums[i - 1] < nums[i]:
                break
            i -= 1
        i -= 1
        while j > i:
            if nums[j] > nums[i]:
                break
            j -= 1
        nums[i], nums[j] = nums[j], nums[i]
        nums[i + 1:] = sorted(nums[i + 1:])


def test():
    arrays_of_numbers = [
        [1, 2, 3],
        [3, 2, 1],
        [1, 1, 5],
        [1],
        [1, 3, 2],
        ]
    expectations = [
        [1, 3, 2],
        [1, 2, 3],
        [1, 5, 1],
        [1],
        [2, 1, 3],
        ]
    for nums, expected in zip(arrays_of_numbers, expectations):
        Solution().nextPermutation(nums)
        assert nums == expected
