### Source : https://leetcode.com/problems/delete-nodes-and-return-forest/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-12

#####################################################################################################
#
# Given the root of a binary tree, each node in the tree has a distinct value.
#
# After deleting all nodes with a value in to_delete, we are left with a forest (a disjoint union of
# trees).
#
# Return the roots of the trees in the remaining forest.  You may return the result in any order.
#
# Example 1:
#
# Input: root = [1,2,3,4,5,6,7], to_delete = [3,5]
# Output: [[1,2,null,4],[6],[7]]
#
# Constraints:
#
# 	The number of nodes in the given tree is at most 1000.
# 	Each node has a distinct value between 1 and 1000.
# 	to_delete.length <= 1000
# 	to_delete contains distinct values between 1 and 1000.
#####################################################################################################

from typing import List
from python3 import BinaryTreeNode as TreeNode


class Solution:
    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:

        def traverse(node: TreeNode, is_root: bool):
            if not node:
                return
            deleted = node.val in remove
            if is_root and not deleted:
                forest.append(node)
            node.left = traverse(node.left, deleted)
            node.right = traverse(node.right, deleted)
            return None if deleted else node

        forest, remove = [], set(to_delete)
        traverse(root, True)
        return forest

    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        remove = set(to_delete)
        stack = [(None, root)]
        forest = []
        while stack:  # dfs
            parent, node = stack.pop()
            if node.val in remove:
                if node.left:
                    stack.append((None, node.left))
                if node.right:
                    stack.append((None, node.right))
            else:
                if node.left:
                    stack.append((node, node.left))
                    if node.left.val in remove:
                        node.left = None
                if node.right:
                    stack.append((node, node.right))
                    if node.right.val in remove:
                        node.right = None
                if not parent or parent.val in remove:
                    forest.append(node)
        return forest

    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        from collections import deque
        remove = set(to_delete)
        forest = [root] if root.val not in remove else []
        queue = deque([root])
        while queue:  # bfs
            node = queue.popleft()
            if node.left:
                queue.append(node.left)
                if node.val not in remove and node.left.val in remove:
                    node.left = None
                elif node.val in remove and node.left.val not in remove:
                    forest.append(node.left)
            if node.right:
                queue.append(node.right)
                if node.val not in remove and node.right.val in remove:
                    node.right = None
                elif node.val in remove and node.right.val not in remove:
                    forest.append(node.right)
        return forest

    def delNodes(self, root: TreeNode, remove: List[int]) -> List[TreeNode]:

        def traverse(node):
            if node:
                node.left = traverse(node.left)
                node.right = traverse(node.right)
                if node.val not in remove:
                    return node
                forest.extend((node.left, node.right))

        forest, remove = [], set(remove)
        forest.append(traverse(root))
        return list(filter(None, forest))


def test():
    from python3 import Codec
    arguments = [
        ("[1,2,3,4,5,6,7]", [3, 5]),
        ("[1,2,4,null,3]", [3]),
    ]
    expectations = [
        ["[1,2,null,4]", "[6]", "[7]"],
        ["[1,2,4]"]
    ]
    for (serialized_tree, to_delete), expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().delNodes(root, to_delete)
        assert set(solution) == set(map(Codec.deserialize, expected))
