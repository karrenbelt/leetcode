### Source : https://leetcode.com/problems/swim-in-rising-water/
### Author : M.A.P. Karrenbelt
### Date   : 2021-06-20

##################################################################################################### 
#
# On an N x N grid, each square grid[i][j] represents the elevation at that point (i,j).
# 
# Now rain starts to fall. At time t, the depth of the water everywhere is t. You can swim from a 
# square to another 4-directionally adjacent square if and only if the elevation of both squares 
# individually are at most t. You can swim infinite distance in zero time. Of course, you must stay 
# within the boundaries of the grid during your swim.
# 
# You start at the top left square (0, 0). What is the least time until you can reach the bottom 
# right square (N-1, N-1)?
# 
# Example 1:
# 
# Input: [[0,2],[1,3]]
# Output: 3
# Explanation:
# At time 0, you are in grid location (0, 0).
# You cannot go anywhere else because 4-directionally adjacent neighbors have a higher elevation than 
# t = 0.
# 
# You cannot reach point (1, 1) until time 3.
# When the depth of water is 3, we can swim anywhere inside the grid.
# 
# Example 2:
# 
# Input: [[0,1,2,3,4],[24,23,22,21,5],[12,13,14,15,16],[11,17,18,19,20],[10,9,8,7,6]]
# Output: 16
# Explanation:
#  0  1  2  3  4
# 24 23 22 21  5
# 12 13 14 15 16
# 11 17 18 19 20
# 10  9  8  7  6
# 
# The final route is marked in bold.
# We need to wait until time 16 so that (0, 0) and (4, 4) are connected.
# 
# Note:
# 
# 	2 <= N <= 50.
# 	grid[i][j] is a permutation of [0, ..., N*N - 1].
# 
#####################################################################################################

from typing import List


class Solution:
    def swimInWater(self, grid: List[List[int]]) -> int:  # O(n^2 * log n) time
        # need to find a path from (0, 0) to (n - 1, n - 1) with the minimum level of elevation
        # we don't care about the number of steps: infinite distance travel,
        # since at t = 0 the height = 0, the maximum elevation we need to pass equals the amount of time we have to wait
        import heapq

        queue, seen, highest = [(grid[0][0], 0, 0)], {(0, 0)}, 0
        while queue:
            height, i, j = heapq.heappop(queue)
            highest = max(highest, height)
            if i == j == len(grid) - 1:
                return highest
            for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and (x, y) not in seen:
                    seen.add((x, y))
                    heapq.heappush(queue, (grid[x][y], x, y))

    def swimInWater(self, grid: List[List[int]]) -> int:  # bs + dfs + pruning: O(n^2 * log n) time

        def dfs(i: int, j: int, height: int, seen: set) -> bool:
            if 0 <= i < len(grid) and 0 <= j < len(grid[0]) and (i, j) not in seen and grid[i][j] <= height:
                if i == j == len(grid) - 1:
                    return True
                seen.add((i, j))
                return any(dfs(x, y, height, seen) for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)])
            return False

        left, right = grid[0][0], len(grid) ** 2 - 1
        while left < right:
            middle = left + (right - left) // 2
            if dfs(0, 0, middle, set()):
                right = middle
            else:
                left = middle + 1
        return left


def test():
    arguments = [
        [[0, 2], [1, 3]],
        [[0, 1, 2, 3, 4], [24, 23, 22, 21, 5], [12, 13, 14, 15, 16], [11, 17, 18, 19, 20], [10, 9, 8, 7, 6]],
        [[4, 1, 2, 3, 0], [24, 23, 22, 21, 5], [12, 13, 14, 15, 16], [11, 17, 18, 19, 20], [10, 9, 8, 7, 6]],
        [[3, 1, 8, 12],  # x x . .
         [11, 5, 6, 7],  # . x x x
         [2, 15, 10, 9], # . . . x
         [0, 13, 14, 4]  # . . . x  highest is 9
         ]
    ]
    expectations = [3, 16, 16]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().swimInWater(grid)
        assert solution == expected, solution
