### Source : https://leetcode.com/problems/binary-search-tree-iterator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-23

##################################################################################################### 
#
# Implement the BSTIterator class that represents an iterator over the in-order traversal of a binary 
# search tree (BST):
# 
# 	BSTIterator(TreeNode root) Initializes an object of the BSTIterator class. The root of the 
# BST is given as part of the constructor. The pointer should be initialized to a non-existent number 
# smaller than any element in the BST.
# 	boolean hasNext() Returns true if there exists a number in the traversal to the right of 
# the pointer, otherwise returns false.
# 	int next() oves the pointer to the right, then returns the number at the pointer.
# 
# Notice that by initializing the pointer to a non-existent smallest number, the first call to next() 
# will return the smallest element in the BST.
# 
# You may assume that next() calls will always be valid. That is, there will be at least a next 
# number in the in-order traversal when next() is called.
# 
# Example 1:
# 
# Input
# ["BSTIterator", "next", "next", "hasNext", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
# [[[7, 3, 15, null, null, 9, 20]], [], [], [], [], [], [], [], [], []]
# Output
# [null, 3, 7, true, 9, true, 15, true, 20, false]
# 
# Explanation
# BSTIterator bSTIterator = new BSTIterator([7, 3, 15, null, null, 9, 20]);
# bSTIterator.next();    // return 3
# bSTIterator.next();    // return 7
# bSTIterator.hasNext(); // return True
# bSTIterator.next();    // return 9
# bSTIterator.hasNext(); // return True
# bSTIterator.next();    // return 15
# bSTIterator.hasNext(); // return True
# bSTIterator.next();    // return 20
# bSTIterator.hasNext(); // return False
# 
# Constraints:
# 
# 	The number of nodes in the tree is in the range [1, 105].
# 	0 <= Node.val <= 106
# 	At most 105 calls will be made to hasNext, and next.
# 
# Follow up:
# 
# 	Could you implement next() and hasNext() to run in average O(1) time and use O(h) memory, 
# where h is the height of the tree?
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class BSTIterator:

    def __init__(self, root: TreeNode):
        self.root = node = root
        self.stack = []
        while node:
            self.stack.append(node)
            node = node.left

    def next(self) -> int:
        node = self.stack.pop()
        child = node.right
        while child:
            self.stack.append(child)
            child = child.left
        return node.val

    def hasNext(self) -> bool:
        return bool(self.stack)


class BSTIterator:

    def __init__(self, root: TreeNode):

        def iterate(node: TreeNode):
            if node is None:
                return
            yield from iterate(node.left)
            yield node.val
            yield from iterate(node.right)

        self.g = iterate(root)
        self.current = next(self.g)

    def next(self) -> int:
        value = self.current
        try:
            self.current = next(self.g)
        except StopIteration:
            self.current = None
        return value

    def hasNext(self) -> bool:
        return self.current is not None

# Your BSTIterator object will be instantiated and called as such:
# obj = BSTIterator(root)
# param_1 = obj.next()
# param_2 = obj.hasNext()

def test():
    from python3 import Codec
    null = None
    methods = ["BSTIterator", "next", "next", "hasNext", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
    arguments = [[7, 3, 15, null, null, 9, 20]], [], [], [], [], [], [], [], [], []
    expectations = [null, 3, 7, True, 9, True, 15, True, 20, False]
    root = Codec.deserialize("[7, 3, 15, null, null, 9, 20]")
    obj = BSTIterator(root)
    for method, args, expected in zip(methods[1:], arguments[1:], expectations[1:]):
        assert getattr(obj, method)(*args) == expected
