### Source : https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/
### Author : M.A.P. Karrenbelt
### Date   : 2021-03-16

##################################################################################################### 
#
# You are given an array prices where prices[i] is the price of a given stock on the ith day, and an 
# integer fee representing a transaction fee.
# 
# Find the maximum profit you can achieve. You may complete as many transactions as you like, but you 
# need to pay the transaction fee for each transaction.
# 
# Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock 
# before you buy again).
# 
# Example 1:
# 
# Input: prices = [1,3,2,8,4,9], fee = 2
# Output: 8
# Explanation: The maximum profit can be achieved by:
# - Buying at prices[0] = 1
# - Selling at prices[3] = 8
# - Buying at prices[4] = 4
# - Selling at prices[5] = 9
# The total profit is ((8 - 1) - 2) + ((9 - 4) - 2) = 8.
# 
# Example 2:
# 
# Input: prices = [1,3,7,5,10,3], fee = 3
# Output: 6
# 
# Constraints:
# 
# 	1 < prices.length <= 5 * 104
# 	0 < prices[i], fee < 5 * 104
#####################################################################################################

from typing import List


class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:  # O(n) time and O(1) space
        ans = 0
        minimum = prices[0]  # running minimum
        for i in range(1, len(prices)):
            if prices[i] < minimum:
                minimum = prices[i]
            elif prices[i] > minimum + fee:
                ans += prices[i] - fee - minimum
                minimum = prices[i] - fee  # e.g. when we find better to sell later, don't pay the fee again
        return ans

    def maxProfit(self, prices: List[int], fee: int) -> int:  # O(n) time and O(1) space
        buy, sell = float('inf'), 0
        for price in prices:
            buy = min(buy, price - sell)
            sell = max(sell, price - buy - fee)
        return sell

    def maxProfit(self, prices: List[int], fee: int) -> int:  # dp: O(n) time and O(1) space
        buy, sell = 0, -prices[0]
        for i in range(1, len(prices)):
            buy = max(buy, sell + prices[i] - fee)
            sell = max(sell, buy - prices[i])
        return buy


def test():
    arguments = [
        ([1, 3, 2, 8, 4, 9], 2),
        ([1, 3, 7, 5, 10, 3], 3),
        ]
    expectations = [8, 6]
    for (prices, fee), expected in zip(arguments, expectations):
        solution = Solution().maxProfit(prices, fee)
        assert solution == expected
