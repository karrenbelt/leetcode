### Source : https://leetcode.com/problems/x-of-a-kind-in-a-deck-of-cards/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-28

##################################################################################################### 
#
# In a deck of cards, each card has an integer written on it.
# 
# Return true if and only if you can choose X >= 2 such that it is possible to split the entire deck 
# into 1 or more groups of cards, where:
# 
# 	Each group has exactly X cards.
# 	All the cards in each group have the same integer.
# 
# Example 1:
# 
# Input: deck = [1,2,3,4,4,3,2,1]
# Output: true
# Explanation: Possible partition [1,1],[2,2],[3,3],[4,4].
# 
# Example 2:
# 
# Input: deck = [1,1,1,2,2,2,3,3]
# Output: false&acute;
# Explanation: No possible partition.
# 
# Example 3:
# 
# Input: deck = [1]
# Output: false
# Explanation: No possible partition.
# 
# Example 4:
# 
# Input: deck = [1,1]
# Output: true
# Explanation: Possible partition [1,1].
# 
# Example 5:
# 
# Input: deck = [1,1,2,2,2,2]
# Output: true
# Explanation: Possible partition [1,1],[2,2],[2,2].
# 
# Constraints:
# 
# 	1 <= deck.length <= 104
# 	0 <= deck[i] < 104
#####################################################################################################

from typing import List


class Solution:
    def hasGroupsSizeX(self, deck: List[int]) -> bool:
        d = {}
        for card in deck:
            d[card] = d[card] + 1 if card in d else 1
        smallest = min(d.values())
        if smallest < 2:
            return False
        for i in range(smallest, 1, -1):
            remainders = (v % i == 0 for v in d.values())
            if all(remainders):
                return True
        return False

    def hasGroupsSizeX(self, deck: List[int]) -> bool:

        def count(values):
            d = {}
            for card in values:
                d[card] = d[card] + 1 if card in d else 1
            return d

        def gcd(a, b):
            while b > 0:
                a, b = b, a % b
            return a

        def reduce(function, iterable):
            it = iter(iterable)
            value = next(it)
            for element in it:
                value = function(value, element)
            return value

        return reduce(gcd, count(deck).values()) >= 2


def test():
    arguments = [
        [1, 2, 3, 4, 4, 3, 2, 1],
        [1, 1, 1, 2, 2, 2, 3, 3],
        [1],
        [1, 1],
        [1, 1, 2, 2, 2, 2],
    ]
    expectations = [True, False, False, True, True]
    for deck, expected in zip(arguments, expectations):
        solution = Solution().hasGroupsSizeX(deck)
        assert solution == expected
