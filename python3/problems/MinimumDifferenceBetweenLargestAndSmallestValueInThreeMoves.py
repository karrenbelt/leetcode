### Source : https://leetcode.com/problems/minimum-difference-between-largest-and-smallest-value-in-three-moves/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-09

##################################################################################################### 
#
# Given an array nums, you are allowed to choose one element of nums and change it by any value in 
# one move.
# 
# Return the minimum difference between the largest and smallest value of nums after perfoming at 
# most 3 moves.
# 
# Example 1:
# 
# Input: nums = [5,3,2,4]
# Output: 0
# Explanation: Change the array [5,3,2,4] to [2,2,2,2].
# The difference between the maximum and minimum is 2-2 = 0.
# 
# Example 2:
# 
# Input: nums = [1,5,0,10,14]
# Output: 1
# Explanation: Change the array [1,5,0,10,14] to [1,1,0,1,1]. 
# The difference between the maximum and minimum is 1-0 = 1.
# 
# Example 3:
# 
# Input: nums = [6,6,0,1,1,4,6]
# Output: 2
# 
# Example 4:
# 
# Input: nums = [1,5,6,14,15]
# Output: 1
# 
# Constraints:
# 
# 	1 <= nums.length <= 105
# 	-109 <= nums[i] <= 109
#####################################################################################################

from typing import List


class Solution:
    def minDifference(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        if len(nums) <= 4:
            return 0
        nums.sort()
        return min(nums[i - 4] - nums[i] for i in range(4))

    def minDifference(self, nums: List[int]) -> int:  # O(n log n) time and O(1) space
        # record the smallest and largest 4 elements
        import heapq
        if len(nums) <= 4:
            return 0
        heapq.heapify(nums)
        smallest = heapq.nsmallest(4, nums)
        largest = heapq.nlargest(4, nums)
        return min(largest[-i - 1] - smallest[i] for i in range(4))


def test():
    arguments = [
        [5, 3, 2, 4],
        [1, 5, 0, 10, 14],
        [6, 6, 0, 1, 1, 4, 6],
        [1, 5, 6, 14, 15],
        ]
    expectations = [0, 1, 2, 1]
    for nums, expected in zip(arguments, expectations):
        solution = Solution().minDifference(nums)
        assert solution == expected
