### Source : https://leetcode.com/problems/minimum-distance-to-the-target-element/
### Author : M.A.P. Karrenbelt
### Date   : 2021-08-13

##################################################################################################### 
#
# Given an integer array nums (0-indexed) and two integers target and start, find an index i such 
# that nums[i] == target and abs(i - start) is minimized. Note that abs(x) is the absolute value of x.
# 
# Return abs(i - start).
# 
# It is guaranteed that target exists in nums.
# 
# Example 1:
# 
# Input: nums = [1,2,3,4,5], target = 5, start = 3
# Output: 1
# Explanation: nums[4] = 5 is the only value equal to target, so the answer is abs(4 - 3) = 1.
# 
# Example 2:
# 
# Input: nums = [1], target = 1, start = 0
# Output: 0
# Explanation: nums[0] = 1 is the only value equal to target, so the answer is abs(0 - 0) = 0.
# 
# Example 3:
# 
# Input: nums = [1,1,1,1,1,1,1,1,1,1], target = 1, start = 0
# Output: 0
# Explanation: Every value of nums is 1, but nums[0] minimizes abs(i - start), which is abs(0 - 0) = 
# 0.
# 
# Constraints:
# 
# 	1 <= nums.length <= 1000
# 	1 <= nums[i] <= 104
# 	0 <= start < nums.length
# 	target is in nums.
#####################################################################################################

from typing import List


class Solution:
    def getMinDistance(self, nums: List[int], target: int, start: int) -> int:  # O(n) time and O(1) space
        left = right = start
        ctr = 0
        while nums[left] != target and nums[right] != target:
            ctr += 1
            left -= left > 0
            right += right < len(nums) - 1
        return ctr

    def getMinDistance(self, nums: List[int], target: int, start: int) -> int:  # O(n) time and O(1) space
        ans = 1 << 10
        for i, x in enumerate(nums):
            if x == target:
                ans = min(ans, abs(i - start))
        return ans

    def getMinDistance(self, nums: List[int], target: int, start: int) -> int:  # O(n) time and O(1) space
        return min(abs(i - start) for i, n in enumerate(nums) if n == target)


def test():
    arguments = [
        ([1, 2, 3, 4, 5], 5, 3),
        ([1], 1, 0),
        ([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 1, 0),
        ([5, 3, 6], 5, 2),
        ([10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], 10000, 9),
    ]
    expectations = [1, 0, 0, 2, 0]
    for (nums, target, start), expected in zip(arguments, expectations):
        solution = Solution().getMinDistance(nums, target, start)
        assert solution == expected
