### Source : https://leetcode.com/problems/next-greater-node-in-linked-list/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-09

##################################################################################################### 
#
# We are given a linked list with head as the first node.  Let's number the nodes in the list: 
# node_1, node_2, node_3, ... etc.
# 
# Each node may have a next larger value: for node_i, next_larger(node_i) is the node_j.val such that 
# j > i, node_j.val > node_i.val, and j is the smallest possible choice.  If such a j does not exist, 
# the next larger value is 0.
# 
# Return an array of integers answer, where answer[i] = next_larger(node_{i+1}).
# 
# Note that in the example inputs (not outputs) below, arrays such as [2,1,5] represent the 
# serialization of a linked list with a head node value of 2, second node value of 1, and third node 
# value of 5.
# 
# Example 1:
# 
# Input: [2,1,5]
# Output: [5,5,0]
# 
# Example 2:
# 
# Input: [2,7,4,3,5]
# Output: [7,0,5,5,0]
# 
# Example 3:
# 
# Input: [1,7,5,1,9,2,5,1]
# Output: [7,9,9,9,0,5,0,0]
# 
# Note:
# 
# 	1 <= node.val <= 109 for each node in the linked list.
# 	The given list has length in the range [0, 10000].
# 
#####################################################################################################

from typing import List
from python3 import SinglyLinkedListNode as ListNode


class Solution:
    def nextLargerNodes(self, head: ListNode) -> List[int]:  # O(n)
        # save index and value, compare value to next, fill result at index if next is larger
        node, i = head, 0
        stack, result = [], []
        while node:
            while stack and stack[-1][1] < node.val:
                result[stack.pop()[0]] = node.val
            result.append(0)
            stack.append((i, node.val))
            i += 1
            node = node.next
        return result

    def nextLargerNodes(self, head: ListNode) -> List[int]:  # O(n)
        # recursively traverse the linked list from the end
        from collections import deque
        stack = []
        result = deque()

        def traverse(cur):
            if cur:
                traverse(cur.next)
                while stack and cur.val >= stack[-1]:
                    stack.pop()
                result.appendleft(stack[-1] if stack else 0)
                stack.append(cur.val)

        traverse(head)
        return list(result)

    def nextLargerNodes(self, head: ListNode) -> List[int]:  # O(n)
        # save index and value, compare value to next, fill result at index if next is larger
        node, i = head, -1
        stack, result = [], []
        while node:
            i += 1
            result.append(0)
            while stack and stack[-1][1] < node.val:
                result[stack.pop()[0]] = node.val
            stack.append((i, node.val))
            node = node.next
        return result


def test():
    from python3 import SinglyLinkedList
    arguments = [
        [2, 1, 5],
        [2, 7, 4, 3, 5],
        [1, 7, 5, 1, 9, 2, 5, 1],
    ]
    expectations = [
        [5, 5, 0],
        [7, 0, 5, 5, 0],
        [7, 9, 9, 9, 0, 5, 0, 0],
    ]
    for nums, expected in zip(arguments, expectations):
        head = SinglyLinkedList(nums).head
        solution = Solution().nextLargerNodes(head)
        assert solution == expected
