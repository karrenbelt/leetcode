### Source : https://leetcode.com/problems/rotting-oranges/
### Author : M.A.P. Karrenbelt
### Date   : 2021-10-22

##################################################################################################### 
#
# You are given an m x n grid where each cell can have one of three values:
# 
# 	0 representing an empty cell,
# 	1 representing a fresh orange, or
# 	2 representing a rotten orange.
# 
# Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.
# 
# Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is 
# impossible, return -1.
# 
# Example 1:
# 
# Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
# Output: 4
# 
# Example 2:
# 
# Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
# Output: -1
# Explanation: The orange in the bottom left corner (row 2, column 0) is never rotten, because 
# rotting only happens 4-directionally.
# 
# Example 3:
# 
# Input: grid = [[0,2]]
# Output: 0
# Explanation: Since there are already no fresh oranges at minute 0, the answer is just 0.
# 
# Constraints:
# 
# 	m == grid.length
# 	n == grid[i].length
# 	1 <= m, n <= 10
# 	grid[i][j] is 0, 1, or 2.
#####################################################################################################

from typing import List


class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:  # O(mn) time and O(mn) space
        from collections import deque

        fresh, queue = set(), deque()
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 1:
                    fresh.add((i, j))
                elif grid[i][j] == 2:
                    queue.append((i, j))

        minutes, prev_state = 0, set()
        while fresh and fresh != prev_state:
            minutes += 1
            prev_state = fresh.copy()
            for _ in range(len(queue)):  # do one level
                i, j = queue.popleft()
                for x, y in [(i + 1, j), (i, j + 1), (i - 1, j), (i, j - 1)]:
                    if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and grid[x][y] == 1:
                        grid[x][y] = 2
                        queue.append((x, y))
                        fresh.remove((x, y))

        return minutes if not fresh else -1

    def orangesRotting(self, grid: List[List[int]]) -> int:  # O(mn) time and O(mn) space
        rotting = {(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == 2}
        fresh = {(i, j) for i in range(len(grid)) for j in range(len(grid[0])) if grid[i][j] == 1}
        minutes = 0
        while fresh and rotting:
            rotting = {node for i, j in rotting for node in [(i+1, j), (i, j+1), (i-1, j), (i, j-1)] if node in fresh}
            fresh -= rotting
            minutes += 1
        return minutes if not fresh else -1


def test():
    arguments = [
        [[2, 1, 1], [1, 1, 0], [0, 1, 1]],
        [[2, 1, 1], [0, 1, 1], [1, 0, 1]],
        [[0, 2]],
    ]
    expectations = [4, -1, 0]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().orangesRotting(grid)
        assert solution == expected
