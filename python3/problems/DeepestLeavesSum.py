### Source : https://leetcode.com/problems/deepest-leaves-sum/
### Author : M.A.P. Karrenbelt
### Date   : 2021-02-10

##################################################################################################### 
#
# Given a binary tree, return the sum of values of its deepest leaves.
# 
# Example 1:
# 
# Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
# Output: 15
# 
# Constraints:
# 
# 	The number of nodes in the tree is between 1 and 104.
# 	The value of nodes is between 1 and 100.
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def deepestLeavesSum(self, root: TreeNode) -> int:

        def traverse(node: TreeNode, level: int = 0):
            if not node:
                return
            if level == len(levels):
                levels.append([])
            levels[level].append(node.val)
            traverse(node.left, level + 1)
            traverse(node.right, level + 1)

        levels = []
        traverse(root)
        return sum(levels[-1])

    def deepestLeavesSum(self, root: TreeNode) -> int:  # short but slow
        prev_level, level = [], [root]
        while level:
            prev_level, level = level, list(filter(None, sum([(node.left, node.right) for node in level], ())))
        return sum(map(lambda n: n.val, prev_level))

    def deepestLeavesSum(self, root: TreeNode) -> int:
        prev_level, level = [], [root]
        while level:
            prev_level, level = level, []
            for node in prev_level:
                if node.left:
                    level.append(node.left)
                if node.right:
                    level.append(node.right)
        return sum(map(lambda x: x.val, prev_level))


def test():
    from python3 import Codec
    arguments = [
        "[1,2,3,4,5,null,6,7,null,null,null,null,8]",
        "[6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]",
        ]
    expectations = [15, 19]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().deepestLeavesSum(root)
        assert solution == expected
