### Source : https://leetcode.com/problems/number-of-atoms/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-16

##################################################################################################### 
#
# Given a string formula representing a chemical formula, return the count of each atom.
# 
# The atomic element always starts with an uppercase character, then zero or more lowercase letters, 
# representing the name.
# 
# One or more digits representing that element's count may follow if the count is greater than 1. If 
# the count is 1, no digits will follow.
# 
# 	For example, "H2O" and "H2O2" are possible, but "H1O2" is impossible.
# 
# Two formulas are concatenated together to produce another formula.
# 
# 	For example, "H2O2He3g4" is also a formula.
# 
# A formula placed in parentheses, and a count (optionally added) is also a formula.
# 
# 	For example, "(H2O2)" and "(H2O2)3" are formulas.
# 
# Return the count of all elements as a string in the following form: the first name (in sorted 
# order), followed by its count (if that count is more than 1), followed by the second name (in 
# sorted order), followed by its count (if that count is more than 1), and so on.
# 
# Example 1:
# 
# Input: formula = "H2O"
# Output: "H2O"
# Explanation: The count of elements are {'H': 2, 'O': 1}.
# 
# Example 2:
# 
# Input: formula = "g(OH)2"
# Output: "H2gO2"
# Explanation: The count of elements are {'H': 2, 'g': 1, 'O': 2}.
# 
# Example 3:
# 
# Input: formula = "K4(ON(SO3)2)2"
# Output: "K4N2O14S4"
# Explanation: The count of elements are {'K': 4, 'N': 2, 'O': 14, 'S': 4}.
# 
# Example 4:
# 
# Input: formula = "Be32"
# Output: "Be32"
# 
# Constraints:
# 
# 	1 <= formula.length <= 1000
# 	formula consists of English letters, digits, '(', and ')'.
# 	formula is always valid.
# 	All the values in the output will fit in a 32-bit integer.
#####################################################################################################


class Solution:
    def countOfAtoms(self, formula: str) -> str:  # O(n log n) time and O(n) space
        atom, atoms, stack, = "", {}, []
        count, exponent = 0, 0
        characters = list(formula)
        while characters and (c := characters.pop()):
            if c.isdigit():
                count += (int(c) * 10 ** exponent)
                exponent += 1
            elif c == ')':
                if count == 0:
                    count = 1
                if stack:
                    count *= stack[-1]
                stack.append(count)
                count = exponent = 0
            elif c.islower():
                atom = c + atom
            elif c.isupper():
                atom = c + atom
                if count == 0:
                    count = 1
                if stack:
                    count *= stack[-1]
                atoms[atom] = atoms.get(atom, 0) + count
                atom = ""
                count = exponent = 0
            elif c == '(':
                stack.pop()
        return "".join(f"{atom}{amount}" if amount != 1 else atom for atom, amount in sorted(atoms.items()))

    def countOfAtoms(self, formula: str) -> str:  # O(n log n) time and O(n) space
        import re
        atoms, stack, count = {}, [], 1
        tokens = list(filter(lambda c: c, re.split(r'([A-Z][a-z]?|\(|\)|\d+)', formula)))
        while tokens and (token := tokens.pop()):
            if token.isdigit():
                count = int(token)
            elif token == ')':
                count *= stack[-1] if stack else 1
                stack.append(count)
                count = 1
            elif token.isalpha():
                count *= stack[-1] if stack else 1
                atoms[token] = atoms.get(token, 0) + count
                count = 1
            elif token == '(':
                stack.pop()
        return "".join(f"{atom}{amount}" if amount != 1 else atom for atom, amount in sorted(atoms.items()))

    def countOfAtoms(self, formula: str) -> str:  # O(n log n) time and O(n) space
        import re
        atoms, stack, count, tokens = {}, [1], 1, re.findall(r'[A-Z][a-z]*|\d+|[()]', formula)
        while tokens and (token := tokens.pop()):
            if token.isdigit():
                count = int(token)
            elif token == ')':
                count = stack.append(count * stack[-1]) or 1
            elif token.isalpha():
                atoms[token], count = atoms.get(token, 0) + count * stack[-1], 1
            elif token == '(':
                stack.pop()
        return "".join(f"{atom}{amount}" if amount != 1 else atom for atom, amount in sorted(atoms.items()))

    def countOfAtoms(self, formula: str) -> str:  # O(n log n) time and O(n) space
        import re
        atoms, stack, count, tokens = {}, [1], 1, re.findall(r'[A-Z][a-z]*|\d+|[()]', formula)
        while tokens and (token := tokens.pop()):
            if token.isalpha(): atoms[token], count = atoms.get(token, 0) + count * stack[-1], 1
            elif token == ')': count = stack.append(count * stack[-1]) or 1
            else: count = int(token) if token.isdigit() else count if stack.pop() else '😱 !PEP8 E701 violation! 😱'
        return "".join(f"{atom}{amount}" if amount != 1 else atom for atom, amount in sorted(atoms.items()))


def test():
    arguments = [
        "H2O",
        "Mg(OH)2",
        "K4(ON(SO3)2)2",
        "Be32",
        "A(A)2A(A)2A(A(A2)2)2ABC",
    ]
    expectations = [
        "H2O",
        "H2MgO2",
        "K4N2O14S4",
        "Be32",
        "A18BC",
    ]
    for formula, expected in zip(arguments, expectations):
        solution = Solution().countOfAtoms(formula)
        assert solution == expected
