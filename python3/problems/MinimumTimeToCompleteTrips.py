### Source : https://leetcode.com/problems/minimum-time-to-complete-trips/description/
### Author : M.A.P. Karrenbelt
### Date   : 2023-03-07

##################################################################################################### 
#
# You are given an array time where time[i] denotes the time taken by the ith bus to complete one 
# trip.
# 
# Each bus can make multiple trips successively; that is, the next trip can start immediately after 
# completing the current trip. Also, each bus operates independently; that is, the trips of one bus 
# do not influence the trips of any other bus.
# 
# You are also given an integer totalTrips, which denotes the number of trips all buses should make 
# in total. Return the minimum time required for all buses to complete at least totalTrips trips.
# 
# Example 1:
# 
# Input: time = [1,2,3], totalTrips = 5
# Output: 3
# Explanation:
# - At time t = 1, the number of trips completed by each bus are [1,0,0]. 
#   The total number of trips completed is 1 + 0 + 0 = 1.
# - At time t = 2, the number of trips completed by each bus are [2,1,0]. 
#   The total number of trips completed is 2 + 1 + 0 = 3.
# - At time t = 3, the number of trips completed by each bus are [3,1,1]. 
#   The total number of trips completed is 3 + 1 + 1 = 5.
# So the minimum time needed for all buses to complete at least 5 trips is 3.
# 
# Example 2:
# 
# Input: time = [2], totalTrips = 1
# Output: 2
# Explanation:
# There is only one bus, and it will complete its first trip at t = 2.
# So the minimum time needed to complete 1 trip is 2.
# 
# Constraints:
# 
# 	1 <= time.length <= 105
# 	1 <= time[i], totalTrips <= 107
#####################################################################################################

from typing import List


class Solution:
    def minimumTime(self, time: List[int], totalTrips: int) -> int:  # O(n log min(time) * totalTrips) time

        def binary_search(left: int, right: int, fn: callable) -> int:
            while left < right:
                mid = left + right >> 1
                left, right = (mid + 1, right) if fn(mid) else (left, mid)
            return left

        def condition(mid: int) -> bool:
            return sum(mid // t for t in time) < totalTrips

        return binary_search(0, min(time) * totalTrips, condition)

    def minimumTime(self, time: List[int], totalTrips: int) -> int:
        from bisect import bisect_left
        time_range = range(min(time) * totalTrips)
        condition = lambda x: sum(x // t for t in time)
        return bisect_left(time_range, totalTrips, key=condition)


def test():
    arguments = [
        ([1, 2, 3], 5),
        ([2], 1),
    ]
    expectations = [3, 2]
    for (time, totalTrips), expected in zip(arguments, expectations):
        solution = Solution().minimumTime(time, totalTrips)
        assert solution == expected, (solution, expected)
