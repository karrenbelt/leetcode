### Source : https://leetcode.com/problems/reverse-integer/
### Author : karrenbelt
### Date   : 2019-04-22

##################################################################################################### 
#
# Given a 32-bit signed integer, reverse digits of an integer.
# 
# Example 1:
# 
# Input: 123
# Output: 321
# 
# Example 2:
# 
# Input: -123
# Output: -321
# 
# Example 3:
# 
# Input: 120
# Output: 21
# 
# Note:
# Assume we are dealing with an environment which could only store integers within the 32-bit signed 
# integer range: [-2^31,  2^31 - 1]. For the purpose of this problem, assume that your
# function returns 0 when the reversed integer overflows.
#####################################################################################################

class Solution:
    def reverse(self, x: int) -> int:
        rev_x = int(str(abs(x))[::-1]) * [-1, 1][x > 0]
        if not -2 ** 31 < rev_x < 2 ** 31 - 1:
            return 0
        return rev_x

    def reverse(self, x: int) -> int:
        reverse = 0
        sign, x = (1, x) if x >= 0 else (-1, -x)
        while x:
            x, r = divmod(x, 10)
            reverse = reverse * 10 + r
        return 0 if reverse > 2 ** 31 else reverse * sign


def test():
    numbers = [123, -123, 120, 0]
    expectations = [321, -321, 21, 0]
    for x, expected in zip(numbers, expectations):
        solution = Solution().reverse(x)
        assert solution == expected
