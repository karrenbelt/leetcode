### Source : https://leetcode.com/problems/n-th-tribonacci-number/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-13

##################################################################################################### 
#
# The Tribonacci sequence Tn is defined as follows: 
# 
# T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
# 
# Given n, return the value of Tn.
# 
# Example 1:
# 
# Input: n = 4
# Output: 4
# Explanation:
# T_3 = 0 + 1 + 1 = 2
# T_4 = 1 + 1 + 2 = 4
# 
# Example 2:
# 
# Input: n = 25
# Output: 1389537
# 
# Constraints:
# 
# 	0 <= n <= 37
# 	The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 231 - 1.
#####################################################################################################


class Solution:
    def tribonacci(self, n: int) -> int:  # O(n) time and O(1) space
        from functools import lru_cache

        @lru_cache(maxsize=37)
        def dfs(i: int) -> int:
            if i == 0:
                return 0
            if i <= 2:
                return 1
            return dfs(i - 1) + dfs(i - 2) + dfs(i - 3)

        return dfs(n)

    def tribonacci(self, n: int) -> int:  # O(n) time and O(1) space
        dp = (0, 1, 1)
        for i in range(2, n):
            dp = dp[1], dp[2], sum(dp)
        return 0 if not n else 1 if n <= 2 else dp[-1]


def test():
    arguments = [4, 25, 0, 1, 2]
    expectations = [4, 1389537, 0, 1, 1]
    for n, expected in zip(arguments, expectations):
        solution = Solution().tribonacci(n)
        assert solution == expected
