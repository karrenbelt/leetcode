### Source : https://leetcode.com/problems/split-a-string-in-balanced-strings/
### Author : M.A.P. Karrenbelt
### Date   : 2020-12-08

##################################################################################################### 
#
# Balanced strings are those who have equal quantity of 'L' and 'R' characters.
# 
# Given a balanced string s split it in the maximum amount of balanced strings.
# 
# Return the maximum amount of splitted balanced strings.
# 
# Example 1:
# 
# Input: s = "RLRRLLRLRL"
# Output: 4
# Explanation: s can be split into "RL", "RRLL", "RL", "RL", each substring contains same number of 
# 'L' and 'R'.
# 
# Example 2:
# 
# Input: s = "RLLLLRRRLR"
# Output: 3
# Explanation: s can be split into "RL", "LLLRRR", "LR", each substring contains same number of 'L' 
# and 'R'.
# 
# Example 3:
# 
# Input: s = "LLLLRRRR"
# Output: 1
# Explanation: s can be split into "LLLLRRRR".
# 
# Example 4:
# 
# Input: s = "RLRRRLLRLL"
# Output: 2
# Explanation: s can be split into "RL", "RRRLLRLL", since each substring contains an equal number of 
# 'L' and 'R'
# 
# Constraints:
# 
# 	1 <= s.length <= 1000
# 	s[i] = 'L' or 'R'
#####################################################################################################


class Solution:
    def balancedStringSplit(self, s: str) -> int:
        sub_ctr, ctr = 0, 0
        for c in s:
            if c == 'R':
                ctr += 1
            else:
                ctr -= 1
            if ctr == 0:
                sub_ctr += 1
        return sub_ctr

    def balancedStringSplit(self, s: str) -> int:
        sub_ctr, ctr = 0, 0
        for c in s:
            ctr += 1 if c == 'R' else -1
            sub_ctr += 1 if ctr == 0 else 0
        return sub_ctr


def test():
    arguments = ["RLRRLLRLRL", "RLLLLRRRLR", "LLLLRRRR", "RLRRRLLRLL"]
    expectations = [4, 3, 1, 2]
    for s, expected in zip(arguments, expectations):
        solution = Solution().balancedStringSplit(s)
        assert solution == expected
