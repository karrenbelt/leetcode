### Source : https://leetcode.com/problems/encode-and-decode-tinyurl/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-25

##################################################################################################### 
#
# Note: This is a companion problem to the System Design problem: Design TinyURL.
# 
# TinyURL is a URL shortening service where you enter a URL such as 
# https://leetcode.com/problems/design-tinyurl and it returns a short URL such as 
# http://tinyurl.com/4e9iAk.
# 
# Design the encode and decode methods for the TinyURL service. There is no restriction on how your 
# encode/decode algorithm should work. You just need to ensure that a URL can be encoded to a tiny 
# URL and the tiny URL can be decoded to the original URL.
#####################################################################################################


class Codec:
    # this works...
    def encode(self, longUrl: str) -> str:
        """ Encodes a URL to a shortened URL. """
        return longUrl

    def decode(self, shortUrl: str) -> str:
        """ Decodes a shortened URL to its original URL. """
        return shortUrl


class Codec:

    prefix = 'http://tinyurl.com/'

    def __init__(self):
        self.map = {}  # I have no good ideas for a stateless solution

    def encode(self, longUrl: str) -> str:
        """ Encodes a URL to a shortened URL. """
        # key = hash(url).to_bytes(10, byteorder='big', signed=True)
        key = float(hash(url)).hex()
        self.map[key] = longUrl
        return f"{self.prefix}{key}"

    def decode(self, shortUrl: str) -> str:
        """ Decodes a shortened URL to its original URL. """
        return self.map[f"{shortUrl.lstrip(self.prefix)}"]


class Codec:
    import string
    alphabet = string.ascii_letters + '0123456789'

    def __init__(self):
        self.url2code = {}
        self.code2url = {}

    def encode(self, longUrl):
        import random
        while longUrl not in self.url2code:
            code = ''.join(random.choice(Codec.alphabet) for _ in range(6))
            if code not in self.code2url:
                self.code2url[code] = longUrl
                self.url2code[longUrl] = code
        return 'http://tinyurl.com/' + self.url2code[longUrl]

    def decode(self, shortUrl):
        return self.code2url[shortUrl[-6:]]

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(url))


def test():
    url = 'https://leetcode.com/problems/design-tinyurl'
    codec = Codec()
    encoded_url = codec.encode(url)
    print(encoded_url)
    decoded_url = codec.decode(encoded_url)
    assert url == decoded_url
