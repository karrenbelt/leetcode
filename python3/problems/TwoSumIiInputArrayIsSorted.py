### Source : https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
### Author : M.A.P. Karrenbelt
### Date   : 2021-01-07

##################################################################################################### 
#
# Given an array of integers that is already sorted in ascending order, find two numbers such that 
# they add up to a specific target number.
# 
# The function twoSum should return indices of the two numbers such that they add up to the target, 
# where index1 must be less than index2.
# 
# Note:
# 
# 	Your returned answers (both index1 and index2) are not zero-based.
# 	You may assume that each input would have exactly one solution and you may not use the same 
# element twice.
# 
# Example 1:
# 
# Input: numbers = [2,7,11,15], target = 9
# Output: [1,2]
# Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
# 
# Example 2:
# 
# Input: numbers = [2,3,4], target = 6
# Output: [1,3]
# 
# Example 3:
# 
# Input: numbers = [-1,0], target = -1
# Output: [1,2]
# 
# Constraints:
# 
# 	2 <= nums.length <= 3 * 104
# 	-1000 <= nums[i] <= 1000
# 	nums is sorted in increasing order.
# 	-1000 <= target <= 1000
#####################################################################################################

from typing import List


class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:  # O(n log n) time and O(1) space

        def binary_search(numbers, target):
            left, right = 0, len(numbers) - 1
            while left <= right:
                mid = left + (right - left) // 2
                if numbers[mid] == target:
                    return mid + 1  # can never be zero
                elif numbers[mid] < target:
                    left = mid + 1
                else:
                    right = mid - 1
            return 0

        for i, n in enumerate(numbers, start=1):
            j = binary_search(numbers, target - n)
            if j and i != j:
                return [i, j] if i < j else [j, i]

    def twoSum(self, numbers: List[int], target: int) -> List[int]:  # O(n) time & O(n) space
        d = {}
        for i, n in enumerate(numbers):
            if target - n in d:
                return [d[target - n] + 1, i + 1]
            d[n] = i

    def twoSum(self, numbers: List[int], target: int) -> List[int]:  # O(n) time and O(1) space
        left, right = 0, len(numbers) - 1
        while numbers[left] + numbers[right] != target:
            if numbers[left] + numbers[right] > target:
                right -= 1
            else:
                left += 1
        return [left + 1, right + 1]

    def twoSum(self, numbers: List[int], target: int) -> List[int]:  # O(n) time and O(1) space
        i = 0
        while (numbers[i] + numbers[-1]) != target:
            if numbers[i] + numbers[-1] > target:
                numbers.pop()
            else:
                i += 1
        return [i + 1, len(numbers)]


def test():
    arrays_of_numbers = [
        [2, 7, 11, 15],
        [2, 3, 4],
        [-1, 0],
        [0, 0, 3, 4],
    ]
    targets = [9, 6, -1, 0]
    expectations = [[1, 2], [1, 3], [1, 2], [1, 2]]
    for numbers, target, expected in zip(arrays_of_numbers, targets, expectations):
        solution = Solution().twoSum(numbers, target)
        assert solution == expected
