### Source : https://leetcode.com/problems/walking-robot-simulation/
### Author : M.A.P. Karrenbelt
### Date   : 2020-11-29

##################################################################################################### 
#
# A robot on an infinite grid starts at point (0, 0) and faces north.  The robot can receive one of 
# three possible types of commands:
# 
# 	-2: turn left 90 degrees
# 	-1: turn right 90 degrees
# 	1 <= x <= 9: move forward x units
# 
# Some of the grid squares are obstacles. 
# 
# The i-th obstacle is at grid point (obstacles[i][0], obstacles[i][1])
# 
# If the robot would try to move onto them, the robot stays on the previous grid square instead (but 
# still continues following the rest of the route.)
# 
# Return the square of the maximum Euclidean distance that the robot will be from the origin.
# 
# Example 1:
# 
# Input: commands = [4,-1,3], obstacles = []
# Output: 25
# Explanation: robot will go to (3, 4)
# 
# Example 2:
# 
# Input: commands = [4,-1,4,-2,4], obstacles = [[2,4]]
# Output: 65
# Explanation: robot will be stuck at (1, 4) before turning left and going to (1, 8)
# 
# Note:
# 
# 	0 <= commands.length <= 10000
# 	0 <= obstacles.length <= 10000
# 	-30000 <= obstacle[i][0] <= 30000
# 	-30000 <= obstacle[i][1] <= 30000
# 	The answer is guaranteed to be less than 2  31.
# 
#####################################################################################################

from typing import List


class Solution:
    def robotSim(self, commands: List[int], obstacles: List[List[int]]) -> int:

        def euclidean_distance(position):
            return (position[0] ** 2 + position[1] ** 2) ** 0.5

        directions = [(0, 1), (-1, 0), (0, -1), (1, 0)]
        turn_left = dict(zip(directions, directions[1:] + [directions[0]]))
        turn_right = dict(zip(directions, [directions[-1]] + directions[:-1]))

        obstacles = set(map(tuple, obstacles))
        max_distance = 0
        direction = (0, 1)
        pos = (0, 0)

        for command in commands:
            if command == -1:
                direction = turn_right[direction]
            elif command == -2:
                direction = turn_left[direction]
            else:
                for _ in range(command):
                    new_pos = (pos[0] + direction[0], pos[1] + direction[1])
                    if new_pos in obstacles:
                        break
                    pos = new_pos
                distance = euclidean_distance(pos)
                max_distance = max(max_distance, distance)
        return round(max_distance ** 2)

    def robotSim(self, commands, obstacles):
        x, y, dx, dy, max_distance = 0, 0, 0, 1, 0
        obstacles = set(map(tuple, obstacles))
        for command in commands:
            if command == -2:
                dx, dy = -dy, dx
            elif command == -1:
                dx, dy = dy, -dx
            else:
                for i in range(command):
                    if (x + dx, y + dy) in obstacles:
                        break
                    x += dx
                    y += dy
                max_distance = max(max_distance, x * x + y * y)
        return max_distance


def test():
    arguments = [
        ([4, -1, 3], []),
        ([4, -1, 4, -2, 4], [[2, 4]]),
    ]
    expectations = []
    for (commands, obstacles), expected in zip(arguments, expectations):
        solution = Solution().robotSim(commands, obstacles)
        assert solution == expected
