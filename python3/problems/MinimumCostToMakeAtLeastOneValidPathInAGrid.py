### Source : https://leetcode.com/problems/minimum-cost-to-make-at-least-one-valid-path-in-a-grid/
### Author : M.A.P. Karrenbelt
### Date   : 2021-07-04

#####################################################################################################
#
# Given a m x n grid. Each cell of the grid has a sign pointing to the next cell you should visit if
# you are currently in this cell. The sign of grid[i][j] can be:
#
# 	1 which means go to the cell to the right. (i.e go from grid[i][j] to grid[i][j + 1])
# 	2 which means go to the cell to the left. (i.e go from grid[i][j] to grid[i][j - 1])
# 	3 which means go to the lower cell. (i.e go from grid[i][j] to grid[i + 1][j])
# 	4 which means go to the upper cell. (i.e go from grid[i][j] to grid[i - 1][j])
#
# Notice that there could be some invalid signs on the cells of the grid which points outside the
# grid.
#
# You will initially start at the upper left cell (0,0). A valid path in the grid is a path which
# starts from the upper left cell (0,0) and ends at the bottom-right cell (m - 1, n - 1) following
# the signs on the grid. The valid path doesn't have to be the shortest.
#
# You can modify the sign on a cell with cost = 1. You can modify the sign on a cell one time only.
#
# Return the minimum cost to make the grid have at least one valid path.
#
# Example 1:
#
# Input: grid = [[1,1,1,1],[2,2,2,2],[1,1,1,1],[2,2,2,2]]
# Output: 3
# Explanation: You will start at point (0, 0).
# The path to (3, 3) is as follows. (0, 0) --> (0, 1) --> (0, 2) --> (0, 3) change the arrow to down
# with cost = 1 --> (1, 3) --> (1, 2) --> (1, 1) --> (1, 0) change the arrow to down with cost = 1
# --> (2, 0) --> (2, 1) --> (2, 2) --> (2, 3) change the arrow to down with cost = 1 --> (3, 3)
# The total cost = 3.
#
# Example 2:
#
# Input: grid = [[1,1,3],[3,2,2],[1,1,4]]
# Output: 0
# Explanation: You can follow the path from (0, 0) to (2, 2).
#
# Example 3:
#
# Input: grid = [[1,2],[4,3]]
# Output: 1
#
# Example 4:
#
# Input: grid = [[2,2,2],[2,2,2]]
# Output: 3
#
# Example 5:
#
# Input: grid = [[4]]
# Output: 0
#
# Constraints:
#
# 	m == grid.length
# 	n == grid[i].length
# 	1 <= m, n <= 100
#####################################################################################################

from typing import List


class Solution:
    def minCost(self, grid: List[List[int]]) -> int:  # O(m * n) time
        import heapq
        queue, seen = [(0, 0, 0)], {(0, 0): 0}  # global where we don't revisit UNLESS at lower cost!
        while queue:
            cost, i, j = heapq.heappop(queue)
            if i == len(grid) - 1 and j == len(grid[0]) - 1:
                return cost            # right       left        down        up
            for d, (x, y) in enumerate([(i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j)], start=1):
                if 0 <= x < len(grid) and 0 <= y < len(grid[0]):
                    if cost + (d != grid[i][j]) < seen.get((x, y), 1 << 31):
                        seen[x, y] = cost + (d != grid[i][j])
                        heapq.heappush(queue, (seen[x, y], x, y))

    def minCost(self, grid: List[List[int]]) -> int:  # O(m * n) time
        import heapq
        queue = [(0, 0, 0)]
        seen = set()
        while queue:
            cost, i, j = heapq.heappop(queue)
            if i == len(grid) - 1 and j == len(grid[0]) - 1:
                return cost
            if (i, j) not in seen:
                seen.add((i, j))  # since heap we add to seen here since lowest cost guaranteed
                for d, (x, y) in enumerate([(i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j)], start=1):
                    if 0 <= x < len(grid) and 0 <= y < len(grid[0]) and (x, y) not in seen:
                        heapq.heappush(queue, (cost + (d != grid[i][j]), x, y))

    def minCost(self, grid: List[List[int]]) -> int:  # O(m * n) time
        from collections import deque
        arrow = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        queue, costs = deque([(0, 0, 0)]), {}
        while queue:
            nx, ny, cost = queue.popleft()
            while 0 <= nx < len(grid) and 0 <= ny < len(grid[0]) and (nx, ny) not in costs:
                costs[nx, ny] = cost
                queue.extend([(nx + dx, ny + dy, cost + 1) for dx, dy in arrow])
                dx, dy = arrow[grid[nx][ny] - 1]
                nx, ny = nx + dx, ny + dy
        return costs[len(grid) - 1, len(grid[0]) - 1]


def test():
    arguments = [
        [[1, 1, 1, 1], [2, 2, 2, 2], [1, 1, 1, 1], [2, 2, 2, 2]],
        [[1, 1, 3], [3, 2, 2], [1, 1, 4]],
        [[1, 2], [4, 3]],
        [[2, 2, 2], [2, 2, 2]],
        [[4]],
        [[3, 4, 3], [2, 2, 2], [2, 1, 1], [4, 3, 2], [2, 1, 4], [2, 4, 1], [3, 3, 3], [1, 4, 2], [2, 2, 1], [2, 1, 1],
         [3, 3, 1], [4, 1, 4], [2, 1, 4], [3, 2, 2], [3, 3, 1], [4, 4, 1], [1, 2, 2], [1, 1, 1], [1, 3, 4], [1, 2, 1],
         [2, 2, 4], [2, 1, 3], [1, 2, 1], [4, 3, 2], [3, 3, 4], [2, 2, 1], [3, 4, 3], [4, 2, 3], [4, 4, 4]],
    ]
    expectations = [3, 0, 1, 3, 0, 18]
    for grid, expected in zip(arguments, expectations):
        solution = Solution().minCost(grid)
        assert solution == expected, (solution, expected)
