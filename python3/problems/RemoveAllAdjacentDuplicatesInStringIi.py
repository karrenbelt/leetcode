### Source : https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-16

##################################################################################################### 
#
# Given a string s, a k duplicate removal consists of choosing k adjacent and equal letters from s 
# and removing them causing the left and the right side of the deleted substring to concatenate 
# together.
# 
# We repeatedly make k duplicate removals on s until we no longer can.
# 
# Return the final string after all such duplicate removals have been made.
# 
# It is guaranteed that the answer is unique.
# 
# Example 1:
# 
# Input: s = "abcd", k = 2
# Output: "abcd"
# Explanation: There's nothing to delete.
# 
# Example 2:
# 
# Input: s = "deeedbbcccbdaa", k = 3
# Output: "aa"
# Explanation: 
# First delete "eee" and "ccc", get "ddbbbdaa"
# Then delete "bbb", get "dddaa"
# Finally delete "ddd", get "aa"
# 
# Example 3:
# 
# Input: s = "pbbcggttciiippooaais", k = 2
# Output: "ps"
# 
# Constraints:
# 
# 	1 <= s.length <= 105
# 	2 <= k <= 104
# 	s only contains lower case English letters.
#####################################################################################################


class Solution:
    def removeDuplicates(self, s: str, k: int) -> str:  # O(n^2) -> TLE: 16/18 passed
        i = j = 0
        while i < len(s):
            while i + j < len(s) - 1 and s[i + j] == s[i + j + 1]:
                j += 1
            if j + 1 == k:
                s = s[:i] + s[i + k:]
                i = j = 0
            else:
                j = 0
                i += 1
        return s

    def removeDuplicates(self, s: str, k: int) -> str:  # O(n^2)
        to_remove = {c * k for c in set(s)}
        while True:
            start = s
            for remove in to_remove:
                s = s.replace(remove, "")
            if start == s:
                return s

    def removeDuplicates(self, s: str, k: int) -> str:  # O(n) time and O(n) space
        stack = [['#', 0]]
        for c in s:
            if stack[-1][0] == c:
                stack[-1][1] += 1
                if stack[-1][1] == k:
                    stack.pop()
            else:
                stack.append([c, 1])
        return ''.join(c * k for c, k in stack)


def test():
    arguments = [
        ("abcd", 2),
        ("deeedbbcccbdaa", 3),
        ("pbbcggttciiippooaais", 2),
        ]
    expectations = ["abcd", "aa", "ps"]
    for (s, k), expected in zip(arguments, expectations):
        solution = Solution().removeDuplicates(s, k)
        assert solution == expected