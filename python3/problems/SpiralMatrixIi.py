### Source : https://leetcode.com/problems/spiral-matrix-ii/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-07

##################################################################################################### 
#
# Given a positive integer n, generate an n x n matrix filled with elements from 1 to n2 in spiral 
# order.
# 
# Example 1:
# 
# Input: n = 3
# Output: [[1,2,3],[8,9,4],[7,6,5]]
# 
# Example 2:
# 
# Input: n = 1
# Output: [[1]]
# 
# Constraints:
# 
# 	1 <= n <= 20
#####################################################################################################

from typing import List


class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:
        # spiral walk
        matrix = [[0] * n for _ in range(n)]
        i = j = 0
        i_dir, j_dir = 0, 1
        for k in range(1, n * n + 1):
            matrix[i][j] = k
            if matrix[(i + i_dir) % n][(j + j_dir) % n]:  # if filled, turn right
                i_dir, j_dir = j_dir, -i_dir
            i += i_dir
            j += j_dir
        return matrix

    def generateMatrix(self, n: int) -> List[List[int]]:
        matrix, low = [], n * n + 1
        while low > 1:
            low, high = low - len(matrix), low
            matrix = [list(range(low, high))] + list(map(list, zip(*matrix[::-1])))
        return matrix


def test():
    arguments = [3, 1]
    expectations = [
        [[1, 2, 3], [8, 9, 4], [7, 6, 5]],
        [[1]],
        ]
    for n, expected in zip(arguments, expectations):
        solution = Solution().generateMatrix(n)
        assert solution == expected
