### Source : https://leetcode.com/problems/binary-tree-cameras/
### Author : M.A.P. Karrenbelt
### Date   : 2021-05-16

#####################################################################################################
#
# Given a binary tree, we install cameras on the nodes of the tree.
#
# Each camera at a node can monitor its parent, itself, and its immediate children.
#
# Calculate the minimum number of cameras needed to monitor all nodes of the tree.
#
# Example 1:
#
# Input: [0,0,null,0,0]
# Output: 1
# Explanation: One camera is enough to monitor all nodes if placed as shown.
#
# Example 2:
#
# Input: [0,0,null,0,null,0,null,null,0]
# Output: 2
# Explanation: At least two cameras are needed to monitor all nodes of the tree. The above image
# shows one of the valid configurations of camera placement.
#
# Note:
#
# 	The number of nodes in the given tree will be in the range [1, 1000].
# 	Every node has value 0.
#
#####################################################################################################

from python3 import BinaryTreeNode as TreeNode


class Solution:
    def minCameraCover(self, root: TreeNode) -> int:
        # placing at parent of a leaf is always preferred over placing on a leaf itself
        # we cover the base case (no node) and then pairs of left and right
        def traverse(node: TreeNode) -> int:  # 1 camera, 0 covered, -1 not covered
            if not node:
                return 0  # base case
            left = traverse(node.left)
            right = traverse(node.right)
            if left == -1 or right == -1:  # if one child not covered, we need to place a camera
                nodes.append(node)
                return 1
            if left == 1 or right == 1:  # if camera at one of the children, we are covered
                return 0
            if left == 0 and right == 0:  # e.g. leaf node
                return -1  # not covered

        nodes = []
        covered = traverse(root)  # add another camera in case the root isn't covered
        return len(nodes) + (covered == -1)

    def minCameraCover(self, root: TreeNode) -> int:

        def traverse(node: TreeNode) -> int:
            nonlocal cameras
            if not node:
                return 1
            left = traverse(node.left)
            right = traverse(node.right)
            if left == 0 or right == 0:
                cameras += 1
                return 2
            return max(left - 1, right - 1)

        cameras = 0
        return (traverse(root) == 0) + cameras

    def minCameraCover(self, root: TreeNode) -> int:

        def dfs(node: TreeNode) -> bool:
            if not node:
                return True
            if not node.left and not node.right:
                return False
            left = dfs(node.left)
            right = dfs(node.right)
            if not left or not right:
                cameras.add(node)
            return node in cameras or node.left in cameras or node.right in cameras

        cameras = set()
        return len(cameras) if dfs(root) else len(cameras) + 1  # why can I not use: len(cameras) + (dfs(root) is False)


def test():
    from python3 import Codec
    arguments = [
        "[0,0,null,0,0]",
        "[0,0,null,0,null,0,null,null,0]",
        "[0]",
        "[0,null,0,null,0,null,0]",
        "[0,0,null,0,null,0,null,null,0]",
    ]
    expectations = [1, 2, 1, 2, 2]
    for serialized_tree, expected in zip(arguments, expectations):
        root = Codec.deserialize(serialized_tree)
        solution = Solution().minCameraCover(root)
        assert solution == expected, serialized_tree
