### Source : https://leetcode.com/problems/flatten-nested-list-iterator/
### Author : M.A.P. Karrenbelt
### Date   : 2021-04-13

##################################################################################################### 
#
# You are given a nested list of integers nestedList. Each element is either an integer or a list 
# whose elements may also be integers or other lists. Implement an iterator to flatten it.
# 
# Implement the NestedIterator class:
# 
# 	NestedIterator(List<NestedInteger> nestedList) Initializes the iterator with the nested 
# list nestedList.
# 	int next() Returns the next integer in the nested list.
# 	boolean hasNext() Returns true if there are still some integers in the nested list and 
# false otherwise.
# 
# Example 1:
# 
# Input: nestedList = [[1,1],2,[1,1]]
# Output: [1,1,2,1,1]
# Explanation: By calling next repeatedly until hasNext returns false, the order of elements returned 
# by next should be: [1,1,2,1,1].
# 
# Example 2:
# 
# Input: nestedList = [1,[4,[6]]]
# Output: [1,4,6]
# Explanation: By calling next repeatedly until hasNext returns false, the order of elements returned 
# by next should be: [1,4,6].
# 
# Constraints:
# 
# 	1 <= nestedList.length <= 500
# 	The values of the integers in the nested list is in the range [-106, 106].
#####################################################################################################

from typing import List, Union


"""
This is the interface that allows for creating nested lists.
You should not implement it, or speculate about its implementation
"""

class NestedInteger:

    def __init__(self, x: Union[int, List]):
        # I added this for testing
        self.x = x

    def isInteger(self) -> bool:
        """
        @return True if this NestedInteger holds a single integer, rather than a nested list.
        """
        return isinstance(self.x, int)

    def getInteger(self) -> int:
        """
        @return the single integer that this NestedInteger holds, if it holds a single integer
        Return None if this NestedInteger holds a nested list
        """
        return self.x if self.isInteger() else None

    def getList(self) -> ['NestedInteger']:
        """
        @return the nested list that this NestedInteger holds, if it holds a nested list
        Return None if this NestedInteger holds a single integer
        """
        return self.x if not self.isInteger() else None


class NestedIterator:

    def __init__(self, nestedList):
        self.stack = [[nestedList, 0]]

    def next(self):
        self.hasNext()
        nestedList, i = self.stack[-1]
        self.stack[-1][1] += 1
        return nestedList[i].getInteger()

    def hasNext(self):
        s = self.stack
        while s:
            nestedList, i = s[-1]
            if i == len(nestedList):
                s.pop()
            else:
                x = nestedList[i]
                if x.isInteger():
                    return True
                s[-1][1] += 1
                s.append([x.getList(), 0])
        return False

# Your NestedIterator object will be instantiated and called as such:
# i, v = NestedIterator(nestedList), []
# while i.hasNext(): v.append(i.next())

def test():
    # TODO: need a nestedList class?
    pass
    # # def make_nested_integer():
    # nested = [1, [2, [3, ]]]
    # nested_list = NestedInteger(
    #                 [NestedInteger(1),
    #                  [NestedInteger(
    #                   [NestedInteger(2),
    #                    [NestedInteger(
    #                     [NestedInteger(3)
    #                      ])]])]])
    # nested_iterator = NestedIterator(nested_list)
    # values = []
    # while nested_iterator.hasNext():
    #     values.append(nested_iterator.next())
