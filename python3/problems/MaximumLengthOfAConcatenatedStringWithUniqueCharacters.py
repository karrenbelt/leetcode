### Source : https://leetcode.com/problems/maximum-length-of-a-concatenated-string-with-unique-characters/
### Author : M.A.P. Karrenbelt
### Date   : 2021-09-25

##################################################################################################### 
#
# You are given an array of strings arr. A string s is formed by the concatenation of a subsequence 
# of arr that has unique characters.
# 
# Return the maximum possible length of s.
# 
# A subsequence is an array that can be derived from another array by deleting some or no elements 
# without changing the order of the remaining elements.
# 
# Example 1:
# 
# Input: arr = ["un","iq","ue"]
# Output: 4
# Explanation: All the valid concatenations are "","un","iq","ue","uniq" and "ique".
# aximum length is 4.
# 
# Example 2:
# 
# Input: arr = ["cha","r","act","ers"]
# Output: 6
# Explanation: Possible solutions are "chaers" and "acters".
# 
# Example 3:
# 
# Input: arr = ["abcdefghijklmnopqrstuvwxyz"]
# Output: 26
# 
# Constraints:
# 
# 	1 <= arr.length <= 16
# 	1 <= arr[i].length <= 26
# 	arr[i] contains only lower case English letters.
#####################################################################################################

from typing import List


class Solution:
    def maxLength(self, arr: List[str]) -> int:

        def backtracking(s, idx):
            nonlocal maximum
            if len(s) != len(set(s)):
                return 0
            maximum = max(maximum, len(s))
            for i in range(idx, len(arr)):
                backtracking(s + arr[i], i + 1)

        maximum = 0
        backtracking("", 0)
        return maximum

    def maxLength(self, arr: List[str]) -> int:

        def backtracking(s, idx):
            if len(s) != len(set(s)):
                return 0
            return max(tuple(backtracking(s + arr[i], i + 1) for i in range(idx, len(arr))) + (len(s), ))

        return backtracking("", 0)

    def maxLength(self, arr: List[str]) -> int:
        maximum, unique = 0, ['']
        for word in arr:
            for sequence in unique:
                s = word + sequence
                if len(s) == len(set(s)):
                    unique.append(s)
                    maximum = max(maximum, len(s))
        return maximum

    def maxLength(self, arr: List[str]) -> int:

        def codec(s: str) -> int:  # bitmask
            res = 0
            for c in s:
                res = res | (1 << (ord(c) - ord('a')))
            return res

        dp = [(0, 0)]
        for x in filter(lambda x: len(set(x)) == len(x), arr):  # filter those with duplicate characters early
            mask = codec(x)
            for m, length in dp:
                if not mask & m:
                    dp.append((mask | m, len(x) + length))

        return max(size for _, size in dp)
    #
    def maxLength(self, arr: List[str]) -> int:
        dp = [set()]
        for a in map(set, filter(lambda x: len(set(x)) == len(x), arr)):
            for c in dp[:]:
                if not a & c:
                    dp.append(a | c)
        return max(map(len, dp))


def test():
    arguments = [
        ["un", "iq", "ue"],
        ["cha", "r", "act", "ers"],
        ["abcdefghijklmnopqrstuvwxyz"],
        ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"],
        ["jnfbyktlrqumowxd", "mvhgcpxnjzrdei"],
    ]
    expectations = [4, 6, 26, 16, 16]
    for arr, expected in zip(arguments, expectations):
        solution = Solution().maxLength(arr)
        assert solution == expected
