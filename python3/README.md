

# Overview

## Tags
Array, Hash Table, Linked List, Math, Two Pointers, String, Binary Search, Divide and Conquer, Dynamic Programming, Backtracking, Stack, Heap, Greedy, Sort, Bit Manipulation, Tree, Depth-first Search, Breath-first Search, Union Find, Graph, Design, Topological Sort, Trie, Binary Indexed Tree, Segment Tree, Binary Search Tree, Recursion, Brainteaser, Memoization, Queue, Minimax, Reservoir Sampling, Ordered Map, Geometry, Random, Rejection Sampling, Sliding Window, Line Sweep, Rolling Hash, Suffix Array, Dequeue, OOP, Meet in the Middle

## Problems
|#   |Title                                                                           |Difficulty|Premium   |
|----|------------------------------------------------------------------------------- |----------|--------  |
|1   |[Two Sum](problems/TwoSum.py)                                                   |Easy      |          |
|2   |[Add Two Numbers](problems/AddTwoNumbers.py)                                    |Medium    |          |
|3   |[Longest Substring Without Repeating Characters](problems/LongestSubstringWithoutRepeatingCharacters.py)|Medium    |          |
|4   |[Median of Two Sorted Arrays](problems/MedianOfTwoSortedArrays.py)              |Hard      |          |
|5   |[Longest Palindromic Substring](problems/LongestPalindromicSubstring.py)        |Medium    |          |
|6   |[ZigZag Conversion](problems/ZigzagConversion.py)                               |Medium    |          |
|7   |[Reverse Integer](problems/ReverseInteger.py)                                   |Easy      |          |
|8   |[String to Integer (atoi)](problems/StringToIntegerAtoi.py)                     |Medium    |          |
|9   |[Palindrome Number](problems/PalindromeNumber.py)                               |Easy      |          |
|10  |[Regular Expression Matching](problems/RegularExpressionMatching.py)            |Hard      |          |
|11  |[Container With Most Water](problems/ContainerWithMostWater.py)                 |Medium    |          |
|12  |[Integer to Roman](problems/IntegerToRoman.py)                                  |Medium    |          |
|13  |[Roman to Integer](problems/RomanToInteger.py)                                  |Easy      |          |
|14  |[Longest Common Prefix](problems/LongestCommonPrefix.py)                        |Easy      |          |
|15  |[3Sum](problems/3sum.py)                                                        |Medium    |          |
|16  |[3Sum Closest](problems/3sumClosest.py)                                         |Medium    |          |
|17  |[Letter Combinations of a Phone Number](problems/LetterCombinationsOfAPhoneNumber.py)|Medium    |          |
|18  |[4Sum](problems/4sum.py)                                                        |Medium    |          |
|19  |[Remove Nth Node From End of List](problems/RemoveNthNodeFromEndOfList.py)      |Medium    |          |
|20  |[Valid Parentheses](problems/ValidParentheses.py)                               |Easy      |          |
|21  |[Merge Two Sorted Lists](problems/MergeTwoSortedLists.py)                       |Easy      |          |
|22  |[Generate Parentheses](problems/GenerateParentheses.py)                         |Medium    |          |
|23  |[Merge k Sorted Lists](problems/MergeKSortedLists.py)                           |Hard      |          |
|24  |[Swap Nodes in Pairs](problems/SwapNodesInPairs.py)                             |Medium    |          |
|25  |[Reverse Nodes in k-Group](problems/ReverseNodesInKGroup.py)                    |Hard      |          |
|26  |[Remove Duplicates from Sorted Array](problems/RemoveDuplicatesFromSortedArray.py)|Easy      |          |
|27  |[Remove Element](problems/RemoveElement.py)                                     |Easy      |          |
|28  |[Implement strStr()](problems/ImplementStrStr.py)                               |Easy      |          |
|29  |[Divide Two Integers](problems/DivideTwoIntegers.py)                            |Medium    |          |
|30  |[Substring with Concatenation of All Words](problems/SubstringWithConcatenationOfAllWords.py)|Hard      |          |
|31  |[Next Permutation](problems/NextPermutation.py)                                 |Medium    |          |
|32  |[Longest Valid Parentheses](problems/LongestValidParentheses.py)                |Hard      |          |
|33  |[Search in Rotated Sorted Array](problems/SearchInRotatedSortedArray.py)        |Medium    |          |
|34  |[Find First and Last Position of Element in Sorted Array](problems/FindFirstAndLastPositionOfElementInSortedArray.py)|Medium    |          |
|35  |[Search Insert Position](problems/SearchInsertPosition.py)                      |Easy      |          |
|36  |[Valid Sudoku](problems/ValidSudoku.py)                                         |Medium    |          |
|37  |[Sudoku Solver](problems/SudokuSolver.py)                                       |Hard      |          |
|38  |[Count and Say](problems/CountAndSay.py)                                        |Medium    |          |
|39  |[Combination Sum](problems/CombinationSum.py)                                   |Medium    |          |
|40  |[Combination Sum II](problems/CombinationSumIi.py)                              |Medium    |          |
|41  |[First Missing Positive](problems/FirstMissingPositive.py)                      |Hard      |          |
|42  |[Trapping Rain Water](problems/TrappingRainWater.py)                            |Hard      |          |
|43  |[Multiply Strings](problems/MultiplyStrings.py)                                 |Medium    |          |
|44  |[Wildcard Matching](problems/WildcardMatching.py)                               |Hard      |          |
|45  |[Jump Game II](problems/JumpGameIi.py)                                          |Hard      |          |
|46  |[Permutations](problems/Permutations.py)                                        |Medium    |          |
|47  |[Permutations II](problems/PermutationsIi.py)                                   |Medium    |          |
|48  |[Rotate Image](problems/RotateImage.py)                                         |Medium    |          |
|49  |[Group Anagrams](problems/GroupAnagrams.py)                                     |Medium    |          |
|50  |[Pow(x, n)](problems/PowxN.py)                                                  |Medium    |          |
|51  |[N-Queens](problems/NQueens.py)                                                 |Hard      |          |
|52  |[N-Queens II](problems/NQueensIi.py)                                            |Hard      |          |
|53  |[Maximum Subarray](problems/MaximumSubarray.py)                                 |Easy      |          |
|54  |[Spiral Matrix](problems/SpiralMatrix.py)                                       |Medium    |          |
|55  |[Jump Game](problems/JumpGame.py)                                               |Medium    |          |
|56  |[Merge Intervals](problems/MergeIntervals.py)                                   |Medium    |          |
|57  |[Insert Interval](problems/InsertInterval.py)                                   |Medium    |          |
|58  |[Length of Last Word](problems/LengthOfLastWord.py)                             |Easy      |          |
|59  |[Spiral Matrix II](problems/SpiralMatrixIi.py)                                  |Medium    |          |
|60  |[Permutation Sequence](problems/PermutationSequence.py)                         |Hard      |          |
|61  |[Rotate List](problems/RotateList.py)                                           |Medium    |          |
|62  |[Unique Paths](problems/UniquePaths.py)                                         |Medium    |          |
|63  |[Unique Paths II](problems/UniquePathsIi.py)                                    |Medium    |          |
|64  |[Minimum Path Sum](problems/MinimumPathSum.py)                                  |Medium    |          |
|65  |[Valid Number](problems/ValidNumber.py)                                         |Hard      |          |
|66  |[Plus One](problems/PlusOne.py)                                                 |Easy      |          |
|67  |[Add Binary](problems/AddBinary.py)                                             |Easy      |          |
|68  |[Text Justification](problems/TextJustification.py)                             |Hard      |          |
|69  |[Sqrt(x)](problems/Sqrtx.py)                                                    |Easy      |          |
|70  |[Climbing Stairs](problems/ClimbingStairs.py)                                   |Easy      |          |
|71  |[Simplify Path](problems/SimplifyPath.py)                                       |Medium    |          |
|72  |[Edit Distance](problems/EditDistance.py)                                       |Hard      |          |
|73  |[Set Matrix Zeroes](problems/SetMatrixZeroes.py)                                |Medium    |          |
|74  |[Search a 2D Matrix](problems/SearchA2dMatrix.py)                               |Medium    |          |
|75  |[Sort Colors](problems/SortColors.py)                                           |Medium    |          |
|76  |[Minimum Window Substring](problems/MinimumWindowSubstring.py)                  |Hard      |          |
|77  |[Combinations](problems/Combinations.py)                                        |Medium    |          |
|78  |[Subsets](problems/Subsets.py)                                                  |Medium    |          |
|79  |[Word Search](problems/WordSearch.py)                                           |Medium    |          |
|80  |[Remove Duplicates from Sorted Array II](problems/RemoveDuplicatesFromSortedArrayIi.py)|Medium    |          |
|81  |[Search in Rotated Sorted Array II](problems/SearchInRotatedSortedArrayIi.py)   |Medium    |          |
|82  |[Remove Duplicates from Sorted List II](problems/RemoveDuplicatesFromSortedListIi.py)|Medium    |          |
|83  |[Remove Duplicates from Sorted List](problems/RemoveDuplicatesFromSortedList.py)|Easy      |          |
|84  |[Largest Rectangle in Histogram](problems/LargestRectangleInHistogram.py)       |Hard      |          |
|85  |[Maximal Rectangle](problems/MaximalRectangle.py)                               |Hard      |          |
|86  |[Partition List](problems/PartitionList.py)                                     |Medium    |          |
|87  |[Scramble String](problems/ScrambleString.py)                                   |Hard      |          |
|88  |[Merge Sorted Array](problems/MergeSortedArray.py)                              |Easy      |          |
|89  |[Gray Code](problems/GrayCode.py)                                               |Medium    |          |
|90  |[Subsets II](problems/SubsetsIi.py)                                             |Medium    |          |
|91  |[Decode Ways](problems/DecodeWays.py)                                           |Medium    |          |
|92  |[Reverse Linked List II](problems/ReverseLinkedListIi.py)                       |Medium    |          |
|93  |[Restore IP Addresses](problems/RestoreIpAddresses.py)                          |Medium    |          |
|94  |[Binary Tree Inorder Traversal](problems/BinaryTreeInorderTraversal.py)         |Medium    |          |
|95  |[Unique Binary Search Trees II](problems/UniqueBinarySearchTreesIi.py)          |Medium    |          |
|96  |[Unique Binary Search Trees](problems/UniqueBinarySearchTrees.py)               |Medium    |          |
|97  |[Interleaving String](problems/InterleavingString.py)                           |Hard      |          |
|98  |[Validate Binary Search Tree](problems/ValidateBinarySearchTree.py)             |Medium    |          |
|99  |[Recover Binary Search Tree](problems/RecoverBinarySearchTree.py)               |Hard      |          |
|100 |[Same Tree](problems/SameTree.py)                                               |Easy      |          |
|101 |[Symmetric Tree](problems/SymmetricTree.py)                                     |Easy      |          |
|102 |[Binary Tree Level Order Traversal](problems/BinaryTreeLevelOrderTraversal.py)  |Medium    |          |
|103 |[Binary Tree Zigzag Level Order Traversal](problems/BinaryTreeZigzagLevelOrderTraversal.py)|Medium    |          |
|104 |[Maximum Depth of Binary Tree](problems/MaximumDepthOfBinaryTree.py)            |Easy      |          |
|105 |[Construct Binary Tree from Preorder and Inorder Traversal](problems/ConstructBinaryTreeFromPreorderAndInorderTraversal.py)|Medium    |          |
|106 |[Construct Binary Tree from Inorder and Postorder Traversal](problems/ConstructBinaryTreeFromInorderAndPostorderTraversal.py)|Medium    |          |
|107 |[Binary Tree Level Order Traversal II](problems/BinaryTreeLevelOrderTraversalIi.py)|Medium    |          |
|108 |[Convert Sorted Array to Binary Search Tree](problems/ConvertSortedArrayToBinarySearchTree.py)|Easy      |          |
|109 |[Convert Sorted List to Binary Search Tree](problems/ConvertSortedListToBinarySearchTree.py)|Medium    |          |
|110 |[Balanced Binary Tree](problems/BalancedBinaryTree.py)                          |Easy      |          |
|111 |[Minimum Depth of Binary Tree](problems/MinimumDepthOfBinaryTree.py)            |Easy      |          |
|112 |[Path Sum](problems/PathSum.py)                                                 |Easy      |          |
|113 |[Path Sum II](problems/PathSumIi.py)                                            |Medium    |          |
|114 |[Flatten Binary Tree to Linked List](problems/FlattenBinaryTreeToLinkedList.py) |Medium    |          |
|115 |[Distinct Subsequences](problems/DistinctSubsequences.py)                       |Hard      |          |
|116 |[Populating Next Right Pointers in Each Node](problems/PopulatingNextRightPointersInEachNode.py)|Medium    |          |
|117 |[Populating Next Right Pointers in Each Node II](problems/PopulatingNextRightPointersInEachNodeIi.py)|Medium    |          |
|118 |[Pascal's Triangle](problems/PascalsTriangle.py)                                |Easy      |          |
|119 |[Pascal's Triangle II](problems/PascalsTriangleIi.py)                           |Easy      |          |
|120 |[Triangle](problems/Triangle.py)                                                |Medium    |          |
|121 |[Best Time to Buy and Sell Stock](problems/BestTimeToBuyAndSellStock.py)        |Easy      |          |
|122 |[Best Time to Buy and Sell Stock II](problems/BestTimeToBuyAndSellStockIi.py)   |Easy      |          |
|123 |[Best Time to Buy and Sell Stock III](problems/BestTimeToBuyAndSellStockIii.py) |Hard      |          |
|124 |[Binary Tree Maximum Path Sum](problems/BinaryTreeMaximumPathSum.py)            |Hard      |          |
|125 |[Valid Palindrome](problems/ValidPalindrome.py)                                 |Easy      |          |
|126 |[Word Ladder II](problems/WordLadderIi.py)                                      |Hard      |          |
|127 |[Word Ladder](problems/WordLadder.py)                                           |Hard      |          |
|128 |[Longest Consecutive Sequence](problems/LongestConsecutiveSequence.py)          |Hard      |          |
|129 |[Sum Root to Leaf Numbers](problems/SumRootToLeafNumbers.py)                    |Medium    |          |
|130 |[Surrounded Regions](problems/SurroundedRegions.py)                             |Medium    |          |
|131 |[Palindrome Partitioning](problems/PalindromePartitioning.py)                   |Medium    |          |
|132 |[Palindrome Partitioning II](problems/PalindromePartitioningIi.py)              |Hard      |          |
|133 |[Clone Graph](problems/CloneGraph.py)                                           |Medium    |          |
|134 |[Gas Station](problems/GasStation.py)                                           |Medium    |          |
|135 |[Candy](problems/Candy.py)                                                      |Hard      |          |
|136 |[Single Number](problems/SingleNumber.py)                                       |Easy      |          |
|137 |[Single Number II](problems/SingleNumberIi.py)                                  |Medium    |          |
|138 |[Copy List with Random Pointer](problems/CopyListWithRandomPointer.py)          |Medium    |          |
|139 |[Word Break](problems/WordBreak.py)                                             |Medium    |          |
|140 |[Word Break II](problems/WordBreakIi.py)                                        |Hard      |          |
|141 |[Linked List Cycle](problems/LinkedListCycle.py)                                |Easy      |          |
|142 |[Linked List Cycle II](problems/LinkedListCycleIi.py)                           |Medium    |          |
|143 |[Reorder List](problems/ReorderList.py)                                         |Medium    |          |
|144 |[Binary Tree Preorder Traversal](problems/BinaryTreePreorderTraversal.py)       |Medium    |          |
|145 |[Binary Tree Postorder Traversal](problems/BinaryTreePostorderTraversal.py)     |Medium    |          |
|146 |[LRU Cache](problems/LruCache.py)                                               |Medium    |          |
|147 |[Insertion Sort List](problems/InsertionSortList.py)                            |Medium    |          |
|148 |[Sort List](problems/SortList.py)                                               |Medium    |          |
|149 |[Max Points on a Line](problems/MaxPointsOnALine.py)                            |Hard      |          |
|150 |[Evaluate Reverse Polish Notation](problems/EvaluateReversePolishNotation.py)   |Medium    |          |
|151 |[Reverse Words in a String](problems/ReverseWordsInAString.py)                  |Medium    |          |
|152 |[Maximum Product Subarray](problems/MaximumProductSubarray.py)                  |Medium    |          |
|153 |[Find Minimum in Rotated Sorted Array](problems/FindMinimumInRotatedSortedArray.py)|Medium    |          |
|154 |[Find Minimum in Rotated Sorted Array II](problems/FindMinimumInRotatedSortedArrayIi.py)|Hard      |          |
|155 |[Min Stack](problems/MinStack.py)                                               |Easy      |          |
|156 |Binary Tree Upside Down                                                         |Medium    |Premium   |
|157 |Read N Characters Given Read4                                                   |Easy      |Premium   |
|158 |Read N Characters Given Read4 II - Call multiple times                          |Hard      |Premium   |
|159 |Longest Substring with At Most Two Distinct Characters                          |Medium    |Premium   |
|160 |[Intersection of Two Linked Lists](problems/IntersectionOfTwoLinkedLists.py)    |Easy      |          |
|161 |One Edit Distance                                                               |Medium    |Premium   |
|162 |[Find Peak Element](problems/FindPeakElement.py)                                |Medium    |          |
|163 |Missing Ranges                                                                  |Easy      |Premium   |
|164 |[Maximum Gap](problems/MaximumGap.py)                                           |Hard      |          |
|165 |[Compare Version Numbers](problems/CompareVersionNumbers.py)                    |Medium    |          |
|166 |Fraction to Recurring Decimal                                                   |Medium    |          |
|167 |[Two Sum II - Input array is sorted](problems/TwoSumIiInputArrayIsSorted.py)    |Easy      |          |
|168 |[Excel Sheet Column Title](problems/ExcelSheetColumnTitle.py)                   |Easy      |          |
|169 |[Majority Element](problems/MajorityElement.py)                                 |Easy      |          |
|170 |Two Sum III - Data structure design                                             |Easy      |Premium   |
|171 |[Excel Sheet Column Number](problems/ExcelSheetColumnNumber.py)                 |Easy      |          |
|172 |[Factorial Trailing Zeroes](problems/FactorialTrailingZeroes.py)                |Easy      |          |
|173 |[Binary Search Tree Iterator](problems/BinarySearchTreeIterator.py)             |Medium    |          |
|174 |[Dungeon Game](problems/DungeonGame.py)                                         |Hard      |          |
|175 |Combine Two Tables - SQL                                                        |Easy      |          |
|176 |Second Highest Salary - SQL                                                     |Easy      |          |
|177 |Nth Highest Salary - SQL                                                        |Medium    |          |
|178 |Rank Scores - SQL                                                               |Medium    |          |
|179 |[Largest Number](problems/LargestNumber.py)                                     |Medium    |          |
|180 |Consecutive Numbers - SQL                                                       |Medium    |          |
|181 |Employees Earning More Than Their Managers - SQL                                |Easy      |          |
|182 |Duplicate Emails - SQL                                                          |Easy      |          |
|183 |[Customers Who Never Order](problems/CustomersWhoNeverOrder.mysql) - SQL        |Easy      |          |
|184 |Department Highest Salary - SQL                                                 |Medium    |          |
|185 |Department Top Three Salaries - SQL                                             |Hard      |          |
|186 |Reverse Words in a String II                                                    |Medium    |Premium   |
|187 |[Repeated DNA Sequences](problems/RepeatedDnaSequences.py)                      |Medium    |          |
|188 |[Best Time to Buy and Sell Stock IV](problems/BestTimeToBuyAndSellStockIv.py)   |Hard      |          |
|189 |[Rotate Array](problems/RotateArray.py)                                         |Medium    |          |
|190 |[Reverse Bits](problems/ReverseBits.py)                                         |Easy      |          |
|191 |[Number of 1 Bits](problems/NumberOf1Bits.py)                                   |Easy      |          |
|192 |Word Frequency - Bash                                                           |Medium    |          |
|193 |[Valid Phone Numbers](problems/ValidPhoneNumbers.sh) - Bash                     |Easy      |          |
|194 |[Transpose File] - Bash                                                         |Medium    |          |
|195 |[Tenth Line](problems/TenthLine.sh) - Bash                                      |Easy      |          |
|196 |Delete Duplicate Emails - SQL                                                   |Easy      |          |
|197 |Rising Temperature - SQL                                                        |Easy      |          |
|198 |[House Robber](problems/HouseRobber.py)                                         |Medium    |          |
|199 |[Binary Tree Right Side View](problems/BinaryTreeRightSideView.py)              |Medium    |          |
|200 |[Number of Islands](problems/NumberOfIslands.py)                                |Medium    |          |
|201 |[Bitwise AND of Numbers Range](problems/BitwiseAndOfNumbersRange.py)            |Medium    |          |
|202 |[Happy Number](problems/HappyNumber.py)                                         |Easy      |          |
|203 |[Remove Linked List Elements](problems/RemoveLinkedListElements.py)             |Easy      |          |
|204 |[Count Primes](problems/CountPrimes.py)                                         |Easy      |          |
|205 |[Isomorphic Strings](problems/IsomorphicStrings.py)                             |Easy      |          |
|206 |[Reverse Linked List](problems/ReverseLinkedList.py)                            |Easy      |          |
|207 |[Course Schedule](problems/CourseSchedule.py)                                   |Medium    |          |
|208 |[Implement Trie (Prefix Tree)](problems/ImplementTriePrefixTree.py)             |Medium    |          |
|209 |[Minimum Size Subarray Sum](problems/MinimumSizeSubarraySum.py)                 |Medium    |          |
|210 |[Course Schedule II](problems/CourseScheduleIi.py)                              |Medium    |          |
|211 |[Design Add and Search Words Data Structure](problems/DesignAddAndSearchWordsDataStructure.py)|Medium    |          |
|212 |[Word Search II](problems/WordSearchIi.py)                                      |Hard      |          |
|213 |[House Robber II](problems/HouseRobberIi.py)                                    |Medium    |          |
|214 |[Shortest Palindrome](problems/ShortestPalindrome.py)                           |Hard      |          |
|215 |[Kth Largest Element in an Array](problems/KthLargestElementInAnArray.py)       |Medium    |          |
|216 |[Combination Sum III](problems/CombinationSumIii.py)                            |Medium    |          |
|217 |[Contains Duplicate](problems/ContainsDuplicate.py)                             |Easy      |          |
|218 |[The Skyline Problem](problems/TheSkylineProblem.py)                            |Hard      |          |
|219 |[Contains Duplicate II](problems/ContainsDuplicateIi.py)                        |Easy      |          |
|220 |[Contains Duplicate III](problems/ContainsDuplicateIii.py)                      |Medium    |          |
|221 |[Maximal Square](problems/MaximalSquare.py)                                     |Medium    |          |
|222 |[Count Complete Tree Nodes](problems/CountCompleteTreeNodes.py)                 |Medium    |          |
|223 |[Rectangle Area](problems/RectangleArea.py)                                     |Medium    |          |
|224 |[Basic Calculator](problems/BasicCalculator.py)                                 |Hard      |          |
|225 |[Implement Stack using Queues](problems/ImplementStackUsingQueues.py)           |Easy      |          |
|226 |[Invert Binary Tree](problems/InvertBinaryTree.py)                              |Easy      |          |
|227 |[Basic Calculator II](problems/BasicCalculatorIi.py)                            |Medium    |          |
|228 |[Summary Ranges](problems/SummaryRanges.py)                                     |Easy      |          |
|229 |[Majority Element II](problems/MajorityElementIi.py)                            |Medium    |          |
|230 |[Kth Smallest Element in a BST](problems/KthSmallestElementInABst.py)           |Medium    |          |
|231 |[Power of Two](problems/PowerOfTwo.py)                                          |Easy      |          |
|232 |[Implement Queue using Stacks](problems/ImplementQueueUsingStacks.py)           |Easy      |          |
|233 |Number of Digit One                                                             |Hard      |          |
|234 |[Palindrome Linked List](problems/PalindromeLinkedList.py)                      |Easy      |          |
|235 |[Lowest Common Ancestor of a Binary Search Tree](problems/LowestCommonAncestorOfABinarySearchTree.py)|Easy      |          |
|236 |[Lowest Common Ancestor of a Binary Tree](problems/LowestCommonAncestorOfABinaryTree.py)|Medium    |          |
|237 |[Delete Node in a Linked List](problems/DeleteNodeInALinkedList.py)             |Easy      |          |
|238 |[Product of Array Except Self](problems/ProductOfArrayExceptSelf.py)            |Medium    |          |
|239 |[Sliding Window Maximum](problems/SlidingWindowMaximum.py)                      |Hard      |          |
|240 |[Search a 2D Matrix II](problems/SearchA2dMatrixIi.py)                          |Medium    |          |
|241 |[Different Ways to Add Parentheses](problems/DifferentWaysToAddParentheses.py)  |Medium    |          |
|242 |[Valid Anagram](problems/ValidAnagram.py)                                       |Easy      |          |
|243 |Shortest Word Distance                                                          |Easy      |Premium   |
|244 |Shortest Word Distance II                                                       |Medium    |Premium   |
|245 |Shortest Word Distance III                                                      |Medium    |Premium   |
|246 |Strobogrammatic Number                                                          |Easy      |Premium   |
|247 |Strobogrammatic Number II                                                       |Medium    |Premium   |
|248 |Strobogrammatic Number III                                                      |Hard      |Premium   |
|249 |[Group Shifted Strings](problems/GroupShiftedStrings.py)                        |Medium    |Premium   |
|250 |Count Univalue Subtrees                                                         |Medium    |Premium   |
|251 |Flatten 2D Vector                                                               |Medium    |Premium   |
|252 |Meeting Rooms                                                                   |Easy      |Premium   |
|253 |Meeting Rooms II                                                                |Medium    |Premium   |
|254 |Factor Combinations                                                             |Medium    |Premium   |
|255 |Verify Preorder Sequence in Binary Search Tree                                  |Medium    |Premium   |
|256 |Paint House                                                                     |Medium    |Premium   |
|257 |[Binary Tree Paths](problems/BinaryTreePaths.py)                                |Easy      |          |
|258 |[Add Digits](problems/AddDigits.py)                                             |Easy      |          |
|259 |3Sum Smaller                                                                    |Medium    |Premium   |
|260 |[Single Number III](problems/SingleNumberIii.py)                                |Medium    |          |
|261 |Graph Valid Tree                                                                |Medium    |Premium   |
|262 |Trips and Users                                                                 |Hard      |Premium   |
|263 |[Ugly Number](problems/UglyNumber.py)                                           |Easy      |          |
|264 |[Ugly Number II](problems/UglyNumberIi.py)                                      |Medium    |Premium   |
|265 |Paint House II                                                                  |Hard      |Premium   |
|266 |Palindrome Permutation                                                          |Easy      |Premium   |
|267 |Palindrome Permutation II                                                       |Medium    |Premium   |
|268 |[Missing Number](problems/MissingNumber.py)                                     |Easy      |          |
|269 |Alien Dictionary                                                                |Hard      |Premium   |
|270 |Closest Binary Search Tree Value                                                |Easy      |Premium   |
|271 |Encode and Decode Strings                                                       |Medium    |Premium   |
|272 |Closest Binary Search Tree Value II                                             |Hard      |Premium   |
|273 |[Integer to English Words](problems/IntegerToEnglishWords.py)                   |Hard      |          |
|274 |[H-Index](problems/HIndex.py)                                                   |Medium    |          |
|275 |[H-Index II](problems/HIndexIi.py)                                              |Medium    |          |
|276 |Paint Fence                                                                     |Easy      |Premium   |
|277 |Find the Celebrity                                                              |Medium    |Premium   |
|278 |[First Bad Version](problems/FirstBadVersion.py)                                |Easy      |          |
|279 |[Perfect Squares](problems/PerfectSquares.py)                                   |Medium    |          |
|280 |Wiggle Sort                                                                     |Medium    |Premium   |
|281 |Zigzag Iterator                                                                 |Medium    |Premium   |
|282 |[Expression Add Operators](problems/ExpressionAddOperators.py)                  |Hard      |          |
|283 |[Move Zeroes](problems/MoveZeroes.py)                                           |Easy      |          |
|284 |[Peeking Iterator](problems/PeekingIterator.py)                                 |Medium    |          |
|285 |Inorder Successor in BST                                                        |Medium    |Premium   |
|286 |Walls and Gates                                                                 |Medium    |Premium   |
|287 |[Find the Duplicate Number](problems/FindTheDuplicateNumber.py)                 |Medium    |          |
|288 |Unique Word Abbreviation                                                        |Medium    |Premium   |
|289 |[Game of Life](problems/GameOfLife.py)                                          |Medium    |          |
|290 |[Word Pattern](problems/WordPattern.py)                                         |Easy      |          |
|291 |Word Pattern II                                                                 |Medium    |Premium   |
|292 |[Nim Game](problems/NimGame.py)                                                 |Easy      |          |
|293 |Flip Game                                                                       |Easy      |Premium   |
|294 |Flip Game II                                                                    |Medium    |Premium   |
|295 |[Find Median from Data Stream](problems/FindMedianFromDataStream.py)            |Hard      |          |
|296 |Best Meeting Point                                                              |Hard      |Premium   |
|297 |[Serialize and Deserialize Binary Tree](problems/SerializeAndDeserializeBinaryTree.py)|Hard      |          |
|298 |Binary Tree Longest Consecutive Sequence                                        |Medium    |Premium   |
|299 |[Bulls and Cows](problems/BullsAndCows.py)                                      |Medium    |          |
|300 |[Longest Increasing Subsequence](problems/LongestIncreasingSubsequence.py)      |Medium    |          |
|301 |[Remove Invalid Parentheses](problems/RemoveInvalidParentheses.py)              |Hard      |          |
|302 |Smallest Rectangle Enclosing Black Pixels                                       |Hard      |Premium   |
|303 |[Range Sum Query - Immutable](problems/RangeSumQueryImmutable.py)               |Easy      |          |
|304 |[Range Sum Query 2D - Immutable](problems/RangeSumQuery2dImmutable.py)          |Medium    |          |
|305 |Number of Islands II                                                            |Hard      |Premium   |
|306 |[Additive Number](problems/AdditiveNumber.py)                                   |Medium    |          |
|307 |[Range Sum Query - Mutable](problems/RangeSumQueryMutable.py)                   |Medium    |          |
|308 |Range Sum Query 2D - Mutable                                                    |Hard      |Premium   |
|309 |[Best Time to Buy and Sell Stock with Cooldown](problems/BestTimeToBuyAndSellStockWithCooldown.py)|Medium    |          |
|310 |[Minimum Height Trees](problems/MinimumHeightTrees.py)                          |Medium    |          |
|311 |Sparse Matrix Multiplication                                                    |Medium    |Premium   |
|312 |[Burst Balloons](problems/BurstBalloons.py)                                     |Hard      |          |
|313 |[Super Ugly Number](problems/SuperUglyNumber.py)                                |Medium    |          |
|314 |Binary Tree Vertical Order Traversal                                            |Medium    |Premium   |
|315 |[Count of Smaller Numbers After Self](problems/CountOfSmallerNumbersAfterSelf.py)|Hard      |          |
|316 |[Remove Duplicate Letters](problems/RemoveDuplicateLetters.py)                  |Medium    |          |
|317 |Shortest Distance from All Buildings                                            |Hard      |Premium   |
|318 |[Maximum Product of Word Lengths](problems/MaximumProductOfWordLengths.py)      |Medium    |          |
|319 |[Bulb Switcher](problems/BulbSwitcher.py)                                       |Medium    |          |
|320 |Generalized Abbreviation                                                        |Medium    |Premium   |
|321 |[Create Maximum Number](problems/CreateMaximumNumber.py)                        |Hard      |          |
|322 |[Coin Change](problems/CoinChange.py)                                           |Medium    |          |
|323 |Number of Connected Components in an Undirected Graph                           |Medium    |Premium   |
|324 |[Wiggle Sort II](problems/WiggleSortIi.py)                                      |Medium    |          |
|325 |Maximum Size Subarray Sum Equals k                                              |Medium    |Premium   |
|326 |[Power of Three](problems/PowerOfThree.py)                                      |Easy      |          |
|327 |[Count of Range Sum](problems/CountOfRangeSum.py)                               |Hard      |          |
|328 |[Odd Even Linked List](problems/OddEvenLinkedList.py)                           |Medium    |          |
|329 |[Longest Increasing Path in a Matrix](problems/LongestIncreasingPathInAMatrix.py)|Hard      |          |
|330 |[Patching Array](problems/PatchingArray.py)                                     |Hard      |          |
|331 |[Verify Preorder Serialization of a Binary Tree](problems/VerifyPreorderSerializationOfABinaryTree.py)|Medium    |          |
|332 |[Reconstruct Itinerary](problems/ReconstructItinerary.py)                       |Medium    |          |
|333 |Largest BST Subtree                                                             |Medium    |Premium   |
|334 |[Increasing Triplet Subsequence](problems/IncreasingTripletSubsequence.py)      |Medium    |          |
|335 |Self Crossing                                                                   |Hard      |          |
|336 |[Palindrome Pairs](problems/PalindromePairs.py)                                 |Hard      |          |
|337 |[House Robber III](problems/HouseRobberIii.py)                                  |Medium    |          |
|338 |[Counting Bits](problems/CountingBits.py)                                       |Medium    |          |
|339 |Nested List Weight Sum                                                          |Medium    |Premium   |
|340 |Longest Substring with At Most K Distinct Characters                            |Medium    |Premium   |
|341 |[Flatten Nested List Iterator](problems/FlattenNestedListIterator.py)           |Medium    |          |
|342 |[Power of Four](problems/PowerOfFour.py)                                        |Easy      |          |
|343 |[Integer Break](problems/IntegerBreak.py)                                       |Medium    |          |
|344 |[Reverse String](problems/ReverseString.py)                                     |Easy      |          |
|345 |[Reverse Vowels of a String](problems/ReverseVowelsOfAString.py)                |Easy      |          |
|346 |Moving Average from Data Stream                                                 |Easy      |Premium   |
|347 |[Top K Frequent Elements](problems/TopKFrequentElements.py)                     |Medium    |          |
|348 |Design Tic-Tac-Toe                                                              |Medium    |Premium   |
|349 |[Intersection of Two Arrays](problems/IntersectionOfTwoArrays.py)               |Easy      |          |
|350 |[Intersection of Two Arrays II](problems/IntersectionOfTwoArraysIi.py)          |Easy      |          |
|351 |Android Unlock Patterns                                                         |Medium    |Premium   |
|352 |Data Stream as Disjoint Intervals                                               |Hard      |          |
|353 |Design Snake Game                                                               |Medium    |Premium   |
|354 |[Russian Doll Envelopes](problems/RussianDollEnvelopes.py)                      |Hard      |          |
|355 |Design Twitter                                                                  |Medium    |          |
|356 |Line Reflection                                                                 |Medium    |Premium   |
|357 |[Count Numbers with Unique Digits](problems/CountNumbersWithUniqueDigits.py)    |Medium    |          |
|358 |Rearrange String k Distance Apart                                               |Hard      |Premium   |
|359 |Logger Rate Limiter                                                             |Easy      |Premium   |
|360 |Sort Transformed Array                                                          |Medium    |Premium   |
|361 |Bomb Enemy                                                                      |Medium    |Premium   |
|362 |Design Hit Counter                                                              |Medium    |Premium   |
|363 |[Max Sum of Rectangle No Larger Than K](problems/MaxSumOfRectangleNoLargerThanK.py)|Hard      |          |
|364 |Nested List Weight Sum II                                                       |Medium    |Premium   |
|365 |[Water and Jug Problem](problems/WaterAndJugProblem.py)                         |Medium    |          |
|366 |Find Leaves of Binary Tree                                                      |Medium    |Premium   |
|367 |[Valid Perfect Square](problems/ValidPerfectSquare.py)                          |Easy      |          |
|368 |[Largest Divisible Subset](problems/LargestDivisibleSubset.py)                  |Medium    |          |
|369 |Plus One Linked List                                                            |Medium    |Premium   |
|370 |Range Addition                                                                  |Medium    |Premium   |
|371 |[Sum of Two Integers](problems/SumOfTwoIntegers.py)                             |Medium    |          |
|372 |[Super Pow](problems/SuperPow.py)                                               |Medium    |          |
|373 |[Find K Pairs with Smallest Sums](problems/FindKPairsWithSmallestSums.py)       |Medium    |          |
|374 |[Guess Number Higher or Lower](problems/GuessNumberHigherOrLower.py)            |Easy      |          |
|375 |Guess Number Higher or Lower II                                                 |Medium    |          |
|376 |[Wiggle Subsequence](problems/WiggleSubsequence.py)                             |Medium    |          |
|377 |[Combination Sum IV](problems/CombinationSumIv.py)                              |Medium    |          |
|378 |[Kth Smallest Element in a Sorted Matrix](problems/KthSmallestElementInASortedMatrix.py)|Medium    |          |
|379 |Design Phone Directory                                                          |Medium    |Premium   |
|380 |[Insert Delete GetRandom O(1)](problems/InsertDeleteGetrandomO1.py)             |Medium    |          |
|381 |Insert Delete GetRandom O(1) - Duplicates allowed                               |Hard      |          |
|382 |[Linked List Random Node](problems/LinkedListRandomNode.py)                     |Medium    |          |
|383 |[Ransom Note](problems/RansomNote.py)                                           |Easy      |          |
|384 |[Shuffle an Array](problems/ShuffleAnArray.py)                                  |Medium    |          |
|385 |[Mini Parser](problems/MiniParser.py)                                           |Medium    |          |
|386 |[Lexicographical Numbers](problems/LexicographicalNumbers.py)                   |Medium    |          |
|387 |[First Unique Character in a String](problems/FirstUniqueCharacterInAString.py) |Easy      |          |
|388 |[Longest Absolute File Path](problems/LongestAbsoluteFilePath.py)               |Medium    |          |
|389 |[Find the Difference](problems/FindTheDifference.py)                            |Easy      |          |
|390 |[Elimination Game](problems/EliminationGame.py)                                 |Medium    |          |
|391 |Perfect Rectangle                                                               |Hard      |          |
|392 |[Is Subsequence](problems/IsSubsequence.py)                                     |Easy      |          |
|393 |[UTF-8 Validation](problems/Utf8Validation.py)                                  |Medium    |          |
|394 |[Decode String](problems/DecodeString.py)                                       |Medium    |          |
|395 |[Longest Substring with At Least K Repeating Characters](problems/LongestSubstringWithAtLeastKRepeatingCharacters.py)|Medium    |          |
|396 |[Rotate Function](problems/RotateFunction.py)                                   |Medium    |          |
|397 |[Integer Replacement](problems/IntegerReplacement.py)                           |Medium    |          |
|398 |[Random Pick Index](problems/RandomPickIndex.py)                                |Medium    |          |
|399 |Evaluate Division                                                               |Medium    |          |
|400 |[Nth Digit](problems/NthDigit.py)                                               |Medium    |          |
|401 |[Binary Watch](problems/BinaryWatch.py)                                         |Easy      |          |
|402 |[Remove K Digits](problems/RemoveKDigits.py)                                    |Medium    |          |
|403 |[Frog Jump](problems/FrogJump.py)                                               |Hard      |          |
|404 |[Sum of Left Leaves](problems/SumOfLeftLeaves.py)                               |Easy      |          |
|405 |[Convert a Number to Hexadecimal](problems/ConvertANumberToHexadecimal.py)      |Easy      |          |
|406 |Queue Reconstruction by Height                                                  |Medium    |          |
|407 |Trapping Rain Water II                                                          |Hard      |          |
|408 |Valid Word Abbreviation                                                         |Easy      |Premium   |
|409 |Longest Palindrome                                                              |Easy      |          |
|410 |[Split Array Largest Sum](problems/SplitArrayLargestSum.py)                     |Hard      |          |
|411 |Minimum Unique Word Abbreviation                                                |Hard      |Premium   |
|412 |[Fizz Buzz](problems/FizzBuzz.py)                                               |Easy      |          |
|413 |[Arithmetic Slices](problems/ArithmeticSlices.py)                               |Medium    |          |
|414 |[Third Maximum Number](problems/ThirdMaximumNumber.py)                          |Easy      |          |
|415 |[Add Strings](problems/AddStrings.py)                                           |Easy      |          |
|416 |Partition Equal Subset Sum                                                      |Medium    |          |
|417 |[Pacific Atlantic Water Flow](problems/PacificAtlanticWaterFlow.py)             |Medium    |          |
|418 |Sentence Screen Fitting                                                         |Medium    |Premium   |
|419 |[Battleships in a Board](problems/BattleshipsInABoard.py)                       |Medium    |          |
|420 |[Strong Password Checker](problems/StrongPasswordChecker.py)                    |Hard      |          |
|421 |[Maximum XOR of Two Numbers in an Array](problems/MaximumXorOfTwoNumbersInAnArray.py)|Medium    |          |
|422 |Valid Word Square                                                               |Easy      |Premium   |
|423 |[Reconstruct Original Digits from English](problems/ReconstructOriginalDigitsFromEnglish.py)|Medium    |          |
|424 |[Longest Repeating Character Replacement](problems/LongestRepeatingCharacterReplacement.py)|Medium    |          |
|425 |Word Squares                                                                    |Hard      |Premium   |
|426 |Convert Binary Search Tree to Sorted Doubly Linked List                         |Medium    |          |
|427 |[Construct Quad Tree](problems/ConstructQuadTree.py)                            |Medium    |          |
|428 |Serialize and Deserialize N-ary Tree                                            |Hard      |Premium   |
|429 |[N-ary Tree Level Order Traversal](problems/NAryTreeLevelOrderTraversal.py)     |Medium    |          |
|430 |[Flatten a Multilevel Doubly Linked List](problems/FlattenAMultilevelDoublyLinkedList.py)|Medium    |          |
|431 |Encode N-ary Tree to Binary Tree                                                |Hard      |Premium   |
|432 |All O`one Data Structure                                                        |Hard      |          |
|433 |Minimum Genetic Mutation                                                        |Medium    |          |
|434 |[Number of Segments in a String](problems/NumberOfSegmentsInAString.py)         |Easy      |          |
|435 |Non-overlapping Intervals                                                       |Medium    |          |
|436 |Find Right Interval                                                             |Medium    |          |
|437 |[Path Sum III](problems/PathSumIii.py)                                          |Medium    |          |
|438 |[Find All Anagrams in a String](problems/FindAllAnagramsInAString.py)           |Medium    |          |
|439 |Ternary Expression Parser                                                       |Medium    |Premium   |
|440 |K-th Smallest in Lexicographical Order                                          |Hard      |          |
|441 |Arranging Coins                                                                 |Easy      |          |
|442 |[Find All Duplicates in an Array](problems/FindAllDuplicatesInAnArray.py)       |Medium    |          |
|443 |[String Compression](problems/StringCompression.py)                             |Medium    |          |
|444 |Sequence Reconstruction                                                         |Medium    |Premium   |
|445 |[Add Two Numbers II](problems/AddTwoNumbersIi.py)                               |Medium    |          |
|446 |[Arithmetic Slices II - Subsequence](problems/ArithmeticSlicesIiSubsequence.py) |Hard      |          |
|447 |[Number of Boomerangs](problems/NumberOfBoomerangs.py)                          |Medium    |          |
|448 |[Find All Numbers Disappeared in an Array](problems/FindAllNumbersDisappearedInAnArray.py)|Easy      |          |
|449 |[Serialize and Deserialize BST](problems/SerializeAndDeserializeBst.py)         |Medium    |          |
|450 |[Delete Node in a BST](problems/DeleteNodeInABst.py)                            |Medium    |          |
|451 |[Sort Characters By Frequency](problems/SortCharactersByFrequency.py)           |Medium    |          |
|452 |Minimum Number of Arrows to Burst Balloons                                      |Medium    |          |
|453 |[Minimum Moves to Equal Array Elements](problems/MinimumMovesToEqualArrayElements.py)|Easy      |          |
|454 |[4Sum II](problems/4sumIi.py)                                                   |Medium    |          |
|455 |Assign Cookies                                                                  |Easy      |          |
|456 |132 Pattern                                                                     |Medium    |          |
|457 |Circular Array Loop                                                             |Medium    |          |
|458 |[Poor Pigs](problems/PoorPigs.py)                                               |Hard      |          |
|459 |Repeated Substring Pattern                                                      |Easy      |          |
|460 |[LFU Cache](problems/LfuCache.py)                                               |Hard      |          |
|461 |[Hamming Distance](problems/HammingDistance.py)                                 |Easy      |          |
|462 |[Minimum Moves to Equal Array Elements II](problems/MinimumMovesToEqualArrayElementsIi.py)|Medium    |          |
|463 |[Island Perimeter](problems/IslandPerimeter.py)                                 |Easy      |          |
|464 |Can I Win                                                                       |Medium    |          |
|465 |Optimal Account Balancing                                                       |Hard      |Premium   |
|466 |Count The Repetitions                                                           |Hard      |          |
|467 |Unique Substrings in Wraparound String                                          |Medium    |          |
|468 |[Validate IP Address](problems/ValidateIpAddress.py)                            |Medium    |          |
|469 |Convex Polygon                                                                  |Medium    |Premium   |
|470 |[Implement Rand10() Using Rand7()](problems/ImplementRand10UsingRand7.py)       |Medium    |          |
|471 |Encode String with Shortest Length                                              |Hard      |Premium   |
|472 |Concatenated Words                                                              |Hard      |          |
|473 |[Matchsticks to Square](problems/MatchsticksToSquare.py)                        |Medium    |          |
|474 |[Ones and Zeroes](problems/OnesAndZeroes.py)                                    |Medium    |          |
|475 |[Heaters](problems/Heaters.py)                                                  |Medium    |          |
|476 |[Number Complement](problems/NumberComplement.py)                               |Easy      |          |
|477 |Total Hamming Distance                                                          |Medium    |          |
|478 |[Generate Random Point in a Circle](problems/GenerateRandomPointInACircle.py)   |Medium    |          |
|479 |Largest Palindrome Product                                                      |Hard      |          |
|480 |Sliding Window Median                                                           |Hard      |          |
|481 |[Magical String](problems/MagicalString.py)                                     |Medium    |          |
|482 |[License Key Formatting](problems/LicenseKeyFormatting.py)                      |Easy      |          |
|483 |[Smallest Good Base](problems/SmallestGoodBase.py)                              |Hard      |          |
|484 |Find Permutation                                                                |Medium    |Premium   |
|485 |[Max Consecutive Ones](problems/MaxConsecutiveOnes.py)                          |Easy      |          |
|486 |[Predict the Winner](problems/PredictTheWinner.py)                              |Medium    |          |
|487 |Max Consecutive Ones II                                                         |Medium    |Premium   |
|488 |[Zuma Game](problems/ZumaGame.py)                                               |Hard      |          |
|489 |Robot Room Cleaner                                                              |Hard      |Premium   |
|490 |The Maze                                                                        |Medium    |Premium   |
|491 |[Increasing Subsequences](problems/IncreasingSubsequences.py)                   |Medium    |          |
|492 |[Construct the Rectangle](problems/ConstructTheRectangle.py)                    |Easy      |          |
|493 |[Reverse Pairs](problems/ReversePairs.py)                                       |Hard      |          |
|494 |[Target Sum](problems/TargetSum.py)                                             |Medium    |          |
|495 |[Teemo Attacking](problems/TeemoAttacking.py)                                   |Medium    |          |
|496 |[Next Greater Element I](problems/NextGreaterElementI.py)                       |Easy      |          |
|497 |Random Point in Non-overlapping Rectangles                                      |Medium    |          |
|498 |[Diagonal Traverse](problems/DiagonalTraverse.py)                               |Medium    |          |
|499 |The Maze III                                                                    |Hard      |Premium   |
|500 |Keyboard Row                                                                    |Easy      |          |
|501 |[Find Mode in Binary Search Tree](problems/FindModeInBinarySearchTree.py)       |Easy      |          |
|502 |[IPO](problems/Ipo.py)                                                          |Hard      |          |
|503 |[Next Greater Element II](problems/NextGreaterElementIi.py)                     |Medium    |          |
|504 |[Base 7](problems/Base7.py)                                                     |Easy      |          |
|505 |The Maze II                                                                     |Medium    |Premium   |
|506 |[Relative Ranks](problems/RelativeRanks.py)                                     |Easy      |          |
|507 |[Perfect Number](problems/PerfectNumber.py)                                     |Easy      |          |
|508 |[Most Frequent Subtree Sum](problems/MostFrequentSubtreeSum.py)                 |Medium    |          |
|509 |[Fibonacci Number](problems/FibonacciNumber.py)                                 |Easy      |          |
|510 |Inorder Successor in BST II                                                     |Medium    |Premium   |
|511 |Game Play Analysis I                                                            |Easy      |Premium   |
|512 |Game Play Analysis II                                                           |Easy      |Premium   |
|513 |[Find Bottom Left Tree Value](problems/FindBottomLeftTreeValue.py)              |Medium    |          |
|514 |Freedom Trail                                                                   |Hard      |          |
|515 |[Find Largest Value in Each Tree Row](problems/FindLargestValueInEachTreeRow.py)|Medium    |          |
|516 |Longest Palindromic Subsequence                                                 |Medium    |          |
|517 |Super Washing Machines                                                          |Hard      |          |
|518 |[Coin Change 2](problems/CoinChange2.py)                                        |Medium    |          |
|519 |[Random Flip Matrix](problems/RandomFlipMatrix.py)                              |Medium    |          |
|520 |[Detect Capital](problems/DetectCapital.py)                                     |Easy      |          |
|521 |[Longest Uncommon Subsequence I](problems/LongestUncommonSubsequenceI.py)       |Easy      |          |
|522 |[Longest Uncommon Subsequence II](problems/LongestUncommonSubsequenceIi.py)     |Medium    |          |
|523 |[Continuous Subarray Sum](problems/ContinuousSubarraySum.py)                    |Medium    |          |
|524 |[Longest Word in Dictionary through Deleting](problems/LongestWordInDictionaryThroughDeleting.py)|Medium    |          |
|525 |[Contiguous Array](problems/ContiguousArray.py)                                 |Medium    |          |
|526 |Beautiful Arrangement                                                           |Medium    |          |
|527 |Word Abbreviation                                                               |Hard      |Premium   |
|528 |Random Pick with Weight                                                         |Medium    |          |
|529 |[Minesweeper](problems/Minesweeper.py)                                          |Medium    |          |
|530 |[Minimum Absolute Difference in BST](problems/MinimumAbsoluteDifferenceInBst.py)|Easy      |          |
|531 |Lonely Pixel I                                                                  |Medium    |Premium   |
|532 |K-diff Pairs in an Array                                                        |Medium    |          |
|533 |Lonely Pixel II                                                                 |Medium    |Premium   |
|534 |Game Play Analysis III                                                          |Medium    |Premium   |
|535 |[Encode and Decode TinyURL](problems/EncodeAndDecodeTinyurl.py)                 |Medium    |          |
|536 |Construct Binary Tree from String                                               |Medium    |Premium   |
|537 |[Complex Number Multiplication](problems/ComplexNumberMultiplication.py)        |Medium    |          |
|538 |[Convert BST to Greater Tree](problems/ConvertBstToGreaterTree.py)              |Medium    |          |
|539 |[Minimum Time Difference](problems/MinimumTimeDifference.py)                    |Medium    |          |
|540 |[Single Element in a Sorted Array](problems/SingleElementInASortedArray.py)     |Medium    |          |
|541 |[Reverse String II](problems/ReverseStringIi.py)                                |Easy      |          |
|542 |[01 Matrix](problems/01Matrix.py)                                               |Medium    |          |
|543 |[Diameter of Binary Tree](problems/DiameterOfBinaryTree.py)                     |Easy      |          |
|544 |Output Contest Matches                                                          |Medium    |Premium   |
|545 |Boundary of Binary Tree                                                         |Medium    |Premium   |
|546 |[Remove Boxes](problems/RemoveBoxes.py)                                         |Hard      |          |
|547 |[Number of Provinces](problems/NumberOfProvinces.py)                            |Medium    |          |
|548 |Split Array with Equal Sum                                                      |Medium    |Premium   |
|549 |Binary Tree Longest Consecutive Sequence II                                     |Medium    |Premium   |
|550 |Game Play Analysis IV                                                           |Medium    |Premium   |
|551 |[Student Attendance Record I](problems/StudentAttendanceRecordI.py)             |Easy      |          |
|552 |Student Attendance Record II                                                    |Hard      |          |
|553 |[Optimal Division](problems/OptimalDivision.py)                                 |Medium    |          |
|554 |[Brick Wall](problems/BrickWall.py)                                             |Medium    |          |
|555 |Split Concatenated Strings                                                      |Medium    |Premium   |
|556 |Next Greater Element III                                                        |Medium    |          |
|557 |[Reverse Words in a String III](problems/ReverseWordsInAStringIii.py)           |Easy      |          |
|558 |Logical OR of Two Binary Grids Represented as Quad-Trees                        |Medium    |          |
|559 |[Maximum Depth of N-ary Tree](problems/MaximumDepthOfNAryTree.py)               |Easy      |          |
|560 |[Subarray Sum Equals K](problems/SubarraySumEqualsK.py)                         |Medium    |          |
|561 |[Array Partition I](problems/ArrayPartitionI.py)                                |Easy      |          |
|562 |Longest Line of Consecutive One in Matrix                                       |Medium    |Premium   |
|563 |[Binary Tree Tilt](problems/BinaryTreeTilt.py)                                  |Easy      |          |
|564 |Find the Closest Palindrome                                                     |Hard      |          |
|565 |[Array Nesting](problems/ArrayNesting.py)                                       |Medium    |          |
|566 |[Reshape the Matrix](problems/ReshapeTheMatrix.py)                              |Easy      |          |
|567 |[Permutation in String](problems/PermutationInString.py)                        |Medium    |          |
|568 |Maximum Vacation Days                                                           |Hard      |Premium   |
|569 |Median Employee Salary                                                          |Hard      |Premium   |
|570 |Managers with at Least 5 Direct Reports                                         |Medium    |Premium   |
|571 |Find Median Given Frequency of Numbers                                          |Hard      |Premium   |
|572 |[Subtree of Another Tree](problems/SubtreeOfAnotherTree.py)                     |Easy      |          |
|573 |Squirrel Simulation                                                             |Medium    |Premium   |
|574 |Winning Candidate                                                               |Medium    |Premium   |
|575 |[Distribute Candies](problems/DistributeCandies.py)                             |Easy      |          |
|576 |[Out of Boundary Paths](problems/OutOfBoundaryPaths.py)                         |Medium    |          |
|577 |Employee Bonus                                                                  |Easy      |Premium   |
|578 |Get Highest Answer Rate Question                                                |Medium    |Premium   |
|579 |Find Cumulative Salary of an Employee                                           |Hard      |Premium   |
|580 |Count Student Number in Departments                                             |Medium    |Premium   |
|581 |[Shortest Unsorted Continuous Subarray](problems/ShortestUnsortedContinuousSubarray.py)|Medium    |          |
|582 |Kill Process                                                                    |Medium    |Premium   |
|583 |[Delete Operation for Two Strings](problems/DeleteOperationForTwoStrings.py)    |Medium    |          |
|584 |Find Customer Referee                                                           |Easy      |Premium   |
|585 |Investments in 2016                                                             |Medium    |Premium   |
|586 |Customer Placing the Largest Number of Orders                                   |Easy      |Premium   |
|587 |[Erect the Fence](problems/ErectTheFence.py)                                    |Hard      |          |
|588 |Design In-Memory File System                                                    |Hard      |Premium   |
|589 |[N-ary Tree Preorder Traversal](problems/NAryTreePreorderTraversal.py)          |Easy      |          |
|590 |[N-ary Tree Postorder Traversal](problems/NAryTreePostorderTraversal.py)        |Easy      |          |
|591 |Tag Validator                                                                   |Hard      |          |
|592 |[Fraction Addition and Subtraction](problems/FractionAdditionAndSubtraction.py) |Medium    |          |
|593 |[Valid Square](problems/ValidSquare.py)                                         |Medium    |          |
|594 |[Longest Harmonious Subsequence](problems/LongestHarmoniousSubsequence.py)      |Easy      |          |
|595 |[Big Countries](problems/BigCountries.mysql)                                    |Easy      |          |
|596 |Classes More Than 5 Students                                                    |Easy      |          |
|597 |Friend Requests I: Overall Acceptance Rate                                      |Easy      |Premium   |
|598 |[Range Addition II](problems/RangeAdditionIi.py)                                |Easy      |          |
|599 |[Minimum Index Sum of Two Lists](problems/MinimumIndexSumOfTwoLists.py)         |Easy      |          |
|600 |[Non-negative Integers without Consecutive Ones](problems/NonNegativeIntegersWithoutConsecutiveOnes.py)|Hard      |          |
|601 |Human Traffic of Stadium                                                        |Hard      |          |
|602 |Friend Requests II: Who Has the Most Friends                                    |Medium    |Premium   |
|603 |Consecutive Available Seats                                                     |Easy      |Premium   |
|604 |Design Compressed String Iterator                                               |Easy      |Premium   |
|605 |[Can Place Flowers](problems/CanPlaceFlowers.py)                                |Easy      |          |
|606 |[Construct String from Binary Tree](problems/ConstructStringFromBinaryTree.py)  |Easy      |          |
|607 |Sales Person                                                                    |Easy      |Premium   |
|608 |Tree Node                                                                       |Medium    |Premium   |
|609 |[Find Duplicate File in System](problems/FindDuplicateFileInSystem.py)          |Medium    |          |
|610 |Triangle Judgement                                                              |Easy      |Premium   |
|611 |[Valid Triangle Number](problems/ValidTriangleNumber.py)                        |Medium    |          |
|612 |Shortest Distance in a Plane                                                    |Medium    |Premium   |
|613 |Shortest Distance in a Line                                                     |Easy      |Premium   |
|614 |Second Degree Follower                                                          |Medium    |Premium   |
|615 |Average Salary: Departments VS Company                                          |Hard      |Premium   |
|616 |Add Bold Tag in String                                                          |Medium    |Premium   |
|617 |[Merge Two Binary Trees](problems/MergeTwoBinaryTrees.py)                       |Easy      |          |
|618 |Students Report By Geography                                                    |Hard      |Premium   |
|619 |Biggest Single Number                                                           |Easy      |Premium   |
|620 |[Not Boring Movies](problems/NotBoringMovies.mysql)                             |Easy      |          |
|621 |[Task Scheduler](problems/TaskScheduler.py)                                     |Medium    |          |
|622 |[Design Circular Queue](problems/DesignCircularQueue.py)                        |Medium    |          |
|623 |[Add One Row to Tree](problems/AddOneRowToTree.py)                              |Medium    |          |
|624 |Maximum Distance in Arrays                                                      |Medium    |Premium   |
|625 |Minimum Factorization                                                           |Medium    |Premium   |
|626 |Exchange Seats - SQL                                                            |Medium    |          |
|627 |[Swap Salary](problems/SwapSalary.mysql)                                        |Easy      |          |
|628 |[Maximum Product of Three Numbers](problems/MaximumProductOfThreeNumbers.py)    |Easy      |          |
|629 |[K Inverse Pairs Array](problems/KInversePairsArray.py)                         |Hard      |          |
|630 |[Course Schedule III](problems/CourseScheduleIii.py)                            |Hard      |          |
|631 |Design Excel Sum Formula                                                        |Hard      |Premium   |
|632 |Smallest Range Covering Elements from K Lists                                   |Hard      |          |
|633 |[Sum of Square Numbers](problems/SumOfSquareNumbers.py)                         |Medium    |          |
|634 |Find the Derangement of An Array                                                |Medium    |Premium   |
|635 |Design Log Storage System                                                       |Medium    |Premium   |
|636 |Exclusive Time of Functions                                                     |Medium    |          |
|637 |[Average of Levels in Binary Tree](problems/AverageOfLevelsInBinaryTree.py)     |Easy      |          |
|638 |[Shopping Offers](problems/ShoppingOffers.py)                                   |Medium    |          |
|639 |[Decode Ways II](problems/DecodeWaysIi.py)                                      |Hard      |          |
|640 |Solve the Equation                                                              |Medium    |          |
|641 |Design Circular Deque                                                           |Medium    |          |
|642 |Design Search Autocomplete System                                               |Hard      |Premium   |
|643 |[Maximum Average Subarray I](problems/MaximumAverageSubarrayI.py)               |Easy      |          |
|644 |Maximum Average Subarray II                                                     |Hard      |Premium   |
|645 |[Set Mismatch](problems/SetMismatch.py)                                         |Easy      |          |
|646 |[Maximum Length of Pair Chain](problems/MaximumLengthOfPairChain.py)            |Medium    |          |
|647 |[Palindromic Substrings](problems/PalindromicSubstrings.py)                     |Medium    |          |
|648 |[Replace Words](problems/ReplaceWords.py)                                       |Medium    |          |
|649 |Dota2 Senate                                                                    |Medium    |          |
|650 |[2 Keys Keyboard](problems/2KeysKeyboard.py)                                    |Medium    |          |
|651 |4 Keys Keyboard                                                                 |Medium    |Premium   |
|652 |[Find Duplicate Subtrees](problems/FindDuplicateSubtrees.py)                    |Medium    |          |
|653 |[Two Sum IV - Input is a BST](problems/TwoSumIvInputIsABst.py)                  |Easy      |          |
|654 |[Maximum Binary Tree](problems/MaximumBinaryTree.py)                            |Medium    |          |
|655 |Print Binary Tree                                                               |Medium    |          |
|656 |Coin Path                                                                       |Hard      |Premium   |
|657 |[Robot Return to Origin](problems/RobotReturnToOrigin.py)                       |Easy      |          |
|658 |[Find K Closest Elements](problems/FindKClosestElements.py)                     |Medium    |          |
|659 |Split Array into Consecutive Subsequences                                       |Medium    |          |
|660 |Remove 9                                                                        |Hard      |Premium   |
|661 |Image Smoother                                                                  |Easy      |          |
|662 |Maximum Width of Binary Tree                                                    |Medium    |          |
|663 |Equal Tree Partition                                                            |Medium    |Premium   |
|664 |Strange Printer                                                                 |Hard      |          |
|665 |[Non-decreasing Array](problems/NonDecreasingArray.py)                          |Medium    |          |
|666 |Path Sum IV                                                                     |Medium    |Premium   |
|667 |[Beautiful Arrangement II](problems/BeautifulArrangementIi.py)                  |Medium    |          |
|668 |[Kth Smallest Number in Multiplication Table](problems/KthSmallestNumberInMultiplicationTable.py)|Hard      |          |
|669 |[Trim a Binary Search Tree](problems/TrimABinarySearchTree.py)                  |Medium    |          |
|670 |Maximum Swap                                                                    |Medium    |          |
|671 |Second Minimum Node In a Binary Tree                                            |Easy      |          |
|672 |Bulb Switcher II                                                                |Medium    |          |
|673 |[Number of Longest Increasing Subsequence](problems/NumberOfLongestIncreasingSubsequence.py)|Medium    |          |
|674 |Longest Continuous Increasing Subsequence                                       |Easy      |          |
|675 |Cut Off Trees for Golf Event                                                    |Hard      |          |
|676 |[Implement Magic Dictionary](problems/ImplementMagicDictionary.py)              |Medium    |          |
|677 |[Map Sum Pairs](problems/MapSumPairs.py)                                        |Medium    |          |
|678 |[Valid Parenthesis String](problems/ValidParenthesisString.py)                  |Medium    |          |
|679 |[24 Game](problems/24Game.py)                                                   |Hard      |          |
|680 |[Valid Palindrome II](problems/ValidPalindromeIi.py)                            |Easy      |          |
|681 |Next Closest Time                                                               |Medium    |Premium   |
|682 |[Baseball Game](problems/BaseballGame.py)                                       |Easy      |          |
|683 |K Empty Slots                                                                   |Hard      |Premium   |
|684 |[Redundant Connection](problems/RedundantConnection.py)                         |Medium    |          |
|685 |[Redundant Connection II](problems/RedundantConnectionIi.py)                    |Hard      |          |
|686 |[Repeated String Match](problems/RepeatedStringMatch.py)                        |Medium    |          |
|687 |[Longest Univalue Path](problems/LongestUnivaluePath.py)                        |Medium    |          |
|688 |[Knight Probability in Chessboard](problems/KnightProbabilityInChessboard.py)   |Medium    |          |
|689 |Maximum Sum of 3 Non-Overlapping Subarrays                                      |Hard      |          |
|690 |[Employee Importance](problems/EmployeeImportance.py)                           |Easy      |          |
|691 |[Stickers to Spell Word](problems/StickersToSpellWord.py)                       |Hard      |          |
|692 |[Top K Frequent Words](problems/TopKFrequentWords.py)                           |Medium    |          |
|693 |[Binary Number with Alternating Bits](problems/BinaryNumberWithAlternatingBits.py)|Easy      |          |
|694 |Number of Distinct Islands                                                      |Medium    |Premium   |
|695 |[Max Area of Island](problems/MaxAreaOfIsland.py)                               |Medium    |          |
|696 |[Count Binary Substrings](problems/CountBinarySubstrings.py)                    |Easy      |          |
|697 |[Degree of an Array](problems/DegreeOfAnArray.py)                               |Easy      |          |
|698 |[Partition to K Equal Sum Subsets](problems/PartitionToKEqualSumSubsets.py)     |Medium    |          |
|699 |[Falling Squares](problems/FallingSquares.py)                                   |Hard      |          |
|700 |[Search in a Binary Search Tree](problems/SearchInABinarySearchTree.py)         |Easy      |          |
|701 |[Insert into a Binary Search Tree](problems/InsertIntoABinarySearchTree.py)     |Medium    |          |
|702 |Search in a Sorted Array of Unknown Size                                        |Medium    |Premium   |
|703 |[Kth Largest Element in a Stream](problems/KthLargestElementInAStream.py)       |Easy      |          |
|704 |[Binary Search](problems/BinarySearch.py)                                       |Easy      |          |
|705 |[Design HashSet](problems/DesignHashset.py)                                     |Easy      |          |
|706 |[Design HashMap](problems/DesignHashmap.py)                                     |Easy      |          |
|707 |[Design Linked List](problems/DesignLinkedList.py)                              |Medium    |          |
|708 |Insert into a Sorted Circular Linked List                                       |Medium    |Premium   |
|709 |[To Lower Case](problems/ToLowerCase.py)                                        |Easy      |          |
|710 |Random Pick with Blacklist                                                      |Hard      |          |
|711 |Number of Distinct Islands II                                                   |Hard      |Premium   |
|712 |[Minimum ASCII Delete Sum for Two Strings](problems/MinimumAsciiDeleteSumForTwoStrings.py)|Medium    |          |
|713 |[Subarray Product Less Than K](problems/SubarrayProductLessThanK.py)            |Medium    |          |
|714 |[Best Time to Buy and Sell Stock with Transaction Fee](problems/BestTimeToBuyAndSellStockWithTransactionFee.py)|Medium    |          |
|715 |[Range Module](problems/RangeModule.py)                                         |Hard      |          |
|716 |Max Stack                                                                       |Easy      |Premium   |
|717 |[1-bit and 2-bit Characters](problems/1BitAnd2BitCharacters.py)                 |Easy      |          |
|718 |[Maximum Length of Repeated Subarray](problems/MaximumLengthOfRepeatedSubarray.py)|Medium    |          |
|719 |[Find K-th Smallest Pair Distance](problems/FindKThSmallestPairDistance.py)     |Hard      |          |
|720 |[Longest Word in Dictionary](problems/LongestWordInDictionary.py)               |Easy      |          |
|721 |[Accounts Merge](problems/AccountsMerge.py)                                     |Medium    |          |
|722 |[Remove Comments](problems/RemoveComments.py)                                   |Medium    |          |
|723 |Candy Crush                                                                     |Medium    |Premium   |
|724 |[Find Pivot Index](problems/FindPivotIndex.py)                                  |Easy      |          |
|725 |[Split Linked List in Parts](problems/SplitLinkedListInParts.py)                |Medium    |          |
|726 |[Number of Atoms](problems/NumberOfAtoms.py)                                    |Hard      |          |
|727 |Minimum Window Subsequence                                                      |Hard      |Premium   |
|728 |[Self Dividing Numbers](problems/SelfDividingNumbers.py)                        |Easy      |          |
|729 |[My Calendar I](problems/MyCalendarI.py)                                        |Medium    |          |
|730 |[Count Different Palindromic Subsequences](problems/CountDifferentPalindromicSubsequences.py)|Hard      |          |
|731 |[My Calendar II](problems/MyCalendarIi.py)                                      |Medium    |          |
|732 |[My Calendar III](problems/MyCalendarIii.py)                                    |Hard      |          |
|733 |[Flood Fill](problems/FloodFill.py)                                             |Easy      |          |
|734 |Sentence Similarity                                                             |Easy      |Premium   |
|735 |[Asteroid Collision](problems/AsteroidCollision.py)                             |Medium    |          |
|736 |Parse Lisp Expression                                                           |Hard      |          |
|737 |Sentence Similarity II                                                          |Medium    |Premium   |
|738 |[Monotone Increasing Digits](problems/MonotoneIncreasingDigits.py)              |Medium    |          |
|739 |[Daily Temperatures](problems/DailyTemperatures.py)                             |Medium    |          |
|740 |[Delete and Earn](problems/DeleteAndEarn.py)                                    |Medium    |          |
|741 |Cherry Pickup                                                                   |Hard      |          |
|742 |Closest Leaf in a Binary Tree                                                   |Medium    |Premium   |
|743 |Network Delay Time                                                              |Medium    |          |
|744 |[Find Smallest Letter Greater Than Target](problems/FindSmallestLetterGreaterThanTarget.py)|Easy      |          |
|745 |[Prefix and Suffix Search](problems/PrefixAndSuffixSearch.py)                   |Hard      |          |
|746 |[Min Cost Climbing Stairs](problems/MinCostClimbingStairs.py)                   |Easy      |          |
|747 |[Largest Number At Least Twice of Others](problems/LargestNumberAtLeastTwiceOfOthers.py)|Easy      |          |
|748 |Shortest Completing Word                                                        |Easy      |          |
|749 |[Contain Virus](problems/ContainVirus.py)                                       |Hard      |          |
|750 |Number Of Corner Rectangles                                                     |Medium    |Premium   |
|751 |IP to CIDR                                                                      |Medium    |Premium   |
|752 |[Open the Lock](problems/OpenTheLock.py)                                        |Medium    |          |
|753 |Cracking the Safe                                                               |Hard      |          |
|754 |Reach a Number                                                                  |Medium    |          |
|755 |Pour Water                                                                      |Medium    |Premium   |
|756 |Pyramid Transition Matrix                                                       |Medium    |          |
|757 |Set Intersection Size At Least Two                                              |Hard      |          |
|758 |Bold Words in String                                                            |Easy      |Premium   |
|759 |Employee Free Time                                                              |Hard      |Premium   |
|760 |Find Anagram Mappings                                                           |Easy      |Premium   |
|761 |Special Binary String                                                           |Hard      |          |
|762 |Prime Number of Set Bits in Binary Representation                               |Easy      |          |
|763 |Partition Labels                                                                |Medium    |          |
|764 |[Largest Plus Sign](problems/LargestPlusSign.py)                                |Medium    |          |
|765 |[Couples Holding Hands](problems/CouplesHoldingHands.py)                        |Hard      |          |
|766 |[Toeplitz Matrix](problems/ToeplitzMatrix.py)                                   |Easy      |          |
|767 |[Reorganize String](problems/ReorganizeString.py)                               |Medium    |          |
|768 |Max Chunks To Make Sorted II                                                    |Hard      |          |
|769 |Max Chunks To Make Sorted                                                       |Medium    |          |
|770 |Basic Calculator IV                                                             |Hard      |          |
|771 |[Jewels and Stones](problems/JewelsAndStones.py)                                |Easy      |          |
|772 |Basic Calculator III                                                            |Hard      |Premium   |
|773 |Sliding Puzzle                                                                  |Hard      |          |
|774 |Minimize Max Distance to Gas Station                                            |Hard      |Premium   |
|775 |[Global and Local Inversions](problems/GlobalAndLocalInversions.py)             |Medium    |          |
|776 |Split BST                                                                       |Medium    |Premium   |
|777 |Swap Adjacent in LR String                                                      |Medium    |          |
|778 |[Swim in Rising Water](problems/SwimInRisingWater.py)                           |Hard      |          |
|779 |[K-th Symbol in Grammar](problems/KThSymbolInGrammar.py)                        |Medium    |          |
|780 |Reaching Points                                                                 |Hard      |          |
|781 |Rabbits in Forest                                                               |Medium    |          |
|782 |[Transform to Chessboard](problems/TransformToChessboard.py)                    |Hard      |          |
|783 |[Minimum Distance Between BST Nodes](problems/MinimumDistanceBetweenBstNodes.py)|Easy      |          |
|784 |[Letter Case Permutation](problems/LetterCasePermutation.py)                    |Medium    |          |
|785 |[Is Graph Bipartite?](problems/IsGraphBipartite.py)                             |Medium    |          |
|786 |K-th Smallest Prime Fraction                                                    |Hard      |          |
|787 |Cheapest Flights Within K Stops                                                 |Medium    |          |
|788 |Rotated Digits                                                                  |Easy      |          |
|789 |[Escape The Ghosts](problems/EscapeTheGhosts.py)                                |Medium    |          |
|790 |[Domino and Tromino Tiling](problems/DominoAndTrominoTiling.py)                 |Medium    |          |
|791 |[Custom Sort String](problems/CustomSortString.py)                              |Medium    |          |
|792 |[Number of Matching Subsequences](problems/NumberOfMatchingSubsequences.py)     |Medium    |          |
|793 |Preimage Size of Factorial Zeroes Function                                      |Hard      |          |
|794 |[Valid Tic-Tac-Toe State](problems/ValidTicTacToeState.py)                      |Medium    |          |
|795 |[Number of Subarrays with Bounded Maximum](problems/NumberOfSubarraysWithBoundedMaximum.py)|Medium    |          |
|796 |[Rotate String](problems/RotateString.py)                                       |Easy      |          |
|797 |[All Paths From Source to Target](problems/AllPathsFromSourceToTarget.py)       |Medium    |          |
|798 |[Smallest Rotation with Highest Score](problems/SmallestRotationWithHighestScore.py)|Hard      |          |
|799 |[Champagne Tower](problems/ChampagneTower.py)                                   |Medium    |          |
|800 |Similar RGB Color                                                               |Easy      |Premium   |
|801 |Minimum Swaps To Make Sequences Increasing                                      |Medium    |          |
|802 |Find Eventual Safe States                                                       |Medium    |          |
|803 |Bricks Falling When Hit                                                         |Hard      |          |
|804 |[Unique Morse Code Words](problems/UniqueMorseCodeWords.py)                     |Easy      |          |
|805 |Split Array With Same Average                                                   |Hard      |          |
|806 |Number of Lines To Write String                                                 |Easy      |          |
|807 |Max Increase to Keep City Skyline                                               |Medium    |          |
|808 |[Soup Servings](problems/SoupServings.py)                                       |Medium    |          |
|809 |[Expressive Words](problems/ExpressiveWords.py)                                 |Medium    |          |
|810 |Chalkboard XOR Game                                                             |Hard      |          |
|811 |Subdomain Visit Count                                                           |Easy      |          |
|812 |Largest Triangle Area                                                           |Easy      |          |
|813 |Largest Sum of Averages                                                         |Medium    |          |
|814 |[Binary Tree Pruning](problems/BinaryTreePruning.py)                            |Medium    |          |
|815 |[Bus Routes](problems/BusRoutes.py)                                             |Hard      |          |
|816 |[Ambiguous Coordinates](problems/AmbiguousCoordinates.py)                       |Medium    |          |
|817 |Linked List Components                                                          |Medium    |          |
|818 |Race Car                                                                        |Hard      |          |
|819 |[Most Common Word](problems/MostCommonWord.py)                                  |Easy      |          |
|820 |[Short Encoding of Words](problems/ShortEncodingOfWords.py)                     |Medium    |          |
|821 |[Shortest Distance to a Character](problems/ShortestDistanceToACharacter.py)    |Easy      |          |
|822 |Card Flipping Game                                                              |Medium    |          |
|823 |[Binary Trees With Factors](problems/BinaryTreesWithFactors.py)                 |Medium    |          |
|824 |[Goat Latin](problems/GoatLatin.py)                                             |Easy      |          |
|825 |Friends Of Appropriate Ages                                                     |Medium    |          |
|826 |Most Profit Assigning Work                                                      |Medium    |          |
|827 |[Making A Large Island](problems/MakingALargeIsland.py)                         |Hard      |          |
|828 |Count Unique Characters of All Substrings of a Given String                     |Hard      |          |
|829 |Consecutive Numbers Sum                                                         |Hard      |          |
|830 |[Positions of Large Groups](problems/PositionsOfLargeGroups.py)                 |Easy      |          |
|831 |Masking Personal Information                                                    |Medium    |          |
|832 |[Flipping an Image](problems/FlippingAnImage.py)                                |Easy      |          |
|833 |[Find And Replace in String](problems/FindAndReplaceInString.py)                |Medium    |          |
|834 |[Sum of Distances in Tree](problems/SumOfDistancesInTree.py)                    |Hard      |          |
|835 |[Image Overlap](problems/ImageOverlap.py)                                       |Medium    |          |
|836 |Rectangle Overlap                                                               |Easy      |          |
|837 |New 21 Game                                                                     |Medium    |          |
|838 |[Push Dominoes](problems/PushDominoes.py)                                       |Medium    |          |
|839 |Similar String Groups                                                           |Hard      |          |
|840 |[Magic Squares In Grid](problems/MagicSquaresInGrid.py)                         |Medium    |          |
|841 |[Keys and Rooms](problems/KeysAndRooms.py)                                      |Medium    |          |
|842 |[Split Array into Fibonacci Sequence](problems/SplitArrayIntoFibonacciSequence.py)|Medium    |          |
|843 |[Guess the Word](problems/GuessTheWord.py)                                      |Hard      |          |
|844 |[Backspace String Compare](problems/BackspaceStringCompare.py)                  |Easy      |          |
|845 |Longest Mountain in Array                                                       |Medium    |          |
|846 |Hand of Straights                                                               |Medium    |          |
|847 |[Shortest Path Visiting All Nodes](problems/ShortestPathVisitingAllNodes.py)    |Hard      |          |
|848 |[Shifting Letters](problems/ShiftingLetters.py)                                 |Medium    |          |
|849 |[Maximize Distance to Closest Person](problems/MaximizeDistanceToClosestPerson.py)|Medium    |          |
|850 |[Rectangle Area II](problems/RectangleAreaIi.py)                                |Hard      |          |
|851 |[Loud and Rich](problems/LoudAndRich.py)                                        |Medium    |          |
|852 |[Peak Index in a Mountain Array](problems/PeakIndexInAMountainArray.py)         |Easy      |          |
|853 |[Car Fleet](problems/CarFleet.py)                                               |Medium    |          |
|854 |K-Similar Strings                                                               |Hard      |          |
|855 |[Exam Room](problems/ExamRoom.py)                                               |Medium    |          |
|856 |[Score of Parentheses](problems/ScoreOfParentheses.py)                          |Medium    |          |
|857 |Minimum Cost to Hire K Workers                                                  |Hard      |          |
|858 |[Mirror Reflection](problems/MirrorReflection.py)                               |Medium    |          |
|859 |[Buddy Strings](problems/BuddyStrings.py)                                       |Easy      |          |
|860 |[Lemonade Change](problems/LemonadeChange.py)                                   |Easy      |          |
|861 |[Score After Flipping Matrix](problems/ScoreAfterFlippingMatrix.py)             |Medium    |          |
|862 |[Shortest Subarray with Sum at Least K](problems/ShortestSubarrayWithSumAtLeastK.py)|Hard      |          |
|863 |[All Nodes Distance K in Binary Tree](problems/AllNodesDistanceKInBinaryTree.py)|Medium    |          |
|864 |Shortest Path to Get All Keys                                                   |Hard      |          |
|865 |[Smallest Subtree with all the Deepest Nodes](problems/SmallestSubtreeWithAllTheDeepestNodes.py)|Medium    |          |
|866 |[Prime Palindrome](problems/PrimePalindrome.py)                                 |Medium    |          |
|867 |[Transpose Matrix](problems/TransposeMatrix.py)                                 |Easy      |          |
|868 |[Binary Gap](problems/BinaryGap.py)                                             |Easy      |          |
|869 |[Reordered Power of 2](problems/ReorderedPowerOf2.py)                           |Medium    |          |
|870 |[Advantage Shuffle](problems/AdvantageShuffle.py)                               |Medium    |          |
|871 |[Minimum Number of Refueling Stops](problems/MinimumNumberOfRefuelingStops.py)  |Hard      |          |
|872 |[Leaf-Similar Trees](problems/LeafSimilarTrees.py)                              |Easy      |          |
|873 |Length of Longest Fibonacci Subsequence                                         |Medium    |          |
|874 |[Walking Robot Simulation](problems/WalkingRobotSimulation.py)                  |Easy      |          |
|875 |Koko Eating Bananas                                                             |Medium    |          |
|876 |[Middle of the Linked List](problems/MiddleOfTheLinkedList.py)                  |Easy      |          |
|877 |[Stone Game](problems/StoneGame.py)                                             |Medium    |          |
|878 |Nth Magical Number                                                              |Hard      |          |
|879 |Profitable Schemes                                                              |Hard      |          |
|880 |[Decoded String at Index](problems/DecodedStringAtIndex.py)                     |Medium    |          |
|881 |[Boats to Save People](problems/BoatsToSavePeople.py)                           |Medium    |          |
|882 |Reachable Nodes In Subdivided Graph                                             |Hard      |          |
|883 |[Projection Area of 3D Shapes](problems/ProjectionAreaOf3dShapes.py)            |Easy      |          |
|884 |[Uncommon Words from Two Sentences](problems/UncommonWordsFromTwoSentences.py)  |Easy      |          |
|885 |[Spiral Matrix III](problems/SpiralMatrixIii.py)                                |Medium    |          |
|886 |Possible Bipartition                                                            |Medium    |          |
|887 |Super Egg Drop                                                                  |Hard      |          |
|888 |[Fair Candy Swap](problems/FairCandySwap.py)                                    |Easy      |          |
|889 |Construct Binary Tree from Preorder and Postorder Traversal                     |Medium    |          |
|890 |[Find and Replace Pattern](problems/FindAndReplacePattern.py)                   |Medium    |          |
|891 |Sum of Subsequence Widths                                                       |Hard      |          |
|892 |Surface Area of 3D Shapes                                                       |Easy      |          |
|893 |Groups of Special-Equivalent Strings                                            |Easy      |          |
|894 |All Possible Full Binary Trees                                                  |Medium    |          |
|895 |[Maximum Frequency Stack](problems/MaximumFrequencyStack.py)                    |Hard      |          |
|896 |[Monotonic Array](problems/MonotonicArray.py)                                   |Easy      |          |
|897 |[Increasing Order Search Tree](problems/IncreasingOrderSearchTree.py)           |Easy      |          |
|898 |Bitwise ORs of Subarrays                                                        |Medium    |          |
|899 |[Orderly Queue](problems/OrderlyQueue.py)                                       |Hard      |          |
|900 |RLE Iterator                                                                    |Medium    |          |
|901 |[Online Stock Span](problems/OnlineStockSpan.py)                                |Medium    |          |
|902 |[Numbers At Most N Given Digit Set](problems/NumbersAtMostNGivenDigitSet.py)    |Hard      |          |
|903 |[Valid Permutations for DI Sequence](problems/ValidPermutationsForDiSequence.py)|Hard      |          |
|904 |[Fruit Into Baskets](problems/FruitIntoBaskets.py)                              |Medium    |          |
|905 |[Sort Array By Parity](problems/SortArrayByParity.py)                           |Easy      |          |
|906 |[Super Palindromes](problems/SuperPalindromes.py)                               |Hard      |          |
|907 |[Sum of Subarray Minimums](problems/SumOfSubarrayMinimums.py)                   |Medium    |          |
|908 |[Smallest Range I](problems/SmallestRangeI.py)                                  |Easy      |          |
|909 |Snakes and Ladders                                                              |Medium    |          |
|910 |Smallest Range II                                                               |Medium    |          |
|911 |[Online Election](problems/OnlineElection.py)                                   |Medium    |          |
|912 |[Sort an Array](problems/SortAnArray.py)                                        |Medium    |          |
|913 |Cat and Mouse                                                                   |Hard      |          |
|914 |[X of a Kind in a Deck of Cards](problems/XOfAKindInADeckOfCards.py)            |Easy      |          |
|915 |[Partition Array into Disjoint Intervals](problems/PartitionArrayIntoDisjointIntervals.py)|Medium    |          |
|916 |[Word Subsets](problems/WordSubsets.py)                                         |Medium    |          |
|917 |[Reverse Only Letters](problems/ReverseOnlyLetters.py)                          |Easy      |          |
|918 |[Maximum Sum Circular Subarray](problems/MaximumSumCircularSubarray.py)         |Medium    |          |
|919 |Complete Binary Tree Inserter                                                   |Medium    |          |
|920 |Number of Music Playlists                                                       |Hard      |          |
|921 |Minimum Add to Make Parentheses Valid                                           |Medium    |          |
|922 |[Sort Array By Parity II](problems/SortArrayByParityIi.py)                      |Easy      |          |
|923 |[3Sum With Multiplicity](problems/3sumWithMultiplicity.py)                      |Medium    |          |
|924 |Minimize Malware Spread                                                         |Hard      |          |
|925 |[Long Pressed Name](problems/LongPressedName.py)                                |Easy      |          |
|926 |[Flip String to Monotone Increasing](problems/FlipStringToMonotoneIncreasing.py)|Medium    |          |
|927 |[Three Equal Parts](problems/ThreeEqualParts.py)                                |Hard      |          |
|928 |Minimize Malware Spread II                                                      |Hard      |          |
|929 |[Unique Email Addresses](problems/UniqueEmailAddresses.py)                      |Easy      |          |
|930 |Binary Subarrays With Sum                                                       |Medium    |          |
|931 |[Minimum Falling Path Sum](problems/MinimumFallingPathSum.py)                   |Medium    |          |
|932 |[Beautiful Array](problems/BeautifulArray.py)                                   |Medium    |          |
|933 |Number of Recent Calls                                                          |Easy      |          |
|934 |[Shortest Bridge](problems/ShortestBridge.py)                                   |Medium    |          |
|935 |[Knight Dialer](problems/KnightDialer.py)                                       |Medium    |          |
|936 |[Stamping The Sequence](problems/StampingTheSequence.py)                        |Hard      |          |
|937 |Reorder Data in Log Files                                                       |Easy      |          |
|938 |Range Sum of BST                                                                |Easy      |          |
|939 |Minimum Area Rectangle                                                          |Medium    |          |
|940 |[Distinct Subsequences II](problems/DistinctSubsequencesIi.py)                  |Hard      |          |
|941 |[Valid Mountain Array](problems/ValidMountainArray.py)                          |Easy      |          |
|942 |DI String Match                                                                 |Easy      |          |
|943 |[Find the Shortest Superstring](problems/FindTheShortestSuperstring.py)         |Hard      |          |
|944 |Delete Columns to Make Sorted                                                   |Easy      |          |
|945 |[Minimum Increment to Make Array Unique](problems/MinimumIncrementToMakeArrayUnique.py)|Medium    |          |
|946 |[Validate Stack Sequences](problems/ValidateStackSequences.py)                  |Medium    |          |
|947 |Most Stones Removed with Same Row or Column                                     |Medium    |          |
|948 |Bag of Tokens                                                                   |Medium    |          |
|949 |[Largest Time for Given Digits](problems/LargestTimeForGivenDigits.py)          |Medium    |          |
|950 |[Reveal Cards In Increasing Order](problems/RevealCardsInIncreasingOrder.py)    |Medium    |          |
|951 |Flip Equivalent Binary Trees                                                    |Medium    |          |
|952 |[Largest Component Size by Common Factor](problems/LargestComponentSizeByCommonFactor.py)|Hard      |          |
|953 |[Verifying an Alien Dictionary](problems/VerifyingAnAlienDictionary.py)         |Easy      |          |
|954 |[Array of Doubled Pairs](problems/ArrayOfDoubledPairs.py)                       |Medium    |          |
|955 |Delete Columns to Make Sorted II                                                |Medium    |          |
|956 |[Tallest Billboard](problems/TallestBillboard.py)                               |Hard      |          |
|957 |Prison Cells After N Days                                                       |Medium    |          |
|958 |Check Completeness of a Binary Tree                                             |Medium    |          |
|959 |Regions Cut By Slashes                                                          |Medium    |          |
|960 |Delete Columns to Make Sorted III                                               |Hard      |          |
|961 |[N-Repeated Element in Size 2N Array](problems/NRepeatedElementInSize2nArray.py)|Easy      |          |
|962 |Maximum Width Ramp                                                              |Medium    |          |
|963 |Minimum Area Rectangle II                                                       |Medium    |          |
|964 |Least Operators to Express Number                                               |Hard      |          |
|965 |[Univalued Binary Tree](problems/UnivaluedBinaryTree.py)                        |Easy      |          |
|966 |[Vowel Spellchecker](problems/VowelSpellchecker.py)                             |Medium    |          |
|967 |Numbers With Same Consecutive Differences                                       |Medium    |          |
|968 |[Binary Tree Cameras](problems/BinaryTreeCameras.py)                            |Hard      |          |
|969 |Pancake Sorting                                                                 |Medium    |          |
|970 |[Powerful Integers](problems/PowerfulIntegers.py)                               |Medium    |          |
|971 |[Flip Binary Tree To Match Preorder Traversal](problems/FlipBinaryTreeToMatchPreorderTraversal.py)|Medium    |          |
|972 |Equal Rational Numbers                                                          |Hard      |          |
|973 |[K Closest Points to Origin](problems/KClosestPointsToOrigin.py)                |Medium    |          |
|974 |[Subarray Sums Divisible by K](problems/SubarraySumsDivisibleByK.py)            |Medium    |          |
|975 |[Odd Even Jump](problems/OddEvenJump.py)                                        |Hard      |          |
|976 |[Largest Perimeter Triangle](problems/LargestPerimeterTriangle.py)              |Easy      |          |
|977 |[Squares of a Sorted Array](problems/SquaresOfASortedArray.py)                  |Easy      |          |
|978 |Longest Turbulent Subarray                                                      |Medium    |          |
|979 |[Distribute Coins in Binary Tree](problems/DistributeCoinsInBinaryTree.py)      |Medium    |          |
|980 |[Unique Paths III](problems/UniquePathsIii.py)                                  |Hard      |          |
|981 |[Time Based Key-Value Store](problems/TimeBasedKeyValueStore.py)                |Medium    |          |
|982 |Triples with Bitwise AND Equal To Zero                                          |Hard      |          |
|983 |Minimum Cost For Tickets                                                        |Medium    |          |
|984 |String Without AAA or BBB                                                       |Medium    |          |
|985 |[Sum of Even Numbers After Queries](problems/SumOfEvenNumbersAfterQueries.py)   |Easy      |          |
|986 |[Interval List Intersections](problems/IntervalListIntersections.py)            |Medium    |          |
|987 |[Vertical Order Traversal of a Binary Tree](problems/VerticalOrderTraversalOfABinaryTree.py)|Hard      |          |
|988 |[Smallest String Starting From Leaf](problems/SmallestStringStartingFromLeaf.py)|Medium    |          |
|989 |[Add to Array-Form of Integer](problems/AddToArrayFormOfInteger.py)             |Easy      |          |
|990 |Satisfiability of Equality Equations                                            |Medium    |          |
|991 |[Broken Calculator](problems/BrokenCalculator.py)                               |Medium    |          |
|992 |Subarrays with K Different Integers                                             |Hard      |          |
|993 |[Cousins in Binary Tree](problems/CousinsInBinaryTree.py)                       |Easy      |          |
|994 |[Rotting Oranges](problems/RottingOranges.py)                                   |Medium    |          |
|995 |Minimum Number of K Consecutive Bit Flips                                       |Hard      |          |
|996 |[Number of Squareful Arrays](problems/NumberOfSquarefulArrays.py)               |Hard      |          |
|997 |[Find the Town Judge](problems/FindTheTownJudge.py)                             |Easy      |          |
|998 |[Maximum Binary Tree II](problems/MaximumBinaryTreeIi.py)                       |Medium    |          |
|999 |[Available Captures for Rook](problems/AvailableCapturesForRook.py)             |Easy      |          |
|1000|Minimum Cost to Merge Stones                                                    |Hard      |          |
|1001|Grid Illumination                                                               |Hard      |          |
|1002|[Find Common Characters](problems/FindCommonCharacters.py)                      |Easy      |          |
|1003|[Check If Word Is Valid After Substitutions](problems/CheckIfWordIsValidAfterSubstitutions.py)|Medium    |          |
|1004|[Max Consecutive Ones III](problems/MaxConsecutiveOnesIii.py)                   |Medium    |          |
|1005|[Maximize Sum Of Array After K Negations](problems/MaximizeSumOfArrayAfterKNegations.py)|Easy      |          |
|1006|[Clumsy Factorial](problems/ClumsyFactorial.py)                                 |Medium    |          |
|1007|[Minimum Domino Rotations For Equal Row](problems/MinimumDominoRotationsForEqualRow.py)|Medium    |          |
|1008|[Construct Binary Search Tree from Preorder Traversal](problems/ConstructBinarySearchTreeFromPreorderTraversal.py)|Medium    |          |
|1009|[Complement of Base 10 Integer](problems/ComplementOfBase10Integer.py)          |Easy      |          |
|1010|[Pairs of Songs With Total Durations Divisible by 60](problems/PairsOfSongsWithTotalDurationsDivisibleBy60.py)|Medium    |          |
|1011|[Capacity To Ship Packages Within D Days](problems/CapacityToShipPackagesWithinDDays.py)|Medium    |          |
|1012|[Numbers With Repeated Digits](problems/NumbersWithRepeatedDigits.py)           |Hard      |          |
|1013|[Partition Array Into Three Parts With Equal Sum](problems/PartitionArrayIntoThreePartsWithEqualSum.py)|Easy      |          |
|1014|Best Sightseeing Pair                                                           |Medium    |          |
|1015|Smallest Integer Divisible by K                                                 |Medium    |          |
|1016|Binary String With Substrings Representing 1 To N                               |Medium    |          |
|1017|[Convert to Base -2](problems/ConvertToBase2.py)                                |Medium    |          |
|1018|Binary Prefix Divisible By 5                                                    |Easy      |          |
|1019|[Next Greater Node In Linked List](problems/NextGreaterNodeInLinkedList.py)     |Medium    |          |
|1020|Number of Enclaves                                                              |Medium    |          |
|1021|[Remove Outermost Parentheses](problems/RemoveOutermostParentheses.py)          |Easy      |          |
|1022|Sum of Root To Leaf Binary Numbers                                              |Easy      |          |
|1023|[Camelcase Matching](problems/CamelcaseMatching.py)                             |Medium    |          |
|1024|Video Stitching                                                                 |Medium    |          |
|1025|Divisor Game                                                                    |Easy      |          |
|1026|[Maximum Difference Between Node and Ancestor](problems/MaximumDifferenceBetweenNodeAndAncestor.py)|Medium    |          |
|1027|Longest Arithmetic Subsequence                                                  |Medium    |          |
|1028|[Recover a Tree From Preorder Traversal](problems/RecoverATreeFromPreorderTraversal.py)|Hard      |          |
|1029|Two City Scheduling                                                             |Medium    |          |
|1030|Matrix Cells in Distance Order                                                  |Easy      |          |
|1031|Maximum Sum of Two Non-Overlapping Subarrays                                    |Medium    |          |
|1032|[Stream of Characters](problems/StreamOfCharacters.py)                          |Hard      |          |
|1033|Moving Stones Until Consecutive                                                 |Easy      |          |
|1034|Coloring A Border                                                               |Medium    |          |
|1035|Uncrossed Lines                                                                 |Medium    |          |
|1036|Escape a Large Maze                                                             |Hard      |          |
|1037|[Valid Boomerang](problems/ValidBoomerang.py)                                   |Easy      |          |
|1038|[Binary Search Tree to Greater Sum Tree](problems/BinarySearchTreeToGreaterSumTree.py)|Medium    |          |
|1039|Minimum Score Triangulation of Polygon                                          |Medium    |          |
|1040|Moving Stones Until Consecutive II                                              |Medium    |          |
|1041|[Robot Bounded In Circle](problems/RobotBoundedInCircle.py)                     |Medium    |          |
|1042|Flower Planting With No Adjacent                                                |Medium    |          |
|1043|Partition Array for Maximum Sum                                                 |Medium    |          |
|1044|[Longest Duplicate Substring](problems/LongestDuplicateSubstring.py)            |Hard      |          |
|1045|Customers Who Bought All Products                                               |Medium    |Premium   |
|1046|[Last Stone Weight](problems/LastStoneWeight.py)                                |Easy      |          |
|1047|[Remove All Adjacent Duplicates In String](problems/RemoveAllAdjacentDuplicatesInString.py)|Easy      |          |
|1048|[Longest String Chain](problems/LongestStringChain.py)                          |Medium    |          |
|1049|[Last Stone Weight II](problems/LastStoneWeightIi.py)                           |Medium    |          |
|1050|Actors and Directors Who Cooperated At Least Three Times                        |Easy      |Premium   |
|1051|[Height Checker](problems/HeightChecker.py)                                     |Easy      |          |
|1052|[Grumpy Bookstore Owner](problems/GrumpyBookstoreOwner.py)                      |Medium    |          |
|1053|[Previous Permutation With One Swap](problems/PreviousPermutationWithOneSwap.py)|Medium    |          |
|1054|[Distant Barcodes](problems/DistantBarcodes.py)                                 |Medium    |          |
|1055|Shortest Way to Form String                                                     |Medium    |Premium   |
|1056|Confusing Number                                                                |Easy      |Premium   |
|1057|Campus Bikes                                                                    |Medium    |Premium   |
|1058|Minimize Rounding Error to Meet Target                                          |Medium    |Premium   |
|1059|All Paths from Source Lead to Destination                                       |Medium    |Premium   |
|1060|Missing Element in Sorted Array                                                 |Medium    |Premium   |
|1061|Lexicographically Smallest Equivalent String                                    |Medium    |Premium   |
|1062|Longest Repeating Substring                                                     |Medium    |Premium   |
|1063|Number of Valid Subarrays                                                       |Hard      |Premium   |
|1064|Fixed Point                                                                     |Easy      |Premium   |
|1065|Index Pairs of a String                                                         |Easy      |Premium   |
|1066|Campus Bikes II                                                                 |Medium    |Premium   |
|1067|Digit Count in Range                                                            |Hard      |Premium   |
|1068|Product Sales Analysis I                                                        |Easy      |Premium   |
|1069|Product Sales Analysis II                                                       |Easy      |Premium   |
|1070|Product Sales Analysis III                                                      |Medium    |Premium   |
|1071|[Greatest Common Divisor of Strings](problems/GreatestCommonDivisorOfStrings.py)|Easy      |          |
|1072|Flip Columns For Maximum Number of Equal Rows                                   |Medium    |          |
|1073|Adding Two Negabinary Numbers                                                   |Medium    |          |
|1074|[Number of Submatrices That Sum to Target](problems/NumberOfSubmatricesThatSumToTarget.py)|Hard      |          |
|1075|Project Employees I                                                             |Easy      |Premium   |
|1076|Project Employees II                                                            |Easy      |Premium   |
|1077|Project Employees III                                                           |Medium    |Premium   |
|1078|Occurrences After Bigram                                                        |Easy      |          |
|1079|Letter Tile Possibilities                                                       |Medium    |          |
|1080|Insufficient Nodes in Root to Leaf Paths                                        |Medium    |          |
|1081|[Smallest Subsequence of Distinct Characters](problems/SmallestSubsequenceOfDistinctCharacters.py)|Medium    |          |
|1082|Sales Analysis I                                                                |Easy      |Premium   |
|1083|Sales Analysis II                                                               |Easy      |Premium   |
|1084|Sales Analysis III                                                              |Easy      |Premium   |
|1085|Sum of Digits in the Minimum Number                                             |Easy      |Premium   |
|1086|High Five                                                                       |Easy      |Premium   |
|1087|Brace Expansion                                                                 |Medium    |Premium   |
|1088|Confusing Number II                                                             |Hard      |Premium   |
|1089|[Duplicate Zeros](problems/DuplicateZeros.py)                                   |Easy      |          |
|1090|[Largest Values From Labels](problems/LargestValuesFromLabels.py)               |Medium    |          |
|1091|[Shortest Path in Binary Matrix](problems/ShortestPathInBinaryMatrix.py)        |Medium    |          |
|1092|Shortest Common Supersequence                                                   |Hard      |          |
|1093|[Statistics from a Large Sample](problems/StatisticsFromALargeSample.py)        |Medium    |          |
|1094|Car Pooling                                                                     |Medium    |          |
|1095|[Find in Mountain Array](problems/FindInMountainArray.py)                       |Hard      |          |
|1096|Brace Expansion II                                                              |Hard      |          |
|1097|Game Play Analysis V                                                            |Hard      |Premium   |
|1098|Unpopular Books                                                                 |Medium    |Premium   |
|1099|Two Sum Less Than K                                                             |Easy      |Premium   |
|1100|Find K-Length Substrings With No Repeated Characters                            |Medium    |Premium   |
|1101|The Earliest Moment When Everyone Become Friends                                |Medium    |Premium   |
|1102|Path With Maximum Minimum Value                                                 |Medium    |Premium   |
|1103|Distribute Candies to People                                                    |Easy      |          |
|1104|Path In Zigzag Labelled Binary Tree                                             |Medium    |          |
|1105|Filling Bookcase Shelves                                                        |Medium    |          |
|1106|Parsing A Boolean Expression                                                    |Hard      |          |
|1107|New Users Daily Count                                                           |Medium    |Premium   |
|1108|[Defanging an IP Address](problems/DefangingAnIpAddress.py)                     |Easy      |          |
|1109|Corporate Flight Bookings                                                       |Medium    |          |
|1110|[Delete Nodes And Return Forest](problems/DeleteNodesAndReturnForest.py)        |Medium    |          |
|1111|Maximum Nesting Depth of Two Valid Parentheses Strings                          |Medium    |          |
|1112|Highest Grade For Each Student                                                  |Medium    |Premium   |
|1113|Reported Posts                                                                  |Easy      |Premium   |
|1114|[Print in Order](problems/PrintInOrder.py)                                      |Easy      |          |
|1115|Print FooBar Alternately                                                        |Medium    |          |
|1116|[Print Zero Even Odd](problems/PrintZeroEvenOdd.py)                             |Medium    |          |
|1117|Building H2O                                                                    |Medium    |          |
|1118|Number of Days in a Month                                                       |Easy      |Premium   |
|1119|Remove Vowels from a String                                                     |Easy      |Premium   |
|1120|Maximum Average Subtree                                                         |Medium    |Premium   |
|1121|Divide Array Into Increasing Sequences                                          |Hard      |Premium   |
|1122|Relative Sort Array                                                             |Easy      |          |
|1123|[Lowest Common Ancestor of Deepest Leaves](problems/LowestCommonAncestorOfDeepestLeaves.py)|Medium    |          |
|1124|Longest Well-Performing Interval                                                |Medium    |          |
|1125|Smallest Sufficient Team                                                        |Hard      |          |
|1126|Active Businesses                                                               |Medium    |Premium   |
|1127|User Purchase Platform                                                          |Hard      |Premium   |
|1128|[Number of Equivalent Domino Pairs](problems/NumberOfEquivalentDominoPairs.py)  |Easy      |          |
|1129|[Shortest Path with Alternating Colors](problems/ShortestPathWithAlternatingColors.py)|Medium    |          |
|1130|[Minimum Cost Tree From Leaf Values](problems/MinimumCostTreeFromLeafValues.py) |Medium    |          |
|1131|Maximum of Absolute Value Expression                                            |Medium    |          |
|1132|Reported Posts II                                                               |Medium    |Premium   |
|1133|Largest Unique Number                                                           |Easy      |Premium   |
|1134|Armstrong Number                                                                |Easy      |Premium   |
|1135|Connecting Cities With Minimum Cost                                             |Medium    |Premium   |
|1136|Parallel Courses                                                                |Hard      |Premium   |
|1137|[N-th Tribonacci Number](problems/NThTribonacciNumber.py)                       |Easy      |          |
|1138|[Alphabet Board Path](problems/AlphabetBoardPath.py)                            |Medium    |          |
|1139|Largest 1-Bordered Square                                                       |Medium    |          |
|1140|[Stone Game II](problems/StoneGameIi.py)                                        |Medium    |          |
|1141|User Activity for the Past 30 Days I                                            |Easy      |Premium   |
|1142|User Activity for the Past 30 Days II                                           |Easy      |Premium   |
|1143|[Longest Common Subsequence](problems/LongestCommonSubsequence.py)              |Medium    |          |
|1144|Decrease Elements To Make Array Zigzag                                          |Medium    |          |
|1145|Binary Tree Coloring Game                                                       |Medium    |          |
|1146|Snapshot Array                                                                  |Medium    |          |
|1147|Longest Chunked Palindrome Decomposition                                        |Hard      |          |
|1148|Article Views I                                                                 |Easy      |Premium   |
|1149|Article Views II                                                                |Medium    |Premium   |
|1150|Check If a Number Is Majority Element in a Sorted Array                         |Easy      |Premium   |
|1151|Minimum Swaps to Group All 1's Together                                         |Medium    |Premium   |
|1152|Analyze User Website Visit Pattern                                              |Medium    |Premium   |
|1153|String Transforms Into Another String                                           |Hard      |Premium   |
|1154|Day of the Year                                                                 |Easy      |          |
|1155|Number of Dice Rolls With Target Sum                                            |Medium    |          |
|1156|Swap For Longest Repeated Character Substring                                   |Medium    |          |
|1157|Online Majority Element In Subarray                                             |Hard      |          |
|1158|Market Analysis I                                                               |Medium    |Premium   |
|1159|Market Analysis II                                                              |Hard      |Premium   |
|1160|[Find Words That Can Be Formed by Characters](problems/FindWordsThatCanBeFormedByCharacters.py)|Easy      |          |
|1161|[Maximum Level Sum of a Binary Tree](problems/MaximumLevelSumOfABinaryTree.py)  |Medium    |          |
|1162|[As Far from Land as Possible](problems/AsFarFromLandAsPossible.py)             |Medium    |          |
|1163|Last Substring in Lexicographical Order                                         |Hard      |          |
|1164|Product Price at a Given Date                                                   |Medium    |Premium   |
|1165|Single-Row Keyboard                                                             |Easy      |Premium   |
|1166|Design File System                                                              |Medium    |Premium   |
|1167|Minimum Cost to Connect Sticks                                                  |Medium    |Premium   |
|1168|Optimize Water Distribution in a Village                                        |Hard      |Premium   |
|1169|Invalid Transactions                                                            |Medium    |          |
|1170|[Compare Strings by Frequency of the Smallest Character](problems/CompareStringsByFrequencyOfTheSmallestCharacter.py)|Medium    |          |
|1171|[Remove Zero Sum Consecutive Nodes from Linked List](problems/RemoveZeroSumConsecutiveNodesFromLinkedList.py)|Medium    |          |
|1172|Dinner Plate Stacks                                                             |Hard      |          |
|1173|Immediate Food Delivery I                                                       |Easy      |Premium   |
|1174|Immediate Food Delivery II                                                      |Medium    |Premium   |
|1175|Prime Arrangements                                                              |Easy      |          |
|1176|Diet Plan Performance                                                           |Easy      |Premium   |
|1177|Can Make Palindrome from Substring                                              |Medium    |          |
|1178|[Number of Valid Words for Each Puzzle](problems/NumberOfValidWordsForEachPuzzle.py)|Hard      |          |
|1179|Reformat Department Table                                                       |Easy      |          |
|1180|Count Substrings with Only One Distinct Letter                                  |Easy      |Premium   |
|1181|Before and After Puzzle                                                         |Medium    |Premium   |
|1182|Shortest Distance to Target Color                                               |Medium    |Premium   |
|1183|Maximum Number of Ones                                                          |Hard      |Premium   |
|1184|[Distance Between Bus Stops](problems/DistanceBetweenBusStops.py)               |Easy      |          |
|1185|[Day of the Week](problems/DayOfTheWeek.py)                                     |Easy      |          |
|1186|Maximum Subarray Sum with One Deletion                                          |Medium    |          |
|1187|Make Array Strictly Increasing                                                  |Hard      |          |
|1188|Design Bounded Blocking Queue                                                   |Medium    |Premium   |
|1189|[Maximum Number of Balloons](problems/MaximumNumberOfBalloons.py)               |Easy      |          |
|1190|[Reverse Substrings Between Each Pair of Parentheses](problems/ReverseSubstringsBetweenEachPairOfParentheses.py)|Medium    |          |
|1191|K-Concatenation Maximum Sum                                                     |Medium    |          |
|1192|[Critical Connections in a Network](problems/CriticalConnectionsInANetwork.py)  |Hard      |          |
|1193|Monthly Transactions I                                                          |Medium    |Premium   |
|1194|Tournament Winners                                                              |Hard      |Premium   |
|1195|Fizz Buzz Multithreaded                                                         |Medium    |          |
|1196|How Many Apples Can You Put into the Basket                                     |Easy      |Premium   |
|1197|Minimum Knight Moves                                                            |Medium    |Premium   |
|1198|Find Smallest Common Element in All Rows                                        |Medium    |Premium   |
|1199|Minimum Time to Build Blocks                                                    |Hard      |Premium   |
|1200|[Minimum Absolute Difference](problems/MinimumAbsoluteDifference.py)            |Easy      |          |
|1201|Ugly Number III                                                                 |Medium    |          |
|1202|[Smallest String With Swaps](problems/SmallestStringWithSwaps.py)               |Medium    |          |
|1203|Sort Items by Groups Respecting Dependencies                                    |Hard      |          |
|1204|Last Person to Fit in the Elevator                                              |Medium    |Premium   |
|1205|Monthly Transactions II                                                         |Medium    |Premium   |
|1206|Design Skiplist                                                                 |Hard      |          |
|1207|[Unique Number of Occurrences](problems/UniqueNumberOfOccurrences.py)           |Easy      |          |
|1208|Get Equal Substrings Within Budget                                              |Medium    |          |
|1209|[Remove All Adjacent Duplicates in String II](problems/RemoveAllAdjacentDuplicatesInStringIi.py)|Medium    |          |
|1210|Minimum Moves to Reach Target with Rotations                                    |Hard      |          |
|1211|Queries Quality and Percentage                                                  |Easy      |Premium   |
|1212|Team Scores in Football Tournament                                              |Medium    |Premium   |
|1213|Intersection of Three Sorted Arrays                                             |Easy      |Premium   |
|1214|Two Sum BSTs                                                                    |Medium    |Premium   |
|1215|Stepping Numbers                                                                |Medium    |Premium   |
|1216|Valid Palindrome III                                                            |Hard      |Premium   |
|1217|[Minimum Cost to Move Chips to The Same Position](problems/MinimumCostToMoveChipsToTheSamePosition.py)|Easy      |          |
|1218|Longest Arithmetic Subsequence of Given Difference                              |Medium    |          |
|1219|Path with Maximum Gold                                                          |Medium    |          |
|1220|[Count Vowels Permutation](problems/CountVowelsPermutation.py)                  |Hard      |          |
|1221|[Split a String in Balanced Strings](problems/SplitAStringInBalancedStrings.py) |Easy      |          |
|1222|Queens That Can Attack the King                                                 |Medium    |          |
|1223|Dice Roll Simulation                                                            |Hard      |          |
|1224|Maximum Equal Frequency                                                         |Hard      |          |
|1225|Report Contiguous Dates                                                         |Hard      |Premium   |
|1226|The Dining Philosophers                                                         |Medium    |          |
|1227|Airplane Seat Assignment Probability                                            |Medium    |          |
|1228|Missing Number In Arithmetic Progression                                        |Easy      |Premium   |
|1229|Meeting Scheduler                                                               |Medium    |Premium   |
|1230|Toss Strange Coins                                                              |Medium    |Premium   |
|1231|Divide Chocolate                                                                |Hard      |Premium   |
|1232|Check If It Is a Straight Line                                                  |Easy      |          |
|1233|Remove Sub-Folders from the Filesystem                                          |Medium    |          |
|1234|Replace the Substring for Balanced String                                       |Medium    |          |
|1235|[Maximum Profit in Job Scheduling](problems/MaximumProfitInJobScheduling.py)    |Hard      |          |
|1236|Web Crawler                                                                     |Medium    |Premium   |
|1237|Find Positive Integer Solution for a Given Equation                             |Medium    |          |
|1238|[Circular Permutation in Binary Representation](problems/CircularPermutationInBinaryRepresentation.py)|Medium    |          |
|1239|[Maximum Length of a Concatenated String with Unique Characters](problems/MaximumLengthOfAConcatenatedStringWithUniqueCharacters.py)|Medium    |          |
|1240|Tiling a Rectangle with the Fewest Squares                                      |Hard      |          |
|1241|Number of Comments per Post                                                     |Easy      |Premium   |
|1242|Web Crawler Multithreaded                                                       |Medium    |Premium   |
|1243|Array Transformation                                                            |Easy      |Premium   |
|1244|Design A Leaderboard                                                            |Medium    |Premium   |
|1245|Tree Diameter                                                                   |Medium    |Premium   |
|1246|Palindrome Removal                                                              |Hard      |Premium   |
|1247|Minimum Swaps to Make Strings Equal                                             |Medium    |          |
|1248|Count Number of Nice Subarrays                                                  |Medium    |          |
|1249|[Minimum Remove to Make Valid Parentheses](problems/MinimumRemoveToMakeValidParentheses.py)|Medium    |          |
|1250|Check If It Is a Good Array                                                     |Hard      |          |
|1251|Average Selling Price                                                           |Easy      |Premium   |
|1252|Cells with Odd Values in a Matrix                                               |Easy      |          |
|1253|Reconstruct a 2-Row Binary Matrix                                               |Medium    |          |
|1254|[Number of Closed Islands](problems/NumberOfClosedIslands.py)                   |Medium    |          |
|1255|Maximum Score Words Formed by Letters                                           |Hard      |          |
|1256|Encode Number                                                                   |Medium    |Premium   |
|1257|Smallest Common Region                                                          |Medium    |Premium   |
|1258|Synonymous Sentences                                                            |Medium    |Premium   |
|1259|Handshakes That Don't Cross                                                     |Hard      |Premium   |
|1260|Shift 2D Grid                                                                   |Easy      |          |
|1261|[Find Elements in a Contaminated Binary Tree](problems/FindElementsInAContaminatedBinaryTree.py)|Medium    |          |
|1262|Greatest Sum Divisible by Three                                                 |Medium    |          |
|1263|Minimum Moves to Move a Box to Their Target Location                            |Hard      |          |
|1264|Page Recommendations                                                            |Medium    |Premium   |
|1265|Print Immutable Linked List in Reverse                                          |Medium    |Premium   |
|1266|Minimum Time Visiting All Points                                                |Easy      |          |
|1267|Count Servers that Communicate                                                  |Medium    |          |
|1268|[Search Suggestions System](problems/SearchSuggestionsSystem.py)                |Medium    |          |
|1269|Number of Ways to Stay in the Same Place After Some Steps                       |Hard      |          |
|1270|All People Report to the Given Manager                                          |Medium    |Premium   |
|1271|Hexspeak                                                                        |Easy      |Premium   |
|1272|Remove Interval                                                                 |Medium    |Premium   |
|1273|Delete Tree Nodes                                                               |Medium    |Premium   |
|1274|Number of Ships in a Rectangle                                                  |Hard      |Premium   |
|1275|[Find Winner on a Tic Tac Toe Game](problems/FindWinnerOnATicTacToeGame.py)     |Easy      |          |
|1276|Number of Burgers with No Waste of Ingredients                                  |Medium    |          |
|1277|Count Square Submatrices with All Ones                                          |Medium    |          |
|1278|Palindrome Partitioning III                                                     |Hard      |          |
|1279|Traffic Light Controlled Intersection                                           |Easy      |Premium   |
|1280|Students and Examinations                                                       |Easy      |Premium   |
|1281|[Subtract the Product and Sum of Digits of an Integer](problems/SubtractTheProductAndSumOfDigitsOfAnInteger.py)|Easy      |          |
|1282|[Group the People Given the Group Size They Belong To](problems/GroupThePeopleGivenTheGroupSizeTheyBelongTo.py)|Medium    |          |
|1283|[Find the Smallest Divisor Given a Threshold](problems/FindTheSmallestDivisorGivenAThreshold.py)|Medium    |          |
|1284|Minimum Number of Flips to Convert Binary Matrix to Zero Matrix                 |Hard      |          |
|1285|Find the Start and End Number of Continuous Ranges                              |Medium    |Premium   |
|1286|[Iterator for Combination](problems/IteratorForCombination.py)                  |Medium    |          |
|1287|Element Appearing More Than 25% In Sorted Array                                 |Easy      |          |
|1288|Remove Covered Intervals                                                        |Medium    |          |
|1289|Minimum Falling Path Sum II                                                     |Hard      |          |
|1290|[Convert Binary Number in a Linked List to Integer](problems/ConvertBinaryNumberInALinkedListToInteger.py)|Easy      |          |
|1291|[Sequential Digits](problems/SequentialDigits.py)                               |Medium    |          |
|1292|Maximum Side Length of a Square with Sum Less than or Equal to Threshold        |Medium    |          |
|1293|[Shortest Path in a Grid with Obstacles Elimination](problems/ShortestPathInAGridWithObstaclesElimination.py)|Hard      |          |
|1294|Weather Type in Each Country                                                    |Easy      |Premium   |
|1295|[Find Numbers with Even Number of Digits](problems/FindNumbersWithEvenNumberOfDigits.py)|Easy      |          |
|1296|Divide Array in Sets of K Consecutive Numbers                                   |Medium    |          |
|1297|Maximum Number of Occurrences of a Substring                                    |Medium    |          |
|1298|[Maximum Candies You Can Get from Boxes](problems/MaximumCandiesYouCanGetFromBoxes.py)|Hard      |          |
|1299|[Replace Elements with Greatest Element on Right Side](problems/ReplaceElementsWithGreatestElementOnRightSide.py)|Easy      |          |
|1300|Sum of Mutated Array Closest to Target                                          |Medium    |          |
|1301|Number of Paths with Max Score                                                  |Hard      |          |
|1302|[Deepest Leaves Sum](problems/DeepestLeavesSum.py)                              |Medium    |          |
|1303|Find the Team Size                                                              |Easy      |Premium   |
|1304|Find N Unique Integers Sum up to Zero                                           |Easy      |          |
|1305|[All Elements in Two Binary Search Trees](problems/AllElementsInTwoBinarySearchTrees.py)|Medium    |          |
|1306|[Jump Game III](problems/JumpGameIii.py)                                        |Medium    |          |
|1307|Verbal Arithmetic Puzzle                                                        |Hard      |          |
|1308|Running Total for Different Genders                                             |Medium    |Premium   |
|1309|[Decrypt String from Alphabet to Integer Mapping](problems/DecryptStringFromAlphabetToIntegerMapping.py)|Easy      |          |
|1310|XOR Queries of a Subarray                                                       |Medium    |          |
|1311|Get Watched Videos by Your Friends                                              |Medium    |          |
|1312|Minimum Insertion Steps to Make a String Palindrome                             |Hard      |          |
|1313|Decompress Run-Length Encoded List                                              |Easy      |          |
|1314|Matrix Block Sum                                                                |Medium    |          |
|1315|Sum of Nodes with Even-Valued Grandparent                                       |Medium    |          |
|1316|Distinct Echo Substrings                                                        |Hard      |          |
|1317|[Convert Integer to the Sum of Two No-Zero Integers](problems/ConvertIntegerToTheSumOfTwoNoZeroIntegers.py)|Easy      |          |
|1318|Minimum Flips to Make a OR b Equal to c                                         |Medium    |          |
|1319|[Number of Operations to Make Network Connected](problems/NumberOfOperationsToMakeNetworkConnected.py)|Medium    |          |
|1320|Minimum Distance to Type a Word Using Two Fingers                               |Hard      |          |
|1321|Restaurant Growth                                                               |Medium    |Premium   |
|1322|Ads Performance                                                                 |Easy      |Premium   |
|1323|[Maximum 69 Number](problems/Maximum69Number.py)                                |Easy      |          |
|1324|Print Words Vertically                                                          |Medium    |          |
|1325|[Delete Leaves With a Given Value](problems/DeleteLeavesWithAGivenValue.py)     |Medium    |          |
|1326|Minimum Number of Taps to Open to Water a Garden                                |Hard      |          |
|1327|List the Products Ordered in a Period                                           |Easy      |Premium   |
|1328|[Break a Palindrome](problems/BreakAPalindrome.py)                              |Medium    |          |
|1329|[Sort the Matrix Diagonally](problems/SortTheMatrixDiagonally.py)               |Medium    |          |
|1330|Reverse Subarray To Maximize Array Value                                        |Hard      |          |
|1331|Rank Transform of an Array                                                      |Easy      |          |
|1332|[Remove Palindromic Subsequences](problems/RemovePalindromicSubsequences.py)    |Easy      |          |
|1333|Filter Restaurants by Vegan-Friendly, Price and Distance                        |Medium    |          |
|1334|[Find the City With the Smallest Number of Neighbors at a Threshold Distance](problems/FindTheCityWithTheSmallestNumberOfNeighborsAtAThresholdDistance.py)|Medium    |          |
|1335|Minimum Difficulty of a Job Schedule                                            |Hard      |          |
|1336|Number of Transactions per Visit                                                |Hard      |Premium   |
|1337|[The K Weakest Rows in a Matrix](problems/TheKWeakestRowsInAMatrix.py)          |Easy      |          |
|1338|[Reduce Array Size to The Half](problems/ReduceArraySizeToTheHalf.py)           |Medium    |          |
|1339|[Maximum Product of Splitted Binary Tree](problems/MaximumProductOfSplittedBinaryTree.py)|Medium    |          |
|1340|[Jump Game V](problems/JumpGameV.py)                                            |Hard      |          |
|1341|Movie Rating                                                                    |Medium    |Premium   |
|1342|[Number of Steps to Reduce a Number to Zero](problems/NumberOfStepsToReduceANumberToZero.py)|Easy      |          |
|1343|Number of Sub-arrays of Size K and Average Greater than or Equal to Threshold   |Medium    |          |
|1344|[Angle Between Hands of a Clock](problems/AngleBetweenHandsOfAClock.py)         |Medium    |          |
|1345|[Jump Game IV](problems/JumpGameIv.py)                                          |Hard      |          |
|1346|[Check If N and Its Double Exist](problems/CheckIfNAndItsDoubleExist.py)        |Easy      |          |
|1347|[Minimum Number of Steps to Make Two Strings Anagram](problems/MinimumNumberOfStepsToMakeTwoStringsAnagram.py)|Medium    |          |
|1348|Tweet Counts Per Frequency                                                      |Medium    |          |
|1349|Maximum Students Taking Exam                                                    |Hard      |          |
|1350|Students With Invalid Departments                                               |Easy      |Premium   |
|1351|[Count Negative Numbers in a Sorted Matrix](problems/CountNegativeNumbersInASortedMatrix.py)|Easy      |          |
|1352|[Product of the Last K Numbers](problems/ProductOfTheLastKNumbers.py)           |Medium    |          |
|1353|Maximum Number of Events That Can Be Attended                                   |Medium    |          |
|1354|[Construct Target Array With Multiple Sums](problems/ConstructTargetArrayWithMultipleSums.py)|Hard      |          |
|1355|Activity Participants                                                           |Medium    |Premium   |
|1356|[Sort Integers by The Number of 1 Bits](problems/SortIntegersByTheNumberOf1Bits.py)|Easy      |          |
|1357|Apply Discount Every n Orders                                                   |Medium    |          |
|1358|Number of Substrings Containing All Three Characters                            |Medium    |          |
|1359|Count All Valid Pickup and Delivery Options                                     |Hard      |          |
|1360|Number of Days Between Two Dates                                                |Easy      |          |
|1361|Validate Binary Tree Nodes                                                      |Medium    |          |
|1362|Closest Divisors                                                                |Medium    |          |
|1363|Largest Multiple of Three                                                       |Hard      |          |
|1364|Number of Trusted Contacts of a Customer                                        |Medium    |Premium   |
|1365|How Many Numbers Are Smaller Than the Current Number                            |Easy      |          |
|1366|[Rank Teams by Votes](problems/RankTeamsByVotes.py)                             |Medium    |          |
|1367|[Linked List in Binary Tree](problems/LinkedListInBinaryTree.py)                |Medium    |          |
|1368|[Minimum Cost to Make at Least One Valid Path in a Grid](problems/MinimumCostToMakeAtLeastOneValidPathInAGrid.py)|Hard      |          |
|1369|Get the Second Most Recent Activity                                             |Hard      |Premium   |
|1370|Increasing Decreasing String                                                    |Easy      |          |
|1371|Find the Longest Substring Containing Vowels in Even Counts                     |Medium    |          |
|1372|Longest ZigZag Path in a Binary Tree                                            |Medium    |          |
|1373|Maximum Sum BST in Binary Tree                                                  |Hard      |          |
|1374|Generate a String With Characters That Have Odd Counts                          |Easy      |          |
|1375|Bulb Switcher III                                                               |Medium    |          |
|1376|Time Needed to Inform All Employees                                             |Medium    |          |
|1377|Frog Position After T Seconds                                                   |Hard      |          |
|1378|Replace Employee ID With The Unique Identifier                                  |Easy      |Premium   |
|1379|[Find a Corresponding Node of a Binary Tree in a Clone of That Tree](problems/FindACorrespondingNodeOfABinaryTreeInACloneOfThatTree.py)|Medium    |          |
|1380|Lucky Numbers in a Matrix                                                       |Easy      |          |
|1381|Design a Stack With Increment Operation                                         |Medium    |          |
|1382|[Balance a Binary Search Tree](problems/BalanceABinarySearchTree.py)            |Medium    |          |
|1383|[Maximum Performance of a Team](problems/MaximumPerformanceOfATeam.py)          |Hard      |          |
|1384|Total Sales Amount by Year                                                      |Hard      |Premium   |
|1385|Find the Distance Value Between Two Arrays                                      |Easy      |          |
|1386|Cinema Seat Allocation                                                          |Medium    |          |
|1387|Sort Integers by The Power Value                                                |Medium    |          |
|1388|Pizza With 3n Slices                                                            |Hard      |          |
|1389|[Create Target Array in the Given Order](problems/CreateTargetArrayInTheGivenOrder.py)|Easy      |          |
|1390|[Four Divisors](problems/FourDivisors.py)                                       |Medium    |          |
|1391|[Check if There is a Valid Path in a Grid](problems/CheckIfThereIsAValidPathInAGrid.py)|Medium    |          |
|1392|Longest Happy Prefix                                                            |Hard      |          |
|1393|Capital Gain/Loss                                                               |Medium    |Premium   |
|1394|[Find Lucky Integer in an Array](problems/FindLuckyIntegerInAnArray.py)         |Easy      |          |
|1395|Count Number of Teams                                                           |Medium    |          |
|1396|[Design Underground System](problems/DesignUndergroundSystem.py)                |Medium    |          |
|1397|Find All Good Strings                                                           |Hard      |          |
|1398|Customers Who Bought Products A and B but Not C                                 |Medium    |Premium   |
|1399|[Count Largest Group](problems/CountLargestGroup.py)                            |Easy      |          |
|1400|Construct K Palindrome Strings                                                  |Medium    |          |
|1401|Circle and Rectangle Overlapping                                                |Medium    |          |
|1402|Reducing Dishes                                                                 |Hard      |          |
|1403|[Minimum Subsequence in Non-Increasing Order](problems/MinimumSubsequenceInNonIncreasingOrder.py)|Easy      |          |
|1404|[Number of Steps to Reduce a Number in Binary Representation to One](problems/NumberOfStepsToReduceANumberInBinaryRepresentationToOne.py)|Medium    |          |
|1405|Longest Happy String                                                            |Medium    |          |
|1406|[Stone Game III](problems/StoneGameIii.py)                                      |Hard      |          |
|1407|Top Travellers                                                                  |Easy      |Premium   |
|1408|[String Matching in an Array](problems/StringMatchingInAnArray.py)              |Easy      |          |
|1409|[Queries on a Permutation With Key](problems/QueriesOnAPermutationWithKey.py)   |Medium    |          |
|1410|HTML Entity Parser                                                              |Medium    |          |
|1411|Number of Ways to Paint N × 3 Grid                                              |Hard      |          |
|1412|Find the Quiet Students in All Exams                                            |Hard      |Premium   |
|1413|[Minimum Value to Get Positive Step by Step Sum](problems/MinimumValueToGetPositiveStepByStepSum.py)|Easy      |          |
|1414|Find the Minimum Number of Fibonacci Numbers Whose Sum Is K                     |Medium    |          |
|1415|The k-th Lexicographical String of All Happy Strings of Length n                |Medium    |          |
|1416|Restore The Array                                                               |Hard      |          |
|1417|[Reformat The String](problems/ReformatTheString.py)                            |Easy      |          |
|1418|[Display Table of Food Orders in a Restaurant](problems/DisplayTableOfFoodOrdersInARestaurant.py)|Medium    |          |
|1419|[Minimum Number of Frogs Croaking](problems/MinimumNumberOfFrogsCroaking.py)    |Medium    |          |
|1420|Build Array Where You Can Find The Maximum Exactly K Comparisons                |Hard      |          |
|1421|NPV Queries                                                                     |Medium    |Premium   |
|1422|[Maximum Score After Splitting a String](problems/MaximumScoreAfterSplittingAString.py)|Easy      |          |
|1423|[Maximum Points You Can Obtain from Cards](problems/MaximumPointsYouCanObtainFromCards.py)|Medium    |          |
|1424|[Diagonal Traverse II](problems/DiagonalTraverseIi.py)                          |Medium    |          |
|1425|Constrained Subsequence Sum                                                     |Hard      |          |
|1426|Counting Elements                                                               |Easy      |Premium   |
|1427|[Perform String Shifts](problems/PerformStringShifts.py)                        |Easy      |Premium   |
|1428|[Leftmost Column with at Least a One](problems/LeftmostColumnWithAtLeastAOne.py)|Medium    |Premium   |
|1429|[First Unique Number](problems/FirstUniqueNumber.py)                            |Medium    |Premium   |
|1430|Check If a String Is a Valid Sequence from Root to Leaves Path in a Binary Tree |Medium    |Premium   |
|1431|Kids With the Greatest Number of Candies                                        |Easy      |          |
|1432|[Max Difference You Can Get From Changing an Integer](problems/MaxDifferenceYouCanGetFromChangingAnInteger.py)|Medium    |          |
|1433|[Check If a String Can Break Another String](problems/CheckIfAStringCanBreakAnotherString.py)|Medium    |          |
|1434|[Number of Ways to Wear Different Hats to Each Other](problems/NumberOfWaysToWearDifferentHatsToEachOther.py)|Hard      |          |
|1435|Create a Session Bar Chart                                                      |Easy      |Premium   |
|1436|Destination City                                                                |Easy      |          |
|1437|Check If All 1's Are at Least Length K Places Away                              |Easy      |          |
|1438|Longest Continuous Subarray With Absolute Diff Less Than or Equal to Limit      |Medium    |          |
|1439|Find the Kth Smallest Sum of a Matrix With Sorted Rows                          |Hard      |          |
|1440|Evaluate Boolean Expression                                                     |Medium    |Premium   |
|1441|[Build an Array With Stack Operations](problems/BuildAnArrayWithStackOperations.py)|Easy      |          |
|1442|[Count Triplets That Can Form Two Arrays of Equal XOR](problems/CountTripletsThatCanFormTwoArraysOfEqualXor.py)|Medium    |          |
|1443|Minimum Time to Collect All Apples in a Tree                                    |Medium    |          |
|1444|Number of Ways of Cutting a Pizza                                               |Hard      |          |
|1445|Apples & Oranges                                                                |Medium    |Premium   |
|1446|[Consecutive Characters](problems/ConsecutiveCharacters.py)                     |Easy      |          |
|1447|Simplified Fractions                                                            |Medium    |          |
|1448|[Count Good Nodes in Binary Tree](problems/CountGoodNodesInBinaryTree.py)       |Medium    |          |
|1449|Form Largest Integer With Digits That Add up to Target                          |Hard      |          |
|1450|Number of Students Doing Homework at a Given Time                               |Easy      |          |
|1451|[Rearrange Words in a Sentence](problems/RearrangeWordsInASentence.py)          |Medium    |          |
|1452|[People Whose List of Favorite Companies Is Not a Subset of Another List](problems/PeopleWhoseListOfFavoriteCompaniesIsNotASubsetOfAnotherList.py)|Medium    |          |
|1453|Maximum Number of Darts Inside of a Circular Dartboard                          |Hard      |          |
|1454|Active Users                                                                    |Medium    |Premium   |
|1455|Check If a Word Occurs As a Prefix of Any Word in a Sentence                    |Easy      |          |
|1456|[Maximum Number of Vowels in a Substring of Given Length](problems/MaximumNumberOfVowelsInASubstringOfGivenLength.py)|Medium    |          |
|1457|Pseudo-Palindromic Paths in a Binary Tree                                       |Medium    |          |
|1458|Max Dot Product of Two Subsequences                                             |Hard      |          |
|1459|Rectangles Area                                                                 |Medium    |Premium   |
|1460|Make Two Arrays Equal by Reversing Sub-arrays                                   |Easy      |          |
|1461|[Check If a String Contains All Binary Codes of Size K](problems/CheckIfAStringContainsAllBinaryCodesOfSizeK.py)|Medium    |          |
|1462|Course Schedule IV                                                              |Medium    |          |
|1463|Cherry Pickup II                                                                |Hard      |          |
|1464|[Maximum Product of Two Elements in an Array](problems/MaximumProductOfTwoElementsInAnArray.py)|Easy      |          |
|1465|[Maximum Area of a Piece of Cake After Horizontal and Vertical Cuts](problems/MaximumAreaOfAPieceOfCakeAfterHorizontalAndVerticalCuts.py)|Medium    |          |
|1466|Reorder Routes to Make All Paths Lead to the City Zero                          |Medium    |          |
|1467|Probability of a Two Boxes Having The Same Number of Distinct Balls             |Hard      |          |
|1468|Calculate Salaries                                                              |Medium    |Premium   |
|1469|Find All The Lonely Nodes                                                       |Easy      |Premium   |
|1470|[Shuffle the Array](problems/ShuffleTheArray.py)                                |Easy      |          |
|1471|[The k Strongest Values in an Array](problems/TheKStrongestValuesInAnArray.py)  |Medium    |          |
|1472|Design Browser History                                                          |Medium    |          |
|1473|Paint House III                                                                 |Hard      |          |
|1474|Delete N Nodes After M Nodes of a Linked List                                   |Easy      |Premium   |
|1475|Final Prices With a Special Discount in a Shop                                  |Easy      |          |
|1476|Subrectangle Queries                                                            |Medium    |          |
|1477|[Find Two Non-overlapping Sub-arrays Each With Target Sum](problems/FindTwoNonOverlappingSubArraysEachWithTargetSum.py)|Medium    |          |
|1478|Allocate Mailboxes                                                              |Hard      |          |
|1479|Sales by Day of the Week                                                        |Hard      |Premium   |
|1480|[Running Sum of 1d Array](problems/RunningSumOf1dArray.py)                      |Easy      |          |
|1481|[Least Number of Unique Integers after K Removals](problems/LeastNumberOfUniqueIntegersAfterKRemovals.py)|Medium    |          |
|1482|Minimum Number of Days to Make m Bouquets                                       |Medium    |          |
|1483|Kth Ancestor of a Tree Node                                                     |Hard      |          |
|1484|Group Sold Products By The Date                                                 |Easy      |Premium   |
|1485|Clone Binary Tree With Random Pointer                                           |Medium    |Premium   |
|1486|[XOR Operation in an Array](problems/XorOperationInAnArray.py)                  |Easy      |          |
|1487|[Making File Names Unique](problems/MakingFileNamesUnique.py)                   |Medium    |          |
|1488|Avoid Flood in The City                                                         |Medium    |          |
|1489|Find Critical and Pseudo-Critical Edges in Minimum Spanning Tree                |Hard      |          |
|1490|Clone N-ary Tree                                                                |Medium    |Premium   |
|1491|Average Salary Excluding the Minimum and Maximum Salary                         |Easy      |          |
|1492|The kth Factor of n                                                             |Medium    |          |
|1493|[Longest Subarray of 1's After Deleting One Element](problems/LongestSubarrayOf1sAfterDeletingOneElement.py)|Medium    |          |
|1494|Parallel Courses II                                                             |Hard      |          |
|1495|Friendly Movies Streamed Last Month                                             |Easy      |Premium   |
|1496|[Path Crossing](problems/PathCrossing.py)                                       |Easy      |          |
|1497|Check If Array Pairs Are Divisible by k                                         |Medium    |          |
|1498|Number of Subsequences That Satisfy the Given Sum Condition                     |Medium    |          |
|1499|[Max Value of Equation](problems/MaxValueOfEquation.py)                         |Hard      |          |
|1500|Design a File Sharing System                                                    |Medium    |Premium   |
|1501|Countries You Can Safely Invest In                                              |Medium    |Premium   |
|1502|Can Make Arithmetic Progression From Sequence                                   |Easy      |          |
|1503|Last Moment Before All Ants Fall Out of a Plank                                 |Medium    |          |
|1504|[Count Submatrices With All Ones](problems/CountSubmatricesWithAllOnes.py)      |Medium    |          |
|1505|[Minimum Possible Integer After at Most K Adjacent Swaps On Digits](problems/MinimumPossibleIntegerAfterAtMostKAdjacentSwapsOnDigits.py)|Hard      |          |
|1506|Find Root of N-Ary Tree                                                         |Medium    |Premium   |
|1507|Reformat Date                                                                   |Easy      |          |
|1508|Range Sum of Sorted Subarray Sums                                               |Medium    |          |
|1509|[Minimum Difference Between Largest and Smallest Value in Three Moves](problems/MinimumDifferenceBetweenLargestAndSmallestValueInThreeMoves.py)|Medium    |          |
|1510|[Stone Game IV](problems/StoneGameIv.py)                                        |Hard      |          |
|1511|Customer Order Frequency                                                        |Easy      |Premium   |
|1512|[Number of Good Pairs](problems/NumberOfGoodPairs.py)                           |Easy      |          |
|1513|[Number of Substrings With Only 1s](problems/NumberOfSubstringsWithOnly1s.py)   |Medium    |          |
|1514|Path with Maximum Probability                                                   |Medium    |          |
|1515|Best Position for a Service Centre                                              |Hard      |          |
|1516|Move Sub-Tree of N-Ary Tree                                                     |Hard      |Premium   |
|1517|Find Users With Valid E-Mails                                                   |Easy      |Premium   |
|1518|[Water Bottles](problems/WaterBottles.py)                                       |Easy      |          |
|1519|Number of Nodes in the Sub-Tree With the Same Label                             |Medium    |          |
|1520|Maximum Number of Non-Overlapping Substrings                                    |Hard      |          |
|1521|[Find a Value of a Mysterious Function Closest to Target](problems/FindAValueOfAMysteriousFunctionClosestToTarget.py)|Hard      |          |
|1522|Diameter of N-Ary Tree                                                          |Medium    |Premium   |
|1523|[Count Odd Numbers in an Interval Range](problems/CountOddNumbersInAnIntervalRange.py)|Easy      |          |
|1524|[Number of Sub-arrays With Odd Sum](problems/NumberOfSubArraysWithOddSum.py)    |Medium    |          |
|1525|[Number of Good Ways to Split a String](problems/NumberOfGoodWaysToSplitAString.py)|Medium    |          |
|1526|Minimum Number of Increments on Subarrays to Form a Target Array                |Hard      |          |
|1527|Patients With a Condition                                                       |Easy      |Premium   |
|1528|[Shuffle String](problems/ShuffleString.py)                                     |Easy      |          |
|1529|Bulb Switcher IV                                                                |Medium    |          |
|1530|Number of Good Leaf Nodes Pairs                                                 |Medium    |          |
|1531|String Compression II                                                           |Hard      |          |
|1532|The Most Recent Three Orders                                                    |Medium    |Premium   |
|1533|Find the Index of the Large Integer                                             |Medium    |Premium   |
|1534|[Count Good Triplets](problems/CountGoodTriplets.py)                            |Easy      |          |
|1535|[Find the Winner of an Array Game](problems/FindTheWinnerOfAnArrayGame.py)      |Medium    |          |
|1536|[Minimum Swaps to Arrange a Binary Grid](problems/MinimumSwapsToArrangeABinaryGrid.py)|Medium    |          |
|1537|Get the Maximum Score                                                           |Hard      |          |
|1538|Guess the Majority in a Hidden Array                                            |Medium    |Premium   |
|1539|[Kth Missing Positive Number](problems/KthMissingPositiveNumber.py)             |Easy      |          |
|1540|Can Convert String in K Moves                                                   |Medium    |          |
|1541|[Minimum Insertions to Balance a Parentheses String](problems/MinimumInsertionsToBalanceAParenthesesString.py)|Medium    |          |
|1542|Find Longest Awesome Substring                                                  |Hard      |          |
|1543|Fix Product Name Format                                                         |Easy      |Premium   |
|1544|Make The String Great                                                           |Easy      |          |
|1545|Find Kth Bit in Nth Binary String                                               |Medium    |          |
|1546|Maximum Number of Non-Overlapping Subarrays With Sum Equals Target              |Medium    |          |
|1547|Minimum Cost to Cut a Stick                                                     |Hard      |          |
|1548|The Most Similar Path in a Graph                                                |Hard      |Premium   |
|1549|The Most Recent Orders for Each Product                                         |Medium    |Premium   |
|1550|Three Consecutive Odds                                                          |Easy      |          |
|1551|[Minimum Operations to Make Array Equal](problems/MinimumOperationsToMakeArrayEqual.py)|Medium    |          |
|1552|Magnetic Force Between Two Balls                                                |Medium    |          |
|1553|Minimum Number of Days to Eat N Oranges                                         |Hard      |          |
|1554|Strings Differ by One Character                                                 |Medium    |Premium   |
|1555|Bank Account Summary                                                            |Medium    |Premium   |
|1556|[Thousand Separator](problems/ThousandSeparator.py)                             |Easy      |          |
|1557|[Minimum Number of Vertices to Reach All Nodes](problems/MinimumNumberOfVerticesToReachAllNodes.py)|Medium    |          |
|1558|Minimum Numbers of Function Calls to Make Target Array                          |Medium    |          |
|1559|Detect Cycles in 2D Grid                                                        |Hard      |          |
|1560|[Most Visited Sector in a Circular Track](problems/MostVisitedSectorInACircularTrack.py)|Easy      |          |
|1561|[Maximum Number of Coins You Can Get](problems/MaximumNumberOfCoinsYouCanGet.py)|Medium    |          |
|1562|Find Latest Group of Size M                                                     |Medium    |          |
|1563|[Stone Game V](problems/StoneGameV.py)                                          |Hard      |          |
|1564|Put Boxes Into the Warehouse I                                                  |Medium    |Premium   |
|1565|Unique Orders and Customers Per Month                                           |Easy      |Premium   |
|1566|Detect Pattern of Length M Repeated K or More Times                             |Easy      |          |
|1567|[Maximum Length of Subarray With Positive Product](problems/MaximumLengthOfSubarrayWithPositiveProduct.py)|Medium    |          |
|1568|Minimum Number of Days to Disconnect Island                                     |Hard      |          |
|1569|Number of Ways to Reorder Array to Get Same BST                                 |Hard      |          |
|1570|Dot Product of Two Sparse Vectors                                               |Medium    |Premium   |
|1571|Warehouse Manager                                                               |Easy      |Premium   |
|1572|[Matrix Diagonal Sum](problems/MatrixDiagonalSum.py)                            |Easy      |          |
|1573|[Number of Ways to Split a String](problems/NumberOfWaysToSplitAString.py)      |Medium    |          |
|1574|Shortest Subarray to be Removed to Make Array Sorted                            |Medium    |          |
|1575|Count All Possible Routes                                                       |Hard      |          |
|1576|Replace All ?'s to Avoid Consecutive Repeating Characters                       |Easy      |          |
|1577|Number of Ways Where Square of Number Is Equal to Product of Two Numbers        |Medium    |          |
|1578|Minimum Deletion Cost to Avoid Repeating Letters                                |Medium    |          |
|1579|Remove Max Number of Edges to Keep Graph Fully Traversable                      |Hard      |          |
|1580|Put Boxes Into the Warehouse II                                                 |Medium    |Premium   |
|1581|Customer Who Visited but Did Not Make Any Transactions                          |Easy      |Premium   |
|1582|[Special Positions in a Binary Matrix](problems/SpecialPositionsInABinaryMatrix.py)|Easy      |          |
|1583|Count Unhappy Friends                                                           |Medium    |          |
|1584|Min Cost to Connect All Points                                                  |Medium    |          |
|1585|Check If String Is Transformable With Substring Sort Operations                 |Hard      |          |
|1586|Binary Search Tree Iterator II                                                  |Medium    |Premium   |
|1587|Bank Account Summary II                                                         |Easy      |Premium   |
|1588|Sum of All Odd Length Subarrays                                                 |Easy      |          |
|1589|Maximum Sum Obtained of Any Permutation                                         |Medium    |          |
|1590|[Make Sum Divisible by P](problems/MakeSumDivisibleByP.py)                      |Medium    |          |
|1591|Strange Printer II                                                              |Hard      |          |
|1592|[Rearrange Spaces Between Words](problems/RearrangeSpacesBetweenWords.py)       |Easy      |          |
|1593|[Split a String Into the Max Number of Unique Substrings](problems/SplitAStringIntoTheMaxNumberOfUniqueSubstrings.py)|Medium    |          |
|1594|Maximum Non Negative Product in a Matrix                                        |Medium    |          |
|1595|Minimum Cost to Connect Two Groups of Points                                    |Hard      |          |
|1596|The Most Frequently Ordered Products for Each Customer                          |Medium    |Premium   |
|1597|Build Binary Expression Tree From Infix Expression                              |Hard      |Premium   |
|1598|[Crawler Log Folder](problems/CrawlerLogFolder.py)                              |Easy      |          |
|1599|Maximum Profit of Operating a Centennial Wheel                                  |Medium    |          |
|1600|[Throne Inheritance](problems/ThroneInheritance.py)                             |Medium    |          |
|1601|[Maximum Number of Achievable Transfer Requests](problems/MaximumNumberOfAchievableTransferRequests.py)|Hard      |          |
|1602|Find Nearest Right Node in Binary Tree                                          |Medium    |Premium   |
|1603|Design Parking System                                                           |Easy      |          |
|1604|Alert Using Same Key-Card Three or More Times in a One Hour Period              |Medium    |          |
|1605|Find Valid Matrix Given Row and Column Sums                                     |Medium    |          |
|1606|Find Servers That Handled Most Number of Requests                               |Hard      |          |
|1607|Sellers With No Sales                                                           |Easy      |Premium   |
|1608|Special Array With X Elements Greater Than or Equal X                           |Easy      |          |
|1609|[Even Odd Tree](problems/EvenOddTree.py)                                        |Medium    |          |
|1610|Maximum Number of Visible Points                                                |Hard      |          |
|1611|Minimum One Bit Operations to Make Integers Zero                                |Hard      |          |
|1612|Check If Two Expression Trees are Equivalent                                    |Medium    |Premium   |
|1613|Find the Missing IDs                                                            |Medium    |Premium   |
|1614|Maximum Nesting Depth of the Parentheses                                        |Easy      |          |
|1615|[Maximal Network Rank](problems/MaximalNetworkRank.py)                          |Medium    |          |
|1616|[Split Two Strings to Make Palindrome](problems/SplitTwoStringsToMakePalindrome.py)|Medium    |          |
|1617|Count Subtrees With Max Distance Between Cities                                 |Hard      |          |
|1618|Maximum Font to Fit a Sentence in a Screen                                      |Medium    |Premium   |
|1619|Mean of Array After Removing Some Elements                                      |Easy      |          |
|1620|Coordinate With Maximum Network Quality                                         |Medium    |          |
|1621|Number of Sets of K Non-Overlapping Line Segments                               |Medium    |          |
|1622|Fancy Sequence                                                                  |Hard      |          |
|1623|All Valid Triplets That Can Represent a Country                                 |Easy      |Premium   |
|1624|[Largest Substring Between Two Equal Characters](problems/LargestSubstringBetweenTwoEqualCharacters.py)|Easy      |          |
|1625|Lexicographically Smallest String After Applying Operations                     |Medium    |          |
|1626|[Best Team With No Conflicts](problems/BestTeamWithNoConflicts.py)              |Medium    |          |
|1627|Graph Connectivity With Threshold                                               |Hard      |          |
|1628|Design an Expression Tree With Evaluate Function                                |Medium    |Premium   |
|1629|[Slowest Key](problems/SlowestKey.py)                                           |Easy      |          |
|1630|Arithmetic Subarrays                                                            |Medium    |          |
|1631|[Path With Minimum Effort](problems/PathWithMinimumEffort.py)                   |Medium    |          |
|1632|[Rank Transform of a Matrix](problems/RankTransformOfAMatrix.py)                |Hard      |          |
|1633|Percentage of Users Attended a Contest                                          |Easy      |Premium   |
|1634|Add Two Polynomials Represented as Linked Lists                                 |Medium    |Premium   |
|1635|Hopper Company Queries I                                                        |Hard      |Premium   |
|1636|[Sort Array by Increasing Frequency](problems/SortArrayByIncreasingFrequency.py)|Easy      |          |
|1637|[Widest Vertical Area Between Two Points Containing No Points](problems/WidestVerticalAreaBetweenTwoPointsContainingNoPoints.py)|Medium    |          |
|1638|Count Substrings That Differ by One Character                                   |Medium    |          |
|1639|Number of Ways to Form a Target String Given a Dictionary                       |Hard      |          |
|1640|Check Array Formation Through Concatenation                                     |Easy      |          |
|1641|Count Sorted Vowel Strings                                                      |Medium    |          |
|1642|[Furthest Building You Can Reach](problems/FurthestBuildingYouCanReach.py)      |Medium    |          |
|1643|Kth Smallest Instructions                                                       |Hard      |          |
|1644|Lowest Common Ancestor of a Binary Tree II                                      |Medium    |Premium   |
|1645|Hopper Company Queries II                                                       |Hard      |Premium   |
|1646|Get Maximum in Generated Array                                                  |Easy      |          |
|1647|[Minimum Deletions to Make Character Frequencies Unique](problems/MinimumDeletionsToMakeCharacterFrequenciesUnique.py)|Medium    |          |
|1648|Sell Diminishing-Valued Colored Balls                                           |Medium    |          |
|1649|Create Sorted Array through Instructions                                        |Hard      |          |
|1650|Lowest Common Ancestor of a Binary Tree III                                     |Medium    |Premium   |
|1651|Hopper Company Queries III                                                      |Hard      |Premium   |
|1652|[Defuse the Bomb](problems/DefuseTheBomb.py)                                    |Easy      |          |
|1653|Minimum Deletions to Make String Balanced                                       |Medium    |          |
|1654|Minimum Jumps to Reach Home                                                     |Medium    |          |
|1655|Distribute Repeating Integers                                                   |Hard      |          |
|1656|Design an Ordered Stream                                                        |Easy      |          |
|1657|Determine if Two Strings Are Close                                              |Medium    |          |
|1658|Minimum Operations to Reduce X to Zero                                          |Medium    |          |
|1659|Maximize Grid Happiness                                                         |Hard      |          |
|1660|Correct a Binary Tree                                                           |Medium    |Premium   |
|1661|Average Time of Process per Machine                                             |Easy      |Premium   |
|1662|[Check If Two String Arrays are Equivalent](problems/CheckIfTwoStringArraysAreEquivalent.py)|Easy      |          |
|1663|[Smallest String With A Given Numeric Value](problems/SmallestStringWithAGivenNumericValue.py)|Medium    |          |
|1664|[Ways to Make a Fair Array](problems/WaysToMakeAFairArray.py)                   |Medium    |          |
|1665|[Minimum Initial Energy to Finish Tasks](problems/MinimumInitialEnergyToFinishTasks.py)|Hard      |          |
|1666|Change the Root of a Binary Tree                                                |Medium    |Premium   |
|1667|Fix Names in a Table                                                            |Easy      |Premium   |
|1668|[Maximum Repeating Substring](problems/MaximumRepeatingSubstring.py)            |Easy      |          |
|1669|[Merge In Between Linked Lists](problems/MergeInBetweenLinkedLists.py)          |Medium    |          |
|1670|Design Front Middle Back Queue                                                  |Medium    |          |
|1671|Minimum Number of Removals to Make Mountain Array                               |Hard      |          |
|1672|Richest Customer Wealth                                                         |Easy      |          |
|1673|[Find the Most Competitive Subsequence](problems/FindTheMostCompetitiveSubsequence.py)|Medium    |          |
|1674|Minimum Moves to Make Array Complementary                                       |Medium    |          |
|1675|[Minimize Deviation in Array](problems/MinimizeDeviationInArray.py)             |Hard      |          |
|1676|Lowest Common Ancestor of a Binary Tree IV                                      |Medium    |Premium   |
|1677|Product's Worth Over Invoices                                                   |Easy      |Premium   |
|1678|[Goal Parser Interpretation](problems/GoalParserInterpretation.py)              |Easy      |          |
|1679|Max Number of K-Sum Pairs                                                       |Medium    |          |
|1680|[Concatenation of Consecutive Binary Numbers](problems/ConcatenationOfConsecutiveBinaryNumbers.py)|Medium    |          |
|1681|Minimum Incompatibility                                                         |Hard      |          |
|1682|Longest Palindromic Subsequence II                                              |Medium    |Premium   |
|1683|Invalid Tweets                                                                  |Easy      |Premium   |
|1684|[Count the Number of Consistent Strings](problems/CountTheNumberOfConsistentStrings.py)|Easy      |          |
|1685|Sum of Absolute Differences in a Sorted Array                                   |Medium    |          |
|1686|[Stone Game VI](problems/StoneGameVi.py)                                        |Medium    |          |
|1687|Delivering Boxes from Storage to Ports                                          |Hard      |          |
|1688|[Count of Matches in Tournament](problems/CountOfMatchesInTournament.py)        |Easy      |          |
|1689|[Partitioning Into Minimum Number Of Deci-Binary Numbers](problems/PartitioningIntoMinimumNumberOfDeciBinaryNumbers.py)|Medium    |          |
|1690|[Stone Game VII](problems/StoneGameVii.py)                                      |Medium    |          |
|1691|Maximum Height by Stacking Cuboids                                              |Hard      |          |
|1692|Count Ways to Distribute Candies                                                |Hard      |Premium   |
|1693|Daily Leads and Partners                                                        |Easy      |Premium   |
|1694|[Reformat Phone Number](problems/ReformatPhoneNumber.py)                        |Easy      |          |
|1695|[Maximum Erasure Value](problems/MaximumErasureValue.py)                        |Medium    |          |
|1696|[Jump Game VI](problems/JumpGameVi.py)                                          |Medium    |          |
|1697|Checking Existence of Edge Length Limited Paths                                 |Hard      |          |
|1698|Number of Distinct Substrings in a String                                       |Medium    |Premium   |
|1699|Number of Calls Between Two Persons                                             |Medium    |Premium   |
|1700|[Number of Students Unable to Eat Lunch](problems/NumberOfStudentsUnableToEatLunch.py)|Easy      |          |
|1701|[Average Waiting Time](problems/AverageWaitingTime.py)                          |Medium    |          |
|1702|[Maximum Binary String After Change](problems/MaximumBinaryStringAfterChange.py)|Medium    |          |
|1703|Minimum Adjacent Swaps for K Consecutive Ones                                   |Hard      |          |
|1704|[Determine if String Halves Are Alike](problems/DetermineIfStringHalvesAreAlike.py)|Easy      |          |
|1705|Maximum Number of Eaten Apples                                                  |Medium    |          |
|1706|Where Will the Ball Fall                                                        |Medium    |          |
|1707|[Maximum XOR With an Element From Array](problems/MaximumXorWithAnElementFromArray.py)|Hard      |          |
|1708|Largest Subarray Length K                                                       |Easy      |Premium   |
|1709|Biggest Window Between Visits                                                   |Medium    |Premium   |
|1710|[Maximum Units on a Truck](problems/MaximumUnitsOnATruck.py)                    |Easy      |          |
|1711|Count Good Meals                                                                |Medium    |          |
|1712|[Ways to Split Array Into Three Subarrays](problems/WaysToSplitArrayIntoThreeSubarrays.py)|Medium    |          |
|1713|Minimum Operations to Make a Subsequence                                        |Hard      |          |
|1714|Sum Of Special Evenly-Spaced Elements In Array                                  |Hard      |Premium   |
|1715|Count Apples and Oranges                                                        |Medium    |Premium   |
|1716|Calculate Money in Leetcode Bank                                                |Easy      |          |
|1717|Maximum Score From Removing Substrings                                          |Medium    |          |
|1718|Construct the Lexicographically Largest Valid Sequence                          |Medium    |          |
|1719|Number Of Ways To Reconstruct A Tree                                            |Hard      |          |
|1720|Decode XORed Array                                                              |Easy      |          |
|1721|[Swapping Nodes in a Linked List](problems/SwappingNodesInALinkedList.py)       |Medium    |          |
|1722|Minimize Hamming Distance After Swap Operations                                 |Medium    |          |
|1723|[Find Minimum Time to Finish All Jobs](problems/FindMinimumTimeToFinishAllJobs.py)|Hard      |          |
|1724|Checking Existence of Edge Length Limited Paths II                              |Hard      |Premium   |
|1725|Number Of Rectangles That Can Form The Largest Square                           |Easy      |          |
|1726|Tuple with Same Product                                                         |Medium    |          |
|1727|Largest Submatrix With Rearrangements                                           |Medium    |          |
|1728|[Cat and Mouse II](problems/CatAndMouseIi.py)                                   |Hard      |          |
|1729|Find Followers Count                                                            |Easy      |Premium   |
|1730|Shortest Path to Get Food                                                       |Medium    |Premium   |
|1731|The Number of Employees Which Report to Each Employee                           |Easy      |Premium   |
|1732|[Find the Highest Altitude](problems/FindTheHighestAltitude.py)                 |Easy      |          |
|1733|Minimum Number of People to Teach                                               |Medium    |          |
|1734|Decode XORed Permutation                                                        |Medium    |          |
|1735|Count Ways to Make Array With Product                                           |Hard      |          |
|1736|[Latest Time by Replacing Hidden Digits](problems/LatestTimeByReplacingHiddenDigits.py)|Easy      |          |
|1737|[Change Minimum Characters to Satisfy One of Three Conditions](problems/ChangeMinimumCharactersToSatisfyOneOfThreeConditions.py)|Medium    |          |
|1738|Find Kth Largest XOR Coordinate Value                                           |Medium    |          |
|1739|Building Boxes                                                                  |Hard      |          |
|1740|Find Distance in a Binary Tree                                                  |Medium    |Premium   |
|1741|Find Total Time Spent by Each Employee                                          |Easy      |Premium   |
|1742|[Maximum Number of Balls in a Box](problems/MaximumNumberOfBallsInABox.py)      |Easy      |          |
|1743|[Restore the Array From Adjacent Pairs](problems/RestoreTheArrayFromAdjacentPairs.py)|Medium    |          |
|1744|Can You Eat Your Favorite Candy on Your Favorite Day?                           |Medium    |          |
|1745|Palindrome Partitioning IV                                                      |Hard      |          |
|1746|Maximum Subarray Sum After One Operation                                        |Medium    |Premium   |
|1747|Leetflex Banned Accounts                                                        |Medium    |Premium   |
|1748|Sum of Unique Elements                                                          |Easy      |          |
|1749|[Maximum Absolute Sum of Any Subarray](problems/MaximumAbsoluteSumOfAnySubarray.py)|Medium    |          |
|1750|Minimum Length of String After Deleting Similar Ends                            |Medium    |          |
|1751|Maximum Number of Events That Can Be Attended II                                |Hard      |          |
|1752|Check if Array Is Sorted and Rotated                                            |Easy      |          |
|1753|[Maximum Score From Removing Stones](problems/MaximumScoreFromRemovingStones.py)|Medium    |          |
|1754|Largest Merge Of Two Strings                                                    |Medium    |          |
|1755|Closest Subsequence Sum                                                         |Hard      |          |
|1756|Design Most Recently Used Queue                                                 |Medium    |Premium   |
|1757|Recyclable and Low Fat Products                                                 |Easy      |Premium   |
|1758|Minimum Changes To Make Alternating Binary String                               |Easy      |          |
|1759|Count Number of Homogenous Substrings                                           |Medium    |          |
|1760|[Minimum Limit of Balls in a Bag](problems/MinimumLimitOfBallsInABag.py)        |Medium    |          |
|1761|Minimum Degree of a Connected Trio in a Graph                                   |Hard      |          |
|1762|Buildings With an Ocean View                                                    |Medium    |Premium   |
|1763|Longest Nice Substring                                                          |Easy      |          |
|1764|Form Array by Concatenating Subarrays of Another Array                          |Medium    |          |
|1765|Map of Highest Peak                                                             |Medium    |          |
|1766|Tree of Coprimes                                                                |Hard      |          |
|1767|Find the Subtasks That Did Not Execute                                          |Hard      |Premium   |
|1768|Merge Strings Alternately                                                       |Easy      |          |
|1769|Minimum Number of Operations to Move All Balls to Each Box                      |Medium    |          |
|1770|[Maximum Score from Performing Multiplication Operations](problems/MaximumScoreFromPerformingMultiplicationOperations.py)|Medium    |          |
|1771|Maximize Palindrome Length From Subsequences                                    |Hard      |          |
|1772|Sort Features by Popularity                                                     |Medium    |Premium   |
|1773|[Count Items Matching a Rule](problems/CountItemsMatchingARule.py)              |Easy      |          |
|1774|[Closest Dessert Cost](problems/ClosestDessertCost.py)                          |Medium    |          |
|1775|[Equal Sum Arrays With Minimum Number of Operations](problems/EqualSumArraysWithMinimumNumberOfOperations.py)|Medium    |          |
|1776|[Car Fleet II](problems/CarFleetIi.py)                                          |Hard      |          |
|1777|Product's Price for Each Store                                                  |Easy      |Premium   |
|1778|Shortest Path in a Hidden Grid                                                  |Medium    |Premium   |
|1779|[Find Nearest Point That Has the Same X or Y Coordinate](problems/FindNearestPointThatHasTheSameXOrYCoordinate.py)|Easy      |          |
|1780|[Check if Number is a Sum of Powers of Three](problems/CheckIfNumberIsASumOfPowersOfThree.py)|Medium    |          |
|1781|Sum of Beauty of All Substrings                                                 |Medium    |          |
|1782|Count Pairs Of Nodes                                                            |Hard      |          |
|1783|Grand Slam Titles                                                               |Medium    |Premium   |
|1784|Check if Binary String Has at Most One Segment of Ones                          |Easy      |          |
|1785|[Minimum Elements to Add to Form a Given Sum](problems/MinimumElementsToAddToFormAGivenSum.py)|Medium    |          |
|1786|Number of Restricted Paths From First to Last Node                              |Medium    |          |
|1787|Make the XOR of All Segments Equal to Zero                                      |Hard      |          |
|1788|Maximize the Beauty of the Garden                                               |Hard      |Premium   |
|1789|Primary Department for Each Employee                                            |Easy      |Premium   |
|1790|Check if One String Swap Can Make Strings Equal                                 |Easy      |          |
|1791|Find Center of Star Graph                                                       |Medium    |          |
|1792|Maximum Average Pass Ratio                                                      |Medium    |          |
|1793|Maximum Score of a Good Subarray                                                |Hard      |          |
|1794|Count Pairs of Equal Substrings With Minimum Difference                         |Medium    |Premium   |
|1795|Rearrange Products Table                                                        |Easy      |Premium   |
|1796|[Second Largest Digit in a String](problems/SecondLargestDigitInAString.py)     |Easy      |          |
|1797|[Design Authentication Manager](problems/DesignAuthenticationManager.py)        |Medium    |          |
|1798|Maximum Number of Consecutive Values You Can Make                               |Medium    |          |
|1799|[Maximize Score After N Operations](problems/MaximizeScoreAfterNOperations.py)  |Hard      |          |
|1800|[Maximum Ascending Subarray Sum](problems/MaximumAscendingSubarraySum.py)       |Easy      |          |
|1801|Number of Orders in the Backlog                                                 |Medium    |          |
|1802|[Maximum Value at a Given Index in a Bounded Array](problems/MaximumValueAtAGivenIndexInABoundedArray.py)|Medium    |          |
|1803|[Count Pairs With XOR in a Range](problems/CountPairsWithXorInARange.py)        |Hard      |          |
|1804|Implement Trie II (Prefix Tree)                                                 |Medium    |Premium   |
|1805|[Number of Different Integers in a String](problems/NumberOfDifferentIntegersInAString.py)|Easy      |          |
|1806|Minimum Number of Operations to Reinitialize a Permutation                      |Medium    |          |
|1807|[Evaluate the Bracket Pairs of a String](problems/EvaluateTheBracketPairsOfAString.py)|Medium    |          |
|1808|Maximize Number of Nice Divisors                                                |Hard      |          |
|1809|Ad-Free Sessions                                                                |Easy      |Premium   |
|1810|Minimum Path Cost in a Hidden Grid                                              |Medium    |Premium   |
|1811|Find Interview Candidates                                                       |Medium    |Premium   |
|1812|Determine Color of a Chessboard Square                                          |Easy      |          |
|1813|Sentence Similarity III                                                         |Medium    |          |
|1814|Count Nice Pairs in an Array                                                    |Medium    |          |
|1815|Maximum Number of Groups Getting Fresh Donuts                                   |Hard      |          |
|1816|Truncate Sentence                                                               |Easy      |          |
|1817|[Finding the Users Active Minutes](problems/FindingTheUsersActiveMinutes.py)    |Medium    |          |
|1818|Minimum Absolute Sum Difference                                                 |Medium    |          |
|1819|[Number of Different Subsequences GCDs](problems/NumberOfDifferentSubsequencesGcds.py)|Hard      |          |
|1820|Maximum Number of Accepted Invitations                                          |Medium    |Premium   |
|1821|Find Customers With Positive Revenue this Year                                  |Easy      |Premium   |
|1822|[Sign of the Product of an Array](problems/SignOfTheProductOfAnArray.py)        |Easy      |          |
|1823|Find the Winner of the Circular Game                                            |Medium    |          |
|1824|Minimum Sideway Jumps                                                           |Medium    |          |
|1825|Finding MK Average                                                              |Hard      |          |
|1826|Faulty Sensor                                                                   |Easy      |Premium   |
|1827|Minimum Operations to Make the Array Increasing                                 |Easy      |          |
|1828|[Queries on Number of Points Inside a Circle](problems/QueriesOnNumberOfPointsInsideACircle.py)|Medium    |          |
|1829|Maximum XOR for Each Query                                                      |Medium    |          |
|1830|Minimum Number of Operations to Make String Sorted                              |Hard      |          |
|1831|Maximum Transaction Each Day                                                    |Medium    |Premium   |
|1832|Check if the Sentence Is Pangram                                                |Easy      |          |
|1833|[Maximum Ice Cream Bars](problems/MaximumIceCreamBars.py)                       |Medium    |          |
|1834|Single-Threaded CPU                                                             |Medium    |          |
|1835|Find XOR Sum of All Pairs Bitwise AND                                           |Hard      |          |
|1836|Remove Duplicates From an Unsorted Linked List                                  |Medium    |Premium   |
|1837|Sum of Digits in Base K                                                         |Easy      |          |
|1838|[Frequency of the Most Frequent Element](problems/FrequencyOfTheMostFrequentElement.py)|Medium    |          |
|1839|Longest Substring Of All Vowels in Order                                        |Medium    |          |
|1840|[Maximum Building Height](problems/MaximumBuildingHeight.py)                    |Hard      |          |
|1841|League Statistics                                                               |Medium    |Premium   |
|1842|Next Palindrome Using Same Digits                                               |Hard      |Premium   |
|1843|Suspicious Bank Accounts                                                        |Medium    |Premium   |
|1844|[Replace All Digits with Characters](problems/ReplaceAllDigitsWithCharacters.py)|Easy      |          |
|1845|Seat Reservation Manager                                                        |Medium    |          |
|1846|Maximum Element After Decreasing and Rearranging                                |Medium    |          |
|1847|[Closest Room](problems/ClosestRoom.py)                                         |Hard      |          |
|1848|[Minimum Distance to the Target Element](problems/MinimumDistanceToTheTargetElement.py)|Easy      |          |
|1849|Splitting a String Into Descending Consecutive Values                           |Medium    |          |
|1850|Minimum Adjacent Swaps to Reach the Kth Smallest Number                         |Medium    |          |
|1851|Minimum Interval to Include Each Query                                          |Hard      |          |
|1852|Distinct Numbers in Each Subarray                                               |Medium    |Premium   |
|1853|Convert Date Format                                                             |Easy      |Premium   |
|1854|Maximum Population Year                                                         |Easy      |          |
|1855|Maximum Distance Between a Pair of Values                                       |Medium    |          |
|1856|Maximum Subarray Min-Product                                                    |Medium    |          |
|1857|Largest Color Value in a Directed Graph                                         |Hard      |          |
|1858|Longest Word With All Prefixes                                                  |Medium    |Premium   |
|1859|[Sorting the Sentence](problems/SortingTheSentence.py)                          |Easy      |          |
|1860|Incremental Memory Leak                                                         |Medium    |          |
|1861|[Rotating the Box](problems/RotatingTheBox.py)                                  |Medium    |          |
|1862|Sum of Floored Pairs                                                            |Hard      |          |
|1863|[Sum of All Subset XOR Totals](problems/SumOfAllSubsetXorTotals.py)             |Easy      |          |
|1864|[Minimum Number of Swaps to Make the Binary String Alternating](problems/MinimumNumberOfSwapsToMakeTheBinaryStringAlternating.py)|Medium    |          |
|1865|Finding Pairs With a Certain Sum                                                |Medium    |          |
|1866|Number of Ways to Rearrange Sticks With K Sticks Visible                        |Hard      |          |
|1867|Orders With Maximum Quantity Above Average                                      |Medium    |Premium   |
|1868|Product of Two Run-Length Encoded Arrays                                        |Medium    |Premium   |
|1869|Longer Contiguous Segments of Ones than Zeros                                   |Easy      |          |
|1870|Minimum Speed to Arrive on Time                                                 |Medium    |          |
|1871|[Jump Game VII](problems/JumpGameVii.py)                                        |Medium    |          |
|1872|[Stone Game VIII](problems/StoneGameViii.py)                                    |Hard      |          |
|1873|Calculate Special Bonus                                                         |Easy      |Premium   |
|1874|Minimize Product Sum of Two Arrays                                              |Medium    |Premium   |
|1875|Group Employees of the Same Salary                                              |Medium    |Premium   |
|1876|Substrings of Size Three with Distinct Characters                               |Easy      |          |
|1877|Minimize Maximum Pair Sum in Array                                              |Medium    |          |
|1878|Get Biggest Three Rhombus Sums in a Grid                                        |Medium    |          |
|1879|Minimum XOR Sum of Two Arrays                                                   |Hard      |          |
|1880|[Check if Word Equals Summation of Two Words](problems/CheckIfWordEqualsSummationOfTwoWords.py)|Easy      |          |
|1881|Maximum Value after Insertion                                                   |Medium    |          |
|1882|Process Tasks Using Servers                                                     |Medium    |          |
|1883|Minimum Skips to Arrive at Meeting On Time                                      |Hard      |          |
|1884|Egg Drop With 2 Eggs and N Floors                                               |Medium    |          |
|1885|Count Pairs in Two Arrays                                                       |Medium    |Premium   |
|1886|Determine Whether Matrix Can Be Obtained By Rotation                            |Easy      |          |
|1887|Reduction Operations to Make the Array Elements Equal                           |Medium    |          |
|1888|Minimum Number of Flips to Make the Binary String Alternating                   |Medium    |          |
|1889|Minimum Space Wasted From Packaging                                             |Hard      |          |
|1890|The Latest Login in 2020                                                        |Easy      |Premium   |
|1891|Cutting Ribbons                                                                 |Medium    |Premium   |
|1892|Page Recommendations II                                                         |Hard      |Premium   |
|1893|[Check if All the Integers in a Range Are Covered](problems/CheckIfAllTheIntegersInARangeAreCovered.py)|Easy      |          |
|1894|Find the Student that Will Replace the Chalk                                    |Medium    |          |
|1895|Largest Magic Square                                                            |Medium    |          |
|1896|Minimum Cost to Change the Final Value of Expression                            |Hard      |          |
|1897|Redistribute Characters to Make All Strings Equal                               |Easy      |          |
|1898|Maximum Number of Removable Characters                                          |Medium    |          |
|1899|[Merge Triplets to Form Target Triplet](problems/MergeTripletsToFormTargetTriplet.py)|Medium    |          |
|1900|The Earliest and Latest Rounds Where Players Compete                            |Hard      |          |
|1901|[Find a Peak Element II](problems/FindAPeakElementIi.py)                        |Medium    |          |
|1902|Depth of BST Given Insertion Order                                              |Medium    |Premium   |
|1903|[Largest Odd Number in String](problems/LargestOddNumberInString.py)            |Easy      |          |
|1904|The Number of Full Rounds You Have Played                                       |Medium    |          |
|1905|[Count Sub Islands](problems/CountSubIslands.py)                                |Medium    |          |
|1906|Minimum Absolute Difference Queries                                             |Medium    |          |
|1907|Count Salary Categories                                                         |Medium    |Premium   |
|1908|Game of Nim                                                                     |Medium    |Premium   |
|1909|Remove One Element to Make the Array Strictly Increasing                        |Easy      |          |
|1910|[Remove All Occurrences of a Substring](problems/RemoveAllOccurrencesOfASubstring.py)|Medium    |          |
|1911|Maximum Alternating Subsequence Sum                                             |Medium    |          |
|1912|Design Movie Rental System                                                      |Hard      |          |
|1913|Maximum Product Difference Between Two Pairs                                    |Easy      |          |
|1914|Cyclically Rotating a Grid                                                      |Medium    |          |
|1915|Number of Wonderful Substrings                                                  |Medium    |          |
|1916|Count Ways to Build Rooms in an Ant Colony                                      |Hard      |          |
|1917|Leetcodify Friends Recommendations                                              |Hard      |Premium   |
|1918|Kth Smallest Subarray Sum                                                       |Medium    |Premium   |
|1919|Leetcodify Similar Friends                                                      |Hard      |Premium   |
|1920|[Build Array from Permutation](problems/BuildArrayFromPermutation.py)           |Easy      |          |
|1921|Eliminate Maximum Number of Monsters                                            |Medium    |          |
|1922|Count Good Numbers                                                              |Medium    |          |
|1923|Longest Common Subpath                                                          |Hard      |          |
|1924|Erect the Fence II                                                              |Hard      |Premium   |
|1925|Count Square Sum Triples                                                        |Easy      |          |
|1926|Nearest Exit from Entrance in Maze                                              |Medium    |          |
|1927|Sum Game                                                                        |Medium    |          |
|1928|Minimum Cost to Reach Destination in Time                                       |Hard      |          |
|1929|[Concatenation of Array](problems/ConcatenationOfArray.py)                      |Easy      |          |
|1930|[Unique Length-3 Palindromic Subsequences](problems/UniqueLength3PalindromicSubsequences.py)|Medium    |          |
|1931|Painting a Grid With Three Different Colors                                     |Hard      |          |
|1932|Merge BSTs to Create Single BST                                                 |Hard      |          |
|1933|Check if String Is Decomposable Into Value-Equal Substrings                     |Easy      |Premium   |
|1934|Confirmation Rate                                                               |Medium    |Premium   |
|1935|[Maximum Number of Words You Can Type](problems/MaximumNumberOfWordsYouCanType.py)|Easy      |          |
|1936|Add Minimum Number of Rungs                                                     |Medium    |          |
|1937|Maximum Number of Points with Cost                                              |Medium    |          |
|1938|Maximum Genetic Difference Query                                                |Hard      |          |
|1939|Users That Actively Request Confirmation Messages                               |Easy      |Premium   |
|1940|Longest Common Subsequence Between Sorted Arrays                                |Medium    |Premium   |
|1941|[Check if All Characters Have Equal Number of Occurrences](problems/CheckIfAllCharactersHaveEqualNumberOfOccurrences.py)|Easy      |          |
|1942|The Number of the Smallest Unoccupied Chair                                     |Medium    |          |
|1943|Describe the Painting                                                           |Medium    |          |
|1944|Number of Visible People in a Queue                                             |Hard      |          |
|1945|Sum of Digits of String After Convert                                           |Easy      |          |
|1946|Largest Number After Mutating Substring                                         |Medium    |          |
|1947|Maximum Compatibility Score Sum                                                 |Medium    |          |
|1948|[Delete Duplicate Folders in System](problems/DeleteDuplicateFoldersInSystem.py)|Hard      |          |
|1949|Strong Friendship                                                               |Medium    |          |
|1950|Maximum of Minimum Values in All Subarrays                                      |Medium    |          |
|1951|All the Pairs With the Maximum Number of Common Followers                       |Medium    |          |
|1952|Three Divisors                                                                  |Easy      |          |
|1953|Maximum Number of Weeks for Which You Can Work                                  |Medium    |          |
|1954|Minimum Garden Perimeter to Collect Enough Apples                               |Medium    |          |
|1955|Count Number of Special Subsequences                                            |Hard      |          |
|1956|Minimum Time For K Virus Variants to Spread                                     |Hard      |          |
|1957|Delete Characters to Make Fancy String                                          |Easy      |          |
|1958|Check if Move is Legal                                                          |Medium    |          |
|1959|Minimum Total Space Wasted With K Resizing Operations                           |Medium    |          |
|1960|Maximum Product of the Length of Two Palindromic Substrings                     |Hard      |          |
|1961|Check If String Is a Prefix of Array                                            |Easy      |          |
|1962|Remove Stones to Minimize the Total                                             |Medium    |          |
|1963|Minimum Number of Swaps to Make the String Balanced                             |Medium    |          |
|1964|Find the Longest Valid Obstacle Course at Each Position                         |Hard      |          |
|1965|Employees With Missing Information                                              |Easy      |          |
|1966|Binary Searchable Numbers in an Unsorted Array                                  |Medium    |          |
|1967|Number of Strings That Appear as Substrings in Word                             |Easy      |          |
|1968|Array With Elements Not Equal to Average of Neighbors                           |Medium    |          |
|1969|Minimum Non-Zero Product of the Array Elements                                  |Medium    |          |
|1970|Last Day Where You Can Still Cross                                              |Hard      |          |
|1971|Find if Path Exists in Graph                                                    |Easy      |          |
|1972|First and Last Call On the Same Day                                             |Hard      |          |
|1973|Count Nodes Equal to Sum of Descendants                                         |Medium    |          |
|1974|Minimum Time to Type Word Using Special Typewriter                              |Easy      |          |
|1975|Maximum Matrix Sum                                                              |Medium    |          |
|1976|Number of Ways to Arrive at Destination                                         |Medium    |          |
|1977|Number of Ways to Separate Numbers                                              |Hard      |          |
|1978|Employees Whose Manager Left the Company                                        |Easy      |          |
|1979|Find Greatest Common Divisor of Array                                           |Easy      |          |
|1980|Find Unique Binary String                                                       |Medium    |          |
|1981|Minimize the Difference Between Target and Chosen Elements                      |Medium    |          |
|1982|Find Array Given Subset Sums                                                    |Hard      |          |
|1983|Widest Pair of Indices With Equal Range Sum                                     |Medium    |          |
|1984|Minimum Difference Between Highest and Lowest of K Scores                       |Easy      |          |
|1985|Find the Kth Largest Integer in the Array                                       |Medium    |          |
|1986|Minimum Number of Work Sessions to Finish the Tasks                             |Medium    |          |
|1987|Number of Unique Good Subsequences                                              |Hard      |          |
|1988|Find Cutoff Score for Each School                                               |Medium    |          |
|1989|Maximum Number of People That Can Be Caught in Tag                              |Medium    |          |
|1990|Count the Number of Experiments                                                 |Medium    |          |
|1991|Find the Middle Index in Array                                                  |Easy      |          |
|1992|Find All Groups of Farmland                                                     |Medium    |          |
|1993|Operations on Tree                                                              |Medium    |          |
|1994|The Number of Good Subsets                                                      |Hard      |          |
|1995|Count Special Quadruplets                                                       |Easy      |          |
|1996|The Number of Weak Characters in the Game                                       |Medium    |          |
|1997|First Day Where You Have Been in All the Rooms                                  |Medium    |          |
|1998|GCD Sort of an Array                                                            |Hard      |          |
|1999|Smallest Greater Multiple Made of Two Digits                                    |Medium    |          |
|2000|[Reverse Prefix of Word](problems/ReversePrefixOfWord.py)                       |Easy      |          |
|2001|Number of Pairs of Interchangeable Rectangles                                   |Medium    |          |
|2002|Maximum Product of the Length of Two Palindromic Subsequences                   |Medium    |          |
|2003|[Smallest Missing Genetic Value in Each Subtree](problems/SmallestMissingGeneticValueInEachSubtree.py)|Hard      |          |
|2004|The Number of Seniors and Juniors to Join the Company                           |Hard      |          |
|2005|Subtree Removal Game with Fibonacci Tree                                        |Hard      |          |
|2006|Count Number of Pairs With Absolute Difference K                                |Easy      |          |
|2007|Find Original Array From Doubled Array                                          |Medium    |          |
|2008|Maximum Earnings From Taxi                                                      |Medium    |          |
|2009|Minimum Number of Operations to Make Array Continuous                           |Hard      |          |
|2010|The Number of Seniors and Juniors to Join the Company II                        |Hard      |          |
|2011|Final Value of Variable After Performing Operations                             |Easy      |          |
|2012|Sum of Beauty in the Array                                                      |Medium    |          |
|2013|Detect Squares                                                                  |Medium    |          |
|2014|Longest Subsequence Repeated k Times                                            |Hard      |          |
|2015|Average Height of Buildings in Each Segment                                     |Medium    |          |
|2016|Maximum Difference Between Increasing Elements                                  |Easy      |          |
|2017|Grid Game                                                                       |Medium    |          |
|2018|Check if Word Can Be Placed In Crossword                                        |Medium    |          |
|2019|The Score of Students Solving Math Expression                                   |Hard      |          |
|2020|Number of Accounts That Did Not Stream                                          |Medium    |          |
|2021|Brightest Position on Street                                                    |Medium    |          |
|2022|[Convert 1D Array Into 2D Array](problems/Convert1dArrayInto2dArray.py)         |Easy      |          |
|2023|Number of Pairs of Strings With Concatenation Equal to Target                   |Medium    |          |
|2024|Maximize the Confusion of an Exam                                               |Medium    |          |
|2025|Maximum Number of Ways to Partition an Array                                    |Hard      |          |
|2026|Low-Quality Problems                                                            |Easy      |          |
|2027|Minimum Moves to Convert String                                                 |Easy      |          |
|2028|Find Missing Observations                                                       |Medium    |          |
|2029|Stone Game IX                                                                   |Medium    |          |
|2030|Smallest K-Length Subsequence With Occurrences of a Letter                      |Hard      |          |
|2031|Count Subarrays With More Ones Than Zeros                                       |Medium    |          |
|2032|[Two Out of Three](problems/TwoOutOfThree.py)                                   |Easy      |          |
|2033|Minimum Operations to Make a Uni-Value Grid                                     |Medium    |          |
|2034|Stock Price Fluctuation                                                         |Medium    |          |
|2035|Partition Array Into Two Arrays to Minimize Sum Difference                      |Hard      |          |
|2036|Maximum Alternating Subarray Sum                                                |Medium    |          |
|2037|Minimum Number of Moves to Seat Everyone                                        |Easy      |          |
|2038|Remove Colored Pieces if Both Neighbors are the Same Color                      |Medium    |          |
|2039|The Time When the Network Becomes Idle                                          |Medium    |          |
|2040|Kth Smallest Product of Two Sorted Arrays                                       |Hard      |          |
|2041|Accepted Candidates From the Interviews                                         |Medium    |          |
|2042|[Check if Numbers Are Ascending in a Sentence](problems/CheckIfNumbersAreAscendingInASentence.py)|Easy      |          |
|2043|Simple Bank System                                                              |Medium    |          |
|2044|[Count Number of Maximum Bitwise-OR Subsets](problems/CountNumberOfMaximumBitwiseOrSubsets.py)|Medium    |          |
|2045|Second Minimum Time to Reach Destination                                        |Hard      |          |
|2046|Sort Linked List Already Sorted Using Absolute Values                           |Medium    |          |
|2047|Number of Valid Words in a Sentence                                             |Easy      |          |
|2048|Next Greater Numerically Balanced Number                                        |Medium    |          |
|2049|Count Nodes With the Highest Score                                              |Medium    |          |
|2050|Parallel Courses III                                                            |Hard      |          |
|2051|The Category of Each Member in the Store                                        |Medium    |          |
|2052|Minimum Cost to Separate Sentence Into Rows                                     |Medium    |          |
|2053|Kth Distinct String in an Array                                                 |Easy      |          |
|2054|Two Best Non-Overlapping Events                                                 |Medium    |          |
|2055|Plates Between Candles                                                          |Medium    |          |
|2056|Number of Valid Move Combinations On Chessboard                                 |Hard      |          |
|2057|[Smallest Index With Equal Value](problems/SmallestIndexWithEqualValue.py)      |Easy      |          |
|2058|Find the Minimum and Maximum Number of Nodes Between Critical Points            |Medium    |          |
|2059|Minimum Operations to Convert Number                                            |Medium    |          |
|2060|Check if an Original String Exists Given Two Encoded Strings                    |Hard      |          |
|2061|Number of Spaces Cleaning Robot Cleaned                                         |Medium    |          |
|2062|Count Vowel Substrings of a String                                              |Easy      |          |
|2063|Vowels of All Substrings                                                        |Medium    |          |
|2064|Minimized Maximum of Products Distributed to Any Store                          |Medium    |          |
|2065|Maximum Path Quality of a Graph                                                 |Hard      |          |
|2066|Account Balance                                                                 |Medium    |          |
|2067|Number of Equal Count Substrings                                                |Medium    |          |
|2068|[Check Whether Two Strings are Almost Equivalent](problems/CheckWhetherTwoStringsAreAlmostEquivalent.py)|Easy      |          |
|2069|Walking Robot Simulation II                                                     |Medium    |          |
|2070|Most Beautiful Item for Each Query                                              |Medium    |          |
|2071|Maximum Number of Tasks You Can Assign                                          |Hard      |          |
|2072|The Winner University                                                           |Easy      |          |
|2073|Time Needed to Buy Tickets                                                      |Easy      |          |
|2074|Reverse Nodes in Even Length Groups                                             |Medium    |          |
|2075|[Decode the Slanted Ciphertext](problems/DecodeTheSlantedCiphertext.py)         |Medium    |          |
|2076|Process Restricted Friend Requests                                              |Hard      |          |
|2077|Paths in Maze That Lead to Same Room                                            |Medium    |          |
|2078|Two Furthest Houses With Different Colors                                       |Easy      |          |
|2079|[Watering Plants](problems/WateringPlants.py)                                   |Medium    |          |
|2080|Range Frequency Queries                                                         |Medium    |          |
|2081|Sum of k-Mirror Numbers                                                         |Hard      |          |
|2082|The Number of Rich Customers                                                    |Easy      |          |
|2083|Substrings That Begin and End With the Same Letter                              |Medium    |          |
|2084|Drop Type 1 Orders for Customers With Type 0 Orders                             |Medium    |          |
|2085|Count Common Words With One Occurrence                                          |Easy      |          |
|2086|Minimum Number of Food Buckets to Feed the Hamsters                             |Medium    |          |
|2087|[Minimum Cost Homecoming of a Robot in a Grid](problems/MinimumCostHomecomingOfARobotInAGrid.py)|Medium    |          |
|2088|Count Fertile Pyramids in a Land                                                |Hard      |          |
|2089|[Find Target Indices After Sorting Array](problems/FindTargetIndicesAfterSortingArray.py)|Easy      |          |
|2090|K Radius Subarray Averages                                                      |Medium    |          |
|2091|[Removing Minimum and Maximum From Array](problems/RemovingMinimumAndMaximumFromArray.py)|Medium    |          |
|2092|Find All People With Secret                                                     |Hard      |          |
|2093|Minimum Cost to Reach City With Discounts                                       |Medium    |          |
|2094|Finding 3-Digit Even Numbers                                                    |Easy      |          |
|2095|Delete the Middle Node of a Linked List                                         |Medium    |          |
|2096|Step-By-Step Directions From a Binary Tree Node to Another                      |Medium    |          |
|2097|Valid Arrangement of Pairs                                                      |Hard      |          |
|2098|Subsequence of Size K With the Largest Even Sum                                 |Medium    |          |
|2099|[Find Subsequence of Length K With the Largest Sum](problems/FindSubsequenceOfLengthKWithTheLargestSum.py)|Easy      |          |
|2100|Find Good Days to Rob the Bank                                                  |Medium    |          |
|2101|Detonate the Maximum Bombs                                                      |Medium    |          |
|2102|Sequentially Ordinal Rank Tracker                                               |Hard      |          |
|2103|Rings and Rods                                                                  |Easy      |          |
|2104|Sum of Subarray Ranges                                                          |Medium    |          |
|2105|Watering Plants II                                                              |Medium    |          |
|2106|Maximum Fruits Harvested After at Most K Steps                                  |Hard      |          |
|2107|Number of Unique Flavors After Sharing K Candies                                |Medium    |          |
|2108|Find First Palindromic String in the Array                                      |Easy      |          |
|2109|[Adding Spaces to a String](problems/AddingSpacesToAString.py)                  |Medium    |          |
|2110|Number of Smooth Descent Periods of a Stock                                     |Medium    |          |
|2111|Minimum Operations to Make the Array K-Increasing                               |Hard      |          |
|2112|The Airport With the Most Traffic                                               |Medium    |          |
|2113|Elements in Array After Removing and Replacing Elements                         |Medium    |          |
|2114|Maximum Number of Words Found in Sentences                                      |Easy      |          |
|2115|Find All Possible Recipes from Given Supplies                                   |Medium    |          |
|2116|Check if a Parentheses String Can Be Valid                                      |Medium    |          |
|2117|Abbreviating the Product of a Range                                             |Hard      |          |
|2118|Build the Equation                                                              |Hard      |          |
|2119|A Number After a Double Reversal                                                |Easy      |          |
|2120|Execution of All Suffix Instructions Staying in a Grid                          |Medium    |          |
|2121|Intervals Between Identical Elements                                            |Medium    |          |
|2122|Recover the Original Array                                                      |Hard      |          |
|2123|Minimum Operations to Remove Adjacent Ones in Matrix                            |Hard      |          |
|2124|Check if All A's Appears Before All B's                                         |Easy      |          |
|2125|Number of Laser Beams in a Bank                                                 |Medium    |          |
|2126|Destroying Asteroids                                                            |Medium    |          |
|2127|Maximum Employees to Be Invited to a Meeting                                    |Hard      |          |
|2128|Remove All Ones With Row and Column Flips                                       |Medium    |          |
|2129|Capitalize the Title                                                            |Easy      |          |
|2130|Maximum Twin Sum of a Linked List                                               |Medium    |          |
|2131|Longest Palindrome by Concatenating Two Letter Words                            |Medium    |          |
|2132|Stamping the Grid                                                               |Hard      |          |
|2133|Check if Every Row and Column Contains All Numbers                              |Easy      |          |
|2134|Minimum Swaps to Group All 1's Together II                                      |Medium    |          |
|2135|Count Words Obtained After Adding a Letter                                      |Medium    |          |
|2136|Earliest Possible Day of Full Bloom                                             |Hard      |          |
|2137|Pour Water Between Buckets to Make Water Levels Equal                           |Medium    |          |
|2138|Divide a String Into Groups of Size k                                           |Easy      |          |
|2139|Minimum Moves to Reach Target Score                                             |Medium    |          |
|2140|Solving Questions With Brainpower                                               |Medium    |          |
|2141|Maximum Running Time of N Computers                                             |Hard      |          |
|2142|The Number of Passengers in Each Bus I                                          |Medium    |          |
|2143|Choose Numbers From Two Arrays in Range                                         |Hard      |          |
|2144|Minimum Cost of Buying Candies With Discount                                    |Easy      |          |
|2145|Count the Hidden Sequences                                                      |Medium    |          |
|2146|K Highest Ranked Items Within a Price Range                                     |Medium    |          |
|2147|Number of Ways to Divide a Long Corridor                                        |Hard      |          |
|2148|Count Elements With Strictly Smaller and Greater Elements                       |Easy      |          |
|2149|Rearrange Array Elements by Sign                                                |Medium    |          |
|2150|Find All Lonely Numbers in the Array                                            |Medium    |          |
|2151|Maximum Good People Based on Statements                                         |Hard      |          |
|2152|Minimum Number of Lines to Cover Points                                         |Medium    |          |
|2153|The Number of Passengers in Each Bus II                                         |Hard      |          |
|2154|Keep Multiplying Found Values by Two                                            |Easy      |          |
|2155|All Divisions With the Highest Score of a Binary Array                          |Medium    |          |
|2156|Find Substring With Given Hash Value                                            |Hard      |          |
|2157|Groups of Strings                                                               |Hard      |          |
|2158|Amount of New Area Painted Each Day                                             |Hard      |          |
|2159|Order Two Columns Independently                                                 |Medium    |          |
|2160|Minimum Sum of Four Digit Number After Splitting Digits                         |Easy      |          |
|2161|Partition Array According to Given Pivot                                        |Medium    |          |
|2162|Minimum Cost to Set Cooking Time                                                |Medium    |          |
|2163|Minimum Difference in Sums After Removal of Elements                            |Hard      |          |
|2164|[Sort Even and Odd Indices Independently](problems/SortEvenAndOddIndicesIndependently.py)|Easy      |          |
|2165|[Smallest Value of the Rearranged Number](problems/SmallestValueOfTheRearrangedNumber.py)|Medium    |          |
|2166|Design Bitset                                                                   |Medium    |          |
|2167|Minimum Time to Remove All Cars Containing Illegal Goods                        |Hard      |          |
|2168|Unique Substrings With Equal Digit Frequency                                    |Medium    |          |
|2169|Count Operations to Obtain Zero                                                 |Easy      |          |
|2170|Minimum Operations to Make the Array Alternating                                |Medium    |          |
|2171|Removing Minimum Number of Magic Beans                                          |Medium    |          |
|2172|Maximum AND Sum of Array                                                        |Hard      |          |
|2173|Longest Winning Streak                                                          |Hard      |          |
|2174|Remove All Ones With Row and Column Flips II                                    |Medium    |          |
|2175|The Change in Global Rankings                                                   |Medium    |          |
|2176|Count Equal and Divisible Pairs in an Array                                     |Easy      |          |
|2177|[Find Three Consecutive Integers That Sum to a Given Number](problems/FindThreeConsecutiveIntegersThatSumToAGivenNumber.py)|Medium    |          |
|2178|Maximum Split of Positive Even Integers                                         |Medium    |          |
|2179|Count Good Triplets in an Array                                                 |Hard      |          |
|2180|Count Integers With Even Digit Sum                                              |Easy      |          |
|2181|[Merge Nodes in Between Zeros](problems/MergeNodesInBetweenZeros.py)            |Medium    |          |
|2182|Construct String With Repeat Limit                                              |Medium    |          |
|2183|Count Array Pairs Divisible by K                                                |Hard      |          |
|2184|Number of Ways to Build Sturdy Brick Wall                                       |Medium    |          |
|2185|[Counting Words With a Given Prefix](problems/CountingWordsWithAGivenPrefix.py) |Easy      |          |
|2186|[Minimum Number of Steps to Make Two Strings Anagram II](problems/MinimumNumberOfStepsToMakeTwoStringsAnagramIi.py)|Medium    |          |
|2187|[Minimum Time to Complete Trips](problems/MinimumTimeToCompleteTrips.py)        |Medium    |          |
|2188|Minimum Time to Finish the Race                                                 |Hard      |          |
|2189|Number of Ways to Build House of Cards                                          |Medium    |          |
|2190|Most Frequent Number Following Key In an Array                                  |Easy      |          |
|2191|[Sort the Jumbled Numbers](problems/SortTheJumbledNumbers.py)                   |Medium    |          |
|2192|All Ancestors of a Node in a Directed Acyclic Graph                             |Medium    |          |
|2193|Minimum Number of Moves to Make Palindrome                                      |Hard      |          |
|2194|Cells in a Range on an Excel Sheet                                              |Easy      |          |
|2195|Append K Integers With Minimal Sum                                              |Medium    |          |
|2196|Create Binary Tree From Descriptions                                            |Medium    |          |
|2197|Replace Non-Coprime Numbers in Array                                            |Hard      |          |
|2198|Number of Single Divisor Triplets                                               |Medium    |          |
|2199|Finding the Topic of Each Post                                                  |Hard      |          |
|2200|Find All K-Distant Indices in an Array                                          |Easy      |          |
|2201|Count Artifacts That Can Be Extracted                                           |Medium    |          |
|2202|Maximize the Topmost Element After K Moves                                      |Medium    |          |
|2203|Minimum Weighted Subgraph With the Required Paths                               |Hard      |          |
|2204|Distance to a Cycle in Undirected Graph                                         |Hard      |          |
|2205|The Number of Users That Are Eligible for Discount                              |Easy      |          |
|2206|Divide Array Into Equal Pairs                                                   |Easy      |          |
|2207|Maximize Number of Subsequences in a String                                     |Medium    |          |
|2208|Minimum Operations to Halve Array Sum                                           |Medium    |          |
|2209|Minimum White Tiles After Covering With Carpets                                 |Hard      |          |
|2210|Count Hills and Valleys in an Array                                             |Easy      |          |
|2211|Count Collisions on a Road                                                      |Medium    |          |
|2212|Maximum Points in an Archery Competition                                        |Medium    |          |
|2213|Longest Substring of One Repeating Character                                    |Hard      |          |
|2214|Minimum Health to Beat Game                                                     |Medium    |          |
|2215|Find the Difference of Two Arrays                                               |Easy      |          |
|2216|Minimum Deletions to Make Array Beautiful                                       |Medium    |          |
|2217|Find Palindrome With Fixed Length                                               |Medium    |          |
|2218|Maximum Value of K Coins From Piles                                             |Hard      |          |
|2219|Maximum Sum Score of Array                                                      |Medium    |          |
|2220|Minimum Bit Flips to Convert Number                                             |Easy      |          |
|2221|Find Triangular Sum of an Array                                                 |Medium    |          |
|2222|Number of Ways to Select Buildings                                              |Medium    |          |
|2223|Sum of Scores of Built Strings                                                  |Hard      |          |
|2224|Minimum Number of Operations to Convert Time                                    |Easy      |          |
|2225|Find Players With Zero or One Losses                                            |Medium    |          |
|2226|Maximum Candies Allocated to K Children                                         |Medium    |          |
|2227|Encrypt and Decrypt Strings                                                     |Hard      |          |
|2228|Users With Two Purchases Within Seven Days                                      |Medium    |          |
|2229|Check if an Array Is Consecutive                                                |Easy      |          |
|2230|The Users That Are Eligible for Discount                                        |Easy      |          |
|2231|Largest Number After Digit Swaps by Parity                                      |Easy      |          |
|2232|Minimize Result by Adding Parentheses to Expression                             |Medium    |          |
|2233|Maximum Product After K Increments                                              |Medium    |          |
|2234|Maximum Total Beauty of the Gardens                                             |Hard      |          |
|2235|Add Two Integers                                                                |Easy      |          |
|2236|Root Equals Sum of Children                                                     |Easy      |          |
|2237|Count Positions on Street With Required Brightness                              |Medium    |          |
|2238|Number of Times a Driver Was a Passenger                                        |Medium    |          |
|2239|Find Closest Number to Zero                                                     |Easy      |          |
|2240|Number of Ways to Buy Pens and Pencils                                          |Medium    |          |
|2241|Design an ATM Machine                                                           |Medium    |          |
|2242|Maximum Score of a Node Sequence                                                |Hard      |          |
|2243|Calculate Digit Sum of a String                                                 |Easy      |          |
|2244|Minimum Rounds to Complete All Tasks                                            |Medium    |          |
|2245|Maximum Trailing Zeros in a Cornered Path                                       |Medium    |          |
|2246|Longest Path With Different Adjacent Characters                                 |Hard      |          |
|2247|Maximum Cost of Trip With K Highways                                            |Hard      |          |
|2248|Intersection of Multiple Arrays                                                 |Easy      |          |
|2249|Count Lattice Points Inside a Circle                                            |Medium    |          |
|2250|Count Number of Rectangles Containing Each Point                                |Medium    |          |
|2251|Number of Flowers in Full Bloom                                                 |Hard      |          |
|2252|Dynamic Pivoting of a Table                                                     |Hard      |          |
|2253|Dynamic Unpivoting of a Table                                                   |Hard      |          |
|2254|Design Video Sharing Platform                                                   |Hard      |          |
|2255|[Count Prefixes of a Given String](problems/CountPrefixesOfAGivenString.py)     |Easy      |          |
|2256|Minimum Average Difference                                                      |Medium    |          |
|2257|Count Unguarded Cells in the Grid                                               |Medium    |          |
|2258|Escape the Spreading Fire                                                       |Hard      |          |
|2259|Remove Digit From Number to Maximize Result                                     |Easy      |          |
|2260|Minimum Consecutive Cards to Pick Up                                            |Medium    |          |
|2261|K Divisible Elements Subarrays                                                  |Medium    |          |
|2262|Total Appeal of A String                                                        |Hard      |          |
|2263|Make Array Non-decreasing or Non-increasing                                     |Hard      |          |
|2264|Largest 3-Same-Digit Number in String                                           |Easy      |          |
|2265|Count Nodes Equal to Average of Subtree                                         |Medium    |          |
|2266|Count Number of Texts                                                           |Medium    |          |
|2267|Check if There Is a Valid Parentheses String Path                               |Hard      |          |
|2268|Minimum Number of Keypresses                                                    |Medium    |          |
|2269|Find the K-Beauty of a Number                                                   |Easy      |          |
|2270|Number of Ways to Split Array                                                   |Medium    |          |
|2271|Maximum White Tiles Covered by a Carpet                                         |Medium    |          |
|2272|Substring With Largest Variance                                                 |Hard      |          |
|2273|Find Resultant Array After Removing Anagrams                                    |Easy      |          |
|2274|Maximum Consecutive Floors Without Special Floors                               |Medium    |          |
|2275|Largest Combination With Bitwise AND Greater Than Zero                          |Medium    |          |
|2276|Count Integers in Intervals                                                     |Hard      |          |
|2277|Closest Node to Path in Tree                                                    |Hard      |          |
|2278|Percentage of Letter in String                                                  |Easy      |          |
|2279|Maximum Bags With Full Capacity of Rocks                                        |Medium    |          |
|2280|Minimum Lines to Represent a Line Chart                                         |Medium    |          |
|2281|Sum of Total Strength of Wizards                                                |Hard      |          |
|2282|Number of People That Can Be Seen in a Grid                                     |Medium    |          |
|2283|Check if Number Has Equal Digit Count and Digit Value                           |Easy      |          |
|2284|Sender With Largest Word Count                                                  |Medium    |          |
|2285|Maximum Total Importance of Roads                                               |Medium    |          |
|2286|Booking Concert Tickets in Groups                                               |Hard      |          |
|2287|Rearrange Characters to Make Target String                                      |Easy      |          |
|2288|Apply Discount to Prices                                                        |Medium    |          |
|2289|Steps to Make Array Non-decreasing                                              |Medium    |          |
|2290|Minimum Obstacle Removal to Reach Corner                                        |Hard      |          |
|2291|Maximum Profit From Trading Stocks                                              |Medium    |          |
|2292|Products With Three or More Orders in Two Consecutive Years                     |Medium    |          |
|2293|Min Max Game                                                                    |Easy      |          |
|2294|Partition Array Such That Maximum Difference Is K                               |Medium    |          |
|2295|Replace Elements in an Array                                                    |Medium    |          |
|2296|Design a Text Editor                                                            |Hard      |          |
|2297|Jump Game VIII                                                                  |Medium    |          |
|2298|Tasks Count in the Weekend                                                      |Medium    |          |
|2299|Strong Password Checker II                                                      |Easy      |          |
|2300|Successful Pairs of Spells and Potions                                          |Medium    |          |
|2301|Match Substring After Replacement                                               |Hard      |          |
|2302|Count Subarrays With Score Less Than K                                          |Hard      |          |
|2303|Calculate Amount Paid in Taxes                                                  |Easy      |          |
|2304|Minimum Path Cost in a Grid                                                     |Medium    |          |
|2305|[Fair Distribution of Cookies](problems/FairDistributionOfCookies.py)           |Medium    |          |
|2306|[Naming a Company](problems/NamingACompany.py)                                  |Hard      |          |
|2307|Check for Contradictions in Equations                                           |Hard      |          |
|2308|Arrange Table by Gender                                                         |Medium    |          |
|2309|Greatest English Letter in Upper and Lower Case                                 |Easy      |          |
|2310|Sum of Numbers With Units Digit K                                               |Medium    |          |
|2311|Longest Binary Subsequence Less Than or Equal to K                              |Medium    |          |
|2312|Selling Pieces of Wood                                                          |Hard      |          |
|2313|Minimum Flips in Binary Tree to Get Result                                      |Hard      |          |
|2314|The First Day of the Maximum Recorded Degree in Each City                       |Medium    |          |
|2315|[Count Asterisks](problems/CountAsterisks.py)                                   |Easy      |          |
|2316|Count Unreachable Pairs of Nodes in an Undirected Graph                         |Medium    |          |
|2317|Maximum XOR After Operations                                                    |Medium    |          |
|2318|Number of Distinct Roll Sequences                                               |Hard      |          |
|2319|Check if Matrix Is X-Matrix                                                     |Easy      |          |
|2320|Count Number of Ways to Place Houses                                            |Medium    |          |
|2321|Maximum Score Of Spliced Array                                                  |Hard      |          |
|2322|Minimum Score After Removals on a Tree                                          |Hard      |          |
|2323|Find Minimum Time to Finish All Jobs II                                         |Medium    |          |
|2324|Product Sales Analysis IV                                                       |Medium    |          |
|2325|Decode the Message                                                              |Easy      |          |
|2326|Spiral Matrix IV                                                                |Medium    |          |
|2327|Number of People Aware of a Secret                                              |Medium    |          |
|2328|Number of Increasing Paths in a Grid                                            |Hard      |          |
|2329|Product Sales Analysis V                                                        |Easy      |          |
|2330|Valid Palindrome IV                                                             |Medium    |          |
|2331|Evaluate Boolean Binary Tree                                                    |Easy      |          |
|2332|The Latest Time to Catch a Bus                                                  |Medium    |          |
|2333|Minimum Sum of Squared Difference                                               |Medium    |          |
|2334|Subarray With Elements Greater Than Varying Threshold                           |Hard      |          |
|2335|Minimum Amount of Time to Fill Cups                                             |Easy      |          |
|2336|[Smallest Number in Infinite Set](problems/SmallestNumberInInfiniteSet.py)      |Medium    |          |
|2337|Move Pieces to Obtain a String                                                  |Medium    |          |
|2338|Count the Number of Ideal Arrays                                                |Hard      |          |
|2339|All the Matches of the League                                                   |Easy      |          |
|2340|Minimum Adjacent Swaps to Make a Valid Array                                    |Medium    |          |
|2341|Maximum Number of Pairs in Array                                                |Easy      |          |
|2342|Max Sum of a Pair With Equal Sum of Digits                                      |Medium    |          |
|2343|Query Kth Smallest Trimmed Number                                               |Medium    |          |
|2344|Minimum Deletions to Make Array Divisible                                       |Hard      |          |
|2345|Finding the Number of Visible Mountains                                         |Medium    |          |
|2346|Compute the Rank as a Percentage                                                |Medium    |          |
|2347|Best Poker Hand                                                                 |Easy      |          |
|2348|Number of Zero-Filled Subarrays                                                 |Medium    |          |
|2349|Design a Number Container System                                                |Medium    |          |
|2350|Shortest Impossible Sequence of Rolls                                           |Hard      |          |
|2351|First Letter to Appear Twice                                                    |Easy      |          |
|2352|Equal Row and Column Pairs                                                      |Medium    |          |
|2353|Design a Food Rating System                                                     |Medium    |          |
|2354|Number of Excellent Pairs                                                       |Hard      |          |
|2355|Maximum Number of Books You Can Take                                            |Hard      |          |
|2356|Number of Unique Subjects Taught by Each Teacher                                |Easy      |          |
|2357|Make Array Zero by Subtracting Equal Amounts                                    |Easy      |          |
|2358|Maximum Number of Groups Entering a Competition                                 |Medium    |          |
|2359|Find Closest Node to Given Two Nodes                                            |Medium    |          |
|2360|Longest Cycle in a Graph                                                        |Hard      |          |
|2361|Minimum Costs Using the Train Line                                              |Hard      |          |
|2362|Generate the Invoice                                                            |Hard      |          |
|2363|Merge Similar Items                                                             |Easy      |          |
|2364|Count Number of Bad Pairs                                                       |Medium    |          |
|2365|Task Scheduler II                                                               |Medium    |          |
|2366|Minimum Replacements to Sort the Array                                          |Hard      |          |
|2367|Number of Arithmetic Triplets                                                   |Easy      |          |
|2368|Reachable Nodes With Restrictions                                               |Medium    |          |
|2369|Check if There is a Valid Partition For The Array                               |Medium    |          |
|2370|Longest Ideal Subsequence                                                       |Medium    |          |
|2371|Minimize Maximum Value in a Grid                                                |Hard      |          |
|2372|Calculate the Influence of Each Salesperson                                     |Medium    |          |
|2373|Largest Local Values in a Matrix                                                |Easy      |          |
|2374|Node With Highest Edge Score                                                    |Medium    |          |
|2375|Construct Smallest Number From DI String                                        |Medium    |          |
|2376|Count Special Integers                                                          |Hard      |          |
|2377|Sort the Olympic Table                                                          |Easy      |          |
|2378|Choose Edges to Maximize Score in a Tree                                        |Medium    |          |
|2379|Minimum Recolors to Get K Consecutive Black Blocks                              |Easy      |          |
|2380|Time Needed to Rearrange a Binary String                                        |Medium    |          |
|2381|Shifting Letters II                                                             |Medium    |          |
|2382|Maximum Segment Sum After Removals                                              |Hard      |          |
|2383|Minimum Hours of Training to Win a Competition                                  |Easy      |          |
|2384|Largest Palindromic Number                                                      |Medium    |          |
|2385|Amount of Time for Binary Tree to Be Infected                                   |Medium    |          |
|2386|Find the K-Sum of an Array                                                      |Hard      |          |
|2387|Median of a Row Wise Sorted Matrix                                              |Medium    |          |
|2388|Change Null Values in a Table to the Previous Value                             |Medium    |          |
|2389|Longest Subsequence With Limited Sum                                            |Easy      |          |
|2390|Removing Stars From a String                                                    |Medium    |          |
|2391|Minimum Amount of Time to Collect Garbage                                       |Medium    |          |
|2392|Build a Matrix With Conditions                                                  |Hard      |          |
|2393|Count Strictly Increasing Subarrays                                             |Medium    |          |
|2394|Employees With Deductions                                                       |Medium    |          |
|2395|Find Subarrays With Equal Sum                                                   |Easy      |          |
|2396|Strictly Palindromic Number                                                     |Medium    |          |
|2397|Maximum Rows Covered by Columns                                                 |Medium    |          |
|2398|Maximum Number of Robots Within Budget                                          |Hard      |          |
|2399|Check Distances Between Same Letters                                            |Easy      |          |
|2400|Number of Ways to Reach a Position After Exactly k Steps                        |Medium    |          |
|2401|Longest Nice Subarray                                                           |Medium    |          |
|2402|Meeting Rooms III                                                               |Hard      |          |
|2403|Minimum Time to Kill All Monsters                                               |Hard      |          |
|2404|Most Frequent Even Element                                                      |Easy      |          |
|2405|Optimal Partition of String                                                     |Medium    |          |
|2406|Divide Intervals Into Minimum Number of Groups                                  |Medium    |          |
|2407|Longest Increasing Subsequence II                                               |Hard      |          |
|2408|Design SQL                                                                      |Medium    |          |
|2409|Count Days Spent Together                                                       |Easy      |          |
|2410|Maximum Matching of Players With Trainers                                       |Medium    |          |
|2411|Smallest Subarrays With Maximum Bitwise OR                                      |Medium    |          |
|2412|Minimum Money Required Before Transactions                                      |Hard      |          |
|2413|Smallest Even Multiple                                                          |Easy      |          |
|2414|Length of the Longest Alphabetical Continuous Substring                         |Medium    |          |
|2415|Reverse Odd Levels of Binary Tree                                               |Medium    |          |
|2416|Sum of Prefix Scores of Strings                                                 |Hard      |          |
|2417|Closest Fair Integer                                                            |Medium    |          |
|2418|Sort the People                                                                 |Easy      |          |
|2419|Longest Subarray With Maximum Bitwise AND                                       |Medium    |          |
|2420|Find All Good Indices                                                           |Medium    |          |
|2421|Number of Good Paths                                                            |Hard      |          |
|2422|Merge Operations to Turn Array Into a Palindrome                                |Medium    |          |
|2423|Remove Letter To Equalize Frequency                                             |Easy      |          |
|2424|Longest Uploaded Prefix                                                         |Medium    |          |
|2425|Bitwise XOR of All Pairings                                                     |Medium    |          |
|2426|Number of Pairs Satisfying Inequality                                           |Hard      |          |
|2427|Number of Common Factors                                                        |Easy      |          |
|2428|Maximum Sum of an Hourglass                                                     |Medium    |          |
|2429|Minimize XOR                                                                    |Medium    |          |
|2430|Maximum Deletions on a String                                                   |Hard      |          |
|2431|Maximize Total Tastiness of Purchased Fruits                                    |Medium    |          |
|2432|The Employee That Worked on the Longest Task                                    |Easy      |          |
|2433|Find The Original Array of Prefix Xor                                           |Medium    |          |
|2434|Using a Robot to Print the Lexicographically Smallest String                    |Medium    |          |
|2435|Paths in Matrix Whose Sum Is Divisible by K                                     |Hard      |          |
|2436|Minimum Split Into Subarrays With GCD Greater Than One                          |Medium    |          |
|2437|Number of Valid Clock Times                                                     |Easy      |          |
|2438|Range Product Queries of Powers                                                 |Medium    |          |
|2439|Minimize Maximum of Array                                                       |Medium    |          |
|2440|Create Components With Same Value                                               |Hard      |          |
|2441|Largest Positive Integer That Exists With Its Negative                          |Easy      |          |
|2442|Count Number of Distinct Integers After Reverse Operations                      |Medium    |          |
|2443|Sum of Number and Its Reverse                                                   |Medium    |          |
|2444|[Count Subarrays With Fixed Bounds](problems/CountSubarraysWithFixedBounds.py)  |Hard      |          |
|2445|Number of Nodes With Value One                                                  |Medium    |          |
|2446|Determine if Two Events Have Conflict                                           |Easy      |          |
|2447|Number of Subarrays With GCD Equal to K                                         |Medium    |          |
|2448|Minimum Cost to Make Array Equal                                                |Hard      |          |
|2449|Minimum Number of Operations to Make Arrays Similar                             |Hard      |          |
|2450|Number of Distinct Binary Strings After Applying Operations                     |Medium    |          |
|2451|Odd String Difference                                                           |Easy      |          |
|2452|Words Within Two Edits of Dictionary                                            |Medium    |          |
|2453|Destroy Sequential Targets                                                      |Medium    |          |
|2454|Next Greater Element IV                                                         |Hard      |          |
|2455|Average Value of Even Numbers That Are Divisible by Three                       |Easy      |          |
|2456|Most Popular Video Creator                                                      |Medium    |          |
|2457|Minimum Addition to Make Integer Beautiful                                      |Medium    |          |
|2458|Height of Binary Tree After Subtree Removal Queries                             |Hard      |          |
|2459|Sort Array by Moving Items to Empty Space                                       |Hard      |          |
|2460|Apply Operations to an Array                                                    |Easy      |          |
|2461|Maximum Sum of Distinct Subarrays With Length K                                 |Medium    |          |
|2462|Total Cost to Hire K Workers                                                    |Medium    |          |
|2463|Minimum Total Distance Traveled                                                 |Hard      |          |
|2464|Minimum Subarrays in a Valid Split                                              |Medium    |          |
|2465|Number of Distinct Averages                                                     |Easy      |          |
|2466|Count Ways To Build Good Strings                                                |Medium    |          |
|2467|Most Profitable Path in a Tree                                                  |Medium    |          |
|2468|Split Message Based on Limit                                                    |Hard      |          |
|2469|Convert the Temperature                                                         |Easy      |          |
|2470|Number of Subarrays With LCM Equal to K                                         |Medium    |          |
|2471|Minimum Number of Operations to Sort a Binary Tree by Level                     |Medium    |          |
|2472|Maximum Number of Non-overlapping Palindrome Substrings                         |Hard      |          |
|2473|Minimum Cost to Buy Apples                                                      |Medium    |          |
|2474|Customers With Strictly Increasing Purchases                                    |Hard      |          |
|2475|Number of Unequal Triplets in Array                                             |Easy      |          |
|2476|Closest Nodes Queries in a Binary Search Tree                                   |Medium    |          |
|2477|[Minimum Fuel Cost to Report to the Capital](problems/MinimumFuelCostToReportToTheCapital.py)|Medium    |          |
|2478|Number of Beautiful Partitions                                                  |Hard      |          |
|2479|Maximum XOR of Two Non-Overlapping Subtrees                                     |Hard      |          |
|2480|Form a Chemical Bond                                                            |Easy      |          |
|2481|Minimum Cuts to Divide a Circle                                                 |Easy      |          |
|2482|Difference Between Ones and Zeros in Row and Column                             |Medium    |          |
|2483|Minimum Penalty for a Shop                                                      |Medium    |          |
|2484|Count Palindromic Subsequences                                                  |Hard      |          |
|2485|Find the Pivot Integer                                                          |Easy      |          |
|2486|Append Characters to String to Make Subsequence                                 |Medium    |          |
|2487|Remove Nodes From Linked List                                                   |Medium    |          |
|2488|Count Subarrays With Median K                                                   |Hard      |          |
|2489|Number of Substrings With Fixed Ratio                                           |Medium    |          |
|2490|Circular Sentence                                                               |Easy      |          |
|2491|Divide Players Into Teams of Equal Skill                                        |Medium    |          |
|2492|Minimum Score of a Path Between Two Cities                                      |Medium    |          |
|2493|Divide Nodes Into the Maximum Number of Groups                                  |Hard      |          |
|2494|Merge Overlapping Events in the Same Hall                                       |Hard      |          |
|2495|Number of Subarrays Having Even Product                                         |Medium    |          |
|2496|Maximum Value of a String in an Array                                           |Easy      |          |
|2497|Maximum Star Sum of a Graph                                                     |Medium    |          |
|2498|Frog Jump II                                                                    |Medium    |          |
|2499|Minimum Total Cost to Make Arrays Unequal                                       |Hard      |          |
|2500|Delete Greatest Value in Each Row                                               |Easy      |          |
|2501|[Longest Square Streak in an Array](problems/LongestSquareStreakInAnArray.py)   |Medium    |          |
|2502|[Design Memory Allocator](problems/DesignMemoryAllocator.py)                    |Medium    |          |
|2503|Maximum Number of Points From Grid Queries                                      |Hard      |          |
|2504|Concatenate the Name and the Profession                                         |Easy      |          |
|2505|Bitwise OR of All Subsequence Sums                                              |Medium    |          |
|2506|Count Pairs Of Similar Strings                                                  |Easy      |          |
|2507|Smallest Value After Replacing With Sum of Prime Factors                        |Medium    |          |
|2508|Add Edges to Make Degrees of All Nodes Even                                     |Hard      |          |
|2509|Cycle Length Queries in a Tree                                                  |Hard      |          |
|2510|Check if There is a Path With Equal Number of 0's And 1's                       |Medium    |          |
|2511|Maximum Enemy Forts That Can Be Captured                                        |Easy      |          |
|2512|Reward Top K Students                                                           |Medium    |          |
|2513|Minimize the Maximum of Two Arrays                                              |Medium    |          |
|2514|Count Anagrams                                                                  |Hard      |          |
|2515|Shortest Distance to Target String in a Circular Array                          |Easy      |          |
|2516|Take K of Each Character From Left and Right                                    |Medium    |          |
|2517|Maximum Tastiness of Candy Basket                                               |Medium    |          |
|2518|Number of Great Partitions                                                      |Hard      |          |
|2519|Count the Number of K-Big Indices                                               |Hard      |          |
|2520|Count the Digits That Divide a Number                                           |Easy      |          |
|2521|Distinct Prime Factors of Product of Array                                      |Medium    |          |
|2522|Partition String Into Substrings With Values at Most K                          |Medium    |          |
|2523|Closest Prime Numbers in Range                                                  |Medium    |          |
|2524|Maximum Frequency Score of a Subarray                                           |Hard      |          |
|2525|Categorize Box According to Criteria                                            |Easy      |          |
|2526|Find Consecutive Integers from a Data Stream                                    |Medium    |          |
|2527|Find Xor-Beauty of Array                                                        |Medium    |          |
|2528|Maximize the Minimum Powered City                                               |Hard      |          |
|2529|Maximum Count of Positive Integer and Negative Integer                          |Easy      |          |
|2530|[Maximal Score After Applying K Operations](problems/MaximalScoreAfterApplyingKOperations.py)|Medium    |          |
|2531|Make Number of Distinct Characters Equal                                        |Medium    |          |
|2532|Time to Cross a Bridge                                                          |Hard      |          |
|2533|Number of Good Binary Strings                                                   |Medium    |          |
|2534|Time Taken to Cross the Door                                                    |Hard      |          |
|2535|Difference Between Element Sum and Digit Sum of an Array                        |Easy      |          |
|2536|Increment Submatrices by One                                                    |Medium    |          |
|2537|Count the Number of Good Subarrays                                              |Medium    |          |
|2538|Difference Between Maximum and Minimum Price Sum                                |Hard      |          |
|2539|Count the Number of Good Subsequences                                           |Medium    |          |
|2540|Minimum Common Value                                                            |Easy      |          |
|2541|Minimum Operations to Make Array Equal II                                       |Medium    |          |
|2542|Maximum Subsequence Score                                                       |Medium    |          |
|2543|Check if Point Is Reachable                                                     |Hard      |          |
|2544|Alternating Digit Sum                                                           |Easy      |          |
|2545|[Sort the Students by Their Kth Score](problems/SortTheStudentsByTheirKthScore.py)|Medium    |          |
|2546|Apply Bitwise Operations to Make Strings Equal                                  |Medium    |          |
|2547|Minimum Cost to Split an Array                                                  |Hard      |          |
|2548|Maximum Price to Fill a Bag                                                     |Medium    |          |
|2549|Count Distinct Numbers on Board                                                 |Easy      |          |
|2550|Count Collisions of Monkeys on a Polygon                                        |Medium    |          |
|2551|Put Marbles in Bags                                                             |Hard      |          |
|2552|Count Increasing Quadruplets                                                    |Hard      |          |
|2553|Separate the Digits in an Array                                                 |Easy      |          |
|2554|Maximum Number of Integers to Choose From a Range I                             |Medium    |          |
|2555|Maximize Win From Two Segments                                                  |Medium    |          |
|2556|Disconnect Path in a Binary Matrix by at Most One Flip                          |Medium    |          |
|2557|Maximum Number of Integers to Choose From a Range II                            |Medium    |          |
|2558|Take Gifts From the Richest Pile                                                |Easy      |          |
|2559|[Count Vowel Strings in Ranges](problems/CountVowelStringsInRanges.py)          |Medium    |          |
|2560|House Robber IV                                                                 |Medium    |          |
|2561|Rearranging Fruits                                                              |Hard      |          |
|2562|Find the Array Concatenation Value                                              |Easy      |          |
|2563|Count the Number of Fair Pairs                                                  |Medium    |          |
|2564|Substring XOR Queries                                                           |Medium    |          |
|2565|Subsequence With the Minimum Score                                              |Hard      |          |
|2566|[Maximum Difference by Remapping a Digit](problems/MaximumDifferenceByRemappingADigit.py)|Easy      |          |
|2567|Minimum Score by Changing Two Elements                                          |Medium    |          |
|2568|Minimum Impossible OR                                                           |Medium    |          |
|2569|Handling Sum Queries After Update                                               |Hard      |          |
|2570|[Merge Two 2D Arrays by Summing Values](problems/MergeTwo2dArraysBySummingValues.py)|Easy      |          |
|2571|Minimum Operations to Reduce an Integer to 0                                    |Medium    |          |
|2572|Count the Number of Square-Free Subsets                                         |Medium    |          |
|2573|Find the String with LCP                                                        |Hard      |          |
|2574|[Left and Right Sum Differences](problems/LeftAndRightSumDifferences.py)        |Easy      |          |
|2575|Find the Divisibility Array of a String                                         |Medium    |          |
|2576|Find the Maximum Number of Marked Indices                                       |Medium    |          |
|2577|Minimum Time to Visit a Cell In a Grid                                          |Hard      |          |
|2578|Split With Minimum Sum                                                          |Easy      |          |
|2579|[Count Total Number of Colored Cells](problems/CountTotalNumberOfColoredCells.py)|Medium    |          |
|2580|Count Ways to Group Overlapping Ranges                                          |Medium    |          |
|2581|Count Number of Possible Root Nodes                                             |Hard      |          |
|2582|Pass the Pillow                                                                 |Easy      |          |
|2583|[Kth Largest Sum in a Binary Tree](problems/KthLargestSumInABinaryTree.py)      |Medium    |          |
|2584|Split the Array to Make Coprime Products                                        |Hard      |          |
|2585|Number of Ways to Earn Points                                                   |Hard      |          |
