/// Source : https://leetcode.com/problems/number-of-zero-filled-subarrays/
/// Author : karrenbelt
/// Date   : 2023-03-21

/***************************************************************************************************** 
 *
 * Given an integer array nums, return the number of subarrays filled with 0.
 * 
 * A subarray is a contiguous non-empty sequence of elements within an array.
 * 
 * Example 1:
 * 
 * Input: nums = [1,3,0,0,2,0,0,4]
 * Output: 6
 * Explanation: 
 * There are 4 occurrences of [0] as a subarray.
 * There are 2 occurrences of [0,0] as a subarray.
 * There is no occurrence of a subarray with a size more than 2 filled with 0. Therefore, we return 6.
 * 
 * Example 2:
 * 
 * Input: nums = [0,0,0,2,0,0]
 * Output: 9
 * Explanation:
 * There are 5 occurrences of [0] as a subarray.
 * There are 3 occurrences of [0,0] as a subarray.
 * There is 1 occurrence of [0,0,0] as a subarray.
 * There is no occurrence of a subarray with a size more than 3 filled with 0. Therefore, we return 9.
 * 
 * Example 3:
 * 
 * Input: nums = [2,10,2019]
 * Output: 0
 * Explanation: There is no subarray filled with 0. Therefore, we return 0.
 * 
 * Constraints:
 * 
 * 	1 <= nums.length <= 105
 * 	-109 <= nums[i] <= 109
 ******************************************************************************************************/

pub struct Solution;


impl Solution {
    pub fn zero_filled_subarray(nums: Vec<i32>) -> i64 {
        let mut ctr = 0;
        let mut number_of_subarray_starting_at_i = 0;
        
        for i in 0..zero_filled_subarray.len() {
        	if zero_filled_subarray[i] == 0 {
        		number_of_subarray_starting_at_i += 1;
        		ctr += number_of_subarray_starting_at_i;
        	} else {
        		number_of_subarray_starting_at_i = 0;
        	}
        }
    }
}


#[cfg(test)]
mod tests {
	use super::*;
	
	#[test]
	fn test_1 () {
		let nums: Vec<i32> = vec![1, 3, 0, 0, 2, 0, 0, 4];
    	let solution = Solution::zero_filled_subarray(nums);
		assert_eq!(solution, 6)
	}
	
}