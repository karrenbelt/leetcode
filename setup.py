import sys
from distutils.core import setup

VERSION = '0.1.0'
CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 10)

if CURRENT_PYTHON < REQUIRED_PYTHON:
    message = "unsupported python version: {}.{} should be {}.{}"
    sys.stderr.write(message.format(*CURRENT_PYTHON + REQUIRED_PYTHON))

setup(
    name='leetcode',
    version=VERSION,
    description='',
    python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
    author='M.A.P. Karrenbelt',
    author_email='m.a.p.karrenbelt@gmail.com',
    url='https://gitlab.com/karrenbelt/leetcode',
    packages=[],
    install_requires=[
        "sortedcontainers==2.4.0",
    ],
    dependency_links=[]
)
