
import os
from pathlib import Path


def get_file_names() -> set[str]:
    path = Path(".", "python3", "problems").absolute()
    return {f.name for f in path.iterdir()}


def update():

    empty, space, col_sep = "", " ", "|"
    path = Path(".", "python3", "README.md").absolute()
    lines = []
    file_names = get_file_names()
    expected_file_names = set()

    def get_name(s: str) -> str:
        return s.split("]")[0][s.startswith('['):]

    def format_title(s: str):
        s = get_name(s).replace("'", "").replace("(", "").replace(")", "")
        words = map(str.capitalize, s.replace("-", " ").split())
        return "".join("".join(filter(str.isalnum, w)) for w in words) + ".py"

    for line in path.read_text().split(os.linesep):
        if not line.startswith(col_sep):
            lines.append(line)
            continue
        elems = tuple(filter(None, line.split(col_sep)))
        assert 3 <= len(elems) <= 4
        number, title, difficulty, *premium = map(str.strip, elems)

        file_name = format_title(title)
        if file_name in file_names:
            title = f"[{get_name(title)}](problems/{file_name})"

        expected_file_names.add(file_name)
        line = [
            f"{number.ljust(4)}",
            f"{title.ljust(80)}",
            f"{difficulty.ljust(10)}",
            "".join(premium).ljust(10)
        ]
        lines.append(col_sep + col_sep.join(filter(None, line)) + col_sep)

    non_matching = file_names.difference(expected_file_names)
    if non_matching:
        print(f"Non-matching {len(non_matching)}: {' '.join(sorted(non_matching))}")

    text = os.linesep.join(lines)
    path.write_text(text)
    print(f"Updated: {path}")


if __name__ == "__main__":
    update()
