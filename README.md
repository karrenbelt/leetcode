
# Leetcode
solutions to exercises and problems from [leetcode.com](https://leetcode.com/)

## TODO:
- clean-up \_\_init__.py

### Grouping
group exercises by similarity
e.g. 
- subarray: 523, 560, 363, 1074, 713, 974, 1590

### Tests & Tags
special cases requiring attention
- problem 116 & 117
- problem 133, 138, 141, 142  (circular references)
- problem 146
- 211, 430
- 341: nested list iterator test
- concurrency tests: 1114, 1116
